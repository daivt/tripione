/* eslint-disable no-undef */
importScripts('https://www.gstatic.com/firebasejs/7.24.0/firebase-app.js');
importScripts('https://www.gstatic.com/firebasejs/7.24.0/firebase-messaging.js');

firebase.initializeApp({
  apiKey: 'AIzaSyA5d5jzlRtRwsb5i_awx67qb85DJ7lvFsY',
  authDomain: 'tripi-fcm-test.firebaseapp.com',
  databaseURL: 'https://tripi-fcm-test.firebaseio.com',
  projectId: 'tripi-fcm-test',
  storageBucket: 'tripi-fcm-test.appspot.com',
  messagingSenderId: '284571539832',
  appId: '1:284571539832:web:ef5646a59279ecda16508d',
  measurementId: 'G-MT01EGNXSW',
});

const messaging = firebase.messaging();

messaging.setBackgroundMessageHandler(function(payload) {
  const promiseChain = clients
    .matchAll({
      type: 'window',
      includeUncontrolled: true,
    })
    .then(windowClients => {
      for (let i = 0; i < windowClients.length; i++) {
        const windowClient = windowClients[i];
        windowClient.postMessage(payload);
      }
    })
    .then(() => {
      return registration.showNotification('my notification title');
    });
  return promiseChain;
});

self.addEventListener('notificationclick', function(event) {
  // do what you want
  // ...
});
