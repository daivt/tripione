const proxy = require('http-proxy-middleware');

const router = {
  '/api/one/': 'https://gate.dev.tripi.vn',
  '/api/payment/': 'https://gate.dev.tripi.vn',
  '/api/gate/': 'https://gate.dev.tripi.vn',
};

module.exports = function(app) {
  app.use(
    proxy('/api/', {
      target: 'https://gate.dev.tripi.vn',
      changeOrigin: true,
      secure: false,
      pathRewrite: {
        '^/api/one/': '/tripione/',
        '^/api/payment/': '/payment/',
        '^/api/gate/': '/',
      },
      router,
      logLevel: 'debug',
    }),
  );
};
