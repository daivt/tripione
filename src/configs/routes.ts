import React from 'react';
import { BookingServices, RoutesTabType, ServiceType } from '../models/permission';
import TripPay from '../modules/tripi-one/tripManagement/pay/pages/TripPay';

const NotFoundBox = React.lazy(() => import('../modules/common/components/NotFoundBox'));
const DashBoard = React.lazy(() => import('../modules/tripi-one/dashboard/pages/DashBoard'));
const QuickSetup = React.lazy(() => import('../modules/tripi-one/quickSetup/pages/QuickSetup'));

/* -------------GeneralSetting----------------- */
const Company = React.lazy(() =>
  import('../modules/tripi-one/generalSetting/company/pages/Company'),
);
const Department = React.lazy(() =>
  import('../modules/tripi-one/generalSetting/department/pages/Department'),
);
const UserManagement = React.lazy(() =>
  import('../modules/tripi-one/generalSetting/userManagement/pages/UserManagement'),
);
const CreateUpdateUser = React.lazy(() =>
  import('../modules/tripi-one/generalSetting/userManagement/pages/CreateUpdateUser'),
);
const UploadUsers = React.lazy(() =>
  import('../modules/tripi-one/generalSetting/userManagement/pages/UploadUsers'),
);
const RoleManagement = React.lazy(() =>
  import('../modules/tripi-one/generalSetting/roleManagement/pages/RoleManagement'),
);

const Position = React.lazy(() =>
  import('../modules/tripi-one/generalSetting/position/pages/Position'),
);

const PaymentMethod = React.lazy(() =>
  import('../modules/tripi-one/generalSetting/paymentMethod/pages/PaymentMethod'),
);

const Approval = React.lazy(() =>
  import('../modules/tripi-one/generalSetting/approval/pages/Approval'),
);
const CreateApproval = React.lazy(() =>
  import('../modules/tripi-one/generalSetting/approval/pages/CreateUpdateApproval'),
);
const Policy = React.lazy(() => import('../modules/tripi-one/generalSetting/policy/pages/Policy'));

const CreateUpdateGroupPolicy = React.lazy(() =>
  import('../modules/tripi-one/generalSetting/policy/pages/CreateUpdateGroupPolicy'),
);
const CreateUpdatePersonPolicy = React.lazy(() =>
  import('../modules/tripi-one/generalSetting/policy/pages/CreateUpdatePersonPolicy'),
);

/* -------------Invoices----------------- */

const Invoices = React.lazy(() => import('../modules/tripi-one/invoices/pages/InvoicesManager'));
const CreateUpdateDepartment = React.lazy(() =>
  import('../modules/tripi-one/generalSetting/department/pages/CreateUpdateDepartment'),
);

/* -------------Transaction----------------- */
const TransactionsHistory = React.lazy(() =>
  import('../modules/tripi-one/transactions/history/pages/TransactionsHistory'),
);

const TransactionsTopup = React.lazy(() =>
  import('../modules/tripi-one/transactions/topup/pages/TransactionsTopup'),
);

const TransactionsTopupInstruction = React.lazy(() =>
  import('../modules/tripi-one/transactions/topup/pages/TransactionsTopupInstruction'),
);

const TransactionsWithdraw = React.lazy(() =>
  import('../modules/tripi-one/transactions/withdraw/pages/TransactionsWithdraw'),
);

/* -------------Account Info----------------- */
const AccountInfo = React.lazy(() => import('../modules/tripi-one/accountInfo/pages/AccountInfo'));

/* -------------Report----------------- */
const Report = React.lazy(() => import('../modules/tripi-one/report/pages/Report'));

/* -------------Flight Booking----------------- */
const Flight = React.lazy(() => import('../modules/booking/flight/default/pages/Flight'));

const FlightResult = React.lazy(() =>
  import('../modules/booking/flight/result/pages/FlightResult'),
);
const FlightBookingInfo = React.lazy(() =>
  import('../modules/booking/flight/booking/pages/FlightBookingInfo'),
);
const FlightReview = React.lazy(() =>
  import('../modules/booking/flight/review/pages/FlightReview'),
);

/* -------------Hotel Booking----------------- */
const Hotel = React.lazy(() => import('../modules/booking/hotel/default/pages/Hotel'));

const HotelResult = React.lazy(() => import('../modules/booking/hotel/result/pages/HotelResult'));
const HotelDetail = React.lazy(() => import('../modules/booking/hotel/detail/pages/HotelDetail'));
const HotelBookingInfo = React.lazy(() =>
  import('../modules/booking/hotel/bookingInfo/pages/HotelBookingInfo'),
);

export const BOOKING_URL = '/booking';
function buildRoutePath(moduleName: ServiceType, path: string) {
  return `/${moduleName}${path}`;
}
function buildRoutePathBooking(moduleName: BookingServices, path: string) {
  return `${BOOKING_URL}/${moduleName}${path}`;
}

/* -------------Admin Approving Corporate Account----------------- */

const AdminApprovingCorporateAccount = React.lazy(() =>
  import(
    '../modules/tripi-one/adminApprovingCorporateAccount/pages/AdminApprovingCorporateAccount'
  ),
);

const AdminApprovalAccount = React.lazy(() =>
  import('../modules/tripi-one/adminApprovingCorporateAccount/pages/ApprovalAccount'),
);

/* -------------Admin Corporate----------------- */
const AdminCorporateResult = React.lazy(() =>
  import('../modules/tripi-one/adminCorporate/result/pages/AdminCorporateResult'),
);

const AdminCorporateDetail = React.lazy(() =>
  import('../modules/tripi-one/adminCorporate/detail/pages/AdminCorporateDetail'),
);

/* -------------Trip Management----------------- */

const TripManagementResult = React.lazy(() =>
  import('../modules/tripi-one/tripManagement/result/pages/TripManagementResult'),
);

const TripManagementDetail = React.lazy(() =>
  import('../modules/tripi-one/tripManagement/detail/pages/TripManagementDetail'),
);

export const ROUTES = {
  login: '/login',
  signUp: '/signUp',
  forgotPass: '/forgotPass',
  firstLogin: '/firstLogin',
  register: '/register',
  verify: '/accounts/verify',
  resetPassword: '/accounts/resetPassword',
  changePassword: '/changePassword',
  transactionSummary: '/transactionSummary',
  accountInfo: '/accountInfo',
  terms: '/terms',
  notFound404: '/404',
  invoices: buildRoutePath('invoices', ''),
  report: {
    result: buildRoutePath('report', '/:tabIndex'),
    trip: buildRoutePath('report', '/trip'),
    flight: buildRoutePath('report', '/flight'),
    hotel: buildRoutePath('report', '/hotel'),
  },

  booking: {
    notFound404: '/booking/404',
    pay: buildRoutePathBooking('pay', ''),
    flight: {
      default: buildRoutePathBooking('flight', '/default'),
      result: buildRoutePathBooking('flight', '/result'),
      bookingInfo: buildRoutePathBooking('flight', '/bookingInfo'),
      review: buildRoutePathBooking('flight', '/review'),
      pay: buildRoutePathBooking('flight', '/pay'),
    },
    hotel: {
      default: buildRoutePathBooking('hotel', '/default'),
      result: buildRoutePathBooking('hotel', '/result'),
      detail: buildRoutePathBooking('hotel', '/detail'),
      bookingInfo: buildRoutePathBooking('hotel', '/bookingInfo'),
      pay: buildRoutePathBooking('hotel', '/pay'),
    },
  },
  generalSetting: {
    company: buildRoutePath('generalSetting', '/company'),
    department: {
      result: buildRoutePath('generalSetting', '/department'),
      create: buildRoutePath('generalSetting', '/department/create'),
      update: buildRoutePath('generalSetting', '/department/update'),
    },
    position: buildRoutePath('generalSetting', '/position'),
    userManagement: {
      result: buildRoutePath('generalSetting', '/userManagement'),
      create: buildRoutePath('generalSetting', '/userManagement/create'),
      update: buildRoutePath('generalSetting', '/userManagement/update'),
      upload: buildRoutePath('generalSetting', '/userManagement/upload'),
    },
    roleManagement: buildRoutePath('generalSetting', '/roleManagement'),
    paymentMethod: buildRoutePath('generalSetting', '/paymentMethod'),
    approval: {
      result: buildRoutePath('generalSetting', '/approval'),
      create: buildRoutePath('generalSetting', '/approval/create'),
      update: buildRoutePath('generalSetting', '/approval/update'),
    },
    policy: {
      result: {
        value: buildRoutePath('generalSetting', '/policy/action/:tabIndex'),
        gen: (tabIndex: number) => buildRoutePath('generalSetting', `/policy/action/${tabIndex}`),
      },
      updateGroup: buildRoutePath('generalSetting', '/policy/updateGroup'),
      createGroup: buildRoutePath('generalSetting', '/policy/createGroup'),
      updatePerson: buildRoutePath('generalSetting', '/policy/updatePerson'),
      createPerson: buildRoutePath('generalSetting', '/policy/createPerson'),
    },
  },
  transactions: {
    history: buildRoutePath('transactions', '/history'),
    topup: buildRoutePath('transactions', '/topup'),
    topupInstruction: buildRoutePath('transactions', '/topupInstruction'),
    withdraw: buildRoutePath('transactions', '/withdraw'),
  },
  adminCorporate: {
    result: buildRoutePath('admin', '/corporate/result'),
    detail: {
      value: buildRoutePath('admin', '/corporate/detail/:id'),
      gen: (id: number) => buildRoutePath('admin', `/corporate/detail/${id}`),
    },
  },
  adminApprovingCorporateAccount: {
    result: buildRoutePath('admin', '/approval/management'),
    approval: buildRoutePath('admin', '/approval/newApproval'),
    history: buildRoutePath('admin', '/approval/history'),
  },
  tripManagement: {
    result: buildRoutePath('tripManagement', '/result'),
    detail: {
      value: buildRoutePath('tripManagement', '/detail/:tripId'),
      gen: (tripId: number) => buildRoutePath('tripManagement', `/detail/${tripId}`),
    },
  },
};

export const ROUTES_TAB: RoutesTabType[] = [
  {
    name: 'notFound404',
    isModule: true,
    path: ROUTES.notFound404,
    component: NotFoundBox,
    disableBreadcrumb: true,
    hidden: true,
  },
  // {
  //   path: '/',
  //   exact: true,
  //   name: 'overView',
  //   isModule: true,
  //   component: DashBoard,
  //   hidden: true,
  // },
  {
    name: 'accountInfo',
    isModule: true,
    path: ROUTES.accountInfo,
    component: AccountInfo,
    hidden: true, // temporarily hide
  },
  {
    name: 'generalSetting',
    isModule: true,
    subMenu: [
      {
        path: ROUTES.generalSetting.company,
        name: 'company',
        component: Company,
        listRole: [
          'tripione:corporatePortal:company:view',
          'tripione:corporatePortal:company:edit',
        ],
      },
      {
        path: ROUTES.generalSetting.department.result,
        name: 'department',
        component: Department,
        listRole: [
          'tripione:corporatePortal:department:filter',
          'tripione:corporatePortal:department:create',
        ],
        hiddenMenu: [
          {
            path: ROUTES.generalSetting.department.create,
            name: 'department.create',
            component: CreateUpdateDepartment,
            listRole: ['tripione:corporatePortal:department:create'],
          },
          {
            path: ROUTES.generalSetting.department.update,
            name: 'department.update',
            component: CreateUpdateDepartment,
            listRole: ['tripione:corporatePortal:department:update'],
          },
        ],
      },
      {
        path: ROUTES.generalSetting.position,
        name: 'position',
        component: Position,
        listRole: [
          'tripione:corporatePortal:jobTitle:filter',
          'tripione:corporatePortal:jobTitle:create',
        ],
      },
      {
        path: ROUTES.generalSetting.userManagement.result,
        name: 'userManagement',
        component: UserManagement,
        listRole: ['tripione:corporatePortal:user:filter', 'tripione:corporatePortal:user:create'],
        hiddenMenu: [
          {
            path: ROUTES.generalSetting.userManagement.create,
            name: 'userManagement.create',
            component: CreateUpdateUser,
            listRole: ['tripione:corporatePortal:user:create'],
          },
          {
            path: ROUTES.generalSetting.userManagement.update,
            name: 'userManagement.update',
            component: CreateUpdateUser,
            listRole: [
              'tripione:corporatePortal:user:update',
              'tripione:corporatePortal:user:delete',
            ],
          },
          {
            path: ROUTES.generalSetting.userManagement.upload,
            name: 'userManagement.upload',
            component: UploadUsers,
            listRole: [
              'tripione:corporatePortal:user:upload',
              'tripione:corporatePortal:user:saveUsers',
              'tripione:corporatePortal:user:processingUsers',
            ],
          },
        ],
      },
      {
        path: ROUTES.generalSetting.policy.result.value,
        directPath: ROUTES.generalSetting.policy.result.gen(0),
        name: 'policy',
        component: Policy,
        listRole: [
          'tripione:corporatePortal:group:budgetPolicy:filter',
          'tripione:corporatePortal:individual:budgetPolicy:filter',
        ],
        hiddenMenu: [
          {
            path: ROUTES.generalSetting.policy.createGroup,
            name: 'policy.createGroup',
            component: CreateUpdateGroupPolicy,
            listRole: ['tripione:corporatePortal:group:budgetPolicy:create'],
          },
          {
            path: ROUTES.generalSetting.policy.updateGroup,
            name: 'policy.updateGroup',
            component: CreateUpdateGroupPolicy,
            listRole: [
              'tripione:corporatePortal:group:budgetPolicy:update',
              // 'tripione:corporatePortal:group:budgetPolicy:delete',
            ],
          },
          {
            path: ROUTES.generalSetting.policy.createPerson,
            name: 'policy.createPerson',
            component: CreateUpdatePersonPolicy,
            listRole: ['tripione:corporatePortal:individual:budgetPolicy:create'],
          },
          {
            path: ROUTES.generalSetting.policy.updatePerson,
            name: 'policy.updatePerson',
            component: CreateUpdatePersonPolicy,
            listRole: ['tripione:corporatePortal:individual:budgetPolicy:update'],
          },
        ],
      },
      {
        path: ROUTES.generalSetting.approval.result,
        name: 'approval',
        component: Approval,
        listRole: [
          'tripione:corporatePortal:approvalPolicy:filter',
          'tripione:corporatePortal:approvalPolicy:bypass:filter',
        ],
        // listRole: [
        //   'tripione:corporatePortal:approvalPolicy:filter',
        //   'tripione:corporatePortal:approvalPolicy:create',
        //   'tripione:corporatePortal:approvalPolicy:update',
        //   'tripione:corporatePortal:approvalPolicy:delete',
        //   'tripione:corporatePortal:approvalPolicy:bypass:filter',
        //   'tripione:corporatePortal:approvalPolicy:bypass:update',
        //   'tripione:corporatePortal:approvalPolicy:bypass:delete',
        // ],
        hiddenMenu: [
          {
            path: ROUTES.generalSetting.approval.create,
            name: 'approval.create',
            component: CreateApproval,
            listRole: ['tripione:corporatePortal:approvalPolicy:create'],
          },
          {
            path: ROUTES.generalSetting.approval.update,
            name: 'approval.update',
            component: CreateApproval,
            listRole: ['tripione:corporatePortal:approvalPolicy:update'],
          },
        ],
      },
      {
        path: ROUTES.generalSetting.roleManagement,
        name: 'roleDetail',
        component: RoleManagement,
        listRole: ['tripione:corporatePortal:roles:view'],
      },
      {
        path: ROUTES.generalSetting.paymentMethod,
        name: 'paymentMethod',
        component: PaymentMethod,
        listRole: [
          'tripione:corporatePortal:payment:paymentMethods:filter',
          'tripione:corporatePortal:payment:paymentMethods:update',
        ],
      },
    ],
  },
  {
    path: ROUTES.invoices,
    name: 'invoices',
    component: Invoices,
    isModule: true,
  },
  // {
  //   path: ROUTES.transactionSummary,
  //   name: 'managerReport.transactionSummary',
  //   component: TransactionSummaryReport,
  // },
  {
    name: 'transactions',
    isModule: true,
    path: ROUTES.transactions.history,
    component: TransactionsHistory,
    hidden: true, // temporarily hide
    hiddenMenu: [
      {
        path: ROUTES.transactions.topup,
        name: 'recharge',
        component: TransactionsTopup,
        hiddenMenu: [
          {
            path: ROUTES.transactions.topupInstruction,
            name: 'recharge',
            component: TransactionsTopupInstruction,
          },
        ],
      },
      {
        path: ROUTES.transactions.withdraw,
        name: 'transactions.withdraw',
        component: TransactionsWithdraw,
      },
    ],
  },
  {
    path: ROUTES.adminApprovingCorporateAccount.result,
    name: 'admin.approval',
    component: AdminApprovingCorporateAccount,
    isModule: true,
    listRole: ['tripione:admin:companyApprovals:filter'],
    hiddenMenu: [
      {
        path: ROUTES.adminApprovingCorporateAccount.approval,
        name: 'admin.corporate.approvalDetail',
        component: AdminApprovalAccount,
        listRole: [
          'tripione:admin:companyApprovals:filter',
          'tripione:admin:companyApprovals:accept',
          'tripione:admin:companyApprovals:reject',
          'tripione:admin:corporate:logs:filter',
        ],
      },
    ],
  },
  {
    name: 'admin.corporate',
    isModule: true,
    path: ROUTES.adminCorporate.result,
    component: AdminCorporateResult,
    listRole: ['tripione:admin:corporate:filter'],
    hiddenMenu: [
      {
        path: ROUTES.adminCorporate.detail.value,
        name: 'admin.corporate.detail',
        component: AdminCorporateDetail,
        listRole: [
          'tripione:admin:corporate:update',
          'tripione:admin:corporate:active',
          'tripione:admin:corporate:inactive',
          'tripione:admin:corporate:changeLicenseAccount',
          'tripione:admin:corporate:get',
          'tripione:admin:corporate:users:filter',
          'tripione:admin:corporate:logs:filter',
        ],
      },
    ],
  },
  {
    path: ROUTES.report.result,
    directPath: ROUTES.report.trip,
    name: 'report',
    component: Report,
    isModule: true,
    exact: true,
  },
  {
    name: 'tripManagement',
    isModule: true,
    // path: ROUTES.tripManagement.result,
    path: '/',
    component: TripManagementResult,
    listRole: [
      'tripione:trip:filter',
      'tripione:trip:create',
      // 'tripione:trip:book',
      // 'tripione:trip:approval:request',
      // 'tripione:trip:approval:approve',
      // 'tripione:trip:approval:reject',
      // 'tripione:trip:approval:cancel',
      // 'tripione:trip:cancellation',
    ],
    hiddenMenu: [
      {
        path: ROUTES.tripManagement.detail.value,
        name: 'tripManagement.detail',
        component: TripManagementDetail,
        listRole: ['tripione:trip:detail'],
      },
    ],
  },
];

export const ROUTES_BOOKING: RoutesTabType[] = [
  {
    name: 'notFound404',
    isModule: true,
    path: ROUTES.booking.notFound404,
    component: NotFoundBox,
    disableBreadcrumb: true,
    hidden: true,
  },
  {
    path: ROUTES.tripManagement.detail.value,
    name: 'tripManagement.detail',
    listRole: ['tripione:trip:detail'],
    hiddenMenu: [
      {
        path: ROUTES.booking.pay,
        name: 'tripManagement.pay',
        component: TripPay,
        listRole: ['tripione:trip:pay'],
      },
    ],
  },

  {
    name: 'flight.home',
    isModule: true,
    path: ROUTES.booking.flight.default,
    component: Flight,
    disableBreadcrumb: true,
    exact: true,
  },
  {
    name: 'flight.flightResult',
    isModule: true,
    path: ROUTES.booking.flight.result,
    component: FlightResult,
    disableBreadcrumb: true,
    exact: true,
    hiddenMenu: [
      {
        name: 'flight.flightBookingInfo',
        path: ROUTES.booking.flight.bookingInfo,
        component: FlightBookingInfo,
        exact: true,
        hiddenMenu: [
          {
            name: 'flight.flightReview',
            path: ROUTES.booking.flight.review,
            component: FlightReview,
            exact: true,
          },
        ],
      },
    ],
  },
  {
    name: 'hotel.home',
    isModule: true,
    path: ROUTES.booking.hotel.default,
    component: Hotel,
    // disableInBreadcrumb: true,
    disableBreadcrumb: true,
    hiddenMenu: [
      {
        name: 'hotel.result',
        path: ROUTES.booking.hotel.result,
        component: HotelResult,
        disableBreadcrumb: true,
        hiddenMenu: [
          {
            name: 'hotel.detail',
            path: ROUTES.booking.hotel.detail,
            component: HotelDetail,
            noStickyHeader: true,
            hiddenMenu: [
              {
                name: 'hotel.bookingInfo',
                path: ROUTES.booking.hotel.bookingInfo,
                component: HotelBookingInfo,
              },
            ],
          },
        ],
      },
    ],
  },
];
