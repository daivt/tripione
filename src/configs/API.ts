import { EVN } from '../constants';

enum APIServices {
  one,
  payment,
  tripi,
  gate,
}

function getBaseUrl(service: APIServices) {
  if (service === APIServices.one) {
    return EVN === 'dev'
      ? '/api/one/'
      : EVN === 'stage'
      ? 'https://gate.stage.tripi.vn/tripione'
      : 'https://gate.dev.tripi.vn/tripione';
  }
  if (service === APIServices.payment) {
    return EVN === 'dev'
      ? '/api/payment/'
      : EVN === 'stage'
      ? 'https://gate.stage.tripi.vn/payment'
      : 'https://gate.dev.tripi.vn/payment';
  }
  if (service === APIServices.gate) {
    return EVN === 'dev'
      ? '/api/gate/'
      : EVN === 'stage'
      ? 'https://gate.stage.tripi.vn'
      : 'https://gate.dev.tripi.vn';
  }

  return null;
}

export const API_PATHS = {
  // Firebase
  registerFireBase: `${getBaseUrl(APIServices.gate)}/msgs/notify/token/register`,

  // Notifications
  getNotificationList: (page: number) => `${getBaseUrl(APIServices.gate)}/msgs/notify/list-notify?page=${page}&pageSize=10`,
  unreadCount: `${getBaseUrl(APIServices.gate)}/msgs/notify/count-unread`,
  markRead: `${getBaseUrl(APIServices.gate)}/msgs/notify/mark-read`,
  markAllRead: `${getBaseUrl(APIServices.gate)}/msgs/notify/mark-all-read`,
  resetUnread: `${getBaseUrl(APIServices.gate)}/msgs/notify/reset-count`,

  // Account
  register: `${getBaseUrl(APIServices.one)}/public/accounts/register`,
  verify: `${getBaseUrl(APIServices.one)}/public/accounts/verify`,
  forgotPassword: (email: string) =>
    `${getBaseUrl(APIServices.one)}/public/accounts/passwords/forgot?email=${email}`,
  resetPassword: `${getBaseUrl(APIServices.one)}/public/accounts/passwords/reset`,
  login: `${getBaseUrl(APIServices.one)}/public/accounts/login`,
  firstLogin: `${getBaseUrl(APIServices.one)}/public/accounts/passwords/first-login`,
  accountBrief: `${getBaseUrl(APIServices.one)}/accounts/brief`,

  // Corporate
  getLicenseApproval: `${getBaseUrl(APIServices.one)}/admins/company-approvals/filter`,
  licenseAccept: `${getBaseUrl(APIServices.one)}/admins/company-approvals/accept`,
  licenseReject: `${getBaseUrl(APIServices.one)}/admins/company-approvals/reject`,
  getApprovalHistory: `${getBaseUrl(APIServices.one)}/admins/corporates/logs/filter`,
  corporateList: `${getBaseUrl(APIServices.one)}/admins/corporates/filter`,
  corporateDetail: (companyId: number) =>
    `${getBaseUrl(APIServices.one)}/admins/corporates?id=${companyId}`,
  updateCorporate: `${getBaseUrl(APIServices.one)}/admins/corporates`,
  getCorporateUsers: `${getBaseUrl(APIServices.one)}/admins/corporates/users`,
  activateCorporate: `${getBaseUrl(APIServices.one)}/admins/corporates/active`,
  deactivateCorporate: `${getBaseUrl(APIServices.one)}/admins/corporates/inactive`,
  updateLicenseCorporateAccount: `${getBaseUrl(
    APIServices.one,
  )}/admins/corporates/license-accounts`,

  // Company
  companyGeneral: `${getBaseUrl(APIServices.one)}/companies/settings/generals`,
  companyJobTitles: `${getBaseUrl(APIServices.one)}/companies/settings/job-titles/filter`,
  companyJobTitlesOptions: `${getBaseUrl(APIServices.one)}/companies/settings/job-titles/options`,
  updateCompanyJobTitles: `${getBaseUrl(APIServices.one)}/companies/settings/job-titles`,
  companyDepartments: `${getBaseUrl(APIServices.one)}/companies/settings/departments/filter`,
  updateCompanyDepartments: `${getBaseUrl(APIServices.one)}/companies/settings/departments`,
  companyDepartmentsOptions: `${getBaseUrl(
    APIServices.one,
  )}/companies/settings/departments/options`,
  getListUser: `${getBaseUrl(APIServices.one)}/companies/settings/users/filter`,
  approvalFlows: `${getBaseUrl(APIServices.one)}/companies/settings/approval-flows/filter`,
  updateApprovalFlows: `${getBaseUrl(APIServices.one)}/companies/settings/approval-flows`,
  bypassApprovalAccounts: `${getBaseUrl(
    APIServices.one,
  )}/companies/settings/bypass-approval-accounts/filter`,
  updateBypassApprovalAccounts: `${getBaseUrl(
    APIServices.one,
  )}/companies/settings/bypass-approval-accounts`,
  quickSetup: `${getBaseUrl(APIServices.one)}/companies/settings/quick-setup`,

  // Users
  usersList: `${getBaseUrl(APIServices.one)}/companies/settings/users/filter`,
  updateUsers: `${getBaseUrl(APIServices.one)}/companies/settings/users`,
  uploadUsers: `${getBaseUrl(APIServices.one)}/users/upload`,
  processUsers: `${getBaseUrl(APIServices.one)}/users/processing-users`,
  saveUsers: `${getBaseUrl(APIServices.one)}/users/save-users`,

  // Trips
  payTrip: `${getBaseUrl(APIServices.payment)}/payments`,
  payStatus: `${getBaseUrl(APIServices.payment)}/payments/status`,
  getPaymentMethods: `${getBaseUrl(APIServices.payment)}/payment-methods`,
  createTrip: `${getBaseUrl(APIServices.one)}/trips`,
  getBooking: `${getBaseUrl(APIServices.one)}/trips/booking`,
  tripsGeneral: `${getBaseUrl(APIServices.one)}/trips/filter`,
  approvals: `${getBaseUrl(APIServices.one)}/trips/approvals`,
  approved: `${getBaseUrl(APIServices.one)}/trips/approvals/approved`,
  cancelled: `${getBaseUrl(APIServices.one)}/trips/approvals/cancelled`,
  rejected: `${getBaseUrl(APIServices.one)}/trips/approvals/rejected`,
  cancellation: `${getBaseUrl(APIServices.one)}/trips/cancellation`,
  tripsDetail: (id: number) => `${getBaseUrl(APIServices.one)}/trips/details?id=${id}`,

  // Policies
  policiesGroup: `${getBaseUrl(APIServices.one)}/companies/settings/group-policies/filter`,
  updateGroupPolicies: `${getBaseUrl(APIServices.one)}/companies/settings/group-policies`,

  policiesIndividual: `${getBaseUrl(
    APIServices.one,
  )}/companies/settings/individual-policies/filter`,
  updateIndividualPolicies: `${getBaseUrl(APIServices.one)}/companies/settings/individual-policies`,

  // Invoices
  invoicesList: `${getBaseUrl(APIServices.one)}/invoices/filter`,
  invoicesRequests: `${getBaseUrl(APIServices.one)}/invoices/requests`,
  invoicesRequestsCancel: `${getBaseUrl(APIServices.one)}/invoices/requests/cancel`,
  invoicesTrip: `${getBaseUrl(APIServices.one)}/invoices/trip`,

  // Reports
  tripReport: `${getBaseUrl(APIServices.one)}/admins/reports/filter`,
  tripReportDownload: `${getBaseUrl(APIServices.one)}/admins/reports/download`,
  flightReport: `${getBaseUrl(APIServices.one)}/admins/reports/filter/flights`,
  flightReportDownload: `${getBaseUrl(APIServices.one)}/admins/reports/download/flights`,
  hotelReport: `${getBaseUrl(APIServices.one)}/admins/reports/filter/hotels`,
  hotelReportDownload: `${getBaseUrl(APIServices.one)}/admins/reports/download/hotels`,

  /*-----------------------------------------------*/

  searchAirports: (term: string) =>
    `${getBaseUrl(APIServices.one)}/flights/search/airport?num_items=10&term=${encodeURIComponent(
      term,
    )}`,
  uploadImage: `${getBaseUrl(APIServices.one)}/uploadImage`,
  // Flight
  getListLocationsInformation: `${getBaseUrl(APIServices.one)}/flights/locations-information`,
  searchOneWayTickets: `${getBaseUrl(APIServices.one)}/flights/search`,
  searchTwoWayTickets: `${getBaseUrl(APIServices.one)}/flights/search-round-trip`,
  flightGeneralInfo: `${getBaseUrl(APIServices.one)}/flights/general-information`,
  getTicketDetail: `${getBaseUrl(APIServices.one)}/flights/ticket-detail`,
  getInsurancePackage: `${getBaseUrl(APIServices.one)}/flights/insurance-package`,
  validateInsuranceInfo: `${getBaseUrl(APIServices.one)}/flights/insurance/validate`,
  getAllCountries: `${getBaseUrl(APIServices.one)}/flights/countries`,
  getTopDestinations: `${getBaseUrl(APIServices.one)}/flights/top-destinations`,

  searchHotTickets: `${getBaseUrl(APIServices.one)}/flights/search/hot-ticket`,

  // Hotel
  getTopHotelLocation: (size: number) =>
    `${getBaseUrl(APIServices.one)}/hotels/top-hotel-location?size=${size}`,

  suggestLocation: (term: string) =>
    `${getBaseUrl(APIServices.one)}/hotels/suggest?maxItems=5&term=${encodeURIComponent(term)}`,

  autocompleteLocation: (term: string) =>
    `${getBaseUrl(APIServices.one)}/hotels/autocomplete?maxItems=20&term=${encodeURIComponent(
      term,
    )}`,

  searchHotel: `${getBaseUrl(APIServices.one)}/hotels/search`,
  hotelDetail: `${getBaseUrl(APIServices.one)}/hotels/details`,
  hotelPrice: `${getBaseUrl(APIServices.one)}/hotels/prices`,
  getHotelReviews: `${getBaseUrl(APIServices.one)}/hotels/getReviews`,
  removeFavoriteHotel: `${getBaseUrl(APIServices.one)}/hotels/favorites/remove`,
  addFavoriteHotel: `${getBaseUrl(APIServices.one)}/hotels/favorites/add`,

  bookHotel: `${getBaseUrl(APIServices.one)}/hotels/bookHotelRoom`,
  getHotelPaymentMethods: `${getBaseUrl(APIServices.one)}/checkout/getPaymentMethods`,

  // Payment

  validateCreditPassword: `${getBaseUrl(APIServices.one)}/validateCreditPassword`,
};
