import * as firebase from 'firebase/app';
import '@firebase/messaging';
// import '@firebase/analytics';

const firebaseConfig = {
  apiKey: 'AIzaSyA5d5jzlRtRwsb5i_awx67qb85DJ7lvFsY',
  authDomain: 'tripi-fcm-test.firebaseapp.com',
  databaseURL: 'https://tripi-fcm-test.firebaseio.com',
  projectId: 'tripi-fcm-test',
  storageBucket: 'tripi-fcm-test.appspot.com',
  messagingSenderId: '284571539832',
  appId: '1:284571539832:web:ef5646a59279ecda16508d',
  measurementId: 'G-MT01EGNXSW',
};

firebase.initializeApp(firebaseConfig);
// firebase.analytics();
// messaging.usePublicVapidKey(
//   // Project Settings => Cloud Messaging => Web Push certificates
//   'BD6n7ebJqtOxaBS8M7xtBwSxgeZwX1gdS...6HkTM-cpLm8007IAzz...QoIajea2WnP8rP-ytiqlsj4AcNNeQcbes',
// );
export default firebase;
