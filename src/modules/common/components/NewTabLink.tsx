import React from 'react';
import styled from 'styled-components';
import { BLUE } from '../../../configs/colors';

const Link = styled.a`
  text-decoration: none;
  color: ${BLUE};
`;

interface Props {
  href: string;
  style?: React.CSSProperties;
}

export const NewTabLink: React.FC<Props> = props => {
  const { href, children, style } = props;
  return (
    <Link href={href} rel="noopener noreferrer" target="_blank" style={style}>
      {children}
    </Link>
  );
};
