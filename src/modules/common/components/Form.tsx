import { createStyles, fade, InputBase, makeStyles, Theme, withStyles } from '@material-ui/core';
import React from 'react';
import NumberFormat, { NumberFormatProps } from 'react-number-format';
import { BLACK, GREY_100, GREY_400, GREY_500, RED } from '../../../configs/colors';

export const useStylesForm = makeStyles((theme: Theme) =>
  createStyles({
    select: {
      minWidth: '200px',
      maxWidth: '200px',
      '&:focus': {
        borderRadius: '4px',
      },
      '&:invalid': {
        color: fade(BLACK, 0.4),
      },
      '& option': {
        color: theme.palette.text.primary,
      },
    },
    bootstrap: {
      margin: 0,
      borderRadius: 4,
      backgroundColor: theme.palette.common.white,
      '& .MuiInputBase-input': {
        fontSize: '14px',
      },
      '& .MuiOutlinedInput-root': {
        minHeight: 40,
        '& fieldset': {
          border: `1px solid ${GREY_400}`,
        },
        '&:hover fieldset': {
          borderColor: theme.palette.primary.main,
        },
        '&.Mui-focused fieldset': {
          borderWidth: '1px',
          transition: theme.transitions.create(['border-color', 'box-shadow']),
          // boxShadow: `${fade(theme.palette.primary.main, 0.25)} 0 0 0 2px`,
          borderColor: theme.palette.primary.main,
        },
        '&.Mui-disabled': {
          background: GREY_100,
          color: GREY_500,
        },
      },
    },
  }),
);

export const redMark = <span style={{ color: RED }}>*</span>;

export const BootstrapInput = withStyles((theme: Theme) =>
  createStyles({
    root: {
      minHeight: 40,
      padding: 0,
      border: `1px solid ${GREY_400}`,
      borderRadius: 4,
      position: 'relative',
      backgroundColor: theme.palette.common.white,
      transition: theme.transitions.create(['border-color', 'box-shadow']),
      overflow: 'hidden',
      '&:hover': {
        borderColor: theme.palette.primary.main,
      },
    },
    input: {
      borderRadius: 4,
      position: 'relative',
      fontSize: theme.typography.body2.fontSize,
      padding: '8px',
    },
    focused: {
      // boxShadow: `${fade(theme.palette.primary.main, 0.25)} 0 0 0 0.2rem`,
      borderColor: theme.palette.primary.main,
    },
    error: {
      // boxShadow: `${fade(theme.palette.primary.main, 0.25)} 0 0 0 0.2rem`,
      borderColor: theme.palette.error.main,
    },
    disabled: {
      backgroundColor: GREY_100,
      color: GREY_500,
    },
  }),
)(InputBase);

interface NumberFormatCustomProps extends NumberFormatProps {
  inputRef: (instance: NumberFormat | null) => void;
  onChange: (event: { target: { name: string; value: string } }) => void;
  name: string;

}

export function NumberFormatCustom(props: NumberFormatCustomProps) {
  const { inputRef, onChange, ...other } = props;

  return (
    <NumberFormat
      {...other}
      allowNegative={false}
      getInputRef={inputRef}
      onValueChange={values => {
        onChange({
          target: {
            name: props.name,
            value: values.value,
          },
        });
      }}
      thousandSeparator="."
      decimalSeparator=","
    />
  );
}
export function NumberFormatCustom2(props: NumberFormatCustomProps) {
  const { inputRef, onChange, ...other } = props;

  return (
    <NumberFormat
      {...other}
      allowLeadingZeros
      allowNegative={false}
      getInputRef={inputRef}
      onValueChange={values => {
        onChange({
          target: {
            name: props.name,
            value: values.value,
          },
        });
      }}
    />
  );
}
