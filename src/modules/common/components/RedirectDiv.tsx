import { useSnackbar } from 'notistack';
import React from 'react';
import { useDispatch } from 'react-redux';
import { Action } from 'redux';
import { ThunkDispatch } from 'redux-thunk';
import { AppState } from '../../../redux/reducers';
import { goBackAction } from '../redux/reducer';
import { snackbarSetting } from './elements';

interface Props {
  message?: string;
}

const RedirectDiv: React.FC<Props> = props => {
  const { message } = props;
  const dispatch = useDispatch<ThunkDispatch<AppState, null, Action<string>>>();
  const { enqueueSnackbar, closeSnackbar } = useSnackbar();

  React.useEffect(() => {
    dispatch(goBackAction());
  }, [dispatch]);

  React.useEffect(() => {
    message &&
      enqueueSnackbar(
        message,
        snackbarSetting(key => closeSnackbar(key), { color: 'error' }),
      );
  }, [closeSnackbar, enqueueSnackbar, message]);
  return null;
};
export default RedirectDiv;
