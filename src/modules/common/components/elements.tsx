import { ButtonBase, IconButton, Theme, Tooltip, Typography, withStyles } from '@material-ui/core';
import { Alert, AlertTitle } from '@material-ui/lab';
import { AlertProps } from '@material-ui/lab/Alert';
import { OptionsObject, SnackbarMessage } from 'notistack';
import React from 'react';
import MaskedInput from 'react-text-mask';
import styled from 'styled-components';
import { GREY_100, GREY_500, PRIMARY, PURPLE, PURPLE_50 } from '../../../configs/colors';
import { HEADER_HEIGHT } from '../../../layout/constants';

export const PageWrapper = styled.div`
  min-height: ${window.innerHeight - HEADER_HEIGHT}px;
  position: relative;
  display: flex;
  flex-direction: column;
`;

export const PageContainer = styled.div`
  min-height: 100vh;
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
  background: ${GREY_100};
`;

export const Wrapper = styled.div`
  border-radius: 4px;
  position: absolute;
  top: 0;
  left: 0;
  transition: all 300ms;
  min-width: 100%;
  overflow: hidden;
`;

export const Line = styled.div`
  display: flex;
  justify-content: flex-start;
  align-items: center;
  min-height: 32px;
`;
export const LineBT = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
  min-height: 32px;
`;

export const Line36 = styled.div`
  display: flex;
  justify-content: flex-start;
  align-items: center;
  min-height: 36px;
`;
export const LineBT36 = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
  min-height: 36px;
`;
export const Line40 = styled.div`
  display: flex;
  justify-content: flex-start;
  align-items: center;
  min-height: 40px;
`;
export const LineBT40 = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
  min-height: 40px;
`;

export const Row = styled.div`
  display: flex;
  align-items: center;
`;

export const Col = styled.div`
  display: flex;
  flex-direction: column;
`;

export function snackbarSetting(
  closeSnackbar: (key: string) => void,
  alertProps?: AlertProps,
  alertTitle?: React.ReactNode,
) {
  return {
    anchorOrigin: {
      vertical: 'top',
      horizontal: 'right',
    },
    preventDuplicate: true,
    autoHideDuration: 3000,
    // persist: true,
    content: (key: string, msg: SnackbarMessage) => (
      <Alert
        style={{ minWidth: '240px' }}
        onClose={() => closeSnackbar(key)}
        severity={alertProps?.color}
        {...alertProps}
      >
        {alertTitle && <AlertTitle>{alertTitle}</AlertTitle>}
        <Typography variant="body2" color="inherit">
          {msg}
        </Typography>
      </Alert>
    ),
  } as OptionsObject;
}

export const IconButtonStyled = withStyles({
  root: {
    stroke: GREY_500,
    '&:hover': {
      stroke: PRIMARY,
    },
  },
})(IconButton);

export const LightTooltip = withStyles((theme: Theme) => ({
  tooltip: {
    backgroundColor: theme.palette.common.white,
    border: `0.5px solid ${GREY_500}`,
    color: theme.palette.text.primary,
    fontSize: theme.typography.caption.fontSize,
    borderRadius: 0,
    boxSizing: 'border-box',
    fontWeight: 'normal',
  },
}))(Tooltip);

export const ChipButton = withStyles(theme => ({
  root: {
    marginLeft: 16,
    padding: '4px 12px',
    borderRadius: '99px',
    color: PURPLE,
    background: PURPLE_50,
    fontSize: 12,
  },
}))(ButtonBase);

interface DateMaskCustomProps {
  inputRef: (ref: HTMLInputElement | null) => void;
  placeholder: string;
}

export const DateMaskCustomRange = (props: DateMaskCustomProps) => {
  const { inputRef, placeholder, ...other } = props;

  return (
    <MaskedInput
      {...other}
      ref={(ref: any) => {
        inputRef(ref ? ref.inputElement : null);
      }}
      mask={[
        /\d/,
        /\d/,
        '/',
        /\d/,
        /\d/,
        '/',
        /\d/,
        /\d/,
        /\d/,
        /\d/,
        ' ',
        '-',
        ' ',
        /\d/,
        /\d/,
        '/',
        /\d/,
        /\d/,
        '/',
        /\d/,
        /\d/,
        /\d/,
        /\d/,
      ]}
      placeholder={placeholder}
      guide={false}
      // placeholderChar="\u2000"
      keepCharPositions
    />
  );
};

export const DateMaskCustomSingle = (props: DateMaskCustomProps) => {
  const { inputRef, placeholder, ...other } = props;

  return (
    <MaskedInput
      {...other}
      ref={(ref: any) => {
        inputRef(ref ? ref.inputElement : null);
      }}
      mask={[/\d/, /\d/, '/', /\d/, /\d/, '/', /\d/, /\d/, /\d/, /\d/]}
      placeholder={placeholder}
      guide={false}
    />
  );
};
export const RenderTag = (value: any[], e: any, label?: string) => {
  return (
    <Tooltip title={value.map(v => (label ? v[label] : v.name)).join(',\r\n')}>
      <Typography
        variant="body2"
        style={{
          textOverflow: 'ellipsis',
          overflow: 'hidden',
          whiteSpace: 'nowrap',
          maxWidth: 200,
          paddingLeft: 8,
        }}
      >
        {value.map(v => (label ? v[label] : v.name)).join(', ')}
      </Typography>
    </Tooltip>
  );
};
