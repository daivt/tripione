import { Typography } from '@material-ui/core';
import * as React from 'react';
import { FormattedMessage } from 'react-intl';
import NoDataIcon from '../../../svg/ic_noContent.svg';
import { Col } from './elements';

interface Props {
  message?: React.ReactNode;
  content?: React.ReactNode;
  style?: React.CSSProperties;
}

const NoDataBox: React.FunctionComponent<Props> = props => {
  const { message, content, style } = props;
  return (
    <Col
      style={{
        alignItems: 'center',
        flex: 1,
        height: '100%',
        justifyContent: 'center',
        minHeight: 320,
        ...style,
      }}
    >
      <img src={NoDataIcon} alt="" />
      <Typography variant="body1" color="textSecondary" style={{ marginTop: 24 }}>
        {message || <FormattedMessage id="noData" />}
      </Typography>
      {content}
    </Col>
  );
};

export default NoDataBox;
