import { Tooltip } from '@material-ui/core';
import React from 'react';
import { connect } from 'react-redux';
import { Dispatch } from 'redux';
import { getCurrentRole } from '../../../layout/utils';
import { AppState } from '../../../redux/reducers';

const mapStateToProps = (state: AppState) => {
  return {
    userData: state.account.userData,
  };
};

interface Props
  extends React.DetailedHTMLProps<React.HTMLAttributes<HTMLDivElement>, HTMLDivElement>,
    ReturnType<typeof mapStateToProps> {
  dispatch: Dispatch;
  permission?: string[] | string;
  tooltip?: React.ReactNode;
  placement?:
    | 'bottom-end'
    | 'bottom-start'
    | 'bottom'
    | 'left-end'
    | 'left-start'
    | 'left'
    | 'right-end'
    | 'right-start'
    | 'right'
    | 'top-end'
    | 'top-start'
    | 'top';
}

const PermissionDiv = React.forwardRef<HTMLDivElement, Props>((props, ref) => {
  const { children, userData, dispatch, permission, tooltip, placement, ...rest } = props;
  const isSufficientRole = React.useMemo(() => {
    const check = getCurrentRole(userData?.roleInfo?.permissions, permission);
    return check;
  }, [permission, userData]);

  if (!isSufficientRole) {
    return null;
  }
  return (
    <Tooltip title={tooltip || ''} placement={placement || 'bottom-end'}>
      <div ref={ref} {...rest}>
        {children}
      </div>
    </Tooltip>
  );
});

export default connect(mapStateToProps)(PermissionDiv);
