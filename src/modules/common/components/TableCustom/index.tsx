/* eslint-disable no-nested-ternary */
import {
  fade,
  makeStyles,
  Paper,
  PaperProps,
  Table,
  TableBody,
  TableCell as TableCellRaw,
  TableContainer,
  TableHead,
  TablePagination,
  TablePaginationProps,
  TableRow as TableRowRaw,
  Theme,
  Typography,
  withStyles,
} from '@material-ui/core';
import { Variant } from '@material-ui/core/styles/createTypography';
import { ClassNameMap } from '@material-ui/core/styles/withStyles';
import React from 'react';
import { FormattedMessage, useIntl } from 'react-intl';
import { BLUE_200, GREEN_50, GREY_100, WHITE } from '../../../../configs/colors';
import { some } from '../../../../constants';
import { ReactComponent as IconNoData } from '../../../../svg/ic_noContent.svg';
import { Col, Row } from '../elements';
import LoadingIcon from '../LoadingIcon';
import TablePaginationActionsCustom from './TablePaginationActionsCustom';

export const TableContainerCS = withStyles(() => ({
  root: {},
}))(TableContainer);

export const TableRow = withStyles(() => ({
  root: {
    '&:hover > td': { background: GREEN_50 },
  },
}))(TableRowRaw);

export const TableCell = withStyles(() => ({
  root: {
    padding: '12px 8px',
    background: WHITE,
    borderBottom: `1px solid ${GREY_100}`,
    // '&:hover': { background: PURPLE_50 },
  },
}))(TableCellRaw);

const useStylePagination = makeStyles((theme: Theme) => ({
  root: { justifyContent: 'flex-end' },
  selectRoot: {
    margin: '0 16px 0 8px',
    minWidth: '64px',
  },
  selectIcon: {
    top: 'calc(50% - 14px)',
  },
  caption: { fontSize: 12 },
  menuItem: { fontSize: 12 },
  input: {
    '& .MuiTablePagination-select': {
      textAlign: 'left',
      textAlignLast: 'left',
      background: 'white',
      border: `0.5px solid ${GREY_100}`,
      borderRadius: '2px',
      fontSize: theme.typography.body2.fontSize,
      padding: '3px 12px',
    },
  },
  actions: {
    marginLeft: '10px',
    '& .MuiIconButton-root': {
      padding: '6px',
    },
  },
  even: {
    background: 'white',
  },
  odd: {
    background: BLUE_200,
  },
}));

export interface Columns {
  title?: string;
  dataIndex?: string;
  key?: string;
  style?: React.CSSProperties;
  styleHeader?: React.CSSProperties;
  render?: (col: some, index: number) => JSX.Element;
  fixed?: 'right' | 'left';
  width?: number;
  hidden?: boolean;
  disableAction?: boolean;
  variant?: Variant | 'inherit';
  lastCell?: { rowSpan?: number; colSpan?: number; render?: () => JSX.Element };
}
interface Props<T> {
  className?: ClassNameMap;
  style?: React.CSSProperties;
  styleTable?: React.CSSProperties;
  styleTableContainer?: React.CSSProperties;
  dataSource?: T[];
  columns: Columns[];
  paginationProps?: TablePaginationProps;
  loading?: boolean;
  caption?: React.ReactNode;
  header?: React.ReactNode;
  noColumnIndex?: boolean;
  stickyHeader?: boolean;
  fixIndexColumn?: boolean;
  onRowClick?: (col: T, index: number) => void;
  paperProps?: PaperProps;
  hiddenHeader?: boolean;
}

export const TableCustom: <T extends some | null>(
  prop: Props<T>,
) => React.ReactElement<Props<T>> = props => {
  const {
    className,
    dataSource,
    columns,
    style,
    styleTable,
    styleTableContainer,
    paginationProps,
    loading,
    caption,
    noColumnIndex,
    stickyHeader,
    onRowClick,
    fixIndexColumn,
    header,
    paperProps,
    hiddenHeader,
  } = props;
  const [scrollLeft, setScrollLeft] = React.useState(false);
  const [scrollRight, setScrollRight] = React.useState(false);
  const classesPagination = useStylePagination(props);
  const container = React.useRef<HTMLDivElement>(null);
  const intl = useIntl();

  const getRowIndex = React.useCallback(
    (i: number) => {
      let index = i;
      if (paginationProps) {
        index += (paginationProps.page - 1) * paginationProps.rowsPerPage;
      }
      return index;
    },
    [paginationProps],
  );

  const getColumn = React.useMemo(() => {
    return columns
      ? !noColumnIndex
        ? [
            {
              title: 'stt',
              dataIndex: 'index',
              fixed: fixIndexColumn ? 'left' : undefined,
              width: 70,
              styleHeader: { textAlign: 'center' },
              style: { textAlign: 'center' },
            } as Columns,
            ...columns,
          ]
        : columns
      : [];
  }, [columns, fixIndexColumn, noColumnIndex]);

  const getDataSource = React.useMemo(() => {
    const temp = dataSource
      ? dataSource.map((v, index) => {
          return { index: getRowIndex(index + 1), ...v };
        })
      : undefined;
    return temp;
  }, [dataSource, getRowIndex]);

  const getWidth = React.useCallback(
    (col: Columns) => {
      let width = 0;
      if (col.fixed) {
        const columnsTmp = col.fixed === 'left' ? [...getColumn] : [...getColumn].reverse();
        for (let i = 0; i < columnsTmp.length; i += 1) {
          if (col.title === columnsTmp[i].title) {
            break;
          }
          width += columnsTmp[i].width || 0;
        }
      }
      return width;
    },
    [getColumn],
  );

  const getShadow = React.useCallback(
    (col: Columns) => {
      if (col.fixed) {
        let isLast = false;
        const columnsTmp = col.fixed === 'left' ? [...getColumn].reverse() : [...getColumn];
        const lastEle = columnsTmp.find(v => v.fixed === col.fixed);
        if (lastEle?.title === col.title) {
          isLast = true;
        }
        if (isLast) {
          if (col.fixed === 'left' && scrollLeft) {
            return 'inset 10px 0 8px -8px rgba(0, 0, 0, 0.15)';
          }
          if (col.fixed === 'right' && scrollRight) {
            return 'inset -10px 0 8px -8px rgba(0, 0, 0, 0.15)';
          }
        }
      }
      return undefined;
    },
    [getColumn, scrollLeft, scrollRight],
  );

  React.useEffect(() => {
    if (
      container.current?.offsetWidth &&
      container.current?.scrollWidth &&
      container.current.offsetWidth < container.current.scrollWidth
    ) {
      setScrollRight(true);
    }
  }, []);

  return (
    <Paper
      {...paperProps}
      className={className?.paper}
      style={{
        position: 'relative',
        borderRadius: 0,
        ...paperProps?.style,
        ...style,
      }}
    >
      {header}
      <div
        className={className?.cover}
        style={{
          overflow: 'hidden',
          borderTop: header ? `1px solid ${GREY_100}` : undefined,
          padding: '0px 8px',
        }}
      >
        <TableContainerCS
          ref={container}
          style={styleTableContainer}
          onScrollCapture={e => {
            if (e.currentTarget.scrollLeft) {
              setScrollLeft(true);
            } else {
              setScrollLeft(false);
            }
            if (
              e.currentTarget.scrollWidth -
              e.currentTarget.clientWidth -
              e.currentTarget.scrollLeft
            ) {
              setScrollRight(true);
            } else {
              setScrollRight(false);
            }
          }}
        >
          <Table
            stickyHeader={stickyHeader}
            className={`custom-table ${className?.table}`}
            style={{ borderCollapse: 'separate', ...styleTable }}
          >
            {(!getDataSource || (getDataSource && getDataSource.length === 0)) && (
              <caption style={{ background: 'white', minHeight: 240 }}>
                {!loading && (
                  <div
                    style={{
                      display: 'flex',
                      justifyContent: 'center',
                      alignItems: 'center',
                    }}
                  >
                    {caption ? (
                      <>{caption}</>
                    ) : (
                      <Col style={{ alignItems: 'center', margin: '12px' }}>
                        <IconNoData />
                        <Typography variant="body2" color="textSecondary" style={{ marginTop: 24 }}>
                          <FormattedMessage id="noData" />
                        </Typography>
                      </Col>
                    )}
                  </div>
                )}
              </caption>
            )}
            {!hiddenHeader && (
              <TableHead className={className?.header}>
                <TableRow className={`${className?.row} ${className?.rowHeader}`}>
                  {getColumn
                    .filter(v => !v.hidden)
                    .map((col: Columns, index: number) => (
                      <TableCell
                        className={className?.cellHeader}
                        key={index}
                        style={{
                          padding: '8px 0px',
                          ...col.styleHeader,
                          width: col.width,
                          minWidth: col.fixed ? col.width : undefined,
                          position: col.fixed ? 'sticky' : undefined,
                          left: col.fixed === 'left' ? getWidth(col) : undefined,
                          right: col.fixed === 'right' ? getWidth(col) : undefined,
                          borderRight:
                            col.fixed === 'left' && getShadow(col)
                              ? `0.5px solid ${GREY_100}`
                              : undefined,
                          borderLeft:
                            col.fixed === 'right' && getShadow(col)
                              ? `0.5px solid ${GREY_100}`
                              : undefined,
                        }}
                      >
                        {col.fixed && (
                          <div
                            style={{
                              position: 'absolute',
                              top: 0,
                              bottom: -1,
                              width: 6,
                              left: col.fixed === 'right' && getShadow(col) ? -5 : undefined,
                              right: col.fixed === 'left' && getShadow(col) ? -5 : undefined,
                              boxShadow: getShadow(col),
                            }}
                          />
                        )}
                        <div
                          style={{
                            padding: '0px 8px',
                            // borderRight:
                            //   col.fixed === 'left' && getShadow(col)
                            //     ? undefined
                            //     : `0.5px solid ${GREY_100}`,
                          }}
                        >
                          {col.title && (
                            <Typography variant="caption" style={{ whiteSpace: 'nowrap' }}>
                              <FormattedMessage id={col.title} />
                            </Typography>
                          )}
                        </div>
                      </TableCell>
                    ))}
                </TableRow>
              </TableHead>
            )}
            <TableBody className={className?.body}>
              {getDataSource &&
                getDataSource.map((items, index) => {
                  return (
                    <TableRow
                      className={className?.row}
                      key={index}
                      onClick={() => onRowClick && onRowClick(items, index)}
                    >
                      {getColumn
                        .filter(v => !v.hidden)
                        .map((col: Columns, i: number) => {
                          return (
                            <TableCell
                              key={i}
                              className={className?.cell}
                              style={{
                                ...col.style,
                                position: col.fixed ? 'sticky' : undefined,
                                left: col.fixed === 'left' ? getWidth(col) : undefined,
                                right: col.fixed === 'right' ? getWidth(col) : undefined,
                                borderLeft:
                                  col.fixed === 'right' && getShadow(col)
                                    ? `1px solid ${GREY_100}`
                                    : undefined,
                                borderRight:
                                  col.fixed === 'left' && getShadow(col)
                                    ? `1px solid ${GREY_100}`
                                    : undefined,
                                // background:
                                //   col.fixed === 'right' && getShadow(col) ? 'white' : undefined,
                                cursor: onRowClick && !col.disableAction ? 'pointer' : undefined,
                              }}
                              onClick={e => {
                                col.disableAction && e.stopPropagation();
                              }}
                            >
                              {col.fixed && (
                                <div
                                  style={{
                                    position: 'absolute',
                                    top: 0,
                                    bottom: -1,
                                    width: 6,
                                    left: col.fixed === 'right' && getShadow(col) ? -6 : undefined,
                                    right: col.fixed === 'left' && getShadow(col) ? -6 : undefined,
                                    boxShadow: getShadow(col),
                                  }}
                                />
                              )}
                              {col.render ? (
                                <>{col.render(items, index)}</>
                              ) : (
                                <>
                                  <Typography variant={col.variant || 'caption'}>
                                    {items[`${col?.dataIndex}`]}
                                  </Typography>
                                </>
                              )}
                            </TableCell>
                          );
                        })}
                    </TableRow>
                  );
                })}
              <TableRow className={className?.row} key="extendRow">
                {getColumn
                  .filter(v => !v.hidden && v.lastCell)
                  .map((col: Columns, i: number) => (
                    <TableCell
                      className={className?.cell}
                      rowSpan={col.lastCell?.rowSpan}
                      colSpan={col.lastCell?.colSpan}
                      key={i}
                      style={{
                        ...col.style,
                        position: col.fixed ? 'sticky' : undefined,
                        left: col.fixed === 'left' ? getWidth(col) : undefined,
                        right: col.fixed === 'right' ? getWidth(col) : undefined,
                        borderLeft:
                          col.fixed === 'right' && getShadow(col)
                            ? `0.5px solid ${GREY_100}`
                            : undefined,
                        // background:
                        //   col.fixed === 'right' && getShadow(col) ? 'white' : undefined,
                        cursor: onRowClick && !col.disableAction ? 'pointer' : undefined,
                      }}
                      onClick={e => {
                        col.disableAction && e.stopPropagation();
                      }}
                    >
                      {col.fixed && (
                        <div
                          style={{
                            position: 'absolute',
                            top: 0,
                            bottom: -1,
                            width: 6,
                            left: col.fixed === 'right' && getShadow(col) ? -6 : undefined,
                            right: col.fixed === 'left' && getShadow(col) ? -6 : undefined,
                            boxShadow: getShadow(col),
                          }}
                        />
                      )}
                      {col.lastCell && <>{col?.lastCell?.render && col?.lastCell?.render()}</>}
                    </TableCell>
                  ))}
              </TableRow>
            </TableBody>
          </Table>
          {loading && (
            <div
              style={{
                position: 'absolute',
                top: 0,
                bottom: 0,
                right: 0,
                left: 0,
                background: fade(GREY_100, 0.7),
                display: 'flex',
                justifyContent: 'center',
                alignItems: 'center',
              }}
            >
              <LoadingIcon />
            </div>
          )}
        </TableContainerCS>
      </div>
      {paginationProps && (
        <TablePagination
          data-tour="step-5"
          component={Row}
          {...paginationProps}
          count={paginationProps.count}
          page={
            dataSource && dataSource?.length > 0 && paginationProps.page - 1 > 0
              ? paginationProps.page - 1
              : 0
          }
          onChangePage={(e, page) => paginationProps.onChangePage(e, page + 1)}
          classes={{
            root: classesPagination.root,
            menuItem: classesPagination.menuItem,
            selectRoot: classesPagination.selectRoot,
            selectIcon: classesPagination.selectIcon,
            input: classesPagination.input,
            actions: classesPagination.actions,
            caption: classesPagination.caption,
          }}
          labelRowsPerPage={intl.formatMessage({ id: 'labelRowPerPage' })}
          ActionsComponent={TablePaginationActionsCustom}
        />
      )}
    </Paper>
  );
};

export default TableCustom;
