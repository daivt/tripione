import { Button, Dialog, DialogActions, DialogProps, Divider, IconButton } from '@material-ui/core';
import IconClose from '@material-ui/icons/CloseOutlined';
import React from 'react';
import { FormattedMessage } from 'react-intl';
import LoadingButton from './LoadingButton';
import { Row } from './elements';

interface Props extends DialogProps {
  open: boolean;
  loading?: boolean;
  acceptLabel?: string;
  rejectLabel?: string;
  styleCloseBtn?: React.CSSProperties;
  styleHeader?: React.CSSProperties;
  onClose(): void;
  onAccept(): void;
  titleLabel?: React.ReactNode;
  footerLabel?: React.ReactNode;
  onReject?: () => void;
  onExited?: () => void;
}

const ConfirmDialog: React.FC<Props> = props => {
  const {
    open,
    styleCloseBtn,
    styleHeader,
    loading,
    onClose,
    onExited,
    onAccept,
    onReject,
    titleLabel,
    footerLabel,
    acceptLabel,
    rejectLabel,
    children,
    ...rest
  } = props;
  return (
    <Dialog
      open={open}
      onClose={onClose}
      PaperProps={{
        style: {
          minWidth: 420,
        },
      }}
      maxWidth="md"
      onExited={onExited}
      {...rest}
    >
      {titleLabel ? (
        <>
          <Row style={styleHeader}>
            <div style={{ flex: 1 }}>{titleLabel}</div>
            <IconButton
              style={{
                padding: '8px',
                ...styleCloseBtn,
              }}
              onClick={onClose}
            >
              <IconClose />
            </IconButton>
          </Row>
          <Divider />
        </>
      ) : (
        <IconButton
          style={{
            position: 'absolute',
            right: 0,
            padding: '8px',
            ...styleCloseBtn,
          }}
          onClick={onClose}
        >
          <IconClose />
        </IconButton>
      )}
      {children}
      <Divider />
      <DialogActions style={{ padding: 16, justifyContent: 'center' }}>
        <LoadingButton
          loading={loading}
          variant="contained"
          color="secondary"
          size="large"
          style={{ minWidth: 144 }}
          onClick={onAccept}
          disableElevation
        >
          <FormattedMessage id={acceptLabel || 'accept'} />
        </LoadingButton>
        {onReject && (
          <Button
            variant="outlined"
            size="large"
            style={{ minWidth: 144, marginLeft: 24 }}
            onClick={onReject}
            disableElevation
          >
            <FormattedMessage id={rejectLabel || 'reject'} />
          </Button>
        )}
      </DialogActions>
    </Dialog>
  );
};

export default ConfirmDialog;
