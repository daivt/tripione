import { ButtonBase } from '@material-ui/core';
import React, { useState } from 'react';
import { useDropzone } from 'react-dropzone';
import { FormattedMessage, useIntl } from 'react-intl';
import { GREY_600 } from '../../../configs/colors';
import { FILE_ALLOW_TYPE } from '../../../models/uploadFile';
import FormControlTextField from './FormControlTextField';
import InputAdornmentSolid from './InputAdornmentSolid';
import { some } from '../../../constants';

interface Props {
  multiple?: boolean;
  onDrop(file: File[]): void;
  loading?: boolean;
}

const FileUpload: React.FC<Props> = props => {
  const { onDrop, multiple, loading } = props;
  const [files, setFiles] = useState<some[]>([]);
  const intl = useIntl();

  const { getRootProps, getInputProps } = useDropzone({
    noKeyboard: true,
    multiple,
    onDrop: acceptedFiles => {
      multiple ? setFiles(one => [...one, ...acceptedFiles]) : setFiles(acceptedFiles);
      onDrop(acceptedFiles);
    },
    accept: FILE_ALLOW_TYPE,
  });

  return (
    <>
      <input {...getInputProps()} />
      <FormControlTextField
        id="amount"
        formControlStyle={{ width: 340 }}
        placeholder={intl.formatMessage({ id: 'browse' })}
        value={files.map(v => v.path).join(', ')}
        inputProps={{
          maxLength: 100,
        }}
        readOnly
        endAdornment={
          <InputAdornmentSolid style={{ padding: 0 }}>
            <ButtonBase
              style={{ width: 80, padding: '12px 8px', color: GREY_600 }}
              {...getRootProps()}
            >
              <FormattedMessage id="browse" />
            </ButtonBase>
          </InputAdornmentSolid>
        }
      />
    </>
  );
};

FileUpload.defaultProps = { loading: false };

export default FileUpload;
