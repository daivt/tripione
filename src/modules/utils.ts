import { some } from '../constants';
import { validNumberRegex } from '../models/regex';

export const trimObjectValues = (obj: some) => {
  const keys = Object.keys(obj);
  // eslint-disable-next-line no-restricted-syntax
  for (const key of keys) {
    if (typeof obj[key] === 'string') {
      // eslint-disable-next-line no-param-reassign
      obj[key] = obj[key].trim();
    }
  }
  return JSON.parse(JSON.stringify(obj));
};

export function uniqueObject(url: some): some {
  const temp = Object.entries(url)
    .map(([key, value]) => {
      if (typeof value === 'string') {
        if (value.trim() && validNumberRegex.test(value.trim())) {
          return [key, Number(value.trim())];
        }
        if (value === 'true') {
          return [key, true];
        }
        if (value === 'false') {
          return [key, false];
        }
        return [key, value.trim()];
      }
      return [key, value];
    })
    .filter(([key, value]) => {
      if (value === undefined || value === 'NaN' || value === null) {
        return undefined;
      }

      return [key, value];
    })
    .map(([key, value]) => {
      
      if (typeof value === 'object') {
        return [key, uniqueObject(value)];
      }
      return [key, value];
    });

  return Object.fromEntries(temp);
}
