/* eslint-disable camelcase */
/* eslint-disable @typescript-eslint/camelcase */
import gym from '../../../../svg/booking/facilityIcon/gym.svg';
import ic_airports_transfer from '../../../../svg/booking/facilityIcon/ic_airports_transfer.svg';
import ic_baby_sitting from '../../../../svg/booking/facilityIcon/ic_baby_sitting.svg';
import ic_bar from '../../../../svg/booking/facilityIcon/ic_bar.svg';
import ic_concierge from '../../../../svg/booking/facilityIcon/ic_concierge.svg';
import ic_currency_change from '../../../../svg/booking/facilityIcon/ic_currency_change.svg';
import ic_dinner from '../../../../svg/booking/facilityIcon/ic_dinner.svg';
import ic_door_hanger_outline from '../../../../svg/booking/facilityIcon/ic_door_hanger_outline.svg';
import ic_elevator from '../../../../svg/booking/facilityIcon/ic_elevator.svg';
import ic_facilities_disabled_guests from '../../../../svg/booking/facilityIcon/ic_facilities_disabled_guests.svg';
import ic_laundry_service from '../../../../svg/booking/facilityIcon/ic_laundry_service.svg';
import ic_luggage_storage from '../../../../svg/booking/facilityIcon/ic_luggage_storage.svg';
import ic_meeting_facilities from '../../../../svg/booking/facilityIcon/ic_meeting_facilities.svg';
import ic_night_club from '../../../../svg/booking/facilityIcon/ic_night_club.svg';
import ic_parking from '../../../../svg/booking/facilityIcon/ic_parking.svg';
import ic_poolside_bar from '../../../../svg/booking/facilityIcon/ic_poolside_bar.svg';
// facility icon
import ic_room_service24hour from '../../../../svg/booking/facilityIcon/ic_room_service24hour.svg';
import ic_safety_deposit_boxes from '../../../../svg/booking/facilityIcon/ic_safety_deposit_boxes.svg';
import ic_shops from '../../../../svg/booking/facilityIcon/ic_shops.svg';
import ic_shutle_service from '../../../../svg/booking/facilityIcon/ic_shutle_service.svg';
import ic_tours_service from '../../../../svg/booking/facilityIcon/ic_tours_service.svg';
import ic_wifi from '../../../../svg/booking/facilityIcon/ic_wifi.svg';
import sauna from '../../../../svg/booking/facilityIcon/sauna.svg';
import spa from '../../../../svg/booking/facilityIcon/spa.svg';
import swimming_pool from '../../../../svg/booking/facilityIcon/swimming_pool.svg';

export function getInternetIcon(id: number) {
  let icon;
  switch (id) {
    case 0: // Quầy lễ tân 24h
      icon = ic_wifi;
      break;
    case 1: //  Quầy bar
      icon = ic_wifi;
      break;
    default:
  }
  return icon;
}
export function getFacilityIcon(id: number) {
  let icon;
  switch (id) {
    case 0: // Quầy lễ tân 24h
      icon = ic_room_service24hour;
      break;
    case 1: //  Quầy bar
      icon = ic_bar;
      break;
    case 2: // Dịch vụ giặt là
      icon = ic_laundry_service;
      break;
    case 3: // Hộp đêm
      icon = ic_night_club;
      break;
    case 4: // Dịch vụ phòng
      icon = ic_door_hanger_outline;
      break;
    case 5: // Dịch vụ đưa đón
      icon = ic_shutle_service;
      break;
    case 6: //  Bãi đỗ xe ô tô
      icon = ic_parking;
      break;
    case 7: // Thu đổi ngoại tệ
      icon = ic_currency_change;
      break;
    case 8: // Dịch vụ phòng 24h
      icon = ic_room_service24hour;
      break;
    case 9: // Thang máy
      icon = ic_elevator;
      break;
    case 10: // Két sắt
      icon = ic_safety_deposit_boxes;
      break;
    case 11: // Giữ hành lý
      icon = ic_luggage_storage;
      break;
    case 12: // Quầy bar cạnh bể bơi
      icon = ic_poolside_bar;
      break;
    case 13: //  Đưa đón sân bay/khách sạn
      icon = ic_airports_transfer;
      break;
    case 14: // Người vận chuyển hành lý
      icon = ic_concierge;
      break;
    case 15: // Cửa hàng lưu niệm
      icon = ic_shops;
      break;
    case 16: // Dịch vụ du lịch
      icon = ic_tours_service;
      break;
    case 17: // Thiết bị phòng họp
      icon = ic_meeting_facilities;
      break;
    case 18: // Dịch vụ trông trẻ
      icon = ic_baby_sitting;
      break;
    case 19: // Tiện nghi cho người khuyết tật
      icon = ic_facilities_disabled_guests;
      break;
    case 20: // Phòng ăn
      icon = ic_dinner;
      break;
    default:
  }
  return icon;
}
export function getRelaxIcon(id: number) {
  let icon;
  switch (id) {
    case 0: //  Bể bơi ngoài trời
      icon = swimming_pool;
      break;
    case 1: //  Bể bơi (trẻ em)
      icon = swimming_pool;
      break;
    case 2: // Phòng thể dục
      icon = gym;
      break;
    case 3: // Xông hơi
      icon = sauna;
      break;
    case 4: // Spa
      icon = spa;
      break;
    case 5: // Mát xa
      icon = spa;
      break;
    case 6: //  Sauna
      icon = sauna;
      break;
    default:
  }
  return icon;
}
