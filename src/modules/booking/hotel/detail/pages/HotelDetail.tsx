import { Typography } from '@material-ui/core';
import { useSnackbar } from 'notistack';
import React from 'react';
import Helmet from 'react-helmet';
import { FormattedMessage } from 'react-intl';
import { useDispatch } from 'react-redux';
import { useLocation } from 'react-router';
import { Action } from 'redux';
import { ThunkDispatch } from 'redux-thunk';
import { API_PATHS } from '../../../../../configs/API';
import { some, SUCCESS_CODE } from '../../../../../constants';
import { AppState } from '../../../../../redux/reducers';
import DialogCustom from '../../../../common/components/DialogCustom';
import { snackbarSetting } from '../../../../common/components/elements';
import LoadingIcon from '../../../../common/components/LoadingIcon';
import { goBackAction, goToReplace } from '../../../../common/redux/reducer';
import { fetchThunk } from '../../../../common/redux/thunk';
import {
  HotelBookingParams,
  parseHotelBookingParams,
  stringifyHotelBookingParams,
} from '../../utils';
import HotelDetailDesktop from '../components/HotelDetailDesktop';

interface Props {}

const HotelDetail: React.FC<Props> = props => {
  const dispatch = useDispatch<ThunkDispatch<AppState, null, Action<string>>>();
  const [data, setData] = React.useState<some | undefined>(undefined);
  const [room, setRoom] = React.useState<some[]>([]);
  const [search, setSearch] = React.useState<HotelBookingParams | undefined>(undefined);
  const [loading, setLoading] = React.useState(true);
  const [open, setOpen] = React.useState(false);
  const location = useLocation();
  const { enqueueSnackbar, closeSnackbar } = useSnackbar();

  const searchData = React.useCallback(
    async (paramsSearch: HotelBookingParams) => {
      setLoading(true);
      const json = await dispatch(
        fetchThunk(
          API_PATHS.hotelDetail,
          'post',
          JSON.stringify({
            adults: paramsSearch.guestInfo.adultCount,
            children: paramsSearch.guestInfo.childCount,
            childrenAges: paramsSearch.guestInfo.childrenAges,
            checkIn: paramsSearch.checkIn,
            checkOut: paramsSearch.checkOut,
            numRooms: paramsSearch.guestInfo.roomCount,
            hotelId: paramsSearch.hotelId,
            direct: paramsSearch.direct,
          }),
        ),
      );
      if (json?.code === SUCCESS_CODE) {
        setData(json.data);
      } else {
        enqueueSnackbar(
          json.message,
          snackbarSetting(key => closeSnackbar(key), { color: 'error' }),
        );
      }
      const json2 = await dispatch(
        fetchThunk(
          `${API_PATHS.hotelPrice}`,
          'post',
          JSON.stringify({
            hotelId: paramsSearch.hotelId,
            adults: paramsSearch.guestInfo.adultCount,
            children: paramsSearch.guestInfo.childCount,
            numRooms: paramsSearch.guestInfo.roomCount,
            checkIn: paramsSearch.checkIn,
            checkOut: paramsSearch.checkOut,
            childrenAges: paramsSearch.guestInfo.childrenAges,
          }),
        ),
      );
      if (json2?.code === SUCCESS_CODE) {
        setRoom(json2.data);
        if (json2.data.length === 0) {
          setOpen(true);
        }
      } else {
        enqueueSnackbar(
          json2.message,
          snackbarSetting(key => closeSnackbar(key), { color: 'error' }),
        );
      }
      setLoading(false);
    },

    [closeSnackbar, dispatch, enqueueSnackbar],
  );

  React.useEffect(() => {
    if (search) {
      searchData(search);
    }
  }, [search, searchData]);

  React.useEffect(() => {
    try {
      const searchUrl = new URLSearchParams(location.search);
      const searchParams = parseHotelBookingParams(searchUrl, true, true);
      setSearch(searchParams);
    } catch (err) {
      dispatch(goBackAction());
    }
  }, [dispatch, location.search]);

  if (loading || !data || !search) {
    return <LoadingIcon style={{ height: 320 }} />;
  }
  return (
    <>
      <Helmet>
        <title>{data ? `${data.name} - ${data.address}` : 'Hotel detail'}</title>
      </Helmet>
      <HotelDetailDesktop
        hotelData={data}
        roomsData={room}
        search={search}
        setSearch={value => {
          if (value !== search) {
            window.scrollTo({ top: 0, behavior: 'smooth' });
            const params = stringifyHotelBookingParams(value);
            dispatch(goToReplace({ search: params }));
          }
        }}
        loading={loading}
      />
      <DialogCustom
        open={open}
        onClose={() => setOpen(false)}
        buttonLabel="close"
        PaperProps={{ style: { minWidth: 400 } }}
        titleLabel={
          <Typography variant="subtitle1">
            <FormattedMessage id="announce" />
          </Typography>
        }
      >
        <div style={{ padding: '16px', minHeight: 100 }}>
          <FormattedMessage id="hotel.noRoom" />
        </div>
      </DialogCustom>
    </>
  );
};

export default HotelDetail;
