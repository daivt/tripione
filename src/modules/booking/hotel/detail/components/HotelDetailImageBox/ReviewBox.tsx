import { Typography } from '@material-ui/core';
import * as React from 'react';
import { FormattedMessage } from 'react-intl';
import styled from 'styled-components';
import { BLUE, RED } from '../../../../../../configs/colors';
import { some } from '../../../../../../constants';

const LIST_SERVICE: some = [
  { id: 'hotel.info.overallScore', value: 'overallScore' },
  { id: 'hotel.info.mealScore', value: 'mealScore' },
  { id: 'hotel.info.cleanlinessScore', value: 'cleanlinessScore' },
  { id: 'hotel.info.locationScore', value: 'locationScore' },
  { id: 'hotel.info.roomScore', value: 'roomScore' },
  { id: 'hotel.info.serviceScore', value: 'serviceScore' },
  { id: 'hotel.info.sleepQualityScore', value: 'sleepQualityScore' },
];

const Line = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
  height: 32px;
`;
const Extend = styled.a`
  &:hover {
    text-decoration: underline;
  }

  color: ${BLUE};
  &:active {
    color: ${RED};
  }
  cursor: pointer;
`;

class ReviewBox extends React.PureComponent<{ hotelData: some }, { extend: boolean }> {
  state = { extend: false };

  public render() {
    const { hotelData } = this.props;
    const { extend } = this.state;
    return (
      <>
        {hotelData && (
          <div style={{ marginBottom: '12px' }}>
            <div
              style={{
                maxHeight: extend ? '100%' : '160px',
                overflow: 'hidden',
              }}
            >
              {LIST_SERVICE.map((v: some, index: number) => (
                <Line key={index}>
                  <Typography
                    variant={index !== 0 ? 'caption' : 'body2'}
                    style={{ color: index === 0 ? BLUE : undefined }}
                  >
                    <FormattedMessage id={v.id} />
                  </Typography>
                  <Typography variant="caption" style={{ color: index === 0 ? BLUE : undefined }}>
                    {hotelData[v.value]}
                  </Typography>
                </Line>
              ))}
            </div>
            <Line
              style={{
                display: 'flex',
                justifyContent: 'flex-end',
              }}
            >
              <Extend onClick={() => this.setState({ extend: !extend })}>
                <Typography variant="body2">
                  <FormattedMessage id={extend ? 'collapse' : 'seeAll'} />
                </Typography>
              </Extend>
            </Line>
          </div>
        )}
      </>
    );
  }
}

export default ReviewBox;
