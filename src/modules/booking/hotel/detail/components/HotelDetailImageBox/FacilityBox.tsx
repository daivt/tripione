import { Typography } from '@material-ui/core';
import React from 'react';
import { FormattedMessage } from 'react-intl';
import styled from 'styled-components';
import { BLUE, RED } from '../../../../../../configs/colors';
import { some } from '../../../../../../constants';
import { getFacilityIcon, getRelaxIcon } from '../../utils';

const Line = styled.div`
  display: flex;
  justify-content: flex-start;
  align-items: center;
  height: 32px;
`;
const Extend = styled.a`
  &:hover {
    text-decoration: underline;
  }

  color: ${BLUE};
  &:active {
    color: ${RED};
  }
  cursor: pointer;
`;
class FacilityBox extends React.PureComponent<{ hotelData: some }, { extend: boolean }> {
  state = { extend: false };

  public render() {
    const { hotelData } = this.props;
    const { extend } = this.state;
    const { features } = hotelData;
    const length = features.facilityIds.length + features.relaxIds.length;

    return (
      <>
        {features && (
          <div style={{ marginBottom: '12px' }}>
            <div
              style={{
                maxHeight: extend ? '100%' : '160px',
                overflow: 'hidden',
              }}
            >
              {features.facilityIds.map((v: number, index: number) => (
                <Line key={index}>
                  <img
                    style={{ marginRight: '12px', width: '24px', height: '24px' }}
                    src={getFacilityIcon(v)}
                    alt=""
                  />
                  <Typography variant="caption">{features.facilities[index]}</Typography>
                </Line>
              ))}
              {features.relaxIds.map((v: number, index: number) => (
                <Line key={index}>
                  <img
                    style={{ marginRight: '12px', width: '24px', height: '24px' }}
                    src={getRelaxIcon(v)}
                    alt=""
                  />
                  <Typography variant="caption">{features.relaxes[index]}</Typography>
                </Line>
              ))}
            </div>
            {length > 0 && (
              <Line
                style={{
                  display: 'flex',
                  justifyContent: 'flex-end',
                  height: '32px',
                }}
              >
                <Extend onClick={() => this.setState({ extend: !extend })}>
                  <Typography variant="body2">
                    <FormattedMessage id={extend ? 'collapse' : 'seeAll'} />
                  </Typography>
                </Extend>
              </Line>
            )}
          </div>
        )}
      </>
    );
  }
}

export default FacilityBox;
