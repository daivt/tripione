import { Divider, IconButton, Tab, Tabs, Typography, useTheme } from '@material-ui/core';
import CloseIcon from '@material-ui/icons/Close';
import IconStar from '@material-ui/icons/StarRounded';
import Rating from '@material-ui/lab/Rating';
import React from 'react';
import { FormattedMessage } from 'react-intl';
import SwipeableViews from 'react-swipeable-views';
import styled from 'styled-components';
import { some } from '../../../../../../constants';
import { ScrollBox } from '../../../common/element';
import HotelBasicStatsBox from '../HotelBasicStatsBox';
import PickRoomBox from '../PickRoomBox';
import FacilityBox from './FacilityBox';
import ReviewBox from './ReviewBox';

const Line = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
  min-height: 40px;
  flex-shrink: 0;
`;

export const INFO_BOX_WIDTH = '320px';

const TabContentContainer = styled.div`
  margin-left: 15px;
  margin-right: 15px;
  margin-top: 15px;
`;

const Wrapper = styled.div`
  background: white;
  width: ${INFO_BOX_WIDTH};
  min-height: 100vh;
  height: 100%;
  max-height: 600px;
  overflow: auto;
  display: flex;
  flex-direction: column;
  z-index: 1400;
  @media (min-height: 725px) {
    max-height: 100vh;
  }
`;

interface Props {
  hotelData: some;
  style?: React.CSSProperties;
  roomsData?: some[];
  onClick(): void;
  onClose?: () => void;
}

const InfoBox: React.FunctionComponent<Props> = props => {
  const { onClose } = props;
  const [currentTabIndex, setIndex] = React.useState(0);
  const { hotelData, roomsData, onClick } = props;
  const theme = useTheme();
  return (
    <Wrapper>
      <Line style={{ minHeight: 'auto' }}>
        <Typography variant="h5" style={{ marginLeft: '27px', marginTop: '8px', flex: 1 }}>
          {hotelData.name}
        </Typography>
        {onClose && (
          <IconButton color="default" size="medium" onClick={onClose}>
            <CloseIcon />
          </IconButton>
        )}
      </Line>
      <Line style={{ marginLeft: '14px' }}>
        <Rating
          icon={<IconStar style={{ height: '20px' }} />}
          value={hotelData.starNumber}
          readOnly
        />
      </Line>
      <ScrollBox style={{ flex: 1 }}>
        <div style={{ marginLeft: '16px' }}>
          <HotelBasicStatsBox hotelData={hotelData} />
        </div>
        <div
          style={{
            marginTop: '16px',
            marginLeft: '12px',
            marginRight: '12px',
            position: 'relative',
            boxSizing: 'border-box',
            maxWidth: '320px',
          }}
        >
          <Tabs
            variant="fullWidth"
            value={currentTabIndex === -1 ? false : currentTabIndex}
            onChange={(e, val) => setIndex(val)}
            indicatorColor="primary"
            textColor="primary"
          >
            <Tab
              fullWidth
              label={<FormattedMessage id="hotel.info.describe" />}
              style={{ minWidth: 'unset' }}
            />
            <Tab
              fullWidth
              label={<FormattedMessage id="hotel.info.review" />}
              style={{ minWidth: 'unset' }}
            />
          </Tabs>
          <Divider />
          <SwipeableViews
            axis={theme.direction === 'rtl' ? 'x-reverse' : 'x'}
            index={currentTabIndex}
            onChangeIndex={val => setIndex(val)}
          >
            <TabContentContainer>
              {currentTabIndex === 0 && <FacilityBox hotelData={hotelData} />}
            </TabContentContainer>
            <TabContentContainer>
              {currentTabIndex === 1 && <ReviewBox hotelData={hotelData} />}
            </TabContentContainer>
          </SwipeableViews>
        </div>
        <Line style={{ marginLeft: '27px', marginTop: '-12px' }}>
          <Typography variant="subtitle2">
            <FormattedMessage id="hotel.info.useful" />
          </Typography>
        </Line>
        {hotelData.checkInTime && (
          <Line style={{ padding: '0px 27px' }}>
            <Typography variant="body1">
              <FormattedMessage id="hotel.checkIn" />
            </Typography>
            <Typography color="textSecondary" variant="body2">
              {hotelData.checkInTime}
            </Typography>
          </Line>
        )}
        {hotelData.checkOutTime && (
          <Line style={{ padding: '0px 27px' }}>
            <Typography variant="body1">
              <FormattedMessage id="hotel.checkOut" />
            </Typography>
            <Typography color="textSecondary" variant="body2">
              {hotelData.checkOutTime}
            </Typography>
          </Line>
        )}
        <Line style={{ padding: '0px 27px' }}>
          <Typography variant="body1">
            <FormattedMessage id="hotel.info.distanceToAirport" />
          </Typography>
          <Typography color="textSecondary" variant="body2">
            <FormattedMessage
              id="hotel.info.distance"
              values={{ num: hotelData.distanceToAirport }}
            />
          </Typography>
        </Line>
        <br />
        <div style={{ padding: '0px 27px' }} dangerouslySetInnerHTML={{ __html: hotelData.desc }} />
        <br />
      </ScrollBox>
      <PickRoomBox roomsData={roomsData} onClick={onClick} />
    </Wrapper>
  );
};

export default InfoBox;
