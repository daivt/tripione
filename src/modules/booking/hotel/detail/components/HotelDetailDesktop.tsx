import { Container } from '@material-ui/core';
import * as React from 'react';
import { some } from '../../../../../constants';
import { HotelBookingParams } from '../../utils';
import HotelBasicInfo from './HotelBasicInfoBox';
import HotelDetailImageBox from './HotelDetailImageBox';
import HotelDetailTabs from './HotelDetailTabs';

export interface Props {
  hotelData: some;
  roomsData: some[];
  search: HotelBookingParams;
  setSearch(search: HotelBookingParams): void;
  loading?: boolean;
}

const HotelDetailDesktop: React.FC<Props> = props => {
  const { hotelData, roomsData, search, setSearch, loading } = props;
  return (
    <>
      <Container>
        <HotelBasicInfo hotel={hotelData} roomsData={roomsData} />
      </Container>
      <HotelDetailImageBox hotelData={hotelData} roomsData={roomsData} />
      <HotelDetailTabs
        hotelData={hotelData}
        roomsData={roomsData}
        search={search}
        setSearch={setSearch}
        loading={loading}
      />
    </>
  );
};

export default HotelDetailDesktop;
