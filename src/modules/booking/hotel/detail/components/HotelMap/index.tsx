import * as React from 'react';
import { some } from '../../../../../../constants';
import { scrollTo } from '../../../../utils';
import MapButtonBox from '../../../result/components/MapButtonBox';
import { OFFSET } from '../../constants';
import MapModal from './MapModal';

interface Props {
  hotelData: some;
  roomsData: some[];
}
const HotelMap: React.FC<Props> = props => {
  const { hotelData, roomsData } = props;
  const [open, setOpen] = React.useState(false);

  return (
    <div style={{ marginTop: '12px' }}>
      <MapButtonBox setOpenMap={() => setOpen(true)} />
      <MapModal
        open={open}
        onClose={() => {
          setOpen(false);
        }}
        hotelData={hotelData}
        roomsData={roomsData}
        onClick={() => {
          setOpen(false);
          scrollTo('hotel.roomList', OFFSET);
        }}
      />
    </div>
  );
};

export default HotelMap;
