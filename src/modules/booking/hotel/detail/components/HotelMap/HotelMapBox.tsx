import { Tooltip, withStyles, Zoom } from '@material-ui/core';
import GoogleMapReact from 'google-map-react';
import * as React from 'react';
import { KEY_GOOGLE_MAP, some } from '../../../../../../constants';
import { ReactComponent as IcLocation } from '../../../../../../svg/booking/ic_pin_map.svg';
import HotelInfoCard from '../../../result/components/MapDialog/HotelInfoCard';

const TooltipCS = withStyles(theme => ({
  tooltip: {
    padding: 0,
    margin: 0,
  },
}))(Tooltip);

const LocationComponent = (props: { lat: number; lng: number; data: some; roomsData?: some[] }) => {
  const { data, roomsData } = props;
  return (
    <TooltipCS
      key={data.id}
      TransitionComponent={Zoom}
      title={
        <HotelInfoCard
          hotelData={data}
          minPrice={roomsData && roomsData[0] && roomsData[0].finalPrice}
          bonusPoint={roomsData && roomsData[0] && roomsData[0].bonusPoint}
        />
      }
    >
      <div className={roomsData !== undefined ? 'mainPin' : 'svgMask'} id={data.id}>
        <IcLocation className="svgPin" />
      </div>
    </TooltipCS>
  );
};

export interface Props {
  hotelData: some;
  roomsData: some[];
}
const HotelMapBox: React.FC<Props> = props => {
  const { hotelData, roomsData } = props;
  const getCenter = React.useMemo(() => {
    return {
      lat: hotelData.latitude || 0,
      lng: hotelData.longitude || 0,
    };
  }, [hotelData.latitude, hotelData.longitude]);

  return (
    <>
      <GoogleMapReact
        bootstrapURLKeys={{ key: KEY_GOOGLE_MAP }}
        defaultCenter={getCenter}
        defaultZoom={17}
        options={maps => {
          return {
            mapTypeControl: true,
            mapTypeControlOptions: {
              position: maps.ControlPosition.TOP_RIGHT,
            },
            fullscreenControl: false,
          };
        }}
      >
        <LocationComponent
          lat={hotelData.latitude}
          lng={hotelData.longitude}
          data={hotelData}
          roomsData={roomsData}
        />
        {hotelData.nearbyPlaces.map((v: some) => (
          <LocationComponent key={v.id} lat={v.latitude} lng={v.longitude} data={v} />
        ))}
      </GoogleMapReact>
    </>
  );
};
export default HotelMapBox;
