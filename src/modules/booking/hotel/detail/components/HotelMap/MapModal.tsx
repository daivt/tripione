import { Modal } from '@material-ui/core';
import * as React from 'react';
import { some } from '../../../../../../constants';
import InfoBox from '../HotelDetailImageBox/InfoBox';
import HotelMapBox from './HotelMapBox';

export interface Props {
  open: boolean;
  onClose(): void;
  hotelData: some;
  roomsData: some[];
  onClick(): void;
}
const MapModal: React.FC<Props> = props => {
  const { open, onClose, hotelData, roomsData, onClick } = props;
  return (
    <Modal keepMounted={false} open={open} style={{ overflow: 'auto' }} onClose={onClose}>
      <div style={{ display: 'flex' }}>
        <div style={{ flex: 1, display: 'flex', flexDirection: 'column' }}>
          <HotelMapBox hotelData={hotelData} roomsData={roomsData} />
        </div>
        <InfoBox hotelData={hotelData} roomsData={roomsData} onClick={onClick} onClose={onClose} />
      </div>
    </Modal>
  );
};
export default MapModal;
