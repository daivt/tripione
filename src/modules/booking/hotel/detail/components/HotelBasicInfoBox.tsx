import { Button, Grid, IconButton, Popover, Typography } from '@material-ui/core';
import IconStar from '@material-ui/icons/StarRounded';
import Rating from '@material-ui/lab/Rating';
import { useSnackbar } from 'notistack';
import * as React from 'react';
import { FormattedMessage, FormattedNumber } from 'react-intl';
import { useDispatch } from 'react-redux';
import { Action } from 'redux';
import { ThunkDispatch } from 'redux-thunk';
import styled from 'styled-components';
import { API_PATHS } from '../../../../../configs/API';
import { GREY_700, ORANGE, RED } from '../../../../../configs/colors';
import { some, SUCCESS_CODE } from '../../../../../constants';
import { AppState } from '../../../../../redux/reducers';
import { ReactComponent as IconCoinSvg } from '../../../../../svg/booking/ic_coin.svg';
import { ReactComponent as IconHeart } from '../../../../../svg/booking/ic_heart.svg';
import { ReactComponent as IconLocation } from '../../../../../svg/booking/ic_pin.svg';
import { ReactComponent as IconShare } from '../../../../../svg/booking/ic_share.svg';
import { ReactComponent as ShareFacebook } from '../../../../../svg/booking/ic_share_facebook.svg';
import { ReactComponent as ShareZalo } from '../../../../../svg/booking/ic_share_zalo.svg';
import CopyTextField from '../../../../common/components/CopyTextField';
import { Row, snackbarSetting } from '../../../../common/components/elements';
import { fetchThunk } from '../../../../common/redux/thunk';
import { scrollTo } from '../../../utils';

const Box = styled.div`
  width: 472px;
  background: #ffffff;
  box-shadow: 0px 1px 3px rgba(0, 0, 0, 0.2), 0px 2px 2px rgba(0, 0, 0, 0.12),
    0px 0px 2px rgba(0, 0, 0, 0.14);
  border-radius: 0px;
  padding: 15px;
`;

const OFFSET = 135;

interface Props {
  hotel: some;
  compact?: boolean;
  style?: React.CSSProperties;
  roomsData?: some[];
}

const HotelBasicInfo: React.FunctionComponent<Props> = props => {
  const { hotel, compact, roomsData } = props;
  const dispatch = useDispatch<ThunkDispatch<AppState, null, Action<string>>>();
  const { enqueueSnackbar, closeSnackbar } = useSnackbar();
  const [anchorEl, setAnchorEl] = React.useState<Element | undefined>(undefined);
  const [isFavorite, setFavorite] = React.useState(hotel.isFavoriteHotel);
  const fullURL = window.location.href;
  const shareUrl = encodeURIComponent(fullURL);

  const handleShareHotel = React.useCallback(
    (event: React.MouseEvent<SVGSVGElement, MouseEvent>) => {
      setAnchorEl(event.currentTarget);
    },
    [],
  );

  const handleClose = React.useCallback(() => {
    setAnchorEl(undefined);
  }, []);

  const removeFavoriteHotel = React.useCallback(async () => {
    const json = await dispatch(
      fetchThunk(`${API_PATHS.removeFavoriteHotel}?hotelId=${hotel.id}`, 'post'),
    );
    if (json?.code === SUCCESS_CODE) {
      setFavorite(false);
    } else {
      enqueueSnackbar(
        json.message,
        snackbarSetting(key => closeSnackbar(key), {
          color: 'error',
        }),
      );
    }
  }, [closeSnackbar, dispatch, enqueueSnackbar, hotel.id]);

  const addFavoriteHotel = React.useCallback(async () => {
    const json = await dispatch(
      fetchThunk(`${API_PATHS.addFavoriteHotel}?hotelId=${hotel.id}`, 'post'),
    );
    if (json?.code === SUCCESS_CODE) {
      setFavorite(true);
    } else {
      enqueueSnackbar(
        json.message,
        snackbarSetting(key => closeSnackbar(key), {
          color: 'error',
        }),
      );
    }
  }, [closeSnackbar, dispatch, enqueueSnackbar, hotel.id]);

  const open = Boolean(anchorEl);

  return (
    <div
      style={{
        display: 'flex',
        justifyContent: 'space-between',
      }}
    >
      <div style={{ flex: 1, display: 'flex', flexDirection: 'column', justifyContent: 'center' }}>
        <Row style={{ minHeight: '40px', alignItems: 'center' }}>
          <Typography variant="h5">{hotel.name}</Typography>
          &nbsp;
          <Rating
            icon={<IconStar style={{ height: '20px' }} />}
            value={hotel.starNumber}
            readOnly
          />
        </Row>
        <Row style={{ height: '30px' }}>
          <IconLocation style={{ marginRight: '8px' }} />
          &nbsp;
          <Typography variant="body2" style={{ color: GREY_700, marginRight: 16 }}>
            {hotel.address}
          </Typography>
          <IconButton
            onClick={() => (isFavorite ? removeFavoriteHotel() : addFavoriteHotel())}
            style={{ padding: 0, marginRight: 16 }}
          >
            <IconHeart
              className="svgFillAll"
              style={{
                cursor: 'pointer',
                fill: isFavorite ? RED : undefined,
                stroke: isFavorite ? RED : GREY_700,
              }}
            />
          </IconButton>
          <IconShare style={{ cursor: 'pointer' }} onClick={handleShareHotel} />
        </Row>
      </div>
      <div
        style={{
          display: 'flex',
          width: '300px',
          flexDirection: 'column',
          marginTop: '8px',
        }}
      >
        <Row style={{ justifyContent: 'flex-end' }}>
          <Typography variant="h5" style={{ color: ORANGE }}>
            {roomsData && roomsData[0] ? (
              <FormattedNumber value={roomsData[0].finalPrice} />
            ) : (
              '---'
            )}
            <FormattedMessage id="currency" />
            &nbsp;
            <Typography variant="body1" color="textPrimary" component="span">
              <FormattedMessage id="hotel.priceNote" />
            </Typography>
          </Typography>
        </Row>
        {!compact && (
          <Row style={{ justifyContent: 'flex-end' }}>
            <IconCoinSvg style={{ marginRight: '8px' }} />
            <Typography variant="body2" className="point">
              {roomsData && roomsData[0] ? <FormattedNumber value={roomsData[0].bonusPoint} /> : 0}
              &nbsp;
              <FormattedMessage id="point" />
            </Typography>
          </Row>
        )}
        <Row style={{ justifyContent: 'flex-end' }}>
          <Button
            size="large"
            style={{
              width: 160,
              marginBottom: '12px',
              marginTop: '8px',
            }}
            disableElevation
            variant="contained"
            color="secondary"
            onClick={() => scrollTo('hotel.roomList', OFFSET)}
          >
            <FormattedMessage id="hotel.bookRoom" />
          </Button>
        </Row>
      </div>
      <Popover
        open={open}
        onClose={handleClose}
        anchorEl={anchorEl}
        style={{ zIndex: 100 }}
        anchorOrigin={{
          vertical: 'bottom',
          horizontal: 'right',
        }}
        transformOrigin={{
          vertical: 'top',
          horizontal: 'right',
        }}
      >
        <Box>
          <Grid container spacing={3} style={{ marginBottom: 8 }}>
            <Grid item xs={6} style={{ cursor: 'pointer' }}>
              <a
                href={`https://www.facebook.com/sharer/sharer.php?u=${shareUrl}`}
                target="_blank"
                rel="noopener noreferrer"
                style={{
                  display: 'flex',
                  alignItems: 'center',
                  textDecoration: 'inherit',
                  color: 'inherit',
                }}
              >
                <ShareFacebook style={{ paddingRight: '9px' }} />
                &nbsp;
                <Typography variant="body2">Share on Facebook</Typography>
              </a>
            </Grid>
            <Grid item xs={6} style={{ display: 'flex', alignItems: 'center', cursor: 'pointer' }}>
              <ShareZalo style={{ marginRight: '9px', height: '48px', width: 'auto' }} />
              <Typography variant="body2">Share on Zalo</Typography>
            </Grid>
          </Grid>
          <CopyTextField
            formControlStyle={{ flex: 1 }}
            label={<FormattedMessage id="shareLink" />}
            value={fullURL}
            inputProps={{
              readOnly: true,
            }}
            fullWidth
          />
        </Box>
      </Popover>
    </div>
  );
};

export default HotelBasicInfo;
