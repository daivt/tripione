import styled from 'styled-components';
import { BLUE, RED } from '../../../../../../configs/colors';

export const Extend = styled.a`
  margin-left: 36px;
  &:hover {
    text-decoration: underline;
  }
  color: ${BLUE};
  &:active {
    color: ${RED};
  }
  cursor: pointer;
`;
