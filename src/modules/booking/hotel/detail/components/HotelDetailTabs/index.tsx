import { Container, Tab, Tabs, Typography } from '@material-ui/core';
import * as React from 'react';
import { FormattedMessage } from 'react-intl';
import { GREY_100, GREY_300, GREY_700, PRIMARY } from '../../../../../../configs/colors';
import { some } from '../../../../../../constants';
import { scrollTo } from '../../../../utils';
import { HotelBookingParams } from '../../../utils';
import { OFFSET } from '../../constants';
import HotelBasicInfo from '../HotelBasicInfoBox';
import HotelMap from '../HotelMap';
import BookingParamsBox from './BookingParamsBox';
import FacilityBox from './FacilityBox';
import ReviewBox from './ReviewBox';
import RoomList from './RoomList';
import UsefulInfoBox from './UsefulInfoBox';

const tabs = [
  { id: 'hotel.introduce', content: <></> },
  { id: 'hotel.facility', content: <></> },
  { id: 'hotel.review', content: <></> },
  { id: 'hotel.useful.title', content: <></> },
  { id: 'hotel.roomList', content: <></> },
];

const STICKY_HEIGHT = 90;

interface Props {
  hotelData: some;
  roomsData: some[];
  search: HotelBookingParams;
  setSearch(search: HotelBookingParams): void;
  loading?: boolean;
}

const HotelDetailTabs: React.FunctionComponent<Props> = props => {
  const { hotelData, roomsData, search, setSearch, loading } = props;
  const [currentTabIndex, setIndex] = React.useState(0);
  const [showSticky, setShowSticky] = React.useState(false);
  const rootDiv = React.useRef<HTMLDivElement>(null);

  React.useEffect(() => {
    const handler = (e: any) => {
      e.preventDefault();
      if (rootDiv.current) {
        if (rootDiv.current.getBoundingClientRect().top < STICKY_HEIGHT) {
          setShowSticky(true);
        } else {
          setShowSticky(false);
        }
      }
    };
    window.addEventListener('scroll', handler);
    return () => window.removeEventListener('scroll', handler);
  }, []);

  return (
    <div ref={rootDiv}>
      <div
        style={{
          borderBottom: `1px solid ${GREY_300}`,
          boxShadow: '0px 1px 3px rgba(0, 0, 0, 0.2)',
          top: `${STICKY_HEIGHT - 2}px`,
          zIndex: 100,
          position: 'sticky',
          background: GREY_100,
        }}
      >
        <div
          style={{
            position: 'absolute',
            width: '100%',
            transform: 'translateY(-100%)',
            background: GREY_100,
            height: `${STICKY_HEIGHT}px`,
            visibility: showSticky ? 'visible' : 'hidden',
            opacity: showSticky ? 1 : 0,
            transition: 'opacity 300ms',
          }}
        >
          <Container>
            <HotelBasicInfo hotel={hotelData} roomsData={roomsData} compact />
          </Container>
        </div>
        <Container>
          <Tabs
            value={currentTabIndex === -1 ? false : currentTabIndex}
            onChange={(e, val) => {
              setIndex(val);
              scrollTo(tabs[val].id, OFFSET);
            }}
            indicatorColor="primary"
            textColor="primary"
          >
            {tabs.map((tab, index) => (
              <Tab
                label={
                  <Typography
                    variant="body2"
                    style={{
                      textDecoration: 'none',
                      color: index === currentTabIndex ? PRIMARY : GREY_700,
                    }}
                  >
                    <FormattedMessage id={tab.id} />
                  </Typography>
                }
                key={index}
              />
            ))}
          </Tabs>
        </Container>
      </div>
      <Container style={{ display: 'flex' }}>
        <div style={{ flex: 1, marginBottom: '32px' }}>
          <Typography id={tabs[0].id} variant="h6" style={{ margin: '16px 0' }}>
            <FormattedMessage id={tabs[0].id} />
          </Typography>
          <div dangerouslySetInnerHTML={{ __html: hotelData.desc }} />
          <Typography id={tabs[1].id} variant="h6" style={{ margin: '36px 0 16px 0' }}>
            <FormattedMessage id={tabs[1].id} />
          </Typography>
          <FacilityBox hotelData={hotelData} />
          <ReviewBox id={tabs[2].id} hotelData={hotelData} roomsData={roomsData} />
          <Typography id={tabs[3].id} variant="h6" style={{ margin: '36px 0 16px 0' }}>
            <FormattedMessage id={tabs[3].id} />
          </Typography>
          <UsefulInfoBox hotelData={hotelData} />
          <Typography id={tabs[4].id} variant="h6" style={{ margin: '28px 0 12px 0' }}>
            <FormattedMessage id={tabs[4].id} />
          </Typography>
          <RoomList loading={loading} roomsData={roomsData} search={search} />
        </div>
        <div style={{ width: '280px', margin: '32px 0 32px 32px' }}>
          <div style={{ position: 'sticky', top: 150 }}>
            <BookingParamsBox hotelData={hotelData} search={search} setSearch={setSearch} />
            <HotelMap hotelData={hotelData} roomsData={roomsData} />
          </div>
        </div>
      </Container>
    </div>
  );
};

export default HotelDetailTabs;
