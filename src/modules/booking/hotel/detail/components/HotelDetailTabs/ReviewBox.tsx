/* eslint-disable no-nested-ternary */
import { Typography } from '@material-ui/core';
import * as React from 'react';
import { FormattedMessage, useIntl } from 'react-intl';
import { connect } from 'react-redux';
import { Action } from 'redux';
import { ThunkDispatch } from 'redux-thunk';

import querystring from 'query-string';
import HotelBasicStatsBox from '../HotelBasicStatsBox';
import CommentCard from './CommentCard';
import { Extend } from './styles';
import ReviewSideBox from './ReviewSideBox';
import { Row } from '../../../../../common/components/elements';
import { some } from '../../../../../../constants';
import { AppState } from '../../../../../../redux/reducers';
import { scrollTo } from '../../../../utils';
import { OFFSET } from '../../constants';
import LoadingIcon from '../../../../../common/components/LoadingIcon';
import { fetchThunk } from '../../../../../common/redux/thunk';
import { API_PATHS } from '../../../../../../configs/API';
import { PAGE_SIZE_20 } from '../../../../constants';

interface Props {
  hotelData: some;
  id: string;
  dispatch: ThunkDispatch<AppState, null, Action<string>>;
  roomsData?: some[];
}

const ReviewBox: React.FunctionComponent<Props> = props => {
  const { hotelData, dispatch, id, roomsData } = props;
  const [open, setOpen] = React.useState(false);
  const [reviews, setReviews] = React.useState<some | undefined | null>(undefined);
  const intl = useIntl();

  const fetchData = React.useCallback(async () => {
    const searchStr = querystring.stringify({
      page: 1,
      language: intl.locale,
      pageSize: PAGE_SIZE_20,
      rootHotelId: hotelData.id,
    });
    const json = await dispatch(fetchThunk(`${API_PATHS.getHotelReviews}?${searchStr}`, 'get'));
    if (json.code === 200) {
      setReviews(json.data);
    } else {
      setReviews(null);
    }
  }, [dispatch, hotelData.id, intl.locale]);

  React.useEffect(() => {
    fetchData();
  }, [fetchData]);

  return (
    <>
      {reviews ? (
        <>
          <Typography id={id} variant="h6" style={{ margin: '36px 0 16px 0' }}>
            <FormattedMessage id="hotel.reviews" values={{ num: reviews.totalReviews }} />
          </Typography>
          <HotelBasicStatsBox hotelData={hotelData} />
          <div style={{ marginTop: '16px' }}>
            {reviews.reviews.slice(0, 2).map((v: some, index: number) => (
              <CommentCard key={index} data={v} />
            ))}
          </div>
          <Row style={{ height: '32px' }}>
            <Extend style={{ margin: 0 }} onClick={() => setOpen(true)}>
              <Typography variant="caption">
                <FormattedMessage id="hotel.seeReview" />
              </Typography>
            </Extend>
          </Row>
          <ReviewSideBox
            hotelData={hotelData}
            reviews={reviews.reviews}
            totalReviews={reviews.totalReviews}
            open={open}
            onClose={() => setOpen(false)}
            roomsData={roomsData}
            onClick={() => {
              setOpen(false);
              scrollTo('hotel.roomList', OFFSET);
            }}
          />
        </>
      ) : reviews === null ? (
        <>
          <Typography id={id} variant="h6" style={{ margin: '36px 0 16px 0' }}>
            <FormattedMessage id="hotel.reviews" values={{ num: 0 }} />
          </Typography>
          <HotelBasicStatsBox hotelData={hotelData} />
        </>
      ) : (
        <LoadingIcon />
      )}
    </>
  );
};

export default connect()(ReviewBox);
