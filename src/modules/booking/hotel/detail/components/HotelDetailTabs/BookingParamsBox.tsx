import { Paper, Typography } from '@material-ui/core';
import IconStar from '@material-ui/icons/StarRounded';
import { Rating } from '@material-ui/lab';
import * as React from 'react';
import { FormattedMessage } from 'react-intl';
import { some } from '../../../../../../constants';
import { ReactComponent as IconPin } from '../../../../../../svg/booking/ic_pin.svg';
import { Row } from '../../../../../common/components/elements';
import SearchBox from '../../../result/components/MapDialog/SearchBox';
import { HotelBookingParams } from '../../../utils';

interface Props {
  hotelData: some;
  search: HotelBookingParams;
  setSearch(search: HotelBookingParams): void;
}

const BookingParamsBox: React.FunctionComponent<Props> = props => {
  const { hotelData, search, setSearch } = props;

  return (
    <Paper variant="outlined" style={{ padding: '12px 16px' }}>
      {hotelData && (
        <>
          <Typography variant="h5" style={{ marginTop: '8px', marginLeft: '8px' }}>
            {hotelData.name}
          </Typography>
          <Rating
            name="half-rating"
            icon={<IconStar style={{ height: '20px' }} />}
            value={hotelData.starNumber}
            readOnly
          />
          <Row>
            <IconPin />
            &nbsp;
            <Typography variant="caption" color="textSecondary">
              {hotelData.address}
            </Typography>
          </Row>
          <Typography variant="h6" style={{ marginTop: 16 }}>
            <FormattedMessage id="hotel.filter.bookingHotelInfo" />
          </Typography>
          <SearchBox params={search} onSearch={setSearch} showButton />
        </>
      )}
    </Paper>
  );
};

export default BookingParamsBox;
