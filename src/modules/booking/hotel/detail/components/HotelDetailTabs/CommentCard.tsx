import { Typography } from '@material-ui/core';
import IconStart from '@material-ui/icons/StarRounded';
import Rating from '@material-ui/lab/Rating';
import * as React from 'react';
import { GREY_500, PRIMARY } from '../../../../../../configs/colors';
import { some } from '../../../../../../constants';
import { Row } from '../../../../../common/components/elements';

const CommentCard: React.FunctionComponent<{ data: some; style?: React.CSSProperties }> = props => {
  const { data, style } = props;
  return (
    <div style={{ padding: '8px 0px', flexDirection: 'column' }}>
      <div>
        <Row>
          <Row
            style={{
              backgroundPosition: 'center center',
              backgroundRepeat: 'no-repeat',
              borderRadius: '50%',
              border: `1px solid ${GREY_500}`,
              width: '40px',
              height: '40px',
              justifyContent: 'center',
              marginRight: '8px',
            }}
          >
            <img
              style={{
                width: '35px',
                maxWidth: '35px',
                objectFit: 'cover',
              }}
              src={data.logo}
              alt=""
            />
          </Row>
          <div style={{ flex: 1, padding: '0px 16px 0px 4px' }}>
            <Typography style={{ marginLeft: '8px' }} variant="body2">
              {data.review.username}
            </Typography>

            <div style={{ display: 'flex', ...style }}>
              <Rating
                style={{ marginRight: '32px' }}
                size="small"
                icon={
                  <IconStart
                    style={{
                      color: PRIMARY,
                      height: '16px',
                      width: '16px',
                    }}
                  />
                }
                value={data.review.rating}
                max={Math.round(data.review.rating)}
                readOnly
              />
              <Typography color="textSecondary" variant="caption">
                {data.review.publishedDate}
              </Typography>
            </div>
          </div>
        </Row>
      </div>
      <div style={{ paddingTop: '8px' }}>
        <Typography color="textSecondary" variant="body2">
          {data.review.content}
        </Typography>
      </div>
    </div>
  );
};

export default CommentCard;
