import * as React from 'react';
import RoomCard from './RoomCard';
import LoadingIcon from '../../../../../common/components/LoadingIcon';
import { some } from '../../../../../../constants';
import RoomDetail from './RoomDetail';
import { HotelBookingParams } from '../../../utils';

interface Props {
  roomsData: some[];
  search: HotelBookingParams;
  loading?: boolean;
}

const RoomList: React.FunctionComponent<Props> = props => {
  const { roomsData, search, loading } = props;
  const [currentRoom, setCurrentRoom] = React.useState<some | undefined>(undefined);
  if (loading) {
    return <LoadingIcon style={{ height: 320 }} />;
  }
  return (
    <>
      <div>
        {roomsData.map(room => (
          <RoomCard
            key={room.SHRUI}
            data={room}
            onSeeMore={() => setCurrentRoom(room)}
            search={search}
          />
        ))}
      </div>
      {currentRoom && (
        <RoomDetail
          data={currentRoom}
          search={search}
          open={!!currentRoom}
          onClose={() => setCurrentRoom(undefined)}
        />
      )}
    </>
  );
};

export default RoomList;
