import { Button, Dialog, IconButton, Typography } from '@material-ui/core';
import CloseIcon from '@material-ui/icons/Close';
import * as React from 'react';
import ReactImageGallery from 'react-image-gallery';
import { FormattedMessage, FormattedNumber } from 'react-intl';
import { GREY_500, ORANGE, RED } from '../../../../../../configs/colors';
import { ROUTES } from '../../../../../../configs/routes';
import { some } from '../../../../../../constants';
import defaultImage from '../../../../../../svg/booking/default_image.svg';
import { ReactComponent as IconCoin } from '../../../../../../svg/booking/ic_coin.svg';
import Link from '../../../../../common/components/Link';
import ProgressiveImage from '../../../../common/ProgressiveImage';
import { Line, ScrollBox } from '../../../common/element';
import { HotelBookingParams, stringifyHotelBookingParams } from '../../../utils';

interface Props {
  data: some;
  open: boolean;
  onClose(): void;
  search: HotelBookingParams;
}

const RoomDetail: React.FunctionComponent<Props> = props => {
  const { open, data, onClose, search } = props;
  return (
    <Dialog
      open={open}
      PaperProps={{
        style: {
          backgroundColor: 'white',
          boxShadow: 'none',
          minHeight: '95vh',
          //   minWidth: '576px',
          display: 'flex',
          borderRadius: '0px',
          flexDirection: 'column',
        },
      }}
      onBackdropClick={onClose}
    >
      <Line style={{ justifyContent: 'space-between', margin: '12px 0px 12px 16px' }}>
        <Typography variant="subtitle1">{data.roomTitle}</Typography>{' '}
        <IconButton
          style={{
            background: 'white',
          }}
          color="default"
          size="medium"
          onClick={() => onClose()}
        >
          <CloseIcon style={{ color: GREY_500 }} />
        </IconButton>
      </Line>
      <ScrollBox style={{ flex: 1 }}>
        {Object.keys(data).length > 1 && (
          <>
            <div style={{ padding: '8px 0px 0px 16px' }}>
              <ReactImageGallery
                items={
                  data.roomImages.length > 0
                    ? data.roomImages.map((v: string) => {
                        return {
                          original: v,
                          thumbnail: v,
                          originalTitle: data.roomTitle,
                          thumbnailTitle: data.roomTitle,
                        };
                      })
                    : [
                        {
                          original: defaultImage,
                          thumbnail: defaultImage,
                          originalTitle: 'no image',
                          thumbnailTitle: 'no image',
                        },
                      ]
                }
                renderItem={item => (
                  <ProgressiveImage
                    title={item.originalTitle}
                    src={item.original}
                    alt={item.originalTitle}
                    style={{
                      width: '536px',
                      height: '302px',
                      objectFit: 'cover',
                    }}
                  />
                )}
                infinite
                thumbnailPosition="bottom"
                showThumbnails
                showIndex
                slideDuration={400}
                startIndex={0}
                showFullscreenButton={false}
                autoPlay
              />
              <div>
                {data.bedInfo && (
                  <>
                    <Line style={{ height: '48px' }}>
                      <Typography variant="h6">
                        <FormattedMessage id="hotel.roomInfo" />
                      </Typography>
                    </Line>
                    <Line style={{ width: '100%' }}>
                      <Typography variant="body2">{data.bedInfo}</Typography>
                    </Line>
                    {data.isMultiBedGroup && (
                      <Line style={{ width: '100%', display: 'flex' }}>
                        <Typography variant="body2">
                          <span style={{ fontWeight: 500 }}>
                            <FormattedMessage id="hotel.note" />:
                          </span>
                          <FormattedMessage id="hotel.info.warning" />
                        </Typography>
                      </Line>
                    )}
                  </>
                )}
                <Line style={{ height: '48px' }}>
                  <Typography variant="h6">
                    <FormattedMessage id="hotel.feature" />
                  </Typography>
                </Line>
                <div style={{ display: 'flex', flexWrap: 'wrap' }}>
                  {data.roomFeatures.map((v: string) => (
                    <Line key={v} style={{ width: '50%' }}>
                      <Typography variant="body2">{v}</Typography>
                    </Line>
                  ))}
                </div>
              </div>
              <div style={{ padding: '0px 16px' }}>
                <Line style={{ height: '48px' }}>
                  <Typography variant="h6">
                    <FormattedMessage id="hotel.cancelPolicies" />
                  </Typography>
                </Line>
                {data.cancellationPoliciesList.length > 0 ? (
                  <div>
                    {data.cancellationPoliciesList.map((v: string, index: number) => (
                      <Typography key={index} variant="body2" style={{ padding: '4px 0px' }}>
                        +&nbsp;
                        {v}
                      </Typography>
                    ))}
                  </div>
                ) : (
                  <Typography variant="body2" style={{ color: RED }}>
                    <FormattedMessage id="hotel.noRefund" />
                  </Typography>
                )}
              </div>
              <div style={{ padding: '0px 16px 16px' }}>
                <Line style={{ height: '48px' }}>
                  <Typography variant="h6">
                    <FormattedMessage id="hotel.useful.title" />
                  </Typography>
                </Line>
                <div
                  dangerouslySetInnerHTML={{
                    __html: data.roomDescription,
                  }}
                />
              </div>
            </div>
          </>
        )}
      </ScrollBox>
      <div
        style={{
          height: '88px',
          bottom: 0,
          boxShadow:
            '0px 1px 15px rgba(0, 0, 0, 0.2), 0px 9px 46px rgba(0, 0, 0, 0.12), 0px 24px 38px rgba(0, 0, 0, 0.14)',
          position: 'relative',
          background: 'white',
        }}
      >
        <Line style={{ justifyContent: 'space-between', padding: '0px 32px', height: '88px' }}>
          <div>
            <Line>
              <Typography variant="h5" style={{ color: ORANGE }}>
                <FormattedNumber value={data.finalPrice} />
                &nbsp;
                <FormattedMessage id="currency" />
                <Typography variant="subtitle2" component="span" color="textPrimary">
                  <FormattedMessage id="hotel.priceNote" />
                </Typography>
              </Typography>
            </Line>
            <Line>
              <IconCoin style={{ marginRight: 8 }} />
              <Typography variant="body2" className="point">
                <FormattedMessage id="hotel.bonusPoint" values={{ num: data.bonusPoint }} />
              </Typography>
            </Line>
          </div>
          <Link
            to={{
              pathname: `${ROUTES.booking.hotel.bookingInfo}`,
              search: stringifyHotelBookingParams({ ...search, shrui: data.SHRUI }),
            }}
          >
            <Button
              color="secondary"
              variant="contained"
              size="large"
              disableElevation
              style={{ minWidth: '112px' }}
            >
              <Typography variant="subtitle2">
                <FormattedMessage id="hotel.bookNow" />
              </Typography>
            </Button>
          </Link>
        </Line>
      </div>
    </Dialog>
  );
};

export default RoomDetail;
