/* eslint-disable no-nested-ternary */
import { Button, Paper, Tooltip, Typography, withStyles } from '@material-ui/core';
import * as React from 'react';
import { FormattedMessage, FormattedNumber } from 'react-intl';
import { BLUE, GREY, GREY_500, PRIMARY, RED } from '../../../../../../configs/colors';
import { ROUTES } from '../../../../../../configs/routes';
import { some } from '../../../../../../constants';
import defaultImage from '../../../../../../svg/booking/default_image.svg';
import icDinner from '../../../../../../svg/booking/facilityIcon/ic_dinner.svg';
import icWifi from '../../../../../../svg/booking/facilityIcon/ic_wifi.svg';
import { ReactComponent as IconCoin } from '../../../../../../svg/booking/ic_coin.svg';
import { ReactComponent as IconDiscount } from '../../../../../../svg/booking/ic_discount_red.svg';
import icGuest from '../../../../../../svg/booking/ic_guest.svg';
import { Col, Row } from '../../../../../common/components/elements';
import Link from '../../../../../common/components/Link';
import ProgressiveImage from '../../../../common/ProgressiveImage';
import { Line } from '../../../common/element';
import { HotelBookingParams, stringifyHotelBookingParams } from '../../../utils';

const TooltipCS = withStyles(theme => ({
  tooltip: {
    fontSize: '14px',
    padding: 0,
    marginTop: 8,
    background: GREY,
  },
  arrow: {
    color: GREY,
  },
}))(Tooltip);

interface Props {
  data: some;
  search: HotelBookingParams;
  onSeeMore(): void;
}

const RoomCard: React.FunctionComponent<Props> = props => {
  const { data, search, onSeeMore } = props;
  return (
    <Paper
      variant="outlined"
      style={{
        overflow: 'hidden',
        display: 'flex',
        padding: '12px 8px 12px 12px',
        marginBottom: 16,
        cursor: 'pointer',
      }}
      onClick={() => onSeeMore()}
    >
      <div style={{ position: 'relative', marginRight: 12 }}>
        <ProgressiveImage
          style={{ width: '262px', height: '192px', objectFit: 'cover' }}
          src={
            data.roomImage
              ? data.roomImage
              : data.roomImages.length > 0
              ? data.roomImages[0]
              : defaultImage
          }
          alt=""
        />
        {data.isPackageRate && (
          <div
            style={{
              background: PRIMARY,
              width: '70px',
              height: '20px',
              borderRadius: '4px',
              right: '12px',
              top: '6px',
              position: 'absolute',
              display: 'flex',
              justifyContent: 'center',
              alignItems: 'center',
            }}
          >
            <Typography variant="caption" style={{ color: 'white' }}>
              <FormattedMessage id="hotel.packageRate" />
            </Typography>
          </div>
        )}
        <Typography
          style={{
            position: 'absolute',
            bottom: '8px',
            right: '8px',
            alignSelf: 'flex-end',
            color: 'white',
          }}
        >
          [<FormattedNumber value={data.roomImages.length} />]
        </Typography>
      </div>
      <Col style={{ flex: 1 }}>
        <Line>
          <Typography variant="subtitle2">{data.roomTitle}</Typography>
        </Line>
        <Line>
          <img src={icGuest} alt="" />
          <Typography variant="body2" style={{ marginLeft: '8px' }}>
            <FormattedMessage id="hotel.maxGuest" values={{ num: data.maxGuests }} />
          </Typography>
        </Line>
        {data.freeBreakfast === 1 && (
          <Line>
            <img src={icDinner} alt="" />
            <Typography variant="body2" style={{ marginLeft: '8px' }}>
              <FormattedMessage id="hotel.freeBreakfast" />
            </Typography>
          </Line>
        )}
        <Line>
          <img src={icWifi} alt="" />
          <Typography variant="body2" style={{ marginLeft: '8px' }}>
            <FormattedMessage id="hotel.freeWifi" />
          </Typography>
        </Line>
        <Line
          style={{
            padding: '5px 10px',
            height: 'max-content',
            flex: 1,
            alignItems: 'flex-end',
          }}
        >
          <Button>
            <Typography
              variant="body2"
              style={{
                color: BLUE,
              }}
            >
              <FormattedMessage id="hotel.roomDetail" />
            </Typography>
          </Button>
        </Line>
      </Col>
      <Col style={{ minWidth: '176px', textAlign: 'end' }}>
        {data.isPackageRate && (
          <div>
            <div style={{ display: 'flex', justifyContent: 'space-between', alignItems: 'center' }}>
              <IconDiscount />
              <Typography variant="caption" style={{ color: RED }}>
                {data.promotionInfo.description}
              </Typography>
            </div>
            <div>
              {data?.promotionInfo?.priceBeforePromotion > 0 && (
                <Typography
                  variant="body2"
                  style={{ textDecoration: 'line-through', color: GREY_500 }}
                >
                  <FormattedNumber value={data.promotionInfo.priceBeforePromotion} />
                  &nbsp;
                  <FormattedMessage id="currency" />
                </Typography>
              )}
            </div>
          </div>
        )}
        <Col style={{ alignItems: 'flex-end', justifyContent: 'flex-end', flex: 1 }}>
          <Row>
            <Typography variant="subtitle1" className="price">
              <FormattedNumber value={data.finalPrice} />
              &nbsp;
              <FormattedMessage id="currency" />
            </Typography>
            <Typography variant="subtitle2">
              <FormattedMessage id="hotel.oneNight" />
            </Typography>
          </Row>
          <Row>
            <IconCoin style={{ marginRight: '8px' }} />
            <Typography variant="body2" className="point">
              <FormattedMessage id="hotel.bonusPoint" values={{ num: data.bonusPoint }} />
            </Typography>
          </Row>
          <div style={{ paddingBottom: '8px' }}>
            <TooltipCS
              arrow
              title={
                <div style={{ padding: '16px', borderRadius: '4px' }}>
                  {data.cancellationPoliciesList && data.cancellationPoliciesList.length > 0 ? (
                    <>
                      {data.cancellationPoliciesList.map((v: string, index: number) => (
                        <Typography
                          key={index}
                          variant="body2"
                          style={{ padding: '4px 0px ', color: 'white' }}
                        >
                          -&nbsp;
                          {v}
                        </Typography>
                      ))}
                    </>
                  ) : (
                    <Typography variant="body2" style={{ color: RED }}>
                      <FormattedMessage id="hotel.noRefund" />
                    </Typography>
                  )}
                </div>
              }
            >
              <Button>
                <Typography
                  variant="caption"
                  style={{
                    color:
                      data.freeCancellation === 1
                        ? BLUE
                        : data.cancellationPoliciesList.length > 0
                        ? BLUE
                        : RED,
                  }}
                >
                  <FormattedMessage
                    id={
                      data.freeCancellation === 1
                        ? 'hotel.refund'
                        : data.cancellationPoliciesList.length > 0
                        ? 'hotel.checkRefund'
                        : 'hotel.noRefund'
                    }
                  />
                </Typography>
              </Button>
            </TooltipCS>
          </div>
          <Link
            to={{
              pathname: ROUTES.booking.hotel.bookingInfo,
              search: stringifyHotelBookingParams({ ...search, shrui: data.SHRUI }),
            }}
          >
            <Button
              style={{
                minWidth: 114,
              }}
              variant="contained"
              color="secondary"
              disableElevation
            >
              <FormattedMessage id="hotel.book" />
            </Button>
          </Link>
        </Col>
      </Col>
    </Paper>
  );
};

export default RoomCard;
