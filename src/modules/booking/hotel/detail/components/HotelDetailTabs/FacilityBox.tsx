/* eslint-disable no-nested-ternary */
import { Grid, Typography } from '@material-ui/core';
import * as React from 'react';
import { FormattedMessage } from 'react-intl';
import { getFacilityIcon, getRelaxIcon, getInternetIcon } from '../../utils';
import { Extend } from './styles';
import { some } from '../../../../../../constants';

interface IFacilityBoxProps {
  hotelData: some;
}
type Features = 'facility' | 'relax' | 'internet';
const FacilityBox: React.FunctionComponent<IFacilityBoxProps> = props => {
  const { hotelData } = props;
  const { features } = hotelData;
  const [extend, setExtend] = React.useState(false);

  const data = features.internetId
    .map((v: number, index: number) => {
      return { id: v, des: features.internet[index], type: 'internet' as Features };
    })
    .concat(
      features.facilityIds.map((v: number, index: number) => {
        return { id: v, des: features?.facilities[index], type: 'facility' as Features };
      }),
    )
    .concat(
      features.relaxIds.map((v: number, index: number) => {
        return { id: v, des: features.relaxes[index], type: 'relax' as Features };
      }),
    );

  return (
    <>
      {features && (
        <div>
          <Grid container spacing={3}>
            {data.slice(0, extend ? data.length : 7).map((v: some, index: number) => (
              <Grid item xs={3} style={{ display: 'flex', alignItems: 'center' }} key={index}>
                <img
                  style={{ marginRight: '12px', width: '24px', height: '24px' }}
                  src={
                    (v.type as Features) === 'relax'
                      ? getRelaxIcon(v.id)
                      : (v.type as Features) === 'facility'
                      ? getFacilityIcon(v.id)
                      : getInternetIcon(v.id)
                  }
                  alt=""
                />
                <Typography variant="caption">{v.des}</Typography>
              </Grid>
            ))}
            {data.length > 8 && (
              <Grid item xs={3} style={{ display: 'flex', alignItems: 'center' }}>
                <Extend onClick={() => setExtend(!extend)}>
                  <Typography variant="body2">
                    <FormattedMessage id={extend ? 'collapse' : 'seeAll'} />
                  </Typography>
                </Extend>
              </Grid>
            )}
          </Grid>
        </div>
      )}
    </>
  );
};

export default FacilityBox;
