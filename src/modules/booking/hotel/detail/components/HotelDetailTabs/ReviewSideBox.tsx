import { IconButton, Slide, Typography } from '@material-ui/core';
import CloseIcon from '@material-ui/icons/Close';
import IconStar from '@material-ui/icons/StarRounded';
import Rating from '@material-ui/lab/Rating';
import querystring from 'query-string';
import * as React from 'react';
import InfiniteScroll from 'react-infinite-scroller';
import { useIntl } from 'react-intl';
import { useDispatch } from 'react-redux';
import { ThunkDispatch } from 'redux-thunk';
import styled from 'styled-components';
import { Action } from 'typesafe-actions';
import { API_PATHS } from '../../../../../../configs/API';
import { some } from '../../../../../../constants';
import { AppState } from '../../../../../../redux/reducers';
import LoadingIcon from '../../../../../common/components/LoadingIcon';
import { fetchThunk } from '../../../../../common/redux/thunk';
import { PAGE_SIZE_20 } from '../../../../constants';
import HotelBasicStatsBox from '../HotelBasicStatsBox';
import PickRoomBox from '../PickRoomBox';
import CommentCard from './CommentCard';
import { ScrollBox } from '../../../common/element';

const WIDTH = 350;
const Line = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
  min-height: 40px;
`;

interface Props {
  open: boolean;
  onClose(): void;
  reviews: some[];
  hotelData: some;
  totalReviews: number;
  roomsData?: some[];
  onClick(): void;
}

const ReviewSideBox: React.FC<Props> = props => {
  const { hotelData, open, onClose, totalReviews, roomsData, onClick, reviews } = props;
  const dispatch = useDispatch<ThunkDispatch<AppState, null, Action<string>>>();
  const intl = useIntl();
  const [data, setData] = React.useState(reviews);

  const loadMore = React.useCallback(
    async (page: number) => {
      if (page > 25) {
        // safety valve
        return;
      }
      const searchStr = querystring.stringify({
        page,
        language: intl.locale,
        pageSize: PAGE_SIZE_20,
        rootHotelId: hotelData.id,
      });
      const json = await dispatch(fetchThunk(`${API_PATHS.getHotelReviews}?${searchStr}`, 'get'));
      if (json.code === 200) {
        setData(one => one.concat(json.data.reviews));
      }
    },
    [dispatch, hotelData.id, intl.locale],
  );

  return (
    <div>
      <Slide in={open} direction="left">
        <div
          style={{
            background: 'white',
            width: WIDTH,
            position: 'fixed',
            top: '0px',
            right: '0px',
            bottom: '0px',
            zIndex: 1300,
            boxShadow:
              '0px 8px 10px rgba(0, 0, 0, 0.2), 0px 6px 30px rgba(0, 0, 0, 0.12), 0px 16px 24px rgba(0, 0, 0, 0.14)',
            overflow: 'auto',
            display: 'flex',
            flexDirection: 'column',
          }}
        >
          <Line style={{ alignItems: 'flex-start' }}>
            <div style={{ margin: '10px 0' }}>
              <Typography variant="h5" style={{ marginLeft: '27px' }}>
                {hotelData.name}
              </Typography>
              <Line style={{ minHeight: '25px' }}>
                <Rating
                  style={{ marginLeft: '14px' }}
                  icon={<IconStar style={{ height: '20px' }} />}
                  value={hotelData.starNumber}
                  readOnly
                />
              </Line>
            </div>
            <IconButton size="medium" onClick={onClose}>
              <CloseIcon />
            </IconButton>
          </Line>
          <ScrollBox style={{ flex: 1 }}>
            <div style={{ marginLeft: '16px' }}>
              <HotelBasicStatsBox hotelData={hotelData} />
            </div>
            <InfiniteScroll
              pageStart={1}
              initialLoad={false}
              loadMore={loadMore}
              hasMore={data.length < totalReviews}
              loader={
                <LoadingIcon
                  key="loader"
                  style={{
                    display: 'flex',
                    alignItems: 'center',
                    justifyContent: 'center',
                  }}
                />
              }
              useWindow={false}
            >
              <div style={{ marginTop: '16px', marginLeft: '16px' }}>
                {data.map((v: some, index: number) => (
                  <CommentCard key={index} data={v} style={{ justifyContent: 'space-between' }} />
                ))}
              </div>
            </InfiniteScroll>
          </ScrollBox>
          <PickRoomBox roomsData={roomsData} onClick={onClick} />
        </div>
      </Slide>
    </div>
  );
};

export default ReviewSideBox;
