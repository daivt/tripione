/* eslint-disable no-nested-ternary */
import { Typography } from '@material-ui/core';
import * as React from 'react';
import { FormattedMessage, FormattedNumber } from 'react-intl';
import { BLUE } from '../../../../../configs/colors';
import { some } from '../../../../../constants';
import { Row } from '../../../../common/components/elements';

const HotelBasicStatsBox: React.FunctionComponent<{ hotelData: some }> = props => {
  const { hotelData } = props;

  return (
    <div>
      <Row style={{ justifyContent: 'flex-start', marginTop: '16px' }}>
        <div
          style={{
            background: BLUE,
            borderRadius: '4px',
            width: '32px',
            height: '32px',
            display: 'flex',
            justifyContent: 'center',
            alignItems: 'center',
          }}
        >
          <Typography variant="h6" style={{ color: 'white' }}>
            <FormattedNumber value={hotelData.overallScore / 10} />
          </Typography>
        </div>
        <div style={{ marginLeft: '12px' }}>
          <Typography variant="body2" style={{ color: BLUE, fontWeight: 500 }}>
            <FormattedMessage
              id={
                hotelData.overallScore > 90
                  ? 'hotel.ratingExcellent'
                  : hotelData.overallScore > 80 && hotelData.overallScore < 90
                  ? 'hotel.ratingVeryGood'
                  : hotelData.overallScore > 70 && hotelData.overallScore < 80
                  ? 'hotel.ratingGood'
                  : hotelData.overallScore > 60 && hotelData.overallScore < 70
                  ? 'hotel.ratingSatisfied'
                  : 'hotel.ratingNormal'
              }
            />
          </Typography>
          <Typography color="textSecondary" variant="caption">
            <FormattedMessage id="hotel.info.ratingNote" values={{ num: hotelData.numReviews }} />
          </Typography>
        </div>
      </Row>
    </div>
  );
};

export default HotelBasicStatsBox;
