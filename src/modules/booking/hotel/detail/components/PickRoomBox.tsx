import { Button, Typography } from '@material-ui/core';
import * as React from 'react';
import { FormattedMessage, FormattedNumber } from 'react-intl';
import { ORANGE } from '../../../../../configs/colors';
import { some } from '../../../../../constants';
import { ReactComponent as IconCoin } from '../../../../../svg/booking/ic_coin.svg';
import { Row } from '../../../../common/components/elements';

interface Props {
  roomsData?: some[];
  onClick(): void;
}

const PickRoomBox: React.FunctionComponent<Props> = props => {
  const { roomsData, onClick } = props;
  const price = roomsData && roomsData[0] ? roomsData[0].finalPrice : undefined;
  const points = roomsData && roomsData[0] ? roomsData[0].bonusPoint : undefined;

  return (
    <div
      style={{
        boxShadow:
          '0px 1px 15px rgba(0, 0, 0, 0.2), 0px 9px 46px rgba(0, 0, 0, 0.12), 0px 24px 38px rgba(0, 0, 0, 0.14)',
        background: 'white',
        position: 'relative',
      }}
    >
      <div style={{ width: '275px', margin: '8px auto' }}>
        <Row style={{ justifyContent: 'flex-end' }}>
          <Typography variant="h5" style={{ color: ORANGE }}>
            {price !== undefined ? <FormattedNumber value={price} /> : '---'}
            <FormattedMessage id="currency" />
            &nbsp;
            <Typography variant="body1" color="textPrimary" component="span">
              <FormattedMessage id="hotel.priceNote" />
            </Typography>
          </Typography>
        </Row>
        <Row style={{ justifyContent: 'flex-end', minHeight: '24px' }}>
          <IconCoin style={{ marginRight: '8px' }} />
          <Typography variant="body2" className="point">
            {points !== undefined ? <FormattedNumber value={points} /> : 0}
            &nbsp;
            <FormattedMessage id="point" />
          </Typography>
        </Row>
        <div style={{ textAlign: 'center', marginTop: '6px' }}>
          <Button
            color="secondary"
            variant="contained"
            size="large"
            disableElevation
            fullWidth
            onClick={onClick}
          >
            <FormattedMessage id="hotel.bookRoom" />
          </Button>
        </div>
      </div>
    </div>
  );
};

export default PickRoomBox;
