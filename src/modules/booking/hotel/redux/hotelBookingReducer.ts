import { ActionType, createAction, getType } from 'typesafe-actions';
import { some } from '../../../../constants';
import { defaultHotelBookingInfo, HotelBookInfo } from '../bookingInfo/utils';
import { HotelBookingParams } from '../utils';

export interface HotelBookingState extends HotelBookInfo {
  paramsBooking?: HotelBookingParams;
  roomsData: some[];
  hotelData: some;
}

export const defaultHotelBookingState: HotelBookingState = {
  ...defaultHotelBookingInfo,
  roomsData: [],
  hotelData: {},
};

export const setHotelBookingState = createAction(
  'common/setHotelBookingState',
  (val: HotelBookingState) => ({
    val,
  }),
)();
export const resetHotelBookingInfo = createAction('common/resetHotelBookingInfo', () => ({}))();

const actions = {
  setHotelBookingState,
  resetHotelBookingInfo,
};

type ActionT = ActionType<typeof actions>;

function reducer(
  state: HotelBookingState = defaultHotelBookingState,
  action: ActionT,
): HotelBookingState {
  switch (action.type) {
    case getType(setHotelBookingState):
      return { ...state, ...action.payload.val };
    case getType(resetHotelBookingInfo):
      return defaultHotelBookingState;
    default:
      return state;
  }
}

export default reducer;
