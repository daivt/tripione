export interface RoomsBookingInfo {
  userId?: number;
  name: string;
  phoneNumber: string;
  specialRequest?: string;
}

export const defaultRoomsBookingInfo: RoomsBookingInfo = {
  name: '',
  phoneNumber: '',
  specialRequest: '',
};

export interface HotelBookInfo {
  roomsBookingInfo: RoomsBookingInfo[];
  flightTicketNumber: string;
}

export const defaultHotelBookingInfo: HotelBookInfo = {
  roomsBookingInfo: [],
  flightTicketNumber: '',
};
