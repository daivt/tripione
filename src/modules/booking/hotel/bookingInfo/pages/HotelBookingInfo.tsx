import { useSnackbar } from 'notistack';
import React from 'react';
import { useIntl } from 'react-intl';
import { shallowEqual, useDispatch, useSelector } from 'react-redux';
import { useLocation } from 'react-router';
import { Action } from 'redux';
import { ThunkDispatch } from 'redux-thunk';
import { API_PATHS } from '../../../../../configs/API';
import { ROUTES } from '../../../../../configs/routes';
import { some, SUCCESS_CODE } from '../../../../../constants';
import { AppState } from '../../../../../redux/reducers';
import { snackbarSetting } from '../../../../common/components/elements';
import LoadingIcon from '../../../../common/components/LoadingIcon';
import RedirectDiv from '../../../../common/components/RedirectDiv';
import { goBackAction, goToAction } from '../../../../common/redux/reducer';
import { fetchThunk } from '../../../../common/redux/thunk';
import { resetHotelBookingInfo } from '../../redux/hotelBookingReducer';
import { getRoomData, HotelBookingParams, parseHotelBookingParams } from '../../utils';
import HotelBookingInfoDesktop from '../components/HotelBookingInfoDesktop';
import { HotelBookInfo } from '../utils';

interface Props {}

const HotelBookingInfo: React.FC<Props> = props => {
  const dispatch = useDispatch<ThunkDispatch<AppState, null, Action<string>>>();
  const intl = useIntl();
  const { paramsBooking } = useSelector((state: AppState) => state.booking.hotel, shallowEqual);

  const [data, setData] = React.useState<some | undefined>(undefined);
  const [room, setRoom] = React.useState<some[] | undefined>(undefined);
  const [search, setSearch] = React.useState<HotelBookingParams | undefined>(undefined);
  const [loading, setLoading] = React.useState(false);
  const location = useLocation();
  const { enqueueSnackbar, closeSnackbar } = useSnackbar();

  const roomDetail = React.useMemo(() => {
    return room && search ? getRoomData(room, search) : undefined;
  }, [room, search]);

  const getTripId = React.useMemo(() => {
    const state = location.state as some;
    const tripiId = state && state.tripiId;
    return tripiId;
  }, [location.state]);

  const searchData = React.useCallback(
    async (paramsSearch: HotelBookingParams) => {
      setLoading(true);
      const json = await dispatch(
        fetchThunk(
          API_PATHS.hotelDetail,
          'post',
          JSON.stringify({
            adults: paramsSearch.guestInfo.adultCount,
            children: paramsSearch.guestInfo.childCount,
            childrenAges: paramsSearch.guestInfo.childrenAges,
            checkIn: paramsSearch.checkIn,
            checkOut: paramsSearch.checkOut,
            numRooms: paramsSearch.guestInfo.roomCount,
            hotelId: paramsSearch.hotelId,
            direct: paramsSearch.direct,
          }),
        ),
      );
      if (json?.code === SUCCESS_CODE) {
        setData(json.data);
      } else {
        enqueueSnackbar(
          json?.message,
          snackbarSetting(key => closeSnackbar(key), { color: 'error' }),
        );
      }
      const json2 = await dispatch(
        fetchThunk(
          `${API_PATHS.hotelPrice}`,
          'post',
          JSON.stringify({
            hotelId: paramsSearch.hotelId,
            adults: paramsSearch.guestInfo.adultCount,
            children: paramsSearch.guestInfo.childCount,
            numRooms: paramsSearch.guestInfo.roomCount,
            checkIn: paramsSearch.checkIn,
            checkOut: paramsSearch.checkOut,
            childrenAges: paramsSearch.guestInfo.childrenAges,
          }),
        ),
      );
      if (json2?.code === SUCCESS_CODE) {
        setRoom(json2.data);
      } else {
        enqueueSnackbar(
          json2?.message,
          snackbarSetting(key => closeSnackbar(key), { color: 'error' }),
        );
      }
      setLoading(false);
    },
    [closeSnackbar, dispatch, enqueueSnackbar],
  );

  const bookHotel = React.useCallback(
    async (params: HotelBookInfo) => {
      if (!getTripId) {
        return;
      }
      const json = await dispatch(
        fetchThunk(
          `${API_PATHS.getBooking}`,
          'post',
          JSON.stringify({
            bookingType: 'HOTEL',
            tripId: getTripId,
            hotel: {
              SHRUI: search?.shrui,
              pnr: roomDetail?.isPackageRate ? params.flightTicketNumber : undefined,
              fromMobile: false,
              roomsBookingInfo: params.roomsBookingInfo.map(v => {
                return {
                  userId: v.userId,
                  specialRequest: v.specialRequest,
                };
              }),
            },
          }),
        ),
      );
      if (json.code === SUCCESS_CODE) {
        enqueueSnackbar(
          intl.formatMessage({ id: 'tripManagement.bookHotel' }),
          snackbarSetting(key => closeSnackbar(key), { color: 'success' }),
        );
        dispatch(
          goToAction({
            pathname: ROUTES.tripManagement.detail.gen(getTripId),
            search: '?tabIndex=1',
          }),
        );
      } else {
        enqueueSnackbar(
          json?.message,
          snackbarSetting(key => closeSnackbar(key), { color: 'error' }),
        );
      }
    },
    // eslint-disable-next-line react-hooks/exhaustive-deps
    [dispatch, getTripId, roomDetail],
  );

  React.useEffect(() => {
    if (search) {
      searchData(search);
    }
  }, [search, searchData]);

  React.useEffect(() => {
    try {
      const searchUrl = new URLSearchParams(location.search);
      const searchParams = parseHotelBookingParams(searchUrl, true);
      if (JSON.stringify(searchParams) !== JSON.stringify(paramsBooking)) {
        dispatch(resetHotelBookingInfo());
      }
      setSearch(searchParams);
    } catch (err) {
      dispatch(goBackAction());
    }
  }, [dispatch, getTripId, location.search, paramsBooking]);

  if (!data || !search || !room || loading) {
    return <LoadingIcon style={{ height: 320 }} />;
  }

  if (!getTripId) {
    return <RedirectDiv message={intl.formatMessage({ id: 'hotel.noTripId' })} />;
  }

  return (
    <>
      <HotelBookingInfoDesktop hotelData={data} roomsData={room} search={search} book={bookHotel} />
    </>
  );
};

export default HotelBookingInfo;
