import { Collapse, Typography } from '@material-ui/core';
import moment from 'moment';
import React from 'react';
import { FormattedDate, FormattedMessage, FormattedNumber } from 'react-intl';
import styled from 'styled-components';
import { BLUE, GREY_500, ORANGE } from '../../../../../../configs/colors';
import { some } from '../../../../../../constants';
import { DATE_FORMAT_BACK_END } from '../../../../../../models/moment';

const Line = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
  min-height: 40px;
`;

const ArrowStyle = styled.span<{ active: boolean }>`
  display: inline-block;
  margin-right: 12px;
  font-size: 12px;
  transition: all 300ms;
  transform: ${props => (!props.active ? 'rotate(0deg)' : 'rotate(180deg)')};
`;

interface Props {
  priceDetail: some;
  checkIn: string;
  isEurope: boolean;
}

export const RoomPriceDetail: React.FC<Props> = props => {
  const { priceDetail, checkIn, isEurope } = props;
  const [collapses, setCollapses] = React.useState<boolean[]>(
    Array(priceDetail.occupancies.length).fill(false),
  );
  return (
    <>
      <Line style={{ marginTop: '16px' }}>
        <Typography variant="subtitle2">
          <FormattedMessage id="hotel.roomDetail" />
        </Typography>
      </Line>

      {priceDetail.occupancies.map((obj: some, index: number) => (
        <div key={index}>
          <Line style={{ borderBottom: `0.5px solid ${GREY_500}` }}>
            <Typography variant="body2" style={{ fontWeight: 'bold', color: ORANGE }}>
              <FormattedMessage id="hotel.room" />
              &nbsp;{index + 1}
            </Typography>
            <Typography
              variant="body2"
              style={{ textTransform: 'lowercase' }}
              color="textSecondary"
            >
              {obj.adultNum}&nbsp;
              <FormattedMessage id="adult" />
              {!!obj.childrenNum && (
                <>
                  ,&nbsp;{obj.childrenNum}&nbsp;
                  <FormattedMessage id="children" />
                  &nbsp; (
                  <FormattedMessage id="childrenAge" values={{ number: obj.childrenAges }} />)
                </>
              )}
            </Typography>
          </Line>
          <Line
            style={{ cursor: 'pointer' }}
            onClick={() => {
              setCollapses([
                ...collapses.slice(0, index),
                !collapses[index],
                ...collapses.slice(index + 1),
              ]);
            }}
          >
            <Typography
              variant="body2"
              style={{ color: BLUE, display: 'flex', alignItems: 'center' }}
            >
              <ArrowStyle active={collapses[index]}>&#9660;</ArrowStyle>
              {obj.nightly.length}&nbsp;
              <FormattedMessage id="hotel.night" />
            </Typography>

            <Typography variant="body2" color="textSecondary">
              <FormattedNumber value={obj.totalBaseRate} />
              &nbsp;
              <FormattedMessage id="currency" />
            </Typography>
          </Line>
          <Collapse in={collapses[index]}>
            {obj.nightly.map((night: some, nIndex: number) => (
              <div
                key={nIndex}
                style={{
                  display: 'flex',
                  justifyContent: 'space-between',
                  marginLeft: '24px',
                  marginRight: '12px',
                }}
              >
                <Typography variant="body2">
                  <FormattedDate
                    value={moment(checkIn, DATE_FORMAT_BACK_END)
                      .clone()
                      .add(nIndex, 'days')
                      .toDate()}
                    weekday="short"
                  />
                  ,&nbsp;
                  {moment(checkIn, DATE_FORMAT_BACK_END)
                    .clone()
                    .add(nIndex, 'days')
                    .format('DD/MM')}
                </Typography>

                <Typography variant="body2" color="textSecondary">
                  <FormattedNumber value={night.baseRate} />
                  &nbsp;
                  <FormattedMessage id="currency" />
                </Typography>
              </div>
            ))}
          </Collapse>
          {!!obj.surchargePerDay && (
            <Line>
              <Typography variant="body2" style={{ color: BLUE }}>
                <FormattedMessage id="booking.guestFeeAddition" />
              </Typography>

              <Typography variant="body2" color="textSecondary">
                <FormattedNumber value={obj.surchargePerDay} />
                &nbsp;
                <FormattedMessage id="currency" />
                /1&nbsp;
                <FormattedMessage id="hotel.night" />
              </Typography>
            </Line>
          )}
          {!!obj.taxAndServiceFeePerDay && (
            <Line>
              {isEurope ? (
                <Typography variant="body2">
                  <FormattedMessage id="booking.vatFeePerDayEuropeOnly" />
                </Typography>
              ) : (
                <Typography variant="body2">
                  <FormattedMessage id="hotel.vatFeePerDay" />
                </Typography>
              )}

              <Typography variant="body2" color="textSecondary">
                <FormattedNumber value={obj.taxAndServiceFeePerDay} />
                &nbsp;
                <FormattedMessage id="currency" />
                /1&nbsp;
                <FormattedMessage id="hotel.night" />
              </Typography>
            </Line>
          )}
        </div>
      ))}
      <div style={{ borderTop: `0.5px solid ${GREY_500}` }}>
        {!!priceDetail.systemFee && (
          <Line>
            <Typography variant="body2" style={{ color: BLUE }}>
              <FormattedMessage id="booking.tripiFeeService" />
            </Typography>

            <Typography variant="body2" color="textSecondary">
              <FormattedNumber value={priceDetail.systemFee} />
              &nbsp;
              <FormattedMessage id="currency" />
            </Typography>
          </Line>
        )}

        {!!priceDetail.discount && (
          <Line>
            <Typography variant="body2" style={{ color: BLUE }}>
              <FormattedMessage id="hotel.discountService" />
            </Typography>
            <Typography variant="body2" className="price">
              <FormattedNumber value={priceDetail.discount} />
              &nbsp;
              <FormattedMessage id="currency" />
            </Typography>
          </Line>
        )}
        {!!priceDetail.mandatoryTaxes.length && (
          <div style={{ borderTop: `0.5px solid ${GREY_500}` }}>
            {priceDetail.mandatoryTaxes.map((item: any) => (
              <Line style={{ paddingTop: '8px' }}>
                <Typography variant="body2" style={{ color: BLUE }}>
                  {item.title}
                </Typography>
                <Typography variant="body2" color="textSecondary">
                  <FormattedNumber value={item.value} />
                  &nbsp;
                  <FormattedMessage id="currency" />
                </Typography>
              </Line>
            ))}
          </div>
        )}
      </div>
    </>
  );
};
