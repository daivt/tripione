import { Button, Paper, Typography } from '@material-ui/core';
import IconStar from '@material-ui/icons/StarRounded';
import Rating from '@material-ui/lab/Rating';
import moment from 'moment';
import * as React from 'react';
import { FormattedMessage, FormattedNumber } from 'react-intl';
import { useDispatch } from 'react-redux';
import { useHistory } from 'react-router';
import { BLUE, GREY_300, RED } from '../../../../../../configs/colors';
import { ROUTES } from '../../../../../../configs/routes';
import { some } from '../../../../../../constants';
import { DATE_FORMAT_BACK_END } from '../../../../../../models/moment';
import { ReactComponent as IconCoinSvg } from '../../../../../../svg/booking/ic_coin.svg';
import { ReactComponent as InfoIcon } from '../../../../../../svg/booking/ic_info.svg';
import { ReactComponent as IconLocation } from '../../../../../../svg/booking/ic_pin.svg';
import DialogCustom from '../../../../../common/components/DialogCustom';
import { LineBT36 } from '../../../../../common/components/elements';
import { goToReplace } from '../../../../../common/redux/reducer';
import { HOTEL_BOOK_PARAMS_NAMES } from '../../../constants';
import { computeHotelPayableNumbers, getRoomData, HotelBookingParams } from '../../../utils';
import FeatureBox from './FeatureBox';
import { RoomPriceDetail } from './RoomPriceDetail';

export interface Props {
  hotelData: some;
  params: HotelBookingParams;
  roomsData: some[];
  style?: React.CSSProperties;
}

const HotelBookingInfoBox: React.FC<Props> = props => {
  const { hotelData, style, params, roomsData } = props;
  const dispatch = useDispatch();
  const history = useHistory();
  const [showExpireDialog, setShowExpireDialog] = React.useState(true);
  const [showCancelPoliciesDialog, setShowCancelPoliciesDialog] = React.useState(false);
  const [showCheckInGuidanceDialog, setShowCheckInGuidanceDialog] = React.useState(false);

  const closeDialog = React.useCallback(() => {
    setShowExpireDialog(false);
    const paramsUrl = new URLSearchParams(history.location.search);
    paramsUrl.delete(HOTEL_BOOK_PARAMS_NAMES.shrui);
    dispatch(goToReplace({ pathname: ROUTES.booking.hotel.detail, search: paramsUrl.toString() }));
  }, [dispatch, history.location.search]);

  const room = React.useMemo(() => {
    return getRoomData(roomsData, params);
  }, [params, roomsData]);

  const payableNumbers = React.useMemo(() => {
    return computeHotelPayableNumbers(params, roomsData);
  }, [params, roomsData]);

  const nights = React.useMemo(() => {
    return moment(params.checkOut, DATE_FORMAT_BACK_END).diff(
      moment(params.checkIn, DATE_FORMAT_BACK_END),
      'days',
    );
  }, [params]);

  const { finalPrice } = payableNumbers;

  if (!room) {
    return (
      <DialogCustom
        open={!!showExpireDialog}
        onClose={closeDialog}
        buttonLabel="ok"
        PaperProps={{ style: { width: 400 } }}
        titleLabel={
          <Typography variant="h6">
            <FormattedMessage id="attention" />
          </Typography>
        }
      >
        <Typography variant="body1" style={{ padding: '24px 16px' }}>
          <FormattedMessage id="hotel.expiredHotel" />
        </Typography>
      </DialogCustom>
    );
  }
  const { priceDetail } = room;
  return (
    <Paper
      style={{
        padding: '16px',
        ...style,
      }}
      variant="outlined"
    >
      <Typography variant="h5">{hotelData.name}</Typography>
      <LineBT36>
        <Rating
          icon={<IconStar style={{ height: '20px', marginRight: '-8px' }} />}
          value={hotelData.starNumber}
          readOnly
        />
      </LineBT36>
      <LineBT36 style={{ padding: '8px 0px', justifyContent: 'flex-start' }}>
        <IconLocation style={{ width: 32, height: 'auto' }} />
        <Typography color="textSecondary" style={{ marginLeft: '8px' }}>
          {hotelData.address}
        </Typography>
      </LineBT36>
      <LineBT36>
        <Typography variant="body2" style={{ color: BLUE }}>
          <FormattedMessage id="hotel.holdingTime" />
        </Typography>
        <Typography variant="body2" style={{ color: BLUE }}>
          <FormattedMessage
            id="hotel.time"
            values={{
              day: Math.floor(moment.duration(room.holdingTime).asDays()),
              time: moment
                .utc(moment.duration(room.holdingTime).asMilliseconds())
                .format('HH:mm:ss'),
            }}
          />
        </Typography>
      </LineBT36>
      <LineBT36 style={{ marginTop: '16px' }}>
        <Typography variant="subtitle1">
          <FormattedMessage id="hotel.bookingHotelInfo" />
        </Typography>
      </LineBT36>
      <LineBT36>
        <Typography variant="subtitle2">
          <FormattedMessage id="hotel.checkIn" />
        </Typography>
        <Typography variant="body2" color="textSecondary" style={{ textAlign: 'end' }}>
          {params.checkIn && moment(params.checkIn, DATE_FORMAT_BACK_END).format('L')}
          {params.checkIn && hotelData.checkInTime && (
            <>
              &nbsp;
              <FormattedMessage id="from" />
              &nbsp;
            </>
          )}
          {hotelData.checkInTime}
        </Typography>
      </LineBT36>
      <LineBT36>
        <Typography variant="subtitle2">
          <FormattedMessage id="hotel.checkOut" />
        </Typography>
        <Typography variant="body2" color="textSecondary" style={{ textAlign: 'end' }}>
          {params.checkOut && moment(params.checkOut, DATE_FORMAT_BACK_END).format('L')}
          {params.checkOut && hotelData.checkOutTime && (
            <>
              &nbsp;
              <FormattedMessage id="to" />
              &nbsp;
            </>
          )}

          {hotelData.checkOutTime}
        </Typography>
      </LineBT36>
      <LineBT36>
        <Typography variant="subtitle2">
          <FormattedMessage id="hotel.totalDay" />
        </Typography>
        <Typography variant="body2" color="textSecondary">
          <FormattedMessage id="hotel.nights" values={{ num: nights }} />
        </Typography>
      </LineBT36>
      <LineBT36 style={{ justifyContent: 'space-between' }}>
        <Typography variant="subtitle2" style={{ marginRight: '16px' }}>
          <FormattedMessage id="hotel.room" values={{ num: '' }} />
        </Typography>
        <div style={{ display: 'flex', justifyContent: 'flex-end', wordBreak: 'break-word' }}>
          <Typography style={{ textAlign: 'right' }} variant="body2" color="textSecondary">
            {room.roomTitle}
          </Typography>
        </div>
      </LineBT36>
      <Button
        style={{ display: 'flex', alignItems: 'center' }}
        onClick={() => {
          setShowCancelPoliciesDialog(true);
        }}
      >
        <InfoIcon />
        <Typography variant="caption" style={{ color: BLUE, marginLeft: '12px' }}>
          <FormattedMessage id="hotel.cancelPolicies" />
        </Typography>
      </Button>
      <Button
        style={{ display: 'flex', alignItems: 'center', marginTop: '10px' }}
        onClick={() => {
          setShowCheckInGuidanceDialog(true);
        }}
      >
        <InfoIcon />
        <Typography variant="caption" style={{ color: BLUE, marginLeft: '12px' }}>
          <FormattedMessage id="hotel.checkinGuidance" />
        </Typography>
      </Button>
      <DialogCustom
        open={showCancelPoliciesDialog}
        onClose={() => {
          setShowCancelPoliciesDialog(false);
        }}
        buttonLabel="ok"
        PaperProps={{ style: { width: 550 } }}
        titleLabel={
          <Typography variant="subtitle1">
            <FormattedMessage id="hotel.cancelPolicies" />
          </Typography>
        }
      >
        <div
          style={{
            padding: '16px',
          }}
        >
          {room.cancellationPoliciesList && room.cancellationPoliciesList.length > 0 ? (
            <>
              {room.cancellationPoliciesList.map((v: string, index: number) => (
                <Typography key={index} variant="body2" style={{ padding: '4px 0px ' }}>
                  -&nbsp;
                  {v}
                </Typography>
              ))}
            </>
          ) : (
            <Typography variant="body2" style={{ color: RED }}>
              <FormattedMessage id="hotel.noRefund" />
            </Typography>
          )}
        </div>
      </DialogCustom>
      <DialogCustom
        open={showCheckInGuidanceDialog}
        onClose={() => {
          setShowCheckInGuidanceDialog(false);
        }}
        buttonLabel="ok"
        PaperProps={{ style: { width: 550 } }}
        titleLabel={
          <Typography variant="subtitle1">
            <FormattedMessage id="hotel.checkinGuidance" />
          </Typography>
        }
      >
        <div
          style={{
            padding: '16px',
          }}
        >
          {room.checkinInstructions || room.checkinSpecialInstructions ? (
            <>
              {room.checkinInstructions && (
                <Typography variant="body2" style={{ padding: '4px 0px ' }}>
                  -&nbsp;
                  <span dangerouslySetInnerHTML={{ __html: room.checkinInstructions }} />
                </Typography>
              )}
              {room.checkinSpecialInstructions && (
                <Typography variant="body2" style={{ padding: '4px 0px ' }}>
                  -&nbsp;
                  <span dangerouslySetInnerHTML={{ __html: room.checkinSpecialInstructions }} />
                </Typography>
              )}
            </>
          ) : (
            <Typography variant="body2" style={{ color: RED }}>
              <FormattedMessage id="hotel.noRefundMessages" />
            </Typography>
          )}
        </div>
      </DialogCustom>
      <LineBT36>
        <Typography variant="subtitle1">
          <FormattedMessage id="hotel.includedServices" />
        </Typography>
      </LineBT36>
      <FeatureBox data={room.roomFeatures} />
      {priceDetail && (
        <RoomPriceDetail
          priceDetail={priceDetail}
          checkIn={params.checkIn}
          isEurope={room.isEuropeHotel}
        />
      )}

      <LineBT36>
        <Typography variant="subtitle2">
          <FormattedMessage id="flight.totalPayable" />
        </Typography>
        <Typography variant="subtitle1" className="final-price">
          <FormattedNumber value={finalPrice > 0 ? finalPrice : 0} maximumFractionDigits={0} />
          &nbsp;
          <FormattedMessage id="currency" />
        </Typography>
      </LineBT36>
      <div
        style={{
          display: 'flex',
          alignItems: 'center',
          justifyContent: 'flex-end',
          color: `${GREY_300}`,
        }}
      >
        <LineBT36>
          <Typography color="textSecondary" variant="caption">
            <FormattedMessage id="flight.includeTaxesAndFees" />
          </Typography>
        </LineBT36>
      </div>
      {room.bonusPoint > 0 && (
        <div style={{ display: 'flex', alignItems: 'center', justifyContent: 'flex-end' }}>
          <IconCoinSvg style={{ marginRight: '10px' }} />
          <Typography variant="body2" className="point">
            <FormattedNumber value={room.bonusPoint} />
            &nbsp;
            <FormattedMessage id="point" />
          </Typography>
        </div>
      )}
    </Paper>
  );
};

export default HotelBookingInfoBox;
