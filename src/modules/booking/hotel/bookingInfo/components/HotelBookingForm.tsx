/* eslint-disable react/no-danger */
import {
  Button,
  Checkbox,
  Divider,
  ExpansionPanel,
  ExpansionPanelDetails,
  ExpansionPanelSummary,
  Paper,
  Typography,
} from '@material-ui/core';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import { useFormik } from 'formik';
import React from 'react';
import { FormattedMessage, useIntl } from 'react-intl';
import { shallowEqual, useSelector } from 'react-redux';
import * as yup from 'yup';
import { GREY_100 } from '../../../../../configs/colors';
import { some } from '../../../../../constants';
import { AppState } from '../../../../../redux/reducers';
import { Row } from '../../../../common/components/elements';
import FormControlTextField from '../../../../common/components/FormControlTextField';
import ListContactDialog from '../../../common/ListContactDialog';
import { getRoomData, HotelBookingParams } from '../../utils';
import {
  defaultHotelBookingInfo,
  defaultRoomsBookingInfo,
  HotelBookInfo,
  RoomsBookingInfo,
} from '../utils';

interface Props {
  params: HotelBookingParams;
  roomsData: some[];
  book(params: HotelBookInfo): void;
}

const HotelBookingForm: React.FC<Props> = props => {
  const { params, roomsData, book } = props;
  const intl = useIntl();
  const { flightTicketNumber, roomsBookingInfo } = useSelector(
    (state: AppState) => state.booking.hotel,
    shallowEqual,
  );

  const room = React.useMemo(() => {
    return getRoomData(roomsData, params);
  }, [params, roomsData]);

  const [confirm, setConfirm] = React.useState(!room?.isPackageRate);

  const bookingInfoSchema = yup.object().shape({
    flightTicketNumber: yup.string().test({
      name: 'flightTicketNumber',
      test: value => (room?.isPackageRate ? !!value : true),
      message: intl.formatMessage({ id: 'required' }),
    }),
    roomsBookingInfo: yup.array().of(
      yup.object().shape({
        userId: yup
          .number()
          .nullable()
          .required(intl.formatMessage({ id: 'required' })),
        name: yup
          .string()
          .trim()
          .test({
            name: 'name',
            message: intl.formatMessage({ id: 'fullNameValid' }),
            test: value => {
              return value ? value.split(' ').length > 1 : true;
            },
          })
          .required(intl.formatMessage({ id: 'required' })),
        phoneNumber: yup
          .string()
          .nullable()
          .min(9, intl.formatMessage({ id: 'phoneNumberValid' }))
          .max(12, intl.formatMessage({ id: 'phoneNumberValid' }))
          .required(intl.formatMessage({ id: 'required' })),
      }),
    ),
  });

  const formik = useFormik({
    initialValues: {
      flightTicketNumber: flightTicketNumber || defaultHotelBookingInfo.flightTicketNumber,
      roomsBookingInfo:
        roomsBookingInfo.length === params.guestInfo.roomCount
          ? roomsBookingInfo
          : Array(params.guestInfo.roomCount).fill(defaultRoomsBookingInfo),
    },
    onSubmit: values => {
      book(values);
    },
    validationSchema: bookingInfoSchema,
  });
  if (!room) {
    return null;
  }

  return (
    <form autoComplete="off" onSubmit={formik.handleSubmit} style={{ flex: 1 }}>
      {room.isPackageRate && (
        <Paper variant="outlined" style={{ padding: 16, marginBottom: 16 }}>
          <Typography variant="h6">
            <FormattedMessage id="hotel.flightTicketCode" />
          </Typography>
          <Typography
            variant="body2"
            color="textSecondary"
            style={{ marginTop: '8px', marginBottom: '8px' }}
          >
            <FormattedMessage id="hotel.flightTicketCodeWarning" />
          </Typography>
          <FormControlTextField
            id="flightTicketNumber"
            value={formik.values.flightTicketNumber}
            onChange={formik.handleChange}
            errorMessage={formik.submitCount ? formik.errors.flightTicketNumber : undefined}
          />
        </Paper>
      )}
      {formik.values.roomsBookingInfo.map((value: RoomsBookingInfo, index: number) => (
        <Paper key={index} variant="outlined" style={{ padding: 16, marginBottom: 16 }}>
          <Row>
            <Typography variant="h6">
              <FormattedMessage id="hotel.customerInfo" values={{ num: index + 1 }} />
            </Typography>
            <ListContactDialog
              onSelect={one =>
                formik.setFieldValue('roomsBookingInfo', [
                  ...formik.values.roomsBookingInfo.slice(0, index),
                  { ...value, name: one.name, phoneNumber: one.phone, userId: one.id },
                  ...formik.values.roomsBookingInfo.slice(index + 1),
                ])
              }
            />
          </Row>
          <Row>
            <FormControlTextField
              label={intl.formatMessage({ id: 'fullName' })}
              placeholder={intl.formatMessage({ id: 'fullNameEx' })}
              value={value.name}
              readOnly
              errorMessage={
                formik.submitCount
                  ? (formik.errors.roomsBookingInfo?.[index] as any)?.name
                  : undefined
              }
            />
            <FormControlTextField
              label={intl.formatMessage({ id: 'phoneNumber' })}
              placeholder={intl.formatMessage({ id: 'phoneNumberEx' })}
              formControlStyle={{ marginRight: 0 }}
              value={value.phoneNumber}
              readOnly
              errorMessage={
                formik.submitCount
                  ? (formik.errors.roomsBookingInfo?.[index] as any)?.phoneNumber
                  : undefined
              }
            />
          </Row>
          <div style={{ display: 'flex', alignItems: 'center', minHeight: 12, marginBottom: 8 }}>
            {index === 0 && formik.values.roomsBookingInfo.length > 1 && (
              <Button
                style={{ background: GREY_100 }}
                onClick={() => {
                  formik.setFieldValue(
                    'roomsBookingInfo',
                    formik.values.roomsBookingInfo.map(v => {
                      return formik.values.roomsBookingInfo[0];
                    }, true),
                  );
                }}
              >
                <Typography variant="body2">
                  <FormattedMessage id="hotel.useForOtherRooms" />
                </Typography>
              </Button>
            )}
          </div>
          <Divider />{' '}
          <ExpansionPanel style={{ boxShadow: 'none', border: 'none', marginTop: 0 }}>
            <ExpansionPanelSummary
              expandIcon={<ExpandMoreIcon />}
              aria-controls="panel1a-content"
              id="panel1a-header"
              style={{ padding: '0px', minHeight: 52, maxHeight: 52 }}
            >
              <Typography variant="subtitle2">
                <FormattedMessage id="hotel.addRequest" />
              </Typography>
            </ExpansionPanelSummary>
            <ExpansionPanelDetails
              style={{
                background: 'white',
                paddingTop: '16px',
                padding: 'unset',
                flexDirection: 'column',
              }}
            >
              <Typography variant="body2" color="textSecondary">
                <FormattedMessage id="hotel.enterYourRequest" />
              </Typography>
              <FormControlTextField
                label={intl.formatMessage({ id: 'hotel.requestContent' })}
                placeholder={intl.formatMessage({ id: 'hotel.enterRequest' })}
                formControlStyle={{ marginRight: 0 }}
                value={value.specialRequest}
                onChange={e =>
                  formik.setFieldValue('roomsBookingInfo', [
                    ...formik.values.roomsBookingInfo.slice(0, index),
                    { ...value, specialRequest: e.target.value },
                    ...formik.values.roomsBookingInfo.slice(index + 1),
                  ])
                }
                optional
              />
            </ExpansionPanelDetails>
          </ExpansionPanel>
        </Paper>
      ))}
      {room && room.isPackageRate && (
        <Row>
          <Checkbox
            color="secondary"
            checked={confirm}
            onChange={event => setConfirm(event.target.checked)}
          />
          <Typography variant="body2">
            <FormattedMessage id="booking.hotel.confirmFlightTicketCode" />
          </Typography>
        </Row>
      )}
      <Row style={{ justifyContent: 'center' }}>
        <Button
          variant="contained"
          color="secondary"
          style={{ marginTop: 16, minWidth: 160 }}
          size="large"
          type="submit"
          disableElevation
          disabled={!confirm}
        >
          <FormattedMessage id="save" />
        </Button>
      </Row>
    </form>
  );
};

export default HotelBookingForm;
