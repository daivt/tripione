import { Container, Typography, useTheme, useMediaQuery } from '@material-ui/core';
import WarningRoundedIcon from '@material-ui/icons/WarningRounded';
import * as React from 'react';
import { FormattedMessage } from 'react-intl';
import { some } from '../../../../../constants';
import { HotelBookingParams } from '../../utils';
import HotelBookingInfoBox from './HotelBookingInfoBox';
import HotelBookingForm from './HotelBookingForm';
import { Row } from '../../../../common/components/elements';
import AsideBound from '../../../common/AsideBound';
import { HotelBookInfo } from '../utils';

export interface Props {
  hotelData: some;
  roomsData: some[];
  search: HotelBookingParams;
  book(params: HotelBookInfo): void;
}

const HotelBookingInfoDesktop: React.FC<Props> = props => {
  const { hotelData, roomsData, search, book } = props;
  const theme = useTheme();
  const isTablet = useMediaQuery(theme.breakpoints.down('md'));
  return (
    <>
      <Container>
        <Typography variant="h5" style={{ marginBottom: '16px' }}>
          <FormattedMessage id="hotel.bookingHotelInfo" />
        </Typography>
        <Row style={{ margin: '18px 0' }}>
          <WarningRoundedIcon style={{ marginRight: '10px' }} color="error" />
          <Typography variant="caption" color="error">
            <FormattedMessage id="hotel.bookerInfoWarning" />
          </Typography>
        </Row>
        <Row style={{ margin: '18px 0', alignItems: 'flex-start' }}>
          <HotelBookingForm roomsData={roomsData} params={search} book={book} />
          <AsideBound
            isTablet={isTablet}
            style={{ width: 360, marginLeft: isTablet ? 0 : 24 }}
            direction="right"
          >
            <HotelBookingInfoBox roomsData={roomsData} hotelData={hotelData} params={search} />
          </AsideBound>
        </Row>
      </Container>
    </>
  );
};

export default HotelBookingInfoDesktop;
