import { debounce } from 'lodash';
import moment from 'moment';
import { useSnackbar } from 'notistack';
import React from 'react';
import Helmet from 'react-helmet';
import { useIntl } from 'react-intl';
import { useDispatch } from 'react-redux';
import { useLocation } from 'react-router';
import { Action } from 'redux';
import { ThunkDispatch } from 'redux-thunk';
import { API_PATHS } from '../../../../../configs/API';
import { ROUTES } from '../../../../../configs/routes';
import { some, SUCCESS_CODE } from '../../../../../constants';
import { AppState } from '../../../../../redux/reducers';
import { snackbarSetting } from '../../../../common/components/elements';
import LoadingIcon from '../../../../common/components/LoadingIcon';
import { goToAction, goToReplace } from '../../../../common/redux/reducer';
import { fetchThunk } from '../../../../common/redux/thunk';
import {
  defaultHotelFilterParams,
  HotelFilterParams,
  HotelSearchParams,
  parseHotelFilterParams,
  parseHotelSearchParams,
  stringifyHotelSearchAndFilterParams,
} from '../../utils';
import HotelResultDesktop from '../components/HotelResultDesktop';

interface Props {}

const HotelResult: React.FC<Props> = props => {
  const dispatch = useDispatch<ThunkDispatch<AppState, null, Action<string>>>();
  const intl = useIntl();
  const [data, setData] = React.useState<some | undefined>(undefined);
  const [search, setSearch] = React.useState<HotelSearchParams | undefined>(undefined);
  const [filter, setFilter] = React.useState<HotelFilterParams>(defaultHotelFilterParams);
  const [time, setTime] = React.useState(moment());
  const [searchCompleted, setSearchCompleted] = React.useState(0);
  const [openMap, setOpenMap] = React.useState(false);
  const location = useLocation();
  const { enqueueSnackbar, closeSnackbar } = useSnackbar();

  const searchData = React.useCallback(
    debounce(
      async (paramsSearch: HotelSearchParams, filterParams: HotelFilterParams) => {
        setSearchCompleted(0);
        const json = await dispatch(
          fetchThunk(
            API_PATHS.searchHotel,
            'post',
            JSON.stringify({
              adults: paramsSearch.guestInfo.adultCount,
              children: paramsSearch.guestInfo.childCount,
              childrenAges: paramsSearch.guestInfo.childrenAges,
              rooms: paramsSearch.guestInfo.roomCount,
              checkIn: paramsSearch.checkIn,
              checkOut: paramsSearch.checkOut,
              provinceId: paramsSearch.location?.provinceId,
              districtId: paramsSearch.location?.districtId,
              streetId: paramsSearch.location?.streetId,
              filters: {
                subLocationIds: filterParams.subLocations,
                priceMax: filterParams.price[1],
                priceMin: filterParams.price[0],
                stars: filterParams.stars,
                types: filterParams.hotelTypes,
                relaxes: filterParams.facilities,
                services: [],
              },
              page: filterParams.page,
              size: filterParams.pageSize,
              sortBy: filterParams.sortBy,
            }),
          ),
        );
        if (json?.code === SUCCESS_CODE) {
          if (filterParams.page === 1) {
            setData(json.data);
          } else {
            setData(one => ({ ...json.data, hotels: [...one?.hotels, ...json.data.hotels] }));
          }
        }
        setTime(moment());
        setSearchCompleted(100);
      },
      500,
      {
        trailing: true,
        leading: false,
      },
    ),
    [],
  );

  React.useEffect(() => {
    if (search) {
      searchData(search, filter);
    }
  }, [filter, search, searchData]);

  React.useEffect(() => {
    try {
      const searchUrl = new URLSearchParams(location.search);
      const searchParams = parseHotelSearchParams(searchUrl);
      setSearch(searchParams);
      const filterParams = parseHotelFilterParams(searchUrl);
      setFilter(filterParams);
    } catch (err) {
      enqueueSnackbar(
        intl.formatMessage({ id: 'linkValid' }),
        snackbarSetting(key => closeSnackbar(key), { color: 'error' }),
      );
      dispatch(goToAction({ pathname: ROUTES.booking.hotel.default }));
    }
  }, [closeSnackbar, dispatch, enqueueSnackbar, intl, location.search]);

  if (!search) {
    return <LoadingIcon style={{ height: 320 }} />;
  }
  return (
    <>
      <Helmet>
        <title>
          {search
            ? `${search.location?.provinceName}: ${search.checkIn} - ${search.checkOut}`
            : 'Hotel result'}
        </title>
      </Helmet>
      <HotelResultDesktop
        data={data}
        filter={filter}
        setFilter={value => {
          if (value !== filter) {
            window.scrollTo({ top: 0, behavior: 'smooth' });
            const params = stringifyHotelSearchAndFilterParams(search, value);
            dispatch(goToReplace({ search: params }));
          } else if (moment.duration(moment().diff(time)).asMinutes() > 5) {
            searchData(search, filter);
          }
        }}
        setPagination={value => {
          setFilter(value);
        }}
        search={search}
        setSearch={value => {
          window.scrollTo({ top: 0, behavior: 'smooth' });
          const params = stringifyHotelSearchAndFilterParams(value, filter);
          dispatch(goToReplace({ search: params }));
          setData(undefined);
        }}
        searchCompleted={searchCompleted}
        openMap={openMap}
        setOpenMap={value => setOpenMap(value)}
      />
    </>
  );
};

export default HotelResult;
