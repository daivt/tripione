import { Typography } from '@material-ui/core';
import Skeleton from '@material-ui/lab/Skeleton';
import moment from 'moment';
import * as React from 'react';
import { FormattedMessage } from 'react-intl';
import styled from 'styled-components';
import { some } from '../../../../../constants';
import { DATE_FORMAT_BACK_END } from '../../../../../models/moment';
import { Row } from '../../../../common/components/elements';
import { HotelFilterParams, HotelSearchParams } from '../../utils';
import HotelResultSortBox from './HotelResultSortBox';

const Line = styled.div`
  height: 40px;
  display: flex;
  align-items: center;
  justify-content: space-between;
`;

interface HotelResultBoxHeaderProps {
  style?: React.CSSProperties;
  search: HotelSearchParams;
  data?: some;
  filter: HotelFilterParams;
  setFilter(filter: HotelFilterParams): void;
}

const HotelResultBoxHeader: React.FunctionComponent<HotelResultBoxHeaderProps> = props => {
  const { search, style, data, filter, setFilter } = props;

  if (!search) {
    return (
      <div style={style}>
        <Skeleton width="300px" height="25px" />
        <Skeleton width="350px" />
      </div>
    );
  }

  return (
    <div style={style}>
      <Line>
        <Typography variant="h5">
          {search.location?.name || search.location?.provinceName}:&nbsp;
          <FormattedMessage
            id="hotel.header.found"
            values={{ num: data ? data.totalResults : 0 }}
          />
        </Typography>
      </Line>
      <Row>
        <span style={{ flex: 1 }}>
          <Typography component="span" variant="body2" style={{ paddingRight: '8px' }}>
            {moment(search.checkIn, DATE_FORMAT_BACK_END).format('L')}&nbsp;-&nbsp;
            {moment(search.checkOut, DATE_FORMAT_BACK_END).format('L')}
          </Typography>
          <Typography component="span" variant="body2">
            <FormattedMessage
              id="hotel.header.night"
              values={{
                num: moment(search.checkOut, DATE_FORMAT_BACK_END).diff(
                  moment(search.checkIn, DATE_FORMAT_BACK_END),
                  'days',
                ),
              }}
            />
          </Typography>
          <Typography component="span" variant="body2" style={{ padding: '0px 8px' }}>
            <FormattedMessage
              id="hotel.header.adult"
              values={{ num: search.guestInfo.adultCount }}
            />
            {search.guestInfo.childCount > 0 && (
              <>
                ,&nbsp;
                <FormattedMessage
                  id="hotel.header.child"
                  values={{ num: search.guestInfo.childCount }}
                />
              </>
            )}
          </Typography>
          <Typography component="span" variant="body2">
            <FormattedMessage id="hotel.header.room" values={{ num: search.guestInfo.roomCount }} />
          </Typography>
        </span>
        <HotelResultSortBox filter={filter} setFilter={setFilter} />
      </Row>
    </div>
  );
};

export default HotelResultBoxHeader;
