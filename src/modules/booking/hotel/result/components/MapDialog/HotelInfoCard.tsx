/* eslint-disable no-nested-ternary */
import { ButtonBase, Divider, Paper, Typography, withStyles } from '@material-ui/core';
import IconStar from '@material-ui/icons/StarBorderOutlined';
import Rating from '@material-ui/lab/Rating';
import Skeleton from '@material-ui/lab/Skeleton';
import * as React from 'react';
import { FormattedMessage, FormattedNumber } from 'react-intl';
import { BLUE, GREEN, ORANGE, YELLOW } from '../../../../../../configs/colors';
import { some } from '../../../../../../constants';
import defaultImage from '../../../../../../svg/booking/default_image.svg';
import { ReactComponent as IcLocation } from '../../../../../../svg/booking/ic_pin.svg';
import { Row } from '../../../../../common/components/elements';
import ProgressiveImage from '../../../../common/ProgressiveImage';

export const PaperCS = withStyles(theme => ({
  root: {
    color: GREEN,
    '&:hover': {
      color: YELLOW,
    },
  },
}))(Paper);

interface Props {
  hotelData?: some;
  minPrice?: number | undefined;
  bonusPoint?: number | undefined;
}

const HotelInfoCard: React.FunctionComponent<Props> = props => {
  const { hotelData, minPrice, bonusPoint } = props;

  if (!hotelData) {
    return (
      <Paper
        variant="outlined"
        style={{ display: 'flex', width: '100%', height: 150, marginBottom: 8 }}
      >
        <Skeleton variant="rect" style={{ width: '100px', height: '100%' }} />
        <div
          style={{
            display: 'flex',
            flexDirection: 'column',
            flex: 1,
            padding: '12px',
          }}
        >
          <Skeleton width="80%" />
          <Skeleton width="50%" />
        </div>
      </Paper>
    );
  }

  return (
    <PaperCS
      variant="outlined"
      style={{ display: 'flex', width: '100%', marginBottom: 8, overflow: 'hidden' }}
    >
      <ButtonBase style={{ textAlign: 'unset', width: '100%' }}>
        {/* [THIS IS NOT NEEDED, REMOVE THIS HACK AND TEST]
       Need to wrap the image as absolute inside relative like this to make
       sure image won't expand vertically to natural height */}
        <div style={{ width: '100px', position: 'relative', height: '100%' }}>
          <ProgressiveImage
            style={{
              boxShadow: 'rgba(0, 0, 0, 0.14) 1px 0px 2px',
              height: '100%',
              width: '100%',
              top: 0,
              left: 0,
              bottom: 0,
              right: 0,
              objectFit: 'cover',
              position: 'absolute',
            }}
            src={
              hotelData.logo ||
              (hotelData.images
                ? hotelData.images[0].src
                : hotelData.icon
                ? hotelData.icon
                : defaultImage)
            }
            alt=""
          />
        </div>
        <div style={{ padding: '4px 8px ', flex: 1 }}>
          <Typography variant="subtitle2">{hotelData.name}</Typography>
          {hotelData.starNumber && (
            <Rating
              icon={<IconStar style={{ height: '20px' }} />}
              value={hotelData.starNumber}
              readOnly
            />
          )}
          <Divider />
          <Row style={{ height: '24px' }}>
            {hotelData.overallScore && (
              <div
                style={{
                  background: BLUE,
                  borderRadius: '4px',
                  width: '32px',
                  height: '16px',
                  display: 'flex',
                  justifyContent: 'center',
                  alignItems: 'center',
                  marginRight: '12px',
                }}
              >
                <Typography variant="caption" style={{ color: 'white' }}>
                  <FormattedNumber value={hotelData.overallScore / 10} />
                </Typography>
              </div>
            )}
            <Typography
              style={{
                color: BLUE,
                fontSize: '10px',
                lineHeight: '14px',

                fontWeight: 500,
              }}
            >
              <FormattedMessage
                id={
                  hotelData.overallScore > 90
                    ? 'hotel.ratingExcellent'
                    : hotelData.overallScore > 80 && hotelData.overallScore < 90
                    ? 'hotel.ratingVeryGood'
                    : hotelData.overallScore > 70 && hotelData.overallScore < 80
                    ? 'hotel.ratingGood'
                    : hotelData.overallScore > 60 && hotelData.overallScore < 70
                    ? 'hotel.ratingSatisfied'
                    : 'hotel.ratingNormal'
                }
              />
            </Typography>
            {(hotelData.numReviews || hotelData.numberOfReviews) && (
              <>
                &nbsp;/&nbsp;
                <Typography color="textSecondary" style={{ fontSize: '10px', lineHeight: '14px' }}>
                  <FormattedMessage
                    id="hotel.info.ratingSmallNote"
                    values={{ num: hotelData.numReviews || hotelData.numberOfReviews }}
                  />
                </Typography>
              </>
            )}
          </Row>

          {hotelData.address && (
            <div style={{ display: 'flex', alignItems: 'center' }}>
              <IcLocation />
              <Typography
                color="textPrimary"
                style={{ fontSize: '10px', lineHeight: '14px', marginLeft: '8px' }}
              >
                {hotelData.address}
              </Typography>
            </div>
          )}
          {hotelData.distance && (
            <div style={{ display: 'flex' }}>
              <Typography
                color="textPrimary"
                style={{ fontSize: '10px', lineHeight: '14px', minWidth: 70 }}
              >
                <FormattedMessage id="hotel.distance" />
              </Typography>
              <Typography
                color="textPrimary"
                style={{
                  fontSize: '10px',
                  lineHeight: '14px',
                  marginLeft: '8px',
                  textAlign: 'end',
                }}
              >
                <FormattedMessage id="hotel.info.distance" values={{ num: hotelData.distance }} />
              </Typography>
            </div>
          )}
          <Row style={{ justifyContent: 'flex-end' }}>
            <Typography style={{ fontSize: '16px', lineHeight: '24px', color: ORANGE }}>
              {minPrice || hotelData.minPrice > 0 ? (
                <FormattedNumber value={minPrice || hotelData.minPrice} />
              ) : (
                '---'
              )}
              &nbsp;
              <FormattedMessage id="currency" />
            </Typography>
          </Row>
          <Row style={{ justifyContent: 'flex-end', minHeight: '24px' }}>
            <Typography variant="caption" style={{ color: GREEN }}>
              {(bonusPoint && bonusPoint > 0) || hotelData.bonusPoint > 0 ? (
                <FormattedNumber value={bonusPoint || hotelData.bonusPoint} />
              ) : (
                '---'
              )}
              &nbsp;
              <FormattedMessage id="point" />
            </Typography>
          </Row>
        </div>
      </ButtonBase>
    </PaperCS>
  );
};

export default HotelInfoCard;
