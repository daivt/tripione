/* eslint-disable no-nested-ternary */
import { Button, Dialog, IconButton, InputAdornment, Typography } from '@material-ui/core';
import CloseIcon from '@material-ui/icons/Close';
import { useFormik } from 'formik';
import moment, { Moment } from 'moment';
import React from 'react';
import { FormattedMessage, useIntl } from 'react-intl';
import { useDispatch, useSelector } from 'react-redux';
import { Action } from 'redux';
import { ThunkDispatch } from 'redux-thunk';
import * as yup from 'yup';
import { API_PATHS } from '../../../../../../configs/API';
import { DATE_FORMAT_BACK_END } from '../../../../../../models/moment';
import { AppState } from '../../../../../../redux/reducers';
import { ReactComponent as IconHotel } from '../../../../../../svg/booking/ic_hotel.svg';
import { ReactComponent as IconPin } from '../../../../../../svg/booking/ic_pin.svg';
import { ReactComponent as IconCalender } from '../../../../../../svg/ic_calendar.svg';
import DateRangeFormControl from '../../../../../common/components/DateRangeFormControl';
import { Col, Row } from '../../../../../common/components/elements';
import FormControlTextField from '../../../../../common/components/FormControlTextField';
import { setGeneralFlight } from '../../../../../common/redux/reducer';
import { fetchThunk } from '../../../../../common/redux/thunk';
import GuestBox, { getInputStr } from '../../../common/GuestInfoBox/GuestBox';
import { HotelSearchParams, Location } from '../../../utils';

export function renderOption(option: Location) {
  const style: React.CSSProperties = {
    flexShrink: 0,
    margin: '0 15px 0 5px',
    width: '28px',
    height: '28px',
    borderRadius: option.thumbnailUrl ? '50%' : undefined,
  };
  return (
    <div style={{ display: 'flex', alignItems: 'center' }}>
      {option.hotelId !== -1 ? (
        <IconHotel style={style} />
      ) : option.thumbnailUrl ? (
        <img style={style} alt="" src={option.thumbnailUrl} />
      ) : (
        <IconPin style={style} />
      )}

      <div style={{ display: 'flex', flexDirection: 'column', overflow: 'hidden' }}>
        <Typography variant="body2" style={{ padding: '5px' }} noWrap>
          {option.name}
        </Typography>
        {option.hotelId !== -1 && (
          <Typography variant="caption" style={{ padding: '0px 5px 3px' }}>
            {option.provinceName}
          </Typography>
        )}
      </div>
    </div>
  );
}

export const getLabel = (option: Location) => {
  return `${option.name || option.provinceName}`;
};

interface Props {
  params: HotelSearchParams;
  onSearch(params: HotelSearchParams): void;
  showButton?: boolean;
}

const SearchBox: React.FC<Props> = props => {
  const { params, onSearch, showButton } = props;
  const dispatch = useDispatch<ThunkDispatch<AppState, null, Action<string>>>();
  const generalFlight = useSelector((state: AppState) => state.common.generalFlight);
  const [open, setOpen] = React.useState(false);
  const intl = useIntl();
  const date = React.createRef<HTMLDivElement>();

  const flightSearchSchema = yup.object().shape({
    // location: yup
    //   .mixed()
    //   .nullable()
    //   .required(intl.formatMessage({ id: 'required' })),
    checkIn: yup
      .string()
      .trim()
      .required(intl.formatMessage({ id: 'required' })),
    checkOut: yup
      .string()
      .trim()
      .required(intl.formatMessage({ id: 'required' })),
    guestInfo: yup.object().shape({
      childrenAges: yup.array().of(yup.number().min(0, intl.formatMessage({ id: 'required' }))),
    }),
  });

  const formik = useFormik({
    initialValues: params,
    onSubmit: values => {
      onSearch(values);
      setOpen(false);
    },
    validationSchema: flightSearchSchema,
  });

  React.useEffect(() => {
    if (!generalFlight) {
      dispatch(fetchThunk(API_PATHS.flightGeneralInfo, 'get')).then(json =>
        dispatch(setGeneralFlight(json.data)),
      );
    }
  }, [dispatch, generalFlight]);

  React.useEffect(() => {
    formik.setValues(params, true);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [params, open]);

  const handleSubmit = React.useCallback(
    (e?: React.FormEvent<HTMLFormElement> | undefined) => {
      formik.handleSubmit(e);
    },
    [formik],
  );

  React.useEffect(() => {
    if (formik.isSubmitting) {
      if (formik.errors.checkIn || formik.errors.checkOut) {
        date.current?.focus();
      }
    }
  }, [date, formik, formik.isSubmitting]);

  return (
    <>
      <Col style={{ cursor: 'pointer', alignItems: 'center' }} onClick={() => setOpen(true)}>
        <FormControlTextField
          label={intl.formatMessage({ id: 'hotel.chooseLocation' })}
          placeholder={intl.formatMessage({ id: 'hotel.chooseLocation' })}
          readOnly
          value={`${params.checkIn} - ${params.checkOut}`}
          formControlStyle={{ margin: 0 }}
          optional
          startAdornment={
            <InputAdornment position="start" style={{ marginLeft: 8 }}>
              <IconButton size="small" edge="start">
                <IconCalender />
              </IconButton>
            </InputAdornment>
          }
        />
        <FormControlTextField
          label={intl.formatMessage({ id: 'hotel.stayDuration' })}
          placeholder={intl.formatMessage({ id: 'hotel.chooseTime' })}
          readOnly
          value={getInputStr(params.guestInfo, intl)}
          formControlStyle={{ margin: 0 }}
          optional
          startAdornment={
            <InputAdornment position="start" style={{ marginLeft: 8 }}>
              <IconButton size="small" edge="start">
                <IconCalender />
              </IconButton>
            </InputAdornment>
          }
        />
        {showButton && (
          <Button
            variant="contained"
            color="secondary"
            size="large"
            disableElevation
            style={{ width: 220 }}
          >
            <FormattedMessage id="change" />
          </Button>
        )}
      </Col>
      <Dialog
        open={open}
        PaperProps={{
          style: {
            padding: 16,
            overflow: 'initial',
            width: 570,
          },
        }}
        scroll="body"
        maxWidth="lg"
      >
        <Row>
          <Typography variant="h6" style={{ flex: 1 }}>
            <FormattedMessage id="hotel.changeParams" />
          </Typography>
          <IconButton color="default" size="small" onClick={() => setOpen(false)}>
            <CloseIcon />
          </IconButton>
        </Row>
        <form autoComplete="off" onSubmit={handleSubmit}>
          <Col>
            <DateRangeFormControl
              iRef={date}
              label={intl.formatMessage({ id: 'hotel.stayDuration' })}
              placeholder={intl.formatMessage({ id: 'hotel.chooseTime' })}
              style={{ marginRight: 0 }}
              optional
              startDate={
                formik.values.checkIn
                  ? moment(formik.values.checkIn, DATE_FORMAT_BACK_END)
                  : undefined
              }
              endDate={
                formik.values.checkOut
                  ? moment(formik.values.checkOut, DATE_FORMAT_BACK_END)
                  : undefined
              }
              onChange={(start?: Moment, end?: Moment) => {
                formik.setFieldValue(
                  'checkIn',
                  start ? start.format(DATE_FORMAT_BACK_END) : undefined,
                );
                formik.setFieldValue(
                  'checkOut',
                  end ? end.format(DATE_FORMAT_BACK_END) : undefined,
                );
              }}
              numberOfMonths={2}
              errorMessage={
                formik.touched.checkIn || formik.touched.checkOut
                  ? formik.errors.checkIn || formik.errors.checkOut
                  : undefined
              }
              startAdornment
              isOutsideRange={(e: any) =>
                moment()
                  .startOf('day')
                  .isAfter(e)
              }
              direction="center"
            />

            <GuestBox
              onChange={(info, noValid) => {
                formik.setFieldValue('guestInfo', info, !noValid);
              }}
              guestInfo={formik.values.guestInfo}
              errorMessage={
                formik.touched.guestInfo?.childrenAges
                  ? formik.errors.guestInfo?.childrenAges
                  : undefined
              }
            />
            <Row style={{ justifyContent: 'center', marginTop: 16 }}>
              <Button
                variant="outlined"
                style={{ marginRight: 16, minWidth: 160 }}
                size="large"
                disableElevation
                onClick={() => setOpen(false)}
              >
                <FormattedMessage id="back" />
              </Button>
              <Button
                // loading={loading}
                variant="contained"
                color="secondary"
                style={{ marginRight: 16, minWidth: 160 }}
                size="large"
                type="submit"
                disableElevation
              >
                <FormattedMessage id="change" />
              </Button>
            </Row>
          </Col>
        </form>
      </Dialog>
    </>
  );
};

export default SearchBox;
