import { Button, Typography } from '@material-ui/core';
import * as React from 'react';
import { FormattedMessage } from 'react-intl';
import { BLUE } from '../../../../../configs/colors';
import { ROUTES } from '../../../../../configs/routes';
import { some } from '../../../../../constants';
import Link from '../../../../common/components/Link';
import { PAGE_SIZE_20 } from '../../../constants';
import { HOTEL_ACTIVITY_DIRECT_PARAM, HOTEL_BOOK_PARAMS_NAMES } from '../../constants';
import {
  HotelFilterParams,
  HotelSearchParams,
  stringifyHotelSearchAndFilterParams,
} from '../../utils';
import HotelResultItem from './HotelResultItem';

export interface Props {
  data?: some;
  search: HotelSearchParams;
  filter: HotelFilterParams;
  setPagination(filter: HotelFilterParams): void;
  searchCompleted: number;
}

const HotelResultBox: React.FC<Props> = props => {
  const { data, searchCompleted, filter, search, setPagination: setFilter } = props;
  const hotels = data ? (data.hotels as some[]) : null;
  return (
    <div style={{ marginTop: '15px' }}>
      {hotels ? (
        hotels.map(hotel => (
          <Link
            key={hotel.id}
            to={{
              pathname: ROUTES.booking.hotel.detail,
              search: `${stringifyHotelSearchAndFilterParams(search)}&${
                HOTEL_BOOK_PARAMS_NAMES.hotelId
              }=${hotel.id}&${HOTEL_ACTIVITY_DIRECT_PARAM}=${false}`,
            }}
            style={{ color: 'inherit' }}
          >
            <HotelResultItem hotel={hotel} />
          </Link>
        ))
      ) : (
        <>
          <HotelResultItem />
          <HotelResultItem />
          <HotelResultItem />
          <HotelResultItem />
          <HotelResultItem />
        </>
      )}
      {searchCompleted < 100 && <HotelResultItem />}
      {data && data.totalResults > filter.page * PAGE_SIZE_20 && searchCompleted === 100 && (
        <div style={{ textAlign: 'center', marginTop: '23px' }}>
          <Button
            onClick={() => {
              setFilter({ ...filter, page: filter.page + 1 });
            }}
          >
            <Typography style={{ color: BLUE }}>
              <FormattedMessage
                id="displayMore"
                values={{ num: data.totalResults - PAGE_SIZE_20 * filter.page }}
              />
            </Typography>
          </Button>
        </div>
      )}
    </div>
  );
};
export default HotelResultBox;
