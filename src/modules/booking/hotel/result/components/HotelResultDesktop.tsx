import { Container, LinearProgress, useMediaQuery, useTheme } from '@material-ui/core';
import * as React from 'react';
import { some } from '../../../../../constants';
import AsideBound from '../../../common/AsideBound';
import HotelSearch from '../../common/HotelSearch';
import { HotelFilterParams, HotelSearchParams } from '../../utils';
import HotelResultBox from './HotelResultBox';
import HotelResultBoxHeader from './HotelResultBoxHeader';
import HotelResultFilter from './HotelResultFilter';
import MapButtonBox from './MapButtonBox';
import MapDialog from './MapDialog';

export interface Props {
  data?: some;
  filter: HotelFilterParams;
  setFilter(filter: HotelFilterParams): void;
  setPagination(filter: HotelFilterParams): void;
  search: HotelSearchParams;
  setSearch(search: HotelSearchParams): void;
  searchCompleted: number;
  openMap: boolean;
  setOpenMap(value: boolean): void;
}

const HotelResultDesktop: React.FC<Props> = props => {
  const {
    data,
    filter,
    setFilter,
    search,
    setSearch,
    searchCompleted,
    setPagination,
    openMap,
    setOpenMap,
  } = props;
  const theme = useTheme();
  const isTablet = useMediaQuery(theme.breakpoints.down('md'));

  return (
    <>
      <LinearProgress variant="determinate" value={searchCompleted} color="secondary" />
      <HotelSearch params={search} onSearch={setSearch} />
      <Container style={{ flex: 1, padding: 0, display: 'flex' }}>
        <AsideBound
          isTablet={isTablet}
          style={{ width: isTablet ? 320 : 270, padding: isTablet ? 0 : '24px 0px' }}
        >
          <HotelResultFilter filterParams={filter} setFilter={setFilter} data={data} />
          <MapButtonBox setOpenMap={setOpenMap} />
        </AsideBound>
        <div style={{ flex: 1, minHeight: '400px', padding: 24 }}>
          <HotelResultBoxHeader
            data={data}
            search={search}
            style={{ flex: 1 }}
            filter={filter}
            setFilter={setFilter}
          />
          <HotelResultBox
            data={data}
            search={search}
            searchCompleted={searchCompleted}
            filter={filter}
            setPagination={setPagination}
          />
        </div>
      </Container>
      <MapDialog
        open={openMap}
        onClose={() => setOpenMap(false)}
        filterTmp={filter}
        setFilterTmp={setFilter}
        searchTmp={search}
      />
    </>
  );
};

export default HotelResultDesktop;
