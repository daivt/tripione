import { Button, Typography } from '@material-ui/core';
import * as React from 'react';
import { FormattedMessage } from 'react-intl';
import { ReactComponent as PreviewMap } from '../../../../../svg/booking/previewMap.svg';

interface Props {
  setOpenMap(value: boolean): void;
}

const MapButtonBox: React.FunctionComponent<Props> = props => {
  const { setOpenMap } = props;
  return (
    <div style={{ position: 'relative' }}>
      <Button
        variant="outlined"
        size="large"
        style={{
          width: 220,
          background: 'white',
          borderRadius: '4px',
          position: 'absolute',
          bottom: 20,
          left: 0,
          right: 0,
          marginRight: 'auto',
          marginLeft: 'auto',
        }}
        onClick={() => {
          setOpenMap(true);
        }}
      >
        <Typography variant="body2" color="secondary">
          <FormattedMessage id="hotel.filter.viewMap" />
        </Typography>
      </Button>
      <PreviewMap style={{ width: '100%' }} />
    </div>
  );
};

export default MapButtonBox;
