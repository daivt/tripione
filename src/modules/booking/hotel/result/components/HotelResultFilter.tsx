import { Button, Slider, Tooltip } from '@material-ui/core';
import Checkbox from '@material-ui/core/Checkbox';
import Collapse from '@material-ui/core/Collapse';
import Divider from '@material-ui/core/Divider';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Typography from '@material-ui/core/Typography';
import IconStar from '@material-ui/icons/StarRate';
import StarRoundedIcon from '@material-ui/icons/StarRounded';
import { remove } from 'lodash';
import * as React from 'react';
import { FormattedMessage, FormattedNumber, useIntl } from 'react-intl';
import styled from 'styled-components';
import { GREY_500, YELLOW } from '../../../../../configs/colors';
import { some } from '../../../../../constants';
import { Col } from '../../../../common/components/elements';
import { ArrowStyle, DropDownHeader, DropDownStyle } from '../../../common/element';
import { FACILITIES, FILTER_MAX_PRICE, HOTEL_TYPES, PRICE_STEP } from '../../constants';
import { HotelFilterParams } from '../../utils';

export const ButtonCS = styled(Button)<{ selected: boolean }>`
  &&& {
    background-color: ${props => (props.selected ? YELLOW : undefined)};
    box-shadow: none;
    text-transform: none;
    color: ${props => (props.selected ? '#fff' : GREY_500)};
    border-radius: 16px;
    max-height: 32px;
    padding: 0px 4px;
    min-width: 48px;
    margin-top: 8px;
  }
`;

interface PropsLabel {
  children: React.ReactElement;
  open: boolean;
  value: number;
}

function PriceLabelComponent(props: PropsLabel) {
  const { children, open, value } = props;

  return (
    <Tooltip
      open={open}
      enterTouchDelay={0}
      placement="top"
      title={
        <>
          <FormattedNumber value={value} />
          &nbsp;
          <FormattedMessage id="currency" />
        </>
      }
    >
      {children}
    </Tooltip>
  );
}

export interface Props {
  data?: some;
  filterParams: HotelFilterParams;
  setFilter(filter: HotelFilterParams): void;
}

export function renderStar(starNumber: number) {
  const starElement = [];

  for (let i = 0; i < starNumber; i += 1) {
    starElement.push(<IconStar style={{ padding: '0', borderRadius: '16px' }} key={i} />);
  }

  return starElement;
}

const HotelResultFilter: React.FC<Props> = props => {
  const { filterParams, setFilter, data } = props;
  const intl = useIntl();
  const [location, setLocation] = React.useState(false);
  const [hotelType, setType] = React.useState(false);
  const [facility, setFacilityState] = React.useState(false);

  const setPrice = React.useCallback(
    (value: number[]) => {
      const filter: HotelFilterParams = { ...filterParams, price: value };
      setFilter(filter);
    },
    [filterParams, setFilter],
  );

  const setStar = React.useCallback(
    (starId: number) => {
      const stars = [...filterParams.stars];
      remove(stars, id => id === starId);

      if (stars.length === filterParams.stars.length) {
        stars.push(starId);
      }
      setFilter({ ...filterParams, stars });
    },
    [filterParams, setFilter],
  );

  const setHotelType = React.useCallback(
    (event: React.ChangeEvent<HTMLInputElement>, value: some) => {
      const hotelTypes = [...filterParams.hotelTypes];
      if (event.target.checked) {
        hotelTypes.push(value.id);
      } else {
        remove(hotelTypes, id => id === value.id);
      }

      setFilter({ ...filterParams, hotelTypes });
    },
    [filterParams, setFilter],
  );

  const setFacility = React.useCallback(
    (event: React.ChangeEvent<HTMLInputElement>, value: some) => {
      const facilities = [...filterParams.facilities];

      if (event.target.checked) {
        facilities.push(value.id);
      } else {
        remove(facilities, id => id === value.id);
      }

      setFilter({ ...filterParams, facilities });
    },
    [filterParams, setFilter],
  );

  const setSubLocation = React.useCallback(
    (event: React.ChangeEvent<HTMLInputElement>, locate: some) => {
      const subLocations = [...filterParams.subLocations];

      if (event.target.checked) {
        subLocations.push(...locate.ids);
      } else {
        remove(subLocations, ids => locate.ids.indexOf(ids) !== -1);
      }

      setFilter({ ...filterParams, subLocations });
    },
    [filterParams, setFilter],
  );

  const subLocations = React.useMemo(() => {
    return data?.filters ? data.filters?.subLocations : [];
  }, [data]);

  return (
    <div>
      <Col>
        <Typography variant="subtitle2" style={{ paddingLeft: '14px' }}>
          <FormattedMessage id="hotel.filter.selectResult" />
        </Typography>
        <div
          style={{
            display: 'flex',
            justifyContent: 'space-between',
            paddingTop: '16px',
            paddingLeft: '14px',
          }}
        >
          <Typography variant="body2">
            <FormattedMessage id="hotel.filter.roomPrice" />
          </Typography>
          <Typography variant="body2" color="textSecondary">
            <FormattedNumber value={filterParams.price[0]} />
            &nbsp;
            <FormattedMessage id="currency" />
            &nbsp;-&nbsp;
            <FormattedNumber value={filterParams.price[1]} />
            &nbsp;
            <FormattedMessage id="currency" />
          </Typography>
        </div>
        <div style={{ margin: '12px 12px 0 12px' }}>
          <Slider
            key={JSON.stringify(filterParams.price)}
            defaultValue={filterParams.price.slice()}
            min={0}
            step={PRICE_STEP}
            max={FILTER_MAX_PRICE}
            onChangeCommitted={(e: any, value: any) => setPrice(value)}
            ValueLabelComponent={PriceLabelComponent}
          />
        </div>
      </Col>
      <Col style={{ paddingTop: '16px', paddingBottom: '16px' }}>
        <Typography variant="subtitle2" style={{ paddingBottom: '6px', paddingLeft: '14px' }}>
          <FormattedMessage id="hotel.filter.starRate" />
        </Typography>
        <div style={{ display: 'flex', flexWrap: 'wrap', justifyContent: 'space-between' }}>
          {Array(5)
            .fill(0)
            .map((v, i) => (
              <ButtonCS
                size="small"
                variant="outlined"
                selected={filterParams.stars.indexOf(i + 1) !== -1}
                key={i}
                onClick={() => setStar(i + 1)}
              >
                {i + 1}&nbsp;
                <StarRoundedIcon fontSize="small" />
              </ButtonCS>
            ))}
        </div>
      </Col>
      <Col>
        <DropDownHeader onClick={() => setLocation(!location)}>
          <Typography variant="body1" style={DropDownStyle}>
            <FormattedMessage id="hotel.filter.area" />
            <ArrowStyle active={location}>&#9660;</ArrowStyle>
          </Typography>
          <Divider />
        </DropDownHeader>
        <Collapse in={location}>
          {subLocations.map((item: some) => (
            <div key={item.name}>
              <FormControlLabel
                style={{ outline: 'none', margin: '0', color: GREY_500 }}
                label={
                  <Typography variant="body2">
                    {item.name}
                    &nbsp;&#40;
                    <FormattedNumber value={item.numHotels} />
                    &#41;
                  </Typography>
                }
                labelPlacement="end"
                control={
                  <Checkbox
                    onChange={event => setSubLocation(event, item)}
                    value={item.id}
                    color="primary"
                    checked={filterParams.subLocations.indexOf(item.ids[0]) !== -1}
                  />
                }
              />
            </div>
          ))}
        </Collapse>
      </Col>
      <Col>
        <DropDownHeader onClick={() => setType(!hotelType)}>
          <Typography variant="body1" style={DropDownStyle}>
            <FormattedMessage id="hotel.filter.hotelType" />
            <ArrowStyle active={hotelType}>&#9660;</ArrowStyle>
          </Typography>
          <Divider />
        </DropDownHeader>
        <Collapse in={hotelType}>
          {HOTEL_TYPES.map((item: some) => (
            <div key={item.id}>
              <FormControlLabel
                style={{ outline: 'none', margin: '0', color: GREY_500 }}
                label={
                  <Typography variant="body2">{intl.formatMessage({ id: item.name })}</Typography>
                }
                labelPlacement="end"
                control={
                  <Checkbox
                    onChange={event => setHotelType(event, item)}
                    value={item.id}
                    color="primary"
                    checked={filterParams.hotelTypes.indexOf(item.id) !== -1}
                  />
                }
              />
            </div>
          ))}
        </Collapse>
      </Col>
      <Col>
        <DropDownHeader onClick={() => setFacilityState(!facility)}>
          <Typography variant="body1" style={DropDownStyle}>
            <FormattedMessage id="hotel.filter.facility" />
            <ArrowStyle active={facility}>&#9660;</ArrowStyle>
          </Typography>
          <Divider />
        </DropDownHeader>
        <Collapse in={facility}>
          {FACILITIES.map((item: some) => (
            <div key={item.id}>
              <FormControlLabel
                style={{ outline: 'none', margin: '0', color: GREY_500 }}
                label={
                  <Typography variant="body2">{intl.formatMessage({ id: item.name })}</Typography>
                }
                labelPlacement="end"
                control={
                  <Checkbox
                    onChange={event => setFacility(event, item)}
                    value={item.id}
                    color="primary"
                    checked={filterParams.facilities.indexOf(item.id) !== -1}
                  />
                }
              />
            </div>
          ))}
        </Collapse>
      </Col>
    </div>
  );
};

// function mapState2Props(state: AppState) {
//   return {
//     filter: state.result.hotel.filterParams,
//     subLocations: state.result.hotel.data ? state.result.hotel.data.filters.subLocations : [],
//   };
// }

export default HotelResultFilter;
