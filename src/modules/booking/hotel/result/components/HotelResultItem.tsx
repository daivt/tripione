import { Paper, Typography } from '@material-ui/core';
import { withStyles } from '@material-ui/core/styles';
import StarRoundedIcon from '@material-ui/icons/StarRounded';
import Rating from '@material-ui/lab/Rating';
import Skeleton from '@material-ui/lab/Skeleton';
import * as React from 'react';
import { FormattedMessage, FormattedNumber } from 'react-intl';
import {
  BLUE,
  BLUE_300,
  GREEN,
  GREY,
  GREY_50,
  GREY_700,
  RED,
  SECONDARY,
} from '../../../../../configs/colors';
import { some } from '../../../../../constants';
import { ReactComponent as IconCoinSvg } from '../../../../../svg/booking/ic_coin.svg';
import { ReactComponent as IconDiscount } from '../../../../../svg/booking/ic_discount.svg';
import { ReactComponent as IconHandMoney } from '../../../../../svg/booking/ic_hotel_result_hand_money.svg';
import { ReactComponent as IconPromo } from '../../../../../svg/booking/ic_hotel_result_promo.svg';
import { ReactComponent as IconLocation } from '../../../../../svg/booking/ic_pin.svg';
import { Col, Row } from '../../../../common/components/elements';
import ProgressiveImage from '../../../common/ProgressiveImage';
import { getScoreDescription } from '../../../utils';

export const Wrapper = withStyles(theme => ({
  root: {
    '&:hover': {
      background: GREY_50,
    },
    minHeight: 210,
    overflow: 'hidden',
    display: 'flex',
    justifyContent: 'flex-start',
    textAlign: 'start',
    marginTop: 16,
  },
}))(Paper);

export interface Props {
  hotel?: some;
}

const HotelResultItem: React.FunctionComponent<Props> = props => {
  const { hotel } = props;

  if (!hotel) {
    return (
      <Wrapper variant="outlined">
        <div style={{ position: 'relative', width: '270px' }}>
          <Skeleton
            variant="rect"
            style={{ height: '100%', width: '100%', objectFit: 'cover', position: 'absolute' }}
          />
        </div>
        <Col
          style={{
            flex: 1,
            padding: '12px',
          }}
        >
          <Skeleton width="80%" height={32} />
          <Skeleton width="60%" height={24} />
          <Skeleton width="50%" />
          <Col
            style={{
              alignItems: 'flex-end',
              marginTop: 16,
            }}
          >
            <Skeleton width="30%" height={32} />
            <Skeleton width="35%" height={24} />
          </Col>{' '}
          <Col
            style={{
              alignItems: 'flex-end',
              marginTop: 16,
            }}
          >
            <Skeleton width="50%" />
          </Col>
        </Col>
      </Wrapper>
    );
  }

  const { bestAgencyPrices } = hotel;
  const buySignal =
    bestAgencyPrices && bestAgencyPrices.length ? bestAgencyPrices[0].buySignal : undefined;
  const promotionInfo =
    bestAgencyPrices && bestAgencyPrices.length ? bestAgencyPrices[0].promotionInfo : undefined;

  return (
    <Wrapper variant="outlined">
      <div style={{ position: 'relative', width: '270px' }}>
        <ProgressiveImage
          style={{ height: '100%', width: '100%', objectFit: 'cover', position: 'absolute' }}
          src={hotel.logo}
          alt=""
        />
      </div>
      <div
        style={{
          display: 'flex',
          flexDirection: 'column',
          flex: 1,
          paddingRight: '12px',
        }}
      >
        <Row
          style={{
            flex: 1,
            alignItems: 'flex-start',
            padding: '8px 12px 12px 16px',
            flexWrap: 'wrap',
          }}
        >
          <Col
            style={{
              flex: 1,
            }}
          >
            <Row style={{ marginBottom: 8, alignItems: 'flex-start' }}>
              <Typography variant="h6" style={{ marginRight: 16, fontWeight: 500 }}>
                {hotel.name}
              </Typography>
              <Rating value={hotel.starNumber} readOnly size="small" icon={<StarRoundedIcon />} />
            </Row>
            <Row style={{ marginBottom: 8 }}>
              <div
                style={{
                  minWidth: '32px',
                  backgroundColor: BLUE,
                  display: 'flex',
                  justifyContent: 'center',
                  alignItems: 'center',
                  borderRadius: '4px',
                  marginRight: 8,
                }}
              >
                <Typography variant="subtitle1" style={{ color: '#fff' }}>
                  <FormattedNumber value={hotel.overallScore / 10} />
                </Typography>
              </div>

              <Typography variant="subtitle2" style={{ color: BLUE, fontWeight: 500 }}>
                <FormattedMessage id={getScoreDescription(hotel.overallScore / 10)} />
                &nbsp;
                <Typography
                  variant="caption"
                  color="textSecondary"
                  style={{ lineHeight: '14px' }}
                  component="span"
                >
                  (<FormattedNumber value={hotel.numberOfReviews} maximumFractionDigits={0} />
                  &nbsp;
                  <FormattedMessage id="hotel.review" />)
                </Typography>
              </Typography>
            </Row>
            <Row style={{ marginBottom: 8 }}>
              <IconLocation style={{ marginRight: 8 }} />
              <Typography variant="body2" color="textSecondary">
                {hotel.address}
              </Typography>
            </Row>
            {promotionInfo && !!promotionInfo.discountPercentage && (
              <Row style={{ marginBottom: 8 }}>
                <IconDiscount style={{ marginRight: 8 }} />
                <Typography variant="caption" style={{ color: GREEN }}>
                  <FormattedMessage
                    id="booking.saveAmount"
                    values={{ amount: promotionInfo.discountPercentage }}
                  />
                </Typography>
              </Row>
            )}
            {buySignal && buySignal.point && !(promotionInfo.discountPercentage > 2) && (
              <Row style={{ marginBottom: 8 }}>
                <IconPromo style={{ height: '28px', width: 'auto' }} />
                <Typography variant="body2" style={{ paddingLeft: '6px', color: GREY_700 }}>
                  <FormattedMessage
                    id="hotel.usePoint"
                    values={{
                      point: <FormattedNumber value={buySignal.point} />,
                      text: (
                        <span style={{ color: RED }}>
                          <FormattedMessage id="hotel.receiveDiscount" />
                          &nbsp;
                          <FormattedNumber value={buySignal.pointValue || 0} />
                          &nbsp;
                          <FormattedMessage id="currency" />
                        </span>
                      ),
                    }}
                  />
                </Typography>
              </Row>
            )}
          </Col>
          <Col
            style={{
              flexShrink: 0,
              alignSelf: 'flex-end',
            }}
          >
            {buySignal &&
            buySignal.point &&
            promotionInfo?.priceBeforePromotion < hotel?.minPrice ? (
              <>
                <div style={{ display: 'flex', justifyContent: 'flex-end' }}>
                  <Typography
                    variant="body1"
                    style={{ textDecoration: 'line-through', color: GREY }}
                  >
                    <FormattedNumber value={hotel.minPrice} />
                    &nbsp;
                    <FormattedMessage id="currency" />
                  </Typography>
                </div>
                <div
                  style={{
                    display: 'flex',
                    alignItems: 'center',
                    justifyContent: 'flex-end',
                    padding: '4px 0',
                  }}
                >
                  <Typography variant="h5" style={{ color: SECONDARY, display: 'flex' }}>
                    <FormattedNumber value={buySignal.price || 0} />
                    &nbsp;
                    <FormattedMessage id="currency" />
                  </Typography>
                  <Typography
                    variant="body1"
                    style={{ color: GREY_700, paddingLeft: '4px', display: 'flex' }}
                  >
                    + <FormattedNumber value={buySignal.point} />
                    &nbsp;
                    <FormattedMessage id="point" />
                  </Typography>
                </div>
              </>
            ) : (
              <>
                {hotel.minPrice < promotionInfo?.priceBeforePromotion && (
                  <Row style={{ justifyContent: 'flex-end' }}>
                    <Typography
                      variant="body1"
                      style={{ textDecoration: 'line-through', color: GREY }}
                    >
                      <FormattedNumber value={promotionInfo.priceBeforePromotion} />
                      &nbsp;
                      <FormattedMessage id="currency" />
                    </Typography>
                  </Row>
                )}
                <Row
                  style={{
                    justifyContent: 'flex-end',
                    padding: '4px 0',
                  }}
                >
                  <Typography variant="h5" className="price">
                    <FormattedNumber value={hotel.minPrice || 0} />
                    &nbsp;
                    <FormattedMessage id="currency" />
                  </Typography>
                </Row>
              </>
            )}

            {hotel.bonusPoint > 0 && (
              <Row style={{ justifyContent: 'flex-end' }}>
                <IconCoinSvg style={{ marginRight: '10px' }} />
                <Typography variant="body2" className="point">
                  <FormattedNumber value={hotel.bonusPoint} />
                  &nbsp;
                  <FormattedMessage id="point" />
                </Typography>
              </Row>
            )}
          </Col>
        </Row>

        <Row
          style={{
            color: BLUE_300,
            justifyContent: 'flex-end',
            padding: '8px 0',
          }}
        >
          <IconHandMoney />
          <Typography variant="caption" style={{ paddingLeft: '4px' }}>
            <FormattedMessage id="hotel.includeVAT" />
          </Typography>
        </Row>
      </div>
    </Wrapper>
  );
};

export default HotelResultItem;
