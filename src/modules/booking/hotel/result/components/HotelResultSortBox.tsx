import { Typography } from '@material-ui/core';
import * as React from 'react';
import { FormattedMessage, useIntl } from 'react-intl';
import { some } from '../../../../../constants';
import { Row } from '../../../../common/components/elements';
import SingleSelect from '../../../../common/components/SingleSelect';
import { HOTEL_SORT_BY_NAMES } from '../../constants';
import { HotelFilterParams } from '../../utils';

export const HOTEL_SORT_BY = [
  {
    text: 'hotel.filter.sortBy.default',
    id: HOTEL_SORT_BY_NAMES.default,
  },
  {
    text: 'hotel.filter.sortBy.cheap',
    id: HOTEL_SORT_BY_NAMES.cheap,
  },
  {
    text: 'hotel.filter.sortBy.expensive',
    id: HOTEL_SORT_BY_NAMES.expensive,
  },
];

interface HotelResultSortBoxProps {
  filter: HotelFilterParams;
  setFilter(filter: HotelFilterParams): void;
}

const HotelResultSortBox: React.FunctionComponent<HotelResultSortBoxProps> = props => {
  const { filter, setFilter } = props;
  const intl = useIntl();
  return (
    <>
      <Row>
        <Typography variant="body2" color="textSecondary" style={{ marginRight: 16 }}>
          <FormattedMessage id="hotel.filter.sortBy" />:
        </Typography>
        <SingleSelect
          id="Hotel_listing.Hotel_Filter"
          value={filter.sortBy}
          formControlStyle={{ width: 180, margin: 0 }}
          onSelectOption={(value: any | null) => {
            setFilter({ ...filter, sortBy: value as string });
          }}
          getOptionLabel={(v: some) => intl.formatMessage({ id: v.text })}
          options={HOTEL_SORT_BY}
          optional
          disabledHelper
        />
      </Row>
    </>
  );
};

export default HotelResultSortBox;
