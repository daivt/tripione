import { HotelFilterParams } from './utils';
import { defaultPaginationFilter } from '../../../models/pagination';

export const PRICE_STEP = 10000;

export const HOTEL_SEARCH_PARAM_NAMES = {
  location: 'location',
  checkIn: 'checkIn',
  checkOut: 'checkOut',
  guestInfo: 'guestInfo',
  mapParams: 'mapParams',
};

export const FILTER_MAX_PRICE = 30000000;

export const HOTEL_SORT_BY_NAMES = {
  default: 'default',
  expensive: 'price-',
  cheap: 'price+',
  starDesc: 'star-',
  starAsc: 'star+',
};

export const DEFAULT_HOTEL_FILTER_PARAMS: HotelFilterParams = {
  price: [0, FILTER_MAX_PRICE],
  stars: [],
  subLocations: [],
  hotelTypes: [],
  facilities: [],
  sortBy: HOTEL_SORT_BY_NAMES.default,
  ...defaultPaginationFilter,
};

export const HOTEL_FILTER_PARAM_NAMES = {
  price: 'price',
  star: 'star',
  area: 'area',
  type: 'type',
  facility: 'facility',
  sortBy: 'sortBy',
};

export const MAX_CHILDREN_AGE = 12;

export const HOTEL_BOOK_PARAMS_NAMES = {
  hotelId: 'hotelId',
  shrui: 'shrui',
};
export const HOTEL_ACTIVITY_DIRECT_PARAM = 'direct';

export const HOTEL_TYPES = [
  {
    id: 1,
    name: 'hotel.filter.hotelType.hotel',
  },
  {
    id: 2,
    name: 'hotel.filter.hotelType.resort',
  },
  {
    id: 3,
    name: 'hotel.filter.hotelType.motel',
  },
  {
    id: 4,
    name: 'hotel.filter.hotelType.villa',
  },
  {
    id: 5,
    name: 'hotel.filter.hotelType.yacht',
  },
];

export const FACILITIES = [
  {
    id: 1,
    name: 'hotel.filter.facility.outdoorPool',
    icon: '',
  },
  {
    id: 2,
    name: 'hotel.filter.facility.indoorPool',
    icon: '',
  },
  {
    id: 3,
    name: 'hotel.filter.facility.gym',
    icon: '',
  },
  {
    id: 4,
    name: 'hotel.filter.facility.steamBath',
    icon: '',
  },
  {
    id: 5,
    name: 'hotel.filter.facility.spa',
    icon: '',
  },
  {
    id: 6,
    name: 'hotel.filter.facility.massage',
    icon: '',
  },
];
