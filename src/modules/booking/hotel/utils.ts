/* eslint-disable no-nested-ternary */
import { remove, uniq } from 'lodash';
import moment from 'moment';
import { some } from '../../../constants';
import { DATE_FORMAT_BACK_END } from '../../../models/moment';
import { PaginationFilter } from '../../../models/pagination';
import { HOTEL_SEARCH_HISTORY, MAX_SEARCH_HISTORY, PAGE_SIZE_20 } from '../constants';
import {
  FILTER_MAX_PRICE,
  HOTEL_ACTIVITY_DIRECT_PARAM,
  HOTEL_BOOK_PARAMS_NAMES,
  HOTEL_FILTER_PARAM_NAMES,
  HOTEL_SEARCH_PARAM_NAMES,
  HOTEL_SORT_BY_NAMES,
} from './constants';

export interface Location {
  hotelId: number;
  provinceId: number;
  provinceName: string;
  streetId: number;
  streetName: string;
  districtId: number;
  districtName: string;
  slug: string;
  numHotels: number;
  name: string;
  latitude: number;
  longitude: number;
  thumbnailUrl: string | null;
}

export interface GuestCountInfo {
  roomCount: number;
  adultCount: number;
  childCount: number;
  childrenAges: number[];
}
export const defaultGuestCountInfo: GuestCountInfo = {
  roomCount: 1,
  adultCount: 2,
  childCount: 0,
  childrenAges: [],
};
export interface HotelSearchParams {
  location: Location | null;
  checkIn: string; // DATE_FORMAT_BACK_END
  checkOut: string;
  guestInfo: GuestCountInfo;
}

export const defaultHotelSearchParams: HotelSearchParams = {
  location: null,
  checkIn: moment()
    .startOf('day')
    .format(DATE_FORMAT_BACK_END),
  checkOut: moment()
    .add(1, 'days')
    .startOf('day')
    .format(DATE_FORMAT_BACK_END),
  guestInfo: defaultGuestCountInfo,
};

export interface HotelFilterParams extends PaginationFilter {
  price: number[];
  stars: number[];
  subLocations: some[];
  hotelTypes: number[];
  facilities: string[];
  page: number;
  pageSize: number;
  sortBy: string;
}

export const defaultHotelFilterParams: HotelFilterParams = {
  price: [0, FILTER_MAX_PRICE],
  stars: [],
  subLocations: [],
  hotelTypes: [],
  facilities: [],
  sortBy: HOTEL_SORT_BY_NAMES.default,
  page: 1,
  pageSize: PAGE_SIZE_20,
};
export interface HotelBookingParams extends HotelSearchParams {
  hotelId: number;
  shrui?: string;
  direct?: boolean;
}
export function isValidGuestInfo(info: GuestCountInfo) {
  if (info.roomCount > 0 && info.adultCount > 0 && !info.childrenAges.includes(-1)) {
    return true;
  }
  return false;
}

export interface MapParams {
  latitude: number;
  longitude: number;
  radius: number;
}

export function stringifyHotelSearchAndFilterParams(
  search: HotelSearchParams,
  filter?: HotelFilterParams,
  mapParams?: MapParams,
  skipLocation = false,
) {
  const arr = [];
  if (!skipLocation) {
    arr.push(
      `${HOTEL_SEARCH_PARAM_NAMES.location}=${encodeURIComponent(JSON.stringify(search.location))}`,
    );
  }
  arr.push(`${HOTEL_SEARCH_PARAM_NAMES.checkIn}=${search.checkIn}`);
  arr.push(`${HOTEL_SEARCH_PARAM_NAMES.checkOut}=${search.checkOut}`);
  arr.push(
    `${HOTEL_SEARCH_PARAM_NAMES.guestInfo}=${encodeURIComponent(JSON.stringify(search.guestInfo))}`,
  );

  if (filter) {
    arr.push(
      `${HOTEL_FILTER_PARAM_NAMES.area}=${encodeURIComponent(JSON.stringify(filter.subLocations))}`,
    );
    arr.push(
      `${HOTEL_FILTER_PARAM_NAMES.facility}=${encodeURIComponent(
        JSON.stringify(filter.facilities),
      )}`,
    );
    arr.push(
      `${HOTEL_FILTER_PARAM_NAMES.price}=${encodeURIComponent(JSON.stringify(filter.price))}`,
    );
    arr.push(
      `${HOTEL_FILTER_PARAM_NAMES.star}=${encodeURIComponent(JSON.stringify(filter.stars))}`,
    );
    arr.push(
      `${HOTEL_FILTER_PARAM_NAMES.type}=${encodeURIComponent(JSON.stringify(filter.hotelTypes))}`,
    );
    arr.push(`${HOTEL_FILTER_PARAM_NAMES.sortBy}=${encodeURIComponent(filter.sortBy)}`);
  }

  if (mapParams) {
    arr.push(
      `${HOTEL_SEARCH_PARAM_NAMES.mapParams}=${encodeURIComponent(JSON.stringify(mapParams))}`,
    );
  }

  return arr.join('&');
}

export function parseHotelSearchParams(
  params: URLSearchParams,
  skipLocation = false,
): HotelSearchParams {
  const locationStr = params.get(HOTEL_SEARCH_PARAM_NAMES.location);
  if (!locationStr && !skipLocation) {
    throw new Error('No location');
  }
  const location = locationStr ? JSON.parse(locationStr) : undefined;

  let checkIn =
    params.get(HOTEL_SEARCH_PARAM_NAMES.checkIn) || moment().format(DATE_FORMAT_BACK_END);
  const checkInMoment = checkIn ? moment(checkIn, DATE_FORMAT_BACK_END) : undefined;
  if (!checkInMoment?.isValid() || checkInMoment?.isBefore(moment().startOf('day'))) {
    checkIn = moment().format(DATE_FORMAT_BACK_END);
  }

  let checkOut = params.get(HOTEL_SEARCH_PARAM_NAMES.checkOut) || '';
  const checkOutMoment = moment(checkOut, DATE_FORMAT_BACK_END);
  if (!checkOutMoment.isValid() || checkOutMoment.isBefore(moment().startOf('day'))) {
    checkOut = moment().format(DATE_FORMAT_BACK_END);
    throw new Error('Invalid checkout date');
  }

  const guestInfoStr = params.get(HOTEL_SEARCH_PARAM_NAMES.guestInfo);
  const guestInfo = guestInfoStr ? JSON.parse(guestInfoStr) : defaultGuestCountInfo;
  if (!isValidGuestInfo(guestInfo)) {
    throw new Error('Invalid guest info');
  }

  return { location, checkIn, checkOut, guestInfo };
}

export function parseHotelFilterParams(params: URLSearchParams): HotelFilterParams {
  const value = { ...defaultHotelFilterParams };

  let str = params.get(HOTEL_FILTER_PARAM_NAMES.area);
  if (str) {
    value.subLocations = JSON.parse(str);
  }

  str = params.get(HOTEL_FILTER_PARAM_NAMES.facility);
  if (str) {
    value.facilities = JSON.parse(str);
  }

  str = params.get(HOTEL_FILTER_PARAM_NAMES.price);
  if (str) {
    value.price = JSON.parse(str);
  }

  str = params.get(HOTEL_FILTER_PARAM_NAMES.star);
  if (str) {
    value.stars = JSON.parse(str);
  }

  str = params.get(HOTEL_FILTER_PARAM_NAMES.type);
  if (str) {
    value.hotelTypes = JSON.parse(str);
  }
  str = params.get(HOTEL_FILTER_PARAM_NAMES.sortBy);
  if (str) {
    value.sortBy = str;
  }

  return value;
}

export function validGuestCount(roomCount: number, adultCount: number, childCount: number) {
  return (
    roomCount >= 1 &&
    roomCount <= 9 &&
    adultCount >= 1 &&
    adultCount <= 36 &&
    childCount <= 9 &&
    adultCount >= roomCount
  );
}

export function validTravellerCount(adultCount: number, childCount: number, babyCount: number) {
  return (
    adultCount >= 1 &&
    childCount >= 0 &&
    babyCount >= 0 &&
    adultCount + childCount <= 9 &&
    babyCount <= adultCount
  );
}

export function validateChildrenAgesInfo(childrenAges: number[]) {
  return childrenAges.includes(-1);
}

export function addHotelRecentSearch(search: HotelSearchParams) {
  const recentSearchData = localStorage.getItem(HOTEL_SEARCH_HISTORY) || false;
  const searchString = stringifyHotelSearchAndFilterParams(search);

  if (!recentSearchData) {
    localStorage.setItem(HOTEL_SEARCH_HISTORY, JSON.stringify([searchString]));
    return;
  }

  let hotelSearch: string[] = JSON.parse(recentSearchData);

  hotelSearch.unshift(searchString);

  remove(hotelSearch, n => {
    try {
      parseHotelSearchParams(new URLSearchParams(n));
      return false;
    } catch (_) {
      return true;
    }
  });

  hotelSearch = [...uniq(hotelSearch)];

  if (hotelSearch.length > MAX_SEARCH_HISTORY) {
    hotelSearch.pop();
  }
  localStorage.setItem(HOTEL_SEARCH_HISTORY, JSON.stringify(hotelSearch));
}

export function getHotelSearchHistory(): string[] {
  const hotelSearchStr = localStorage.getItem(HOTEL_SEARCH_HISTORY) || false;

  if (!hotelSearchStr) {
    return [];
  }

  const hotelSearch: string[] = JSON.parse(hotelSearchStr);

  remove(hotelSearch, n => {
    try {
      parseHotelSearchParams(new URLSearchParams(n));
      return false;
    } catch (_) {
      return true;
    }
  });

  return hotelSearch;
}
export function clearHotelSearchHistory() {
  localStorage.removeItem(HOTEL_SEARCH_HISTORY);
}

export function parseHotelDetailParams(params: URLSearchParams, skipShrui = false) {
  const hotelIdStr = params.get(HOTEL_BOOK_PARAMS_NAMES.hotelId);
  if (!hotelIdStr) {
    throw new Error('No hotel ID');
  }
  const hotelId = parseInt(hotelIdStr, 10);
  if (!Number.isInteger(hotelId)) {
    throw new Error('Invalid hotel ID');
  }
  const shrui = params.get(HOTEL_BOOK_PARAMS_NAMES.shrui) || '';
  if (!shrui && !skipShrui) {
    throw new Error('No shrui ID');
  }
  const direct = params.get(HOTEL_ACTIVITY_DIRECT_PARAM) === 'true';

  return { hotelId, shrui, direct };
}

export function stringifyHotelBookingParams(search: HotelBookingParams, skipLocation = true) {
  const arr = [];
  if (!skipLocation) {
    arr.push(
      `${HOTEL_SEARCH_PARAM_NAMES.location}=${encodeURIComponent(JSON.stringify(search.location))}`,
    );
  }
  arr.push(`${HOTEL_SEARCH_PARAM_NAMES.checkIn}=${search.checkIn}`);
  arr.push(`${HOTEL_SEARCH_PARAM_NAMES.checkOut}=${search.checkOut}`);
  arr.push(
    `${HOTEL_SEARCH_PARAM_NAMES.guestInfo}=${encodeURIComponent(JSON.stringify(search.guestInfo))}`,
  );
  arr.push(
    `${HOTEL_SEARCH_PARAM_NAMES.guestInfo}=${encodeURIComponent(JSON.stringify(search.guestInfo))}`,
  );
  arr.push(
    `${HOTEL_BOOK_PARAMS_NAMES.hotelId}=${encodeURIComponent(JSON.stringify(search.hotelId))}`,
  );
  search.shrui && arr.push(`${HOTEL_BOOK_PARAMS_NAMES.shrui}=${encodeURIComponent(search.shrui)}`);
  arr.push(`${HOTEL_ACTIVITY_DIRECT_PARAM}=${encodeURIComponent(JSON.stringify(search.direct))}`);

  return arr.join('&');
}
export function parseHotelBookingParams(
  params: URLSearchParams,
  skipShrui = true,
  skipLocation = true,
): HotelBookingParams {
  const value = parseHotelDetailParams(params, skipShrui);
  const search = parseHotelSearchParams(params, skipLocation);
  return { ...search, ...value };
}

export const getRoomData = (data: some[], params: HotelBookingParams) =>
  data.find((roomData: some) => {
    const str1 = roomData.SHRUI as string;
    const str2 = (params && params.shrui) || '';
    return str1.slice(0, str1.indexOf('@')) === str2.slice(0, str2.indexOf('@'));
  });

export function computeHotelPayableNumbers(
  params: HotelBookingParams,
  roomsData: some[],
  noMarkUp = false,
) {
  let roomPrice = 0;

  if (roomsData) {
    const roomDetail = params ? getRoomData(roomsData, params) : undefined;
    roomPrice = roomDetail
      ? noMarkUp
        ? roomDetail.finalPriceWithoutBookerMarkup
        : roomDetail.finalPrice
      : 0;
  }

  const nightCount = params
    ? moment(params.checkOut, DATE_FORMAT_BACK_END).diff(
        moment(params.checkIn, DATE_FORMAT_BACK_END),
        'days',
      )
    : 1;

  const roomCount = params ? params.guestInfo.roomCount : 1;

  const finalPriceIncludeTax = roomPrice * roomCount * nightCount;

  const finalPrice = finalPriceIncludeTax;

  return {
    nightCount,
    roomCount,
    finalPriceIncludeTax,
    finalPrice,
  };
}

function rad(x: number) {
  return (x * Math.PI) / 180;
}

export function getDistance(p1: some, p2: some) {
  const R = 6378137; // Earth’s mean radius in meter
  const dLat = rad(p2.lat - p1.lat);
  const dLong = rad(p2.lng - p1.lng);
  const a =
    Math.sin(dLat / 2) * Math.sin(dLat / 2) +
    Math.cos(rad(p1.lat)) * Math.cos(rad(p2.lat)) * Math.sin(dLong / 2) * Math.sin(dLong / 2);
  const c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
  const d = R * c;
  return d / 1000; // returns the distance in meter
}
