/* eslint-disable no-nested-ternary */
import {
  Button,
  IconButton,
  InputAdornment,
  Typography,
  Container,
  useTheme,
  useMediaQuery,
} from '@material-ui/core';
import SearchIcon from '@material-ui/icons/Search';
import { useFormik } from 'formik';
import moment, { Moment } from 'moment';
import React from 'react';
import { useIntl, FormattedMessage } from 'react-intl';
import { useDispatch, useSelector } from 'react-redux';
import { Action } from 'redux';
import { ThunkDispatch } from 'redux-thunk';
import * as yup from 'yup';
import { API_PATHS } from '../../../../configs/API';
import { ROUTES } from '../../../../configs/routes';
import { some, SUCCESS_CODE } from '../../../../constants';
import { DATE_FORMAT_BACK_END } from '../../../../models/moment';
import { AppState } from '../../../../redux/reducers';
import { ReactComponent as IconHotel } from '../../../../svg/booking/ic_hotel.svg';
import { ReactComponent as IconPin } from '../../../../svg/booking/ic_pin.svg';
import DateRangeFormControl from '../../../common/components/DateRangeFormControl';
import { Row } from '../../../common/components/elements';
import FormControlAutoComplete from '../../../common/components/FormControlAutoComplete';
import { goToAction, setGeneralFlight } from '../../../common/redux/reducer';
import { fetchThunk } from '../../../common/redux/thunk';
import { HOTEL_ACTIVITY_DIRECT_PARAM, HOTEL_BOOK_PARAMS_NAMES } from '../constants';
import { HotelSearchParams, Location, stringifyHotelSearchAndFilterParams } from '../utils';
import GuestInfoBox from './GuestInfoBox';

export function renderOption(location: Location) {
  const style: React.CSSProperties = {
    flexShrink: 0,
    margin: '0 15px 0 5px',
    width: '28px',
    height: '28px',
    borderRadius: location.thumbnailUrl ? '50%' : undefined,
  };
  return (
    <div style={{ display: 'flex', alignItems: 'center' }}>
      {location.hotelId !== -1 ? (
        <IconHotel style={style} />
      ) : location.thumbnailUrl ? (
        <img style={style} alt="" src={location.thumbnailUrl} />
      ) : (
        <IconPin style={style} />
      )}

      <div style={{ display: 'flex', flexDirection: 'column', overflow: 'hidden' }}>
        <Typography variant="body2" style={{ padding: '5px' }} noWrap>
          {location.name}
        </Typography>
        {location.hotelId !== -1 && (
          <Typography variant="caption" style={{ padding: '0px 5px 3px' }}>
            {location.provinceName}
          </Typography>
        )}
      </div>
    </div>
  );
}

export const getLabel = (option: Location) => {
  return `${option.name || option.provinceName}`;
};

interface Props {
  params: HotelSearchParams;
  onSearch(params: HotelSearchParams): void;
}

const HotelSearch: React.FC<Props> = props => {
  const { params, onSearch } = props;
  const dispatch = useDispatch<ThunkDispatch<AppState, null, Action<string>>>();
  const generalFlight = useSelector((state: AppState) => state.common.generalFlight);
  const intl = useIntl();
  const date = React.createRef<HTMLDivElement>();
  const guest = React.createRef<HTMLDivElement>();
  const [notableLocationsHotel, setNotableLocationsHotel] = React.useState<Location[]>([]);
  const theme = useTheme();
  const isTablet = useMediaQuery(theme.breakpoints.down('md'));

  const flightSearchSchema = yup.object().shape({
    location: yup
      .mixed()
      .nullable()
      .required(intl.formatMessage({ id: 'required' })),
    checkIn: yup
      .string()
      .trim()
      .required(intl.formatMessage({ id: 'required' })),
    checkOut: yup
      .string()
      .trim()
      .required(intl.formatMessage({ id: 'required' })),
    guestInfo: yup.object().shape({
      childrenAges: yup.array().of(yup.number().min(0, intl.formatMessage({ id: 'required' }))),
    }),
  });

  const formik = useFormik({
    initialValues: params,
    onSubmit: values => {
      const { location } = values;
      if (values.location?.hotelId !== -1) {
        const searchParams = `${stringifyHotelSearchAndFilterParams(
          { ...formik.values, location },
          undefined,
          undefined,
          true,
        )}&${HOTEL_BOOK_PARAMS_NAMES.hotelId}=${
          location?.hotelId
        }&${HOTEL_ACTIVITY_DIRECT_PARAM}=${true}`;
        dispatch(goToAction({ pathname: ROUTES.booking.hotel.detail, search: searchParams }));
      } else {
        onSearch(values);
      }
    },
    validationSchema: flightSearchSchema,
  });

  const onSelectLocation = React.useCallback(
    (location: Location | null) => {
      formik.setFieldValue('location', location, true);
      if (date.current && !formik.touched.location && location) {
        date.current?.focus();
      }
    },
    [date, formik],
  );

  const fetchTopDestination = React.useCallback(async () => {
    const json = await dispatch(fetchThunk(API_PATHS.getTopHotelLocation(10), 'get'));
    if (json?.code === SUCCESS_CODE) {
      setNotableLocationsHotel(
        json.data.map((v: some) => {
          return { ...v, thumbnailUrl: v.thumb, name: v.name, hotelId: -1 };
        }),
      );
    }
  }, [dispatch]);

  React.useEffect(() => {
    if (!generalFlight) {
      dispatch(fetchThunk(API_PATHS.flightGeneralInfo, 'get')).then(json =>
        dispatch(setGeneralFlight(json.data)),
      );
    }
  }, [dispatch, generalFlight]);

  React.useEffect(() => {
    formik.setValues(params, true);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [params]);

  React.useEffect(() => {
    fetchTopDestination();
  }, [fetchTopDestination]);

  const handleSubmit = React.useCallback(
    (e?: React.FormEvent<HTMLFormElement> | undefined) => {
      formik.handleSubmit(e);
    },
    [formik],
  );

  React.useEffect(() => {
    if (formik.submitCount > 0 && formik.isSubmitting && !formik.isValid) {
      if (formik.errors.location) {
        return;
      }
      if (formik.errors.checkIn || formik.errors.checkOut) {
        date.current?.focus();
        return;
      }
      if (formik.errors.guestInfo) {
        guest.current?.focus();
      }
    }
  }, [date, formik, formik.errors.guestInfo, formik.isSubmitting, guest]);

  return (
    <div className="hotel-search">
      <Container
        style={{
          display: 'flex',
          flexDirection: 'column',
          justifyContent: 'center',
          padding: isTablet ? '16px 8px 0px' : '32px 24px 0px',
          color: 'black',
        }}
      >
        <form autoComplete="off" onSubmit={handleSubmit}>
          <Row>
            <FormControlAutoComplete<Location>
              id="origin"
              label={intl.formatMessage({ id: 'hotel.chooseLocation' })}
              placeholder={intl.formatMessage({ id: 'hotel.chooseLocation' })}
              value={formik.values.location || null}
              formControlStyle={{ width: 270 }}
              onChange={(e: any, value: Location | null) => {
                onSelectLocation(value);
              }}
              loadOptions={async (str: string) => {
                const json = await dispatch(fetchThunk(API_PATHS.suggestLocation(str)));
                return json.data?.locations?.concat(json.data.hotels);
              }}
              getOptionSelected={(option, value) => {
                return option.slug === value.slug;
              }}
              getOptionLabel={getLabel}
              options={notableLocationsHotel}
              renderOption={renderOption}
              startAdornment={
                <InputAdornment position="start" style={{ marginLeft: 8 }}>
                  <IconButton size="small" edge="start">
                    <IconPin />
                  </IconButton>
                </InputAdornment>
              }
              errorMessage={
                formik.errors.location && formik.touched.location
                  ? formik.errors.location
                  : undefined
              }
            />
            <DateRangeFormControl
              iRef={date}
              label={intl.formatMessage({ id: 'hotel.stayDuration' })}
              placeholder={intl.formatMessage({ id: 'hotel.chooseTime' })}
              style={{ width: 270 }}
              optional
              startDate={
                formik.values.checkIn
                  ? moment(formik.values.checkIn, DATE_FORMAT_BACK_END)
                  : undefined
              }
              endDate={
                formik.values.checkOut
                  ? moment(formik.values.checkOut, DATE_FORMAT_BACK_END)
                  : undefined
              }
              onChange={(start?: Moment, end?: Moment) => {
                formik.setFieldValue(
                  'checkIn',
                  start ? start.format(DATE_FORMAT_BACK_END) : undefined,
                );
                formik.setFieldValue(
                  'checkOut',
                  end ? end.format(DATE_FORMAT_BACK_END) : undefined,
                );
              }}
              onClickBtn={() => {
                if (guest.current && !formik.touched.guestInfo) {
                  guest.current?.focus();
                }
              }}
              numberOfMonths={isTablet ? 1 : 2}
              errorMessage={
                formik.touched.checkIn || formik.touched.checkOut
                  ? formik.errors.checkIn || formik.errors.checkOut
                  : undefined
              }
              startAdornment
              isOutsideRange={(e: any) =>
                moment()
                  .startOf('day')
                  .isAfter(e)
              }
            />

            <GuestInfoBox
              iRef={guest}
              onChange={(info, noValid) => {
                formik.setFieldValue('guestInfo', info, !noValid);
              }}
              guestInfo={formik.values.guestInfo}
              errorMessage={
                formik.touched.guestInfo?.childrenAges
                  ? formik.errors.guestInfo?.childrenAges
                  : undefined
              }
              label={intl.formatMessage({ id: 'hotel.guestInfo' })}
              style={{ width: 270 }}
            />
            <Button
              // loading={loading}
              variant="contained"
              color="secondary"
              style={{ marginTop: 4, width: 160 }}
              size="large"
              type="submit"
              disableElevation
            >
              <SearchIcon />
              {!isTablet && <FormattedMessage id="search" />}
            </Button>
          </Row>
        </form>
      </Container>
    </div>
  );
};

export default HotelSearch;
