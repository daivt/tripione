import { Button, FormControl, IconButton, InputAdornment, RootRef } from '@material-ui/core';
import React from 'react';
import { FormattedMessage, IntlShape, useIntl } from 'react-intl';
import { CSSTransition } from 'react-transition-group';
import { ReactComponent as IconGuest } from '../../../../../svg/booking/ic_guest.svg';
import { Wrapper } from '../../../../common/components/elements';
import FormControlTextField from '../../../../common/components/FormControlTextField';
import styles from '../../../../common/components/styles.module.scss';
import { GuestCountInfo } from '../../utils';
import GuestBox from './GuestBox';

export function getInputStr(guestInfo: GuestCountInfo, intl: IntlShape) {
  return `${intl.formatMessage(
    {
      id: 'hotel.guestInfo.adultCount',
    },
    { adultCount: guestInfo.adultCount },
  )}${
    guestInfo.childCount
      ? `, ${intl.formatMessage(
          { id: 'hotel.guestInfo.childCount' },
          { childCount: guestInfo.childCount },
        )}`
      : ''
  }${
    guestInfo.roomCount
      ? `, ${intl.formatMessage(
          { id: 'hotel.guestInfo.roomCount' },
          { roomCount: guestInfo.roomCount },
        )}`
      : ''
  }`;
}

interface Props {
  minimizedWidth?: string | number;
  guestInfo: GuestCountInfo;
  errorMessage?: any;
  onChange(newValue: GuestCountInfo, noValid?: boolean): void;
  iRef?: React.RefObject<HTMLDivElement>;
  formControlStyle?: React.CSSProperties;
  style?: React.CSSProperties;
  label?: React.ReactNode;
}

interface State {
  isFocused: boolean;
}

const GuestInfoBox: React.FC<Props> = props => {
  const {
    minimizedWidth,
    guestInfo,
    errorMessage,
    onChange,
    formControlStyle,
    style,
    label,
    iRef,
  } = props;

  const intl = useIntl();
  const [isFocused, setFocus] = React.useState(false);
  const [height, setHeight] = React.useState(0);

  const parent = iRef || React.createRef<HTMLDivElement>();
  const innerRef = React.useRef<HTMLInputElement>();
  const onBlur = React.useCallback(
    (e: React.FocusEvent<HTMLDivElement>) => {
      if (e.relatedTarget instanceof Element) {
        if (e.currentTarget.contains(e.relatedTarget as Element)) {
          if (parent.current) {
            parent.current.focus();
            return;
          }
        }
      }
      setFocus(false);
    },
    [parent],
  );

  // const handleChangeChildrenAge = React.useCallback(
  //   (value: any, index: number) => {
  //     const newAgesArray = [...guestInfo.childrenAges];
  //     newAgesArray[index] = value;
  //     onChange({
  //       ...guestInfo,
  //       childrenAges: newAgesArray,
  //     });
  //   },
  //   [guestInfo, onChange],
  // );

  React.useEffect(() => {
    setHeight(innerRef.current?.offsetHeight as number);
  }, []);

  return (
    <FormControl
      style={{
        position: 'relative',
        outline: 'none',
        minHeight: height,
        color: isFocused ? 'black' : undefined,
        ...style,
      }}
      tabIndex={-1}
      ref={parent}
      onBlur={onBlur}
      onFocus={e => {
        setFocus(true);
      }}
    >
      <Wrapper
        style={{
          boxShadow: isFocused ? '0px 4px 8px rgba(0, 0, 0, 0.25)' : undefined,
          zIndex: isFocused ? 100 : 0,
          backgroundColor: isFocused ? 'rgba(255,255,255,1)' : 'rgba(255,255,255,0)',
          margin: isFocused ? -12 : undefined,
          transition: 'none',
        }}
      >
        <div style={{ padding: isFocused ? '12px 12px 0px 12px' : undefined }}>
          <RootRef rootRef={innerRef}>
            <FormControlTextField
              id="dateField"
              label={label}
              formControlStyle={{
                margin: 0,
                width: '100%',
                minWidth: minimizedWidth,
                ...formControlStyle,
              }}
              style={{
                background: 'white',
              }}
              fullWidth
              value={getInputStr(guestInfo, intl)}
              optional
              readOnly
              inputProps={{ style: { width: '100%' } }}
              startAdornment={
                <InputAdornment position="start" style={{ marginLeft: 8 }}>
                  <IconButton size="small" edge="start">
                    <IconGuest />
                  </IconButton>
                </InputAdornment>
              }
              errorMessage={
                errorMessage
                  ? intl.formatMessage({ id: 'hotel.guestInfo.childrenValid' })
                  : undefined
              }
            />
          </RootRef>
        </div>
        <CSSTransition
          classNames={{
            enter: styles.enter,
            enterActive: styles.enterActive,
            exit: styles.leave,
            exitActive: styles.leaveActive,
          }}
          timeout={200}
          in={isFocused}
          unmountOnExit
        >
          <div
            style={{
              transition: 'all 300ms ease',
              textAlign: 'start',
              transformOrigin: '0 0',
              backgroundColor: 'white',
              padding: '0 12px 8px 12px',
            }}
          >
            <GuestBox guestInfo={guestInfo} onChange={onChange} errorMessage={errorMessage} />
            <div
              style={{
                marginBottom: '10px',
                marginTop: '10px',
                textAlign: 'end',
                position: 'relative',
              }}
            >
              <Button
                size="large"
                variant="contained"
                color="secondary"
                disableElevation
                onClick={() => {
                  if (!errorMessage) {
                    parent.current?.blur();
                  }
                }}
              >
                <FormattedMessage id="done" />
              </Button>
            </div>
          </div>
        </CSSTransition>
      </Wrapper>
    </FormControl>
  );
};

export default GuestInfoBox;
