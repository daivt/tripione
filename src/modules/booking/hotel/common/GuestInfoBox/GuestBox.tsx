import { Divider, IconButton, Typography } from '@material-ui/core';
import AddCircle from '@material-ui/icons/AddCircle';
import RemoveCircleOutline from '@material-ui/icons/RemoveCircleOutline';
import React from 'react';
import { FormattedMessage, IntlShape } from 'react-intl';
import { GREY_300 } from '../../../../../configs/colors';
import { GuestCountInfo, validGuestCount } from '../../utils';
import ChildrenAgeSelectSection from './ChildrenAgeSelectSection';
import { CountBox, TypeBox, TypeLine, ValueBox, ValueControl } from './styles';

export function getInputStr(guestInfo: GuestCountInfo, intl: IntlShape) {
  return `${intl.formatMessage(
    {
      id: 'hotel.guestInfo.adultCount',
    },
    { adultCount: guestInfo.adultCount },
  )}${
    guestInfo.childCount
      ? `, ${intl.formatMessage(
          { id: 'hotel.guestInfo.childCount' },
          { childCount: guestInfo.childCount },
        )}`
      : ''
  }${
    guestInfo.roomCount
      ? `, ${intl.formatMessage(
          { id: 'hotel.guestInfo.roomCount' },
          { roomCount: guestInfo.roomCount },
        )}`
      : ''
  }`;
}

interface Props {
  guestInfo: GuestCountInfo;
  errorMessage?: any;
  onChange(newValue: GuestCountInfo, noValid?: boolean): void;
}

const GuestBox: React.FC<Props> = props => {
  const { guestInfo, errorMessage, onChange } = props;

  const handleChangeChildrenAge = React.useCallback(
    (value: any, index: number) => {
      const newAgesArray = [...guestInfo.childrenAges];
      newAgesArray[index] = value;
      onChange({
        ...guestInfo,
        childrenAges: newAgesArray,
      });
    },
    [guestInfo, onChange],
  );

  return (
    <>
      <CountBox>
        <TypeLine>
          <Typography variant="body2">
            <FormattedMessage id="hotel.guestInfo.room" />
          </Typography>
        </TypeLine>
        <ValueControl>
          <IconButton
            size="small"
            disabled={
              !validGuestCount(guestInfo.roomCount - 1, guestInfo.adultCount, guestInfo.childCount)
            }
            onClick={() => onChange({ ...guestInfo, roomCount: guestInfo.roomCount - 1 })}
          >
            <RemoveCircleOutline
              style={{
                fontSize: '20px',
                color: !validGuestCount(
                  guestInfo.roomCount - 1,
                  guestInfo.adultCount,
                  guestInfo.childCount,
                )
                  ? GREY_300
                  : undefined,
              }}
              color="secondary"
            />
          </IconButton>
          <ValueBox>{guestInfo.roomCount}</ValueBox>
          <IconButton
            size="small"
            disabled={
              !validGuestCount(guestInfo.roomCount + 1, guestInfo.adultCount, guestInfo.childCount)
            }
            onClick={() => onChange({ ...guestInfo, roomCount: guestInfo.roomCount + 1 })}
          >
            <AddCircle
              style={{
                fontSize: '20px',
                color: !validGuestCount(
                  guestInfo.roomCount + 1,
                  guestInfo.adultCount,
                  guestInfo.childCount,
                )
                  ? GREY_300
                  : undefined,
              }}
              color="secondary"
            />
          </IconButton>
        </ValueControl>
      </CountBox>
      <CountBox>
        <TypeBox>
          <TypeLine>
            <Typography variant="body2">
              <FormattedMessage id="hotel.guestInfo.adult" />
            </Typography>{' '}
          </TypeLine>

          <TypeLine style={{ color: GREY_300 }}>
            <Typography variant="body2">
              <FormattedMessage id="hotel.guestInfo.adultDef" />
            </Typography>
          </TypeLine>
        </TypeBox>
        <ValueControl>
          <IconButton
            size="small"
            disabled={
              !validGuestCount(guestInfo.roomCount, guestInfo.adultCount - 1, guestInfo.childCount)
            }
            onClick={() => onChange({ ...guestInfo, adultCount: guestInfo.adultCount - 1 })}
          >
            <RemoveCircleOutline
              style={{
                fontSize: '20px',
                color: !validGuestCount(
                  guestInfo.roomCount,
                  guestInfo.adultCount - 1,
                  guestInfo.childCount,
                )
                  ? GREY_300
                  : undefined,
              }}
              color="secondary"
            />
          </IconButton>
          <ValueBox>{guestInfo.adultCount}</ValueBox>
          <IconButton
            size="small"
            disabled={
              !validGuestCount(guestInfo.roomCount, guestInfo.adultCount + 1, guestInfo.childCount)
            }
            onClick={() => onChange({ ...guestInfo, adultCount: guestInfo.adultCount + 1 })}
          >
            <AddCircle
              style={{
                fontSize: '20px',
                color: !validGuestCount(
                  guestInfo.roomCount,
                  guestInfo.adultCount + 1,
                  guestInfo.childCount,
                )
                  ? GREY_300
                  : undefined,
              }}
              color="secondary"
            />
          </IconButton>
        </ValueControl>
      </CountBox>
      <CountBox>
        <TypeBox>
          <TypeLine>
            <Typography variant="body2">
              <FormattedMessage id="hotel.guestInfo.children" />
            </Typography>
          </TypeLine>

          <TypeLine style={{ color: GREY_300 }}>
            <Typography variant="body2">
              <FormattedMessage id="hotel.guestInfo.childrenDef" />
            </Typography>
          </TypeLine>
        </TypeBox>
        <ValueControl>
          <IconButton
            size="small"
            disabled={guestInfo.childCount === 0}
            onClick={() =>
              onChange({
                ...guestInfo,
                childCount: guestInfo.childCount - 1,
                childrenAges: guestInfo.childrenAges.slice(0, guestInfo.childrenAges.length - 1),
              })
            }
          >
            <RemoveCircleOutline
              style={{
                fontSize: '20px',
                color: guestInfo.childCount === 0 ? GREY_300 : undefined,
              }}
              color="secondary"
            />
          </IconButton>
          <ValueBox>{guestInfo.childCount}</ValueBox>
          <IconButton
            size="small"
            disabled={
              !validGuestCount(guestInfo.roomCount, guestInfo.adultCount, guestInfo.childCount + 1)
            }
            onClick={() =>
              onChange(
                {
                  ...guestInfo,
                  childCount: guestInfo.childCount + 1,
                  childrenAges: guestInfo.childrenAges.concat(-1),
                },
                true,
              )
            }
          >
            <AddCircle
              style={{
                fontSize: '20px',
                color: !validGuestCount(
                  guestInfo.roomCount,
                  guestInfo.adultCount,
                  guestInfo.childCount + 1,
                )
                  ? GREY_300
                  : undefined,
              }}
              color="secondary"
            />
          </IconButton>
        </ValueControl>
      </CountBox>
      <Divider style={{ marginTop: '8px' }} />
      <ChildrenAgeSelectSection
        childrenAges={guestInfo.childrenAges}
        onChange={handleChangeChildrenAge}
        errorMessage={errorMessage}
      />
    </>
  );
};

export default GuestBox;
