import styled from 'styled-components';

export const ScrollBox = styled.div`
  /* box-shadow: 0px 1px 15px rgba(0, 0, 0, 0.2), 0px 9px 46px rgba(0, 0, 0, 0.12),
    0px 24px 38px rgba(0, 0, 0, 0.14); */
  overflow: auto;
  /* margin-top: 16px; */
  &::-webkit-scrollbar-thumb {
    background: gray;
    border: 8px solid white;
  }
  &::-webkit-scrollbar {
    background: white;
    width: 24px;
    margin-top: 40px;
    margin-bottom: 88px;
  }
`;

export const Line = styled.div`
  display: flex;
  justify-content: flex-start;
  align-items: center;
  min-height: 32px;
`;
