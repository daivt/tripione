import { ButtonBase, Typography } from '@material-ui/core';
import * as React from 'react';
import { GREY_500, PRIMARY, WHITE } from '../../../../../configs/colors';
import { ROUTES } from '../../../../../configs/routes';
import { ReactComponent as IconDoor } from '../../../../../svg/booking/ic_door.svg';
import { ReactComponent as IconGuest } from '../../../../../svg/booking/ic_guest.svg';
import { ReactComponent as IconHotel } from '../../../../../svg/booking/ic_hotel.svg';
import { ReactComponent as IconPin } from '../../../../../svg/booking/ic_pin.svg';
import Link from '../../../../common/components/Link';
import { HOTEL_ACTIVITY_DIRECT_PARAM, HOTEL_BOOK_PARAMS_NAMES } from '../../constants';
import { parseHotelSearchParams, stringifyHotelSearchAndFilterParams } from '../../utils';

interface Props {
  data: string;
}

const HotelRecentCard: React.FunctionComponent<Props> = props => {
  const { data } = props;
  const { location, checkIn, checkOut, guestInfo } = parseHotelSearchParams(
    new URLSearchParams(data),
  );
  const getLink = React.useMemo(() => {
    return location?.hotelId === -1
      ? {
          pathname: ROUTES.booking.hotel.result,
          search: stringifyHotelSearchAndFilterParams({
            location,
            checkIn,
            checkOut,
            guestInfo,
          }),
        }
      : {
          pathname: ROUTES.booking.hotel.detail,
          search: `${stringifyHotelSearchAndFilterParams(
            {
              location,
              checkIn,
              checkOut,
              guestInfo,
            },
            undefined,
            undefined,
            true,
          )}&${HOTEL_BOOK_PARAMS_NAMES.hotelId}=${
            location?.hotelId
          }&${HOTEL_ACTIVITY_DIRECT_PARAM}=${true}`,
        };
  }, [checkIn, checkOut, guestInfo, location]);

  return (
    <Link to={getLink}>
      <ButtonBase
        style={{
          padding: 10,
          border: `1px solid ${GREY_500}`,
          display: 'flex',
          flexDirection: 'column',
          minWidth: 250,
          marginRight: 8,
          alignItems: 'flex-start',
          borderRadius: 4,
          background: WHITE,
        }}
      >
        <div style={{ display: 'flex', alignItems: 'center' }}>
          {location?.hotelId === -1 ? (
            <IconPin style={{ stroke: PRIMARY, flexShrink: 0 }} className="svgFill" />
          ) : (
            <IconHotel style={{ stroke: PRIMARY, flexShrink: 0 }} className="svgFill" />
          )}
          <Typography variant="subtitle2" noWrap style={{ marginLeft: '16px' }}>
            {location ? location.name : ''}
          </Typography>
        </div>
        <div style={{ marginTop: '16px', display: 'flex' }}>
          <div style={{ display: 'flex', alignItems: 'center' }}>
            <IconDoor />
            <Typography variant="body1" style={{ marginLeft: '8px' }}>
              {guestInfo.roomCount}
            </Typography>
          </div>
          <div style={{ display: 'flex', alignItems: 'center', marginLeft: '32px' }}>
            <IconGuest />
            <Typography variant="body1" style={{ marginLeft: '8px' }}>
              {guestInfo.adultCount + guestInfo.childCount}
            </Typography>
          </div>
        </div>
        <div style={{ marginTop: '16px' }}>
          <Typography variant="body2">{`${checkIn} - ${checkOut}`}</Typography>
        </div>
      </ButtonBase>
    </Link>
  );
};

export default HotelRecentCard;
