import { ButtonBase, Card, Typography } from '@material-ui/core';
import Skeleton from '@material-ui/lab/Skeleton';
import * as React from 'react';
import { ROUTES } from '../../../../../configs/routes';
import { some } from '../../../../../constants';
import Link from '../../../../common/components/Link';
import ProgressiveImage from '../../../common/ProgressiveImage';
import {
  defaultHotelSearchParams,
  Location,
  stringifyHotelSearchAndFilterParams,
} from '../../utils';

const HEIGHT = '360px';

interface Props {
  data: some;
}

const PopularDestinationsCard: React.FunctionComponent<Props> = props => {
  const { data } = props;
  const location = data as Location;
  const getLink = React.useCallback(() => {
    const params = stringifyHotelSearchAndFilterParams({ ...defaultHotelSearchParams, location });
    return {
      pathname: `${ROUTES.booking.hotel.result}`,
      search: `?${params}`,
    };
  }, [location]);

  if (data.skeleton) {
    return (
      <Card style={{ height: HEIGHT, display: 'flex', flexDirection: 'column' }} elevation={2}>
        <Skeleton height={HEIGHT} variant="rect" />
        <div
          style={{
            position: 'relative',
            bottom: 0,
            left: 0,
            right: 0,
            display: 'flex',
            background: 'white',
            minHeight: '72px',
            alignItems: 'center',
            padding: '8px 16px',
          }}
        >
          <Skeleton variant="text" width="80%" />
        </div>
      </Card>
    );
  }
console.log(data);

  return (
    <Link to={getLink()} tooltip={data.name || data.provinceName || ''}>
      <ButtonBase
        style={{ height: 360, position: 'relative', borderRadius: 4, overflow: 'hidden' }}
      >
        <ProgressiveImage
          style={{ width: '100%', height: '100%', objectFit: 'cover' }}
          src={data.imageUrl || data.thumb}
          alt=""
        />
        <div
          style={{
            position: 'absolute',
            bottom: 0,
            left: 0,
            right: 0,
            display: 'flex',
            background: 'linear-gradient(180deg, rgba(0, 0, 0, 0) 0%, rgba(0, 0, 0, 0.72) 100%)',
            minHeight: '72px',
            alignItems: 'center',
            padding: '8px 16px',
            zIndex: 1,
          }}
        >
          <Typography
            style={{
              color: 'white',
            }}
            variant="h5"
          >
            {data.name || data.provinceName}
          </Typography>
        </div>
      </ButtonBase>
    </Link>
  );
};

export default PopularDestinationsCard;
