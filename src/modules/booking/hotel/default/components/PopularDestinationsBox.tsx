import { Grid } from '@material-ui/core';
import * as React from 'react';
import PopularDestinationsCard from './PopularDestinationCard';
import { some } from '../../../../../constants';

interface Props {
  data: some[];
}

const PopularDestinationsBox: React.FunctionComponent<Props> = props => {
  const { data } = props;
  return (
    <Grid container spacing={3}>
      {data.map(v => (
        <Grid
          key={`${v.id}-${v.hotelId}-${v.provinceId}-${v.name}`}
          item
          xs={3}
          style={{ cursor: 'pointer' }}
        >
          <PopularDestinationsCard data={v} />
        </Grid>
      ))}
    </Grid>
  );
};

export default PopularDestinationsBox;
