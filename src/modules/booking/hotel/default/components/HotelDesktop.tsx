import { Container, Typography } from '@material-ui/core';
import React from 'react';
import { FormattedMessage } from 'react-intl';
import { some } from '../../../../../constants';
import HotelSearch from '../../common/HotelSearch';
import { defaultHotelSearchParams, HotelSearchParams } from '../../utils';
import HotelFindRecentBox from './HotelFindRecentBox';
import PopularDestinationsBox from './PopularDestinationsBox';

interface Props {
  data?: some[];
  onSearch(params: HotelSearchParams): void;
}

const HotelDesktop: React.FC<Props> = props => {
  const { data, onSearch } = props;

  const getData = React.useMemo(() => {
    let popularDestinations: some[] = [];
    if (data?.length === 0 || data === undefined) {
      popularDestinations = [
        { id: 0, skeleton: true },
        { id: 1, skeleton: true },
        { id: 2, skeleton: true },
        { id: 3, skeleton: true },
        { id: 4, skeleton: true },
        { id: 5, skeleton: true },
        { id: 6, skeleton: true },
        { id: 7, skeleton: true },
      ];
    } else {
      popularDestinations = data.slice(0, 8);
    }
    return popularDestinations;
  }, [data]);

  return (
    <>
      <HotelSearch params={defaultHotelSearchParams} onSearch={onSearch} />
      <Container style={{ padding: '16px 24px 24px 32px', alignItems: 'center' }}>
        <HotelFindRecentBox />
        <Typography style={{ marginTop: '16px' }} variant="h5">
          <FormattedMessage id="hotel.popularDestinations" />
        </Typography>
        <PopularDestinationsBox data={getData} />
      </Container>
    </>
  );
};

export default HotelDesktop;
