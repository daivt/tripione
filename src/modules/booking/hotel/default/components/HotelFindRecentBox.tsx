import { Button } from '@material-ui/core';
import Typography from '@material-ui/core/Typography';
import React, { PureComponent } from 'react';
import { FormattedMessage } from 'react-intl';
import Slider from 'react-slick';
import { BLUE } from '../../../../../configs/colors';
import { ReactComponent as IconDelete } from '../../../../../svg/booking/ic_delete.svg';
import { slideSettings } from '../../../common/Slider/setting';
import { clearHotelSearchHistory, getHotelSearchHistory } from '../../utils';
import HotelRecentCard from './HotelRecentCard';

interface Props {}

interface State {
  recentSearchData: string[];
}

class HotelFindRecentBox extends PureComponent<Props, State> {
  state: State = {
    recentSearchData: [],
  };

  componentDidMount() {
    const recentSearchData = getHotelSearchHistory();
    this.setState({ recentSearchData });
  }

  render() {
    const { recentSearchData } = this.state;

    return (
      <>
        {!!recentSearchData.length && (
          <div style={{ marginTop: '10px', marginBottom: '35px' }}>
            <div style={{ display: 'flex', justifyContent: 'space-between' }}>
              <Typography variant="h5" style={{ padding: '10px 0' }}>
                <FormattedMessage id="searchRecent" />
              </Typography>

              <div
                style={{
                  display: 'flex',
                  flex: 1,
                  justifyContent: 'flex-end',
                  alignItems: 'flex-end',
                  margin: '12px 0px',
                }}
              >
                <Button
                  onClick={() => {
                    clearHotelSearchHistory();
                    this.setState({ recentSearchData: [] });
                  }}
                >
                  <div style={{ display: 'flex', alignItems: 'center', padding: '3px 6px' }}>
                    <IconDelete />
                    <Typography variant="body2" style={{ color: BLUE, paddingLeft: '6px' }}>
                      <FormattedMessage id="deleteSearchHistory" />
                    </Typography>
                  </div>
                </Button>
              </div>
            </div>

            <div style={{ margin: '0 36px' }}>
              <Slider {...slideSettings()}>
                {recentSearchData.map((item: string, index: number) => (
                  <HotelRecentCard key={index} data={item} />
                ))}
              </Slider>
            </div>
          </div>
        )}
      </>
    );
  }
}

export default HotelFindRecentBox;
