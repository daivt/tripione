import React from 'react';
import { useDispatch } from 'react-redux';
import { Action } from 'redux';
import { ThunkDispatch } from 'redux-thunk';
import { API_PATHS } from '../../../../../configs/API';
import { ROUTES } from '../../../../../configs/routes';
import { some, SUCCESS_CODE } from '../../../../../constants';
import { AppState } from '../../../../../redux/reducers';
import { goToAction } from '../../../../common/redux/reducer';
import { fetchThunk } from '../../../../common/redux/thunk';
import { addHotelRecentSearch, stringifyHotelSearchAndFilterParams } from '../../utils';
import HotelDesktop from '../components/HotelDesktop';

interface Props {}

const Hotel: React.FC<Props> = props => {
  const dispatch = useDispatch<ThunkDispatch<AppState, null, Action<string>>>();
  const [data, setData] = React.useState<some[] | undefined>();
  const [loading, setLoading] = React.useState(false);
  const [hasLocation, setHasLocation] = React.useState(true);

  const fetchTopDestination = React.useCallback(async () => {
    setLoading(true);
    const json = await dispatch(fetchThunk(API_PATHS.getTopHotelLocation(10), 'get'));
    if (json.code === SUCCESS_CODE) {
      setData(
        json.data.map((v: some) => {
          return { ...v, thumbnailUrl: v.thumb, name: v.name, hotelId: -1 };
        }),
      );
    }
    setLoading(false);
  }, [dispatch]);

  React.useEffect(() => {
    fetchTopDestination();
  }, [fetchTopDestination]);

  return (
    <HotelDesktop
      data={data}
      onSearch={params => {
        const search = stringifyHotelSearchAndFilterParams(params);
        addHotelRecentSearch(params);
        dispatch(
          goToAction({
            pathname: ROUTES.booking.hotel.result,
            search: `?${search}`,
          }),
        );
      }}
    />
  );
};

export default Hotel;
