import {
  Button,
  Checkbox,
  Collapse,
  Divider,
  FormControlLabel,
  Tooltip,
  Typography,
} from '@material-ui/core';
import Slider from '@material-ui/core/Slider';
import ExpandLessIcon from '@material-ui/icons/ExpandLess';
import RefreshIcon from '@material-ui/icons/Refresh';
import { remove } from 'lodash';
import React from 'react';
import { FormattedMessage, FormattedNumber } from 'react-intl';
import { GREY } from '../../../../../configs/colors';
import { some } from '../../../../../constants';
import { ReactComponent as IconFunnel } from '../../../../../svg/booking/ic_funnel.svg';
import { DropDownHeader, DropDownStyle } from '../../../common/element';
import { durationMillisecondToHour, roundUp } from '../../../utils';
import CheckButton from '../../common/CheckButton';
import { FlightBookingResult, FlightFilterParams } from '../../utils';
import { PRICE_STEP, TIME_TAKE_OFF_AND_LAND } from '../constants';

interface PropsLabel {
  children: React.ReactElement;
  open: boolean;
  value: number;
}
function PriceLabelComponent(props: PropsLabel) {
  const { children, open, value } = props;

  return (
    <Tooltip
      open={open}
      enterTouchDelay={0}
      placement="top"
      title={
        <>
          <FormattedNumber value={value} />
          &nbsp;
          <FormattedMessage id="currency" />
        </>
      }
    >
      {children}
    </Tooltip>
  );
}
function TimeLabelComponent(props: PropsLabel) {
  const { children, open, value } = props;

  return (
    <Tooltip
      open={open}
      enterTouchDelay={0}
      placement="top"
      title={durationMillisecondToHour(value)}
    >
      {children}
    </Tooltip>
  );
}

interface Props {
  isChangeItinerary?: boolean;
  filterParams: FlightFilterParams;
  setFilterParams(filterParams: FlightFilterParams): void;
  resetFilter(): void;
  data?: some;
  booking: FlightBookingResult;
}

interface State {
  showNumberStop: boolean;
  showLeg: boolean;
  fromAndTime: boolean;
  airline: boolean;
}
const FlightFilter: React.FC<Props> = props => {
  const { setFilterParams, isChangeItinerary, filterParams, data, booking, resetFilter } = props;
  const [state, setState] = React.useState<State>({
    showNumberStop: true,
    showLeg: true,
    fromAndTime: true,
    airline: true,
  });

  const maxTransitDuration = React.useMemo(() => {
    let maxDuration = 0;
    if (data) {
      if (booking.outbound.ticket) {
        maxDuration = filterParams.maxTransitDuration.inbound;
      } else {
        maxDuration = filterParams.maxTransitDuration.outbound;
      }
    }
    return maxDuration;
  }, [
    booking.outbound.ticket,
    data,
    filterParams.maxTransitDuration.inbound,
    filterParams.maxTransitDuration.outbound,
  ]);

  const maxFlightDuration = React.useMemo(() => {
    let maxDuration = 0;
    if (data) {
      if (booking.outbound.ticket) {
        maxDuration = filterParams.maxFlightDuration.inbound;
      } else {
        maxDuration = filterParams.maxFlightDuration.outbound;
      }
    }
    return maxDuration;
  }, [
    booking,
    data,
    filterParams.maxFlightDuration.inbound,
    filterParams.maxFlightDuration.outbound,
  ]);

  const setNumStop = React.useCallback(
    (event: React.ChangeEvent<HTMLInputElement>, value: number) => {
      const numStops = [...filterParams.numStops];
      if (event.target.checked) {
        numStops.push(value);
      } else {
        remove(numStops, obj => obj === value);
      }
      setFilterParams({ ...filterParams, numStops });
    },
    [filterParams, setFilterParams],
  );

  const setPrice = React.useCallback(
    (value: number[]) => {
      const tmp = { ...filterParams, price: value };
      setFilterParams(tmp);
    },
    [filterParams, setFilterParams],
  );

  const setAirline = React.useCallback(
    (event: React.ChangeEvent<HTMLInputElement>, value: some) => {
      const airline = [...filterParams.airline];
      if (event.target.checked) {
        airline.push(value.id);
      } else {
        remove(airline, obj => obj === value.id);
      }
      const tmp = { ...filterParams, airline };

      if (!isChangeItinerary) {
        setFilterParams(tmp);
      }
    },
    [filterParams, isChangeItinerary, setFilterParams],
  );

  const setTimeTakeOff = React.useCallback(
    (value: some, checked: boolean) => {
      const timeTakeOff = [...filterParams.timeTakeOff];
      if (checked) {
        timeTakeOff.push(value);
      } else {
        remove(timeTakeOff, obj => obj.id === value.id);
      }

      const tmp = { ...filterParams, timeTakeOff };
      setFilterParams(tmp);
    },
    [filterParams, setFilterParams],
  );

  const setTimeLand = React.useCallback(
    (value: some, checked: boolean) => {
      const timeLand = [...filterParams.timeLand];
      if (checked) {
        timeLand.push(value);
      } else {
        remove(timeLand, obj => obj.id === value.id);
      }
      const tmp = { ...filterParams, timeLand };
      setFilterParams(tmp);
    },
    [filterParams, setFilterParams],
  );

  const setFlightDuration = React.useCallback(
    (value: number[]) => {
      const tmp = { ...filterParams, flightDuration: value };
      setFilterParams(tmp);
    },
    [filterParams, setFilterParams],
  );

  const setTransitDuration = React.useCallback(
    (value: number[]) => {
      const tmp = { ...filterParams, transitDuration: value };
      setFilterParams(tmp);
    },
    [filterParams, setFilterParams],
  );

  const { showNumberStop, showLeg, fromAndTime, airline } = state;
  return (
    <div>
      <div>
        <div className="card-background" style={{ marginBottom: 12 }}>
          <Button variant="text" fullWidth style={{ padding: '8px 12px' }} onClick={resetFilter}>
            <IconFunnel />
            &nbsp; &nbsp;
            <Typography variant="subtitle1">
              <FormattedMessage id="flight.result.filter" />
            </Typography>
            <div style={{ flex: 1 }} />
            <RefreshIcon color="primary" fontSize="small" /> &nbsp;
            <Typography variant="subtitle2" color="primary">
              <FormattedMessage id="reset" />
            </Typography>
          </Button>
        </div>
        <div>
          <Typography
            variant="subtitle2"
            style={{ display: 'flex', justifyContent: 'space-between', paddingBottom: '8px' }}
          >
            <span>
              <FormattedMessage id="flight.filter.showResult.price" />
            </span>
            <Typography variant="subtitle2" color="textSecondary" component="span">
              <FormattedNumber value={filterParams.price[0]} />
              &nbsp;
              <FormattedMessage id="currency" />
              &nbsp;-&nbsp;
              <FormattedNumber value={filterParams.price[1]} />
              &nbsp;
              <FormattedMessage id="currency" />
            </Typography>
          </Typography>
          <div style={{ margin: '0 12px' }}>
            <Slider
              key={JSON.stringify(filterParams.price)}
              defaultValue={filterParams.price.slice()}
              min={0}
              step={PRICE_STEP}
              max={
                filterParams.isFarePrice
                  ? roundUp(filterParams.filters?.maxFarePrice, PRICE_STEP)
                  : roundUp(filterParams.filters?.maxPrice, PRICE_STEP)
              }
              onChangeCommitted={(e: any, value: any) => setPrice(value)}
              ValueLabelComponent={PriceLabelComponent}
            />
          </div>
        </div>
      </div>
      <div>
        <div>
          <DropDownHeader
            onClick={() => setState(one => ({ ...one, showNumberStop: !one.showNumberStop }))}
          >
            <Typography variant="subtitle2" style={DropDownStyle}>
              <FormattedMessage id="flight.filter.numberStop" />
              <ExpandLessIcon
                style={{
                  transition: 'all 300ms',
                  transform: showNumberStop ? 'rotate(0deg)' : 'rotate(180deg)',
                }}
              />
            </Typography>
            <Divider />
          </DropDownHeader>
          <Collapse in={showNumberStop}>
            {data?.filters?.numStops?.map((item: number, index: number) => (
              <FormControlLabel
                key={item}
                style={{ outline: 'none', margin: '0', color: GREY }}
                checked={filterParams.numStops.findIndex(v => v === item) !== -1}
                labelPlacement="end"
                label={
                  <Typography variant="body2">
                    <FormattedMessage
                      id={
                        item === 0
                          ? 'flight.filter.numberStop.noStop'
                          : 'flight.filter.numberStop.stop'
                      }
                      values={{ num: item }}
                    />
                  </Typography>
                }
                control={
                  <Checkbox
                    size="medium"
                    onChange={event => setNumStop(event, item)}
                    value={item}
                    color="secondary"
                  />
                }
              />
            ))}
          </Collapse>
        </div>
      </div>
      <div>
        <div>
          <Collapse in={showLeg}>
            <div style={{ padding: '14px 0px 6px' }}>
              <Typography variant="subtitle2">
                <FormattedMessage id="flight.filter.takeOff" />
              </Typography>
              <div style={{ display: 'flex', flexWrap: 'wrap', alignItems: 'center' }}>
                {TIME_TAKE_OFF_AND_LAND.map(item => (
                  <div style={{ padding: '6px' }} key={item.id}>
                    <CheckButton
                      id="Flight_listing.Flight_Filter_Flight time"
                      active={filterParams.timeTakeOff.indexOf(item) !== -1}
                      onClick={() =>
                        setTimeTakeOff(item, filterParams.timeTakeOff.indexOf(item) === -1)
                      }
                    >
                      <Typography variant="body2">{item.text}</Typography>
                    </CheckButton>
                  </div>
                ))}
              </div>
            </div>
            <div style={{ padding: '14px 0px 6px' }}>
              <Typography variant="subtitle2">
                <FormattedMessage id="flight.filter.land" />
              </Typography>
              <div style={{ display: 'flex', flexWrap: 'wrap', alignItems: 'center' }}>
                {TIME_TAKE_OFF_AND_LAND.map(item => (
                  <div style={{ padding: '6px' }} key={item.id}>
                    <CheckButton
                      id="Flight_listing.Flight_Filter_Take off"
                      active={filterParams.timeLand.indexOf(item) !== -1}
                      onClick={() => setTimeLand(item, filterParams.timeLand.indexOf(item) === -1)}
                    >
                      <Typography variant="body2">{item.text}</Typography>
                    </CheckButton>
                  </div>
                ))}
              </div>
            </div>
          </Collapse>
        </div>
      </div>

      <div>
        <div>
          <DropDownHeader
            onClick={() => setState(one => ({ ...one, fromAndTime: !one.fromAndTime }))}
          >
            <Typography variant="subtitle2" style={DropDownStyle}>
              <FormattedMessage id="flight.filter.flightTime" />
              <ExpandLessIcon
                style={{
                  transition: 'all 300ms',
                  transform: fromAndTime ? 'rotate(0deg)' : 'rotate(180deg)',
                }}
              />
            </Typography>
            <Divider />
          </DropDownHeader>
          <Collapse in={fromAndTime}>
            <div style={{ padding: '12px 0' }}>
              <Typography
                variant="subtitle2"
                style={{ display: 'flex', justifyContent: 'space-between', paddingBottom: '8px' }}
              >
                <span>
                  <FormattedMessage id="flight.filter.leg" />
                </span>
                <Typography variant="subtitle2" color="textSecondary" component="span">
                  {filterParams.flightDuration.length
                    ? durationMillisecondToHour(filterParams.flightDuration[0])
                    : '0h'}
                  &nbsp;-&nbsp;
                  {durationMillisecondToHour(
                    filterParams.flightDuration.length
                      ? filterParams.flightDuration[1]
                      : maxFlightDuration,
                  )}
                </Typography>
              </Typography>
              <div style={{ margin: '0 12px' }}>
                <Slider
                  key={`${filterParams.flightDuration.join(',')}${maxFlightDuration}`}
                  defaultValue={
                    filterParams.flightDuration
                      ? filterParams.flightDuration
                      : [0, maxFlightDuration]
                  }
                  min={0}
                  max={maxFlightDuration}
                  onChangeCommitted={(e: any, value: any) => {
                    setFlightDuration(value);
                  }}
                  ValueLabelComponent={TimeLabelComponent}
                />
              </div>
            </div>
            <div style={{ padding: '12px 0' }}>
              <Typography
                variant="subtitle2"
                style={{ display: 'flex', justifyContent: 'space-between', paddingBottom: '8px' }}
              >
                <span>
                  <FormattedMessage id="flight.filter.transit" />
                </span>
                <Typography variant="subtitle2" color="textSecondary" component="span">
                  {filterParams.transitDuration.length
                    ? durationMillisecondToHour(filterParams.transitDuration[0])
                    : '0h'}
                  &nbsp;-&nbsp;
                  {durationMillisecondToHour(
                    filterParams.transitDuration.length
                      ? filterParams.transitDuration[1]
                      : maxTransitDuration,
                  )}
                </Typography>
              </Typography>
              <div style={{ margin: '0 12px' }}>
                <Slider
                  key={`${filterParams.transitDuration.join(',')}${maxTransitDuration}`}
                  defaultValue={
                    filterParams.transitDuration.length
                      ? filterParams.transitDuration
                      : [0, maxTransitDuration]
                  }
                  min={0}
                  max={maxTransitDuration}
                  disabled={!maxTransitDuration}
                  onChangeCommitted={(e: any, value: any) => setTransitDuration(value)}
                  ValueLabelComponent={TimeLabelComponent}
                />
              </div>
            </div>
          </Collapse>
        </div>
      </div>

      <div>
        <div>
          <DropDownHeader onClick={() => setState(one => ({ ...one, airline: !one.airline }))}>
            <Typography variant="subtitle2" style={DropDownStyle}>
              <FormattedMessage id="flight.filter.airline" />
              <ExpandLessIcon
                style={{
                  transition: 'all 300ms',
                  transform: airline ? 'rotate(0deg)' : 'rotate(180deg)',
                }}
              />
            </Typography>
            <Divider />
          </DropDownHeader>
          <Collapse in={airline}>
            {data?.airlines?.map((item: some) => (
              <FormControlLabel
                key={item.id}
                style={{ outline: 'none', margin: '0', width: '100%', color: GREY }}
                label={item.name}
                labelPlacement="end"
                control={
                  <Checkbox
                    id="Flight_listing.Flight_Filter_Brand"
                    checked={!!filterParams.airline.find(one => one === item.id)}
                    size="medium"
                    onChange={event => setAirline(event, item)}
                    value={item.id}
                    color="secondary"
                  />
                }
              />
            ))}
          </Collapse>
        </div>
      </div>
    </div>
  );
};

export default FlightFilter;
