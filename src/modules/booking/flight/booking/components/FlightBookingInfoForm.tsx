/* eslint-disable react/no-danger */
import {
  Button,
  Checkbox,
  Container,
  FormControlLabel,
  IconButton,
  InputAdornment,
  Paper,
  Typography,
  useMediaQuery,
  useTheme,
} from '@material-ui/core';
import WarningRoundedIcon from '@material-ui/icons/WarningRounded';
import { FormikErrors, useFormik } from 'formik';
import moment from 'moment';
import React from 'react';
import { FormattedMessage, FormattedNumber, useIntl } from 'react-intl';
import { useDispatch } from 'react-redux';
import { Action } from 'redux';
import { ThunkDispatch } from 'redux-thunk';
import * as yup from 'yup';
import { some } from '../../../../../constants';
import { HEADER_HEIGHT } from '../../../../../layout/constants';
import { DATE_FORMAT, DATE_FORMAT_BACK_END } from '../../../../../models/moment';
import { AppState } from '../../../../../redux/reducers';
import { ReactComponent as IconCalender } from '../../../../../svg/ic_calendar.svg';
import BirthDayField from '../../../../common/components/BirthDayField';
import { Col, Row } from '../../../../common/components/elements';
import FormControlAutoComplete from '../../../../common/components/FormControlAutoComplete';
import FormControlTextField from '../../../../common/components/FormControlTextField';
import { goBackAction } from '../../../../common/redux/reducer';
import AsideBound from '../../../common/AsideBound';
import { GenderField } from '../../../common/element';
import ListContactDialog from '../../../common/ListContactDialog';
import { scrollTo } from '../../../utils';
import FlightInfoBox from '../../common/FlightInfoBox';
import FlightTicketDialog from '../../common/FlightTicketDialog';
import { FlightInfo, TravellerInfo } from '../utils';

interface Props {
  info: FlightInfo;
  listCounties: some[];
  setInfo(info: FlightInfo): void;
}

const FlightBookingInfoForm: React.FC<Props> = props => {
  const { info, setInfo, listCounties } = props;
  const dispatch = useDispatch<ThunkDispatch<AppState, null, Action<string>>>();
  const intl = useIntl();
  const [seeDetail, setSeeDetail] = React.useState(false);
  const theme = useTheme();
  const isTablet = useMediaQuery(theme.breakpoints.down('md'));

  const styleCS: React.CSSProperties = { width: isTablet ? 225 : 272 };
  const needPassport = info.outbound.ticket
    ? info.outbound.ticket.outbound?.ticketdetail.needPassport
    : false;

  const departureDate = info.outbound.ticket
    ? info.outbound.ticket?.outbound.arrivalTime
    : undefined;
  const returnDate = info.inbound.ticket
    ? info.inbound.ticket?.outbound.arrivalTime
    : info.outbound.ticket?.outbound.arrivalTime;

  const bookingInfoSchema = React.useMemo(() => {
    return yup.object().shape({
      outbound: yup.mixed().notRequired(),
      travellersInfo: yup.object().shape({
        adults: yup.array().of(
          yup.object().shape({
            fullName: yup
              .string()
              .trim()
              .test({
                name: 'fullName',
                message: intl.formatMessage({ id: 'fullNameValid' }),
                test: value => {
                  return value ? value.split(' ').length > 1 : true;
                },
              })
              .required(intl.formatMessage({ id: 'required' })),
            birthday: yup
              .string()
              .trim()
              .test('birthday', intl.formatMessage({ id: 'required' }), value => {
                return needPassport ? !!value?.trim() : true;
              })
              .test('birthday', intl.formatMessage({ id: 'birthdayValid' }), value => {
                const birthday = moment(value, DATE_FORMAT_BACK_END, true);
                const age = moment(departureDate)
                  .startOf('day')
                  .diff(birthday, 'years');
                return needPassport ? birthday.isValid() === true && age >= 12 : true;
              }),
            gender: yup
              .string()
              .trim()
              .required(intl.formatMessage({ id: 'required' })),
            passportInfo: yup.object().when('outbound', {
              is: () => needPassport,
              then: yup.object().shape({
                passport: yup
                  .string()
                  .trim()
                  .required(intl.formatMessage({ id: 'required' })),
                passportExpiredDate: yup
                  .string()
                  .trim()
                  .required(intl.formatMessage({ id: 'required' }))
                  .test(
                    'passportExpiredDate',
                    intl.formatMessage({ id: 'passportExpiredDateValid' }),
                    value => {
                      const date = moment(value, DATE_FORMAT_BACK_END, true);
                      return (
                        date.isValid() === true &&
                        date.isSameOrAfter(moment().startOf('day')) === true &&
                        departureDate &&
                        date.isSameOrAfter(moment(departureDate)) === true
                      );
                    },
                  ),
                passportCountry: yup
                  .mixed()
                  .nullable()
                  .required(intl.formatMessage({ id: 'required' })),
                nationalityCountry: yup
                  .mixed()
                  .nullable()
                  .required(intl.formatMessage({ id: 'required' })),
              }),
              otherwise: yup.object().notRequired(),
            }),
          }),
        ),
        children: yup.array().of(
          yup.object().shape({
            fullName: yup
              .string()
              .trim()
              .test({
                name: 'fullName',
                message: intl.formatMessage({ id: 'fullNameValid' }),
                test: value => {
                  return value ? value.split(' ').length > 1 : true;
                },
              })
              .required(intl.formatMessage({ id: 'required' })),
            birthday: yup
              .string()
              .trim()
              .required(intl.formatMessage({ id: 'required' }))
              .test('birthday', intl.formatMessage({ id: 'birthdayValid' }), value => {
                const birthday = moment(value, DATE_FORMAT_BACK_END, true);
                const ageStart = moment(departureDate)
                  .startOf('day')
                  .diff(birthday, 'years');
                const ageEnd = moment(returnDate)
                  .startOf('day')
                  .diff(birthday, 'years');
                return !!(birthday.isValid() === true && ageStart >= 2 && ageEnd < 12);
              }),
            gender: yup
              .string()
              .trim()
              .required(intl.formatMessage({ id: 'required' })),
            passportInfo: yup.object().when('outbound', {
              is: () => needPassport,
              then: yup.object().shape({
                passport: yup
                  .string()
                  .trim()
                  .required(intl.formatMessage({ id: 'required' })),
                passportExpiredDate: yup
                  .string()
                  .trim()
                  .required(intl.formatMessage({ id: 'required' }))
                  .test(
                    'passportExpiredDate',
                    intl.formatMessage({ id: 'passportExpiredDateValid' }),
                    value => {
                      const date = moment(value, DATE_FORMAT_BACK_END, true);
                      return (
                        date.isValid() === true &&
                        date.isSameOrAfter(moment().startOf('day')) === true &&
                        departureDate &&
                        date.isSameOrAfter(moment(departureDate)) === true
                      );
                    },
                  ),
                passportCountry: yup
                  .mixed()
                  .nullable()
                  .required(intl.formatMessage({ id: 'required' })),
                nationalityCountry: yup
                  .mixed()
                  .nullable()
                  .required(intl.formatMessage({ id: 'required' })),
              }),
              otherwise: yup.object().notRequired(),
            }),
          }),
        ),
        babies: yup.array().of(
          yup.object().shape({
            fullName: yup
              .string()
              .trim()
              .test({
                name: 'fullName',
                message: intl.formatMessage({ id: 'fullNameValid' }),
                test: value => {
                  return value ? value.split(' ').length > 1 : true;
                },
              })
              .required(intl.formatMessage({ id: 'required' })),
            birthday: yup
              .string()
              .trim()
              .required(intl.formatMessage({ id: 'required' }))
              .test('birthday', intl.formatMessage({ id: 'birthdayValid' }), value => {
                const birthday = moment(value, DATE_FORMAT_BACK_END, true);
                const age = moment(returnDate)
                  .startOf('day')
                  .diff(birthday, 'months');
                return !!(birthday.isValid() === true && age < 24 && age >= 0);
              }),
            gender: yup
              .string()
              .trim()
              .required(intl.formatMessage({ id: 'required' })),
            passportInfo: yup.object().when('outbound', {
              is: () => needPassport,
              then: yup.object().shape({
                passport: yup
                  .string()
                  .trim()
                  .required(intl.formatMessage({ id: 'required' })),
                passportExpiredDate: yup
                  .string()
                  .trim()
                  .required(intl.formatMessage({ id: 'required' }))
                  .test(
                    'passportExpiredDate',
                    intl.formatMessage({ id: 'passportExpiredDateValid' }),
                    value => {
                      const date = moment(value, DATE_FORMAT_BACK_END, true);
                      return (
                        date.isValid() === true &&
                        date.isSameOrAfter(moment().startOf('day')) === true &&
                        departureDate &&
                        date.isSameOrAfter(moment(departureDate)) === true
                      );
                    },
                  ),
                passportCountry: yup
                  .mixed()
                  .nullable()
                  .required(intl.formatMessage({ id: 'required' })),
                nationalityCountry: yup
                  .mixed()
                  .nullable()
                  .required(intl.formatMessage({ id: 'required' })),
              }),
              otherwise: yup.object().notRequired(),
            }),
          }),
        ),
      }),
    });
  }, [intl, needPassport, departureDate, returnDate]);

  const formik = useFormik({
    initialValues: info,
    onSubmit: values => {
      setInfo(values);
    },
    validationSchema: bookingInfoSchema,
  });

  React.useEffect(() => {
    formik.setValues(info, true);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [info]);

  React.useEffect(() => {
    if (formik.submitCount > 0 && formik.isSubmitting && !formik.isValid) {
      scrollTo(Object.keys(formik.errors)[0], HEADER_HEIGHT + 16);
    }
  }, [formik]);

  return (
    <Container style={{ flex: 1, display: 'flex', marginBottom: 32 }}>
      <form autoComplete="off" onSubmit={formik.handleSubmit} style={{ flex: 1 }}>
        <Paper
          id="travellersInfo"
          variant="outlined"
          style={{ overflow: 'hidden', padding: '12px 16px' }}
        >
          <Typography variant="h6">
            <FormattedMessage id="flight.flightBookingInfo" />
          </Typography>
          <Row style={{ marginBottom: 16, marginTop: 8 }}>
            <WarningRoundedIcon color="error" style={{ width: 20, height: 20, marginRight: 8 }} />
            <Typography variant="body2" color="error">
              <FormattedMessage id="flight.inputInfoInstructions" />
            </Typography>
          </Row>
          {formik.values.travellersInfo?.adults.map((obj: TravellerInfo, index: number) => (
            <Col key={index} style={{ marginBottom: 16 }}>
              <Row style={{ marginBottom: 16 }}>
                <Typography variant="subtitle1">
                  {index + 1}.<FormattedMessage id="flight.adult" values={{ num: index + 1 }} />
                </Typography>
                <ListContactDialog
                  exceptionList={[
                    ...formik.values.travellersInfo?.adults,
                    ...formik.values.travellersInfo?.children,
                    ...formik.values.travellersInfo?.babies,
                  ]}
                  onSelect={one => {
                    formik.setFieldValue(
                      'travellersInfo',
                      {
                        ...formik.values.travellersInfo,
                        adults: formik.values.travellersInfo?.adults.map((v, i) => {
                          if (i === index) {
                            return {
                              ...v,
                              fullName: one.name,
                              birthday: one.birthday,
                              gender: one.gender ? 'm' : 'f',
                              userId: one.id,
                            };
                          }
                          return v;
                        }),
                      },
                      true,
                    );
                  }}
                />
              </Row>
              <Row style={{ flexWrap: 'wrap' }}>
                <FormControlTextField
                  label={intl.formatMessage({ id: 'flight.fullName' })}
                  placeholder={intl.formatMessage({ id: 'flight.fullNameEx' })}
                  formControlStyle={styleCS}
                  value={obj.fullName}
                  readOnly
                  errorMessage={
                    (formik.errors.travellersInfo?.adults?.[index] as FormikErrors<TravellerInfo>)
                      ?.fullName && formik.submitCount
                      ? (formik.errors.travellersInfo?.adults?.[index] as FormikErrors<
                          TravellerInfo
                        >)?.fullName
                      : undefined
                  }
                />
                {needPassport && (
                  <FormControlTextField
                    formControlStyle={styleCS}
                    label={
                      <Typography variant="subtitle2" component="span">
                        <FormattedMessage id="birthday" />
                        &nbsp;
                        <Typography variant="caption" component="span">
                          <FormattedMessage id="flight.adultBirthday" />
                        </Typography>
                      </Typography>
                    }
                    fullWidth
                    value={
                      obj.birthday
                        ? moment(obj.birthday, DATE_FORMAT_BACK_END).format(DATE_FORMAT)
                        : undefined
                    }
                    placeholder={DATE_FORMAT.toLowerCase()}
                    readOnly
                    errorMessage={
                      (formik.errors.travellersInfo?.adults?.[index] as FormikErrors<TravellerInfo>)
                        ?.birthday && formik.submitCount
                        ? (formik.errors.travellersInfo?.adults?.[index] as FormikErrors<
                            TravellerInfo
                          >)?.birthday
                        : undefined
                    }
                    endAdornment={
                      <InputAdornment position="end" style={{ marginRight: 8 }}>
                        <IconButton size="small" edge="start" tabIndex={-1}>
                          <IconCalender />
                        </IconButton>
                      </InputAdornment>
                    }
                  />
                )}

                <GenderField gender={obj.gender} />
              </Row>
              {needPassport && (
                <Row style={{ flexWrap: 'wrap' }}>
                  <FormControlTextField
                    label={intl.formatMessage({ id: 'flight.passport' })}
                    placeholder={intl.formatMessage({ id: 'flight.passportEx' })}
                    formControlStyle={styleCS}
                    value={obj.passportInfo.passport}
                    onChange={e =>
                      formik.setFieldValue('travellersInfo', {
                        ...formik.values.travellersInfo,
                        adults: formik.values.travellersInfo?.adults.map((v, i) => {
                          if (i === index) {
                            return {
                              ...v,
                              passportInfo: { ...v.passportInfo, passport: e.target.value },
                            };
                          }
                          return v;
                        }, (formik.errors.travellersInfo?.adults?.[index] as FormikErrors<TravellerInfo>)?.passportInfo?.passport),
                      })
                    }
                    errorMessage={
                      (formik.errors.travellersInfo?.adults?.[index] as FormikErrors<TravellerInfo>)
                        ?.passportInfo?.passport && formik.submitCount
                        ? (formik.errors.travellersInfo?.adults?.[index] as FormikErrors<
                            TravellerInfo
                          >)?.passportInfo?.passport
                        : undefined
                    }
                  />
                  <BirthDayField
                    disablePast
                    date={
                      obj.passportInfo.passportExpiredDate
                        ? moment(obj.passportInfo.passportExpiredDate, DATE_FORMAT_BACK_END)
                        : undefined
                    }
                    update={value => {
                      formik.setFieldValue(
                        'travellersInfo',
                        {
                          ...formik.values.travellersInfo,
                          adults: formik.values.travellersInfo?.adults.map((v, i) => {
                            if (i === index) {
                              return {
                                ...v,
                                passportInfo: {
                                  ...v.passportInfo,
                                  passportExpiredDate: value?.format(DATE_FORMAT_BACK_END),
                                },
                              };
                            }
                            return v;
                          }),
                        },
                        !!(formik.errors.travellersInfo?.adults?.[index] as FormikErrors<
                          TravellerInfo
                        >)?.passportInfo?.passportExpiredDate,
                      );
                    }}
                    label={intl.formatMessage({ id: 'flight.passportExpired' })}
                    inputStyle={styleCS}
                    errorMessage={
                      (formik.errors.travellersInfo?.adults?.[index] as FormikErrors<TravellerInfo>)
                        ?.passportInfo?.passportExpiredDate && formik.submitCount
                        ? (formik.errors.travellersInfo?.adults?.[index] as FormikErrors<
                            TravellerInfo
                          >)?.passportInfo?.passportExpiredDate
                        : undefined
                    }
                  />
                  <FormControlAutoComplete
                    label={intl.formatMessage({ id: 'flight.passportCountry' })}
                    placeholder={intl.formatMessage({ id: 'flight.selectPassportCountry' })}
                    value={obj.passportInfo.passportCountry}
                    formControlStyle={styleCS}
                    onChange={(e: any, value: some | null) => {
                      formik.setFieldValue(
                        'travellersInfo',
                        {
                          ...formik.values.travellersInfo,
                          adults: formik.values.travellersInfo?.adults.map((v, i) => {
                            if (i === index) {
                              return {
                                ...v,
                                passportInfo: { ...v.passportInfo, passportCountry: value },
                              };
                            }
                            return v;
                          }),
                        },
                        !!(formik.errors.travellersInfo?.adults?.[index] as FormikErrors<
                          TravellerInfo
                        >)?.passportInfo?.passportCountry,
                      );
                    }}
                    getOptionSelected={(option: some, value: some) => {
                      return option.id === value.id;
                    }}
                    getOptionLabel={(v: some) => `${v.name} (${v.code})` || ''}
                    options={listCounties}
                    errorMessage={
                      (formik.errors.travellersInfo?.adults?.[index] as FormikErrors<TravellerInfo>)
                        ?.passportInfo?.passportCountry && formik.submitCount
                        ? (formik.errors.travellersInfo?.adults?.[index] as FormikErrors<
                            TravellerInfo
                          >)?.passportInfo?.passportCountry
                        : undefined
                    }
                  />

                  <FormControlAutoComplete
                    label={intl.formatMessage({ id: 'flight.nationalityCountry' })}
                    placeholder={intl.formatMessage({ id: 'flight.selectNationalityCountry' })}
                    value={obj.passportInfo.nationalityCountry}
                    formControlStyle={styleCS}
                    onChange={(e: any, value: some | null) => {
                      formik.setFieldValue(
                        'travellersInfo',
                        {
                          ...formik.values.travellersInfo,
                          adults: formik.values.travellersInfo?.adults.map((v, i) => {
                            if (i === index) {
                              return {
                                ...v,
                                passportInfo: { ...v.passportInfo, nationalityCountry: value },
                              };
                            }
                            return v;
                          }),
                        },
                        !!(formik.errors.travellersInfo?.adults?.[index] as FormikErrors<
                          TravellerInfo
                        >)?.passportInfo?.nationalityCountry,
                      );
                    }}
                    getOptionSelected={(option: some, value: some) => {
                      return option.id === value.id;
                    }}
                    getOptionLabel={(v: some) => `${v.name} (${v.code})` || ''}
                    options={listCounties}
                    errorMessage={
                      (formik.errors.travellersInfo?.adults?.[index] as FormikErrors<TravellerInfo>)
                        ?.passportInfo?.nationalityCountry && formik.submitCount
                        ? (formik.errors.travellersInfo?.adults?.[index] as FormikErrors<
                            TravellerInfo
                          >)?.passportInfo?.nationalityCountry
                        : undefined
                    }
                  />
                </Row>
              )}
              <Row style={{ flexWrap: 'wrap' }}>
                {formik.values.outbound.ticket?.outbound.baggages &&
                formik.values.outbound.ticket?.outbound.baggages[0] &&
                formik.values.outbound.extraBaggages ? (
                  <FormControlAutoComplete
                    id={`flightBookingInfo.More_packages${index}`}
                    label={intl.formatMessage({ id: 'flight.outBaggages' })}
                    value={formik.values.outbound?.extraBaggages?.[index] || null}
                    options={formik.values.outbound.ticket?.outbound.baggages}
                    getOptionSelected={(option: some, value: some) => {
                      return option.id === value.id;
                    }}
                    getOptionLabel={(v: some) =>
                      `${v.name} - ${intl.formatNumber(v.price)} ${intl.formatMessage({
                        id: 'currency',
                      })}`
                    }
                    onChange={(e: any, value?: any) => {
                      formik.setFieldValue('outbound', {
                        ...formik.values.outbound,
                        extraBaggages: formik.values.outbound?.extraBaggages?.map((v, i) => {
                          if (i === index) {
                            return value;
                          }
                          return v;
                        }),
                      });
                    }}
                    formControlStyle={styleCS}
                    optional
                    disableClearable
                    readOnly
                  />
                ) : (
                  <Col style={{ ...styleCS, marginRight: 16 }}>
                    <Typography variant="body2">
                      <FormattedMessage id="flight.outBaggages" />
                    </Typography>
                    <div style={{ minHeight: 64, paddingTop: 14, boxSizing: 'border-box' }}>
                      <Typography variant="body2" color="textSecondary">
                        <FormattedMessage id="flight.noSupportBuyBaggage" />
                      </Typography>
                    </div>
                  </Col>
                )}
                {formik.values.inbound.ticket && (
                  <>
                    {formik.values.inbound.ticket?.outbound.baggages &&
                    formik.values.inbound.ticket?.outbound.baggages[0] &&
                    formik.values.inbound.extraBaggages ? (
                      <FormControlAutoComplete
                        label={intl.formatMessage({ id: 'flight.inBaggages' })}
                        value={formik.values.inbound?.extraBaggages?.[index]}
                        options={formik.values.inbound.ticket?.outbound.baggages}
                        getOptionSelected={(option: some, value: some) => {
                          return option.id === value.id;
                        }}
                        getOptionLabel={(v: some) =>
                          `${v.name} - ${intl.formatNumber(v.price)} ${intl.formatMessage({
                            id: 'currency',
                          })}`
                        }
                        onChange={(e: any, value?: any) => {
                          formik.setFieldValue('inbound', {
                            ...formik.values.inbound,
                            extraBaggages: formik.values.inbound?.extraBaggages?.map((v, i) => {
                              if (i === index) {
                                return value;
                              }
                              return v;
                            }),
                          });
                        }}
                        formControlStyle={styleCS}
                        optional
                        disableClearable
                        readOnly
                      />
                    ) : (
                      <Col style={{ ...styleCS, marginRight: 16 }}>
                        <Typography variant="body2">
                          <FormattedMessage id="flight.inBaggages" />
                        </Typography>
                        <div style={{ minHeight: 64, paddingTop: 14, boxSizing: 'border-box' }}>
                          <Typography variant="body2" color="textSecondary">
                            <FormattedMessage id="flight.noSupportBuyBaggage" />
                          </Typography>
                        </div>
                      </Col>
                    )}
                  </>
                )}
              </Row>
            </Col>
          ))}
          {formik.values.travellersInfo?.children.map((obj: TravellerInfo, index: number) => {
            const indexTmp = index + (formik.values.travellersInfo?.adults.length || 0);
            return (
              <Col key={index} style={{ marginBottom: 12, marginTop: 16 }}>
                <Row style={{ marginBottom: 16 }}>
                  <Typography variant="subtitle1">
                    {index + 1 + (formik.values?.travellersInfo?.adults?.length || 0)}.
                    <FormattedMessage id="flight.children" values={{ num: index + 1 }} />
                  </Typography>
                  <ListContactDialog
                    exceptionList={[
                      ...formik.values.travellersInfo?.adults,
                      ...formik.values.travellersInfo?.children,
                      ...formik.values.travellersInfo?.babies,
                    ]}
                    onSelect={one => {
                      formik.setFieldValue(
                        'travellersInfo',
                        {
                          ...formik.values.travellersInfo,
                          children: formik.values.travellersInfo?.children.map((v, i) => {
                            if (i === index) {
                              return {
                                ...v,
                                fullName: one.name,
                                birthday: one.birthday,
                                gender: one.gender ? 'm' : 'f',
                                userId: one.id,
                              };
                            }
                            return v;
                          }),
                        },
                        true,
                      );
                    }}
                  />
                </Row>
                <Row style={{ flexWrap: 'wrap' }}>
                  <FormControlTextField
                    label={intl.formatMessage({ id: 'flight.fullName' })}
                    placeholder={intl.formatMessage({ id: 'flight.fullNameEx' })}
                    formControlStyle={styleCS}
                    value={obj.fullName}
                    readOnly
                    errorMessage={
                      (formik.errors.travellersInfo?.children?.[index] as FormikErrors<
                        TravellerInfo
                      >)?.fullName && formik.submitCount
                        ? (formik.errors.travellersInfo?.children?.[index] as FormikErrors<
                            TravellerInfo
                          >)?.fullName
                        : undefined
                    }
                  />
                  <FormControlTextField
                    formControlStyle={styleCS}
                    label={
                      <Typography variant="subtitle2" component="span">
                        <FormattedMessage id="birthday" />
                        &nbsp;
                        <Typography variant="caption" component="span">
                          <FormattedMessage id="flight.childBirthday" />
                        </Typography>
                      </Typography>
                    }
                    fullWidth
                    value={
                      obj.birthday
                        ? moment(obj.birthday, DATE_FORMAT_BACK_END).format(DATE_FORMAT)
                        : undefined
                    }
                    placeholder={DATE_FORMAT.toLowerCase()}
                    readOnly
                    errorMessage={
                      (formik.errors.travellersInfo?.children?.[index] as FormikErrors<
                        TravellerInfo
                      >)?.birthday && formik.submitCount
                        ? (formik.errors.travellersInfo?.children?.[index] as FormikErrors<
                            TravellerInfo
                          >)?.birthday
                        : undefined
                    }
                    endAdornment={
                      <InputAdornment position="end" style={{ marginRight: 8 }}>
                        <IconButton size="small" edge="start" tabIndex={-1}>
                          <IconCalender />
                        </IconButton>
                      </InputAdornment>
                    }
                  />
                  <GenderField
                    gender={obj.gender}
                    onChange={e =>
                      formik.setFieldValue(
                        'travellersInfo',
                        {
                          ...formik.values.travellersInfo,
                          children: formik.values.travellersInfo?.children.map((v, i) => {
                            if (i === index) {
                              return { ...v, gender: e };
                            }
                            return v;
                          }),
                        },
                        false,
                      )
                    }
                  />

                  {needPassport && (
                    <Row style={{ flexWrap: 'wrap' }}>
                      <FormControlTextField
                        label={intl.formatMessage({ id: 'flight.passport' })}
                        placeholder={intl.formatMessage({ id: 'flight.passportEx' })}
                        formControlStyle={styleCS}
                        value={obj.passportInfo.passport}
                        onChange={e =>
                          formik.setFieldValue(
                            'travellersInfo',
                            {
                              ...formik.values.travellersInfo,
                              children: formik.values.travellersInfo?.children.map((v, i) => {
                                if (i === index) {
                                  return {
                                    ...v,
                                    passportInfo: { ...v.passportInfo, passport: e.target.value },
                                  };
                                }
                                return v;
                              }),
                            },
                            !!(formik.errors.travellersInfo?.children?.[index] as FormikErrors<
                              TravellerInfo
                            >)?.passportInfo?.passport,
                          )
                        }
                        errorMessage={
                          (formik.errors.travellersInfo?.children?.[index] as FormikErrors<
                            TravellerInfo
                          >)?.passportInfo?.passport && formik.submitCount
                            ? (formik.errors.travellersInfo?.children?.[index] as FormikErrors<
                                TravellerInfo
                              >)?.passportInfo?.passport
                            : undefined
                        }
                      />
                      <BirthDayField
                        disablePast
                        date={
                          obj.passportInfo.passportExpiredDate
                            ? moment(obj.passportInfo.passportExpiredDate, DATE_FORMAT_BACK_END)
                            : undefined
                        }
                        update={value => {
                          formik.setFieldValue(
                            'travellersInfo',
                            {
                              ...formik.values.travellersInfo,
                              children: formik.values.travellersInfo?.children.map((v, i) => {
                                if (i === index) {
                                  return {
                                    ...v,
                                    passportInfo: {
                                      ...v.passportInfo,
                                      passportExpiredDate: value?.format(DATE_FORMAT_BACK_END),
                                    },
                                  };
                                }
                                return v;
                              }),
                            },
                            !!(formik.errors.travellersInfo?.children?.[index] as FormikErrors<
                              TravellerInfo
                            >)?.passportInfo?.passportExpiredDate,
                          );
                        }}
                        label={intl.formatMessage({ id: 'flight.passportExpired' })}
                        inputStyle={styleCS}
                        errorMessage={
                          (formik.errors.travellersInfo?.children?.[index] as FormikErrors<
                            TravellerInfo
                          >)?.passportInfo?.passportExpiredDate && formik.submitCount
                            ? (formik.errors.travellersInfo?.children?.[index] as FormikErrors<
                                TravellerInfo
                              >)?.passportInfo?.passportExpiredDate
                            : undefined
                        }
                      />
                      <FormControlAutoComplete
                        label={intl.formatMessage({ id: 'flight.passportCountry' })}
                        placeholder={intl.formatMessage({ id: 'flight.selectPassportCountry' })}
                        value={obj.passportInfo.passportCountry}
                        formControlStyle={styleCS}
                        onChange={(e: any, value: some | null) => {
                          formik.setFieldValue(
                            'travellersInfo',
                            {
                              ...formik.values.travellersInfo,
                              children: formik.values.travellersInfo?.children.map((v, i) => {
                                if (i === index) {
                                  return {
                                    ...v,
                                    passportInfo: { ...v.passportInfo, passportCountry: value },
                                  };
                                }
                                return v;
                              }),
                            },
                            !!(formik.errors.travellersInfo?.children?.[index] as FormikErrors<
                              TravellerInfo
                            >)?.passportInfo?.passportCountry,
                          );
                        }}
                        getOptionSelected={(option: some, value: some) => {
                          return option.id === value.id;
                        }}
                        getOptionLabel={(v: some) => `${v.name} (${v.code})` || ''}
                        options={listCounties}
                        errorMessage={
                          (formik.errors.travellersInfo?.children?.[index] as FormikErrors<
                            TravellerInfo
                          >)?.passportInfo?.passportCountry && formik.submitCount
                            ? (formik.errors.travellersInfo?.children?.[index] as FormikErrors<
                                TravellerInfo
                              >)?.passportInfo?.passportCountry
                            : undefined
                        }
                      />

                      <FormControlAutoComplete
                        label={intl.formatMessage({ id: 'flight.nationalityCountry' })}
                        placeholder={intl.formatMessage({
                          id: 'flight.selectNationalityCountry',
                        })}
                        value={obj.passportInfo.nationalityCountry}
                        formControlStyle={styleCS}
                        onChange={(e: any, value: some | null) => {
                          formik.setFieldValue(
                            'travellersInfo',
                            {
                              ...formik.values.travellersInfo,
                              children: formik.values.travellersInfo?.children.map((v, i) => {
                                if (i === index) {
                                  return {
                                    ...v,
                                    passportInfo: {
                                      ...v.passportInfo,
                                      nationalityCountry: value,
                                    },
                                  };
                                }
                                return v;
                              }),
                            },
                            !!(formik.errors.travellersInfo?.children?.[index] as FormikErrors<
                              TravellerInfo
                            >)?.passportInfo?.nationalityCountry,
                          );
                        }}
                        getOptionSelected={(option: some, value: some) => {
                          return option.id === value.id;
                        }}
                        getOptionLabel={(v: some) => `${v.name} (${v.code})` || ''}
                        options={listCounties}
                        errorMessage={
                          (formik.errors.travellersInfo?.children?.[index] as FormikErrors<
                            TravellerInfo
                          >)?.passportInfo?.nationalityCountry && formik.submitCount
                            ? (formik.errors.travellersInfo?.children?.[index] as FormikErrors<
                                TravellerInfo
                              >)?.passportInfo?.nationalityCountry
                            : undefined
                        }
                      />
                    </Row>
                  )}
                </Row>
                <Row>
                  {formik.values.outbound.ticket?.outbound.baggages &&
                  formik.values.outbound.ticket?.outbound.baggages[0] &&
                  formik.values.outbound.extraBaggages ? (
                    <FormControlAutoComplete
                      label={intl.formatMessage({ id: 'flight.outBaggages' })}
                      value={formik.values.outbound?.extraBaggages?.[indexTmp] || null}
                      options={formik.values.outbound.ticket?.outbound.baggages}
                      getOptionSelected={(option: some, value: some) => {
                        return option.id === value.id;
                      }}
                      getOptionLabel={(v: some) =>
                        `${v.name} - ${intl.formatNumber(v.price)} ${intl.formatMessage({
                          id: 'currency',
                        })}`
                      }
                      onChange={(e: any, value?: any) => {
                        formik.setFieldValue('outbound', {
                          ...formik.values.outbound,
                          extraBaggages: formik.values.outbound?.extraBaggages?.map((v, i) => {
                            if (i === indexTmp) {
                              return value;
                            }
                            return v;
                          }),
                        });
                      }}
                      formControlStyle={styleCS}
                      optional
                      disableClearable
                      readOnly
                    />
                  ) : (
                    <Col style={{ ...styleCS, marginRight: 16 }}>
                      <Typography variant="body2">
                        <FormattedMessage id="flight.outBaggages" />
                      </Typography>
                      <div style={{ minHeight: 64, paddingTop: 14, boxSizing: 'border-box' }}>
                        <Typography variant="body2" color="textSecondary">
                          <FormattedMessage id="flight.noSupportBuyBaggage" />
                        </Typography>
                      </div>
                    </Col>
                  )}
                  {formik.values.inbound.ticket && (
                    <>
                      {formik.values.inbound.ticket?.outbound.baggages &&
                      formik.values.inbound.ticket?.outbound.baggages[0] &&
                      formik.values.inbound.extraBaggages ? (
                        <FormControlAutoComplete
                          label={intl.formatMessage({ id: 'flight.inBaggages' })}
                          value={formik.values.inbound?.extraBaggages?.[indexTmp]}
                          options={formik.values.inbound.ticket?.outbound.baggages}
                          getOptionSelected={(option: some, value: some) => {
                            return option.id === value.id;
                          }}
                          getOptionLabel={(v: some) =>
                            `${v.name} - ${intl.formatNumber(v.price)} ${intl.formatMessage({
                              id: 'currency',
                            })}`
                          }
                          onChange={(e: any, value?: any) => {
                            formik.setFieldValue('inbound', {
                              ...formik.values.inbound,
                              extraBaggages: formik.values.inbound?.extraBaggages?.map((v, i) => {
                                if (i === indexTmp) {
                                  return value;
                                }
                                return v;
                              }),
                            });
                          }}
                          formControlStyle={styleCS}
                          optional
                          disableClearable
                          readOnly
                        />
                      ) : (
                        <Col style={{ ...styleCS, marginRight: 16 }}>
                          <Typography variant="body2">
                            <FormattedMessage id="flight.inBaggages" />
                          </Typography>
                          <div style={{ minHeight: 64, paddingTop: 14, boxSizing: 'border-box' }}>
                            <Typography variant="body2" color="textSecondary">
                              <FormattedMessage id="flight.noSupportBuyBaggage" />
                            </Typography>
                          </div>
                        </Col>
                      )}
                    </>
                  )}
                </Row>
              </Col>
            );
          })}
          {formik.values.travellersInfo?.babies.map((obj: TravellerInfo, index: number) => (
            <Col key={index} style={{ marginBottom: 12, marginTop: 16 }}>
              <Row style={{ marginBottom: 16 }}>
                <Typography variant="subtitle1">
                  {index +
                    1 +
                    (formik.values?.travellersInfo?.adults?.length || 0) +
                    (formik.values?.travellersInfo?.children?.length || 0)}
                  .
                  <FormattedMessage id="flight.infant" values={{ num: index + 1 }} />
                </Typography>
                <ListContactDialog
                  exceptionList={[
                    ...formik.values.travellersInfo?.adults,
                    ...formik.values.travellersInfo?.children,
                    ...formik.values.travellersInfo?.babies,
                  ]}
                  onSelect={one => {
                    formik.setFieldValue(
                      'travellersInfo',
                      {
                        ...formik.values.travellersInfo,
                        babies: formik.values.travellersInfo?.babies.map((v, i) => {
                          if (i === index) {
                            return {
                              ...v,
                              fullName: one.name,
                              birthday: one.birthday,
                              gender: one.gender ? 'm' : 'f',
                              userId: one.id,
                            };
                          }
                          return v;
                        }),
                      },
                      true,
                    );
                  }}
                />
              </Row>
              <Row style={{ flexWrap: 'wrap' }}>
                <FormControlTextField
                  label={intl.formatMessage({ id: 'flight.fullName' })}
                  placeholder={intl.formatMessage({ id: 'flight.fullNameEx' })}
                  formControlStyle={styleCS}
                  value={obj.fullName}
                  readOnly
                  errorMessage={
                    (formik.errors.travellersInfo?.babies?.[index] as FormikErrors<TravellerInfo>)
                      ?.fullName && formik.submitCount
                      ? (formik.errors.travellersInfo?.babies?.[index] as FormikErrors<
                          TravellerInfo
                        >)?.fullName
                      : undefined
                  }
                />
                <FormControlTextField
                  formControlStyle={styleCS}
                  label={
                    <Typography variant="subtitle2" component="span">
                      <FormattedMessage id="birthday" />
                      &nbsp;
                      <Typography variant="caption" component="span">
                        <FormattedMessage id="flight.infantBirthday" />
                      </Typography>
                    </Typography>
                  }
                  fullWidth
                  value={
                    obj.birthday
                      ? moment(obj.birthday, DATE_FORMAT_BACK_END).format(DATE_FORMAT)
                      : undefined
                  }
                  placeholder={DATE_FORMAT.toLowerCase()}
                  readOnly
                  errorMessage={
                    (formik.errors.travellersInfo?.babies?.[index] as FormikErrors<TravellerInfo>)
                      ?.birthday && formik.submitCount
                      ? (formik.errors.travellersInfo?.babies?.[index] as FormikErrors<
                          TravellerInfo
                        >)?.birthday
                      : undefined
                  }
                  endAdornment={
                    <InputAdornment position="end" style={{ marginRight: 8 }}>
                      <IconButton size="small" edge="start" tabIndex={-1}>
                        <IconCalender />
                      </IconButton>
                    </InputAdornment>
                  }
                />

                <GenderField
                  gender={obj.gender}
                  onChange={e =>
                    formik.setFieldValue(
                      'travellersInfo',
                      {
                        ...formik.values.travellersInfo,
                        babies: formik.values.travellersInfo?.babies.map((v, i) => {
                          if (i === index) {
                            return { ...v, gender: e };
                          }
                          return v;
                        }),
                      },
                      false,
                    )
                  }
                />
              </Row>
              {needPassport && (
                <Row style={{ flexWrap: 'wrap' }}>
                  <FormControlTextField
                    label={intl.formatMessage({ id: 'flight.passport' })}
                    placeholder={intl.formatMessage({ id: 'flight.passportEx' })}
                    formControlStyle={styleCS}
                    value={obj.passportInfo.passport}
                    onChange={e =>
                      formik.setFieldValue('travellersInfo', {
                        ...formik.values.travellersInfo,
                        babies: formik.values.travellersInfo?.babies.map((v, i) => {
                          if (i === index) {
                            return {
                              ...v,
                              passportInfo: { ...v.passportInfo, passport: e.target.value },
                            };
                          }
                          return v;
                        }),
                      })
                    }
                    errorMessage={
                      (formik.errors.travellersInfo?.babies?.[index] as FormikErrors<TravellerInfo>)
                        ?.passportInfo?.passport && formik.submitCount
                        ? (formik.errors.travellersInfo?.babies?.[index] as FormikErrors<
                            TravellerInfo
                          >)?.passportInfo?.passport
                        : undefined
                    }
                  />
                  <BirthDayField
                    disablePast
                    date={
                      obj.passportInfo.passportExpiredDate
                        ? moment(obj.passportInfo.passportExpiredDate, DATE_FORMAT_BACK_END)
                        : undefined
                    }
                    update={value => {
                      formik.setFieldValue('travellersInfo', {
                        ...formik.values.travellersInfo,
                        babies: formik.values.travellersInfo?.babies.map((v, i) => {
                          if (i === index) {
                            return {
                              ...v,
                              passportInfo: {
                                ...v.passportInfo,
                                passportExpiredDate: value?.format(DATE_FORMAT_BACK_END),
                              },
                            };
                          }
                          return v;
                        }),
                      });
                    }}
                    label={intl.formatMessage({ id: 'flight.passportExpired' })}
                    inputStyle={styleCS}
                    errorMessage={
                      (formik.errors.travellersInfo?.babies?.[index] as FormikErrors<TravellerInfo>)
                        ?.passportInfo?.passportExpiredDate && formik.submitCount
                        ? (formik.errors.travellersInfo?.babies?.[index] as FormikErrors<
                            TravellerInfo
                          >)?.passportInfo?.passportExpiredDate
                        : undefined
                    }
                  />
                  <FormControlAutoComplete
                    label={intl.formatMessage({ id: 'flight.passportCountry' })}
                    placeholder={intl.formatMessage({ id: 'flight.selectPassportCountry' })}
                    value={obj.passportInfo.passportCountry}
                    formControlStyle={styleCS}
                    onChange={(e: any, value: some | null) => {
                      formik.setFieldValue('travellersInfo', {
                        ...formik.values.travellersInfo,
                        babies: formik.values.travellersInfo?.babies.map((v, i) => {
                          if (i === index) {
                            return {
                              ...v,
                              passportInfo: { ...v.passportInfo, passportCountry: value },
                            };
                          }
                          return v;
                        }),
                      });
                    }}
                    getOptionSelected={(option: some, value: some) => {
                      return option.id === value.id;
                    }}
                    getOptionLabel={(v: some) => `${v.name} (${v.code})` || ''}
                    options={listCounties}
                    errorMessage={
                      (formik.errors.travellersInfo?.babies?.[index] as FormikErrors<TravellerInfo>)
                        ?.passportInfo?.passportCountry && formik.submitCount
                        ? (formik.errors.travellersInfo?.babies?.[index] as FormikErrors<
                            TravellerInfo
                          >)?.passportInfo?.passportCountry
                        : undefined
                    }
                  />

                  <FormControlAutoComplete
                    label={intl.formatMessage({ id: 'flight.nationalityCountry' })}
                    placeholder={intl.formatMessage({
                      id: 'flight.selectNationalityCountry',
                    })}
                    value={obj.passportInfo.nationalityCountry}
                    formControlStyle={styleCS}
                    onChange={(e: any, value: some | null) => {
                      formik.setFieldValue('travellersInfo', {
                        ...formik.values.travellersInfo,
                        babies: formik.values.travellersInfo?.babies.map((v, i) => {
                          if (i === index) {
                            return {
                              ...v,
                              passportInfo: { ...v.passportInfo, nationalityCountry: value },
                            };
                          }
                          return v;
                        }),
                      });
                    }}
                    getOptionSelected={(option: some, value: some) => {
                      return option.id === value.id;
                    }}
                    getOptionLabel={(v: some) => `${v.name} (${v.code})` || ''}
                    options={listCounties}
                    errorMessage={
                      (formik.errors.travellersInfo?.babies?.[index] as FormikErrors<TravellerInfo>)
                        ?.passportInfo?.nationalityCountry && formik.submitCount
                        ? (formik.errors.travellersInfo?.babies?.[index] as FormikErrors<
                            TravellerInfo
                          >)?.passportInfo?.nationalityCountry
                        : undefined
                    }
                  />
                </Row>
              )}
            </Col>
          ))}
        </Paper>
        <Paper variant="outlined" style={{ padding: '12px 16px', marginTop: 16 }}>
          {/* <Typography variant="h6" style={{ margin: '24px 0px 16px' }}>
            <FormattedMessage id="flight.travelInsurance" />
          </Typography> */}
          <Row style={{ alignItems: 'flex-start' }}>
            <FormControlLabel
              label={
                <Typography variant="subtitle2">
                  <FormattedMessage id="flight.buyInsurance" />
                </Typography>
              }
              disabled={!formik.values.insurancePackage}
              style={{ paddingBottom: '8px' }}
              labelPlacement="end"
              value={formik.values.buyInsurance}
              onChange={(e, value) =>
                formik.setFieldValue('buyInsurance', !formik.values.buyInsurance)
              }
              control={
                <Checkbox
                  id="flightBookingInfo.buyInsurance"
                  color="secondary"
                  checked={formik.values.buyInsurance}
                />
              }
            />
            <Col style={{ textAlign: 'end', alignItems: 'flex-end', flex: 1 }}>
              {formik.values.insurancePackage && (
                <>
                  <img
                    alt=""
                    src={formik.values.insurancePackage.image}
                    style={{ height: 36, objectFit: 'cover', minWidth: 100 }}
                  />

                  <Typography variant="caption">
                    <FormattedMessage id="flight.providedByInsurance" />
                  </Typography>
                </>
              )}
            </Col>
          </Row>
          <Row>
            {formik.values.insurancePackage && (
              <Typography
                variant="body1"
                className="final-price"
                style={{ flex: 1, textAlign: 'end' }}
              >
                <FormattedMessage
                  id="flight.chubbInsurance.price"
                  values={{
                    price: <FormattedNumber value={formik.values.insurancePackage.price} />,
                  }}
                />
              </Typography>
            )}
          </Row>
          {formik.values.insurancePackage ? (
            <Typography variant="caption">
              <span
                dangerouslySetInnerHTML={{ __html: formik.values.insurancePackage.introduction }}
              />
            </Typography>
          ) : (
            <Typography variant="body2" color="textSecondary">
              <FormattedMessage id="flight.noSupportBuyInsurance" />
            </Typography>
          )}
        </Paper>
        <Row style={{ justifyContent: 'center', marginTop: 24 }}>
          <Button
            id="flightBookingInfo.back"
            variant="outlined"
            style={{ marginRight: 24, minWidth: 160 }}
            size="large"
            disableElevation
            onClick={() => dispatch(goBackAction())}
          >
            <FormattedMessage id="back" />
          </Button>
          <Button
            id="flightBookingInfo.continue"
            variant="contained"
            color="secondary"
            style={{ minWidth: 160 }}
            size="large"
            type="submit"
            disableElevation
          >
            <FormattedMessage id="continue" />
          </Button>
        </Row>
      </form>
      <AsideBound
        isTablet={isTablet}
        style={{ width: 360, marginLeft: isTablet ? 0 : 24 }}
        direction="right"
      >
        <FlightInfoBox
          hideTravellersBox
          seeDetail={() => setSeeDetail(true)}
          booking={formik.values}
        />
      </AsideBound>
      <FlightTicketDialog
        close={() => setSeeDetail(false)}
        open={seeDetail}
        booking={formik.values}
      />
    </Container>
  );
};

export default FlightBookingInfoForm;
