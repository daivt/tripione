import React from 'react';
import { some } from '../../../../../constants';
import { FlightInfo } from '../../booking/utils';
import FlightBookingInfoForm from './FlightBookingInfoForm';

interface Props {
  info: FlightInfo;
  listCounties: some[];
  setInfo(info: FlightInfo): void;
}
const FlightBookingInfoDesktop: React.FC<Props> = props => {
  const { info, listCounties, setInfo } = props;
  return <FlightBookingInfoForm info={info} listCounties={listCounties} setInfo={setInfo} />;
};

export default FlightBookingInfoDesktop;
