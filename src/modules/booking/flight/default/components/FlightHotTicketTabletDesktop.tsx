import { Container, Typography } from '@material-ui/core';
import { Moment } from 'moment';
import React, { PureComponent } from 'react';
import { FormattedMessage } from 'react-intl';
import { some } from '../../../../../constants';
import FlightSearch from '../../common/FlightSearch';
import { FlightSearchParams } from '../../utils';
import FlightFindRecentBox from './FlightFindRecentBox';
import FlightHotTicketBox from './FlightHotTicketBox';
import FlightHotTicketBoxHeader from './FlightHotTicketBoxHeader';
import SelectLocationDialog from './SelectLocationDialog';

interface Props {
  hasLocation?: boolean;
  locations: some[];
  currentLocation: some | null;
  topDestinations: some[];
  currentDestination: some | null;
  hotTickets: some[];
  month: Moment;
  loading: boolean;
  onSelectCurrentLocation(location: some): void;
  onSelectDestination(data: some): void;
  onRefreshData(): void;
  onSelectMonth(date: Moment): void;
  paramsSearch: FlightSearchParams;
  onSearch(params: FlightSearchParams): void;
}

export default class FlightHotTicketTabletDesktop extends PureComponent<Props> {
  render() {
    const {
      hasLocation,
      hotTickets,
      locations,
      currentLocation,
      topDestinations,
      currentDestination,
      onSelectCurrentLocation,
      onSelectDestination,
      onRefreshData,
      month,
      onSelectMonth,
      loading,
      paramsSearch,
      onSearch,
    } = this.props;
    return (
      <>
        <FlightSearch params={paramsSearch} onSearch={onSearch} />
        <Container style={{ padding: '16px 24px', alignItems: 'center' }}>
          <FlightFindRecentBox />
          {hasLocation ? (
            <>
              <Typography variant="h5" style={{ padding: '10px 0' }}>
                <FormattedMessage id="flight.bestTicketPrice" />
              </Typography>
              <FlightHotTicketBoxHeader
                locations={locations}
                currentLocation={currentLocation}
                topDestinations={topDestinations}
                currentDestination={currentDestination}
                month={month}
                onSelectCurrentLocation={location => onSelectCurrentLocation(location)}
                onSelectDestination={data => onSelectDestination(data)}
                onRefreshData={onRefreshData}
                onSelectMonth={date => onSelectMonth(date)}
              />

              <FlightHotTicketBox
                hotTickets={hotTickets}
                currentLocation={currentLocation}
                currentDestination={currentDestination}
                loading={loading}
              />
            </>
          ) : (
            <SelectLocationDialog
              show={!hasLocation}
              onClose={onSelectCurrentLocation}
              locations={locations}
            />
          )}
        </Container>
      </>
    );
  }
}
