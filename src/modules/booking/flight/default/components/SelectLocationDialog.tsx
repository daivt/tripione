import { Button } from '@material-ui/core';
import Dialog from '@material-ui/core/Dialog';
import Typography from '@material-ui/core/Typography';
import React from 'react';
import { FormattedMessage, useIntl } from 'react-intl';
import { GREY_700, PRIMARY } from '../../../../../configs/colors';
import { some } from '../../../../../constants';
import FormControlAutoComplete from '../../../../common/components/FormControlAutoComplete';
import { TopDestinations } from '../../constants';

interface Props {
  show: boolean;
  onClose(location?: some): void;
  locations: some[];
}

const SelectLocationDialog: React.FC<Props> = props => {
  const { show, onClose, locations } = props;
  const [locationSelected, setLocationSelected] = React.useState<some | null>(null);
  const intl = useIntl();

  return (
    <Dialog
      open={show}
      PaperProps={{
        style: {
          textAlign: 'center',
          overflowY: 'visible',
        },
      }}
      maxWidth="sm"
    >
      <div style={{ padding: '20px' }}>
        <div style={{ padding: '8px' }}>
          <Typography variant="h5">
            <FormattedMessage id="flight.whereAreYou" />
          </Typography>
          <Typography variant="body2" style={{ paddingTop: '8px' }}>
            <FormattedMessage id="flight.pleaseProvideYourLocation" />
          </Typography>
        </div>
        <FormControlAutoComplete<some, undefined, undefined, undefined>
          placeholder={intl.formatMessage({ id: 'flight.selectYourLocation' })}
          value={locationSelected}
          formControlStyle={{ width: '100%', margin: 0 }}
          onChange={(e: any, value: some | null) => {
            setLocationSelected(value);
          }}
          getOptionSelected={(option: some, value: some) => {
            return option.id === value.id;
          }}
          fullWidth
          getOptionLabel={(v: some) => v.name}
          options={locations}
          optional
        />

        <div style={{ display: 'flex', justifyContent: 'space-between', padding: '8px' }}>
          {TopDestinations.map(item => {
            const isSelected = item.id === locationSelected?.id;
            return (
              <Button
                key={item.id}
                variant="outlined"
                style={{
                  margin: '0 3px',
                  padding: '2px 16px',
                  borderRadius: '32px',
                  backgroundColor: isSelected ? PRIMARY : 'transparent',
                  color: isSelected ? '#fff' : GREY_700,
                  borderColor: isSelected ? PRIMARY : undefined,
                }}
                size="medium"
                onClick={() => setLocationSelected(item)}
              >
                <Typography variant="body2">{item.name}</Typography>
              </Button>
            );
          })}
        </div>

        <Button
          style={{ marginTop: '32px', marginBottom: '12px', minWidth: '160px' }}
          size="large"
          variant="contained"
          color="secondary"
          disableElevation
          onClick={() => locationSelected && onClose(locationSelected)}
        >
          <Typography variant="button">
            <FormattedMessage id="accept" />
          </Typography>
        </Button>
      </div>
    </Dialog>
  );
};

export default SelectLocationDialog;
