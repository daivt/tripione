import { Button, ButtonBase, Tooltip } from '@material-ui/core';
import Typography from '@material-ui/core/Typography';
import moment from 'moment';
import React, { PureComponent } from 'react';
import { FormattedMessage, useIntl } from 'react-intl';
import Slider from 'react-slick';
import { BLUE, GREY_500, GREY_700, WHITE } from '../../../../../configs/colors';
import { ROUTES } from '../../../../../configs/routes';
import { DATE_FORMAT_BACK_END } from '../../../../../models/moment';
import { ReactComponent as IconDelete } from '../../../../../svg/booking/ic_delete.svg';
import { ReactComponent as IconFlightDown } from '../../../../../svg/booking/ic_flight_down.svg';
import { ReactComponent as IconFlightUp } from '../../../../../svg/booking/ic_flight_up.svg';
import { Col } from '../../../../common/components/elements';
import Link from '../../../../common/components/Link';
import { slideSettings } from '../../../common/Slider/setting';
import { getInputStr } from '../../common/TravellerCountInfoBox';
import { parseFlightSearchParams } from '../../utils';
import { clearFlightSearchHistory, getFlightSearchHistory } from '../utils';

interface PropCustom {
  search: string;
  index: number;
}

const CustomSlide: React.FC<PropCustom> = props => {
  const { index, search } = props;
  const intl = useIntl();
  const params = parseFlightSearchParams(new URLSearchParams(search));

  return (
    <Link to={{ pathname: ROUTES.booking.flight.result, search }}>
      <ButtonBase
        key={index}
        style={{
          padding: 10,
          border: `1px solid ${GREY_500}`,
          display: 'flex',
          flexDirection: 'column',
          minWidth: 250,
          marginRight: 8,
          alignItems: 'flex-start',
          borderRadius: 4,
          background: WHITE,
        }}
      >
        <Col
          style={{
            height: '60px',
          }}
        >
          <div style={{ display: 'flex', alignItems: 'center' }}>
            <IconFlightUp className="svgFillAll" style={{ paddingRight: '4px', fill: BLUE }} />
            <Typography variant="body2" style={{ fontWeight: 500 }}>
              {params.origin && (
                <>
                  {params.origin.location} ({params.origin.code})
                </>
              )}
            </Typography>
          </div>
          <div style={{ display: 'flex', width: '28px', justifyContent: 'center', flex: 1 }}>
            <div style={{ borderLeft: `1px solid ${BLUE}`, height: '100%' }} />
          </div>
          <div style={{ display: 'flex', alignItems: 'center' }}>
            <IconFlightDown className="svgFillAll" style={{ paddingRight: '4px', fill: BLUE }} />
            <Typography variant="body2" style={{ fontWeight: 500 }}>
              {params.destination && (
                <>
                  {params.destination.location} ({params.destination.code})
                </>
              )}
            </Typography>
          </div>
        </Col>
        <div style={{ display: 'flex', alignItems: 'center', paddingTop: '5px' }}>
          <Typography noWrap variant="body2" color="textSecondary" style={{ flexShrink: 0 }}>
            {moment(params.departureDate, DATE_FORMAT_BACK_END).format('L')}
            {params.returnDate && (
              <>&nbsp;- {moment(params.returnDate, DATE_FORMAT_BACK_END).format('L')}</>
            )}
          </Typography>

          <div
            style={{
              width: '4px',
              height: '4px',
              borderRadius: '4px',
              marginLeft: '12px',
              marginRight: '6px',
              background: GREY_700,
            }}
          />
          <Tooltip title={getInputStr(params.seatClass, params.travellerCountInfo, intl)}>
            <Typography noWrap variant="body2" color="textSecondary" style={{ maxWidth: 120 }}>
              {getInputStr(params.seatClass, params.travellerCountInfo, intl)}
            </Typography>
          </Tooltip>
        </div>
      </ButtonBase>
    </Link>
  );
};

interface Props {}

interface State {
  searchHistory: string[];
}

class FlightFindRecentBox extends PureComponent<Props, State> {
  state: State = {
    searchHistory: [],
  };

  componentDidMount() {
    const searchHistory = getFlightSearchHistory();

    this.setState({ searchHistory });
  }

  render() {
    const { searchHistory } = this.state;

    if (!searchHistory.length) {
      return <div />;
    }

    return (
      <div style={{ marginBottom: '40px' }}>
        <div style={{ display: 'flex', justifyContent: 'space-between' }}>
          <Typography variant="h5" style={{ padding: '10px 0' }}>
            <FormattedMessage id="searchRecent" />
          </Typography>

          <div
            style={{
              display: 'flex',
              flex: 1,
              justifyContent: 'flex-end',
              alignItems: 'flex-end',
              margin: '12px 20px',
            }}
          >
            <Button
              style={{ borderRadius: '4px' }}
              onClick={() => {
                clearFlightSearchHistory();
                this.setState({ searchHistory: [] });
              }}
            >
              <div style={{ display: 'flex', alignItems: 'center', padding: '3px 6px' }}>
                <IconDelete />
                <Typography variant="body2" style={{ color: BLUE, paddingLeft: '6px' }}>
                  <FormattedMessage id="deleteSearchHistory" />
                </Typography>
              </div>
            </Button>
          </div>
        </div>

        <div style={{ margin: '0 36px' }}>
          <Slider {...slideSettings()}>
            {searchHistory.map((item: string, index: number) => (
              <CustomSlide key={index} index={index} search={item} />
            ))}
          </Slider>
        </div>
      </div>
    );
  }
}

export default FlightFindRecentBox;
