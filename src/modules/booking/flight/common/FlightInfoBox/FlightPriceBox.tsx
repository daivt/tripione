import { Typography } from '@material-ui/core';
import * as React from 'react';
import { FormattedMessage, FormattedNumber } from 'react-intl';
import styled from 'styled-components';
import { GREY } from '../../../../../configs/colors';
import { ReactComponent as IconCoin } from '../../../../../svg/booking/ic_coin.svg';
import { PointPayment } from '../../../utils';
import { FlightInfo } from '../../booking/utils';
import { computePoints } from '../../result/utils';
import { computeFlightPayableNumbers } from '../../utils';

const Line = styled.div`
  height: 36px;
  display: flex;
  align-items: center;
  justify-content: space-between;
`;

const CountSpan = styled.span`
  color: ${GREY};
`;
interface Props {
  booking: FlightInfo;
  pointPaymentData?: PointPayment;
}

const FlightPriceBox: React.FunctionComponent<Props> = props => {
  const { booking, pointPaymentData } = props;
  const payableNumbers = computeFlightPayableNumbers(booking, pointPaymentData);

  const { adult, children, infant, extraBaggagesCosts, finalPrice } = payableNumbers;

  const getArrayDetail = React.useMemo(() => {
    const array = [];
    if (booking.insurancePackage && booking.buyInsurance) {
      array.push(
        <Line key={1}>
          <FormattedMessage id="flight.travelInsurance" />
          <div className="price">
            <CountSpan>
              {adult.number + children.number + infant.number}
              &nbsp;x&nbsp;
            </CountSpan>
            <FormattedNumber value={booking.insurancePackage.price} />
            &nbsp;
            <FormattedMessage id="currency" />
          </div>
        </Line>,
      );
    }
    if (extraBaggagesCosts > 0) {
      array.push(
        <Line key={2}>
          <FormattedMessage id="flight.buyMoreCheckIn" />
          <div className="price">
            <FormattedNumber value={extraBaggagesCosts} />
            &nbsp;
            <FormattedMessage id="currency" />
          </div>
        </Line>,
      );
    }
    return array;
  }, [
    adult.number,
    infant.number,
    booking.buyInsurance,
    booking.insurancePackage,
    children.number,
    extraBaggagesCosts,
  ]);
  return (
    <div style={{ flex: 1, marginTop: 16, marginBottom: 8 }}>
      <Typography variant="subtitle1">
        <FormattedMessage id="flight.priceDetails" />
      </Typography>
      <div>
        <div>
          {adult.number > 0 && (
            <Line>
              <FormattedMessage id="flight.adultPrice" />
              <div className="price">
                <CountSpan>
                  {adult.number}
                  &nbsp;x&nbsp;
                </CountSpan>
                <FormattedNumber value={adult.unitPrice} />
                &nbsp;
                <FormattedMessage id="currency" />
              </div>
            </Line>
          )}
          {children.number > 0 && (
            <Line>
              <FormattedMessage id="flight.childPrice" />
              <div className="price">
                <CountSpan>
                  {children.number}
                  &nbsp;x&nbsp;
                </CountSpan>
                <FormattedNumber value={children.unitPrice} />
                &nbsp;
                <FormattedMessage id="currency" />
              </div>
            </Line>
          )}
          {infant.number > 0 && (
            <Line>
              <FormattedMessage id="flight.infantPrice" />
              <div className="price">
                <CountSpan>
                  {infant.number}
                  &nbsp;x&nbsp;
                </CountSpan>
                <FormattedNumber value={infant.unitPrice} />
                &nbsp;
                <FormattedMessage id="currency" />
              </div>
            </Line>
          )}
        </div>
        {getArrayDetail}
        <div>
          <Line
            style={{
              display: 'flex',
              justifyContent: 'space-between',
              alignItems: 'center',
            }}
          >
            <Typography variant="subtitle2" className="final-price">
              <FormattedMessage id="flight.totalPayable" />
            </Typography>
            <Typography variant="subtitle1" className="final-price">
              <FormattedNumber value={finalPrice > 0 ? finalPrice : 0} />
              &nbsp;
              <FormattedMessage id="currency" />
            </Typography>
          </Line>
          <Line
            style={{
              justifyContent: 'flex-end',
              color: `${GREY}`,
            }}
          >
            <Typography variant="caption" color="textSecondary">
              <FormattedMessage id="flight.includeTaxesAndFees" />
            </Typography>
          </Line>
          {computePoints(booking) > 0 && (
            <Line
              style={{
                justifyContent: 'flex-end',
                color: `${GREY}`,
              }}
            >
              <IconCoin style={{ marginRight: '10px' }} />
              <Typography variant="body2" className="point">
                <FormattedNumber value={computePoints(booking)} />
                &nbsp;
                <FormattedMessage id="point" />
              </Typography>
            </Line>
          )}
        </div>
      </div>
    </div>
  );
};

export default FlightPriceBox;
