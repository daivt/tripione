import Typography from '@material-ui/core/Typography';
import moment from 'moment';
import * as React from 'react';
import { FormattedMessage } from 'react-intl';
import { shallowEqual, useSelector } from 'react-redux';
import { GREY } from '../../../../../configs/colors';
import { some } from '../../../../../constants';
import { DATE_FORMAT } from '../../../../../models/moment';
import { AppState } from '../../../../../redux/reducers';
import { ReactComponent as IconAirplane } from '../../../../../svg/booking/ic_airplane_horizontal.svg';
import { Col, Row } from '../../../../common/components/elements';
import { millisecondsToHour } from '../../../utils';
import { FlightInfo } from '../../booking/utils';

interface Props {
  booking: FlightInfo;
}

function renderFlightItineraryItem(ticketDetail: some, airlineInfo: some) {
  return (
    <Row
      key={ticketDetail.flightNumber}
      className="card-background"
      style={{
        padding: 8,
        marginBottom: 8,
        borderRadius: '4px',
        border: `1px solid ${GREY}`,
      }}
    >
      <Col style={{ flex: 1 }}>
        <Row>
          <Typography variant="body2" style={{ minWidth: 50, marginRight: 16 }}>
            {ticketDetail.departureTimeStr}
          </Typography>
          <Typography variant="body2">
            {ticketDetail.departureCity} ({ticketDetail.departureAirport})
          </Typography>
        </Row>
        <Row style={{ marginTop: 8 }}>
          <Typography variant="body2" style={{ minWidth: 50, marginRight: 16 }}>
            {ticketDetail.arrivalTimeStr}
          </Typography>
          <Typography variant="body2">
            {ticketDetail.arrivalCity} ({ticketDetail.arrivalAirport})
          </Typography>
        </Row>
      </Col>
      <Row>{airlineInfo && <img style={{ maxWidth: '48px' }} src={airlineInfo.logo} alt="" />}</Row>
    </Row>
  );
}

const RenderFlightItineraryBox: React.FunctionComponent<some> = props => {
  const { getFlightGeneralInfo, ticket } = props;
  const { outbound } = ticket;
  const transitTickets = outbound.transitTickets ? outbound.transitTickets : undefined;

  return (
    <div style={{ paddingTop: 8 }}>
      {transitTickets
        ? transitTickets.map((item: some) =>
            renderFlightItineraryItem(item, getFlightGeneralInfo(item.aid)),
          )
        : renderFlightItineraryItem(outbound, getFlightGeneralInfo(outbound.aid))}
    </div>
  );
};

const FlightItineraryBox: React.FunctionComponent<Props> = props => {
  const { booking } = props;
  const outboundTicket = booking.outbound.ticket;
  const inboundTicket = booking.inbound.ticket ? booking.inbound.ticket : undefined;

  const generalFlight = useSelector((state: AppState) => state.common.generalFlight, shallowEqual);

  const getFlightGeneralInfo = React.useCallback(
    (id: number) => {
      return generalFlight?.airlines?.find((v: some) => v.aid === id) || {};
    },
    [generalFlight],
  );

  const holdingTime = React.useMemo(() => {
    return inboundTicket
      ? Math.min(
          outboundTicket?.outbound.ticketdetail.holdingTime,
          inboundTicket.outbound.ticketdetail.holdingTime,
        )
      : outboundTicket?.outbound.ticketdetail.holdingTime;
  }, [inboundTicket, outboundTicket]);
  const holdingTimeHours = React.useMemo(() => {
    return millisecondsToHour(holdingTime);
  }, [holdingTime]);

  if (!outboundTicket) {
    return null;
  }

  return (
    <div>
      {!!holdingTimeHours && (
        <Typography variant="body2" style={{ marginTop: 12 }}>
          <FormattedMessage id="flight.holdingTime" values={{ num: holdingTimeHours }} />
        </Typography>
      )}
      <div style={{ display: 'flex', flexDirection: 'column', paddingTop: '12px' }}>
        <div style={{ display: 'flex', alignItems: 'center' }}>
          <IconAirplane style={{ width: 16, height: 16 }} />
          <Typography variant="subtitle2" style={{ marginLeft: '10px' }}>
            <FormattedMessage id="outbound" /> -{' '}
            {moment(outboundTicket.outbound.departureDayStr, DATE_FORMAT).format('L')}
          </Typography>
        </div>
        <RenderFlightItineraryBox
          ticket={outboundTicket}
          getFlightGeneralInfo={getFlightGeneralInfo}
        />
      </div>
      {inboundTicket && (
        <div style={{ display: 'flex', flexDirection: 'column', paddingTop: '12px' }}>
          <div style={{ display: 'flex', alignItems: 'center' }}>
            <IconAirplane style={{ width: 16, height: 16 }} />
            <Typography variant="subtitle2" style={{ marginLeft: '10px' }}>
              <FormattedMessage id="inbound" /> -{' '}
              {moment(inboundTicket.outbound.departureDayStr, DATE_FORMAT).format('L')}
            </Typography>
          </div>
          <RenderFlightItineraryBox
            ticket={inboundTicket}
            getFlightGeneralInfo={getFlightGeneralInfo}
          />
        </div>
      )}
    </div>
  );
};

export default FlightItineraryBox;
