import { Paper, Typography } from '@material-ui/core';
import * as React from 'react';
import { FormattedMessage } from 'react-intl';
import { BLUE } from '../../../../../configs/colors';
import { PointPayment } from '../../../utils';
import FlightTravellersBox from '../../booking/components/FlightTravellersBox';
import { FlightInfo } from '../../booking/utils';
import FlightItineraryBox from './FlightItineraryBox';
import FlightPriceBox from './FlightPriceBox';

interface Props {
  seeDetail(): void;
  styles?: React.CSSProperties;
  booking: FlightInfo;
  pointPaymentData?: PointPayment;
  hideTravellersBox?: boolean;
}

const FlightInfoBox: React.FunctionComponent<Props> = props => {
  const { booking, pointPaymentData, styles, seeDetail, hideTravellersBox } = props;
  return (
    <Paper
      variant="outlined"
      style={{
        ...styles,
        padding: '14px 16px',
        marginBottom: '20px',
        alignSelf: 'flex-start',
      }}
    >
      <div style={{ display: 'flex', alignItems: 'baseline' }}>
        <Typography variant="h5" style={{ flex: 1 }}>
          <FormattedMessage id="flight.result.flightInfo" />
        </Typography>
        <Typography
          variant="caption"
          style={{ color: BLUE, cursor: 'pointer' }}
          onClick={seeDetail}
        >
          <FormattedMessage id="flight.seeDetails" />
        </Typography>
      </div>
      <FlightItineraryBox booking={booking} />
      {!hideTravellersBox && booking.travellersInfo && (
        <FlightTravellersBox travellers={booking.travellersInfo} />
      )}
      <FlightPriceBox booking={booking} pointPaymentData={pointPaymentData} />
    </Paper>
  );
};

export default FlightInfoBox;
