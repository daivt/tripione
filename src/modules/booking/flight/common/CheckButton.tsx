/* eslint-disable no-nested-ternary */
import { ButtonProps, fade, PropTypes } from '@material-ui/core';
import React from 'react';
import { GREY_700, PRIMARY, SECONDARY, WHITE } from '../../../../configs/colors';
import LoadingButton from '../../../common/components/LoadingButton';

const CheckButton = (
  props: ButtonProps & {
    active: boolean;
    loading?: boolean;
    loadingColor?: PropTypes.Color;
    backgroundColor?: PropTypes.Color;
  },
) => {
  const { active, style, ref, loading, loadingColor, backgroundColor, children, ...rest } = props;

  return (
    <LoadingButton
      loadingColor={loadingColor}
      loading={loading}
      variant="outlined"
      color={active ? 'secondary' : 'default'}
      size="small"
      style={{
        backgroundColor: active
          ? loading
            ? fade(backgroundColor === 'secondary' ? SECONDARY : PRIMARY, 0.4)
            : backgroundColor === 'secondary'
            ? SECONDARY
            : PRIMARY
          : undefined,
        borderRadius: '15px',
        boxShadow: 'none',
        textTransform: 'none',
        color: active ? WHITE : GREY_700,
        borderColor: active ? (backgroundColor === 'secondary' ? SECONDARY : PRIMARY) : undefined,
        minHeight: '30px',
        display: 'flex',
        alignItems: 'center',
        padding: '0px 16px',
        ...style,
      }}
      {...rest}
    >
      {children}
    </LoadingButton>
  );
};
export default CheckButton;
