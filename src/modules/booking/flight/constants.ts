import { Airport } from './utils';

export const defaultAirports: Airport[] = [
  { code: 'HAN', location: 'Hà Nội', name: 'Nội Bài' },
  { code: 'SGN', location: 'Hồ Chí Minh', name: 'Tân Sơn Nhất' },
  { code: 'DAD', location: 'Đà Nẵng', name: 'Quốc tế Đà Nẵng' },
  { code: 'CXR', location: 'Nha Trang', name: 'Cam Ranh' },
  { code: 'PQC', location: 'Phú Quốc', name: 'Quốc tế Phú Quốc' },
  { code: 'HUI', location: 'Huế', name: 'Quốc tế Phú Bài' },
  { code: 'HPH', location: 'Hải Phòng', name: 'Quốc tế Cát bi' },
];

export const TopDestinations = [
  {
    airportCode: 'HAN',
    id: 11,
    latitude: 21.028333,
    longitude: 105.853333,
    name: 'Hà Nội',
    nameNoAccent: 'ha noi',
  },
  {
    airportCode: 'SGN',
    id: 33,
    latitude: 10.769444,
    longitude: 106.681944,
    name: 'Hồ Chí Minh',
    nameNoAccent: 'ho chi minh',
  },
  {
    airportCode: 'DAD',
    id: 50,
    latitude: 16.031944,
    longitude: 108.220556,
    name: 'Đà Nẵng',
    nameNoAccent: 'da nang',
  },
  {
    airportCode: 'HPH',
    id: 3,
    latitude: 20.866389,
    longitude: 106.6825,
    name: 'Hải Phòng',
    nameNoAccent: 'hai phong',
  },
  {
    airportCode: 'VCA',
    id: 38,
    latitude: 10.032415,
    longitude: 105.784092,
    name: 'Cần Thơ',
    nameNoAccent: 'can tho',
  },
];
