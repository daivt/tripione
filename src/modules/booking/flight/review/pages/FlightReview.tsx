import React from 'react';
import { shallowEqual, useSelector, useDispatch } from 'react-redux';
import { ThunkDispatch } from 'redux-thunk';
import { Action } from 'redux';
import { useLocation } from 'react-router';
import { useIntl } from 'react-intl';
import { useSnackbar } from 'notistack';
import { AppState } from '../../../../../redux/reducers';
import RedirectDiv from '../../../../common/components/RedirectDiv';
import FlightReviewDesktop from '../components/FlightReviewDesktop';
import { fetchThunk } from '../../../../common/redux/thunk';
import { API_PATHS } from '../../../../../configs/API';
import { some, SUCCESS_CODE } from '../../../../../constants';
import { snackbarSetting } from '../../../../common/components/elements';
import { goToAction } from '../../../../common/redux/reducer';
import { ROUTES } from '../../../../../configs/routes';
import { genFlightPayParams } from '../utils';

interface Props {}

const FlightReview: React.FC<Props> = props => {
  const bookingInfo = useSelector((state: AppState) => state.booking.flight.info, shallowEqual);
  const dispatch = useDispatch<ThunkDispatch<AppState, null, Action<string>>>();
  const location = useLocation();
  const intl = useIntl();
  const { enqueueSnackbar, closeSnackbar } = useSnackbar();

  const getTripId = React.useMemo(() => {
    const state = location.state as some;
    const tripiId = state && state.tripiId;
    return tripiId;
  }, [location.state]);

  const bookFlight = React.useCallback(async () => {
    if (!getTripId || !bookingInfo) {
      return;
    }
    const json = await dispatch(
      fetchThunk(
        `${API_PATHS.getBooking}`,
        'post',
        JSON.stringify({
          bookingType: 'FLIGHT',
          tripId: getTripId,
          flight: genFlightPayParams(bookingInfo),
        }),
      ),
    );
    if (json.code === SUCCESS_CODE) {
      enqueueSnackbar(
        intl.formatMessage({ id: 'tripManagement.bookFlight' }),
        snackbarSetting(key => closeSnackbar(key), { color: 'success' }),
      );
      dispatch(
        goToAction({
          pathname: ROUTES.tripManagement.detail.gen(getTripId),
          search: '?tabIndex=0',
        }),
      );
    } else {
      enqueueSnackbar(
        json?.message,
        snackbarSetting(key => closeSnackbar(key), { color: 'error' }),
      );
    }
  }, [bookingInfo, closeSnackbar, dispatch, enqueueSnackbar, getTripId, intl]);

  if (!getTripId || !bookingInfo) {
    return <RedirectDiv message={intl.formatMessage({ id: 'flight.noTripId' })} />;
  }

  return <FlightReviewDesktop bookingInfo={bookingInfo} bookFlight={bookFlight} />;
};

export default FlightReview;
