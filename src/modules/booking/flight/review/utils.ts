import moment from 'moment';
import { some } from '../../../../constants';
import { DATE_TIME_FORMAT } from '../../../../models/moment';
import { FlightInfo, TravellersInfo } from '../booking/utils';
import { OneDirection } from '../utils';

export interface FlightPayParams {
  point: number;
  usingPoint: boolean;
  fromMobile: boolean;
  promotionCode?: string;
  promotion?: some;
  selectedPaymentMethod?: some;
}

function genInsuranceInfo(outbound: OneDirection, inbound: OneDirection, insurancePackage?: some) {
  const departureOutboundDate = outbound.ticket
    ? moment(
        `${outbound.ticket.outbound.departureDayStr} ${outbound.ticket.outbound.departureTimeStr}`,
        DATE_TIME_FORMAT,
      ).format(DATE_TIME_FORMAT)
    : '';

  let arrivalOutboundDate = outbound.ticket
    ? moment(
        `${outbound.ticket.outbound.arrivalDayStr} ${outbound.ticket.outbound.arrivalTimeStr}`,
        DATE_TIME_FORMAT,
      ).format(DATE_TIME_FORMAT)
    : '';

  if (inbound.ticket) {
    arrivalOutboundDate = moment(
      `${inbound.ticket.outbound.arrivalDayStr} ${inbound.ticket.outbound.arrivalTimeStr}`,
      DATE_TIME_FORMAT,
    ).format(DATE_TIME_FORMAT);
  }
  return {
    insurancePackageCode: insurancePackage ? insurancePackage.code : '',
    fromDate: arrivalOutboundDate,
    toDate: departureOutboundDate,
  };
}
export function convertTravellersFull(
  info: TravellersInfo,
  outbound: OneDirection,
  inbound: OneDirection,
  insurancePackage?: some,
) {
  let value: some[] = [];
  // const needPassport = outbound.ticket ? outbound.ticket.outbound.ticketdetail.needPassport : false;
  value = value
    .concat(
      info?.adults?.map((one, i) => ({
        userId: one.userId,
        outboundBaggageId: outbound.ticket ? outbound.extraBaggages[i]?.id : null,
        inboundBaggageId: inbound.ticket ? inbound.extraBaggages[i]?.id : null,
        insuranceInfo: insurancePackage
          ? genInsuranceInfo(outbound, inbound, insurancePackage)
          : null,
        passport: one.passportInfo.passport,
        passportExpiredDate: one.passportInfo.passportExpiredDate,
        passportCountryId: one.passportInfo.passportCountry?.id || null,
        nationalityCountryId: one.passportInfo.nationalityCountry?.id || null,
      })),
    )
    .concat(
      info?.children?.map((one, i) => ({
        userId: one.userId,
        dob: one.birthday,
        outboundBaggageId: outbound.ticket
          ? outbound.extraBaggages[info.adults.length + i]?.id
          : null,
        inboundBaggageId: inbound.ticket ? inbound.extraBaggages[info.adults.length + i]?.id : null,
        insuranceInfo: insurancePackage
          ? genInsuranceInfo(outbound, inbound, insurancePackage)
          : undefined,
        passport: one.passportInfo.passport,
        passportExpiredDate: one.passportInfo.passportExpiredDate,
        passportCountryId: one.passportInfo.passportCountry?.id || null,
        nationalityCountryId: one.passportInfo.nationalityCountry?.id || null,
      })),
    )
    .concat(
      info?.babies?.map(one => ({
        userId: one.userId,
        insuranceInfo: insurancePackage
          ? genInsuranceInfo(outbound, inbound, insurancePackage)
          : undefined,
        passport: one.passportInfo.passport,
        passportExpiredDate: one.passportInfo.passportExpiredDate,
        passportCountryId: one.passportInfo.passportCountry?.id || null,
        nationalityCountryId: one.passportInfo.nationalityCountry?.id || null,
      })),
    );
  return value;
}

export function genFlightPayParams(booking: FlightInfo) {
  const { tid, travellersInfo, inbound, outbound, buyInsurance, insurancePackage } = booking;
  if (!tid || !travellersInfo) {
    return {};
  }

  return {
    guests: convertTravellersFull(
      travellersInfo,
      outbound,
      inbound,
      buyInsurance ? insurancePackage : undefined,
    ),
    tickets: {
      inbound: tid.inbound
        ? { agencyId: tid.inbound.aid, requestId: tid.requestId, ticketId: tid.inbound.id }
        : null,
      outbound: {
        agencyId: tid.outbound.aid,
        requestId: tid.requestId,
        ticketId: tid.outbound.id,
      },
    },
  };
}
