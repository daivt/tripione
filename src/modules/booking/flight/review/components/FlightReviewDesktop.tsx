import { Container, useMediaQuery, useTheme, Button } from '@material-ui/core';
import React from 'react';
import { useDispatch } from 'react-redux';
import { ThunkDispatch } from 'redux-thunk';
import { Action } from 'redux';
import { FormattedMessage } from 'react-intl';
import AsideBound from '../../../common/AsideBound';
import { FlightInfo } from '../../booking/utils';
import FlightInfoBox from '../../common/FlightInfoBox';
import FlightTicketDialog from '../../common/FlightTicketDialog';
import FlightReviewBox from './FlightReviewBox';
import { Row, Col } from '../../../../common/components/elements';
import { AppState } from '../../../../../redux/reducers';
import { goBackAction } from '../../../../common/redux/reducer';

interface Props {
  bookingInfo: FlightInfo;
  bookFlight(): void;
}

const FlightReviewDesktop: React.FC<Props> = props => {
  const { bookingInfo, bookFlight } = props;
  const dispatch = useDispatch<ThunkDispatch<AppState, null, Action<string>>>();
  const [seeDetail, setSeeDetail] = React.useState(false);
  const theme = useTheme();
  const isTablet = useMediaQuery(theme.breakpoints.down('md'));

  return (
    <div>
      <Container style={{ flex: 1, display: 'flex', marginBottom: 32 }}>
        <Col style={{ flex: 1 }}>
          <FlightReviewBox bookingInfo={bookingInfo} bookFlight={bookFlight} />
          <Row style={{ justifyContent: 'center', marginTop: 24 }}>
            <Button
              variant="outlined"
              size="large"
              style={{ marginRight: 16, minWidth: 160 }}
              onClick={() => dispatch(goBackAction())}
            >
              <FormattedMessage id="back" />
            </Button>
            <Button
              variant="contained"
              color="secondary"
              disableElevation
              size="large"
              style={{ minWidth: 160 }}
              onClick={bookFlight}
            >
              <FormattedMessage id="continue" />
            </Button>
          </Row>
        </Col>
        <AsideBound
          isTablet={isTablet}
          style={{ width: 360, marginLeft: isTablet ? 0 : 24 }}
          direction="right"
        >
          <FlightInfoBox seeDetail={() => setSeeDetail(true)} booking={bookingInfo} />
        </AsideBound>
      </Container>
      <FlightTicketDialog
        close={() => setSeeDetail(false)}
        open={seeDetail}
        booking={bookingInfo}
      />
    </div>
  );
};

export default FlightReviewDesktop;
