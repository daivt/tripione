/* eslint-disable no-nested-ternary */
/* eslint-disable react/jsx-one-expression-per-line */
import { Divider, Paper, Typography } from '@material-ui/core';
import * as React from 'react';
import { FormattedMessage } from 'react-intl';
import styled from 'styled-components';
import { BLUE_100 } from '../../../../../configs/colors';
import { some } from '../../../../../constants';
import { FlightInfo, TravellerInfo } from '../../booking/utils';

// const Typography = (props: some) => (
//   <Typography variant="body2" variant="body2" style={{ ...props.style, flexShrink: 0 }}>
//     {props.children}
//   </Typography>
// );
const Line = styled.div`
  min-height: 40px;
  display: flex;
  align-items: center;
`;
function renderInfo(name: string, infos: TravellerInfo[], offset: number, data: FlightInfo) {
  if (!data.outbound.ticket) {
    return false;
  }

  const outboundBag = data.outbound.extraBaggages;
  const inboundBag = data.inbound.extraBaggages;
  const ticketOutboundBag = data.outbound.ticket.outbound.baggages;
  let ticketInboundBag: some;
  if (data.inbound.ticket) {
    ticketInboundBag = data.inbound.ticket.outbound.baggages;
  }

  const extraBaggagesMsg = outboundBag.map((v, index) => ({
    out:
      ticketOutboundBag && outboundBag[offset + index] ? (
        <>
          {`${outboundBag[offset + index].name} - ${outboundBag[offset + index].price} `}
          <FormattedMessage id="currency" />
        </>
      ) : (
        <FormattedMessage id="none" />
      ),
    in: ticketInboundBag ? (
      inboundBag && inboundBag[offset + index] ? (
        <>
          {`${inboundBag[offset + index]?.name} - ${inboundBag[offset + index]?.price}`}
          <FormattedMessage id="currency" />
        </>
      ) : (
        <FormattedMessage id="none" />
      )
    ) : null,
  }));

  return infos.map((info, index) => {
    return (
      // eslint-disable-next-line react/no-array-index-key
      <div key={index}>
        <div style={{ display: 'flex', padding: '8px 0px 16px 0px' }}>
          <Typography
            variant="body2"
            style={{
              fontWeight: 500,
              height: '40px',
              flexBasis: '160px',
              display: 'flex',
              alignItems: 'center',
            }}
          >
            {index + 1 + offset}
            .&nbsp;
            <FormattedMessage id={name} values={{ num: infos.length !== 1 ? index + 1 : '' }} />
          </Typography>
          <div style={{ display: 'flex', flexDirection: 'column', width: '100%' }}>
            <div style={{ display: 'flex', flexDirection: 'column', paddingLeft: 16 }}>
              <Line>
                <Typography variant="body2" style={{ width: 180 }}>
                  <FormattedMessage id="fullName" />
                  :&nbsp;
                </Typography>
                <Typography variant="body2" style={{ width: '220px' }}>
                  {info.fullName}
                </Typography>
                <Typography variant="body2" style={{ width: 180 }}>
                  <FormattedMessage id="gender" />:
                </Typography>
                <Typography variant="body2" style={{ fontWeight: 500, marginLeft: '20px' }}>
                  <FormattedMessage id={info.gender === 'm' ? 'male' : 'female'} />
                </Typography>
              </Line>
              {((name === 'flight.adult' && !!info.passportInfo.passport) ||
                name !== 'flight.adult') && (
                <Line>
                  <Typography variant="body2" style={{ width: 180 }}>
                    <FormattedMessage id="birthday" />
                    :&nbsp;
                  </Typography>
                  <Typography variant="body2">{info.birthday}</Typography>
                </Line>
              )}
              {!!info.passportInfo.passport && (
                <>
                  <Line>
                    <Typography variant="body2" style={{ width: 180 }}>
                      <FormattedMessage id="flight.passport" />
                      :&nbsp;
                    </Typography>
                    <Typography variant="body2" style={{ width: '220px' }}>
                      {info.passportInfo.passport}
                    </Typography>
                    <Typography variant="body2" style={{ width: 180 }}>
                      <FormattedMessage id="flight.passportExpired" />:
                    </Typography>
                    <Typography variant="body2" style={{ marginLeft: '20px' }}>
                      {info.passportInfo.passportExpiredDate}
                    </Typography>
                  </Line>
                  <Line>
                    <Typography variant="body2" style={{ width: 180 }}>
                      <FormattedMessage id="flight.passportCountry" />
                      :&nbsp;
                    </Typography>
                    <Typography variant="body2" style={{ width: '220px' }}>
                      {info.passportInfo.passportCountry?.name}
                    </Typography>
                    <Typography variant="body2" style={{ width: 180 }}>
                      <FormattedMessage id="flight.nationalityCountry" />
                      :&nbsp;
                    </Typography>
                    <Typography variant="body2" style={{ marginLeft: '20px' }}>
                      {info.passportInfo.nationalityCountry?.name}
                    </Typography>
                  </Line>
                </>
              )}
            </div>
            {name !== 'flight.infant' && (
              <div>
                <Line
                  style={{
                    background: BLUE_100,
                    paddingLeft: 16,
                    display: 'flex',
                    alignItems: 'center',
                  }}
                >
                  <Typography variant="body2" style={{ minWidth: 180 }}>
                    <FormattedMessage id="flight.outBaggages" />
                  </Typography>
                  <Typography variant="body2">{extraBaggagesMsg[index].out}</Typography>
                </Line>
                {extraBaggagesMsg[index].in && (
                  <Line
                    style={{
                      background: BLUE_100,
                      paddingLeft: 16,
                      display: 'flex',
                      alignItems: 'center',
                    }}
                  >
                    <Typography variant="body2" style={{ minWidth: 180 }}>
                      <FormattedMessage id="flight.inBaggages" />
                    </Typography>
                    <Typography variant="body2">{extraBaggagesMsg[index].in}</Typography>
                  </Line>
                )}
              </div>
            )}
          </div>
        </div>
        {index !== infos.length - 1 && <Divider />}
      </div>
    );
  });
}

interface IFlightReviewBoxProps {
  bookingInfo: FlightInfo;
  bookFlight(): void;
}

const FlightReviewBox: React.FunctionComponent<IFlightReviewBoxProps> = props => {
  const { bookingInfo } = props;

  return (
    <Paper variant="outlined" style={{ padding: 16 }}>
      <Typography variant="h6">
        <FormattedMessage id="flight.travellersInfo" />
      </Typography>
      {renderInfo('flight.adult', bookingInfo.travellersInfo.adults, 0, bookingInfo)}
      {!!bookingInfo.travellersInfo.children.length && <Divider />}
      {renderInfo(
        'flight.children',
        bookingInfo.travellersInfo.children,
        bookingInfo.travellersInfo.adults.length,
        bookingInfo,
      )}
      {!!bookingInfo.travellersInfo.babies.length && <Divider />}
      {renderInfo(
        'flight.infant',
        bookingInfo.travellersInfo.babies,
        bookingInfo.travellersInfo.adults.length + bookingInfo.travellersInfo.children.length,
        bookingInfo,
      )}
    </Paper>
  );
};

export default FlightReviewBox;
