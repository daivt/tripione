import { Button, Dialog, Divider, IconButton, InputAdornment, Typography } from '@material-ui/core';
import IconClose from '@material-ui/icons/CloseOutlined';
import SearchIcon from '@material-ui/icons/Search';
import { debounce } from 'lodash';
import moment from 'moment';
import { useSnackbar } from 'notistack';
import * as React from 'react';
import { FormattedMessage, useIntl } from 'react-intl';
import { useDispatch } from 'react-redux';
import { Action } from 'redux';
import { ThunkDispatch } from 'redux-thunk';
import { API_PATHS } from '../../../configs/API';
import { BLUE } from '../../../configs/colors';
import { some, SUCCESS_CODE } from '../../../constants';
import { DATE_FORMAT, DATE_FORMAT_BACK_END } from '../../../models/moment';
import { AppState } from '../../../redux/reducers';
import { ReactComponent as IconContact } from '../../../svg/booking/ic_contact.svg';
import { ReactComponent as IconEmpty } from '../../../svg/ic_emptyData.svg';
import { ReactComponent as IconNodata } from '../../../svg/ic_noContent.svg';
import { Col, Row, snackbarSetting } from '../../common/components/elements';
import FormControlTextField from '../../common/components/FormControlTextField';
import TableCustom, { Columns } from '../../common/components/TableCustom';
import { fetchThunk } from '../../common/redux/thunk';
import { PAGE_SIZE_20 } from '../constants';

interface ContactInfo {
  birthday: string | null;
  department: { id: number; name: string } | null;
  email: string;
  employeeCode: string;
  gender: number;
  id: number;
  identityExpiredDate: string;
  identityNumber: string;
  isActive: true;
  jobTitle: string;
  name: string;
  phone: string;
  role: string;
}

interface Props {
  onSelect?: (info: ContactInfo) => void;
  style?: React.CSSProperties;
  exceptionList?: some[];
}

const ListContactDialog: React.FunctionComponent<Props> = props => {
  const { onSelect, style, exceptionList } = props;
  const dispatch = useDispatch<ThunkDispatch<AppState, null, Action<string>>>();
  const intl = useIntl();
  const { enqueueSnackbar, closeSnackbar } = useSnackbar();
  const [search, setSearch] = React.useState('');
  const [open, setOpen] = React.useState(false);
  const [loading, setLoading] = React.useState(true);
  const [count, setCount] = React.useState(0);
  const [data, setData] = React.useState<some | undefined>(undefined);

  const columns = React.useMemo(() => {
    return [
      {
        title: 'contact.employeeCode',
        dataIndex: 'employeeCode',
      },
      {
        title: 'fullName',
        dataIndex: 'name',
      },
      {
        title: 'birthday',
        dataIndex: 'birthday',
        render: (record: some) => (
          <Typography variant="body2">
            {record?.birthday && moment(record?.birthday, DATE_FORMAT_BACK_END).format(DATE_FORMAT)}
          </Typography>
        ),
      },
      {
        title: 'email',
        dataIndex: 'email',
      },
      {
        title: 'contact.department',
        dataIndex: 'department',
        render: (record: some) => (
          <Typography variant="body2">{record?.department?.name}</Typography>
        ),
      },
      {
        title: 'contact.jobTitle',
        dataIndex: 'jobTitle',
        render: (record: some) => <Typography variant="body2">{record?.jobTitle?.name}</Typography>,
      },
      {
        title: 'phoneNumber',
        dataIndex: 'phone',
      },
    ] as Columns[];
  }, []);

  const searchData = React.useCallback(
    debounce(
      async (searchStr: string) => {
        setLoading(true);
        const json = await dispatch(
          fetchThunk(
            API_PATHS.getListUser,
            'post',
            JSON.stringify({
              filters: { searchStr },
              page: 1,
              pageSize: PAGE_SIZE_20,
            }),
          ),
        );
        if (json?.code === SUCCESS_CODE) {
          setData(json.data);
          setCount(one => one + 1);
        } else {
          json?.message &&
            enqueueSnackbar(
              json.message,
              snackbarSetting(key => closeSnackbar(key), { color: 'error' }),
            );
        }
        setLoading(false);
      },
      500,
      {
        trailing: true,
        leading: false,
      },
    ),
    [],
  );

  return (
    <>
      <Button onClick={() => setOpen(true)} style={{ marginLeft: 16, ...style }}>
        <IconContact style={{ marginRight: 8 }} />
        <Typography variant="body2" style={{ color: BLUE }}>
          <FormattedMessage id="contact.add" />
        </Typography>
      </Button>
      <Dialog
        open={open}
        onClose={() => {
          setOpen(false);
          setLoading(true);
        }}
        PaperProps={{
          style: {
            minWidth: 780,
            minHeight: 536,
          },
        }}
        keepMounted={false}
        maxWidth="lg"
        onEnter={() => searchData(search)}
      >
        <Row>
          <Typography variant="subtitle1" style={{ flex: 1, padding: '12px 0px 12px 16px' }}>
            <FormattedMessage id="contact.list" />
          </Typography>
          <IconButton onClick={() => setOpen(false)} style={{ padding: 10 }}>
            <IconClose />
          </IconButton>
        </Row>
        <Divider />
        <Col style={{ padding: 16, overflow: 'hidden' }}>
          <FormControlTextField
            placeholder={intl.formatMessage({ id: 'contact.findContact' })}
            fullWidth
            value={search}
            onChange={e => {
              setSearch(e.target.value);
              searchData(e.target.value);
            }}
            optional
            endAdornment={
              <InputAdornment position="end" style={{ marginRight: 8 }}>
                <IconButton size="small" edge="start" tabIndex={-1}>
                  <SearchIcon />
                </IconButton>
              </InputAdornment>
            }
          />
          <TableCustom
            paperProps={{ elevation: 0 }}
            dataSource={
              exceptionList
                ? data?.itemList.filter(
                    (v: some) => exceptionList.findIndex(obj => obj.userId === v.id) === -1,
                  )
                : data?.itemList
            }
            loading={loading}
            columns={columns}
            noColumnIndex
            onRowClick={(record: some) => {
              onSelect && onSelect(record as ContactInfo);
              setOpen(false);
            }}
            caption={
              <>
                {!search && count <= 1 ? (
                  <Col style={{ alignItems: 'center', margin: '12px' }}>
                    <IconEmpty />
                    <Typography variant="body2" color="textSecondary" style={{ marginTop: 24 }}>
                      <FormattedMessage id="contact.emptyData" />
                    </Typography>
                    <Button
                      variant="contained"
                      size="large"
                      color="secondary"
                      disableElevation
                      style={{ marginTop: 16, minWidth: 180 }}
                    >
                      <FormattedMessage id="contact.addNew" />
                    </Button>
                  </Col>
                ) : (
                  <Col style={{ alignItems: 'center', margin: '12px' }}>
                    <IconNodata />
                    <Typography variant="body2" color="textSecondary" style={{ marginTop: 24 }}>
                      <FormattedMessage id="contact.emptyData" />
                    </Typography>
                  </Col>
                )}
              </>
            }
          />
        </Col>
      </Dialog>
    </>
  );
};

export default ListContactDialog;
