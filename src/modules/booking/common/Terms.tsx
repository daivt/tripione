import { Container } from '@material-ui/core';
import * as React from 'react';
import Helmet from 'react-helmet';
import { injectIntl, WrappedComponentProps } from 'react-intl';
import { PageWrapper } from '../../common/components/elements';

interface ITermsProps extends WrappedComponentProps {}

const Terms: React.FunctionComponent<ITermsProps> = props => {
  const { intl } = props;
  const [html, setHtml] = React.useState('');

  React.useEffect(() => {
    const read = async () => {
      const content = await fetch('/static/terms.xml');
      setHtml(await content.text());
    };
    read();
  }, []);

  return (
    <>
      <Helmet>
        <title>{intl.formatMessage({ id: 'terms.title' })}</title>
      </Helmet>
      <PageWrapper>
        <Container style={{ flex: 1 }}>
          <div style={{ padding: '25px 0' }} dangerouslySetInnerHTML={{ __html: html }} />
        </Container>
      </PageWrapper>
    </>
  );
};

export default injectIntl(Terms);
