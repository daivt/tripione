import { Skeleton } from '@material-ui/lab';
import * as React from 'react';
import RPI from 'react-progressive-image';
import styled, { keyframes } from 'styled-components';

const fadeIn = keyframes`
  from {
    opacity: 0;
  }
  to {
    opacity: 1;
  }
`;

const FadeInImg = styled.img`
  animation-name: ${fadeIn};
  animation-duration: 0.3s;
  animation-timing-function: linear;
`;

interface IProgressiveImageProps extends React.ImgHTMLAttributes<HTMLImageElement> {}

const ProgressiveImage: React.FunctionComponent<IProgressiveImageProps> = props => {
  const { src, style, alt } = props;
  return (
    <RPI src={src || ''} placeholder={src || ''}>
      {(srcTmp: string, loading: boolean) =>
        loading ? (
          <Skeleton
            variant="rect"
            style={{
              ...style,
              display: 'inline-block',
            }}
          />
        ) : (
          <FadeInImg
            {...props}
            alt={alt || ''}
            style={{
              ...style,
            }}
          />
        )
      }
    </RPI>
  );
};

export default ProgressiveImage;
