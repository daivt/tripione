import { combineReducers } from 'redux';
import hotelReducer, { HotelBookingState } from '../hotel/redux/hotelBookingReducer';
import flightReducer, { FlightBookingState } from '../flight/redux/flightBookingReducer';

export interface BookingState {
  hotel: HotelBookingState;
  flight: FlightBookingState;
}

export default combineReducers({
  hotel: hotelReducer,
  flight: flightReducer,
});
