export interface ITransactionsTopup {
  bankId: number | null;
  amount: number | null;
  toAccount: { [key: string]: string;}
}

export const TransactionTopupDefault: ITransactionsTopup = {
  bankId: null,
  amount: null,
  toAccount: {}
}
