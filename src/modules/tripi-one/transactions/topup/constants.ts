export const fakeTransactionsTopupBanks = {
  code: "SUCCESS",
  message: "success",
  data: {
    itemList: [
      {
        id: 1,
        shortName: "Techcombank",
        fullName: "Ngân hàng Kĩ Thương Việt Nam",
        logo: "https://storage.googleapis.com/tripi-assets/images/payment-method-banks/techcombank_logo.png"
      },
      {
        id: 2,
        shortName: "Vietcombank",
        fullName: "Ngân hàng TMCP Ngoại thương Việt Nam",
        logo: "https://storage.googleapis.com/tripi-assets/images/payment-method-banks/vietcombank_logo.png"
      }
    ]
  }
}

export const fakeTransactionsTopupRequest = {
  code: "SUCCESS",
  message: "success",
  data: {
    id: 123456789,
    bank: {
      id: 1,
      shortName: "Techcombank",
      fullName: "Ngân hàng Kĩ Thương Việt Nam",
      logo: "https://storage.googleapis.com/tripi-assets/images/payment-method-banks/techcombank_logo.png"
    },
    transferInfo: {
      fromAccount: null,
      toAccount: {
        name: "Công ty Cổ phần Phát tiển Công nghệ Thương mại và Du lịch",
        number: "0011001100110011"
      },
      amount: 8000000,
      fee: null,
      currencyCode: "VND",
      note: "NAPTIEN UITYJHG"
    },
    isTopup: true,
    createdDate: 1591157814583,
    expiredDate: null,
    status: null
  }
}
