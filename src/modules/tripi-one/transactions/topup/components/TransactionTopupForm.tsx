import { Button } from '@material-ui/core';
import { useFormik } from 'formik';
import * as React from 'react';
import { FormattedMessage, useIntl } from 'react-intl';
import { useDispatch } from 'react-redux';
import { Action } from 'redux';
import { ThunkDispatch } from 'redux-thunk';
import * as yup from 'yup';
import { some } from '../../../../../constants';
import { AppState } from '../../../../../redux/reducers';
import { Row } from '../../../../common/components/elements';
import { NumberFormatCustom } from '../../../../common/components/Form';
import FormControlTextField from '../../../../common/components/FormControlTextField';
import InputAdornmentSolid from '../../../../common/components/InputAdornmentSolid';
import { goBackAction } from '../../../../common/redux/reducer';
import AmountPicker from '../../components/AmountPicker';
import BankPicker from '../../components/BankPicker';
import { ITransactionsTopup, TransactionTopupDefault } from '../utils';

interface Props {
  data: some;
  onSubmit(value: ITransactionsTopup): void;
}

const TransactionsTopupForm = (props: Props) => {
  const { data, onSubmit } = props;
  const dispatch = useDispatch<ThunkDispatch<AppState, null, Action<string>>>();
  const intl = useIntl();
  const transactionsTopUpSchema = yup.object().shape({
    bankId: yup
      .number()
      .nullable()
      .required(intl.formatMessage({ id: 'required' })),
    amount: yup
      .number()
      .nullable()
      .required(intl.formatMessage({ id: 'required' })),
  });

  const formik = useFormik({
    initialValues: TransactionTopupDefault,
    onSubmit: values => {
      onSubmit(values);
      // dispatch(goToAction({ pathname: 'ROUTES.transactions.topupInstruction', state: { topupRequestData: JSON.data} }))
    },
    validationSchema: transactionsTopUpSchema,
  });

  return (
    <form onSubmit={formik.handleSubmit} autoComplete="off">
      <BankPicker
        bankId={formik.values.bankId || undefined}
        options={data.itemList}
        onChange={value => formik.setFieldValue('bankId', Number(value), false)}
        error={formik.errors.bankId && formik.touched.bankId ? formik.errors.bankId : undefined}
      />
      <Row style={{ flexWrap: 'wrap', marginBottom: 16 }}>
        <FormControlTextField
          id="amount"
          label={<FormattedMessage id="addMoney" />}
          formControlStyle={{ width: 250 }}
          placeholder={intl.formatMessage({ id: 'addMoney' })}
          value={formik.values.amount || ''}
          onChange={e => formik.setFieldValue('amount', Number(e.target.value))}
          inputProps={{
            maxLength: 100,
          }}
          inputComponent={NumberFormatCustom as any}
          endAdornment={
            <InputAdornmentSolid>
              <FormattedMessage id="currency" />
            </InputAdornmentSolid>
          }
          errorMessage={
            formik.errors.amount && formik.touched.amount ? formik.errors.amount : undefined
          }
        />
        <AmountPicker onChange={value => formik.setFieldValue('amount', Number(value), false)} />
      </Row>
      <Row>
        <Button
          type="submit"
          variant="contained"
          style={{ minWidth: 144, marginRight: 20 }}
          color="secondary"
          size="large"
          disableElevation
        >
          <FormattedMessage id="continue" />
        </Button>
        <Button
          variant="outlined"
          style={{ minWidth: 144 }}
          size="large"
          disableElevation
          onClick={() => dispatch(goBackAction())}
        >
          <FormattedMessage id="reject" />
        </Button>
      </Row>
    </form>
  );
};

export default TransactionsTopupForm;
