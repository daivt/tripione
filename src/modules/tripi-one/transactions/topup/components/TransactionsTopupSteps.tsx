import { Typography } from '@material-ui/core';
import ReportProblemOutlinedIcon from '@material-ui/icons/ReportProblemOutlined';
import * as React from 'react';
import { FormattedHTMLMessage, FormattedMessage } from 'react-intl';
import styled from 'styled-components';
import { GREEN, GREY_300, RED } from '../../../../../configs/colors';
import { Row } from '../../../../common/components/elements';

const Circle = styled.div`
  width: 10px;
  height: 10px;
  border: 3px solid;
  border-radius: 50%;
  border-color: ${props => (props.color ? props.color : GREEN)};
`;

const RoundedLine = styled.div`
  width: 32px;
  transform: rotate(90deg) translate3d(-15px, 8px, 0);
  border-top: 3px solid ${GREY_300};
  border-bottom: 3px solid ${GREY_300};
  border-radius: 3px;
`;

interface Props {}

const TransactionsTopupSteps = (props: Props) => {
  return (
    <>
      <Row style={{ marginBottom: 28 }}>
        <Circle />
        <Typography variant="h5" style={{ margin: '0 12px' }}>
          1
        </Typography>
        <Typography variant="body2">
          <FormattedMessage id="transactions.topup.instruction1" />
        </Typography>
      </Row>
      <RoundedLine />
      <Row style={{ marginBottom: 28 }}>
        <Circle />
        <Typography variant="h5" style={{ margin: '0 12px' }}>
          2
        </Typography>
        <Typography variant="body2">
          <FormattedMessage id="transactions.topup.instruction2" />
        </Typography>
      </Row>
      <RoundedLine />
      <Row style={{ marginBottom: 28 }}>
        <Circle color={RED} />
        <ReportProblemOutlinedIcon style={{ width: 22, color: RED, margin: '0 8px' }} />
        <Typography variant="body2">
          <FormattedHTMLMessage id="transactions.topup.instruction3" />
        </Typography>
      </Row>
    </>
  );
};

export default TransactionsTopupSteps;
