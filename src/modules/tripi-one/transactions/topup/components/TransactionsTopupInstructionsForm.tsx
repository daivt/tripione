import { Button, Paper, Typography } from '@material-ui/core';
import ReportProblemOutlinedIcon from '@material-ui/icons/ReportProblemOutlined';
import * as React from 'react';
import { FormattedMessage, FormattedHTMLMessage } from 'react-intl';
import { useDispatch } from 'react-redux';
import { ThunkDispatch } from 'redux-thunk';
import { Action } from 'redux';
import { RED, WHITE } from '../../../../../configs/colors';
import CopyTextField from '../../../../common/components/CopyTextField';
import { Col, Row } from '../../../../common/components/elements';
import { NumberFormatCustom } from '../../../../common/components/Form';
import TransactionsTopupSteps from './TransactionsTopupSteps';
import { some } from '../../../../../constants';
import { AppState } from '../../../../../redux/reducers';
import { goBackAction } from '../../../../common/redux/reducer';

interface Props {
  data: some;
}

const TransactionsTopupInstructionForm = (props: Props) => {
  const dispatch = useDispatch<ThunkDispatch<AppState, null, Action<string>>>();
  const { data } = props;
  return (
    <div>
      <TransactionsTopupSteps />
      <Row>
        <Typography variant="body1" style={{ fontWeight: 'bold', marginBottom: 16 }}>
          <FormattedMessage id="transactions.info" />:
        </Typography>
      </Row>
      <Row style={{ alignItems: 'flex-start' }}>
        <Paper
          style={{
            display: 'flex',
            alignItems: 'center',
            minWidth: 140,
            height: 80,
            margin: '0 20px 12px 0',
            padding: 0,
            background: WHITE,
          }}
        >
          <img src={data.bank.logo} alt="Bank logo" style={{ width: 140 }} />
        </Paper>
        <Col>
          <Row>
            <Typography variant="body2">
              {data.bank.fullName}({data.bank.shortName})
            </Typography>
          </Row>
          <Row>
            <Typography variant="body2" style={{ marginBottom: 8 }}>
              <FormattedMessage id="transactions.toAccount" />
              :&nbsp;
              {data.transferInfo.toAccount.name}
            </Typography>
          </Row>
          <CopyTextField
            label={<FormattedMessage id="transactions.accountNumber" />}
            value={data.transferInfo.toAccount.number}
            errorMessage=""
            formControlStyle={{ width: 250 }}
            optional
          />
          <CopyTextField
            label={<FormattedMessage id="transactions.transferContent" />}
            value={data.transferInfo.note}
            errorMessage=""
            formControlStyle={{ width: 250 }}
            optional
          />
          <CopyTextField
            label={<FormattedMessage id="transactions.amount" />}
            inputComponent={NumberFormatCustom as any}
            value={data.transferInfo.amount}
            errorMessage=""
            formControlStyle={{ width: 250 }}
            optional
          />
        </Col>
      </Row>
      <Row>
        <ReportProblemOutlinedIcon style={{ color: RED, marginRight: 8 }} />
        <Typography variant="body2" style={{ color: RED }}>
          <FormattedMessage id="warning" />:
        </Typography>
      </Row>
      <Col style={{ padding: '12px 0 24px' }}>
        <Typography variant="body2" style={{ marginBottom: 8 }}>
          <FormattedHTMLMessage id="transactions.topup.warning1" />
        </Typography>
        <Typography variant="body2" style={{ marginBottom: 8 }}>
          <FormattedMessage id="transactions.topup.warning2" />
        </Typography>
        <Typography variant="body2">
          <FormattedMessage id="transactions.topup.warning3" />
        </Typography>
      </Col>
      <Button
        variant="outlined"
        style={{ minWidth: 144, marginRight: 20 }}
        size="large"
        disableElevation
        onClick={() => dispatch(goBackAction())}
      >
        <FormattedMessage id="back" />
      </Button>
    </div>
  );
};

export default TransactionsTopupInstructionForm;
