import { debounce } from 'lodash';
import * as React from 'react';
import { useDispatch } from 'react-redux';
import { Action } from 'redux';
import { ThunkDispatch } from 'redux-thunk';
import { ROUTES } from '../../../../../configs/routes';
import { some } from '../../../../../constants';
import { AppState } from '../../../../../redux/reducers';
import LoadingIcon from '../../../../common/components/LoadingIcon';
import { goToAction } from '../../../../common/redux/reducer';
import TransactionsTopupForm from '../components/TransactionTopupForm';
import { fakeTransactionsTopupBanks, fakeTransactionsTopupRequest } from '../constants';
import { ITransactionsTopup } from '../utils';

interface Props {}

const TransactionsTopup = (props: Props) => {
  const dispatch = useDispatch<ThunkDispatch<AppState, null, Action<string>>>();
  const [data, setData] = React.useState<some | undefined>();
  const [loading, setLoading] = React.useState(true);

  const fetchData = React.useCallback(
    debounce(
      async () => {
        setLoading(true);
        setData(fakeTransactionsTopupBanks.data);
        setLoading(false);
      },
      200,
      {
        trailing: true,
        leading: false,
      },
    ),
    [],
  );

  const onSubmit = React.useCallback(
    (values: ITransactionsTopup) => {
      dispatch(
        goToAction({
          pathname: ROUTES.transactions.topupInstruction,
          state: { instructionData: fakeTransactionsTopupRequest.data },
        }),
      );
    },
    [dispatch],
  );

  React.useEffect(() => {
    fetchData();
  }, [fetchData]);

  return data ? (
    <TransactionsTopupForm data={data} onSubmit={onSubmit} />
  ) : (
    <LoadingIcon style={{ height: 320 }} />
  );
};

export default TransactionsTopup;
