import * as React from 'react';
import { shallowEqual, useDispatch, useSelector } from 'react-redux';
import { Action } from 'redux';
import { ThunkDispatch } from 'redux-thunk';
import { some } from '../../../../../constants';
import { AppState } from '../../../../../redux/reducers';
import { goBackAction } from '../../../../common/redux/reducer';
import TransactionsTopupInstructionForm from '../components/TransactionsTopupInstructionsForm';

interface Props {}

const TransactionsTopupInstruction = (props: Props) => {
  const dispatch = useDispatch<ThunkDispatch<AppState, null, Action<string>>>();
  const router = useSelector((state: AppState) => state.router, shallowEqual);

  const getActionData = React.useMemo(() => {
    const state = router.location.state as some;
    const data = state && (state.instructionData as some);
    return data;
  }, [router.location.state]);

  React.useEffect(() => {
    if (!getActionData) {
      dispatch(goBackAction());
    }
  }, [dispatch, getActionData]);

  if (!getActionData) {
    return null;
  }
  return <TransactionsTopupInstructionForm data={getActionData} />;
};

export default TransactionsTopupInstruction;
