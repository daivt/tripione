import { debounce } from 'lodash';
import moment from 'moment';
import queryString from 'query-string';
import React, { useCallback } from 'react';
import { useDispatch } from 'react-redux';
import { useLocation } from 'react-router';
import { Action } from 'redux';
import { ThunkDispatch } from 'redux-thunk';
import { some } from '../../../../../constants';
import { defaultPaginationFilter, PaginationFilter } from '../../../../../models/pagination';
import { AppState } from '../../../../../redux/reducers';
import { goToReplace } from '../../../../common/redux/reducer';
import CurrentAmount from '../components/CurrentAmount';
import TableBox from '../components/TableBox';
import { fakeTransactionsCurrentAmount, fakeTransactionsData } from '../constants';
import { defaultTransactionsFilterParams, ITransactionsFilterParams } from '../utils';

interface Props {}

const TransactionsHistory: React.FunctionComponent<Props> = props => {
  const dispatch = useDispatch<ThunkDispatch<AppState, null, Action<string>>>();
  const [data, setData] = React.useState<some | undefined>();
  const [currentAmountData, setCurrentAmountData] = React.useState<some | undefined>();
  const [loading, setLoading] = React.useState(true);
  const [filter, setFilter] = React.useState<ITransactionsFilterParams>(
    defaultTransactionsFilterParams,
  );
  const [pagination, setPagination] = React.useState<PaginationFilter>(defaultPaginationFilter);

  const location = useLocation();
  const updateQueryParams = React.useCallback(() => {
    if (location.search) {
      const filterParams = queryString.parse(location.search) as any;
      const filterTmp = queryString.parse(filterParams.filters) as any;
      const fromDate = filterTmp.fromDate && parseInt(`${filterTmp.fromDate}`, 10);
      const toDate = filterTmp.toDate && parseInt(`${filterTmp.toDate}`, 10);
      setFilter({
        fromDate: toDate < fromDate ? undefined : fromDate,
        toDate: toDate > moment().valueOf() ? moment().valueOf() : toDate,
        transactionsType:
          filterTmp.transactionsType && parseInt(`${filterTmp.transactionsType}`, 10),
        status: filterTmp.status && parseInt(`${filterTmp.status}`, 10),
      });
      const paginationTmp = {
        page: filterParams.page ? parseInt(`${filterParams.page}`, 10) : 1,
        pageSize: filterParams.pageSize ? parseInt(`${filterParams.pageSize}`, 10) : 10,
      } as PaginationFilter;
      setPagination(paginationTmp);
    } else {
      dispatch(
        goToReplace({
          search: queryString.stringify({
            ...defaultPaginationFilter,
            filters: queryString.stringify(defaultTransactionsFilterParams),
          }),
        }),
      );
    }
  }, [dispatch, location.search]);

  const fetchData = useCallback(
    debounce(
      async (filterParams: ITransactionsFilterParams, paginationParams: PaginationFilter) => {
        setLoading(true);
        setData(fakeTransactionsData.data);
        setLoading(false);
      },
      200,
      {
        trailing: true,
        leading: false,
      },
    ),
    [],
  );

  const fetchCurrentAmountData = useCallback(
    () => setCurrentAmountData(fakeTransactionsCurrentAmount.data),
    [],
  );

  React.useEffect(() => {
    updateQueryParams();
  }, [updateQueryParams]);

  React.useEffect(() => {
    fetchCurrentAmountData();
  }, [fetchCurrentAmountData]);

  React.useEffect(() => {
    fetchData(filter, pagination);
  }, [fetchData, filter, pagination]);

  return (
    <>
      <CurrentAmount data={currentAmountData} />
      <TableBox
        data={data}
        loading={loading}
        filter={filter}
        onUpdateFilter={values => {
          dispatch(
            goToReplace({
              search: queryString.stringify({
                ...pagination,
                page: 1,
                filters: queryString.stringify(values),
              }),
            }),
          );
        }}
        pagination={pagination}
        onUpdatePagination={values => {
          dispatch(
            goToReplace({
              search: queryString.stringify({ ...values, filters: queryString.stringify(filter) }),
            }),
          );
        }}
      />
    </>
  );
};

export default TransactionsHistory;
