export const fakeTransactionsCurrentAmount = {
  code: 'SUCCESS',
  message: 'success',
  data: {
    companyName: 'Công ty TNHH Lữ Hành Du Lịch Tân Kỷ Nguyên',
    amount: 12345678900,
    currencyCode: "VND"
  },
};

export const fakeTransactionsData = {
  code: 'SUCCESS',
  message: 'success',
  data: {
    totalResults: 100,
    page: 1,
    pageSize: 10,
    itemList: [
      {
        id: 123456789,
        bank: {
          bankID: 1,
          shortName: 'Techcombank',
          fullName: 'Ngân hàng Kĩ Thương Việt Nam',
          logo:
            'https://toppng.com/uploads/preview/techcombank-logo-vector-11573944006nwkjhnpkvs.png',
        },
        transferInfo: {
          fromAccount: {
            name: null,
            number: null,
          },
          toAccount: {
            name: 'Công ty Cổ phần Phát tiển Công nghệ Thương mại và Du lịch',
            number: '0011001100110011',
          },
          amount: 8000000,
          fee: 23000,
          currencyCode: 'VND',
          note: null,
        },
        isTopup: false,
        createdDate: 1590641108809,
        expiredDate: 1590894000000,
        status: {
          id: 1,
          text: 'Thành Công',
        },
      },
      {
        id: 987654321,
        bank: {
          bankID: 2,
          shortName: 'Vietcombank',
          fullName: 'Ngân hàng TMCP Ngoại thương Việt Nam',
          logo:
            'https://brasol.vn/public/ckeditor/uploads/tin-tuc/brasol.vn-logo-vietcombank-logo-vietcombank.jpg',
        },
        transferInfo: {
          fromAccount: {
            name: 'Công ty cổ phần lữ hành VietTravel',
            number: '0011001100110011',
          },
          toAccount: {
            name: null,
            number: null,
          },
          amount: 2000000,
          fee: 5500,
          currencyCode: 'VND',
          note: 'NAPTIEN UITYJHG',
        },
        isTopup: true,
        createdDate: 1590641108809,
        expiredDate: null,
        status: {
          id: 2,
          text: 'Chờ thanh toán',
        },
      },
    ],
  },
};

