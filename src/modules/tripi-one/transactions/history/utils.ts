import moment from 'moment';

export interface ITransactionsFilterParams {
  fromDate?: number | undefined;
  toDate?: number | undefined;
  transactionsType?: number;
  status?: number;
}

export const defaultTransactionsFilterParams: ITransactionsFilterParams = {
  toDate: moment().valueOf(),
};
