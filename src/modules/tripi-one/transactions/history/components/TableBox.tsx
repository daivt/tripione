import { Typography, Button } from '@material-ui/core';
import moment from 'moment';
import * as React from 'react';
import { FormattedMessage, FormattedNumber } from 'react-intl';
import { some } from '../../../../../constants';
import { ITransactionsFilterParams } from '../utils';
import { Row } from '../../../../common/components/elements';
import TableCustom, { Columns } from '../../../../common/components/TableCustom';
import Filter from './Filter';
import { GREY_500, PINK, ORANGE, RED, GREEN } from '../../../../../configs/colors';
import { DATE_TIME_FORMAT_BREAK } from '../../../../../models/moment';
import { PaginationFilter } from '../../../../../models/pagination';

interface Props {
  loading: boolean;
  data?: some;
  filter: ITransactionsFilterParams;
  pagination: PaginationFilter;
  onUpdateFilter(filter: ITransactionsFilterParams): void;
  onUpdatePagination(pagination: PaginationFilter): void;
}

const TableBox: React.FunctionComponent<Props> = props => {
  const { loading, filter, onUpdateFilter, pagination, onUpdatePagination, data } = props;

  const columns = React.useMemo(() => {
    return [
      {
        title: 'bank',
        dataIndex: 'bank',
        styleHeader: { textAlign: 'center' },
        variant: 'caption',
        render: (record: some, index: number) => (
          <Row style={{ justifyContent: 'center' }}>
            <img src={record.bank.logo} alt="Bank logo" style={{ width: 40 }} />
          </Row>
        ),
      },
      {
        title: 'transactions.transferInfo',
        dataIndex: 'transferInfo',
        variant: 'caption',
        render: (record: some, index: number) => (
          <Typography variant="caption">
            <Row>
              <Typography variant="caption" style={{ color: GREY_500 }}>
                {record.bank.fullName}
              </Typography>
            </Row>
            {record.transferInfo.fromAccount.number ? (
              <>
                <Row>
                  <FormattedMessage id="transactions.accountNumber" />
                  :&nbsp;
                  <Typography variant="caption" style={{ color: ORANGE }}>
                    {record.transferInfo.fromAccount.number}
                  </Typography>
                </Row>
                <Row>
                  <FormattedMessage id="transactions.transferMoney" />
                  :&nbsp;
                  <Typography variant="caption" style={{ color: ORANGE }}>
                    <FormattedNumber value={record.transferInfo.amount} />
                    &nbsp;
                    <FormattedMessage id="currency" />
                  </Typography>
                </Row>
                <Row>
                  <FormattedMessage id="transactions.transferContent" />
                  :&nbsp;
                  <Typography variant="caption" style={{ color: ORANGE }}>
                    {record.transferInfo.note}
                  </Typography>
                </Row>
              </>
            ) : (
              <>
                <Row>
                  <FormattedMessage id="transactions.accountNumber" />
                  <Typography variant="caption" style={{ color: ORANGE }}>
                    :&nbsp;
                    {record.transferInfo.toAccount.number}
                  </Typography>
                </Row>
                <Row>
                  <FormattedMessage id="transactions.withdrawMoney" />
                  :&nbsp;
                  <Typography variant="caption" style={{ color: ORANGE }}>
                    <FormattedNumber value={record.transferInfo.amount} />
                    &nbsp;
                    <FormattedMessage id="currency" />
                  </Typography>
                </Row>
                <Row>
                  <FormattedMessage id="transactions.withdrawFee" />
                  :&nbsp;
                  <Typography variant="caption" style={{ color: PINK }}>
                    <FormattedNumber value={record.transferInfo.fee} />
                    &nbsp;
                    <FormattedMessage id="currency" />
                  </Typography>
                </Row>
              </>
            )}
          </Typography>
        ),
      },
      {
        title: 'transactions.type',
        dataIndex: 'isTopup',
        variant: 'caption',
        render: (record: some, index: number) => (
          <Typography variant="caption">
            <FormattedMessage id={record.isTopup ? 'recharge' : 'transactions.withdraw'} />
          </Typography>
        ),
      },
      {
        title: 'transactions.createdDate',
        dataIndex: 'createdDate',
        variant: 'caption',
        render: (record: some, index: number) => (
          <Typography variant="caption" style={{ whiteSpace: 'pre' }}>
            {moment(record.createdDate).isValid()
              ? moment(record.createdDate).format(DATE_TIME_FORMAT_BREAK)
              : null}
          </Typography>
        ),
      },
      {
        title: 'transactions.expiredDate',
        dataIndex: 'expiredDate',
        variant: 'caption',
        render: (record: some, index: number) => (
          <Typography variant="caption" style={{ whiteSpace: 'pre' }}>
            {moment(record.expiredDate).isValid()
              ? moment(record.expiredDate).format(DATE_TIME_FORMAT_BREAK)
              : null}
          </Typography>
        ),
      },
      {
        title: 'transactions.status',
        dataIndex: 'status',
        variant: 'caption',
        render: (record: some, index: number) => (
          <Typography variant="caption" style={{ color: record.status.id === 1 ? GREEN : RED }}>
            {record.status.text}
          </Typography>
        ),
      },
      {
        title: '',
        dataIndex: '',
        variant: 'caption',
        render: (record: some, index: number) =>
          record.status.id === 2 && (
            <Button
              variant="text"
              color="secondary"
              style={{ padding: '7px 22px' }}
              onClick={() => console.log('Delete transaction')}
            >
              <Typography variant="body2">
                <FormattedMessage id="cancelRequest" />
              </Typography>
            </Button>
          ),
      },
    ] as Columns[];
  }, []);

  return (
    <>
      <Filter loading={loading} filter={filter} onUpdateFilter={onUpdateFilter} />
      <TableCustom
        dataSource={data?.itemList}
        loading={loading}
        columns={columns}
        style={{ marginTop: 24 }}
        header={
          <Row style={{ padding: '16px 12px' }}>
            <Typography variant="subtitle2" style={{ fontWeight: 500, marginRight: '24px' }}>
              <FormattedMessage id="transactions.history" />
            </Typography>
          </Row>
        }
        paginationProps={{
          count: data?.totalResults || 0,
          page: pagination.page || 1,
          rowsPerPage: pagination?.pageSize || 0,
          onChangePage: (event: unknown, newPage: number) => {
            onUpdatePagination({ ...pagination, page: newPage });
          },
          onChangeRowsPerPage: (event: React.ChangeEvent<HTMLInputElement>) => {
            onUpdatePagination({
              pageSize: parseInt(event.target.value, 10),
              page: 1,
            });
          },
        }}
      />
    </>
  );
};

export default TableBox;
