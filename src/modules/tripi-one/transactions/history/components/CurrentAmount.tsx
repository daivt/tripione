import { Typography, Button, Paper } from '@material-ui/core';
import * as React from 'react';
import { FormattedMessage, FormattedNumber } from 'react-intl';
import Link from '../../../../common/components/Link';
import { TEAL_50, RED, PURPLE_100, PURPLE } from '../../../../../configs/colors';
import { some } from '../../../../../constants';
import { Col } from '../../../../common/components/elements';
import { ROUTES } from '../../../../../configs/routes';

interface Props {
  data?: some;
}

const CurrentAmount: React.FunctionComponent<Props> = props => {
  const { data } = props;

  return (
    <Paper
      style={{
        display: 'flex',
        flexWrap: 'wrap',
        padding: '10px 16px',
        marginBottom: 24,
        background: TEAL_50,
        whiteSpace: 'nowrap',
      }}
    >
      <Col style={{ display: 'flex', flexDirection: 'column', marginRight: 12 }}>
        <Typography variant="body2">
          <FormattedMessage id="companyName" />:
        </Typography>
        <Typography variant="body2">
          <FormattedMessage id="mainAccount" />:
        </Typography>
      </Col>
      <Col style={{ display: 'flex', flexDirection: 'column', flex: 1 }}>
        <Typography variant="body2">{data?.companyName}</Typography>
        <Typography variant="body2" style={{ fontWeight: 'bold', color: RED }}>
          <FormattedNumber value={data?.amount} />
          &nbsp;
          {data?.currencyCode}
        </Typography>
      </Col>
      <Col style={{ display: 'flex', flexDirection: 'row' }}>
        <Link to={{ pathname: ROUTES.transactions.topup }}>
          <Button
            type="submit"
            variant="contained"
            style={{ minWidth: 144, marginRight: 20 }}
            color="secondary"
            size="large"
            disableElevation
          >
            <FormattedMessage id="recharge" />
          </Button>
        </Link>
        <Link to={{ pathname: ROUTES.transactions.withdraw }}>
          <Button
            type="submit"
            variant="contained"
            style={{ minWidth: 144, color: PURPLE, background: PURPLE_100 }}
            color="secondary"
            size="large"
            disableElevation
          >
            <FormattedMessage id="transactions.withdraw" />
          </Button>
        </Link>
      </Col>
    </Paper>
  );
};

export default CurrentAmount;
