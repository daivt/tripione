import { Button } from '@material-ui/core';
import { useFormik } from 'formik';
import moment from 'moment';
import React from 'react';
import { FormattedMessage, useIntl } from 'react-intl';
import { useSelector, shallowEqual } from 'react-redux';
import { ReactComponent as IconRefresh } from '../../../../../svg/ic_refresh.svg';
import DateField from '../../../../common/components/DateField';
import { Row } from '../../../../common/components/elements';
import LoadingButton from '../../../../common/components/LoadingButton';
import { SingleSelect } from '../../../../common/components/SingleSelect';
import { defaultTransactionsFilterParams, ITransactionsFilterParams } from '../utils';
import { AppState } from '../../../../../redux/reducers';
import { trimObjectValues } from '../../../../utils';

interface Props {
  filter: ITransactionsFilterParams;
  onUpdateFilter(filter: ITransactionsFilterParams): void;
  loading?: boolean;
}

const Filter: React.FunctionComponent<Props> = (props: Props) => {
  const { filter, onUpdateFilter, loading } = props;
  const intl = useIntl();
  const { transactionsTypeData, transactionsStatusData } = useSelector(
    (state: AppState) => state.common,
    shallowEqual,
  );

  const formik = useFormik({
    initialValues: filter,
    onSubmit: values => onUpdateFilter(trimObjectValues(values)),
  });

  React.useEffect(() => {
    formik.setValues(filter);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [filter]);
  return (
    <form onSubmit={formik.handleSubmit} autoComplete="off">
      <Row style={{ flexWrap: 'wrap' }}>
        <DateField
          label={<FormattedMessage id="fromDate" />}
          style={{ maxWidth: 220 }}
          optional
          date={formik.values.fromDate ? moment(formik.values.fromDate) : undefined}
          onChange={value => {
            formik.setFieldValue('fromDate', value?.valueOf());
          }}
          isOutsideRange={(e: any) => {
            if (formik.values.toDate) {
              return (
                moment(formik.values.toDate)
                  .add(1, 'day')
                  .startOf('day')
                  .isSameOrBefore(e) ||
                moment()
                  .subtract(6, 'months')
                  .startOf('day')
                  .isSameOrAfter(e)
              );
            }
            return (
              moment()
                .add(1, 'day')
                .startOf('day')
                .isSameOrBefore(e) ||
              moment()
                .subtract(6, 'months')
                .startOf('day')
                .isSameOrAfter(e)
            );
          }}
        />
        <DateField
          label={<FormattedMessage id="toDate" />}
          style={{ maxWidth: 220 }}
          optional
          date={formik.values.toDate ? moment(formik.values.toDate) : undefined}
          onChange={value => {
            formik.setFieldValue('toDate', value?.valueOf());
            if (
              value &&
              formik.values.fromDate &&
              value.startOf('day').isBefore(moment(formik.values.fromDate).startOf('day'))
            ) {
              formik.setFieldValue('fromDate', undefined);
            }
          }}
        />
        <SingleSelect
          value={formik.values?.transactionsType || undefined}
          label={<FormattedMessage id="transactions.type" />}
          formControlStyle={{ width: 220 }}
          onSelectOption={(value: any) => {
            formik.setFieldValue('transactionsType', value);
          }}
          getOptionLabel={value => intl.formatMessage({ id: value.name })}
          options={[{ id: undefined, name: 'all' }, ...transactionsTypeData]}
          optional
        />
        <SingleSelect
          value={formik.values?.status || undefined}
          label={<FormattedMessage id="status" />}
          formControlStyle={{ width: 220 }}
          onSelectOption={(value: any) => {
            formik.setFieldValue('status', value);
          }}
          getOptionLabel={value => intl.formatMessage({ id: value.name })}
          options={[{ id: undefined, name: 'all' }, ...transactionsStatusData]}
          optional
        />
        <Row style={{ marginTop: 4 }}>
          <LoadingButton
            type="submit"
            loading={loading}
            variant="contained"
            style={{ minWidth: 160, marginRight: 16 }}
            color="secondary"
            size="large"
            disableElevation
          >
            <FormattedMessage id="search" />
          </LoadingButton>
          <Button
            size="large"
            style={{ minWidth: 0 }}
            onClick={() => {
              formik.setValues(defaultTransactionsFilterParams);
              onUpdateFilter(defaultTransactionsFilterParams);
            }}
          >
            <IconRefresh />
          </Button>
        </Row>
      </Row>
    </form>
  );
};

export default Filter;
