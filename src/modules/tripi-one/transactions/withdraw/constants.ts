export const fakeTransactionsTopupBanks = {
  code: "SUCCESS",
  message: "success",
  data: {
    withdrawalFeePercent: 3,
    itemList: [
      {
        id: 1,
        shortName: "Techcombank",
        fullName: "Ngân hàng Kĩ Thương Việt Nam",
        logo: "https://storage.googleapis.com/tripi-assets/images/payment-method-banks/techcombank_logo.png"
      },
      {
        id: 2,
        shortName: "Vietcombank",
        fullName: "Ngân hàng TMCP Ngoại thương Việt Nam",
        logo: "https://storage.googleapis.com/tripi-assets/images/payment-method-banks/vietcombank_logo.png"
      }
    ]
  }
}
