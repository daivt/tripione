import { debounce } from 'lodash';
import * as React from 'react';
import { some } from '../../../../../constants';
import LoadingIcon from '../../../../common/components/LoadingIcon';
import TransactionsWithdrawForm from '../components/TransactionsWithdrawForm';
import { fakeTransactionsTopupBanks } from '../constants';

interface Props {}

const TransactionsWithdraw = (props: Props) => {
  const [data, setData] = React.useState<some | undefined>();
  const [loading, setLoading] = React.useState(true);

  const fetchData = React.useCallback(
    debounce(
      async () => {
        setLoading(true);
        setData(fakeTransactionsTopupBanks.data);
        setLoading(false);
      },
      200,
      {
        trailing: true,
        leading: false,
      },
    ),
    [],
  );

  React.useEffect(() => {
    fetchData();
  }, [fetchData]);

  return data ? <TransactionsWithdrawForm data={data} /> : <LoadingIcon style={{ height: 320 }} />;
};

export default TransactionsWithdraw;
