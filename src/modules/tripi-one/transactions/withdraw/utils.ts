
export interface ITransactionsWithdraw {
  bankId: number | null;
  amount: number | null;
  toAccount: {
    name: string;
    number: string;
  };
}

export const TransactionWithdrawDefault: ITransactionsWithdraw = {
  bankId: null,
  amount: null,
  toAccount: { name:'', number:''} ,
}
