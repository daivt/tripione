import { Button, Typography } from '@material-ui/core';
import { useFormik } from 'formik';
import * as React from 'react';
import { FormattedMessage, useIntl, FormattedNumber } from 'react-intl';
import * as yup from 'yup';
import ReportProblemOutlinedIcon from '@material-ui/icons/ReportProblemOutlined';
import { ORANGE, WHITE, RED } from '../../../../../configs/colors';
import { ROUTES } from '../../../../../configs/routes';
import { some } from '../../../../../constants';
import { Row, Col } from '../../../../common/components/elements';
import { NumberFormatCustom } from '../../../../common/components/Form';
import FormControlTextField from '../../../../common/components/FormControlTextField';
import InputAdornmentSolid from '../../../../common/components/InputAdornmentSolid';
import Link from '../../../../common/components/Link';
import AmountPicker from '../../components/AmountPicker';
import BankPicker from '../../components/BankPicker';
import { TransactionWithdrawDefault } from '../utils';

interface Props {
  data: some;
}

const TransactionsWithdrawForm = (props: Props) => {
  const { data } = props;
  const intl = useIntl();
  const transactionsWithdrawSchema = yup.object().shape({
    bankId: yup
      .number()
      .nullable()
      .required(intl.formatMessage({ id: 'required' })),
    amount: yup
      .number()
      .nullable()
      .required(intl.formatMessage({ id: 'required' })),
    name: yup.string().required(intl.formatMessage({ id: 'required' })),
    number: yup.string().required(intl.formatMessage({ id: 'required' })),
    toAccount: yup
      .object()
      .shape({
        name: yup
          .string()
          .trim()
          .required(intl.formatMessage({ id: 'required' })),
        number: yup
          .string()
          .trim()
          .required(intl.formatMessage({ id: 'required' })),
      })
      .required(intl.formatMessage({ id: 'required' })),
  });

  const formik = useFormik({
    initialValues: TransactionWithdrawDefault,
    onSubmit: values => {
      // dispatch(goToAction({ pathname: 'ROUTES.transactions.topupInstruction', state: { topupRequestData: JSON.data} }))
    },
    validationSchema: transactionsWithdrawSchema,
  });

  return (
    <form onSubmit={formik.handleSubmit} autoComplete="off">
      <BankPicker
        bankId={formik.values.bankId || undefined}
        options={data.itemList}
        onChange={value => formik.setFieldValue('bankId', Number(value))}
        error={formik.errors.bankId}
      />
      <Row>
        <FormControlTextField
          id="toAccount"
          label={<FormattedMessage id="transactions.accountNumber" />}
          formControlStyle={{ width: 250 }}
          placeholder={intl.formatMessage({ id: 'transactions.insertAccountNumber' })}
          value={formik.values.toAccount.name || ''}
          onChange={e =>
            formik.setFieldValue('toAccount', { ...formik.values.toAccount, name: e.target.value })
          }
          inputProps={{
            maxLength: 100,
          }}
          errorMessage={
            formik.errors.toAccount?.name && formik.touched.toAccount?.name
              ? formik.errors.toAccount?.name
              : undefined
          }
        />
        <FormControlTextField
          id="toAccount"
          label={<FormattedMessage id="transactions.accountHolder" />}
          formControlStyle={{ width: 250 }}
          placeholder={intl.formatMessage({ id: 'transactions.insertAccountHolder' })}
          value={formik.values.toAccount.number || ''}
          onChange={e =>
            formik.setFieldValue('toAccount', {
              ...formik.values.toAccount,
              number: e.target.value,
            })
          }
          inputProps={{
            maxLength: 100,
          }}
          errorMessage={
            formik.errors.toAccount?.number && formik.touched.toAccount?.number
              ? formik.errors.toAccount?.number
              : undefined
          }
        />
      </Row>
      <Row style={{ flexWrap: 'wrap' }}>
        <FormControlTextField
          id="amount"
          label={<FormattedMessage id="addMoney" />}
          formControlStyle={{ width: 250 }}
          placeholder={intl.formatMessage({ id: 'addMoney' })}
          value={formik.values.amount || ''}
          onChange={e => formik.setFieldValue('amount', Number(e.target.value))}
          inputProps={{
            maxLength: 100,
          }}
          inputComponent={NumberFormatCustom as any}
          endAdornment={
            <InputAdornmentSolid>
              <FormattedMessage id="currency" />
            </InputAdornmentSolid>
          }
          errorMessage={
            formik.errors.amount && formik.touched.amount ? formik.errors.amount : undefined
          }
        />
        <AmountPicker onChange={value => formik.setFieldValue('amount', Number(value))} />
      </Row>
      {!!formik.values.amount && data && (
        <Row style={{ marginBottom: 16 }}>
          <Typography variant="body2">
            <FormattedMessage id="transactions.withdrawFee" />: &nbsp;
            <Typography variant="caption" style={{ color: ORANGE }}>
              <FormattedNumber
                value={Math.round((formik.values.amount * data.withdrawalFeePercent) / 100)}
              />
              &nbsp;
              <FormattedMessage id="currency" />
            </Typography>
          </Typography>
        </Row>
      )}
      <Row>
        <ReportProblemOutlinedIcon style={{ color: RED, marginRight: 8 }} />
        <Typography variant="body2" style={{ color: RED }}>
          <FormattedMessage id="warning" />:
        </Typography>
      </Row>
      <Col style={{ padding: '12px 0 24px' }}>
        <Typography variant="body2" style={{ color: RED, marginBottom: 8 }}>
          <FormattedMessage id="transactions.withdraw.warning1" />
        </Typography>
        <Typography variant="body2">
          <FormattedMessage id="transactions.withdraw.warning2" />
        </Typography>
      </Col>
      <Row>
        <Button
          type="submit"
          variant="contained"
          style={{ minWidth: 144, marginRight: 20 }}
          color="secondary"
          size="large"
          disableElevation
        >
          <FormattedMessage id="continue" />
        </Button>
        <Link to={ROUTES.transactions.history}>
          <Button
            type="submit"
            variant="outlined"
            style={{ minWidth: 144, background: WHITE }}
            size="large"
            disableElevation
          >
            <FormattedMessage id="reject" />
          </Button>
        </Link>
      </Row>
    </form>
  );
};

export default TransactionsWithdrawForm;
