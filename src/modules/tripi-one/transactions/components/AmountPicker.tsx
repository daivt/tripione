import { Button, InputLabel } from '@material-ui/core';
import * as React from 'react';
import { FormattedMessage, FormattedNumber } from 'react-intl';
import { PURPLE, PURPLE_50 } from '../../../../configs/colors';
import { Row } from '../../../common/components/elements';

const amountPickerData = [10000000, 50000000, 100000000, 200000000];

interface Props {
  onChange: (value: number) => void;
}

const AmountPicker = (props: Props) => {
  const { onChange } = props;
  return (
    <Row style={{ flexDirection: 'column', alignItems: 'start', marginBottom: 18 }}>
      <InputLabel shrink htmlFor="transactions.amountPicker">
        <FormattedMessage id="transactions.amountPicker" /> &nbsp;
      </InputLabel>
      {amountPickerData.length && (
        <Row id="transactions.amountPicker">
          {amountPickerData.map((value: number, id: number) => {
            return (
              <Button
                key={id}
                variant="contained"
                style={{ minWidth: 144, marginRight: 12, color: PURPLE, background: PURPLE_50 }}
                color="secondary"
                size="large"
                disableElevation
                value={value}
                onClick={() => onChange(value)}
              >
                <FormattedNumber value={value} />
                &nbsp;
                <FormattedMessage id="currency" />
              </Button>
            );
          })}
        </Row>
      )}
    </Row>
  );
};

export default AmountPicker;
