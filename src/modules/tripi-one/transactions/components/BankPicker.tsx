import { Button, InputLabel, Typography } from '@material-ui/core';
import * as React from 'react';
import { FormattedMessage } from 'react-intl';
import CheckCircleIcon from '@material-ui/icons/CheckCircle';
import { some } from '../../../../constants';
import { Row } from '../../../common/components/elements';
import { WHITE, GREY_300, GREEN_50, GREEN, RED } from '../../../../configs/colors';
import { redMark } from '../../../common/components/Form';

interface Props {
  options: some[];
  bankId?: number;
  error?: string;
  onChange: (value: number) => void;
}

const BankPicker = (props: Props) => {
  const { options, bankId, onChange, error } = props;
  return (
    <Row style={{ flexDirection: 'column', alignItems: 'start' }}>
      <InputLabel
        shrink
        htmlFor="transactions.bankPicker"
        style={{ color: error ? RED : 'inherit' }}
      >
        <FormattedMessage id="transactions.bankPicker" /> &nbsp;
        {redMark}
      </InputLabel>
      {options.length && (
        <Row id="transactions.bankPicker" style={{ maxWidth: 810, flexWrap: 'wrap' }}>
          {options.map((obj: some, id: number) => {
            const isChecked = obj.id === bankId;
            return (
              <Button
                key={id}
                variant="outlined"
                color="primary"
                style={{
                  position: 'relative',
                  minWidth: 140,
                  height: 80,
                  margin: '0 20px 12px 0',
                  padding: 0,
                  borderRadius: 8,
                  borderColor: isChecked ? GREEN : GREY_300,
                  background: isChecked ? GREEN_50 : WHITE,
                }}
                size="large"
                disableElevation
                value={obj.id}
                onClick={() => {
                  onChange(obj.id);
                }}
              >
                <img src={obj.logo} alt="Bank logo" style={{ width: 140 }} />
                {isChecked && (
                  <CheckCircleIcon
                    style={{ position: 'absolute', top: 1, right: 1, color: GREEN }}
                  />
                )}
              </Button>
            );
          })}
        </Row>
      )}
      <div style={{ position: 'relative', top: -10, minHeight: 20 }}>
        {error && (
          <Typography variant="body2" style={{ color: RED }}>
            {error}
          </Typography>
        )}
      </div>
    </Row>
  );
};

export default BankPicker;
