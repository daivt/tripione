import { GREEN, GREY_700, PINK, RED } from '../../../configs/colors';
import { some } from '../../../constants';

export interface AdminApprovalFilter {
  id?: number;
  name: string;
  email: string;
  referrerPhone: string;
  representative: string;
  representativePhone: string;
  status?: some;
}
export const defaultAdminApprovalFilter: AdminApprovalFilter = {
  name: '',
  representative: '',
  email: '',
  referrerPhone: '',
  representativePhone: '',
};
export interface ApprovalAccountInfo {
  id?: number;
  name: string;
  status?: number;
  companySizeRange: some | undefined;
  taxCode: string;
  email?: string;
  referrerPhone: string;
  representative: string;
  representativePhone: string;
  reason: string;
  address: string;
  contractStartDate: number | undefined;
  contractEndDate: number | undefined;
  approval: boolean | undefined;
}
export const defaultApprovalAccountInfo: ApprovalAccountInfo = {
  name: '',
  email: '',
  address: '',
  companySizeRange: undefined,
  taxCode: '',
  referrerPhone: '',
  representative: '',
  representativePhone: '',
  reason: '',
  contractStartDate: undefined,
  contractEndDate: undefined,
  approval: undefined,
};
export interface NewApproval {
  id?: number;
  taxCode: string;
  name: string;
  companySizeRange: some | null;
  referrerPhone: string;
  representative: string;
  email: string;
  representativePhone: string;
  address: string;
  password: string;
  contractStartDate: number | undefined;
  contractEndDate: number | undefined;
}
export const defaultNewApproval: NewApproval = {
  name: '',
  taxCode: '',
  address: '',
  password: '',
  companySizeRange: null,
  representative: '',
  email: '',
  referrerPhone: '',
  representativePhone: '',
  contractStartDate: undefined,
  contractEndDate: undefined,
};

export function getStatusColorLicense(status: number) {
  if (status === 1) {
    return GREEN;
  }
  if (status === 2) {
    return PINK;
  }
  if (status === 3) {
    return RED;
  }
  return GREY_700;
}
