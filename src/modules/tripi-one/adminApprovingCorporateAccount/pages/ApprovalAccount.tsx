import { useSnackbar } from 'notistack';
import React, { useCallback, useMemo, useState } from 'react';
import { useIntl } from 'react-intl';
import { shallowEqual, useDispatch, useSelector } from 'react-redux';
import { Action } from 'redux';
import { ThunkDispatch } from 'redux-thunk';
import { API_PATHS } from '../../../../configs/API';
import { some, SUCCESS_CODE } from '../../../../constants';
import { defaultPaginationFilter, PaginationFilter } from '../../../../models/pagination';
import { AppState } from '../../../../redux/reducers';
import { snackbarSetting } from '../../../common/components/elements';
import RedirectDiv from '../../../common/components/RedirectDiv';
import { fetchThunk } from '../../../common/redux/thunk';
import DetailTable from '../components/DetailTable';
import { ApprovalAccountInfo } from '../utils';

interface Props {}

const ApprovalAccount: React.FC<Props> = props => {
  const dispatch = useDispatch<ThunkDispatch<AppState, null, Action<string>>>();
  const router = useSelector((state: AppState) => state.router, shallowEqual);
  const [pagination, setPagination] = React.useState<PaginationFilter>(defaultPaginationFilter);
  const [historyData, setHistoryData] = React.useState<some>();

  const getActionData = useMemo(() => {
    const state = router.location.state as some;
    const data = state && (state.approvalAccountData as ApprovalAccountInfo);
    return data;
  }, [router.location.state]);

  const [loading, setLoading] = useState(false);
  const { enqueueSnackbar, closeSnackbar } = useSnackbar();
  const intl = useIntl();

  const [info, setInfo] = React.useState<ApprovalAccountInfo>(getActionData);

  const fetchHistoryData = React.useCallback(
    async (paginationParams: PaginationFilter) => {
      setLoading(true);
      const json = await dispatch(
        fetchThunk(
          API_PATHS.getApprovalHistory,
          'post',
          JSON.stringify({
            filters: {
              companyId: getActionData.id,
              events: [1, 2],
            },
            ...paginationParams,
          }),
        ),
      );
      if (json?.code === SUCCESS_CODE) {
        setHistoryData(json.data);
      }
      setLoading(false);
    },
    // eslint-disable-next-line react-hooks/exhaustive-deps
    [dispatch],
  );

  const onUpdate = useCallback(
    async (data: ApprovalAccountInfo) => {
      setLoading(true);
      if (!data.approval) {
        const json = await dispatch(
          fetchThunk(
            API_PATHS.licenseReject,
            'put',
            JSON.stringify({
              companyId: data.id,
              reason: data.reason,
            }),
          ),
        );
        if (json?.code === SUCCESS_CODE) {
          setInfo(json?.data);
          enqueueSnackbar(
            intl.formatMessage({ id: 'admin.approval.refuseSuccess' }),
            snackbarSetting(key => closeSnackbar(key), { color: 'success' }),
          );
        } else {
          enqueueSnackbar(
            json.message,
            snackbarSetting(key => closeSnackbar(key), { color: 'error' }),
          );
        }
      } else if (data.approval) {
        const json = await dispatch(
          fetchThunk(
            API_PATHS.licenseAccept,
            'put',
            JSON.stringify({
              ...data,
              fromDate: data.contractStartDate,
              toDate: data.contractEndDate,
            }),
          ),
        );
        if (json?.code === SUCCESS_CODE) {
          setInfo(json?.data);
          enqueueSnackbar(
            intl.formatMessage({ id: 'admin.approval.approveSuccess' }),
            snackbarSetting(key => closeSnackbar(key), { color: 'success' }),
          );
        } else {
          enqueueSnackbar(
            json.message,
            snackbarSetting(key => closeSnackbar(key), { color: 'error' }),
          );
        }
      }
      setLoading(false);
    },

    [closeSnackbar, dispatch, enqueueSnackbar, intl],
  );

  React.useEffect(() => {
    fetchHistoryData(pagination);
  }, [fetchHistoryData, pagination, info]);

  if (!getActionData) {
    return <RedirectDiv />;
  }
  return (
    <DetailTable
      loading={loading}
      info={info}
      setInfo={onUpdate}
      historyData={historyData}
      pagination={pagination}
      onUpdatePagination={values => setPagination(values)}
    />
  );
};

export default ApprovalAccount;
