import { debounce } from 'lodash';
import queryString from 'query-string';
import React from 'react';
import { useDispatch } from 'react-redux';
import { useLocation } from 'react-router';
import { Action } from 'redux';
import { ThunkDispatch } from 'redux-thunk';
import { API_PATHS } from '../../../../configs/API';
import { some, SUCCESS_CODE } from '../../../../constants';
import { defaultPaginationFilter, PaginationFilter } from '../../../../models/pagination';
import { AppState } from '../../../../redux/reducers';
import { goToReplace } from '../../../common/redux/reducer';
import { fetchThunk } from '../../../common/redux/thunk';
import TableBox from '../components/TableBox';
import { AdminApprovalFilter, defaultAdminApprovalFilter } from '../utils';

interface Props {}
const ApprovingCorporateAccountAdmin: React.FC<Props> = props => {
  const dispatch = useDispatch<ThunkDispatch<AppState, null, Action<string>>>();
  const [data, setData] = React.useState<some | undefined>(undefined);
  const [filter, setFilter] = React.useState<AdminApprovalFilter>(defaultAdminApprovalFilter);
  const [pagination, setPagination] = React.useState<PaginationFilter>(defaultPaginationFilter);
  const [loading, setLoading] = React.useState(true);
  const location = useLocation();

  const updateQueryParams = React.useCallback(() => {
    if (location.search) {
      const filterParams = queryString.parse(location.search) as any;
      const filterTmp = queryString.parse(filterParams.filters) as any;
      setFilter({
        ...filterTmp,
        status: filterParams.status && parseInt(`${filterParams.status}`, 10),
      });
      const paginationTmp = {
        page: filterParams.page ? parseInt(`${filterParams.page}`, 10) : 1,
        pageSize: filterParams.pageSize ? parseInt(`${filterParams.pageSize}`, 10) : 10,
      } as PaginationFilter;
      setPagination(paginationTmp);
    } else {
      dispatch(
        goToReplace({
          search: queryString.stringify({
            filters: defaultAdminApprovalFilter,
            ...defaultPaginationFilter,
          }),
        }),
      );
    }
  }, [dispatch, location.search]);

  const fetchData = React.useCallback(
    debounce(
      async (filterParams: AdminApprovalFilter, paginationParams: PaginationFilter) => {
        setLoading(true);
        const json = await dispatch(
          fetchThunk(
            API_PATHS.getLicenseApproval,
            'post',
            JSON.stringify({
              ...paginationParams,
              filters: filterParams,
            }),
          ),
        );
        if (json?.code === SUCCESS_CODE) {
          setData(json.data);
        }
        setLoading(false);
      },
      200,
      {
        trailing: true,
        leading: false,
      },
    ),
    [],
  );

  React.useEffect(() => {
    updateQueryParams();
  }, [updateQueryParams]);

  React.useEffect(() => {
    fetchData(filter, pagination);
  }, [fetchData, filter, pagination]);

  return (
    <TableBox
      loading={loading}
      data={data}
      filter={filter}
      onUpdateFilter={values => {
        dispatch(
          goToReplace({
            search: queryString.stringify({
              ...pagination,
              page: 1,
              filters: queryString.stringify(values),
              status: JSON.stringify(values.status),
            }),
          }),
        );
      }}
      pagination={pagination}
      onUpdatePagination={values => {
        dispatch(
          goToReplace({
            search: queryString.stringify({ ...values, filters: queryString.stringify(filter) }),
          }),
        );
      }}
    />
  );
};

export default ApprovingCorporateAccountAdmin;
