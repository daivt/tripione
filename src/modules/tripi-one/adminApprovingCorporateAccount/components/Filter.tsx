import { Button } from '@material-ui/core';
import { useFormik } from 'formik';
import React from 'react';
import { FormattedMessage, useIntl } from 'react-intl';
import { shallowEqual, useSelector } from 'react-redux';
import { AppState } from '../../../../redux/reducers';
import { ReactComponent as RefreshIcon } from '../../../../svg/ic_refresh.svg';
import { Row } from '../../../common/components/elements';
import FormControlTextField from '../../../common/components/FormControlTextField';
import LoadingButton from '../../../common/components/LoadingButton';
import SingleSelect from '../../../common/components/SingleSelect';
import { AdminApprovalFilter, defaultAdminApprovalFilter } from '../utils';
import { trimObjectValues } from '../../../utils';

interface Props {
  filter: AdminApprovalFilter;
  loading: boolean;
  onUpdateFilter(filter: AdminApprovalFilter): void;
}

const Filter: React.FC<Props> = props => {
  const status = useSelector((state: AppState) => state.common.approvalStatusOptions, shallowEqual);

  const { filter, loading, onUpdateFilter } = props;
  const intl = useIntl();
  const formik = useFormik({
    initialValues: filter,
    onSubmit: values => {
      onUpdateFilter(trimObjectValues(values));
    },
  });

  React.useEffect(() => {
    formik.setValues(filter);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [filter]);

  return (
    <form onSubmit={formik.handleSubmit} autoComplete="off">
      <Row style={{ flexWrap: 'wrap', maxWidth: 1200 }}>
        <FormControlTextField
          id="name"
          label={<FormattedMessage id="admin.approval.companyName" />}
          placeholder={intl.formatMessage({ id: 'admin.approval.enterCompanyName' })}
          value={formik.values.name}
          formControlStyle={{ width: 470 }}
          onChange={formik.handleChange}
          optional
          inputProps={{
            maxLength: '100',
          }}
        />
        <FormControlTextField
          id="representative"
          label={<FormattedMessage id="admin.approval.contactPersonName" />}
          placeholder={intl.formatMessage({ id: 'admin.approval.enterContactPersonName' })}
          value={formik.values.representative}
          formControlStyle={{ width: 470 }}
          onChange={formik.handleChange}
          optional
          inputProps={{
            maxLength: '100',
          }}
        />
        <FormControlTextField
          id="email"
          label={<FormattedMessage id="email" />}
          placeholder={intl.formatMessage({ id: 'enterEmail' })}
          value={formik.values.email}
          formControlStyle={{ width: 220 }}
          onChange={formik.handleChange}
          optional
          inputProps={{
            maxLength: '100',
          }}
        />
        <FormControlTextField
          id="referrerPhone"
          label={<FormattedMessage id="phoneNumber" />}
          placeholder={intl.formatMessage({ id: 'enterPhoneNumber' })}
          value={formik.values.referrerPhone}
          formControlStyle={{ width: 220 }}
          onChange={formik.handleChange}
          optional
          inputProps={{
            maxLength: '100',
          }}
        />
        <FormControlTextField
          id="representativePhone"
          label={<FormattedMessage id="admin.approval.referenceContactPhone" />}
          placeholder={intl.formatMessage({ id: 'admin.approval.enterReferenceContactPhone' })}
          value={formik.values.representativePhone}
          formControlStyle={{ width: 220 }}
          onChange={formik.handleChange}
          optional
          inputProps={{
            maxLength: '100',
          }}
        />
        <SingleSelect
          value={formik.values.status}
          label={<FormattedMessage id="status" />}
          formControlStyle={{ width: 220 }}
          onSelectOption={(value: any) => {
            formik.setFieldValue('status', value);
          }}
          getOptionLabel={value => intl.formatMessage({ id: value.name })}
          options={status}
          optional
        />
        <Row>
          <LoadingButton
            loading={loading}
            type="submit"
            variant="contained"
            size="large"
            style={{ minWidth: '140px', marginRight: '20px' }}
            color="secondary"
            disableElevation
          >
            <FormattedMessage id="search" />
          </LoadingButton>
          <Button
            onClick={() => {
              formik.setValues(defaultAdminApprovalFilter);
              onUpdateFilter(defaultAdminApprovalFilter);
            }}
          >
            <RefreshIcon />
          </Button>
        </Row>
      </Row>
    </form>
  );
};

export default Filter;
