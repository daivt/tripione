import { DialogActions, Divider, Typography } from '@material-ui/core';
import { useFormik } from 'formik';
import moment, { Moment } from 'moment';
import { useSnackbar } from 'notistack';
import React from 'react';
import { FormattedMessage, useIntl } from 'react-intl';
import { useDispatch } from 'react-redux';
import { Action } from 'redux';
import { ThunkDispatch } from 'redux-thunk';
import * as yup from 'yup';
import { API_PATHS } from '../../../../configs/API';
import { some, SUCCESS_CODE } from '../../../../constants';
import { AppState } from '../../../../redux/reducers';
import DateRangeFormControl from '../../../common/components/DateRangeFormControl';
import DialogCustom from '../../../common/components/DialogCustom';
import { Col, Row, snackbarSetting } from '../../../common/components/elements';
import { NumberFormatCustom2 } from '../../../common/components/Form';
import FormControlAutoComplete from '../../../common/components/FormControlAutoComplete';
import FormControlTextField from '../../../common/components/FormControlTextField';
import LoadingButton from '../../../common/components/LoadingButton';
import { generalCompanySize } from '../../../common/redux/constants';
import { fetchThunk } from '../../../common/redux/thunk';
import { defaultNewApproval, NewApproval } from '../utils';

interface Props {
  open: boolean;
  onClose: () => void;
}

const NewApprovalAccountDialog: React.FC<Props> = props => {
  const dispatch = useDispatch<ThunkDispatch<AppState, null, Action<string>>>();
  const { open, onClose } = props;
  const [loading, setLoading] = React.useState(false);
  const { enqueueSnackbar, closeSnackbar } = useSnackbar();
  const intl = useIntl();
  const storeSchema = yup.object().shape({
    name: yup
      .string()
      .trim()
      .required(intl.formatMessage({ id: 'required' })),
    representative: yup
      .string()
      .trim()
      .required(intl.formatMessage({ id: 'required' })),
    taxId: yup.string().trim(),
    email: yup
      .string()
      .trim()
      .email(intl.formatMessage({ id: 'emailInvalid' }))
      .required(intl.formatMessage({ id: 'required' })),
    address: yup.string().trim(),
    referrerPhone: yup
      .string()
      .trim()
      .required(intl.formatMessage({ id: 'required' })),
    companySizeRange: yup
      .object()
      .nullable()
      .required(intl.formatMessage({ id: 'required' })),
    representativePhone: yup.string().trim(),
    password: yup
      .string()
      .trim()
      .required(intl.formatMessage({ id: 'required' })),
  });

  const onApprove = React.useCallback(
    async (values: NewApproval) => {
      setLoading(true);
      const json = await dispatch(
        fetchThunk(API_PATHS.updateCorporate, 'post', JSON.stringify(values)),
      );
      if (json?.code === SUCCESS_CODE) {
        onClose();
        enqueueSnackbar(
          json.message,
          snackbarSetting(key => closeSnackbar(key), {}),
        );
      } else {
        enqueueSnackbar(
          json.message,
          snackbarSetting(key => closeSnackbar(key), {
            color: 'error',
          }),
        );
      }
      setLoading(false);
      return json;
    },
    [closeSnackbar, dispatch, enqueueSnackbar, onClose],
  );

  const formik = useFormik({
    initialValues: defaultNewApproval,
    onSubmit: values => {
      onApprove(values);
    },
    validationSchema: storeSchema,
  });

  React.useEffect(() => {
    formik.resetForm();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [open]);

  return (
    <DialogCustom
      open={open}
      onClose={onClose}
      PaperProps={{ style: { width: 720 } }}
      titleLabel={
        <Typography variant="subtitle1">
          <FormattedMessage id="company" />
        </Typography>
      }
      footerContent={<></>}
    >
      <form onSubmit={formik.handleSubmit}>
        <Col style={{ padding: '24px' }}>
          <Row>
            <FormControlTextField
              id="name"
              label={<FormattedMessage id="companyName" />}
              placeholder={intl.formatMessage({ id: 'enterCompanyName' })}
              inputProps={{
                maxLength: '100',
              }}
              formControlStyle={{ flex: 1 }}
              value={formik.values.name}
              onChange={formik.handleChange}
              errorMessage={
                formik.errors.name && formik.submitCount > 0 ? formik.errors.name : undefined
              }
            />
            <FormControlTextField
              id="representative"
              label={<FormattedMessage id="admin.approval.contactPersonName" />}
              placeholder={intl.formatMessage({ id: 'admin.approval.enterContactPersonName' })}
              inputProps={{
                maxLength: '225',
              }}
              formControlStyle={{ flex: 1, margin: 0 }}
              value={formik.values.representative}
              onChange={formik.handleChange}
              errorMessage={
                formik.errors.representative && formik.submitCount > 0
                  ? formik.errors.representative
                  : undefined
              }
            />
          </Row>
          <Row>
            <FormControlTextField
              id="taxCode"
              optional
              label={<FormattedMessage id="taxCode" />}
              placeholder={intl.formatMessage({ id: 'enterTaxCode' })}
              inputProps={{
                maxLength: '100',
              }}
              formControlStyle={{ flex: 1 }}
              value={formik.values.taxCode}
              onChange={formik.handleChange}
              errorMessage={
                formik.errors.taxCode && formik.submitCount > 0 ? formik.errors.taxCode : undefined
              }
            />
            <FormControlTextField
              id="email"
              label={<FormattedMessage id="email" />}
              placeholder={intl.formatMessage({ id: 'enterEmail' })}
              inputProps={{
                maxLength: '50',
              }}
              formControlStyle={{ flex: 1, margin: 0 }}
              value={formik.values.email}
              onChange={formik.handleChange}
              errorMessage={
                formik.errors.email && formik.submitCount > 0 ? formik.errors.email : undefined
              }
            />
          </Row>
          <Row>
            <FormControlTextField
              id="address"
              optional
              label={<FormattedMessage id="address" />}
              placeholder={intl.formatMessage({ id: 'enterAddress' })}
              inputProps={{
                maxLength: '255',
              }}
              formControlStyle={{ flex: 1 }}
              value={formik.values.address}
              onChange={formik.handleChange}
              errorMessage={
                formik.errors.address && formik.submitCount > 0 ? formik.errors.address : undefined
              }
            />
            <FormControlTextField
              name="referrerPhone"
              label={<FormattedMessage id="phoneNumber" />}
              placeholder={intl.formatMessage({ id: 'enterPhoneNumber' })}
              inputProps={{
                maxLength: '15',
              }}
              formControlStyle={{ flex: 1, margin: 0 }}
              value={formik.values.referrerPhone}
              onChange={formik.handleChange}
              inputComponent={NumberFormatCustom2 as any}
              errorMessage={
                formik.errors.referrerPhone && formik.submitCount > 0
                  ? formik.errors.referrerPhone
                  : undefined
              }
            />
          </Row>
          <Row>
            <FormControlAutoComplete
              readOnly
              value={formik.values.companySizeRange}
              label={<FormattedMessage id="companySize" />}
              placeholder={intl.formatMessage({ id: 'chooseCompanySize' })}
              getOptionSelected={(option, value) => {
                return option.from === value.from;
              }}
              formControlStyle={{ flex: 1 }}
              onChange={(e: any, value: some | null) => {
                formik.setFieldValue('companySizeRange', value);
              }}
              getOptionLabel={value =>
                intl.formatMessage(
                  {
                    id: 'companySize.value',
                  },
                  {
                    from: value.from,
                    to: value.to,
                  },
                )
              }
              options={generalCompanySize}
              errorMessage={
                formik.errors.companySizeRange && formik.submitCount > 0
                  ? formik.errors.companySizeRange
                  : undefined
              }
            />
            <FormControlTextField
              name="representativePhone"
              optional
              label={intl.formatMessage({ id: 'admin.approval.referenceContactPhone' })}
              placeholder={intl.formatMessage({ id: 'enterPhoneNumber' })}
              inputProps={{
                maxLength: '15',
              }}
              formControlStyle={{ flex: 1, margin: 0 }}
              value={formik.values.representativePhone}
              onChange={formik.handleChange}
              inputComponent={NumberFormatCustom2 as any}
              errorMessage={
                formik.errors.representativePhone && formik.submitCount > 0
                  ? formik.errors.representativePhone
                  : undefined
              }
            />
          </Row>
          <Row>
            <DateRangeFormControl
              style={{ minWidth: 325 }}
              label={intl.formatMessage({ id: 'admin.corporate.contractTime' })}
              optional
              startDate={
                formik.values.contractStartDate
                  ? moment(formik.values.contractStartDate)
                  : undefined
              }
              endDate={
                formik.values.contractEndDate ? moment(formik.values.contractEndDate) : undefined
              }
              onChange={(start?: Moment, end?: Moment) => {
                formik.setFieldValue('contractStartDate', start ? start.valueOf() : undefined);
                start && formik.setFieldValue('contractEndDate', end ? end.valueOf() : undefined);
              }}
              isOutsideRange={(e: any) => false}
              numberOfMonths={1}
            />
            <FormControlTextField
              id="password"
              label={<FormattedMessage id="password" />}
              placeholder={intl.formatMessage({ id: 'enterPassword' })}
              inputProps={{
                maxLength: '100',
              }}
              formControlStyle={{ flex: '0 0 321px' }}
              value={formik.values.password}
              onChange={formik.handleChange}
              errorMessage={
                formik.errors.password && formik.submitCount > 0
                  ? formik.errors.password
                  : undefined
              }
            />
          </Row>
        </Col>
        <Divider />
        <DialogActions style={{ padding: 16, justifyContent: 'flex-end' }}>
          <LoadingButton
            type="submit"
            loading={loading}
            variant="contained"
            color="secondary"
            style={{ padding: '0px 32px' }}
            disableElevation
          >
            <Typography variant="body2">
              <FormattedMessage id="invoices.sendRequestButtonLabel" />
            </Typography>
          </LoadingButton>
        </DialogActions>
      </form>
    </DialogCustom>
  );
};

export default NewApprovalAccountDialog;
