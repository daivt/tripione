import { Button, Divider, Typography } from '@material-ui/core';
import { useFormik } from 'formik';
import moment, { Moment } from 'moment';
import React from 'react';
import { FormattedMessage, useIntl } from 'react-intl';
import { useDispatch } from 'react-redux';
import { Action } from 'redux';
import { ThunkDispatch } from 'redux-thunk';
import * as yup from 'yup';
import { GREY_700 } from '../../../../configs/colors';
import { some } from '../../../../constants';
import { AppState } from '../../../../redux/reducers';
import DialogCustom from '../../../common/components/DialogCustom';
import { Col, Line, Row } from '../../../common/components/elements';
import FormControlAutoComplete from '../../../common/components/FormControlAutoComplete';
import FormControlTextField from '../../../common/components/FormControlTextField';
import { generalCompanySize } from '../../../common/redux/constants';
import { goBackAction } from '../../../common/redux/reducer';
import { ApprovalAccountInfo } from '../utils';
import LoadingButton from '../../../common/components/LoadingButton';
import ApprovalHistoryBox from './ApprovalHistoryBox';
import { PaginationFilter } from '../../../../models/pagination';
import DateRangeFormControl from '../../../common/components/DateRangeFormControl';

interface Props {
  info: ApprovalAccountInfo;
  loading: boolean;
  setInfo(info: ApprovalAccountInfo): void;
  historyData?: some;
  pagination: PaginationFilter;
  onUpdatePagination(pagination: PaginationFilter): void;
}

const WAITING_APPROVAL_STATUS = 2;

const DetailTable: React.FC<Props> = props => {
  const { info, setInfo, loading, historyData, pagination, onUpdatePagination } = props;
  const intl = useIntl();

  const dispatch = useDispatch<ThunkDispatch<AppState, null, Action<string>>>();
  const approvalAccountSchema = yup.object().shape({
    name: yup
      .string()
      .nullable()
      .required(intl.formatMessage({ id: 'required' })),
    taxCode: yup
      .string()
      .nullable()
      .required(intl.formatMessage({ id: 'required' })),
    address: yup
      .string()
      .nullable()
      .required(intl.formatMessage({ id: 'required' })),
    companySizeRange: yup
      .mixed()
      .nullable()
      .required(intl.formatMessage({ id: 'required' })),
    representative: yup
      .string()
      .trim()
      .required(intl.formatMessage({ id: 'required' })),
    referrerPhone: yup
      .string()
      .trim()
      .required(intl.formatMessage({ id: 'required' })),
    representativePhone: yup
      .string()
      .trim()
      .required(intl.formatMessage({ id: 'required' })),
    contractStartDate: yup
      .number()
      .nullable()
      .required(intl.formatMessage({ id: 'required' })),
    contractEndDate: yup
      .number()
      .nullable()
      .required(intl.formatMessage({ id: 'required' })),
  });
  const formik = useFormik({
    initialValues: info,
    onSubmit: () => {
      // setOpenConfirm(true);
    },
    validationSchema: approvalAccountSchema,
  });

  React.useEffect(() => {
    formik.setValues(info);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [info]);

  return (
    <>
      <Row style={{ alignItems: 'flex-start' }}>
        <form autoComplete="off" onSubmit={formik.handleSubmit}>
          <Col style={{ width: 560 }}>
            <Row style={{ marginBottom: 8 }}>
              <Typography
                variant="body2"
                style={{ minWidth: 120, marginRight: 28, marginBottom: 20 }}
              >
                <FormattedMessage id="companyName" />
              </Typography>
              <FormControlTextField
                id="name"
                placeholder={intl.formatMessage({ id: 'company.enterCompanyName' })}
                value={formik.values.name}
                onChange={formik.handleChange}
                inputProps={{
                  maxLength: 50,
                }}
                errorMessage={
                  formik.errors.name && formik.submitCount > 0 ? formik.errors.name : undefined
                }
              />
            </Row>
            <Row style={{ marginBottom: 8 }}>
              <Typography
                variant="body2"
                style={{ minWidth: 120, marginRight: 28, marginBottom: 20 }}
              >
                <FormattedMessage id="taxCode" />
              </Typography>
              <FormControlTextField
                id="taxCode"
                formControlStyle={{ width: 250 }}
                placeholder={intl.formatMessage({ id: 'enterTaxCode' })}
                value={formik.values.taxCode}
                onChange={formik.handleChange}
                inputProps={{
                  maxLength: 50,
                }}
                errorMessage={
                  formik.errors.taxCode && formik.submitCount > 0
                    ? formik.errors.taxCode
                    : undefined
                }
              />
            </Row>
            <Row style={{ marginBottom: 8 }}>
              <Typography
                variant="body2"
                style={{ minWidth: 120, marginRight: 28, marginBottom: 20 }}
              >
                <FormattedMessage id="address" />
              </Typography>
              <FormControlTextField
                id="address"
                placeholder={intl.formatMessage({ id: 'enterAddress' })}
                value={formik.values.address}
                onChange={formik.handleChange}
                inputProps={{
                  maxLength: 50,
                }}
                errorMessage={
                  formik.errors.address && formik.submitCount > 0
                    ? formik.errors.address
                    : undefined
                }
              />
            </Row>
            <Row style={{ marginBottom: 8 }}>
              <Typography
                variant="body2"
                style={{ minWidth: 120, marginBottom: 20, marginRight: 28 }}
              >
                <FormattedMessage id="admin.approval.companySize" />
              </Typography>
              <FormControlAutoComplete
                readOnly
                value={formik.values.companySizeRange}
                getOptionSelected={(option, value) => {
                  return option.from === value.from;
                }}
                formControlStyle={{ width: 250 }}
                onChange={(e: any, value: some | null) => {
                  formik.setFieldValue('companySizeRange', value);
                }}
                getOptionLabel={value =>
                  intl.formatMessage(
                    {
                      id: 'companySize.value',
                    },
                    {
                      from: value.from,
                      to: value.to,
                    },
                  )
                }
                options={generalCompanySize}
                errorMessage={
                  formik.errors.companySizeRange && formik.submitCount > 0
                    ? formik.errors.companySizeRange
                    : undefined
                }
              />
            </Row>
            <Divider style={{ marginRight: 30 }} />
            <Typography variant="subtitle2" style={{ padding: '16px 0' }}>
              <FormattedMessage id="admin.corporate.contractTime" />
            </Typography>
            <Row style={{ alignItems: 'flex-start', marginBottom: 8, marginTop: 16 }}>
              <Typography variant="body2" style={{ flex: '0 0 152px', marginTop: 12 }}>
                <FormattedMessage id="admin.corporate.contractTime" />:
              </Typography>
              <DateRangeFormControl
                style={{ width: 250, marginRight: 0 }}
                optional
                startDate={
                  formik.values.contractStartDate
                    ? moment(formik.values.contractStartDate)
                    : undefined
                }
                endDate={
                  formik.values.contractEndDate ? moment(formik.values.contractEndDate) : undefined
                }
                onChange={(start?: Moment, end?: Moment) => {
                  formik.setFieldValue('contractStartDate', start ? start.valueOf() : undefined);
                  start && formik.setFieldValue('contractEndDate', end ? end.valueOf() : undefined);
                }}
                isOutsideRange={(e: any) => false}
                numberOfMonths={1}
                errorMessage={
                  formik.submitCount > 0
                    ? formik.errors.contractEndDate || formik.errors.contractStartDate
                    : undefined
                }
              />
            </Row>
            <Divider style={{ marginRight: 30 }} />
            <Row style={{ marginTop: 20 }}>
              <Typography
                variant="body2"
                style={{ minWidth: 120, marginRight: 28, marginBottom: 20 }}
              >
                <FormattedMessage id="email" />
              </Typography>
              <div style={{ minWidth: 120, marginRight: 28, marginBottom: 20 }}>{info.email}</div>
            </Row>
            <Row style={{ marginTop: 20 }}>
              <Typography
                variant="body2"
                style={{ minWidth: 120, marginRight: 28, marginBottom: 20 }}
              >
                <FormattedMessage id="admin.approval.contactPersonName" />
              </Typography>
              <FormControlTextField
                id="representative"
                placeholder={intl.formatMessage({ id: 'admin.approval.enterContactPersonName' })}
                value={formik.values.representative}
                onChange={formik.handleChange}
                inputProps={{
                  maxLength: 50,
                }}
                errorMessage={
                  formik.errors.representative && formik.submitCount > 0
                    ? formik.errors.representative
                    : undefined
                }
              />
            </Row>
            <Row style={{ marginTop: 20 }}>
              <Typography
                variant="body2"
                style={{ minWidth: 120, marginRight: 28, marginBottom: 20 }}
              >
                <FormattedMessage id="phoneNumber" />
              </Typography>
              <FormControlTextField
                id="referrerPhone"
                placeholder={intl.formatMessage({ id: 'enterPhoneNumber' })}
                value={formik.values.referrerPhone}
                onChange={formik.handleChange}
                inputProps={{
                  maxLength: 15,
                }}
                errorMessage={
                  formik.errors.referrerPhone && formik.submitCount > 0
                    ? formik.errors.referrerPhone
                    : undefined
                }
              />
            </Row>
            <Row style={{ marginTop: 20 }}>
              <Typography
                variant="body2"
                style={{ minWidth: 120, marginRight: 28, marginBottom: 20, flex: 1 }}
              >
                <FormattedMessage id="admin.approval.referenceContactPhone" />
              </Typography>
              <FormControlTextField
                id="representativePhone"
                placeholder={intl.formatMessage({ id: 'enterPhoneNumber' })}
                value={formik.values.representativePhone}
                onChange={formik.handleChange}
                inputProps={{
                  maxLength: 15,
                }}
                errorMessage={
                  formik.errors.representativePhone && formik.submitCount > 0
                    ? formik.errors.representativePhone
                    : undefined
                }
              />
            </Row>
            <Row>
              <>
                <Button
                  disabled={info.status !== WAITING_APPROVAL_STATUS}
                  size="large"
                  // type="submit"
                  color="secondary"
                  variant="contained"
                  style={{ minWidth: '120px', marginRight: '20px' }}
                  disableElevation
                  onClick={() => {
                    formik.submitForm().then(e => {
                      if (formik.isValid) {
                        formik.setFieldValue('approval', true);
                        formik.setFieldValue('reason', '');
                      }
                    });
                  }}
                >
                  <FormattedMessage id="approve" />
                </Button>
                <Button
                  disabled={info.status !== WAITING_APPROVAL_STATUS}
                  size="large"
                  color="secondary"
                  variant="contained"
                  disableElevation
                  style={{ minWidth: '120px', marginRight: '20px' }}
                  onClick={() => formik.setFieldValue('approval', false)}
                >
                  <FormattedMessage id="refuse" />
                </Button>
              </>
              <Button
                size="large"
                variant="outlined"
                disableElevation
                style={{ minWidth: '120px', marginRight: '20px' }}
                onClick={() => dispatch(goBackAction())}
              >
                <FormattedMessage id="reject" />
              </Button>
            </Row>
          </Col>
        </form>
        <ApprovalHistoryBox
          data={historyData}
          loading={loading}
          pagination={pagination}
          onUpdatePagination={onUpdatePagination}
        />
      </Row>
      <DialogCustom
        open={formik.values.approval !== undefined}
        onClose={() => {
          formik.setFieldValue('approval', undefined);
        }}
        PaperProps={{ style: { minWidth: 512 } }}
        titleLabel={
          <Typography variant="subtitle1">
            <FormattedMessage
              id={
                formik.values.approval
                  ? 'admin.approval.approveTitle'
                  : 'admin.approval.refuseTitle'
              }
            />
          </Typography>
        }
        footerContent={<></>}
      >
        <Col style={{ padding: '16px', minHeight: 60 }}>
          <Typography variant="body2" style={{ color: GREY_700 }}>
            <FormattedMessage
              id={
                formik.values.approval ? 'admin.approval.approveNote' : 'admin.approval.refuseNote'
              }
            />
          </Typography>
          <Row style={{ marginTop: 20 }}>
            <FormControlTextField
              id="reason"
              multiline
              formControlStyle={{ margin: 0 }}
              rows={4}
              rowsMax={5}
              label={<FormattedMessage id="admin.approval.reason" />}
              value={formik.values.reason}
              onChange={formik.handleChange}
              inputProps={{
                maxLength: 100,
                autoComplete: 'off',
              }}
              optional
            />
          </Row>
          <Row style={{ paddingTop: 8, justifyContent: 'flex-end' }}>
            <LoadingButton
              loading={loading}
              size="medium"
              variant="contained"
              color="secondary"
              disableElevation
              style={{ width: '120px', marginRight: '20px' }}
              onClick={() => {
                setInfo(formik.values);
              }}
            >
              <FormattedMessage id="accept" />
            </LoadingButton>
            <Button
              size="medium"
              variant="outlined"
              disableElevation
              style={{ width: '92px' }}
              onClick={() => {
                formik.setFieldValue('reason', '');
                formik.setFieldValue('approval', undefined);
              }}
            >
              <FormattedMessage id="reject" />
            </Button>
          </Row>
        </Col>
      </DialogCustom>
    </>
  );
};

export default DetailTable;
