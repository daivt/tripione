import { Button, Typography } from '@material-ui/core';
import React from 'react';
import { FormattedMessage } from 'react-intl';
import { shallowEqual, useSelector } from 'react-redux';
import { BLUE } from '../../../../configs/colors';
import { ROUTES } from '../../../../configs/routes';
import { some } from '../../../../constants';
import { PaginationFilter } from '../../../../models/pagination';
import { AppState } from '../../../../redux/reducers';
import { ChipButton, Row } from '../../../common/components/elements';
import Link from '../../../common/components/Link';
import TableCustom, { Columns } from '../../../common/components/TableCustom';
import { AdminApprovalFilter, getStatusColorLicense } from '../utils';
import Filter from './Filter';
import NewApprovalAccountDialog from './NewApprovalAccountDialog';
import PermissionDiv from '../../../common/components/PermissionDiv';

interface Props {
  loading: boolean;
  filter: AdminApprovalFilter;
  onUpdateFilter(filter: AdminApprovalFilter): void;
  data?: some;
  pagination: PaginationFilter;
  onUpdatePagination(pagination: PaginationFilter): void;
}
const TableBox: React.FC<Props> = props => {
  const { filter, loading, onUpdateFilter, data, pagination, onUpdatePagination } = props;
  const approvalStatusOptions = useSelector(
    (state: AppState) => state.common.approvalStatusOptions,
    shallowEqual,
  );

  const [open, setOpen] = React.useState(false);
  const columns = React.useMemo(() => {
    return [
      {
        title: 'admin.approval.companyName',
        dataIndex: 'name',
        style: { minWidth: 120 },
        render: (record: some, index: number) => (
          <Typography variant="caption" key={index} style={{ color: BLUE }}>
            {record?.name}
          </Typography>
        ),
      },
      {
        title: 'admin.approval.companySize',
        dataIndex: 'companySizeRange',
        style: { minWidth: 150 },
        render: (record: some, index: number) => (
          <Typography variant="caption" key={index}>
            <FormattedMessage
              id="companySize.value"
              values={{ from: record?.companySizeRange.from, to: record?.companySizeRange.to }}
            />
          </Typography>
        ),
      },
      {
        title: 'taxCode',
        dataIndex: 'taxCode',
      },
      {
        title: 'admin.approval.contactPersonName',
        render: (record: some, index: number) => (
          <Typography variant="caption" key={index}>
            {record?.representative}
          </Typography>
        ),
      },
      {
        title: 'email',
        dataIndex: 'email',
      },
      {
        title: 'phoneNumber',
        dataIndex: 'referrerPhone',
      },
      {
        title: 'admin.approval.referenceContactPhone',
        dataIndex: 'representativePhone',
      },
      {
        title: 'status',
        dataIndex: 'status',
        style: { minWidth: 120 },
        render: (record: some, index: number) => (
          <Typography
            variant="caption"
            key={index}
            style={{ color: getStatusColorLicense(record.status) }}
          >
            <FormattedMessage
              id={`${approvalStatusOptions.find(item => item.id === record.status)?.name}`}
            />
          </Typography>
        ),
      },
      {
        fixed: 'right',
        render: (record: some, index: number) => (
          <Row>
            <Link
              to={{
                pathname: ROUTES.adminApprovingCorporateAccount.approval,
                state: { approvalAccountData: record },
              }}
            >
              {record.status === 2 ? (
                <Button
                  variant="contained"
                  color="secondary"
                  disableElevation
                  style={{ minWidth: 100 }}
                >
                  <Typography variant="caption">
                    <FormattedMessage id="approve" />
                  </Typography>
                </Button>
              ) : (
                <ChipButton style={{ minWidth: 100, borderRadius: 4, margin: 0, padding: 8 }}>
                  <Typography variant="caption">
                    <FormattedMessage id="detail" />
                  </Typography>
                </ChipButton>
              )}
            </Link>
          </Row>
        ),
      },
    ] as Columns[];
  }, [approvalStatusOptions]);
  return (
    <div>
      <Filter filter={filter} loading={loading} onUpdateFilter={onUpdateFilter} />
      <TableCustom
        dataSource={data?.itemList}
        loading={loading}
        columns={columns}
        style={{ marginTop: 24 }}
        header={
          <Row style={{ padding: '16px 12px', justifyContent: 'space-between' }}>
            <Typography variant="subtitle2">
              <FormattedMessage id="admin.approval.approveList" />
            </Typography>
            <PermissionDiv permission={['tripione:admin:corporate:create']}>
              <Button
                variant="contained"
                color="secondary"
                style={{ minWidth: 120 }}
                disableElevation
                onClick={() => setOpen(true)}
              >
                <FormattedMessage id="addNew" />
              </Button>
            </PermissionDiv>
          </Row>
        }
        paginationProps={{
          count: data?.totalResults || 0,
          page: pagination.page || 1,
          rowsPerPage: pagination?.pageSize || 0,
          onChangePage: (event: unknown, newPage: number) => {
            onUpdatePagination({ ...pagination, page: newPage });
          },
          onChangeRowsPerPage: (event: React.ChangeEvent<HTMLInputElement>) => {
            onUpdatePagination({
              pageSize: parseInt(event.target.value, 10),
              page: 1,
            });
          },
        }}
      />
      <NewApprovalAccountDialog open={open} onClose={() => setOpen(false)} />
    </div>
  );
};

export default TableBox;
