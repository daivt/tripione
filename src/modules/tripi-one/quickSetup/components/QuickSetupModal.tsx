import { Button, Checkbox, Divider, FormControlLabel, Typography } from '@material-ui/core';
import Collapse from '@material-ui/core/Collapse';
import CheckBoxIcon from '@material-ui/icons/CheckBox';
import ExpandLess from '@material-ui/icons/ExpandLess';
import ExpandMore from '@material-ui/icons/ExpandMore';
import * as React from 'react';
import { FormattedHTMLMessage, FormattedMessage } from 'react-intl';
import { GREY_900 } from '../../../../configs/colors';
import { ReactComponent as TripOneWelcome } from '../../../../svg/ic_welcome.svg';
import { ReactComponent as SuccessIcon } from '../../../../svg/ic_success.svg';
import DialogCustom from '../../../common/components/DialogCustom';
import { Col, Row } from '../../../common/components/elements';
import LoadingButton from '../../../common/components/LoadingButton';
import Link from '../../../common/components/Link';
import { ROUTES } from '../../../../configs/routes';

interface Props {
  applyQuickSetup(ignored: boolean): void;
  open: boolean;
  onClose(): void;
  loading: boolean;
  success: boolean;
}

const QuickSetupModal = (props: Props) => {
  const { applyQuickSetup, open, onClose, loading, success } = props;
  const [openDetail, setOpenDetail] = React.useState<boolean>(false);

  return (
    <DialogCustom
      open={open}
      PaperProps={{ style: { width: '80vw', minWidth: 500 } }}
      titleLabel={
        <Typography variant="h6">
          <FormattedMessage id="quickSetup" />
        </Typography>
      }
      footerContent={<div />}
    >
      {!success ? (
        <>
          <Col style={{ padding: 16 }}>
            <TripOneWelcome style={{ margin: 'auto', width: 160, marginBottom: 18 }} />
            <Typography variant="h5" style={{ marginBottom: 18, textAlign: 'center' }}>
              <FormattedMessage id="quickSetup.welcome" />
            </Typography>
            <Typography variant="body1">
              <FormattedMessage id="quickSetup.note" />
            </Typography>
            <Button
              disableElevation
              variant="text"
              size="large"
              style={{ width: 240, marginTop: 8, padding: 8 }}
              onClick={() => setOpenDetail(one => !one)}
            >
              <FormattedMessage id="quickSetup.detailButton" />
              {openDetail ? <ExpandLess /> : <ExpandMore />}
            </Button>
            <Collapse in={openDetail}>
              <Typography variant="body2" style={{ color: GREY_900, padding: 8 }}>
                <FormattedHTMLMessage id="quickSetup.detail" />
              </Typography>
            </Collapse>
            <Typography variant="body1" style={{ margin: '12px 0' }}>
              <FormattedMessage id="quickSetup.confirm" />
            </Typography>
          </Col>
          <Divider />
          <Row style={{ padding: 16, justifyContent: 'center' }}>
            <LoadingButton
              loading={loading}
              disableElevation
              variant="contained"
              color="secondary"
              size="large"
              style={{ minWidth: 120, marginRight: 16 }}
              onClick={() => applyQuickSetup(false)}
            >
              <FormattedMessage id="quickSetup.apply" />
            </LoadingButton>
            <Button
              disableElevation
              variant="outlined"
              color="secondary"
              size="large"
              style={{ minWidth: 120 }}
              onClick={() => applyQuickSetup(true)}
            >
              <FormattedMessage id="reject" />
            </Button>
          </Row>
        </>
      ) : (
        <>
          <Col style={{ padding: 16 }}>
            <SuccessIcon style={{ margin: 'auto', width: 160, marginBottom: 18 }} />
            <Typography variant="h5" style={{ marginBottom: 18, textAlign: 'center' }}>
              <FormattedMessage id="quickSetup.successTitle" />
            </Typography>
            <Typography variant="body1" style={{ marginBottom: 8, textAlign: 'center' }}>
              <FormattedMessage id="quickSetup.successNote" />
            </Typography>
            <Typography variant="body1">
              <FormattedMessage id="quickSetup.successRemind" />
            </Typography>
          </Col>
          <Divider />
          <Row style={{ padding: 16, justifyContent: 'center' }}>
            <Link to={{ pathname: ROUTES.generalSetting.userManagement.result }}>
              <Button
                disableElevation
                variant="contained"
                color="secondary"
                size="large"
                style={{ minWidth: 160, marginRight: 16 }}
                onClick={onClose}
              >
                <FormattedMessage id="userManagement" />
              </Button>
            </Link>
            <Link to="/">
              <Button
                disableElevation
                variant="contained"
                color="secondary"
                size="large"
                style={{ minWidth: 140 }}
                onClick={onClose}
              >
                <FormattedMessage id="home" />
              </Button>
            </Link>
          </Row>
        </>
      )}
    </DialogCustom>
  );
};

export default QuickSetupModal;
