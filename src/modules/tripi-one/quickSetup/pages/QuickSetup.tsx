import { useSnackbar } from 'notistack';
import * as React from 'react';
import { useSelector, shallowEqual, useDispatch } from 'react-redux';
import { Action } from 'redux';
import { ThunkDispatch } from 'redux-thunk';
import { SUCCESS_CODE } from '../../../../constants';
import { AppState } from '../../../../redux/reducers';
import { snackbarSetting } from '../../../common/components/elements';
import QuickSetupModal from '../components/QuickSetupModal';
import { API_PATHS } from '../../../../configs/API';
import { fetchThunk } from '../../../common/redux/thunk';
import { roleOptions } from '../../../common/redux/constants';
import { setUserData } from '../../../account/redux/accountReducer';

interface Props {}

const QuickSetup = (props: Props) => {
  const dispatch = useDispatch<ThunkDispatch<AppState, null, Action<string>>>();
  const userData = useSelector((state: AppState) => state.account.userData, shallowEqual);
  const { enqueueSnackbar, closeSnackbar } = useSnackbar();

  const [open, setOpen] = React.useState<boolean>(false);
  const [success, setSuccess] = React.useState<boolean>(false);
  const [loading, setLoading] = React.useState<boolean>(false);

  const applyQuickSetup = React.useCallback(
    async (ignored: boolean) => {
      setLoading(true);
      const json = await dispatch(
        fetchThunk(
          API_PATHS.quickSetup,
          'post',
          JSON.stringify({
            ignoreQuickSetup: ignored,
          }),
        ),
      );
      if (json.code !== SUCCESS_CODE) {
        enqueueSnackbar(
          json.message,
          snackbarSetting(key => closeSnackbar(key), { color: 'error' }),
        );
      } else if (!ignored && json.code === SUCCESS_CODE) {
        setSuccess(true);
        dispatch(setUserData({ ...userData, quickSetup: true }));
        enqueueSnackbar(
          json.message,
          snackbarSetting(key => closeSnackbar(key)),
        );
      } else {
        setOpen(false);
        dispatch(setUserData({ ...userData, quickSetup: true }));
      }
      setLoading(false);
    },
    [closeSnackbar, dispatch, enqueueSnackbar, userData],
  );

  React.useEffect(() => {
    // Check if this is the OWNER and the quickSetup: false, show the modal
    !userData?.quickSetup &&
      userData?.role === roleOptions[0].value &&
      setTimeout(() => {
        setOpen(true);
      }, 1000);
  }, [userData]);

  return (
    <>
      <QuickSetupModal
        success={success}
        loading={loading}
        open={open}
        onClose={() => setOpen(false)}
        applyQuickSetup={applyQuickSetup}
      />
    </>
  );
};

export default QuickSetup;
