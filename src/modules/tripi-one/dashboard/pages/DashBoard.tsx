import React, { useCallback } from 'react';
import { useSelector } from 'react-redux';
import { some } from '../../../../constants';
import { AppState } from '../../../../redux/reducers';
import StatisticBoard from '../components/StatisticBoard';
import TopBar from '../components/TopBar';
import { fakeCurrentAmount } from '../constants';

interface Props {}

const DashBoard: React.FC<Props> = props => {
  const userData = useSelector((state: AppState) => state.account.userData);
  const [currentAmount, setCurrentAmount] = React.useState<some | undefined>();
  const [topBarLoading, setTopBarLoading] = React.useState(true);

  const fetchCurrentAmount = useCallback(() => {
    setTopBarLoading(true);
    setCurrentAmount(fakeCurrentAmount.data);
    setTopBarLoading(false);
  }, []);

  React.useEffect(() => {
    fetchCurrentAmount();
  }, [fetchCurrentAmount]);

  return (
    <div>
      <TopBar loading={topBarLoading} userData={userData} currentAmount={currentAmount} />
      <StatisticBoard />
    </div>
  );
};

export default DashBoard;
