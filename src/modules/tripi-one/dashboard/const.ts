export const fakeData = [
  { name: 'Store 1', value: 10000 },
  { name: 'Store 2', value: 20000 },
  { name: 'Store 3', value: 30000 },
  { name: 'Store 4', value: 40000 },
];

export const FILTER_BY_MONTH = [
  {
    id: 0,
    text: 'dashBoard.sortByAll',
    value: 'all',
  },
  ...Array(12)
    .fill(0)
    .map((v: any, index: number) => {
      return { id: index + 1, text: 'month', value: index + 1 };
    }),
];

export const HEIGHT_CHART = 300;
