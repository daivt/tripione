export const fakeCurrentAmount = {
  code: 'SUCCESS',
  message: 'success',
  data: {
    companyName: 'Công ty TNHH Lữ Hành Du Lịch Tân Kỷ Nguyên',
    amount: 12345678900,
    currencyCode: "VND"
  },
};

export const fakeData = {
  code: "SUCCESS",
  message: "success",
  data: {
    orderStatistic: {
      totalOrder: 12563,
      totalFlight: 6000,
      totalHotel: 6563,
      itemList: [
        {
          month: 6,
          numFlight: 1900,
          numHotel: 1950
        },
        {
          month: 7,
          numFlight: 2150,
          numHotel: 2700
        },
        {
          month: 8,
          numFlight: 950,
          numHotel: 1913
        }
      ]
    },
    revenueStatistic: {
      totalExpense: 250000000,
      flightExpense: 110000000,
      hotelExpense: 140000000,
      currencyCode: "VND"
    }
  }
}
