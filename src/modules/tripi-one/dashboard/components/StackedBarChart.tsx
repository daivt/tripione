import * as am4charts from '@amcharts/amcharts4/charts';
import * as am4core from '@amcharts/amcharts4/core';
import animated from '@amcharts/amcharts4/themes/animated';
import material from '@amcharts/amcharts4/themes/material';
import { Card } from '@material-ui/core';
import moment, { Moment } from 'moment';
import React, { useState } from 'react';
import { useIntl } from 'react-intl';
import { TEAL, YELLOW } from '../../../../configs/colors';
import { Col } from '../../../common/components/elements';
import LoadingIcon from '../../../common/components/LoadingIcon';
import { HEIGHT_CHART } from '../const';

am4core.useTheme(material);
am4core.useTheme(animated);

interface Props {}

const StackedBarChart = (props: Props) => {
  const intl = useIntl();
  const [loading, setLoading] = useState(false);
  const [dateRange, setDateRange] = useState<{ startDate: Moment; endDate: Moment }>({
    startDate: moment().subtract(30, 'day'),
    endDate: moment(),
  });
  const chart = React.useRef<am4charts.XYChart | null>(null);

  const createSeries = React.useCallback((field, name, color) => {
    if (chart.current) {
      const series = chart.current.series.push(new am4charts.ColumnSeries());
      series.name = name;
      series.dataFields.valueY = field;
      series.dataFields.categoryX = 'month';
      series.sequencedInterpolation = true;

      // Make it stacked
      series.stacked = true;

      // Configure columns
      series.columns.template.width = am4core.percent(60);
      series.columns.template.tooltipText = '[color: #fff][bold]{name}[/]: {valueY}[/]';
      series.columns.template.fill = color;
      series.stroke = am4core.color(color);
      if (series.tooltip) {
        series.tooltip.autoTextColor = false;
        series.tooltip.label.fill = am4core.color('white');
      }

      // Add label
      const labelBullet = series.bullets.push(new am4charts.LabelBullet());
      labelBullet.label.text = '{valueY}';
      labelBullet.label.fontSize = 10;
      labelBullet.locationY = 0.5;
      labelBullet.label.hideOversized = true;
    }
  }, []);

  const buildChart = React.useCallback(() => {
    const chartTemp = am4core.create('StackChart', am4charts.XYChart);

    chartTemp.data = [
      {
        month: '1',
        flightsBooking: 25,
        hotelsBooking: 35,
      },
      {
        month: '2',
        flightsBooking: 26,
        hotelsBooking: 27,
      },
      {
        month: '3',
        flightsBooking: 21,
        hotelsBooking: 29,
      },
      {
        month: '4',
        flightsBooking: 18,
        hotelsBooking: 49,
      },
      {
        month: '5',
        flightsBooking: 25,
        hotelsBooking: 32,
      },
    ];

    const categoryAxis = chartTemp.xAxes.push(new am4charts.CategoryAxis());
    categoryAxis.dataFields.category = 'month';
    categoryAxis.renderer.grid.template.disabled = true;

    const valueAxis = chartTemp.yAxes.push(new am4charts.ValueAxis());
    valueAxis.renderer.inside = true;
    valueAxis.renderer.grid.template.disabled = true;
    valueAxis.renderer.labels.template.disabled = true;
    valueAxis.min = 0;

    chartTemp.legend = new am4charts.Legend();
    chartTemp.legend.position = 'bottom';
    chartTemp.legend.fontSize = 14;
    // chartTemp.legend.markers.template.
    // chartTemp.legend.useDefaultMarker = true;

    const marker: any = chartTemp.legend.markers.template.children.getIndex(0);
    if (marker) {
      marker.cornerRadius(16, 16, 16, 16);
    }
    chartTemp.legend.markers.template.width = 16;
    chartTemp.legend.markers.template.height = 16;

    chart.current = chartTemp;
    createSeries('flightsBooking', intl.formatMessage({ id: 'dashboard.flightsBooking' }), TEAL);
    createSeries('hotelsBooking', intl.formatMessage({ id: 'dashboard.hotelsBooking' }), YELLOW);
  }, [createSeries, intl]);

  React.useEffect(() => {
    buildChart();
    return () => {
      chart?.current?.dispose();
    };
  }, [buildChart]);

  return (
    <Card
      style={{
        flex: '1 1 50%',
        margin: '32px 16px 0',
        minWidth: 200,
        maxWidth: '50%',
        boxShadow: 'none',
      }}
    >
      <Col style={{ position: 'relative', padding: '4px' }}>
        <div
          id="StackChart"
          style={{
            height: HEIGHT_CHART,
            opacity: loading ? 0.5 : 1,
            transition: 'opacity 0.3s',
          }}
        />
        {loading && (
          <LoadingIcon
            style={{
              position: 'absolute',
              display: 'flex',
              justifyContent: 'center',
              top: 0,
              bottom: 0,
              right: 65,
              width: '100%',
            }}
          />
        )}
      </Col>
    </Card>
  );
};

export default StackedBarChart;
