import { Typography, Paper, Button } from '@material-ui/core';
import * as React from 'react';
import { FormattedMessage } from 'react-intl';
import { Row } from '../../../common/components/elements';
import DateRangeFormControl from '../../../common/components/DateRangeFormControl';
import StackedBarChart from './StackedBarChart';
import DonutChart from './DonutChart';
import Link from '../../../common/components/Link';
import { ROUTES } from '../../../../configs/routes';

interface Props {}

const StatisticBoard = (props: Props) => {
  return (
    <div>
      <Paper style={{ padding: '16px 8px', marginBottom: 12 }}>
        <Row style={{ justifyContent: 'space-between' }}>
          <Row>
            <Typography variant="body2" style={{ marginBottom: 20 }}>
              <FormattedMessage id="dashboard.dateRange" />
            </Typography>
            <DateRangeFormControl
              style={{ width: 238, marginLeft: 16 }}
              optional
              onChange={(startDate, endDate) => {}}
            />
          </Row>
          <Button
            type="submit"
            variant="contained"
            style={{ minWidth: 160 }}
            color="primary"
            size="large"
            disableElevation
          >
            <FormattedMessage id="dashboard.viewDetail" />
          </Button>
        </Row>
        <Row>
          <StackedBarChart />
          <DonutChart />
        </Row>
      </Paper>
      <Row>
        <Link to={{ pathname: ROUTES.booking.flight.default }}>
          <Button
            type="submit"
            variant="contained"
            style={{ minWidth: 150, marginRight: 16 }}
            color="secondary"
            size="large"
            disableElevation
          >
            <FormattedMessage id="flight.booking" />
          </Button>
        </Link>
        <Link to={{ pathname: ROUTES.booking.hotel.default }}>
          <Button
            type="submit"
            variant="contained"
            style={{ minWidth: 150 }}
            color="secondary"
            size="large"
            disableElevation
          >
            <FormattedMessage id="hotel.booking" />
          </Button>
        </Link>
      </Row>
    </div>
  );
};

export default StatisticBoard;
