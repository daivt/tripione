import { Typography, Paper, Button } from '@material-ui/core';
import * as React from 'react';
import { FormattedMessage, FormattedNumber } from 'react-intl';
import styled from 'styled-components';
import { some } from '../../../../constants';
import { Col, Row } from '../../../common/components/elements';
import { RED, PURPLE, PURPLE_100, GREY_700, GREEN, PINK } from '../../../../configs/colors';
import Link from '../../../common/components/Link';
import { ROUTES } from '../../../../configs/routes';
import { ReactComponent as IconWallet } from '../../../../svg/ic_wallet.svg';
import LoadingIcon from '../../../common/components/LoadingIcon';

const BigTypo = styled.span`
  margin-top: 8px;
  font-size: 34px;
  color: ${props => props.color};
`;

interface Props {
  loading: boolean;
  userData?: some;
  currentAmount?: some;
}

const TopBar = (props: Props) => {
  const { loading, userData, currentAmount } = props;

  return (
    <Paper
      style={{
        display: 'flex',
        padding: 16,
        marginBottom: 16,
      }}
    >
      <Row style={{ flex: '0 0 50%' }}>
        {loading ? (
          <LoadingIcon style={{ height: 120 }} />
        ) : (
          <Row>
            <img
              src={userData?.accountDetailInfo?.profilePhoto}
              alt="Profile"
              style={{ width: 'auto', height: 100, borderRadius: 6 }}
            />
            <Col style={{ display: 'flex', flexDirection: 'column', marginLeft: 12 }}>
              <Typography variant="body1">
                <FormattedMessage id="hello" />,{' '}
                <strong>{userData?.accountDetailInfo?.name}</strong>
              </Typography>
              <Typography variant="body2" style={{ color: GREY_700 }}>
                {currentAmount?.companyName}
              </Typography>
              <Row style={{ marginTop: 12, flexWrap: 'wrap' }}>
                <IconWallet />
                <Typography
                  variant="subtitle1"
                  style={{ fontSize: 18, fontWeight: 'bold', color: RED, margin: '0 8px' }}
                >
                  <FormattedNumber value={currentAmount?.amount} />
                  &nbsp;
                  {currentAmount?.currencyCode}
                </Typography>
                <Row>
                  <Link to={{ pathname: ROUTES.transactions.topup }}>
                    <Button
                      type="submit"
                      variant="contained"
                      style={{ minWidth: 100, minHeight: 30, marginRight: 12 }}
                      color="secondary"
                      size="small"
                      disableElevation
                    >
                      <FormattedMessage id="recharge" />
                    </Button>
                  </Link>
                  <Link to={{ pathname: ROUTES.transactions.withdraw }}>
                    <Button
                      type="submit"
                      variant="contained"
                      style={{
                        minWidth: 100,
                        minHeight: 30,
                        color: PURPLE,
                        background: PURPLE_100,
                      }}
                      color="secondary"
                      size="small"
                      disableElevation
                    >
                      <FormattedMessage id="transactions.withdraw" />
                    </Button>
                  </Link>
                </Row>
              </Row>
            </Col>
          </Row>
        )}
      </Row>
      <Row
        style={{
          flex: '0 1 50%',
          alignItems: 'start',
          justifyContent: 'space-between',
          padding: '12px 0 12px 8px',
        }}
      >
        <Col style={{ padding: '0 8px 0 0' }}>
          <Col>
            <Typography variant="body2">
              <FormattedMessage id="dashboard.pendingTrips" />
            </Typography>
            <BigTypo color={GREEN}>20</BigTypo>
          </Col>
        </Col>
        <Col style={{ padding: '0 8px 0 0' }}>
          <Col>
            <Typography variant="body2">
              <FormattedMessage id="dashboard.pendingBills" />
            </Typography>
            <BigTypo color={PURPLE}>20</BigTypo>
          </Col>
        </Col>
        <Col style={{ padding: '0 8px 0 0' }}>
          <Col>
            <Typography variant="body2">
              <FormattedMessage id="dashboard.activeAccounts" />
            </Typography>
            <BigTypo color={RED}>20</BigTypo>
          </Col>
        </Col>
        <Col style={{ padding: '0 8px 0 0' }}>
          <Col>
            <Typography variant="body2">
              <FormattedMessage id="dashboard.pendingAccounts" />
            </Typography>
            <BigTypo color={PINK}>20</BigTypo>
          </Col>
        </Col>
      </Row>
    </Paper>
  );
};

export default TopBar;
