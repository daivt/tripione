import * as am4charts from '@amcharts/amcharts4/charts';
import * as am4core from '@amcharts/amcharts4/core';
import animated from '@amcharts/amcharts4/themes/animated';
import material from '@amcharts/amcharts4/themes/material';
import { Card } from '@material-ui/core';
import React from 'react';
import { connect } from 'react-redux';
import { AnyAction } from 'redux';
import { ThunkDispatch } from 'redux-thunk';
import { AppState } from '../../../../redux/reducers';
import { Col } from '../../../common/components/elements';
import LoadingIcon from '../../../common/components/LoadingIcon';
import { HEIGHT_CHART } from '../const';

am4core.useTheme(material);
am4core.useTheme(animated);

interface Props {
  dispatch: ThunkDispatch<AppState, null, AnyAction>;
  loading?: boolean;
}

const DonutChard: React.FC<Props> = props => {
  const { loading } = props;

  const dataChart = React.useRef<am4charts.PieChart | null>(null);

  const buildChart = React.useCallback(() => {
    // Create chart instance
    const chart = am4core.create('DonutChart', am4charts.PieChart);

    // Add data
    chart.data = [
      {
        subject: 'Vé máy bay',
        spend: 501.9,
      },
      {
        subject: 'Khách sạn',
        spend: 301.9,
      },
      {
        subject: 'Khác',
        spend: 201.1,
      },
    ];

    // Set inner radius
    chart.innerRadius = am4core.percent(60);

    // Add and configure Series
    const pieSeries = chart.series.push(new am4charts.PieSeries());
    pieSeries.dataFields.value = 'spend';
    pieSeries.dataFields.category = 'subject';
    pieSeries.slices.template.stroke = am4core.color('#fff');
    pieSeries.slices.template.strokeWidth = 2;
    pieSeries.slices.template.strokeOpacity = 1;
    pieSeries.ticks.template.disabled = true;
    pieSeries.labels.template.disabled = true;

    // This creates initial animation
    pieSeries.hiddenState.properties.opacity = 1;
    pieSeries.hiddenState.properties.endAngle = -90;
    pieSeries.hiddenState.properties.startAngle = -90;

    const middleLabel = pieSeries.createChild(am4core.Label);
    middleLabel.text = 'Tổng: 90.000.000 VND';
    middleLabel.horizontalCenter = 'middle';
    middleLabel.verticalCenter = 'middle';
    middleLabel.fontSize = 12;

    chart.legend = new am4charts.Legend();
    chart.legend.position = 'right';
    chart.legend.fontSize = 14;

    const marker: any = chart.legend.markers.template.children.getIndex(0);
    if (marker) {
      marker.cornerRadius(16, 16, 16, 16);
    }
    chart.legend.markers.template.width = 16;
    chart.legend.markers.template.height = 16;
  }, []);

  React.useEffect(() => {
    buildChart();
    return () => {
      dataChart?.current?.dispose();
    };
  }, [buildChart, dataChart]);

  return (
    <Card
      style={{
        flex: '1 1 50%',
        margin: '32px 16px 0',
        minWidth: 200,
        maxWidth: '50%',
        boxShadow: 'none',
      }}
    >
      <Col style={{ position: 'relative' }}>
        <div
          id="DonutChart"
          style={{
            height: HEIGHT_CHART,
            opacity: loading ? 0.5 : 1,
            transition: 'opacity 0.3s',
          }}
        />
        {loading && (
          <LoadingIcon
            style={{
              position: 'absolute',
              display: 'flex',
              justifyContent: 'center',
              top: 0,
              bottom: 0,
              right: 65,
              width: '100%',
            }}
          />
        )}
      </Col>
    </Card>
  );
};

export default connect()(DonutChard);
