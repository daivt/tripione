import { Divider, Tab, Tabs } from '@material-ui/core';
import * as React from 'react';
import { useIntl } from 'react-intl';
import { Route, useHistory, useLocation } from 'react-router';
import SwipeableViews from 'react-swipeable-views';
import { ROUTES } from '../../../../configs/routes';
import FlightReport from '../flight/pages/FlightReport';
import HotelReport from '../hotel/pages/HotelReport';
import TripReport from '../trip/pages/TripReport';

interface Props {}

const Report = (props: Props) => {
  const [loading, setLoading] = React.useState(true);
  const location = useLocation();
  const history = useHistory();
  const intl = useIntl();

  const listRoutes = [
    ROUTES.report.trip,
    ROUTES.report.flight,
    ROUTES.report.hotel,
  ];

  const getTabIndex = React.useMemo(() => {
    let tabIndex = 0;
    listRoutes.forEach((value: string, index: number) => {
      if (value === location.pathname) {
        tabIndex = index;
      }
    });
    return tabIndex;
  }, [listRoutes, location.pathname]);

  return (
    <>
      <Tabs
        value={getTabIndex}
        onChange={(e, val) => {
          history.replace({ pathname: listRoutes[val] });
        }}
        indicatorColor="primary"
        textColor="primary"
      >
        <Tab label={intl.formatMessage({ id: 'report.tripReport' })} />
        <Tab label={intl.formatMessage({ id: 'report.flightReport' })} />
        <Tab label={intl.formatMessage({ id: 'report.hotelReport' })} />
      </Tabs>
      <Divider />
      <SwipeableViews
        index={getTabIndex}
        style={{ marginTop: 16 }}
        onChangeIndex={val => history.replace({ pathname: listRoutes[val] })}
      >
        <Route path={ROUTES.report.trip} component={TripReport} />
        <Route path={ROUTES.report.flight} component={FlightReport} />
        <Route path={ROUTES.report.hotel} component={HotelReport} />
      </SwipeableViews>
    </>
  );
};

export default Report;
