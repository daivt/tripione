import { SelectItem } from '../../../../models/object';

export interface IFlightReportFilter {
  from: number | null;
  to: number | null;
  departureFrom: string;
  departureTo: string;
  prn: string;
  bookingId: string;
  passengerName: string;
  departmentId: SelectItem | undefined;
  jobTitleId: SelectItem | undefined;
  status: SelectItem | null;
}

export const defaultFlightReportFilter: IFlightReportFilter = {
  from: null,
  to: null,
  departureFrom: '',
  departureTo: '',
  prn: '',
  bookingId: '',
  passengerName: '',
  departmentId: undefined,
  jobTitleId: undefined,
  status: null,
}
