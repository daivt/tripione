export const fakeFlightReport = {
  code: "SUCCESS",
  message: "success",
  data: {
    totalResults: 1,
    page: 1,
    pageSize: 10,
    itemList: [
      {
        orderId: 1,
        companyName: "Công ty cổ phần Tin học An Phát",
        bookedDate: 1591870453611,
        ticketCode: "EPVSHU",
        orderCode: "F345345",
        ticketNumber: "7382436819294",
        passengerName: "HOANG VU BINH",
        flightCourse: "HAN-SGN-HAN",
        outbound: {
          fromAirport: "HAN",
          toAirport: "SGN",
          fromAirportName: "Nội Bài",
          toAirportName: "Tân Sơn Nhất",
          departureDate: 1590740389003,
          arrivalDate: 1590757200000,
          airlineName: "Vietnam Airline",
          flightCode: "VN917",
          ticketClassName: "Business"
        },
        inbound: {
          fromAirport: "SGN",
          toAirport: "HAN",
          fromAirportName: "Tân Sơn Nhất",
          toAirportName: "Nội Bài",
          departureDate: 1590740389003,
          arrivalDate: 1590757200000,
          airlineName: "Vietnam Airline",
          flightCode: "VN537",
          ticketClassName: "Business"
        },
        productType: "Vé máy bay",
        booker: {
          id: 123456789,
          email: "quannm.it@tripi.vn",
          name: "Ngô Minh Quân",
          employeeId: "IT00984",
          department: {
            id: "1",
            name: "Phòng IT"
          },
          position: {
            id: "1",
            name: "Quản lí dự án"
          }
        },
        lastApprover: {
          id: 987654321,
          email: "daivt.it@tripi.vn",
          name: "Vũ Tiến Đại",
          employeeId: "IT00969",
          department: {
            id: "1",
            name: "Phòng IT"
          },
          position: {
            id: "2",
            name: "Lập trình viên Web"
          }
        },
        orderStatus: {
          id: "1",
          name: "Đã thanh toán"
        },
        paymentDetail: {
          ticketPrice: 11900000,
          addonServicePrice: 175000,
          addonService: "Hành lí; Suất ăn",
          tax: 119000,
          additionalFee: 862000,
          bookerDiscount: 30000,
          discountCode: "ONEDISCOUNT",
          usedPoint: 500,
          usedPointAmount: 50000,
          paymentMethod: "QR Pay",
          paymentMethodFee: 3000,
          grandTotal: 12500000,
          currencyCode: "VND",
          paymentDate: 1591871468087
        }
      }
    ]
  }
}
