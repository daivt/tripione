import { Button, Typography } from '@material-ui/core';
import moment from 'moment';
import * as React from 'react';
import { FormattedMessage } from 'react-intl';
import { shallowEqual, useSelector } from 'react-redux';
import { some } from '../../../../../constants';
import { DATE_FORMAT, DATE_TIME_FORMAT_BREAK } from '../../../../../models/moment';
import { PaginationFilter } from '../../../../../models/pagination';
import { AppState } from '../../../../../redux/reducers';
import { ReactComponent as IconExport } from '../../../../../svg/ic_export_file.svg';
import { Row } from '../../../../common/components/elements';
import TableCustom, { Columns } from '../../../../common/components/TableCustom';
import { getAprrovalColorStatus } from '../../../tripManagement/utils';
import { ITripReportFilter } from '../utils';
import Filter from './Filter';

interface Props {
  loading: boolean;
  data?: some;
  filter: ITripReportFilter;
  pagination: PaginationFilter;
  onUpdateFilter(filter: ITripReportFilter): void;
  onUpdatePagination(pagination: PaginationFilter): void;
  onDownloadTripReport(data: some[]): void;
}

const TableBox: React.FunctionComponent<Props> = props => {
  const { loading, data, filter,pagination, onUpdateFilter, onUpdatePagination, onDownloadTripReport } = props;
  const { tripApprovalStatusOptions } = useSelector(
    (state: AppState) => state.common,
    shallowEqual,
  );

  const columns = React.useMemo(() => {
    return [
      {
        title: 'report.bookedDate',
        render: (record: some, index: number) => (
          <Typography variant="caption" style={{ whiteSpace: 'pre-wrap' }}>
            {moment(record.bookDate).isValid()
              ? moment(record.bookDate).format(DATE_TIME_FORMAT_BREAK)
              : null}
          </Typography>
        ),
      },
      {
        title: 'tripManagement.tripCode',
        width: 80,
        dataIndex: 'companyTripCode',
      },
      {
        title: 'tripManagement.tripName',
        width: 80,
        dataIndex: 'companyTripName',
      },
      {
        title: 'report.bookedBy',
        render: (record: some, index: number) => (
          <Typography variant="caption">{record.bookerName}</Typography>
        ),
      },
      {
        title: 'department',
        render: (record: some, index: number) => (
          <Typography variant="caption">{record.departmentName}</Typography>
        ),
      },
      {
        title: 'position',
        render: (record: some, index: number) => (
          <Typography variant="caption">{record.jobTitleName}</Typography>
        ),
      },
      {
        title: 'report.tripApprovalStatus',
        render: (record: some, index: number) => (
          <Typography
            variant="caption"
            style={{ color: getAprrovalColorStatus(record.status) }}
          >
            <FormattedMessage
              id={
                tripApprovalStatusOptions?.find(
                  v => v.id === record.status,
                )?.name
              }
            />
          </Typography>
        ),
      },
      {
        title: 'tripManagement.totalPayable',
        dataIndex: 'paymentFee'
      },
      {
        title: 'paymentMethod',
        render: (record: some, index: number) => (
          <Typography variant="caption">{record.paymentMethod}</Typography>
        ),
      },
      {
        title: 'report.paymentDate',
        render: (record: some, index: number) => (
          <Typography variant="caption">
            {moment(record.paymentTime).isValid()
              ? moment(record.paymentTime).format(DATE_FORMAT)
              : null}
          </Typography>
        ),
      },
    ] as Columns[];
  }, [tripApprovalStatusOptions]);

  return (
    <div style={{ minHeight: 380 }}>
      <Filter loading={loading} filter={filter} onUpdateFilter={onUpdateFilter} />
      <TableCustom
        noColumnIndex
        dataSource={data?.itemList}
        loading={loading}
        columns={columns}
        style={{ marginTop: 24, whiteSpace: 'nowrap' }}
        header={
          <Row style={{ justifyContent: 'space-between', padding: '16px 12px' }}>
            <Typography variant="subtitle2" style={{ fontWeight: 500, marginRight: '24px' }}>
              <FormattedMessage id="report.reportTable" />
            </Typography>
            <Button
              disabled={data?.itemList.length === 0}
              variant="contained"
              color="secondary"
              style={{ minWidth: 148, marginRight: 12}}
              disableElevation
              onClick={() => onDownloadTripReport(data?.itemList)}
            >
              <IconExport style={{ marginRight: 8 }} />
              <FormattedMessage id="report.export" />
            </Button>
          </Row>
        }
        paginationProps={{
          count: data?.totalResults || 0,
          page: pagination.page || 1,
          rowsPerPage: pagination?.pageSize || 0,
          onChangePage: (event: unknown, newPage: number) => {
            onUpdatePagination({ ...pagination, page: newPage });
          },
          onChangeRowsPerPage: (event: React.ChangeEvent<HTMLInputElement>) => {
            onUpdatePagination({
              pageSize: parseInt(event.target.value, 10),
              page: 1,
            });
          },
        }}
      />
    </div>
  );
};

export default TableBox;
