import { SelectItem } from '../../../../models/object';

export interface ITripReportFilter {
  from: number | undefined;
  to: number | undefined;
  status: SelectItem | undefined;
  companyTripCode?: string;
  bookerName?: string;
  departmentId: SelectItem | undefined;
  jobTitleId: SelectItem | undefined;
}

export const defaultTripReportFilter: ITripReportFilter = {
  from: undefined,
  to: undefined,
  status: undefined,
  departmentId: undefined,
  jobTitleId: undefined,
}
