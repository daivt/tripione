import { useSnackbar } from 'notistack';
import moment from 'moment';
import queryString from 'query-string';
import * as React from 'react';
import { useIntl } from 'react-intl';
import { useDispatch } from 'react-redux';
import { useLocation } from 'react-router';
import { Action } from 'redux';
import { ThunkDispatch } from 'redux-thunk';
import { API_PATHS } from '../../../../../configs/API';
import { some, SUCCESS_CODE } from '../../../../../constants';
import { DATE_FORMAT_BACK_END } from '../../../../../models/moment';
import { defaultPaginationFilter, PaginationFilter } from '../../../../../models/pagination';
import { AppState } from '../../../../../redux/reducers';
import { goToReplace } from '../../../../common/redux/reducer';
import { fetchThunk } from '../../../../common/redux/thunk';
import { uniqueObject } from '../../../../utils';
import TableBox from '../components/TableBox';
import { defaultTripReportFilter, ITripReportFilter } from '../utils';

interface Props {}

const TripReport = (props: Props) => {
  const dispatch = useDispatch<ThunkDispatch<AppState, null, Action<string>>>();
  const [data, setData] = React.useState<some | undefined>(undefined);
  const [loading, setLoading] = React.useState(true);
  const [filter, setFilter] = React.useState<ITripReportFilter>(defaultTripReportFilter);
  const [pagination, setPagination] = React.useState<PaginationFilter>(defaultPaginationFilter);
  const { enqueueSnackbar, closeSnackbar } = useSnackbar();
  const location = useLocation();
  const intl = useIntl();

  const updateQueryParams = React.useCallback(() => {
    if (location.search) {
      const filterParams = queryString.parse(location.search) as any;
      const filterTmp = queryString.parse(filterParams.filters) as any;
      const tmp = {
        ...filterTmp,
      };
      setFilter(uniqueObject(tmp) as any);
      const paginationTmp = {
        page: filterParams.page ? parseInt(`${filterParams.page}`, 10) : 1,
        pageSize: filterParams.pageSize ? parseInt(`${filterParams.pageSize}`, 10) : 10,
      } as PaginationFilter;
      setPagination(paginationTmp);
    } else {
      dispatch(
        goToReplace({
          search: queryString.stringify({
            ...defaultTripReportFilter,
            filters: queryString.stringify({}),
          }),
        }),
      );
    }
  }, [dispatch, location.search]);

  const fetchData = React.useCallback(
    async (filterParams: some, paginationParams: PaginationFilter) => {
      setLoading(true);
      const json = await dispatch(
        fetchThunk(
          API_PATHS.tripReport,
          'post',
          JSON.stringify({
            ...paginationParams,
            filters: filterParams,
          }),
        ),
      );
      if (json?.code === SUCCESS_CODE) {
        setData(json.data);
      }
      setLoading(false);
    },
    [dispatch],
  )

  const onDownloadTripReport = React.useCallback(
    async(values: some[]) => {
      const blob = await dispatch(
        fetchThunk(
          API_PATHS.flightReportDownload,
          'post',
          JSON.stringify(values),
          true,
          undefined,
          true
        ),
      );

      const url = window.URL.createObjectURL(blob);
      const link = document.createElement('a');
      link.href = url;
      link.setAttribute(
        'download',
        `${intl.formatMessage({ id: 'report.tripReportExportName' })}${filter.from ? `_${filter.to}_` : '_'}${filter.to ? filter.to : moment().format(DATE_FORMAT_BACK_END)}.xlsx`
      );
      document.body.appendChild(link);
      link.click();
      link.parentNode?.removeChild(link);
    }, [dispatch, intl, filter.from, filter.to]
  )

  React.useEffect(() => {
    fetchData(filter, pagination);
  }, [fetchData, filter, pagination]);

  React.useEffect(() => {
    updateQueryParams();
  }, [updateQueryParams]);

  return (
    <TableBox
      data={data}
      loading={loading}
      filter={filter}
      pagination={pagination}
      onUpdateFilter={values => {
        dispatch(
          goToReplace(
            uniqueObject({
              search: queryString.stringify({
                ...pagination,
                page: 1,
                filters: queryString.stringify({
                  ...values,
                }),
              }),
            }),
          ),
        );
      }}
      onUpdatePagination={values => {
        dispatch(
          goToReplace(
            uniqueObject({
              search: queryString.stringify({
                ...values,
                filters: queryString.stringify({
                  ...filter,
                }),
              }),
            }),
          ),
        );
      }}
      onDownloadTripReport={onDownloadTripReport}
    />
  );
};

export default TripReport;
