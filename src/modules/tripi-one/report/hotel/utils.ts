import { SelectItem } from '../../../../models/object';

export interface IHotelReportFilter {
  bookingDate: number | null;
  from: number | null;
  to: number | null;
  checkinDate: number | null;
  checkoutDate: number | null;
  prn: string;
  orderCode: string;
  bookerName: string;
  departmentId: SelectItem | undefined;
  jobTitleId: SelectItem | undefined;
  status: SelectItem | null;
}

export const defaultHotelReportFilter: IHotelReportFilter = {
  bookingDate: null,
  from: null,
  to: null,
  checkinDate: null,
  checkoutDate: null,
  prn: '',
  orderCode: '',
  bookerName: '',
  departmentId: undefined,
  jobTitleId: undefined,
  status:  null,
}
