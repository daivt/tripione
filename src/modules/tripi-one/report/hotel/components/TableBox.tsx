import { Button, Typography } from '@material-ui/core';
import moment from 'moment';
import * as React from 'react';
import { FormattedMessage, FormattedNumber } from 'react-intl';
import { shallowEqual, useSelector } from 'react-redux';
import { some } from '../../../../../constants';
import { DATE_FORMAT, DATE_TIME_FORMAT_BREAK } from '../../../../../models/moment';
import { PaginationFilter } from '../../../../../models/pagination';
import { AppState } from '../../../../../redux/reducers';
import { ReactComponent as IconExport } from '../../../../../svg/ic_export_file.svg';
import { Row } from '../../../../common/components/elements';
import TableCustom, { Columns } from '../../../../common/components/TableCustom';
import { getAprrovalColorStatus } from '../../../tripManagement/utils';
import { IHotelReportFilter } from '../utils';
import Filter from './Filter';

interface Props {
  loading: boolean;
  data?: some;
  filter: IHotelReportFilter;
  pagination: PaginationFilter;
  onUpdateFilter(filter: IHotelReportFilter): void;
  onUpdatePagination(pagination: PaginationFilter): void;
  onDownloadHotelReport(data: some[]): void;
}

const TableBox: React.FunctionComponent<Props> = props => {
  const { loading, data, filter, pagination, onUpdateFilter, onUpdatePagination, onDownloadHotelReport } = props;
  const { tripApprovalStatusOptions } = useSelector(
    (state: AppState) => state.common,
    shallowEqual,
  );

  const columns = React.useMemo(() => {
    return [
      {
        title: 'report.bookedDate',
        width: 150,
        dataIndex: 'bookedDate',
        render: (record: some, index: number) => (
          <Typography variant="caption" style={{ whiteSpace: 'pre-wrap' }}>
            {moment(record.bookedDate).isValid()
              ? moment(record.bookedDate).format(DATE_TIME_FORMAT_BREAK)
              : null}
          </Typography>
        ),
      },
      {
        title: 'companyName',
        width: 150,
        dataIndex: 'companyName',
      },
      {
        title: 'report.bookingId',
        dataIndex: 'orderCode',
      },
      {
        title: 'report.prn',
        dataIndex: 'prn',
      },
      {
        title: 'report.hotelName',
        dataIndex: 'hotelName',
      },
      {
        title: 'report.bookerName',
        dataIndex: 'bookerName',
      },
      {
        title: 'checkinDate',
        dataIndex: 'checkinDate',
        render: (record: some, index: number) => (
          <Typography variant="caption">
            {moment(record.checkin).isValid()
              ? moment(record.checkin).format(DATE_FORMAT)
              : null}
          </Typography>
        ),
      },
      {
        title: 'checkoutDate',
        dataIndex: 'checkoutDate',
        render: (record: some, index: number) => (
          <Typography variant="caption">
            {moment(record.checkout).isValid()
              ? moment(record.checkout).format(DATE_FORMAT)
              : null}
          </Typography>
        ),
      },
      {
        title: 'position',
        dataIndex: 'jobTitleName',
      },
      {
        title: 'department',
        dataIndex: 'departmentName',
      },
      {
        title: 'report.tripApprovalStatus',
        render: (record: some, index: number) => (
          <Typography
            variant="caption"
            style={{ color: getAprrovalColorStatus(record.bookStatus) }}
          >
            <FormattedMessage
              id={
                tripApprovalStatusOptions?.find(
                  v => v.id === record.bookStatus,
                )?.name
              }
            />
          </Typography>
        ),
      },
      {
        title: 'report.paymentMethodFee',
        render: (record: some, index: number) => (
          <Typography variant="caption" className="price">
            <FormattedNumber value={record.totalPrice} />
            <FormattedMessage id="currency" />
          </Typography>
        ),
      },
      {
        title: 'paymentMethod',
        render: (record: some, index: number) => (
          <Typography variant="caption">{record.paymentMethod}</Typography>
        ),
      },
      {
        title: 'report.paymentDate',
        render: (record: some, index: number) => (
          <Typography variant="caption" style={{ whiteSpace: 'pre-wrap' }}>
            {moment(record.paymentTime).isValid()
              ? moment(record.paymentTime).format(DATE_TIME_FORMAT_BREAK)
              : null}
          </Typography>
        ),
      },
    ] as Columns[];
  }, [tripApprovalStatusOptions]);

  return (
    <div style={{ minHeight: 380 }}>
      <Filter loading={loading} filter={filter} onUpdateFilter={onUpdateFilter} />
      <TableCustom
        dataSource={data?.itemList}
        noColumnIndex
        loading={loading}
        columns={columns}
        style={{ marginTop: 24, whiteSpace: 'nowrap' }}
        header={
          <Row style={{ justifyContent: 'space-between', padding: '16px 12px' }}>
            <Typography variant="subtitle2" style={{ fontWeight: 500, marginRight: '24px' }}>
              <FormattedMessage id="report.reportTable" />
            </Typography>
            <Button
              disabled={data?.itemList.length === 0}
              variant="contained"
              color="secondary"
              style={{ minWidth: 148, marginRight: 12}}
              disableElevation
              onClick={() => onDownloadHotelReport(data?.itemList)}
            >
              <IconExport style={{ marginRight: 8 }} />
              <FormattedMessage id="report.export" />
            </Button>
          </Row>
        }
        paginationProps={{
          count: data?.totalResults || 0,
          page: pagination.page || 1,
          rowsPerPage: pagination?.pageSize || 0,
          onChangePage: (event: unknown, newPage: number) => {
            onUpdatePagination({ ...pagination, page: newPage });
          },
          onChangeRowsPerPage: (event: React.ChangeEvent<HTMLInputElement>) => {
            onUpdatePagination({
              pageSize: parseInt(event.target.value, 10),
              page: 1,
            });
          },
        }}
      />
    </div>
  );
};

export default TableBox;
