import { Button } from '@material-ui/core';
import { useFormik } from 'formik';
import moment, { Moment } from 'moment';
import React from 'react';
import { FormattedMessage, useIntl } from 'react-intl';
import { useSelector, shallowEqual } from 'react-redux';
import { ReactComponent as IconRefresh } from '../../../../../svg/ic_refresh.svg';
import DateField from '../../../../common/components/DateField';
import { Row } from '../../../../common/components/elements';
import LoadingButton from '../../../../common/components/LoadingButton';
import { SingleSelect } from '../../../../common/components/SingleSelect';
import { IHotelReportFilter, defaultHotelReportFilter } from '../utils';
import { AppState } from '../../../../../redux/reducers';
import FormControlTextField from '../../../../common/components/FormControlTextField';
import { trimObjectValues } from '../../../../utils';
import DateRangeFormControl from '../../../../common/components/DateRangeFormControl';

interface Props {
  filter: IHotelReportFilter;
  onUpdateFilter(filter: IHotelReportFilter): void;
  loading?: boolean;
}

const Filter: React.FunctionComponent<Props> = (props: Props) => {
  const { filter, onUpdateFilter, loading } = props;
  const { departmentOptions, positionOptions } = useSelector(
    (state: AppState) => state.common,
    shallowEqual,
  );
  const intl = useIntl();

  const formik = useFormik({
    initialValues: filter,
    onSubmit: values => onUpdateFilter(trimObjectValues(values)),
  });

  React.useEffect(() => {
    formik.setValues(filter);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [filter]);
  return (
    <form onSubmit={formik.handleSubmit} autoComplete="off">
      <Row style={{ flexWrap: 'wrap' }}>
        <DateRangeFormControl
          style={{ width: 220 }}
          label={intl.formatMessage({ id: 'dateRange' })}
          optional
          startDate={formik.values.from ? moment(formik.values.from) : undefined}
          endDate={formik.values.to ? moment(formik.values.to) : undefined}
          onChange={(start?: Moment, end?: Moment) => {
            formik.setFieldValue('from', start ? start.valueOf() : undefined);
            start && formik.setFieldValue('to', end ? end.valueOf() : undefined);
          }}
          numberOfMonths={1}
        />
        <FormControlTextField
          id="prn"
          label={<FormattedMessage id="report.prn" />}
          formControlStyle={{ width: 220 }}
          placeholder={intl.formatMessage({ id: 'report.insertPrn' })}
          value={formik.values.prn}
          onChange={formik.handleChange}
          optional
          inputProps={{
            maxLength: 100,
          }}
        />
        <FormControlTextField
          id="orderCode"
          label={<FormattedMessage id="report.bookingId" />}
          formControlStyle={{ width: 220 }}
          placeholder={intl.formatMessage({ id: 'report.insertBookingId' })}
          value={formik.values.orderCode}
          onChange={formik.handleChange}
          optional
          inputProps={{
            maxLength: 100,
          }}
        />
        <FormControlTextField
          id="bookerName"
          label={<FormattedMessage id="report.bookerName" />}
          formControlStyle={{ width: 220 }}
          placeholder={intl.formatMessage({ id: 'report.insertPassengerName' })}
          value={formik.values.bookerName}
          onChange={formik.handleChange}
          optional
          inputProps={{
            maxLength: 100,
          }}
        />
        <SingleSelect
          value={formik.values?.departmentId}
          label={<FormattedMessage id="department" />}
          formControlStyle={{ width: 220 }}
          onSelectOption={(value: any) => {
            formik.setFieldValue('departmentId', value);
          }}
          getOptionLabel={value => value.name}
          options={[
            { id: undefined, name: intl.formatMessage({ id: 'all' }) },
            ...departmentOptions,
          ]}
          optional
        />
        <SingleSelect
          value={formik.values?.jobTitleId}
          label={<FormattedMessage id="position" />}
          formControlStyle={{ width: 220 }}
          onSelectOption={(value: any) => {
            formik.setFieldValue('jobTitleId', value);
          }}
          getOptionLabel={value => value.name}
          options={[{ id: undefined, name: intl.formatMessage({ id: 'all' }) }, ...positionOptions]}
          optional
        />
        <Row style={{ marginTop: 4 }}>
          <LoadingButton
            type="submit"
            loading={loading}
            variant="contained"
            style={{ minWidth: 160, marginRight: 16 }}
            color="secondary"
            size="large"
            disableElevation
          >
            <FormattedMessage id="report.search" />
          </LoadingButton>
          <Button
            size="large"
            style={{ minWidth: 0 }}
            onClick={() => {
              formik.setValues(defaultHotelReportFilter);
              onUpdateFilter(defaultHotelReportFilter);
            }}
          >
            <IconRefresh />
          </Button>
        </Row>
      </Row>
    </form>
  );
};

export default Filter;
