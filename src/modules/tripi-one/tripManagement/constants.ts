export const INITIALIZE = 0;
export const SENT_REQUEST = 1;
export const APPROVING = 2;
export const APPROVED = 3;
export const REJECTED = 4;
export const PAYMENT_WAITING = 0;
export const PAYMENT_SUCCESS = 1;
export const PAYMENT_FAIL = 2;

