
import { GREY_400, GREY_600, GREEN, RED, BLACK, PINK } from '../../../configs/colors';

// Apply to payment status
export function getColorStatus(status: number) {
  switch (status) {
    case 1:
      return GREEN;
    case 2:
      return RED;
    default:
      return GREY_600;
  }
}

export function getAprrovalColorStatus(status: number) {
  switch (status) {
    case 0:
      return GREY_400;
    case 1:
      return GREY_600;
    case 2:
      return PINK;
    case 3:
      return GREEN;
    case 4:
      return RED;
    default:
      return BLACK;
  }
}

