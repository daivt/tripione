import { SelectItem } from '../../../../models/object';
import { some } from '../../../../constants';

export interface ITripManagementFilter {
  tripCode: string | undefined;
  createdDateFrom: number | undefined;
  createdDateTo: number | undefined;
  approvalStatus: SelectItem | undefined;
  paymentStatus: SelectItem | undefined;
  bookedBy: some | null;
  department: SelectItem | undefined;
  position: SelectItem | undefined;
}

export const defaultTripManagementFilter: ITripManagementFilter = {
  tripCode: undefined,
  createdDateFrom: undefined,
  createdDateTo: undefined,
  approvalStatus: undefined,
  paymentStatus: undefined,
  bookedBy: null,
  department: undefined,
  position: undefined,
};
