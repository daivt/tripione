import { debounce } from 'lodash';
import { useSnackbar } from 'notistack';
import queryString from 'query-string';
import * as React from 'react';
import { useDispatch } from 'react-redux';
import { useLocation } from 'react-router';
import { Action } from 'redux';
import { ThunkDispatch } from 'redux-thunk';
import { API_PATHS } from '../../../../../configs/API';
import { some, SUCCESS_CODE } from '../../../../../constants';
import { defaultPaginationFilter, PaginationFilter } from '../../../../../models/pagination';
import { AppState } from '../../../../../redux/reducers';
import { snackbarSetting } from '../../../../common/components/elements';
import { goToReplace } from '../../../../common/redux/reducer';
import { fetchThunk } from '../../../../common/redux/thunk';
import TableBox from '../components/TableBox';
import { defaultTripManagementFilter, ITripManagementFilter } from '../utils';
import { uniqueObject } from '../../../../utils';

interface Props {}

const TripManagementResult = (props: Props) => {
  const dispatch = useDispatch<ThunkDispatch<AppState, null, Action<string>>>();
  const [loading, setLoading] = React.useState(true);
  const [tripsData, setTripsData] = React.useState<some | undefined>(undefined);
  const [filter, setFilter] = React.useState<ITripManagementFilter>(defaultTripManagementFilter);
  const [pagination, setPagination] = React.useState<PaginationFilter>(defaultPaginationFilter);
  const { enqueueSnackbar, closeSnackbar } = useSnackbar();
  const location = useLocation();

  const updateQueryParams = React.useCallback(() => {
    if (location.search) {
      const filterParams = queryString.parse(location.search) as any;
      const filterTmp = queryString.parse(filterParams.filters) as any;
      const bookerBy = filterTmp.bookedBy && queryString.parse(filterTmp.bookedBy);
      const tmp = {
        ...filterTmp,
        bookedBy: bookerBy,
      };
      setFilter(uniqueObject(tmp) as any);
      const paginationTmp = {
        page: filterParams.page ? parseInt(`${filterParams.page}`, 10) : 1,
        pageSize: filterParams.pageSize ? parseInt(`${filterParams.pageSize}`, 10) : 10,
      } as PaginationFilter;
      setPagination(paginationTmp);
    } else {
      dispatch(
        goToReplace({
          search: queryString.stringify({
            ...defaultTripManagementFilter,
            filters: queryString.stringify({}),
          }),
        }),
      );
    }
  }, [dispatch, location.search]);

  const fetchData = React.useCallback(
    debounce(
      async (filterParams: ITripManagementFilter, paginationParams: PaginationFilter) => {
        setLoading(true);
        const json = await dispatch(
          fetchThunk(
            API_PATHS.tripsGeneral,
            'post',
            JSON.stringify({
              ...paginationParams,
              filters: { ...filterParams, bookedBy: filterParams?.bookedBy?.id },
            }),
          ),
        );
        if (json?.code === SUCCESS_CODE) {
          setTripsData(json.data);
        } else {
          json.message &&
            enqueueSnackbar(
              json.message,
              snackbarSetting(key => closeSnackbar(key), {
                color: 'error',
              }),
            );
        }
        setLoading(false);
      },
      300,
      {
        trailing: true,
        leading: false,
      },
    ),
    [],
  );

  React.useEffect(() => {
    updateQueryParams();
  }, [updateQueryParams]);

  React.useEffect(() => {
    fetchData(filter, pagination);
  }, [fetchData, filter, pagination]);

  return (
    <TableBox
      loading={loading}
      data={tripsData}
      filter={filter}
      onUpdateFilter={values => {
        dispatch(
          goToReplace(
            uniqueObject({
              search: queryString.stringify({
                ...pagination,
                page: 1,
                filters: queryString.stringify({
                  ...values,
                  bookedBy: values.bookedBy && queryString.stringify(values.bookedBy),
                }),
              }),
            }),
          ),
        );
      }}
      pagination={pagination}
      onUpdatePagination={values => {
        dispatch(
          goToReplace(
            uniqueObject({
              search: queryString.stringify({
                ...values,
                filters: queryString.stringify({
                  ...filter,
                  bookedBy: filter.bookedBy && queryString.stringify(filter.bookedBy),
                }),
              }),
            }),
          ),
        );
      }}
    />
  );
};

export default TripManagementResult;
