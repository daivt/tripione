import { Button, Dialog, Divider, IconButton, Typography } from '@material-ui/core';
import IconClose from '@material-ui/icons/CloseOutlined';
import { useFormik } from 'formik';
import { useSnackbar } from 'notistack';
import * as React from 'react';
import { FormattedMessage, useIntl } from 'react-intl';
import { useDispatch } from 'react-redux';
import { Action } from 'redux';
import { ThunkDispatch } from 'redux-thunk';
import * as yup from 'yup';
import { API_PATHS } from '../../../../../configs/API';
import { ROUTES } from '../../../../../configs/routes';
import { some, SUCCESS_CODE } from '../../../../../constants';
import { AppState } from '../../../../../redux/reducers';
import { Row, snackbarSetting } from '../../../../common/components/elements';
import FormControlTextField from '../../../../common/components/FormControlTextField';
import LoadingButton from '../../../../common/components/LoadingButton';
import { goToAction } from '../../../../common/redux/reducer';
import { fetchThunk } from '../../../../common/redux/thunk';

interface Props {
  open: boolean;
  onClose(): void;
}

const CreateTripDialog = (props: Props) => {
  const { open, onClose } = props;
  const dispatch = useDispatch<ThunkDispatch<AppState, null, Action<string>>>();
  const intl = useIntl();
  const [loading, setLoading] = React.useState(false);
  const { enqueueSnackbar, closeSnackbar } = useSnackbar();

  const createNewTrip = React.useCallback(
    async (obj: some) => {
      setLoading(true);
      const json = await dispatch(fetchThunk(API_PATHS.createTrip, 'post', JSON.stringify(obj)));
      if (json.code === SUCCESS_CODE) {
        dispatch(goToAction({ pathname: ROUTES.tripManagement.detail.gen(json.data.id) }));
        enqueueSnackbar(
          json.message,
          snackbarSetting(key => closeSnackbar(key), {
            color: 'success',
          }),
        );
      } else {
        enqueueSnackbar(
          json.message,
          snackbarSetting(key => closeSnackbar(key), {
            color: 'error',
          }),
        );
      }
      setLoading(false);
    },
    [closeSnackbar, dispatch, enqueueSnackbar],
  );

  const storeSchema = yup.object().shape({
    name: yup
      .string()
      .required(intl.formatMessage({ id: 'required' }))
      .trim(),
  });

  const formik = useFormik({
    initialValues: { name: '' },
    onSubmit: values => {
      createNewTrip(values);
    },
    validationSchema: storeSchema,
  });

  return (
    <>
      <Dialog
        open={open}
        onClose={onClose}
        PaperProps={{
          style: {
            minWidth: 420,
          },
        }}
        maxWidth="md"
        onEnter={() => formik.resetForm()}
      >
        <form onSubmit={formik.handleSubmit}>
          <Row style={{ paddingLeft: 16 }}>
            <Typography variant="subtitle1" style={{ flex: 1 }}>
              <FormattedMessage id="tripManagement.addNew" />
            </Typography>
            <IconButton onClick={onClose}>
              <IconClose />
            </IconButton>
          </Row>
          <Divider />
          <Row style={{ padding: 16 }}>
            <FormControlTextField
              id="name"
              label={<FormattedMessage id="tripManagement.tripName" />}
              placeholder={intl.formatMessage({ id: 'tripManagement.insertTripName' })}
              value={formik.values.name}
              onChange={formik.handleChange}
              formControlStyle={{ margin: 0 }}
              inputProps={{
                maxLength: 150,
              }}
              errorMessage={formik.submitCount ? formik.errors.name : undefined}
            />
          </Row>
          <Divider />
          <Row style={{ padding: 16, justifyContent: 'center' }}>
            <LoadingButton
              loading={loading}
              type="submit"
              variant="contained"
              color="secondary"
              size="large"
              style={{ minWidth: 144 }}
              disableElevation
            >
              <FormattedMessage id="accept" />
            </LoadingButton>
            <Button
              variant="outlined"
              size="large"
              style={{ minWidth: 144, marginLeft: 24 }}
              onClick={onClose}
              disableElevation
            >
              <FormattedMessage id="reject" />
            </Button>
          </Row>
        </form>
      </Dialog>
    </>
  );
};

export default CreateTripDialog;
