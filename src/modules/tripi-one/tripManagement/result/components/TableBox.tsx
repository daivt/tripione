import { Button, Typography } from '@material-ui/core';
import moment from 'moment';
import * as React from 'react';
import { FormattedMessage, FormattedNumber } from 'react-intl';
import { shallowEqual, useDispatch, useSelector } from 'react-redux';
import { Action } from 'redux';
import { ThunkDispatch } from 'redux-thunk';
import { GREY_600, ORANGE } from '../../../../../configs/colors';
import { ROUTES } from '../../../../../configs/routes';
import { some } from '../../../../../constants';
import { DATE_TIME_FORMAT_BREAK } from '../../../../../models/moment';
import { PaginationFilter } from '../../../../../models/pagination';
import { AppState } from '../../../../../redux/reducers';
import { Col, Row } from '../../../../common/components/elements';
import NoDataBox from '../../../../common/components/NoDataBox';
import PermissionDiv from '../../../../common/components/PermissionDiv';
import TableCustom, { Columns } from '../../../../common/components/TableCustom';
import { goToAction } from '../../../../common/redux/reducer';
import { getAprrovalColorStatus, getColorStatus } from '../../utils';
import { defaultTripManagementFilter, ITripManagementFilter } from '../utils';
import CreateTripDialog from './CreateTripDialog';
import Filter from './Filter';

interface Props {
  loading: boolean;
  data?: some;
  filter: ITripManagementFilter;
  pagination: PaginationFilter;
  onUpdateFilter(filter: ITripManagementFilter): void;
  onUpdatePagination(pagination: PaginationFilter): void;
}

const TableBox: React.FunctionComponent<Props> = props => {
  const dispatch = useDispatch<ThunkDispatch<AppState, null, Action<string>>>();
  const { loading, filter, onUpdateFilter, pagination, onUpdatePagination, data } = props;
  const { tripApprovalStatusOptions, paymentStatusOptions } = useSelector(
    (state: AppState) => state.common,
    shallowEqual,
  );
  const [open, setOpen] = React.useState(false);

  const columns = React.useMemo(() => {
    return [
      {
        title: 'tripManagement.tripCode',
        dataIndex: 'code',
        variant: 'caption',
      },
      {
        title: 'tripManagement.tripName',
        dataIndex: 'name',
        variant: 'caption',
      },
      {
        title: 'tripManagement.tripCreatedDate',
        dataIndex: 'createdDate',
        width: 120,
        render: (record: some, index: number) => (
          <Typography variant="caption">
            {moment(record.createdDate).isValid()
              ? moment(record.createdDate).format(DATE_TIME_FORMAT_BREAK)
              : null}
          </Typography>
        ),
      },
      {
        title: 'tripManagement.creatorName',
        dataIndex: 'creator',
        render: (record: some, index: number) => (
          <Col>
            <Typography variant="caption">{record.creator.name}</Typography>
            <Typography variant="caption" style={{ color: GREY_600 }}>
              {record.creator?.jobTitle?.name} - {record.creator?.department?.name}
            </Typography>
          </Col>
        ),
      },
      {
        title: 'tripManagement.tripSum',
        dataIndex: 'totalMoney',
        render: (record: some, index: number) => (
          <Typography variant="caption" style={{ color: ORANGE }}>
            <FormattedNumber value={record.totalPrice} />
            &nbsp;
            <FormattedMessage id="fullCurrency" />
          </Typography>
        ),
      },
      {
        title: 'tripManagement.tripApprovalStatus',
        dataIndex: 'status',
        render: (record: some, index: number) => (
          <Typography
            variant="caption"
            style={{ color: getAprrovalColorStatus(record.tripApprovalInfo.tripApprovalStatus) }}
          >
            <FormattedMessage
              id={
                tripApprovalStatusOptions?.find(
                  v => v.id === record.tripApprovalInfo.tripApprovalStatus,
                )?.name
              }
            />
          </Typography>
        ),
      },
      {
        title: 'paymentStatus',
        dataIndex: 'paymentStatus',
        variant: 'caption',
        render: (record: some, index: number) => {
          const name = paymentStatusOptions?.find(
            v => v.id === record.tripPaymentInfo.paymentStatus,
          )?.name;
          return (
            <Typography
              variant="caption"
              style={{ color: getColorStatus(record.tripPaymentInfo.paymentStatus) }}
            >
              {name && <FormattedMessage id={name || ' '} />}
            </Typography>
          );
        },
      },
    ] as Columns[];
  }, [paymentStatusOptions, tripApprovalStatusOptions]);

  if (
    data?.itemList &&
    data?.itemList?.length <= 0 &&
    filter === defaultTripManagementFilter &&
    pagination.page === 1
  ) {
    return (
      <>
        <NoDataBox
          message={<FormattedMessage id="tripManagement.noData" />}
          content={
            <PermissionDiv permission={['tripione:trip:create']}>
              <Button
                variant="contained"
                color="secondary"
                size="large"
                style={{ minWidth: 160, marginTop: 16 }}
                disableElevation
                onClick={() => setOpen(true)}
              >
                <FormattedMessage id="tripManagement.addNew" />
              </Button>
            </PermissionDiv>
          }
        />
        <CreateTripDialog open={open} onClose={() => setOpen(false)} />
      </>
    );
  }
  return (
    <>
      <Filter loading={loading} filter={filter} onUpdateFilter={onUpdateFilter} />
      <TableCustom
        dataSource={data?.itemList}
        loading={loading}
        columns={columns}
        style={{ marginTop: 24 }}
        onRowClick={(record: some) => {
          dispatch(
            goToAction({
              pathname: ROUTES.tripManagement.detail.gen(record.id),
            }),
          );
        }}
        header={
          <Row style={{ justifyContent: 'space-between', padding: '16px 12px' }}>
            <Typography variant="subtitle2">
              <FormattedMessage id="tripManagement.list" />
            </Typography>
            <PermissionDiv permission={['tripione:trip:create']}>
              <Button
                variant="contained"
                color="secondary"
                style={{ minWidth: 160, marginRight: 12 }}
                disableElevation
                onClick={() => setOpen(true)}
              >
                <FormattedMessage id="tripManagement.addNew" />
              </Button>
            </PermissionDiv>
          </Row>
        }
        paginationProps={{
          count: data?.totalResults || 0,
          page: pagination.page || 1,
          rowsPerPage: pagination?.pageSize || 0,
          onChangePage: (event: unknown, newPage: number) => {
            onUpdatePagination({ ...pagination, page: newPage });
          },
          onChangeRowsPerPage: (event: React.ChangeEvent<HTMLInputElement>) => {
            onUpdatePagination({
              pageSize: parseInt(event.target.value, 10),
              page: 1,
            });
          },
        }}
      />
      <CreateTripDialog open={open} onClose={() => setOpen(false)} />
    </>
  );
};

export default TableBox;
