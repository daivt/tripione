import { Button, Typography } from '@material-ui/core';
import { useFormik } from 'formik';
import moment, { Moment } from 'moment';
import React from 'react';
import { FormattedMessage, useIntl } from 'react-intl';
import { shallowEqual, useDispatch, useSelector } from 'react-redux';
import { Action } from 'redux';
import { ThunkDispatch } from 'redux-thunk';
import { API_PATHS } from '../../../../../configs/API';
import { some } from '../../../../../constants';
import { AppState } from '../../../../../redux/reducers';
import { ReactComponent as IconRefresh } from '../../../../../svg/ic_refresh.svg';
import { PAGE_SIZE_20 } from '../../../../booking/constants';
import DateRangeFormControl from '../../../../common/components/DateRangeFormControl';
import { Row } from '../../../../common/components/elements';
import FormControlAutoComplete from '../../../../common/components/FormControlAutoComplete';
import FormControlTextField from '../../../../common/components/FormControlTextField';
import LoadingButton from '../../../../common/components/LoadingButton';
import { SingleSelect } from '../../../../common/components/SingleSelect';
import { fetchThunk } from '../../../../common/redux/thunk';
import { defaultTripManagementFilter, ITripManagementFilter } from '../utils';
import { trimObjectValues } from '../../../../utils';

export function renderOption(option: some) {
  return (
    <div style={{ padding: '5px 16px', display: 'flex', alignItems: 'center' }}>
      <Typography variant="body2">
        {option?.name}
        {option.jobTitle?.name && `(${option.jobTitle.name})`}
      </Typography>
    </div>
  );
}

interface Props {
  filter: ITripManagementFilter;
  onUpdateFilter(filter: ITripManagementFilter): void;
  loading?: boolean;
}

const Filter: React.FunctionComponent<Props> = (props: Props) => {
  const dispatch = useDispatch<ThunkDispatch<AppState, null, Action<string>>>();
  const { filter, onUpdateFilter, loading } = props;
  const intl = useIntl();
  const {
    tripApprovalStatusOptions,
    paymentStatusOptions,
    departmentOptions,
    positionOptions,
  } = useSelector((state: AppState) => state.common, shallowEqual);

  const formik = useFormik({
    initialValues: filter,
    onSubmit: values =>
      onUpdateFilter({ ...values, tripCode: values.tripCode?.trim() || undefined }),
  });

  React.useEffect(() => {
    formik.setValues(filter);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [filter]);

  return (
    <form onSubmit={formik.handleSubmit} autoComplete="off">
      <Row style={{ flexWrap: 'wrap', maxWidth: 1300 }}>
        <FormControlTextField
          id="tripCode"
          label={<FormattedMessage id="tripManagement.tripCode" />}
          placeholder={intl.formatMessage({ id: 'tripManagement.insertTripCode' })}
          value={formik.values.tripCode}
          formControlStyle={{ width: 280 }}
          onChange={formik.handleChange}
          optional
          inputProps={{
            maxLength: '100',
          }}
        />
        <DateRangeFormControl
          style={{ width: 280 }}
          label={intl.formatMessage({ id: 'dateRange' })}
          optional
          startDate={
            formik.values.createdDateFrom ? moment(formik.values.createdDateFrom) : undefined
          }
          endDate={formik.values.createdDateTo ? moment(formik.values.createdDateTo) : undefined}
          onChange={(start?: Moment, end?: Moment) => {
            formik.setFieldValue('createdDateFrom', start ? start.valueOf() : undefined);
            start && formik.setFieldValue('createdDateTo', end ? end.valueOf() : undefined);
          }}
          numberOfMonths={1}
        />
        <SingleSelect
          value={formik.values?.approvalStatus}
          label={<FormattedMessage id="tripManagement.tripApprovalStatus" />}
          formControlStyle={{ width: 280 }}
          onSelectOption={(value: any) => {
            formik.setFieldValue('approvalStatus', value);
          }}
          getOptionLabel={value => intl.formatMessage({ id: value.name })}
          options={tripApprovalStatusOptions}
          optional
        />
        <SingleSelect
          value={formik.values?.paymentStatus}
          label={<FormattedMessage id="paymentStatus" />}
          formControlStyle={{ width: 280 }}
          onSelectOption={(value: any) => {
            formik.setFieldValue('paymentStatus', value);
          }}
          getOptionLabel={value => intl.formatMessage({ id: value.name })}
          options={paymentStatusOptions}
          optional
        />
        <FormControlAutoComplete
          label={<FormattedMessage id="tripManagement.creatorName" />}
          placeholder={intl.formatMessage({ id: 'tripManagement.insertCreatorName' })}
          value={formik.values.bookedBy || null}
          formControlStyle={{ width: 280 }}
          onChange={(e: any, value: some | null) => {
            formik.setFieldValue('bookedBy', value, true);
          }}
          loadOptions={async (str: string) => {
            const json = await dispatch(
              fetchThunk(
                API_PATHS.getListUser,
                'post',
                JSON.stringify({
                  filters: { str },
                  page: 1,
                  pageSize: PAGE_SIZE_20,
                }),
              ),
            );
            return json.data?.itemList.map((v: some) => {
              return {
                id: v.id,
                name: `${v?.name} ${`${v.jobTitle?.name ? `(${v.jobTitle.name})` : ''}`}` || '',
              };
            });
          }}
          getOptionSelected={(option: some, value: some) => {
            return option.id === value.id;
          }}
          getOptionLabel={(v: some) => v?.name || ''}
          filterOptions={(options, state) => options}
          options={[]}
          renderOption={renderOption}
          optional
        />

        <SingleSelect
          value={formik.values?.department}
          label={<FormattedMessage id="department" />}
          formControlStyle={{ width: 280 }}
          onSelectOption={(value: any) => {
            formik.setFieldValue('department', value);
          }}
          getOptionLabel={value => value.name}
          options={[
            { id: undefined, name: intl.formatMessage({ id: 'all' }) },
            ...departmentOptions,
          ]}
          optional
        />
        <SingleSelect
          value={formik.values?.position}
          label={<FormattedMessage id="position" />}
          formControlStyle={{ width: 280 }}
          onSelectOption={(value: any) => {
            formik.setFieldValue('position', value);
          }}
          getOptionLabel={value => value.name}
          options={[{ id: undefined, name: intl.formatMessage({ id: 'all' }) }, ...positionOptions]}
          optional
        />
        <Row style={{ marginTop: 4 }}>
          <LoadingButton
            type="submit"
            loading={loading}
            variant="contained"
            style={{ minWidth: 160, marginRight: 16 }}
            color="secondary"
            size="large"
            disableElevation
          >
            <FormattedMessage id="search" />
          </LoadingButton>
          <Button
            size="large"
            style={{ minWidth: 0 }}
            onClick={() => {
              formik.setValues(defaultTripManagementFilter);
              onUpdateFilter(defaultTripManagementFilter);
            }}
          >
            <IconRefresh />
          </Button>
        </Row>
      </Row>
    </form>
  );
};

export default Filter;
