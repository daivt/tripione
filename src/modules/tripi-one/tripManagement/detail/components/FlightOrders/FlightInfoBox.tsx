import { Button, Divider, Tooltip, Typography } from '@material-ui/core';
import ReportProblemOutlinedIcon from '@material-ui/icons/ReportProblemOutlined';
import { Alert } from '@material-ui/lab';
import moment from 'moment';
import * as React from 'react';
import { FormattedMessage, FormattedNumber } from 'react-intl';
import { RED, RED_50 } from '../../../../../../configs/colors';
import { some } from '../../../../../../constants';
import { DATE_TIME_ISOLATE_FORMAT } from '../../../../../../models/moment';
import DialogCustom from '../../../../../common/components/DialogCustom';
import { Col, Line, LineBT, Row } from '../../../../../common/components/elements';
import TableCustom, { Columns } from '../../../../../common/components/TableCustom';
import { StyledPaper } from '../../elements';
import FlightActions from './FlightActions';
import FlightItem from './FlightItem';

interface Props {
  data: some;
  setDeleteId?: (id: some) => void;
}

const FlightInfoBox = (props: Props) => {
  const { data, setDeleteId } = props;
  const [guestDetailModal, setGuestDetailModal] = React.useState(false);

  const columns = React.useMemo(() => {
    return [
      {
        title: 'guestName',
        dataIndex: 'name',
        variant: 'caption',
      },
      {
        title: 'position',
        dataIndex: 'jobTitle',
        render: (record: some, index: number) => (
          <Typography variant="caption">{record.jobTitle.name}</Typography>
        ),
      },
      {
        title: 'department',
        dataIndex: 'department',
        render: (record: some, index: number) => (
          <Typography variant="caption">{record.department.name}</Typography>
        ),
      },
      {
        title: 'tripManagement.outboundTicketPrice',
        dataIndex: 'ticketPrice',
        render: (record: some, index: number) => (
          <Typography
            variant="caption"
            style={{ color: record.overBudgetInbound ? RED : 'inherit' }}
          >
            <FormattedNumber value={record.inboundPrice} />
            &nbsp;
            <FormattedMessage id="currency" />
          </Typography>
        ),
      },
      {
        title: 'tripManagement.inboundTicketPrice',
        dataIndex: 'ticketPrice',
        render: (record: some, index: number) => (
          <Typography
            variant="caption"
            style={{ color: record.overBudgetOutbound ? RED : 'inherit' }}
          >
            <FormattedNumber value={record.outboundPrice} />
            &nbsp;
            <FormattedMessage id="currency" />
          </Typography>
        ),
      },
      {
        title: 'tripManagement.flightBudget',
        dataIndex: 'flightBudget',
        render: (record: some, index: number) => (
          <Typography variant="caption">
            <FormattedNumber value={record.flightBudget} />
            &nbsp;
            <FormattedMessage id="currency" />
          </Typography>
        ),
      },
      {
        title: 'amountSpentInMonth',
        dataIndex: 'amountSpentInMonth',
        render: (record: some, index: number) => (
          <Typography variant="caption">
            <FormattedNumber value={record.amountSpentInMonth} />
            &nbsp;
            <FormattedMessage id="currency" />
          </Typography>
        ),
      },
      {
        title: 'budgetPerMonth',
        dataIndex: 'budgetPerMonth',
        render: (record: some, index: number) => (
          <Typography variant="caption">
            <FormattedNumber value={record.budgetPerMonth} />
            &nbsp;
            <FormattedMessage id="currency" />
          </Typography>
        ),
      },
      {
        title: '',
        dataIndex: 'isOverBudget',
        render: (record: some, index: number) => (
          <Tooltip title={<FormattedMessage id="tripManagement.overBudget" />}>
            <Typography variant="caption">
              {(record.overBudgetInbound || record.overBudgetOutbound) && (
                <ReportProblemOutlinedIcon style={{ marginTop: 6, fontSize: 16, color: RED }} />
              )}
            </Typography>
          </Tooltip>
        ),
      },
    ] as Columns[];
  }, []);

  return (
    <StyledPaper style={{ flexDirection: 'column' }} variant="outlined">
      <FlightItem data={data} setDeleteId={setDeleteId} />
      <Divider style={{ margin: '18px 0' }} />
      <FlightActions />
      <Row style={{ alignItems: 'flex-start', paddingBottom: 16 }}>
        <Col
          style={{
            flex: 2,
            minWidth: '280px',
            padding: '0 16px',
          }}
        >
          <Typography variant="subtitle2" style={{ marginBottom: 24 }}>
            <FormattedMessage id="tripManagement.tripInfo" />
          </Typography>
          <Line>
            <Typography variant="body2" style={{ minWidth: 142, marginRight: 8 }}>
              <FormattedMessage id="tripManagement.tripCreatedDate" />
              :&nbsp;
            </Typography>
            <Typography variant="body2">
              {moment(data?.booker.bookedDate).format(DATE_TIME_ISOLATE_FORMAT)}
            </Typography>
          </Line>
          <Line>
            <Typography variant="body2" style={{ minWidth: 142, marginRight: 8 }}>
              <FormattedMessage id="tripManagement.orderId" />
              :&nbsp;
            </Typography>
            <Typography variant="body2">{data?.id}</Typography>
          </Line>
          <Line>
            <Typography variant="body2" style={{ minWidth: 142, marginRight: 8 }}>
              <FormattedMessage id="tripManagement.outboundPnrCodes" />
              :&nbsp;
            </Typography>
            <Typography variant="body2">{data?.outboundPnrCodes}</Typography>
          </Line>
          {data?.inboundPnrCodes.length > 0 && (
            <Line>
              <Typography variant="body2" style={{ minWidth: 142, marginRight: 8 }}>
                <FormattedMessage id="tripManagement.inboundPnrCodes" />
                :&nbsp;
              </Typography>
              <Typography variant="body2">{data?.inboundPnrCodes}</Typography>
            </Line>
          )}
          <Line>
            <Typography variant="body2" style={{ minWidth: 142, marginRight: 8 }}>
              <FormattedMessage id="tripManagement.numGuests" />
              :&nbsp;
            </Typography>
            <Typography variant="body2">
              {data?.numAdults}
              &nbsp;
              <FormattedMessage id="adult" />
            </Typography>
            {data?.numChildren > 0 && (
              <Typography variant="body2">
                ,&nbsp;{data?.numChildren}
                <FormattedMessage id="children" />
              </Typography>
            )}
          </Line>
          {data?.guests.length > 0 && (
            <Line style={{ marginLeft: 150 }}>
              <Button
                size="medium"
                variant="contained"
                color="secondary"
                disableElevation
                style={{ width: 148 }}
                onClick={() => setGuestDetailModal(true)}
              >
                <FormattedMessage id="tripManagement.guestDetail" />
              </Button>
            </Line>
          )}
        </Col>
        <Col style={{ flex: 1, paddingLeft: 16 }}>
          <Typography variant="subtitle2" style={{ marginBottom: 24 }}>
            <FormattedMessage id="tripManagement.paymentDetail" />
          </Typography>
          {data.overBudget && (
            <Alert
              severity="error"
              color="error"
              variant="outlined"
              style={{ background: RED_50, marginBottom: 8 }}
              icon={null}
            >
              <FormattedMessage id="tripManagement.overBudgetOrder" />
            </Alert>
          )}
          <LineBT>
            <Typography variant="body2" style={{ minWidth: 142, marginRight: 8 }}>
              <FormattedMessage id="tripManagement.outboundTicketPrice" />:
            </Typography>
            <Typography variant="body2">
              <FormattedNumber value={data?.paymentDetail.outboundTicketPrice} />
              &nbsp;
              <FormattedMessage id="currency" />
            </Typography>
          </LineBT>
          {data?.paymentDetail.inboundTicketPrice > 0 && (
            <LineBT>
              <Typography variant="body2" style={{ minWidth: 150, marginRight: 8 }}>
                <FormattedMessage id="tripManagement.inboundTicketPrice" />:
              </Typography>
              <Typography variant="body2">
                <FormattedNumber value={data?.paymentDetail.inboundTicketPrice} />
                &nbsp;
                <FormattedMessage id="currency" />
              </Typography>
            </LineBT>
          )}
          {data?.paymentDetail?.addonServiceFee?.baggageFee > 0 && (
            <LineBT>
              <Typography variant="body2" style={{ minWidth: 150, marginRight: 8 }}>
                <FormattedMessage id="tripManagement.addonServiceFee.baggageFee" />:
              </Typography>
              <Typography variant="body2">
                <FormattedNumber value={data?.paymentDetail?.addonServiceFee?.baggageFee} />
                &nbsp;
                <FormattedMessage id="currency" />
              </Typography>
            </LineBT>
          )}
          {data?.paymentDetail?.addonServiceFee?.seatFee && (
            <LineBT>
              <Typography variant="body2" style={{ minWidth: 150, marginRight: 8 }}>
                <FormattedMessage id="tripManagement.addonServiceFee.seatFee" />:
              </Typography>
              <Typography variant="body2">
                <FormattedNumber value={data?.paymentDetail?.addonServiceFee?.seatFee} />
                &nbsp;
                <FormattedMessage id="currency" />
              </Typography>
            </LineBT>
          )}
          <Divider style={{ marginBottom: 12 }} />
          <LineBT>
            <Typography variant="body2" style={{ minWidth: 150, marginRight: 8 }}>
              <FormattedMessage id="tripManagement.grandTotal" />
              :&nbsp;
            </Typography>
            <Typography variant="body2" className="price">
              <FormattedNumber value={data?.paymentDetail.grandTotal} />
              &nbsp;
              <FormattedMessage id="currency" />
            </Typography>
          </LineBT>
        </Col>
      </Row>
      <DialogCustom
        open={guestDetailModal}
        onClose={() => setGuestDetailModal(false)}
        titleLabel={
          <Typography variant="subtitle1">
            <FormattedMessage id="travellersList" />
          </Typography>
        }
        footerContent={<></>}
      >
        <TableCustom
          dataSource={data?.guests}
          columns={columns}
          style={{ boxShadow: 'none', paddingBottom: 32 }}
          noColumnIndex
        />
        <Divider />
        <Line style={{ paddingTop: 8, justifyContent: 'flex-end' }}>
          <Button
            size="medium"
            variant="contained"
            color="secondary"
            disableElevation
            style={{ margin: '8px 12px', width: '92px' }}
            onClick={() => setGuestDetailModal(false)}
          >
            <FormattedMessage id="close" />
          </Button>
        </Line>
      </DialogCustom>
    </StyledPaper>
  );
};

export default FlightInfoBox;
