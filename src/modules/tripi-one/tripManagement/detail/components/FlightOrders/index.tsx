import * as React from 'react';
import { some } from '../../../../../../constants';
import NoDataBox from '../../../../../common/components/NoDataBox';
import FlightInfoBox from './FlightInfoBox';

interface Props {
  data: some;
  setDeleteId?: (id?: some) => void;
}

const FlightOrders = (props: Props) => {
  const { data, setDeleteId } = props;

  if (data?.flightOrders?.length > 0) {
    return (
      <div>
        {data?.flightOrders?.map((item: some, index: number) => (
          <FlightInfoBox key={index} data={item} setDeleteId={setDeleteId} />
        ))}
      </div>
    );
  }

  return <NoDataBox />;
};

export default FlightOrders;
