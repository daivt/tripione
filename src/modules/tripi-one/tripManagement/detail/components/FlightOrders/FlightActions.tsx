import { Typography } from '@material-ui/core';
import * as React from 'react';
import { FormattedMessage } from 'react-intl';
import { ReactComponent as IconChangeTime } from '../../../../../../svg/booking/flight/ic_change_time.svg';
import { ReactComponent as IconFood } from '../../../../../../svg/booking/flight/ic_food.svg';
import { ReactComponent as IconLuggage } from '../../../../../../svg/booking/flight/ic_luggage.svg';
import { ReactComponent as IconSeat } from '../../../../../../svg/booking/flight/ic_seat.svg';
import { ReactComponent as IconSplitCode } from '../../../../../../svg/booking/flight/ic_split_code.svg';
import { ReactComponent as IconTicketBack } from '../../../../../../svg/booking/flight/ic_ticket_back.svg';
import { ChipButton, Row } from '../../../../../common/components/elements';

interface Props {}

const FlightActions = (props: Props) => {
  if (true) {
    return null;
  }
  return (
    <Row style={{ marginBottom: 16 }}>
      <ChipButton onClick={() => null}>
        <IconLuggage />
        <Typography variant="body2" style={{ marginLeft: 4 }}>
          <FormattedMessage id="tripManagement.addBaggage" />
        </Typography>
      </ChipButton>
      <ChipButton onClick={() => null}>
        <IconFood />
        <Typography variant="body2" style={{ marginLeft: 4 }}>
          <FormattedMessage id="tripManagement.buyFood" />
        </Typography>
      </ChipButton>
      <ChipButton onClick={() => null}>
        <IconSeat />
        <Typography variant="body2" style={{ marginLeft: 4 }}>
          <FormattedMessage id="tripManagement.chooseSeat" />
        </Typography>
      </ChipButton>
      <ChipButton onClick={() => null}>
        <IconChangeTime />
        <Typography variant="body2" style={{ marginLeft: 4 }}>
          <FormattedMessage id="tripManagement.changeTime" />
        </Typography>
      </ChipButton>
      <ChipButton onClick={() => null}>
        <IconSplitCode />
        <Typography variant="body2" style={{ marginLeft: 4 }}>
          <FormattedMessage id="tripManagement.splitCode" />
        </Typography>
      </ChipButton>
      <ChipButton onClick={() => null}>
        <IconTicketBack />
        <Typography variant="body2" style={{ marginLeft: 4 }}>
          <FormattedMessage id="tripManagement.ticketBack" />
        </Typography>
      </ChipButton>
    </Row>
  );
};

export default FlightActions;
