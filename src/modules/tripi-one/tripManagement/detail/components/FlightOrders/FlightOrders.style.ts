import styled from 'styled-components';
import { TEAL } from '../../../../../../configs/colors';

export const BoxLogoAirline = styled.div`
  display: flex;
  flex-flow: column;
  padding-top: 8px;
  width: 148px;
  text-align: center;
  align-items: center;
`;

export const BoxInfoFlight = styled.div`
  display: flex;
  align-items: start;
  flex-direction: column;
  padding-top: 8px;
  min-width: 225px;
  max-height: 160px;
  text-transform: capitalize;
`;

export const FlightRoute = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  margin: 4px 0;
  text-transform: uppercase;
  color: ${TEAL};
  font-size: 20px;
  font-weight: bold;
`;

export const RoundedLine = styled.div`
  width: 30px;
  height: 0;
  margin: 0 5px 0 8px;
  border-top: 1px solid ${TEAL};
  border-bottom: 1px solid ${TEAL};
  border-radius: 1px;
`;
