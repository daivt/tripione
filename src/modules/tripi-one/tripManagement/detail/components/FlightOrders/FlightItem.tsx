import { IconButton, Typography } from '@material-ui/core';
import moment from 'moment';
import * as React from 'react';
import { shallowEqual, useSelector } from 'react-redux';
import { some } from '../../../../../../constants';
import { DAY_DATE_ISOLATE, HOUR_MINUTE } from '../../../../../../models/moment';
import { AppState } from '../../../../../../redux/reducers';
import { ReactComponent as IconDelete } from '../../../../../../svg/ic_delete.svg';
import { ReactComponent as IconPlane } from '../../../../../../svg/ic_plane.svg';
import { Col, Row } from '../../../../../common/components/elements';
import { BoxInfoFlight, BoxLogoAirline, FlightRoute, RoundedLine } from './FlightOrders.style';

interface Props {
  data: some;
  setDeleteId?: (id: some) => void;
}

const FlightItem = (props: Props) => {
  const { data, setDeleteId } = props;
  const generalFlight = useSelector((state: AppState) => state.common.generalFlight, shallowEqual);

  const getFlightGeneralInfo = React.useCallback(
    (id: number) => {
      return generalFlight?.airlines?.find((v: some) => v.aid === id) || {};
    },
    [generalFlight],
  );

  return (
    <Row>
      <Col style={{ flex: 1 }}>
        <Row>
          {data?.outbound && (
            <Row>
              <BoxLogoAirline>
                <img
                  alt=""
                  style={{ height: '32px' }}
                  src={getFlightGeneralInfo(data?.outbound.airlineId).logo}
                />
                <Typography variant="body2">{data?.outbound.airlineName}</Typography>
                <Typography variant="body2">{data?.outbound.flightCode}</Typography>
                <Typography variant="body2">{data?.outbound.ticketClassName}</Typography>
              </BoxLogoAirline>
              <BoxInfoFlight>
                {moment(data?.outbound.departureDate).format(DAY_DATE_ISOLATE)}
                <FlightRoute>
                  {data?.outbound.fromAirport}
                  <RoundedLine />
                  <IconPlane />
                  <RoundedLine />
                  {data?.outbound.toAirport}
                </FlightRoute>
                <Row>
                  <Typography variant="body2">
                    {moment(data?.outbound.departureDate).format(HOUR_MINUTE)}
                  </Typography>
                  <Typography variant="body2" style={{ margin: '0 38px' }}>
                    {moment(data?.outbound.duration).format(HOUR_MINUTE)}
                  </Typography>
                  <Typography variant="body2">
                    {moment(data?.outbound.arrivalDate).format(HOUR_MINUTE)}
                    {moment(data?.outbound.departureDate).diff(
                      moment(data?.outbound.arriveDate),
                      'days',
                    ) > 0 &&
                      `+${moment(data?.outbound.departureDate).diff(
                        moment(data?.outbound.arriveDate),
                        'days',
                      )}d`}
                  </Typography>
                </Row>
              </BoxInfoFlight>
            </Row>
          )}
          {data?.inbound && (
            <Row>
              <BoxLogoAirline>
                <img
                  alt=""
                  style={{ height: '32px' }}
                  src={getFlightGeneralInfo(data?.inbound.airlineId).logo}
                />
                <Typography variant="body2">{data?.inbound.airlineName}</Typography>
                <Typography variant="body2">{data?.inbound.flightCode}</Typography>
                <Typography variant="body2">{data?.inbound.ticketClassName}</Typography>
              </BoxLogoAirline>
              <BoxInfoFlight>
                {moment(data?.inbound.departureDate).format(DAY_DATE_ISOLATE)}
                <FlightRoute>
                  {data?.inbound.fromAirport}
                  <RoundedLine />
                  <IconPlane />
                  <RoundedLine />
                  {data?.inbound.toAirport}
                </FlightRoute>
                <Row>
                  <Typography variant="body2">
                    {moment(data?.inbound.departureDate).format(HOUR_MINUTE)}
                  </Typography>
                  <Typography variant="body2" style={{ margin: '0 38px' }}>
                    {moment(data?.inbound.duration).format(HOUR_MINUTE)}
                  </Typography>
                  <Typography variant="body2">
                    {moment(data?.inbound.arrivalDate).format(HOUR_MINUTE)}
                    {moment(data?.inbound.departureDate).diff(
                      moment(data?.inbound.arriveDate),
                      'days',
                    ) > 0 &&
                      `+${moment(data?.inbound.departureDate).diff(
                        moment(data?.inbound.arriveDate),
                        'days',
                      )}d`}
                  </Typography>
                </Row>
              </BoxInfoFlight>
            </Row>
          )}
        </Row>
      </Col>
      <Row>
        {/* <Link to={{}}>
          <Button color="secondary" style={{ padding: '6px 12px' }}>
            <Typography variant="caption">
              <FormattedMessage id="tripManagement.flightDetail" />
            </Typography>
          </Button>
        </Link>
        <Button
          size="small"
          color="secondary"
          style={{ width: 22, padding: 0, margin: '0 16px' }}
          onClick={() => null}
        >
          <IconEdit />
        </Button> */}
        {setDeleteId && (
          <IconButton
            color="secondary"
            onClick={() => setDeleteId({ orderId: data.id, type: 'FLIGHT' })}
          >
            <IconDelete />
          </IconButton>
        )}
      </Row>
    </Row>
  );
};

export default FlightItem;
