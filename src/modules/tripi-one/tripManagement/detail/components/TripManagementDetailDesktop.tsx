import { Divider, Tab, Tabs } from '@material-ui/core';
import React from 'react';
import { useIntl } from 'react-intl';
import { useDispatch } from 'react-redux';
import { useLocation } from 'react-router';
import SwipeableViews from 'react-swipeable-views';
import { Action } from 'redux';
import { ThunkDispatch } from 'redux-thunk';
import { some } from '../../../../../constants';
import { AppState } from '../../../../../redux/reducers';
import { goToReplace } from '../../../../common/redux/reducer';
import FlightOrders from './FlightOrders';
import HotelOrders from './HotelOrders';
import General from './General';

interface Props {
  tripData: some;
  reload(): void;
  setDeleteId?: (id?: some) => void;
}

const TripManagementDetailDesktop = (props: Props) => {
  const { tripData, reload, setDeleteId } = props;
  const dispatch = useDispatch<ThunkDispatch<AppState, null, Action<string>>>();
  const location = useLocation();
  const intl = useIntl();
  const [tabValue, setTabValue] = React.useState(0);

  const getTabIndex = React.useMemo(() => {
    const search = new URLSearchParams(location.search);
    return Number(search.get('tabIndex')) || 0;
  }, [location.search]);

  React.useEffect(() => {
    setTabValue(getTabIndex);
  }, [getTabIndex]);

  return (
    <>
      <General tripData={tripData} reload={reload} />
      <Tabs
        value={tabValue}
        onChange={(e, val) => {
          const params = new URLSearchParams(location.search);
          params.set('tabIndex', val);
          dispatch(goToReplace({ search: params.toString() }));
        }}
        indicatorColor="primary"
        textColor="primary"
      >
        <Tab label={intl.formatMessage({ id: 'tripManagement.flight' })} />
        <Tab label={intl.formatMessage({ id: 'tripManagement.hotel' })} />
      </Tabs>
      <Divider />
      <SwipeableViews
        index={tabValue}
        style={{ marginTop: 16 }}
        onChangeIndex={val => setTabValue(val)}
      >
        <FlightOrders data={tripData} setDeleteId={setDeleteId} />
        <HotelOrders data={tripData} setDeleteId={setDeleteId} />
      </SwipeableViews>
    </>
  );
};

export default TripManagementDetailDesktop;
