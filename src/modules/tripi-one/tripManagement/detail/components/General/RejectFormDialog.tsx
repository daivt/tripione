import { Typography } from '@material-ui/core';
import { useFormik } from 'formik';
import * as React from 'react';
import { FormattedMessage, useIntl } from 'react-intl';
import * as yup from 'yup';
import ConfirmDialog from '../../../../../common/components/ConfirmDialog';
import { Col } from '../../../../../common/components/elements';
import FormControlTextField from '../../../../../common/components/FormControlTextField';

interface Props {
  open: boolean;
  onClose(): void;
  onSubmit(reason: string): void;
}

const RejectFormDialog = (props: Props) => {
  const { open, onClose, onSubmit } = props;
  const intl = useIntl();

  const storeSchema = yup.object().shape({
    reason: yup
      .string()
      .trim()
      .required(intl.formatMessage({ id: 'required' })),
  });

  const formik = useFormik({
    initialValues: { reason: '' },
    onSubmit: values => {
      onSubmit(values.reason);
    },
    validationSchema: storeSchema,
  });

  return (
    <ConfirmDialog
      titleLabel={
        <Typography variant="subtitle2" style={{ padding: '12px 16px' }}>
          <FormattedMessage id="tripManagement.rejectRequest" />
        </Typography>
      }
      open={open}
      onClose={() => {
        onClose();
      }}
      onAccept={() => {
        formik.submitForm();
      }}
      onReject={() => {
        onClose();
      }}
    >
      <Col style={{ padding: 16 }}>
        <Typography variant="body2" color="textSecondary" style={{ margin: '24px 0px' }}>
          <FormattedMessage id="tripManagement.rejectNote" />
        </Typography>
        <FormControlTextField
          id="reason"
          fullWidth
          formControlStyle={{ marginRight: 0 }}
          label={<FormattedMessage id="note" />}
          placeholder={intl.formatMessage({ id: 'enterNote' })}
          onChange={formik.handleChange}
          value={formik.values.reason}
          multiline
          rows={3}
          errorMessage={formik.submitCount > 0 ? formik.errors.reason : undefined}
        />
      </Col>
    </ConfirmDialog>
  );
};

export default RejectFormDialog;
