import { Typography } from '@material-ui/core';
import * as React from 'react';
import { FormattedMessage, FormattedNumber } from 'react-intl';
import { shallowEqual, useSelector } from 'react-redux';
import { GREEN, RED } from '../../../../../../configs/colors';
import { some } from '../../../../../../constants';
import { AppState } from '../../../../../../redux/reducers';
import { Col, Line } from '../../../../../common/components/elements';
import { PAYMENT_SUCCESS, PAYMENT_WAITING } from '../../../constants';
import { getAprrovalColorStatus as getApprovalColorStatus } from '../../../utils';

interface Props {
  tripData: some;
}

const GeneralInfo = (props: Props) => {
  const { tripData } = props;
  const { tripApprovalStatusOptions } = useSelector(
    (state: AppState) => state.common,
    shallowEqual,
  );
  const paymentStatus = tripData.tripPaymentInfo?.paymentStatus;

  return (
    <Col style={{ flex: 2 }}>
      <Line>
        <Typography variant="subtitle2" style={{ minWidth: 100, marginRight: 8 }}>
          <FormattedMessage id="tripManagement.tripCode" />
          :&nbsp;
        </Typography>
        <Typography variant="subtitle2">{tripData?.code}</Typography>
      </Line>
      <Line>
        <Typography variant="body2" style={{ minWidth: 100, marginRight: 8 }}>
          <FormattedMessage id="tripManagement.tripName" />
          :&nbsp;
        </Typography>
        <Typography variant="body2">{tripData?.name}</Typography>
      </Line>
      {tripData?.note && (
        <Line>
          <Typography variant="body2" style={{ minWidth: 100, marginRight: 8 }}>
            <FormattedMessage id="note" />
            :&nbsp;
          </Typography>
          <Typography variant="body2">{tripData.note}</Typography>
        </Line>
      )}
      <Line>
        <Typography variant="body2" style={{ minWidth: 100, marginRight: 8 }}>
          <FormattedMessage id="tripManagement.tripSum" />
          :&nbsp;
        </Typography>
        <Typography variant="body2" className="price">
          <FormattedNumber value={tripData?.totalPrice} />
          &nbsp;
          <FormattedMessage id="fullCurrency" />
        </Typography>
        {/* <ChipButton type="submit" style={{ minWidth: 100 }} onClick={() => null}>
          <FormattedMessage id="tripManagement.updatePrice" />
        </ChipButton> */}
      </Line>
      <Line>
        <Typography variant="body2" style={{ minWidth: 100, marginRight: 8 }}>
          <FormattedMessage id="status" />
          :&nbsp;
        </Typography>
        <Typography
          variant="body2"
          style={{
            color:
              paymentStatus === PAYMENT_WAITING
                ? getApprovalColorStatus(tripData.tripApprovalInfo.tripApprovalStatus)
                : paymentStatus === PAYMENT_SUCCESS
                ? GREEN
                : RED,
          }}
        >
          <FormattedMessage
            id={
              paymentStatus === PAYMENT_WAITING
                ? tripApprovalStatusOptions.find(
                    v => v.id === tripData.tripApprovalInfo.tripApprovalStatus,
                  )?.name
                : paymentStatus === PAYMENT_SUCCESS
                ? 'tripManagement.paySuccess'
                : 'tripManagement.payFail'
            }
          />
        </Typography>
      </Line>
    </Col>
  );
};

export default GeneralInfo;
