import { Button, Typography } from '@material-ui/core';
import { push } from 'connected-react-router';
import { useSnackbar } from 'notistack';
import * as React from 'react';
import { FormattedMessage } from 'react-intl';
import { useDispatch, useSelector } from 'react-redux';
import { Action } from 'redux';
import { ThunkDispatch } from 'redux-thunk';
import { API_PATHS } from '../../../../../../configs/API';
import { ROUTES } from '../../../../../../configs/routes';
import { some, SUCCESS_CODE } from '../../../../../../constants';
import { AppState } from '../../../../../../redux/reducers';
import ConfirmDialog from '../../../../../common/components/ConfirmDialog';
import { Row, snackbarSetting } from '../../../../../common/components/elements';
import PermissionDiv from '../../../../../common/components/PermissionDiv';
import { goToAction, goBackAction } from '../../../../../common/redux/reducer';
import { fetchThunk } from '../../../../../common/redux/thunk';
import { APPROVED, INITIALIZE, PAYMENT_SUCCESS, REJECTED, SENT_REQUEST } from '../../../constants';
import RejectFormDialog from './RejectFormDialog';

interface Props {
  tripData: some;
  reload(): void;
}

const GeneralActions = (props: Props) => {
  const { tripData, reload } = props;
  const dispatch = useDispatch<ThunkDispatch<AppState, null, Action<string>>>();
  const { enqueueSnackbar, closeSnackbar } = useSnackbar();
  const userData = useSelector((state: AppState) => state.account.userData);
  const [openOutBudget, setOpenOutBudget] = React.useState<string | undefined>(undefined);
  const [openReject, setOpenReject] = React.useState(false);

  const statusTrip = tripData.tripApprovalInfo?.tripApprovalStatus;
  const paymentStatus = tripData.tripPaymentInfo?.paymentStatus;
  const isPass = tripData.approvers.length === 0;

  const isApprove = React.useMemo(() => {
    let check = false;
    tripData.approvers.forEach((val: some) => {
      val.steps.forEach((one: some) => {
        one.userApprovals.forEach((v: some, i: number) => {
          if (userData?.id === v.id) {
            check = true;
          }
        });
      });
    });
    return check;
  }, [tripData.approvers, userData]);

  const isApproved = React.useMemo(() => {
    let check = false;
    tripData.approvers.forEach((val: some) => {
      val.steps.forEach((one: some) => {
        one.userApprovals.forEach((v: some, i: number) => {
          if (
            userData?.id === v.id &&
            (v.tripApprovalStatus === APPROVED || v.tripApprovalStatus === REJECTED)
          ) {
            check = true;
          }
        });
      });
    });
    return check;
  }, [tripData.approvers, userData]);

  const isCreator = React.useMemo(() => {
    return userData?.id === tripData.creator?.id;
  }, [tripData.creator, userData]);

  // Gửi yêu cầu phê duyệt
  const sendRequest = React.useCallback(async () => {
    const json = await dispatch(
      fetchThunk(API_PATHS.approvals, 'post', JSON.stringify({ id: tripData.id })),
    );
    if (json.code === SUCCESS_CODE) {
      reload();
    }
    enqueueSnackbar(
      json.message,
      snackbarSetting(key => closeSnackbar(key), {
        color: json.code === SUCCESS_CODE ? 'success' : 'error',
      }),
    );
  }, [closeSnackbar, dispatch, enqueueSnackbar, reload, tripData.id]);

  // Phê duyệt yêu cầu
  const acceptRequest = React.useCallback(async () => {
    const json = await dispatch(
      fetchThunk(API_PATHS.approved, 'post', JSON.stringify({ id: tripData.id })),
    );
    if (json.code === SUCCESS_CODE) {
      reload();
    }
    enqueueSnackbar(
      json.message,
      snackbarSetting(key => closeSnackbar(key), {
        color: json.code === SUCCESS_CODE ? 'success' : 'error',
      }),
    );
  }, [closeSnackbar, dispatch, enqueueSnackbar, reload, tripData.id]);

  // Từ chối phê duyệt chuyến đi
  const rejectRequest = React.useCallback(
    async (reason: string) => {
      const json = await dispatch(
        fetchThunk(API_PATHS.rejected, 'post', JSON.stringify({ id: tripData.id, reason })),
      );
      if (json.code === SUCCESS_CODE) {
        setOpenReject(false);
        reload();
      }
      enqueueSnackbar(
        json.message,
        snackbarSetting(key => closeSnackbar(key), {
          color: json.code === SUCCESS_CODE ? 'success' : 'error',
        }),
      );
    },
    [closeSnackbar, dispatch, enqueueSnackbar, reload, tripData.id],
  );

  // Hủy yêu cầu phê duyệt chuyến đi
  const cancelRequest = React.useCallback(async () => {
    const json = await dispatch(
      fetchThunk(API_PATHS.cancelled, 'delete', JSON.stringify({ id: tripData.id })),
    );
    if (json.code === SUCCESS_CODE) {
      reload();
    }
    enqueueSnackbar(
      json.message,
      snackbarSetting(key => closeSnackbar(key), {
        color: json.code === SUCCESS_CODE ? 'success' : 'error',
      }),
    );
  }, [closeSnackbar, dispatch, enqueueSnackbar, reload, tripData.id]);

  // Hủy chuyến đi
  const cancelTrip = React.useCallback(async () => {
    const json = await dispatch(
      fetchThunk(API_PATHS.cancellation, 'delete', JSON.stringify({ id: tripData.id })),
    );
    if (json.code === SUCCESS_CODE) {
      dispatch(goBackAction());
    }
    enqueueSnackbar(
      json.message,
      snackbarSetting(key => closeSnackbar(key), {
        color: json.code === SUCCESS_CODE ? 'success' : 'error',
      }),
    );
  }, [closeSnackbar, dispatch, enqueueSnackbar, tripData.id]);

  if (paymentStatus === PAYMENT_SUCCESS) {
    return null;
  }

  return (
    <Row style={{ flexWrap: 'wrap' }}>
      <PermissionDiv permission={['tripione:trip:pay']}>
        <Button
          variant="contained"
          color="secondary"
          style={{
            minWidth: 120,
            margin: '0px 12px 12px 0px',
            padding: '0 16px',
            whiteSpace: 'nowrap',
          }}
          disableElevation
          disabled={
            (statusTrip !== APPROVED && !isPass) ||
            (tripData.flightOrders?.length === 0 && tripData.hotelOrders?.length === 0)
          }
          size="large"
          onClick={() => {
            if (tripData.outOfBudget) {
              setOpenOutBudget('tripManagement.outBudgetWarningPay');
            } else {
              dispatch(
                goToAction({
                  pathname: ROUTES.booking.pay,
                  state: { tripiId: tripData.id, tripData, module: 'hotel' },
                }),
              );
            }
          }}
        >
          <FormattedMessage id="tripManagement.pay" />
        </Button>
      </PermissionDiv>
      {isCreator && (
        <>
          <>
            <PermissionDiv permission={['tripione:trip:approval:request']}>
              <Button
                variant="contained"
                color="secondary"
                style={{
                  minWidth: 120,
                  margin: '0px 12px 12px 0px',
                  padding: '0 16px',
                  whiteSpace: 'nowrap',
                }}
                disableElevation
                disabled={statusTrip !== INITIALIZE}
                size="large"
                onClick={() => {
                  sendRequest();
                }}
              >
                <FormattedMessage id="tripManagement.sendRequest" />
              </Button>
            </PermissionDiv>
            {statusTrip === SENT_REQUEST && (
              <PermissionDiv permission={['tripione:trip:approval:cancel']}>
                <Button
                  variant="contained"
                  color="secondary"
                  style={{
                    minWidth: 120,
                    margin: '0px 12px 12px 0px',
                    padding: '0 16px',
                    whiteSpace: 'nowrap',
                  }}
                  disableElevation
                  size="large"
                  onClick={() => cancelRequest()}
                >
                  <FormattedMessage id="tripManagement.cancelRequest" />
                </Button>
              </PermissionDiv>
            )}
          </>

          <PermissionDiv permission={['tripione:trip:book']}>
            <Button
              variant="contained"
              color="secondary"
              style={{
                minWidth: 120,
                margin: '0px 12px 12px 0px',
                padding: '0 16px',
                whiteSpace: 'nowrap',
              }}
              disableElevation
              disabled={statusTrip !== INITIALIZE && !isPass}
              size="large"
              onClick={
                () =>
                  dispatch(
                    push({
                      pathname: ROUTES.booking.hotel.default,
                      state: { tripiId: tripData.id, module: 'hotel' },
                    }),
                  )
                // Phai  de push lam de Breadcumbs dung
              }
            >
              <FormattedMessage id="tripManagement.addHotel" />
            </Button>
          </PermissionDiv>
          <PermissionDiv permission={['tripione:trip:book']}>
            <Button
              variant="contained"
              color="secondary"
              style={{
                minWidth: 120,
                margin: '0px 12px 12px 0px',
                padding: '0 16px',
                whiteSpace: 'nowrap',
              }}
              disableElevation
              disabled={statusTrip !== INITIALIZE && !isPass}
              size="large"
              onClick={
                () =>
                  dispatch(
                    push({
                      pathname: ROUTES.booking.flight.default,
                      state: { tripiId: tripData.id, module: 'flight' },
                    }),
                  )
                // Phai  de push lam de Breadcumbs dung
              }
            >
              <FormattedMessage id="tripManagement.addFlight" />
            </Button>
          </PermissionDiv>

          {(statusTrip === INITIALIZE || isPass) && (
            <PermissionDiv permission={['tripione:trip:cancellation']}>
              <Button
                variant="contained"
                color="secondary"
                style={{
                  minWidth: 120,
                  margin: '0px 12px 12px 0px',
                  padding: '0 16px',
                  whiteSpace: 'nowrap',
                }}
                disableElevation
                size="large"
                onClick={() => cancelTrip()}
              >
                <FormattedMessage id="tripManagement.cancelTrip" />
              </Button>
            </PermissionDiv>
          )}
        </>
      )}
      {isApprove && (
        <>
          <PermissionDiv permission={['tripione:trip:approval:approve']}>
            <Button
              variant="contained"
              color="secondary"
              style={{
                minWidth: 120,
                margin: '0px 12px 12px 0px',
                padding: '0 16px',
                whiteSpace: 'nowrap',
              }}
              disabled={isApproved || statusTrip >= APPROVED}
              disableElevation
              size="large"
              onClick={() => {
                if (tripData.outOfBudget) {
                  setOpenOutBudget('tripManagement.outBudgetWarningAccept');
                } else {
                  setOpenOutBudget('tripManagement.approvalConfirm');
                }
              }}
            >
              <FormattedMessage id="tripManagement.accept" />
            </Button>
          </PermissionDiv>

          <PermissionDiv permission={['tripione:trip:approval:reject']}>
            <Button
              variant="contained"
              color="secondary"
              style={{
                minWidth: 120,
                margin: '0px 12px 12px 0px',
                padding: '0 16px',
                whiteSpace: 'nowrap',
              }}
              disabled={isApproved || statusTrip >= APPROVED}
              disableElevation
              size="large"
              onClick={() => setOpenReject(true)}
            >
              <FormattedMessage id="tripManagement.reject" />
            </Button>
          </PermissionDiv>
          <RejectFormDialog
            open={openReject}
            onClose={() => {
              setOpenReject(false);
            }}
            onSubmit={rejectRequest}
          />
        </>
      )}
      <ConfirmDialog
        titleLabel={
          <Typography variant="subtitle2" style={{ padding: '12px 16px' }}>
            <FormattedMessage
              id={
                tripData.outOfBudget
                  ? 'tripManagement.outBudgetTitle'
                  : 'tripManagement.approvalTitle'
              }
            />
          </Typography>
        }
        open={!!openOutBudget}
        onClose={() => {
          setOpenOutBudget(undefined);
        }}
        onAccept={() => {
          if (openOutBudget === 'tripManagement.outBudgetWarningPay') {
            dispatch(
              goToAction({
                pathname: ROUTES.booking.pay,
                state: { tripiId: tripData.id, tripData, module: 'hotel' },
              }),
            );
          } else {
            acceptRequest();
          }
          setOpenOutBudget(undefined);
        }}
        onReject={() => {
          setOpenOutBudget(undefined);
        }}
      >
        <Typography
          variant="body2"
          color="textSecondary"
          style={{ padding: 16, minHeight: 100, textAlign: 'center', width: 450 }}
        >
          <FormattedMessage
            id={
              tripData.outOfBudget
                ? 'tripManagement.outBudgetWarningAccept'
                : 'tripManagement.approvalConfirm'
            }
          />
        </Typography>
      </ConfirmDialog>
    </Row>
  );
};

export default GeneralActions;
