import { Paper } from '@material-ui/core';
import * as React from 'react';
import { some } from '../../../../../../constants';
import { Col } from '../../../../../common/components/elements';
import GeneralActions from './GeneralActions';
import GeneralApprovalFlows from './GeneralApprovalFlows';
import GeneralInfo from './GeneralInfo';

interface Props {
  tripData: some;
  reload(): void;
}

const General = (props: Props) => {
  const { tripData, reload } = props;
  return (
    <Paper
      style={{
        display: 'flex',
        alignItems: 'flex-start',
        justifyContent: 'space-between',
        marginBottom: '16px',
        overflow: 'auto',
        padding: '12px 16px',
      }}
      variant="outlined"
    >
      <GeneralInfo tripData={tripData} />
      <Col style={{ flex: 3, marginLeft: 32 }}>
        <GeneralActions tripData={tripData} reload={reload} />
        <GeneralApprovalFlows tripData={tripData} />
      </Col>
    </Paper>
  );
};

export default General;
