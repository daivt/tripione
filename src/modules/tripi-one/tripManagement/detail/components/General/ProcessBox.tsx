import { Tooltip as TooltipRaw, Typography, withStyles } from '@material-ui/core';
import * as React from 'react';
import { FormattedMessage } from 'react-intl';
import { GREEN, GREY, GREY_500, ORANGE, RED } from '../../../../../../configs/colors';
import { some } from '../../../../../../constants';
import { ReactComponent as IconSuccess } from '../../../../../../svg/process/ic_check.svg';
import { ReactComponent as IconDeny } from '../../../../../../svg/process/ic_close.svg';
import { ReactComponent as IconPoint } from '../../../../../../svg/process/ic_pointStart.svg';
import { ReactComponent as IconPending } from '../../../../../../svg/process/ic_subtract.svg';
import { Col, Row } from '../../../../../common/components/elements';
import TableCustom, { Columns } from '../../../../../common/components/TableCustom';
import { APPROVED, APPROVING, REJECTED, SENT_REQUEST } from '../../../constants';

const Tooltip = withStyles(theme => ({
  tooltip: {
    ...theme.typography.body2,
    padding: 0,
    maxWidth: 'unset',
    border: `1px solid ${GREY_500}`,
    borderRadius: 8,
    minWidth: 434,
    background: theme.palette.common.white,
  },
}))(TooltipRaw);

interface Props {
  data: some;
}

const ProcessBox = (props: Props) => {
  const { data } = props;

  const columns = React.useMemo(() => {
    return [
      {
        title: 'tripManagement.approveList',
        dataIndex: 'name',
      },
      {
        title: 'tripManagement.jobTitle',
        dataIndex: 'jobTitle',
        render: (record: some, index: number) => (
          <Typography variant="caption" key={index}>
            {record?.jobTitle?.name}
          </Typography>
        ),
      },
      {
        title: 'status',
        dataIndex: 'tripApprovalStatus',
        styleHeader: { textAlign: 'center' },
        render: (record: some, index: number) => (
          <Row style={{ justifyContent: 'center' }}>
            {record.tripApprovalStatus === REJECTED ? (
              <IconDeny />
            ) : record.tripApprovalStatus === APPROVED ? (
              <IconSuccess />
            ) : record.tripApprovalStatus === SENT_REQUEST ? (
              <IconPending />
            ) : (
              <IconPoint />
            )}
          </Row>
        ),
      },
    ] as Columns[];
  }, []);

  const getStatus = React.useCallback((val: some) => {
    if (val.userApprovals.length > 0) {
      if (val.userApprovals.filter((v: some) => v.tripApprovalStatus === REJECTED).length > 0) {
        return REJECTED;
        // Từ chối
      }
      if (
        val.userApprovals.filter((v: some) => v.tripApprovalStatus === APPROVED).length ===
        val.userApprovals.length
      ) {
        return APPROVED;
        // Đã duyệt
      }
      if (
        val.userApprovals.filter((v: some) => v.tripApprovalStatus === SENT_REQUEST).length !==
          val.userApprovals.length &&
        val.userApprovals.filter((v: some) => v.tripApprovalStatus === SENT_REQUEST).length >= 1
      ) {
        return APPROVING;
        // Đang phê duyệt
      }
    }
    return 1;
    // Chờ phê duyệt
  }, []);

  const renderTitle = React.useCallback(
    (val: some) => {
      return (
        <div style={{ borderRadius: 8, padding: '12px 16px' }}>
          <TableCustom
            noColumnIndex
            dataSource={val.userApprovals}
            columns={columns}
            style={{ boxShadow: 'none', padding: 0 }}
            header={
              <Typography
                variant="body2"
                style={{
                  marginLeft: 16,
                  marginBottom: 8,
                  color:
                    getStatus(val) === REJECTED
                      ? RED
                      : getStatus(val) === APPROVED
                      ? GREEN
                      : getStatus(val) === APPROVING
                      ? ORANGE
                      : GREY,
                }}
              >
                <FormattedMessage
                  id={
                    getStatus(val) === REJECTED
                      ? 'tripManagement.reject'
                      : getStatus(val) === APPROVED
                      ? 'tripManagement.accepted'
                      : 'tripManagement.pending'
                  }
                />
              </Typography>
            }
          />
        </div>
      );
    },
    [columns, getStatus],
  );

  return (
    <Row style={{ flex: 1 }}>
      {data.steps.map((val: some, index: number) => {
        return (
          <Tooltip title={renderTitle(val)} key={val.id} arrow>
            <Col
              style={{
                width: 100,
                alignItems: 'center',
                justifyContent: 'center',
                position: 'relative',
                cursor: 'pointer',
              }}
            >
              {getStatus(val) === REJECTED ? (
                <IconDeny />
              ) : getStatus(val) === APPROVED ? (
                <IconSuccess />
              ) : getStatus(val) === APPROVING ? (
                <IconPending />
              ) : (
                <IconPoint />
              )}
              <Typography
                variant="body2"
                color={
                  getStatus(val) === APPROVING || getStatus(val) === 1 ? 'textSecondary' : undefined
                }
                style={{ marginTop: 8 }}
              >
                {val.name}
              </Typography>

              {index !== 0 && (
                <div
                  style={{
                    flex: 1,
                    position: 'absolute',
                    top: 12,
                    left: 'calc(-52% + 20px)',
                    right: 'calc(50% + 20px)',
                  }}
                >
                  <div
                    style={{
                      borderBottomWidth: 1,
                      borderBottomStyle: 'solid',
                      borderColor:
                        getStatus(val) === REJECTED
                          ? RED
                          : getStatus(val) === APPROVED
                          ? GREEN
                          : GREY_500,
                    }}
                  />
                </div>
              )}
            </Col>
          </Tooltip>
        );
      })}
    </Row>
  );
};

export default ProcessBox;
