import { Button, createStyles, makeStyles, Theme, Typography } from '@material-ui/core';
import * as React from 'react';
import { FormattedMessage } from 'react-intl';
import { BLUE } from '../../../../../../configs/colors';
import { some } from '../../../../../../constants';
import DialogCustom from '../../../../../common/components/DialogCustom';
import TableCustom, { Columns } from '../../../../../common/components/TableCustom';
import ProcessBox from './ProcessBox';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    paper: {
      minWidth: 500,
      marginBottom: 16,
      boxShadow: 'none',
      padding: 0,
    },
    cell: {
      background: 'unset !important',
      '&:hover': {
        background: 'unset !important',
      },
    },
  }),
);
interface Props {
  tripData: some;
}

const GeneralApprovalFlows = (props: Props) => {
  const { tripData } = props;
  const [tmp, setTmp] = React.useState<some | undefined>();
  const classes = useStyles();

  const getAllApproval = React.useCallback((record?: some) => {
    let array: some[] = [];
    record?.steps.forEach((e: some) => {
      array = array.concat(e.userApprovals);
    });
    return array.filter(v => v.note);
  }, []);

  const columns = React.useMemo(() => {
    return [
      {
        style: { border: 'unset', padding: 0, paddingBottom: 16 },
        dataIndex: 'approvers',
        render: (record: some, index: number) => <ProcessBox data={record} />,
      },
      {
        style: { border: 'unset', padding: 0, paddingBottom: 16 },
        dataIndex: 'approvers',
        render: (record: some, index: number) => (
          <>
            {getAllApproval(record).length > 0 && (
              <Button variant="text" style={{ color: BLUE }} onClick={() => setTmp(record)}>
                <FormattedMessage id="tripManagement.checkNote" />
              </Button>
            )}
          </>
        ),
      },
    ] as Columns[];
  }, [getAllApproval]);

  const columnsDetail = React.useMemo(() => {
    return [
      {
        title: 'tripManagement.creator',
        dataIndex: 'name',
        render: (record: some, index: number) => (
          <Typography variant="caption">{record.name}</Typography>
        ),
      },
      {
        title: 'tripManagement.jobTitle',
        dataIndex: 'jobTitle',
        render: (record: some, index: number) => (
          <>
            {record.jobTitle && (
              <Typography variant="caption">&nbsp;-&nbsp;{record.jobTitle.name}</Typography>
            )}
          </>
        ),
      },
      {
        title: 'tripManagement.note',
        dataIndex: 'note',
      },
    ] as Columns[];
  }, []);

  if (!tripData.approvers?.length) {
    return null;
  }

  return (
    <div>
      <Typography variant="subtitle2" style={{ marginTop: 12, marginBottom: 8 }}>
        <FormattedMessage id="tripManagement.tripApprovalInfo" />
      </Typography>
      <TableCustom
        className={classes}
        hiddenHeader
        dataSource={tripData.approvers}
        columns={columns}
        styleTable={{ width: 'unset' }}
        noColumnIndex
      />
      <DialogCustom
        open={!!tmp}
        onClose={() => {
          setTmp(undefined);
        }}
        buttonLabel="close"
        PaperProps={{ style: { width: 750 } }}
        titleLabel={
          <Typography variant="subtitle1">
            <FormattedMessage id="note" />
          </Typography>
        }
      >
        <TableCustom dataSource={getAllApproval(tmp)} columns={columnsDetail} noColumnIndex />
      </DialogCustom>
    </div>
  );
};

export default GeneralApprovalFlows;
