import * as React from 'react';
import { some } from '../../../../../../constants';
import HotelInfoBox from './HotelInfoBox';
import NoDataBox from '../../../../../common/components/NoDataBox';

interface Props {
  data: some;
  setDeleteId?: (id: some) => void;
}

const HotelOrders = (props: Props) => {
  const { data, setDeleteId } = props;

  if (data?.hotelOrders?.length > 0) {
    return (
      <div>
        {data?.hotelOrders?.map((item: some, index: number) => (
          <HotelInfoBox key={index} data={item} setDeleteId={setDeleteId} />
        ))}
      </div>
    );
  }
  return <NoDataBox />;
};

export default HotelOrders;
