import { Button, Divider, Tooltip, Typography } from '@material-ui/core';
import ReportProblemOutlinedIcon from '@material-ui/icons/ReportProblemOutlined';
import { Alert } from '@material-ui/lab';
import moment from 'moment';
import * as React from 'react';
import { FormattedMessage, FormattedNumber } from 'react-intl';
import { RED, RED_50 } from '../../../../../../configs/colors';
import { some } from '../../../../../../constants';
import { DATE_TIME_ISOLATE_FORMAT } from '../../../../../../models/moment';
import DialogCustom from '../../../../../common/components/DialogCustom';
import { Col, Line, LineBT } from '../../../../../common/components/elements';
import TableCustom, { Columns } from '../../../../../common/components/TableCustom';
import { StyledPaper } from '../../elements';
import HotelItem from './HotelItem';

interface Props {
  data: some;
  setDeleteId?: (id: some) => void;
}

const HotelInfoBox = (props: Props) => {
  const { data, setDeleteId } = props;
  const [guestDetailModal, setGuestDetailModal] = React.useState(false);

  const columns = React.useMemo(() => {
    return [
      {
        title: 'guestName',
        dataIndex: 'name',
        render: (record: some, index: number) => (
          <Typography variant="caption">{record?.name}</Typography>
        ),
      },
      {
        title: 'position',
        dataIndex: 'jobTitle',
        render: (record: some, index: number) => (
          <Typography variant="caption">{record?.jobTitleName}</Typography>
        ),
      },
      {
        title: 'department',
        dataIndex: 'department',
        render: (record: some, index: number) => (
          <Typography variant="caption">{record?.departmentName}</Typography>
        ),
      },
      {
        title: 'roomPrice',
        dataIndex: 'roomPrice',
        variant: 'caption',
        render: (record: some, index: number) => (
          <Typography variant="caption">
            <FormattedNumber value={record.roomPrice} />
            <FormattedMessage id="currency" />
          </Typography>
        ),
      },
      {
        title: 'tripManagement.hotelBudget',
        dataIndex: 'hotelBudget',
        render: (record: some, index: number) => (
          <Typography variant="caption" className="price">
            {record.hotelBudget === -1 ? (
              <FormattedMessage id="unlimited" />
            ) : (
              <>
                <FormattedNumber value={record.hotelBudget} />
                <FormattedMessage id="currency" />
              </>
            )}
          </Typography>
        ),
      },
      {
        title: 'amountSpentInMonth',
        dataIndex: 'amountSpentInMonth',
        render: (record: some, index: number) => (
          <Typography variant="caption">
            <FormattedNumber value={record.amountSpentInMonth} />
            <FormattedMessage id="currency" />
          </Typography>
        ),
      },
      {
        title: 'budgetPerMonth',
        dataIndex: 'budgetPerMonth',
        render: (record: some, index: number) => (
          <Typography variant="caption" className="price">
            {record.budgetPerMonth === -1 ? (
              <FormattedMessage id="unlimited" />
            ) : (
              <>
                <FormattedNumber value={record.budgetPerMonth} />
                <FormattedMessage id="currency" />
              </>
            )}
          </Typography>
        ),
      },
      {
        title: '',
        dataIndex: 'isOverBudget',
        render: (record: some, index: number) => (
          <Tooltip title={<FormattedMessage id="tripManagement.overBudget" />}>
            <Typography variant="caption">
              {record.isOverBudget && (
                <ReportProblemOutlinedIcon style={{ marginTop: 6, fontSize: 16, color: RED }} />
              )}
            </Typography>
          </Tooltip>
        ),
      },
    ] as Columns[];
  }, []);

  const isOverBudget = !!data.guests.find((one: some) => one.overBudget);

  return (
    <StyledPaper variant="outlined">
      <Col style={{ width: '100%' }}>
        <HotelItem data={data} setDeleteId={setDeleteId} />
        <Divider style={{ margin: '18px 0' }} />
        <Line style={{ alignItems: 'flex-start', paddingBottom: 16 }}>
          <Col
            style={{
              flex: 2,
              minWidth: 280,
              paddingRight: 16,
            }}
          >
            <Typography variant="subtitle2" style={{ marginBottom: 16 }}>
              <FormattedMessage id="tripManagement.tripInfo" />
            </Typography>
            <Line>
              <Typography variant="body2" style={{ minWidth: 142, marginRight: 8 }}>
                <FormattedMessage id="tripManagement.tripCreatedDate" />
                :&nbsp;
              </Typography>
              <Typography variant="body2">
                {moment(data?.bookedDate).format(DATE_TIME_ISOLATE_FORMAT)}
              </Typography>
            </Line>
            <Line>
              <Typography variant="body2" style={{ minWidth: 142, marginRight: 8 }}>
                <FormattedMessage id="tripManagement.orderId" />
                :&nbsp;
              </Typography>
              <Typography variant="body2">{data?.orderId}</Typography>
            </Line>
            <Line>
              <Typography variant="body2" style={{ minWidth: 142, marginRight: 8 }}>
                <FormattedMessage id="tripManagement.orderCode" />
                :&nbsp;
              </Typography>
              <Typography variant="body2">{data?.orderCode}</Typography>
            </Line>
            <Line>
              <Typography variant="body2" style={{ minWidth: 142, marginRight: 8 }}>
                <FormattedMessage id="tripManagement.roomTitle" />
                :&nbsp;
              </Typography>
              <Typography variant="body2">{data?.roomTitle}</Typography>
            </Line>
            <Line>
              <Typography variant="body2" style={{ minWidth: 142, marginRight: 8 }}>
                <FormattedMessage id="tripManagement.numRooms" />
                :&nbsp;
              </Typography>
              <Typography variant="body2">{data?.numRooms}</Typography>
            </Line>
            <Line>
              <Typography variant="body2" style={{ minWidth: 142, marginRight: 8 }}>
                <FormattedMessage id="tripManagement.numGuests" />
                :&nbsp;
              </Typography>
              <Typography variant="body2">{data?.numGuests}</Typography>
            </Line>
            {data?.guests.length > 0 && (
              <Line style={{ marginLeft: 150 }}>
                <Button
                  size="medium"
                  variant="contained"
                  color="secondary"
                  disableElevation
                  style={{ width: 148 }}
                  onClick={() => setGuestDetailModal(true)}
                >
                  <FormattedMessage id="tripManagement.guestDetail" />
                </Button>
              </Line>
            )}
          </Col>
          <Col style={{ flex: 1, paddingLeft: 16 }}>
            <Typography variant="subtitle2" style={{ marginBottom: 16 }}>
              <FormattedMessage id="tripManagement.paymentDetail" />
            </Typography>
            {isOverBudget && (
              <Alert
                severity="error"
                color="error"
                variant="outlined"
                style={{ background: RED_50, marginBottom: 8 }}
                icon={null}
              >
                <FormattedMessage id="tripManagement.overBudgetOrder" />
              </Alert>
            )}
            <LineBT>
              <Typography variant="body2" style={{ minWidth: 142, marginRight: 8 }}>
                <FormattedMessage id="tripManagement.totalRoomPrice" />
                :&nbsp;
              </Typography>
              <Typography variant="body2">
                <FormattedNumber value={data?.totalRoomPrice} />
                &nbsp;
                <FormattedMessage id="currency" />
              </Typography>
            </LineBT>
            {/* <LineBT>
              <Typography variant="body2" style={{ minWidth: 150, marginRight: 8 }}>
                <FormattedMessage id="tripManagement.childrenSurcharge1yo" />
                :&nbsp;
              </Typography>
              <Typography variant="body2">
                <FormattedNumber value={data?.childrenSurcharge1yo} /> &nbsp;
                <FormattedMessage id="currency" />
              </Typography>
            </LineBT> */}
            {data?.totalSurcharge > 0 && (
              <LineBT>
                <Typography variant="body2" style={{ minWidth: 150, marginRight: 8 }}>
                  <FormattedMessage id="tripManagement.paymentFixedFeeSide" />
                  :&nbsp;
                </Typography>
                <Typography variant="body2">
                  <FormattedNumber value={data?.totalSurcharge} /> &nbsp;
                  <FormattedMessage id="currency" />
                </Typography>
              </LineBT>
            )}
            <Divider style={{ marginBottom: 8, marginTop: 8 }} />
            <LineBT>
              <Typography variant="body2" style={{ minWidth: 150, marginRight: 8 }}>
                <FormattedMessage id="tripManagement.grandTotal" />
                :&nbsp;
              </Typography>
              <Typography variant="subtitle1" className="price">
                <FormattedNumber value={data?.totalPayment} />
                &nbsp;
                <FormattedMessage id="currency" />
              </Typography>
            </LineBT>
          </Col>
        </Line>
      </Col>
      <DialogCustom
        open={guestDetailModal}
        onClose={() => setGuestDetailModal(false)}
        titleLabel={
          <Typography variant="subtitle1">
            <FormattedMessage id="travellersList" />
          </Typography>
        }
        footerContent={<></>}
      >
        <TableCustom
          dataSource={data?.guests}
          columns={columns}
          style={{ boxShadow: 'none', paddingBottom: 32 }}
          noColumnIndex
        />
        <Divider />
        <Line style={{ paddingTop: 8, justifyContent: 'flex-end' }}>
          <Button
            size="medium"
            variant="contained"
            color="secondary"
            disableElevation
            style={{ margin: '8px 12px', width: '92px' }}
            onClick={() => setGuestDetailModal(false)}
          >
            <FormattedMessage id="close" />
          </Button>
        </Line>
      </DialogCustom>
    </StyledPaper>
  );
};

export default HotelInfoBox;
