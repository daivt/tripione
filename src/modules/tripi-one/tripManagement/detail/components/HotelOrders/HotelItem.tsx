import { IconButton, Typography } from '@material-ui/core';
import LocationOnIcon from '@material-ui/icons/LocationOn';
import StarRoundedIcon from '@material-ui/icons/StarRounded';
import { Rating } from '@material-ui/lab';
import moment from 'moment';
import * as React from 'react';
import { FormattedMessage } from 'react-intl';
import { GREY_700 } from '../../../../../../configs/colors';
import { some } from '../../../../../../constants';
import { DATE_FORMAT } from '../../../../../../models/moment';
import { ReactComponent as IconDelete } from '../../../../../../svg/ic_delete.svg';
import { ReactComponent as IconLogin } from '../../../../../../svg/ic_login.svg';
import { ReactComponent as IconLogout } from '../../../../../../svg/ic_logout.svg';
import { ReactComponent as IconMoon } from '../../../../../../svg/ic_moon.svg';
import { Col, Line, Row } from '../../../../../common/components/elements';

interface Props {
  data: some;
  setDeleteId?: (id: some) => void;
}

const HotelItem = (props: Props) => {
  const { data, setDeleteId } = props;

  return (
    <Row>
      <Col style={{ flex: 1 }}>
        <Line>
          <Typography variant="subtitle1">{data?.hotelName}</Typography>
          <Rating
            name="maxStars"
            value={data?.hotelStar}
            readOnly
            size="small"
            icon={<StarRoundedIcon fontSize="small" />}
            style={{ marginLeft: 16 }}
          />
        </Line>
        <Line style={{ color: GREY_700, marginBottom: 6 }}>
          <LocationOnIcon style={{ fontSize: 16, marginRight: 8 }} />
          <Typography variant="caption">{data?.hotelAddress}</Typography>
        </Line>
        <Line>
          <IconLogin style={{ width: '16px', height: 'auto', marginRight: 8 }} />
          <Typography variant="body2">
            <FormattedMessage id="checkinDate" />
            :&nbsp;
            {data?.checkinDate && moment(data?.checkinDate).format(DATE_FORMAT)}
          </Typography>
          <IconLogout style={{ width: '16px', height: 'auto', margin: '0 8px 0 40px' }} />
          <Typography variant="body2">
            <FormattedMessage id="checkoutDate" />
            :&nbsp;
            {data?.checkoutDate && moment(data?.checkoutDate).format(DATE_FORMAT)}
          </Typography>
          <IconMoon style={{ width: '16px', height: 'auto', margin: '0 8px 0 40px' }} />
          <Typography variant="body2">
            <FormattedMessage id="tripManagement.numNights" />
            :&nbsp;
            {data?.numNights}
          </Typography>
        </Line>
      </Col>
      <Line>
        {/* <Button
          size="small"
          color="secondary"
          style={{ width: 22, padding: 0, marginRight: 18 }}
          onClick={() => null}
        >
          <IconEdit />
        </Button> */}
        {setDeleteId && (
          <IconButton
            color="secondary"
            onClick={() => setDeleteId({ orderId: data.orderId, type: 'HOTEL' })}
          >
            <IconDelete />
          </IconButton>
        )}
      </Line>
    </Row>
  );
};

export default HotelItem;
