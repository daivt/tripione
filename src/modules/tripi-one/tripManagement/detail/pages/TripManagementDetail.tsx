import { useSnackbar } from 'notistack';
import React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { RouteComponentProps } from 'react-router';
import { Action } from 'redux';
import { ThunkDispatch } from 'redux-thunk';
import { Typography } from '@material-ui/core';
import { FormattedMessage } from 'react-intl';
import { API_PATHS } from '../../../../../configs/API';
import { some, SUCCESS_CODE } from '../../../../../constants';
import { AppState } from '../../../../../redux/reducers';
import { snackbarSetting } from '../../../../common/components/elements';
import LoadingIcon from '../../../../common/components/LoadingIcon';
import NoDataBox from '../../../../common/components/NoDataBox';
import RedirectDiv from '../../../../common/components/RedirectDiv';
import { fetchThunk } from '../../../../common/redux/thunk';
import TripManagementDetailDesktop from '../components/TripManagementDetailDesktop';
import { INITIALIZE } from '../../constants';
import ConfirmDialog from '../../../../common/components/ConfirmDialog';

interface Props extends RouteComponentProps<{ tripId: string }> {}

const TripManagementDetail = (props: Props) => {
  const { match } = props;
  const userData = useSelector((state: AppState) => state.account.userData);

  const { enqueueSnackbar, closeSnackbar } = useSnackbar();
  const { tripId } = match.params;
  const dispatch = useDispatch<ThunkDispatch<AppState, null, Action<string>>>();
  const [loading, setLoading] = React.useState(true);
  const [loadingDelete, setLoadingDelete] = React.useState(false);
  const [tripData, setTripData] = React.useState<some | undefined>(undefined);
  const [reload, setReload] = React.useState(false);
  const [deleteId, setDeleteId] = React.useState<some | undefined>();

  const isCreator = React.useMemo(() => {
    return (
      userData?.id === tripData?.creator?.id &&
      (tripData?.tripApprovalInfo?.tripApprovalStatus === INITIALIZE ||
        tripData?.approvers?.length === 0)
    );
  }, [tripData, userData]);

  const fetchData = React.useCallback(
    async (id: string) => {
      if (!Number.isInteger(Number(tripId))) {
        return;
      }
      setLoading(true);
      const json = await dispatch(fetchThunk(API_PATHS.tripsDetail(Number(tripId)), 'get'));
      if (json?.code === SUCCESS_CODE) {
        setTripData(json.data);
      } else {
        enqueueSnackbar(
          json.message,
          snackbarSetting(key => closeSnackbar(key), { color: 'error' }),
        );
      }
      setLoading(false);
    },
    [closeSnackbar, dispatch, enqueueSnackbar, tripId],
  );

  const deleteBooking = React.useCallback(async () => {
    if (!deleteId || !tripData) {
      return;
    }
    setLoadingDelete(true);
    const json = await dispatch(
      fetchThunk(
        API_PATHS.getBooking,
        'delete',
        JSON.stringify({
          bookingType: deleteId?.type,
          orderId: deleteId?.orderId,
          tripId: tripData?.id,
        }),
      ),
    );
    if (json?.code === SUCCESS_CODE) {
      setTripData(one =>
        one
          ? {
              ...one,
              flightOrders:
                deleteId?.type === 'FLIGHT'
                  ? one.flightOrders.filter((v: some) => v.id !== deleteId?.orderId)
                  : one.flightOrders,
              hotelOrders:
                deleteId?.type === 'HOTEL'
                  ? one.hotelOrders.filter((v: some) => v.orderId !== deleteId?.orderId)
                  : one.hotelOrders,
            }
          : one,
      );
      setDeleteId(undefined);
    } else {
      enqueueSnackbar(
        json.message,
        snackbarSetting(key => closeSnackbar(key), { color: 'error' }),
      );
    }
    setLoadingDelete(false);
  }, [closeSnackbar, deleteId, dispatch, enqueueSnackbar, tripData]);

  React.useEffect(() => {
    fetchData(tripId);
  }, [fetchData, tripId, reload]);

  if (!Number.isInteger(Number(tripId))) {
    return <RedirectDiv />;
  }

  if (loading) {
    return <LoadingIcon />;
  }

  if (!tripData) {
    return <NoDataBox />;
  }

  return (
    <>
      <TripManagementDetailDesktop
        tripData={tripData}
        reload={() => setReload(!reload)}
        setDeleteId={isCreator ? value => setDeleteId(value) : undefined}
      />
      <ConfirmDialog
        titleLabel={
          <Typography variant="h5" style={{ margin: '12px 16px' }}>
            <FormattedMessage id="attention" />
          </Typography>
        }
        open={!!deleteId}
        rejectLabel="cancel"
        acceptLabel="confirm"
        onClose={() => setDeleteId(undefined)}
        onReject={() => setDeleteId(undefined)}
        onAccept={() => {
          deleteBooking();
        }}
        loading={loadingDelete}
      >
        <div style={{ textAlign: 'center', padding: '24px 16px', minHeight: 120 }}>
          <Typography variant="body1">
            <FormattedMessage id="tripManagement.confirmDelete" />
          </Typography>
        </div>
      </ConfirmDialog>
    </>
  );
};

export default TripManagementDetail;
