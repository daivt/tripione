import { withStyles, Paper } from '@material-ui/core';
import { RED_50, RED } from '../../../../configs/colors';

export const StyledPaper = withStyles(theme => ({
  root: {
    display: 'flex',
    marginBottom: '16px',
    overflow: 'auto',
    padding: '12px 16px',
  },
}))(Paper);

export const Alert = withStyles(theme => ({
  root: {
    marginBottom: '24px',
    overflow: 'hidden',
    padding: '14px 24px',
    boxShadow: 'none',
    background: RED_50,
    color: RED,
    border: `1px solid ${RED}`,
  },
}))(Paper);
