import { useSnackbar } from 'notistack';
import React from 'react';
import { useIntl } from 'react-intl';
import { shallowEqual, useDispatch, useSelector } from 'react-redux';
import { useLocation } from 'react-router';
import { Action } from 'redux';
import { ThunkDispatch } from 'redux-thunk';
import { API_PATHS } from '../../../../../configs/API';
import { some, SUCCESS_CODE } from '../../../../../constants';
import { AppState } from '../../../../../redux/reducers';
import { snackbarSetting } from '../../../../common/components/elements';
import RedirectDiv from '../../../../common/components/RedirectDiv';
import { goBackAction } from '../../../../common/redux/reducer';
import { fetchThunk } from '../../../../common/redux/thunk';
import TripPayDesktop from '../components/TripPayDesktop';
import { defaultTripParams, TripParams } from '../utils';
import CheckStatusDialog from '../components/CheckStatusDialog';

interface Props {}

const TripPay: React.FC<Props> = props => {
  const dispatch = useDispatch<ThunkDispatch<AppState, null, Action<string>>>();
  const intl = useIntl();
  const location = useLocation();
  const { enqueueSnackbar, closeSnackbar } = useSnackbar();
  const [params, setParams] = React.useState<TripParams>(defaultTripParams);
  const [paymentMethods, setPayment] = React.useState<some[]>([]);
  const [loading, setLoading] = React.useState(false);
  const [paying, setPaying] = React.useState(false);
  const [transactionId, setTransactionId] = React.useState<number | undefined>(undefined);

  const userData = useSelector((state: AppState) => state.account.userData, shallowEqual);

  const getActionData = React.useMemo(() => {
    const state = location.state as some;
    const data = state && (state.tripData as some);
    return data;
  }, [location.state]);

  const fetchPaymentMethod = React.useCallback(async () => {
    setLoading(true);
    const json = await dispatch(
      fetchThunk(
        API_PATHS.getPaymentMethods,
        'post',
        JSON.stringify({
          bookingCode: `TO${getActionData.id}`,
        }),
      ),
    );
    if (json.code === 200 || json.code === '200') {
      setPayment(json.data.items);
      json.data.items[0] &&
        setParams(one => ({ ...one, selectedPaymentMethod: json.data.items[0] }));
    } else if (json.code === 400 || json.code === '400') {
      dispatch(goBackAction());
    }
    setLoading(false);
  }, [dispatch, getActionData.id]);

  const tripPay = React.useCallback(async () => {
    if (!params.selectedPaymentMethod || !userData) {
      return;
    }
    const { selectedPaymentMethod } = params;
    setPaying(true);
    const json = await dispatch(
      fetchThunk(
        API_PATHS.payTrip,
        'post',
        JSON.stringify({
          bookingCode: `TO${getActionData.id}`,
          paymentMethodId: selectedPaymentMethod.id,
          paymentMethodBankId: selectedPaymentMethod.bankId,
        }),
      ),
    );
    setPaying(false);

    if (json.code === SUCCESS_CODE || json.code === '200') {
      await new Promise(resolve => setTimeout(resolve, 2000));
      enqueueSnackbar(
        intl.formatMessage({ id: 'tripManagement.paySuccess' }),
        snackbarSetting(key => closeSnackbar(key)),
      );
      // console.log(json);
      // dispatch(goBackAction());
      setTransactionId(json.data.transactionId);
    } else {
      enqueueSnackbar(
        json.message,
        snackbarSetting(key => closeSnackbar(key), { color: 'error' }),
      );
    }
  }, [closeSnackbar, dispatch, enqueueSnackbar, getActionData, intl, params, userData]);

  React.useEffect(() => {
    fetchPaymentMethod();
  }, [fetchPaymentMethod]);

  if (!getActionData) {
    return <RedirectDiv />;
  }
  return (
    <>
      <TripPayDesktop
        paymentMethods={paymentMethods}
        params={params}
        setParams={value => setParams(value)}
        tripPay={tripPay}
        loading={loading}
        paying={paying}
        tripData={getActionData}
      />
      <CheckStatusDialog
        transactionId={transactionId}
        onClose={() => setTransactionId(undefined)}
      />
    </>
  );
};

export default TripPay;
