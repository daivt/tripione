import { Container, Typography, useTheme, useMediaQuery } from '@material-ui/core';
import * as React from 'react';
import { FormattedMessage } from 'react-intl';
import { some } from '../../../../../constants';
import LoadingIcon from '../../../../common/components/LoadingIcon';
import { TripParams } from '../utils';
import TripPayBox from './TripPayBox';
import TripiInfoBox from './TripiInfoBox';
import AsideBound from '../../../../booking/common/AsideBound';

export interface Props {
  tripData: some;
  paymentMethods: some[];
  params: TripParams;
  setParams(params: TripParams): void;
  tripPay(smartOTPPass?: string): void;
  loading: boolean;
  paying: boolean;
}

const TripPayDesktop: React.FC<Props> = props => {
  const { tripData, paymentMethods, params, setParams, tripPay, loading, paying } = props;
  const theme = useTheme();
  const isTablet = useMediaQuery(theme.breakpoints.down('md'));
  return (
    <Container style={{ flex: 1 }}>
      <Typography variant="h5">
        <FormattedMessage id="pay" />
      </Typography>
      <div style={{ display: 'flex' }}>
        {loading ? (
          <LoadingIcon
            style={{
              height: 320,
              flex: 1,
            }}
          />
        ) : (
          <>
            <div style={{ flex: 1 }}>
              <TripPayBox
                paymentMethods={paymentMethods}
                params={params}
                setParams={setParams}
                tripPay={tripPay}
                paying={paying}
                tripData={tripData}
              />
            </div>
            <AsideBound
              isTablet={isTablet}
              style={{ width: 360, marginLeft: isTablet ? 0 : 24 }}
              direction="right"
            >
              <TripiInfoBox data={tripData} params={params} />
            </AsideBound>
          </>
        )}
      </div>
    </Container>
  );
};

export default TripPayDesktop;
