import { Paper, Typography } from '@material-ui/core';
import * as React from 'react';
import { FormattedMessage } from 'react-intl';
import { some } from '../../../../../constants';
import PaymentMethodsBox from '../../../../booking/common/payment/PaymentMethodsBox';
import { TripParams } from '../utils';

interface Props {
  paymentMethods: some[];
  params: TripParams;
  setParams(params: TripParams): void;
  tripPay(smartOTPPass?: string): void;
  paying: boolean;
  tripData: some;
}

const TripPayBox: React.FunctionComponent<Props> = props => {
  const { paymentMethods, params, setParams, tripPay, paying, tripData } = props;

  return (
    <>
      <Paper variant="outlined" style={{ padding: 16 }}>
        <Typography variant="h6">
          <FormattedMessage id="pay.paymentMethod" />
        </Typography>
        <PaymentMethodsBox
          paymentMethods={paymentMethods}
          total={tripData.totalPrice}
          priceAfter={tripData.totalPrice}
          setSelectedMethod={method =>
            setParams({
              ...params,
              selectedPaymentMethod: method,
              usingPoint: false,
              point: 0,
            })
          }
          selectedMethod={params.selectedPaymentMethod}
          promotionCode={params.promotionCode}
          point={params.point}
          usingPoint={params.usingPoint}
          pay={tripPay}
          paying={paying}
        />
      </Paper>
      {/* <div
        style={{
          padding: '16px 16px 32px 16px',
          color: BLUE,
        }}
      >
        <Typography variant="body2">
          <FormattedMessage id="hotel.seePaymentDetail" />
        </Typography>
      </div> */}

      {/* <TermsAndConditions /> */}
    </>
  );
};

export default TripPayBox;
