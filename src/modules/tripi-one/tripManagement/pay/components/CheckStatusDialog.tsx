import { Button, Typography } from '@material-ui/core';
import * as React from 'react';
import { FormattedHTMLMessage, FormattedMessage } from 'react-intl';
import { useDispatch } from 'react-redux';
import { Action } from 'redux';
import { ThunkDispatch } from 'redux-thunk';
import { API_PATHS } from '../../../../../configs/API';
import { AppState } from '../../../../../redux/reducers';
import { ReactComponent as IconSad } from '../../../../../svg/ic_sad.svg';
import { ReactComponent as IconSuccess } from '../../../../../svg/ic_success.svg';
import DialogCustom from '../../../../common/components/DialogCustom';
import { Col, Row } from '../../../../common/components/elements';
import Link from '../../../../common/components/Link';
import LoadingIcon from '../../../../common/components/LoadingIcon';
import { goBackAction } from '../../../../common/redux/reducer';
import { fetchThunk } from '../../../../common/redux/thunk';

interface Props {
  transactionId?: number;
  onClose(): void;
}

const CheckStatusDialog: React.FunctionComponent<Props> = props => {
  const { transactionId, onClose } = props;
  const dispatch = useDispatch<ThunkDispatch<AppState, null, Action<string>>>();
  const [success, setSuccess] = React.useState<boolean | undefined>(undefined);

  const updateStatus = React.useCallback(async () => {
    const json = await dispatch(
      fetchThunk(
        API_PATHS.payStatus,
        'post',
        JSON.stringify({
          transactionId,
        }),
      ),
    );
    return json;
  }, [dispatch, transactionId]);

  const process = React.useCallback(async () => {
    let count = 0;
    while (success === undefined) {
      const json = await updateStatus();
      if (json.code === 200 || json.code === '200') {
        setSuccess(true);
        break;
      }
      await new Promise(resolve => setTimeout(resolve, 2000));
      count += 1;
      if (count === 10) {
        setSuccess(false);
        break;
      }
    }
  }, [success, updateStatus]);

  return (
    <DialogCustom
      open={!!transactionId}
      onEnter={process}
      onClose={onClose}
      PaperProps={{ style: { minWidth: 500 } }}
      footerContent={<div />}
    >
      {' '}
      <Col
        style={{
          padding: '16px',
          minHeight: 300,
          alignItems: 'center',
          justifyContent: 'center',
          textAlign: 'center',
        }}
      >
        {success !== undefined ? (
          <>
            {success ? (
              <>
                <IconSuccess />
                <Typography
                  variant="subtitle1"
                  style={{
                    marginBottom: 16,
                    marginTop: 16,
                  }}
                >
                  <FormattedMessage id="pay.success" />
                </Typography>
                <Typography variant="body2">
                  <FormattedHTMLMessage id="pay.successNote" />
                </Typography>
                <Row style={{ marginTop: 32 }}>
                  <Button
                    variant="contained"
                    color="secondary"
                    style={{ marginRight: 32, minWidth: 160 }}
                    disableElevation
                    size="large"
                    onClick={() => {
                      dispatch(goBackAction());
                    }}
                  >
                    <FormattedHTMLMessage id="pay.managerOrder" />
                  </Button>
                  <Link to="/">
                    <Button
                      size="large"
                      variant="contained"
                      color="secondary"
                      disableElevation
                      style={{ minWidth: 160 }}
                    >
                      <FormattedHTMLMessage id="home" />
                    </Button>
                  </Link>
                </Row>
              </>
            ) : (
              <>
                <IconSad />
                <Typography
                  variant="subtitle1"
                  style={{
                    marginBottom: 16,
                    marginTop: 16,
                  }}
                >
                  <FormattedMessage id="pay.interrupt" />
                </Typography>
                <Typography variant="body2">
                  <FormattedHTMLMessage id="pay.sorryNote" />
                </Typography>
                <Row style={{ marginTop: 32 }}>
                  <Button
                    variant="contained"
                    color="secondary"
                    style={{ minWidth: 160 }}
                    disableElevation
                    size="large"
                    onClick={() => {
                      dispatch(goBackAction());
                    }}
                  >
                    <FormattedHTMLMessage id="pay.managerOrder" />
                  </Button>
                </Row>
              </>
            )}
          </>
        ) : (
          <>
            <LoadingIcon color="secondary" style={{ minHeight: 'unset', flex: 'unset' }} />
            <Typography
              variant="subtitle1"
              style={{
                marginBottom: 16,
                marginTop: 16,
              }}
            >
              <FormattedMessage id="pay.updateStatusPayment" />
            </Typography>
            <Typography variant="body2">
              <FormattedMessage id="pay.process" />
            </Typography>
          </>
        )}
      </Col>
    </DialogCustom>
  );
};

export default CheckStatusDialog;
