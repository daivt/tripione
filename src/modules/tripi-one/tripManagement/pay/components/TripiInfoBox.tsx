import { Paper, Typography } from '@material-ui/core';
import * as React from 'react';
import { FormattedMessage, FormattedNumber } from 'react-intl';
import { some } from '../../../../../constants';
import { computePaymentFees } from '../../../../booking/utils';
import { LineBT36 } from '../../../../common/components/elements';
import { TripParams } from '../utils';

interface Props {
  styles?: React.CSSProperties;
  data: some;
  params: TripParams;
}

const TripiInfoBox: React.FunctionComponent<Props> = props => {
  const { data, styles, params } = props;
  const fee = params?.selectedPaymentMethod
    ? computePaymentFees(params?.selectedPaymentMethod, data.totalPrice)
    : 0;

  return (
    <Paper
      variant="outlined"
      style={{
        ...styles,
        padding: '14px 16px',
        marginBottom: '20px',
        alignSelf: 'flex-start',
      }}
    >
      <Typography variant="h6" style={{ marginBottom: 8 }}>
        <FormattedMessage id="tripManagement.priceDetails" />
      </Typography>
      {data.totalFlightPrice > 0 && (
        <LineBT36>
          <Typography variant="body2">
            <FormattedMessage id="tripManagement.flightSum" />
          </Typography>
          <Typography className="price" variant="body2">
            <FormattedNumber value={data.totalFlightPrice} />
            &nbsp;
            <FormattedMessage id="currency" />
          </Typography>
        </LineBT36>
      )}
      {data.totalHotelPrice > 0 && (
        <LineBT36>
          <Typography variant="body2">
            <FormattedMessage id="tripManagement.hotelSum" />
          </Typography>
          <Typography className="price" variant="body2">
            <FormattedNumber value={data.totalHotelPrice} />
            &nbsp;
            <FormattedMessage id="currency" />
          </Typography>
        </LineBT36>
      )}
      {fee > 0 && (
        <LineBT36>
          <Typography variant="body2">
            <FormattedMessage id="tripManagement.paymentFixedFeeSide" />
          </Typography>
          <Typography className="price" variant="body2">
            <FormattedNumber value={fee} />
            &nbsp;
            <FormattedMessage id="currency" />
          </Typography>
        </LineBT36>
      )}
      {data.totalPrice > 0 && (
        <LineBT36>
          <Typography variant="subtitle2">
            <FormattedMessage id="tripManagement.totalPayable" />
          </Typography>
          <Typography className="final-price" variant="subtitle1">
            <FormattedNumber value={data.totalPrice + fee} />
            &nbsp;
            <FormattedMessage id="currency" />
          </Typography>
        </LineBT36>
      )}
    </Paper>
  );
};

export default TripiInfoBox;
