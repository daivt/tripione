import { some } from '../../../../constants';

export interface TripParams {
  point: number;
  usingPoint: boolean;
  fromMobile: boolean;
  promotionCode?: string;
  promotion?: some;
  selectedPaymentMethod?: some;
}

export const defaultTripParams: TripParams = {
  point: 0,
  usingPoint: false,
  fromMobile: false,
};
