/* eslint-disable react-hooks/exhaustive-deps */
import { setIndex } from '@amcharts/amcharts4/.internal/core/utils/Array';
import { Button, Divider, Paper, TextField, Typography } from '@material-ui/core';
import { createFilterOptions } from '@material-ui/lab';
import { useFormik } from 'formik';
import React from 'react';
import { FormattedMessage, useIntl } from 'react-intl';
import { shallowEqual, useDispatch, useSelector } from 'react-redux';
import { Action } from 'redux';
import { ThunkDispatch } from 'redux-thunk';
import * as yup from 'yup';
import { API_PATHS } from '../../../../../configs/API';
import { some } from '../../../../../constants';
import { SelectItem } from '../../../../../models/object';
import { AppState } from '../../../../../redux/reducers';
import { ReactComponent as IconDelete } from '../../../../../svg/ic_delete.svg';
import DialogCustom from '../../../../common/components/DialogCustom';
import { Col, Row } from '../../../../common/components/elements';
import TableCustom, { Columns } from '../../../../common/components/TableCustom';
import FormControlAutoComplete from '../../../../common/components/FormControlAutoComplete';
import FormControlTextField from '../../../../common/components/FormControlTextField';
import LoadingButton from '../../../../common/components/LoadingButton';
import SingleSelect from '../../../../common/components/SingleSelect';
import { goBackAction } from '../../../../common/redux/reducer';
import { fetchThunk } from '../../../../common/redux/thunk';
import { activeStatusOptions } from '../../constants';
import { DepartmentInfo } from '../utils';
import { GREY_100 } from '../../../../../configs/colors';

interface Props {
  info: DepartmentInfo;
  loading: boolean;
  setInfo(info: DepartmentInfo): void;
  handleAddMore(value: string): Promise<SelectItem>;
  addMoreData?: SelectItem;
}

const filter = createFilterOptions<some>();

const DepartmentForm: React.FC<Props> = props => {
  const { info, setInfo, loading, handleAddMore, addMoreData } = props;
  const dispatch = useDispatch<ThunkDispatch<AppState, null, Action<string>>>();
  const intl = useIntl();
  const positionOptions = useSelector(
    (state: AppState) => state.common.positionOptions,
    shallowEqual,
  );

  const [open, setOpen] = React.useState(false);
  const [newJobTitle, setNewJobTitle] = React.useState<string>('');
  const [jobTitleIndex, setJobTitleIndex] = React.useState<number>(0);
  const [users, setUsers] = React.useState<some[]>([]);

  const loadUsers = React.useCallback(
    async (text: string) => {
      const json = await dispatch(
        fetchThunk(
          API_PATHS.usersList,
          'post',
          JSON.stringify({
            filters: {
              isActive: true,
              jobTitle: null,
              department: null,
              searchStr: text,
            },
            page: 1,
            pageSize: 5,
          }),
        ),
      );
      return json.data?.itemList;
    },
    [dispatch],
  );

  const departmentSchema = yup.object().shape({
    name: yup
      .string()
      .trim()
      .required(intl.formatMessage({ id: 'required' })),
    approvalJobTitles: yup.array().of(
      yup
        .object()
        .nullable()
        .notRequired(),
    ),
    otherApprovers: yup.array().of(
      yup
        .object()
        .nullable()
        .notRequired(),
    ),
  });

  const formik = useFormik({
    initialValues: info,
    onSubmit: values => {
      if (values.id) {
        setOpen(true);
      } else {
        setInfo(values);
      }
    },
    validationSchema: departmentSchema,
  });

  const columns = React.useMemo(() => {
    return [
      {
        title: 'name',
        width: 200,
        render: (record: some, index: number) => (
          <FormControlAutoComplete
            formControlStyle={{ minWidth: 160, marginBottom: -12 }}
            value={record}
            getOptionSelected={(option, value) => {
              return option.id === value.id;
            }}
            placeholder={intl.formatMessage({ id: 'department.enterApprovers' })}
            onChange={(e: any, value?: any) => {
              formik.setFieldValue('otherApprovers', [
                ...formik.values.otherApprovers.slice(0, index),
                value || null,
                ...formik.values.otherApprovers.slice(index + 1),
              ]);
            }}
            freeSolo
            clearOnBlur
            loadOptions={(str: string) => loadUsers(str)}
            filterOptions={(options, state) =>
              options.filter(obj => {
                return (
                  formik.values.otherApprovers?.findIndex(
                    v => v?.id === obj?.id && v?.id !== record?.id,
                  ) === -1
                );
              })
            }
            getOptionLabel={v => (v.name ? v.name : '')}
            options={[]}
            errorMessage={
              formik.errors.otherApprovers?.[index] && formik.submitCount > 0
                ? (formik.errors.otherApprovers?.[index] as string)
                : undefined
            }
            autoComplete={false}
            disableClearable
          />
        ),
      },
      {
        title: 'email',
        render: (record: some, index: number) => (
          <Typography variant="caption">{record?.email || '-'}</Typography>
        ),
      },
      {
        title: 'department',
        render: (record: some, index: number) => (
          <Typography variant="caption">{record?.department?.name || '-'}</Typography>
        ),
      },
      {
        title: 'position',
        render: (record: some, index: number) => (
          <Typography variant="caption">{record?.jobTitle?.name || '-'}</Typography>
        ),
      },
      {
        disableAction: true,
        width: 80,
        render: (record: some, index: number) => (
          <Button
            size="small"
            color="secondary"
            style={{ padding: 0 }}
            onClick={() => {
              formik.setFieldValue('otherApprovers', [
                ...formik.values.otherApprovers.slice(0, index),
                ...formik.values.otherApprovers.slice(index + 1),
              ]);
            }}
          >
            <IconDelete />
          </Button>
        ),
      },
    ] as Columns[];
  }, [formik, intl, loadUsers, users]);

  React.useEffect(() => {
    formik.setValues(info, true);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [info]);

  React.useEffect(() => {
    loadUsers('').then(res => {
      setUsers(res);
    });
  }, []);

  React.useEffect(() => {
    addMoreData && setNewJobTitle('');
  }, [addMoreData]);

  return (
    <>
      <form autoComplete="off" onSubmit={formik.handleSubmit}>
        <Row style={{ marginBottom: 12, marginTop: 24, alignItems: 'flex-start' }}>
          <Row>
            <FormControlTextField
              id="name"
              label={<FormattedMessage id="department.name" />}
              formControlStyle={{ marginBottom: 12, width: 300 }}
              placeholder={intl.formatMessage({ id: 'department.enterName' })}
              value={formik.values.name}
              onChange={formik.handleChange}
              inputProps={{
                maxLength: 50,
              }}
              errorMessage={
                formik.errors.name && formik.submitCount > 0 ? formik.errors.name : undefined
              }
            />
            {info.id && (
              <SingleSelect
                value={formik.values.isActive}
                label={<FormattedMessage id="status" />}
                formControlStyle={{ marginBottom: 12, width: 300 }}
                onSelectOption={(value: any) => {
                  formik.setFieldValue('isActive', value);
                }}
                optional
                getOptionLabel={value => intl.formatMessage({ id: value.name })}
                options={activeStatusOptions.slice(1)}
              />
            )}
          </Row>
        </Row>
        <Row style={{ alignItems: 'flex-start', flexWrap: 'wrap' }}>
          <Paper style={{ width: 332, margin: '0 28px 24px 0', maxWidth: 648 }}>
            <Row style={{ justifyContent: 'space-between' }}>
              <Typography variant="subtitle2" style={{ padding: 16 }}>
                <FormattedMessage id="department.approval" />
              </Typography>
              <Button
                variant="contained"
                color="secondary"
                style={{ marginRight: 16, minWidth: 120 }}
                size="medium"
                disableElevation
                onClick={() =>
                  formik.setFieldValue('approvalJobTitles', [
                    ...formik.values.approvalJobTitles,
                    null,
                  ])
                }
              >
                <FormattedMessage id="add" />
              </Button>
            </Row>
            <Divider style={{ backgroundColor: GREY_100 }} />
            <div style={{ padding: 16, paddingBottom: 30 }}>
              {formik.values.approvalJobTitles.map((one: some | null, index: number) => (
                <Row key={index} style={{ alignItems: 'flex-start' }}>
                  <Typography variant="body2" style={{ minWidth: 64, marginTop: 8 }}>
                    <FormattedMessage id="position" />
                  </Typography>
                  <FormControlAutoComplete
                    value={one}
                    formControlStyle={{
                      marginBottom: 12,
                      width: 200,
                      minWidth: 200,
                      marginRight: 12,
                    }}
                    placeholder={intl.formatMessage({ id: 'position.choose' })}
                    onChange={(e: some, value: some | null | string) => {
                      if (typeof value === 'object' && value?.id === null) {
                        setNewJobTitle(value.input);
                        setJobTitleIndex(index);
                      } else {
                        formik.setFieldValue('approvalJobTitles', [
                          ...formik.values.approvalJobTitles.slice(0, index),
                          value || null,
                          ...formik.values.approvalJobTitles.slice(index + 1),
                        ]);
                      }
                    }}
                    getOptionLabel={value => value.name}
                    options={positionOptions || []}
                    getOptionSelected={(option: some, value: some) => option.id === value.id}
                    filterOptions={(options, params) => {
                      const filtered = filter(options, params);
                      // Suggest the creation of a new value
                      if (params.inputValue !== '' && filtered.length === 0) {
                        filtered.push({
                          id: null,
                          input: params.inputValue,
                          name: `${params.inputValue} (${intl.formatMessage({ id: 'addMore' })})`,
                        });
                      }
                      return filtered;
                    }}
                    clearOnBlur
                  />
                  <Button
                    size="small"
                    color="secondary"
                    style={{ padding: 0, margin: '8px 4px 0' }}
                    onClick={() => {
                      formik.setFieldValue('approvalJobTitles', [
                        ...formik.values.approvalJobTitles.slice(0, index),
                        ...formik.values.approvalJobTitles.slice(index + 1),
                      ]);
                    }}
                  >
                    <IconDelete />
                  </Button>
                </Row>
              ))}
            </div>
          </Paper>
          <TableCustom
            dataSource={formik.values.otherApprovers}
            loading={loading}
            columns={columns}
            style={{ flex: 1, minWidth: 648 }}
            noColumnIndex
            header={
              <Row style={{ justifyContent: 'space-between' }}>
                <Typography variant="subtitle2" style={{ padding: 16 }}>
                  <FormattedMessage id="department.otherApprovers" />
                </Typography>
                <Button
                  variant="contained"
                  color="secondary"
                  style={{ marginRight: 16, minWidth: 120 }}
                  size="medium"
                  disableElevation
                  onClick={() =>
                    formik.setFieldValue('otherApprovers', [...formik.values.otherApprovers, null])
                  }
                >
                  <FormattedMessage id="add" />
                </Button>
              </Row>
            }
          />
        </Row>
        <Row style={{ padding: '24px 0' }}>
          <LoadingButton
            data-tour="department-save"
            loading={loading}
            variant="contained"
            color="secondary"
            style={{ marginRight: 16, minWidth: 140 }}
            size="large"
            type="submit"
            disableElevation
          >
            <FormattedMessage id="save" />
          </LoadingButton>
          <Button
            variant="outlined"
            style={{ minWidth: 140 }}
            size="large"
            onClick={() => dispatch(goBackAction())}
          >
            <FormattedMessage id="reject" />
          </Button>
        </Row>
      </form>
      <DialogCustom
        open={open || !!newJobTitle}
        onClose={() => {
          newJobTitle && setNewJobTitle('');
          setOpen(false);
        }}
        onAction={() => {
          if (newJobTitle) {
            handleAddMore(newJobTitle).then(res => {
              formik.setFieldValue('approvalJobTitles', [
                ...formik.values.approvalJobTitles.slice(0, jobTitleIndex),
                res,
                ...formik.values.approvalJobTitles.slice(jobTitleIndex + 1),
              ]);
            });
          } else {
            setInfo(formik.values);
            setOpen(false);
          }
        }}
        PaperProps={{ style: { minWidth: 380 } }}
        titleLabel={
          <Typography variant="subtitle1">
            <FormattedMessage id="confirm" />
          </Typography>
        }
        buttonLabel="accept"
      >
        <Col style={{ padding: '16px', minHeight: 60 }}>
          <Typography variant="body2">
            <FormattedMessage
              id={newJobTitle ? 'department.confirmNewJobTitle' : 'updateConfirm'}
              values={{ position: newJobTitle }}
            />
          </Typography>
        </Col>
      </DialogCustom>
    </>
  );
};

export default DepartmentForm;
