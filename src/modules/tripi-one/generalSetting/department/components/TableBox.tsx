import { Button, Typography } from '@material-ui/core';
import * as React from 'react';
import { FormattedMessage } from 'react-intl';
import { GREEN, GREY_500 } from '../../../../../configs/colors';
import { ROUTES } from '../../../../../configs/routes';
import { some } from '../../../../../constants';
import { PaginationFilter } from '../../../../../models/pagination';
import { ReactComponent as IconDelete } from '../../../../../svg/ic_delete.svg';
import DialogCustom from '../../../../common/components/DialogCustom';
import { ChipButton, Col, Row } from '../../../../common/components/elements';
import EmailLink from '../../../../common/components/EmailLink';
import Link from '../../../../common/components/Link';
import TableCustom, { Columns } from '../../../../common/components/TableCustom';
import WarningDialog from '../../../../common/components/WarningDialog';
import { defaultDepartmentInfo, DepartmentFilter, DepartmentInfo } from '../utils';
import Filter from './Filter';

interface Props {
  loading: boolean;
  data?: some;
  filter: DepartmentFilter;
  pagination: PaginationFilter;
  onDelete(value: DepartmentInfo): void;
  onUpdateFilter(filter: DepartmentFilter): void;
  onUpdatePagination(pagination: PaginationFilter): void;
}

const TableBox: React.FunctionComponent<Props> = props => {
  const { onDelete, data, loading, filter, onUpdateFilter, onUpdatePagination, pagination } = props;
  const [detail, setDetail] = React.useState<DepartmentInfo | undefined>(undefined);
  const [deleteData, setDeleteData] = React.useState<DepartmentInfo | undefined>(undefined);

  const columns = React.useMemo(() => {
    return [
      {
        title: 'department.name',
        dataIndex: 'name',
      },
      {
        title: 'department.amount',
        dataIndex: 'numOfEmployees',
      },
      {
        title: 'status',
        dataIndex: 'isActive',
        render: (record: some, index: number) => (
          <Typography variant="caption" style={{ color: record.isActive ? GREEN : GREY_500 }}>
            <FormattedMessage id={record.isActive ? 'global.active' : 'global.inactive'} />
          </Typography>
        ),
      },
      {
        title: 'department.approvers',
        dataIndex: 'approvers',
        render: (record: some, index: number) => (
          <Col>
            {record.approvers.map((one: some, ind: number) => (
              <Typography key={ind} variant="caption">
                {one.name}
              </Typography>
            ))}
          </Col>
        ),
      },
      {
        disableAction: true,
        width: 100,
        render: (record: some, index: number) => (
          <Link
            to={{
              pathname: ROUTES.generalSetting.department.update,
              state: { departmentData: record },
            }}
          >
            <ChipButton style={{ minWidth: 110, borderRadius: 4, margin: '0 20px' }}>
              <Typography variant="caption">
                <FormattedMessage id="department.edit" />
              </Typography>
            </ChipButton>
          </Link>
        ),
      },
      {
        disableAction: true,
        width: 60,
        render: (record: DepartmentInfo, index: number) => (
          <Button
            size="small"
            color="secondary"
            style={{ width: 24, padding: 0 }}
            onClick={() => setDeleteData(record)}
          >
            <IconDelete />
          </Button>
        ),
      },
    ] as Columns[];
  }, []);

  return (
    <>
      <Filter loading={loading} params={filter} onUpdateFilter={onUpdateFilter} />
      <TableCustom
        dataSource={data?.itemList}
        loading={loading}
        columns={columns}
        style={{ maxWidth: 1024, marginTop: 24 }}
        header={
          <Row style={{ justifyContent: 'space-between', padding: '16px 12px' }}>
            <Typography variant="subtitle2">
              <FormattedMessage id="department.listAccount" />
            </Typography>
            <Row>
              <Link
                to={{
                  pathname: ROUTES.generalSetting.department.create,
                  state: { departmentData: defaultDepartmentInfo },
                }}
              >
                <Button
                  variant="contained"
                  color="secondary"
                  style={{ minWidth: 120 }}
                  disableElevation
                >
                  <FormattedMessage id="addNew" />
                </Button>
              </Link>
            </Row>
          </Row>
        }
        paginationProps={{
          count: data?.totalResults || 0,
          page: pagination.page || 1,
          rowsPerPage: pagination?.pageSize || 0,
          onChangePage: (event: unknown, newPage: number) => {
            onUpdatePagination({ ...pagination, page: newPage });
          },
          onChangeRowsPerPage: (event: React.ChangeEvent<HTMLInputElement>) => {
            onUpdatePagination({
              pageSize: parseInt(event.target.value, 10),
              page: 1,
            });
          },
        }}
        // Temporary hide the detail - Quan confirmed
        // onRowClick={(record: some) => setDetail(record as DepartmentInfo)}
      />
      <DialogCustom
        open={!!detail}
        onClose={() => {
          setDetail(undefined);
        }}
        PaperProps={{ style: { minWidth: 500 } }}
        titleLabel={
          <Typography variant="subtitle1">
            <FormattedMessage id="department.info" />
          </Typography>
        }
        buttonLabel="close"
      >
        {detail && (
          <Col style={{ padding: '32px 16px' }}>
            <Row style={{ marginBottom: 16 }}>
              <Typography variant="body2" style={{ minWidth: 120, marginRight: 40 }}>
                <FormattedMessage id="department" />:
              </Typography>
              <Typography variant="body2">{detail.name}</Typography>
            </Row>
            <Row style={{ marginBottom: 16 }}>
              <Typography variant="body2" style={{ minWidth: 120, marginRight: 40 }}>
                <FormattedMessage id="status" />:
              </Typography>
              <Typography variant="body2" style={{ color: detail.isActive ? GREEN : GREY_500 }}>
                <FormattedMessage id={detail.isActive ? 'global.active' : 'global.inactive'} />
              </Typography>
            </Row>
            <Row style={{ alignItems: 'flex-start' }}>
              <Typography variant="body2" style={{ minWidth: 120, marginRight: 40 }}>
                <FormattedMessage id="department.approvers" />
              </Typography>
              <Col>{detail.approvers?.map(v => v && <EmailLink key={v.id} value={v.email} />)}</Col>
            </Row>
          </Col>
        )}
      </DialogCustom>
      <WarningDialog
        open={!!deleteData}
        onClose={() => {
          setDeleteData(undefined);
        }}
        onAccept={() => {
          deleteData && onDelete(deleteData);
          setDeleteData(undefined);
        }}
        messageContent={
          <Typography variant="body2">
            <FormattedMessage id="department.deleteConfirm" />
          </Typography>
        }
      />
    </>
  );
};

export default TableBox;
