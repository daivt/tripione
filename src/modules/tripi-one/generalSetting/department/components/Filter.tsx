import { Button } from '@material-ui/core';
import { useFormik } from 'formik';
import * as React from 'react';
import { FormattedMessage, useIntl } from 'react-intl';
import { ReactComponent as IconRefresh } from '../../../../../svg/ic_refresh.svg';
import { Row } from '../../../../common/components/elements';
import FormControlTextField from '../../../../common/components/FormControlTextField';
import LoadingButton from '../../../../common/components/LoadingButton';
import SingleSelect from '../../../../common/components/SingleSelect';
import { defaultDepartmentFilter, DepartmentFilter } from '../utils';
import { statusBaseOptions } from '../../../../../models/status';
import { trimObjectValues } from '../../../../utils';

interface Props {
  params: DepartmentFilter;
  onUpdateFilter(params: DepartmentFilter): void;
  loading?: boolean;
}

const Filter: React.FunctionComponent<Props> = props => {
  const { params, onUpdateFilter, loading } = props;
  const intl = useIntl();
  const formik = useFormik({
    initialValues: params,
    onSubmit: values => {
      onUpdateFilter(trimObjectValues(values));
    },
  });

  React.useEffect(() => {
    formik.setValues(params);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [params]);

  return (
    <form onSubmit={formik.handleSubmit} autoComplete="off">
      <Row style={{ flexWrap: 'wrap' }}>
        <FormControlTextField
          id="departmentName"
          label={<FormattedMessage id="department.name" />}
          formControlStyle={{ width: 250 }}
          placeholder={intl.formatMessage({ id: 'department.enterName' })}
          value={formik.values.departmentName}
          onChange={formik.handleChange}
          optional
          inputProps={{
            maxLength: 100,
          }}
        />
        <SingleSelect
          value={formik.values.isActive}
          label={<FormattedMessage id="status" />}
          formControlStyle={{ width: 250 }}
          onSelectOption={(value: any) => {
            formik.setFieldValue('isActive', value);
          }}
          getOptionLabel={value => intl.formatMessage({ id: value.name })}
          options={[{ id: undefined, name: 'all' }, ...statusBaseOptions]}
          optional
        />
        <Row style={{ marginTop: 4 }}>
          <LoadingButton
            type="submit"
            loading={loading}
            variant="contained"
            style={{ minWidth: 160, marginRight: 16 }}
            color="secondary"
            size="large"
            disableElevation
          >
            <FormattedMessage id="search" />
          </LoadingButton>
          <Button
            size="large"
            style={{ minWidth: 0 }}
            onClick={() => {
              formik.setValues(defaultDepartmentFilter);
              onUpdateFilter(defaultDepartmentFilter);
            }}
          >
            <IconRefresh />
          </Button>
        </Row>
      </Row>
    </form>
  );
};

export default Filter;
