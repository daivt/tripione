import { some } from '../../../../constants';

export interface DepartmentInfo {
  id?: number;
  name?: string;
  numberOfEmployee?: number;
  isActive: boolean;
  approvers: some[];
  approvalJobTitles: (some|null)[];
  otherApprovers: (some|null)[];
}

export const defaultDepartmentInfo: DepartmentInfo = {
  name: '',
  isActive: true,
  approvers: [],
  approvalJobTitles: [null],
  otherApprovers: [null],
};

export interface DepartmentFilter {
  departmentName: string;
  isActive?: boolean;
}

export const defaultDepartmentFilter: DepartmentFilter = {
  departmentName: '',
};

export const removeNullInArray = (arr: any[]) => {
  return arr.filter((el: any) => el != null)
}
