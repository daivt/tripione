import { useSnackbar } from 'notistack';
import React, { useEffect, useMemo, useState } from 'react';
import { shallowEqual, useDispatch, useSelector } from 'react-redux';
import { Action } from 'redux';
import { ThunkDispatch } from 'redux-thunk';
import { API_PATHS } from '../../../../../configs/API';
import { some, SUCCESS_CODE } from '../../../../../constants';
import { SelectItem } from '../../../../../models/object';
import { AppState } from '../../../../../redux/reducers';
import { snackbarSetting } from '../../../../common/components/elements';
import {
  fetchDepartmentOptions,
  fetchPositionOptions,
  goBackAction,
} from '../../../../common/redux/reducer';
import { fetchThunk } from '../../../../common/redux/thunk';
import DepartmentForm from '../components/DepartmentForm';
import { DepartmentInfo, removeNullInArray } from '../utils';

interface Props {}

const CreateUpdateDepartment: React.FC<Props> = props => {
  const dispatch = useDispatch<ThunkDispatch<AppState, null, Action<string>>>();
  const router = useSelector((state: AppState) => state.router, shallowEqual);
  const getActionData = useMemo(() => {
    const state = router.location.state as some;
    const data = state && (state.departmentData as DepartmentInfo);
    return data;
  }, [router.location.state]);

  const [loading, setLoading] = useState(false);
  const { enqueueSnackbar, closeSnackbar } = useSnackbar();
  const [addMoreData, setAddMoreData] = React.useState<SelectItem | undefined>(undefined);

  const onCreateOrUpdate = React.useCallback(
    async (data: DepartmentInfo) => {
      setLoading(true);
      if (data.id) {
        const json = await dispatch(
          fetchThunk(
            API_PATHS.updateCompanyDepartments,
            'put',
            JSON.stringify({
              departmentDetail: {
                ...data,
                approvalJobTitles: removeNullInArray(data.approvalJobTitles),
                otherApprovers: removeNullInArray(data.otherApprovers),
              },
            }),
          ),
        );
        if (json.code === SUCCESS_CODE) {
          dispatch(goBackAction());
          dispatch(fetchDepartmentOptions());
          enqueueSnackbar(
            json.message,
            snackbarSetting(key => closeSnackbar(key)),
          );
        } else {
          enqueueSnackbar(
            json.message,
            snackbarSetting(key => closeSnackbar(key), {
              color: 'error',
            }),
          );
        }
      } else {
        const json = await dispatch(
          fetchThunk(
            API_PATHS.updateCompanyDepartments,
            'post',
            JSON.stringify({
              ...data,
              approvalJobTitles: removeNullInArray(data.approvalJobTitles),
              otherApprovers: removeNullInArray(data.otherApprovers),
            }),
          ),
        );
        if (json.code === SUCCESS_CODE) {
          dispatch(goBackAction());
          dispatch(fetchDepartmentOptions());
          enqueueSnackbar(
            json.message,
            snackbarSetting(key => closeSnackbar(key)),
          );
        } else {
          enqueueSnackbar(
            json.message,
            snackbarSetting(key => closeSnackbar(key), {
              color: 'error',
            }),
          );
        }
      }
      setLoading(false);
    },
    [closeSnackbar, dispatch, enqueueSnackbar],
  );

  const createJobTitle = React.useCallback(
    async (value: string) => {
      const json = await dispatch(
        fetchThunk(API_PATHS.updateCompanyJobTitles, 'post', JSON.stringify({ name: value })),
      );
      if (json.code === SUCCESS_CODE) {
        dispatch(fetchPositionOptions());
        setAddMoreData(json.data.itemList.slice(-1).pop());
        enqueueSnackbar(
          json.message,
          snackbarSetting(key => closeSnackbar(key)),
        );
        return json.data.itemList.find((v: SelectItem) => v.name === value);
      }
    },
    [closeSnackbar, dispatch, enqueueSnackbar],
  );

  useEffect(() => {
    if (!getActionData) {
      dispatch(goBackAction());
    }
  }, [dispatch, getActionData]);

  if (!getActionData) {
    return null;
  }
  return (
    <>
      <DepartmentForm
        loading={loading}
        info={getActionData}
        setInfo={onCreateOrUpdate}
        handleAddMore={createJobTitle}
        addMoreData={addMoreData}
      />
    </>
  );
};

export default CreateUpdateDepartment;
