import { debounce } from 'lodash';
import { useSnackbar } from 'notistack';
import queryString from 'query-string';
import * as React from 'react';
import { useDispatch } from 'react-redux';
import { useLocation } from 'react-router';
import { Action } from 'redux';
import { ThunkDispatch } from 'redux-thunk';
import { API_PATHS } from '../../../../../configs/API';
import { some, SUCCESS_CODE } from '../../../../../constants';
import { defaultPaginationFilter, PaginationFilter } from '../../../../../models/pagination';
import { AppState } from '../../../../../redux/reducers';
import { snackbarSetting } from '../../../../common/components/elements';
import { fetchDepartmentOptions, goToReplace } from '../../../../common/redux/reducer';
import { fetchThunk } from '../../../../common/redux/thunk';
import { uniqueObject } from '../../../../utils';
import TableBox from '../components/TableBox';
import { defaultDepartmentFilter, DepartmentFilter, DepartmentInfo } from '../utils';

interface Props {}

const Department: React.FunctionComponent<Props> = props => {
  const dispatch = useDispatch<ThunkDispatch<AppState, null, Action<string>>>();
  const [data, setData] = React.useState<some | undefined>(undefined);
  const [filter, setFilter] = React.useState<DepartmentFilter>(defaultDepartmentFilter);
  const [pagination, setPagination] = React.useState<PaginationFilter>(defaultPaginationFilter);
  const [loading, setLoading] = React.useState(true);

  const { enqueueSnackbar, closeSnackbar } = useSnackbar();
  const location = useLocation();

  const updateQueryParams = React.useCallback(() => {
    if (location.search) {
      const filterParams = queryString.parse(location.search) as any;
      const filterTmp = queryString.parse(filterParams.filters) as any;
      setFilter(uniqueObject(filterTmp) as any);
      const paginationTmp = {
        page: filterParams.page ? parseInt(`${filterParams.page}`, 10) : 1,
        pageSize: filterParams.pageSize ? parseInt(`${filterParams.pageSize}`, 10) : 10,
      } as PaginationFilter;
      setPagination(paginationTmp);
    } else {
      dispatch(
        goToReplace({
          search: queryString.stringify({
            ...defaultPaginationFilter,
            filters: queryString.stringify(defaultDepartmentFilter),
          }),
        }),
      );
    }
  }, [dispatch, location.search]);

  const fetchData = React.useCallback(
    debounce(
      async (filterParams: DepartmentFilter, paginationParams: PaginationFilter) => {
        setLoading(true);
        const json = await dispatch(
          fetchThunk(
            API_PATHS.companyDepartments,
            'post',
            JSON.stringify({ filters: filterParams, ...paginationParams }),
          ),
        );
        if (json.code === SUCCESS_CODE) {
          setData(json.data);
        }
        setLoading(false);
      },
      200,
      {
        trailing: true,
        leading: false,
      },
    ),
    [],
  );

  const onDelete = React.useCallback(
    async (info: DepartmentInfo) => {
      const json = await dispatch(
        fetchThunk(
          API_PATHS.updateCompanyDepartments,
          'delete',
          JSON.stringify({
            ids: [info.id],
          }),
        ),
      );
      if (json.code === SUCCESS_CODE) {
        setData(one => ({
          ...one,
          itemList: one?.itemList.filter((item: any) => item.id !== info.id),
        }));
        dispatch(fetchDepartmentOptions());
        enqueueSnackbar(
          json.message,
          snackbarSetting(key => closeSnackbar(key)),
        );
      } else {
        enqueueSnackbar(
          json.message,
          snackbarSetting(key => closeSnackbar(key), {
            color: 'error',
          }),
        );
      }
    },
    [closeSnackbar, dispatch, enqueueSnackbar],
  );

  React.useEffect(() => {
    updateQueryParams();
  }, [updateQueryParams]);

  React.useEffect(() => {
    fetchData(filter, pagination);
  }, [fetchData, filter, pagination]);

  return (
    <TableBox
      loading={loading}
      data={data}
      onDelete={onDelete}
      filter={filter}
      onUpdateFilter={values => {
        dispatch(
          goToReplace({
            search: queryString.stringify({
              ...pagination,
              page: 1,
              filters: queryString.stringify(uniqueObject(values)),
            }),
          }),
        );
      }}
      pagination={pagination}
      onUpdatePagination={values => {
        dispatch(
          goToReplace(
            uniqueObject({
              search: queryString.stringify({ ...values, filters: queryString.stringify(filter) }),
            }),
          ),
        );
      }}
    />
  );
};

export default Department;
