import { Button, Typography } from '@material-ui/core';
import * as React from 'react';
import { FormattedMessage } from 'react-intl';
import { GREEN, GREY_500 } from '../../../../../configs/colors';
import { some } from '../../../../../constants';
import { PaginationFilter } from '../../../../../models/pagination';
import { ReactComponent as IconDelete } from '../../../../../svg/ic_delete.svg';
import { ReactComponent as IconEdit } from '../../../../../svg/ic_edit.svg';
import { Row } from '../../../../common/components/elements';
import PermissionDiv from '../../../../common/components/PermissionDiv';
import TableCustom, { Columns } from '../../../../common/components/TableCustom';
import { defaultPositionInfo, PositionFilter, PositionInfo } from '../utils';
import Filter from './Filter';

interface Props {
  loading: boolean;
  data?: some;
  filter: PositionFilter;
  pagination: PaginationFilter;
  onOpenCreateOrUpdate(value: PositionInfo): void;
  onOpenDelete(value: PositionInfo): void;
  onUpdateFilter(filter: PositionFilter): void;
  onUpdatePagination(pagination: PaginationFilter): void;
}

const TableBox: React.FunctionComponent<Props> = props => {
  const {
    data,
    loading,
    filter,
    pagination,
    onUpdateFilter,
    onUpdatePagination,
    onOpenCreateOrUpdate,
    onOpenDelete,
  } = props;

  const columns = React.useMemo(() => {
    return [
      {
        title: 'position',
        dataIndex: 'name',
      },

      {
        title: 'status',
        dataIndex: 'isActive',
        render: (record: some, index: number) => (
          <Typography variant="caption" style={{ color: record.isActive ? GREEN : GREY_500 }}>
            <FormattedMessage id={record.isActive ? 'global.active' : 'global.inactive'} />
          </Typography>
        ),
      },
      {
        width: 120,
        style: { justifyContent: 'center', display: 'flex' },
        render: (record: PositionInfo, index: number) => (
          <Row >
            <PermissionDiv permission={['tripione:corporatePortal:jobTitle:update']}>
              <Button
                size="small"
                color="secondary"
                style={{ width: 24, padding: 0, marginRight: 20 }}
                onClick={() => onOpenCreateOrUpdate(record)}
              >
                <IconEdit />
              </Button>
            </PermissionDiv>
            <PermissionDiv permission={['tripione:corporatePortal:jobTitle:delete']}>
              <Button
                size="small"
                color="secondary"
                style={{ width: 24, padding: 0 }}
                onClick={() => onOpenDelete(record)}
              >
                <IconDelete />
              </Button>{' '}
            </PermissionDiv>
          </Row>
        ),
      },
    ] as Columns[];
  }, [onOpenCreateOrUpdate, onOpenDelete]);

  return (
    <>
      <div data-tour="step-1" style={{ maxWidth: 800 }}>
        <Filter loading={loading} params={filter} onUpdateFilter={onUpdateFilter} />
        <TableCustom
          dataSource={data?.itemList}
          loading={loading}
          columns={columns}
          style={{ maxWidth: 624, marginTop: 24 }}
          header={
            <Row style={{ justifyContent: 'space-between', padding: '16px 12px' }}>
              <Typography variant="subtitle2">
                <FormattedMessage id="position.listAccount" />
              </Typography>
              <PermissionDiv permission={['tripione:corporatePortal:jobTitle:create']}>
                <Button
                  data-tour="step-3"
                  variant="contained"
                  color="secondary"
                  style={{ minWidth: 120 }}
                  disableElevation
                  onClick={() => {
                    onOpenCreateOrUpdate(defaultPositionInfo);
                  }}
                >
                  <FormattedMessage id="addNew" />
                </Button>
              </PermissionDiv>
            </Row>
          }
          paginationProps={{
            count: data?.totalResults || 0,
            page: pagination.page || 1,
            rowsPerPage: pagination?.pageSize || 10,
            onChangePage: (event: unknown, newPage: number) => {
              onUpdatePagination({ ...pagination, page: newPage });
            },
            onChangeRowsPerPage: (event: React.ChangeEvent<HTMLInputElement>) => {
              onUpdatePagination({
                pageSize: parseInt(event.target.value, 10),
                page: 1,
              });
            },
          }}
        />
      </div>
    </>
  );
};

export default TableBox;
