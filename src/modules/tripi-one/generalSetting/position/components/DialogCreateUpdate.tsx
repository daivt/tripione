import { Button, Divider, Typography } from '@material-ui/core';
import { useFormik } from 'formik';
import * as React from 'react';
import { FormattedMessage, useIntl } from 'react-intl';
import * as yup from 'yup';
import DialogCustom from '../../../../common/components/DialogCustom';
import { Col, Row } from '../../../../common/components/elements';
import { redMark } from '../../../../common/components/Form';
import FormControlTextField from '../../../../common/components/FormControlTextField';
import LoadingButton from '../../../../common/components/LoadingButton';
import SingleSelect from '../../../../common/components/SingleSelect';
import { activeStatusOptions } from '../../constants';
import { PositionInfo } from '../utils';

interface Props {
  onClose(): void;
  onCreateOrUpdate: (data: PositionInfo) => void;
  data: PositionInfo;
  loading: boolean;
}

const DialogCreateUpdate: React.FunctionComponent<Props> = props => {
  const { data, onClose, onCreateOrUpdate } = props;
  const [loading, setLoading] = React.useState(false);
  const intl = useIntl();

  const positionSchema = yup.object().shape({
    name: yup
      .string()
      .trim()
      .required(intl.formatMessage({ id: 'required' })),
    isActive: yup
      .boolean()
      .nullable()
      .required(intl.formatMessage({ id: 'required' })),
  });

  const formik = useFormik({
    initialValues: data,
    onSubmit: values => {
      onCreateOrUpdate({...values, name: values.name?.trim()});
      onClose();
    },
    validationSchema: positionSchema,
  });

  return (
    <DialogCustom
      open
      onClose={onClose}
      onEnter={() => {
        formik.setValues(data, true);
      }}
      PaperProps={{ style: { minWidth: 500 } }}
      titleLabel={
        <Typography variant="subtitle1">
          <FormattedMessage id={data?.id ? 'position.update' : 'position.create'} />
        </Typography>
      }
      disableBackdropClick
      footerContent={<div />}
    >
      <form onSubmit={formik.handleSubmit}>
        <Col style={{ padding: '24px 16px 16px 16px', minHeight: 60 }}>
          <Row style={{ marginBottom: 12 }}>
            <Typography
              variant="body2"
              style={{ marginRight: 24, minWidth: 100, marginBottom: 20 }}
            >
              <FormattedMessage id="position.name" />
              &nbsp; {redMark}
            </Typography>
            <FormControlTextField
              id="name"
              formControlStyle={{ margin: 0 }}
              value={formik.values.name}
              onChange={formik.handleChange}
              errorMessage={
                formik.errors.name && formik.touched.name ? formik.errors.name : undefined
              }
            />
          </Row>
          <Row>
            <Typography
              variant="body2"
              style={{ marginRight: 24, minWidth: 100, marginBottom: 20 }}
            >
              <FormattedMessage id="status" />
            </Typography>
            <SingleSelect
              value={formik.values.isActive}
              formControlStyle={{ width: '100%', margin: 0 }}
              onSelectOption={(value: any) => {
                formik.setFieldValue('isActive', value);
              }}
              getOptionLabel={value => intl.formatMessage({ id: value.name })}
              options={activeStatusOptions.slice(1)}
              errorMessage={
                formik.errors.isActive && formik.touched.isActive
                  ? formik.errors.isActive
                  : undefined
              }
            />
          </Row>
        </Col>
        <Divider />
        <Row style={{ padding: 16, justifyContent: 'flex-end' }}>
          <LoadingButton
            loading={loading}
            variant="contained"
            color="secondary"
            style={{ marginRight: 12, minWidth: 110 }}
            disableElevation
            type="submit"
          >
            <FormattedMessage id="save" />
          </LoadingButton>
          <Button variant="outlined" style={{ minWidth: 92 }} onClick={() => onClose()}>
            <FormattedMessage id="reject" />
          </Button>
        </Row>
      </form>
    </DialogCustom>
  );
};

export default DialogCreateUpdate;
