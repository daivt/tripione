export const fakePosition = {
  code: 'SUCCESS',
  message: 'success',
  data: {
    totalResults: 6,
    page: 1,
    pageSize: 10,
    itemList: [
      {
        id: 1,
        name: 'Giám đốc điều hành',
        isActive: true,
      },
      {
        id: 2,
        name: 'Trưởng phòng',
        isActive: true,
      },
      {
        id: 3,
        name: 'Phó phòng',
        isActive: true,
      },
      {
        id: 4,
        name: 'Nhân viên',
        isActive: true,
      },
      {
        id: 5,
        name: 'Bảo vệ',
        isActive: false,
      },
      {
        id: 6,
        name: 'Tạp vụ',
        isActive: false,
      },
    ],
  },
};
