import { Typography } from '@material-ui/core';
import { debounce } from 'lodash';
import { useSnackbar } from 'notistack';
import queryString from 'query-string';
import * as React from 'react';
import { FormattedMessage } from 'react-intl';
import { useDispatch } from 'react-redux';
import { useLocation } from 'react-router';
import { Action } from 'redux';
import { ThunkDispatch } from 'redux-thunk';
import { API_PATHS } from '../../../../../configs/API';
import { some, SUCCESS_CODE } from '../../../../../constants';
import { defaultPaginationFilter, PaginationFilter } from '../../../../../models/pagination';
import { AppState } from '../../../../../redux/reducers';
import { snackbarSetting } from '../../../../common/components/elements';
import WarningDialog from '../../../../common/components/WarningDialog';
import { fetchPositionOptions, goToReplace } from '../../../../common/redux/reducer';
import { fetchThunk } from '../../../../common/redux/thunk';
import DialogCreateUpdate from '../components/DialogCreateUpdate';
import TableBox from '../components/TableBox';
import { defaultPositionFilter, PositionFilter, PositionInfo } from '../utils';
import { uniqueObject } from '../../../../utils';

interface Props {}

const Position: React.FunctionComponent<Props> = props => {
  const dispatch = useDispatch<ThunkDispatch<AppState, null, Action<string>>>();
  const [data, setData] = React.useState<some | undefined>(undefined);
  const [filter, setFilter] = React.useState<PositionFilter>(defaultPositionFilter);
  const [pagination, setPagination] = React.useState<PaginationFilter>(defaultPaginationFilter);
  const [loading, setLoading] = React.useState(true);
  const [detailTarget, setDetailTarget] = React.useState<PositionInfo | undefined>(undefined);
  const [deleteTarget, setDeleteTarget] = React.useState<PositionInfo | undefined>(undefined);
  const { enqueueSnackbar, closeSnackbar } = useSnackbar();
  const location = useLocation();

  const updateQueryParams = React.useCallback(() => {
    if (location.search) {
      const filterParams = queryString.parse(location.search) as any;
      const filterTmp = queryString.parse(filterParams.filters) as any;

      setFilter(uniqueObject(filterTmp) as any);
      const paginationTmp = {
        page: filterParams.page ? parseInt(`${filterParams.page}`, 10) : 1,
        pageSize: filterParams.pageSize ? parseInt(`${filterParams.pageSize}`, 10) : 10,
      } as PaginationFilter;
      setPagination(paginationTmp);
    } else {
      dispatch(
        goToReplace({
          search: queryString.stringify({
            filters: queryString.stringify(defaultPositionFilter),
            ...defaultPaginationFilter,
          }),
        }),
      );
    }
  }, [dispatch, location.search]);

  const fetchData = React.useCallback(
    debounce(
      async (filterParams: PositionFilter, paginationParams: PaginationFilter) => {
        setLoading(true);
        const json = await dispatch(
          fetchThunk(
            API_PATHS.companyJobTitles,
            'post',
            JSON.stringify({ filters: filterParams, ...paginationParams }),
          ),
        );
        if (json.code === SUCCESS_CODE) {
          setData(json.data);
        }
        setLoading(false);
      },
      200,
      {
        trailing: true,
        leading: false,
      },
    ),
    [],
  );

  const onCreateOrUpdate = React.useCallback(
    async (info: PositionInfo) => {
      setLoading(true);
      if (info.id) {
        const json = await dispatch(
          fetchThunk(
            API_PATHS.updateCompanyJobTitles,
            'put',
            JSON.stringify({
              id: info.id,
              ...info,
            }),
          ),
        );
        if (json.code === SUCCESS_CODE) {
          setData(one => ({
            ...one,
            itemList: [
              ...one?.itemList.map((item: any) => (item.id === info.id ? json.data : item)),
            ],
          }));
          setDetailTarget(undefined);
          dispatch(fetchPositionOptions());
          enqueueSnackbar(
            json.message,
            snackbarSetting(key => closeSnackbar(key)),
          );
        } else {
          enqueueSnackbar(
            json.message,
            snackbarSetting(key => closeSnackbar(key), {
              color: 'error',
            }),
          );
        }
      } else {
        const json = await dispatch(
          fetchThunk(API_PATHS.updateCompanyJobTitles, 'post', JSON.stringify(info)),
        );
        if (json.code === SUCCESS_CODE) {
          setData(one => ({...json.data, totalResults: one?.totalResults + 1}));
          setDetailTarget(undefined);
          dispatch(fetchPositionOptions());
          enqueueSnackbar(
            json.message,
            snackbarSetting(key => closeSnackbar(key)),
          );
        } else {
          enqueueSnackbar(
            json.message,
            snackbarSetting(key => closeSnackbar(key), {
              color: 'error',
            }),
          );
        }
      }
      setLoading(false);
    },
    [closeSnackbar, dispatch, enqueueSnackbar],
  );

  const onDelete = React.useCallback(
    async (info: PositionInfo) => {
      const json = await dispatch(
        fetchThunk(
          API_PATHS.updateCompanyJobTitles,
          'delete',
          JSON.stringify({
            ids: [info.id],
          }),
        ),
      );
      if (json.code === SUCCESS_CODE) {
        setData(one => ({
          ...one,
          itemList: one?.itemList.filter((item: any) => item.id !== info.id),
          totalResults: one?.totalResults - 1
        }));
        dispatch(fetchPositionOptions());
        enqueueSnackbar(
          json.message,
          snackbarSetting(key => closeSnackbar(key)),
        );
      } else {
        enqueueSnackbar(
          json.message,
          snackbarSetting(key => closeSnackbar(key), {
            color: 'error',
          }),
        );
      }
    },
    [closeSnackbar, dispatch, enqueueSnackbar],
  );

  React.useEffect(() => {
    updateQueryParams();
  }, [updateQueryParams]);

  React.useEffect(() => {
    fetchData(filter, pagination);
  }, [fetchData, filter, pagination]);

  return (
    <>
      <TableBox
        loading={loading}
        data={data}
        onOpenCreateOrUpdate={(record: PositionInfo) => setDetailTarget(record)}
        onOpenDelete={(record: PositionInfo) => setDeleteTarget(record)}
        filter={filter}
        onUpdateFilter={values => {
          dispatch(
            goToReplace(
              uniqueObject({
                search: queryString.stringify({
                  ...pagination,
                  pageOffset: 0,
                  filters: queryString.stringify(values),
                }),
              }),
            ),
          );
        }}
        pagination={pagination}
        onUpdatePagination={values => {
          dispatch(
            goToReplace(
              uniqueObject({
                search: queryString.stringify({
                  ...values,
                  filters: queryString.stringify(filter),
                }),
              }),
            ),
          );
        }}
      />
      <WarningDialog
        open={!!deleteTarget}
        onClose={() => {
          setDeleteTarget(undefined);
        }}
        onAccept={() => {
          deleteTarget && onDelete(deleteTarget);
          setDeleteTarget(undefined);
        }}
        messageContent={
          <Typography variant="body2">
            <FormattedMessage id="policy.deleteConfirm" />
          </Typography>
        }
      />
      {detailTarget && (
        <DialogCreateUpdate
          loading={loading}
          data={detailTarget}
          onCreateOrUpdate={onCreateOrUpdate}
          onClose={() => {
            setDetailTarget(undefined);
          }}
        />
      )}
    </>
  );
};

export default Position;
