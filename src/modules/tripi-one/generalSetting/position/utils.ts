export interface PositionInfo {
  id?: number;
  name?: string;
  isActive: boolean | null;
}

export const defaultPositionInfo: PositionInfo = {
  name: '',
  isActive: true,
};

export interface PositionFilter {
  isActive?: boolean;
  name: string;
}

export const defaultPositionFilter: PositionFilter = {
  name: '',
};
