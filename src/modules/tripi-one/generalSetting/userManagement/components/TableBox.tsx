import { Button, Typography } from '@material-ui/core';
import React from 'react';
import { FormattedMessage } from 'react-intl';
import { GREEN, GREY_500 } from '../../../../../configs/colors';
import { ROUTES } from '../../../../../configs/routes';
import { some } from '../../../../../constants';
import { PaginationFilter } from '../../../../../models/pagination';
import { ReactComponent as IconDelete } from '../../../../../svg/ic_delete.svg';
import { ReactComponent as IconEdit } from '../../../../../svg/ic_edit.svg';
import { Row } from '../../../../common/components/elements';
import Link from '../../../../common/components/Link';
import TableCustom, { Columns } from '../../../../common/components/TableCustom';
import WarningDialog from '../../../../common/components/WarningDialog';
import { roleOptions } from '../../../../common/redux/constants';
import { defaultUserManagementInfo, UserManagementFilter, UserManagementInfo } from '../utils';
import Filter from './Filter';

interface Props {
  data?: some;
  filter: UserManagementFilter;
  onUpdateFilter(params: UserManagementFilter): void;
  onDelete(value: UserManagementInfo): void;
  loading?: boolean;
  pagination: PaginationFilter;
  onUpdatePagination(pagination: PaginationFilter): void;
}
export const TableBox: React.FunctionComponent<Props> = props => {
  const { data, filter, onUpdateFilter, loading, pagination, onUpdatePagination, onDelete } = props;
  const [deleteData, setDeleteData] = React.useState<UserManagementInfo | undefined>(undefined);
  const columns = React.useMemo(() => {
    return [
      {
        title: 'userManagement.employeeId',
        dataIndex: 'employeeCode',
      },
      {
        title: 'fullName',
        dataIndex: 'name',
      },
      {
        title: 'gender',
        render: (record: some, index: number) =>
          record.gender !== null && (
            <Typography variant="caption">
              <FormattedMessage id={record.gender === 0 ? 'female' : 'male'} />
            </Typography>
          ),
      },
      {
        title: 'email',
        dataIndex: 'email',
      },
      {
        title: 'department',
        render: (record: some, index: number) => (
          <Typography variant="caption">{record.department?.name}</Typography>
        ),
      },
      {
        title: 'position',
        render: (record: some, index: number) => (
          <Typography variant="caption">{record.jobTitle?.name}</Typography>
        ),
      },
      {
        title: 'phoneNumber',
        dataIndex: 'phone',
      },
      {
        title: 'role',
        render: (record: some, index: number) => (
          <Typography variant="caption">
            {roleOptions.find((v: some) => v.value === record?.role)?.name}
          </Typography>
        ),
      },
      {
        title: 'status',
        dataIndex: 'isActive',
        render: (record: some, index: number) => (
          <Typography variant="caption" style={{ color: record.isActive ? GREEN : GREY_500 }}>
            <FormattedMessage id={record.isActive ? 'global.active' : 'global.inactive'} />
          </Typography>
        ),
      },
      {
        width: 120,
        render: (record: some, index: number) => (
          <Row style={{ justifyContent: 'center', display: 'flex' }}>
            <Link
              to={{
                pathname: ROUTES.generalSetting.userManagement.update,
                state: { userManagementData: record as UserManagementInfo },
              }}
            >
              <Button
                size="small"
                color="secondary"
                style={{ width: 24, padding: 0, marginRight: 20 }}
              >
                <IconEdit />
              </Button>
            </Link>
            <Button
              size="small"
              color="secondary"
              style={{ width: 24, padding: 0 }}
              onClick={() => setDeleteData(record as UserManagementInfo)}
            >
              <IconDelete />
            </Button>
          </Row>
        ),
      },
    ] as Columns[];
  }, []);
  return (
    <>
      <Filter params={filter} onUpdateFilter={onUpdateFilter} loading={loading} />
      <TableCustom
        dataSource={data?.itemList}
        loading={loading}
        columns={columns}
        style={{ marginTop: 24 }}
        header={
          <Row style={{ justifyContent: 'space-between', padding: '16px 12px' }}>
            <Typography variant="subtitle2">
              <FormattedMessage id="userManagement.listUser" />
            </Typography>
            <Row>
              <Link
                to={{
                  pathname: ROUTES.generalSetting.userManagement.create,
                  state: { userManagementData: defaultUserManagementInfo },
                }}
              >
                <Button
                  variant="contained"
                  color="secondary"
                  style={{ minWidth: 120, marginRight: 12 }}
                  disableElevation
                >
                  <FormattedMessage id="addNew" />
                </Button>
              </Link>
              <Link
                to={{
                  pathname: ROUTES.generalSetting.userManagement.upload,
                  state: { userManagementData: defaultUserManagementInfo },
                }}
              >
                <Button
                  variant="contained"
                  color="secondary"
                  style={{ minWidth: 140 }}
                  disableElevation
                >
                  <FormattedMessage id="userManagement.upload" />
                </Button>
              </Link>
            </Row>
          </Row>
        }
        paginationProps={{
          count: data?.totalResults || 0,
          page: pagination.page || 1,
          rowsPerPage: pagination?.pageSize || 0,
          onChangePage: (event: unknown, newPage: number) => {
            onUpdatePagination({ ...pagination, page: newPage });
          },
          onChangeRowsPerPage: (event: React.ChangeEvent<HTMLInputElement>) => {
            onUpdatePagination({
              pageSize: parseInt(event.target.value, 10),
              page: 1,
            });
          },
        }}
      />
      <WarningDialog
        open={!!deleteData}
        onClose={() => {
          setDeleteData(undefined);
        }}
        onAccept={() => {
          deleteData && onDelete(deleteData);
          setDeleteData(undefined);
        }}
        messageContent={
          <Typography variant="body2">
            <FormattedMessage id="userManagement.deleteConfirm" />
          </Typography>
        }
      />
    </>
  );
};

export default TableBox;
