import { Button, LinearProgress, Tooltip, Typography } from '@material-ui/core';
import moment from 'moment';
import React from 'react';
import { FormattedMessage } from 'react-intl';
import { useDispatch } from 'react-redux';
import { Action } from 'redux';
import { ThunkDispatch } from 'redux-thunk';
import { GREEN, GREY, RED } from '../../../../../configs/colors';
import { some } from '../../../../../constants';
import { DATE_FORMAT } from '../../../../../models/moment';
import { PaginationFilter } from '../../../../../models/pagination';
import { AppState } from '../../../../../redux/reducers';
import { ReactComponent as IconFile } from '../../../../../svg/ic_file.svg';
import { ChipButton, Col, Row } from '../../../../common/components/elements';
import FileUpload from '../../../../common/components/FileUpload';
import TableCustom, { Columns } from '../../../../common/components/TableCustom';
import { goBackAction } from '../../../../common/redux/reducer';

interface Props {
  data?: some[];
  loading?: boolean;
  uploadFile: (files: File[]) => void;
  saveUsers: () => void;
  progress: number;
}

const SAMPLE_LINK = 'https://storage.googleapis.com/tripi-assets/trip_one/templates/user.xlsx';

export const TableUpload: React.FunctionComponent<Props> = props => {
  const { data, loading, uploadFile, saveUsers, progress } = props;
  const dispatch = useDispatch<ThunkDispatch<AppState, null, Action<string>>>();

  const checkStatus = React.useCallback((field: some) => {
    return (
      <Tooltip title={field.description || ''} arrow>
        <Typography variant="caption" style={{ color: field.status ? GREY : RED }}>
          {field.value}
        </Typography>
      </Tooltip>
    );
  }, []);

  const columns = React.useMemo(() => {
    return [
      {
        title: 'userManagement.employeeId',
        dataIndex: 'employeeCode',
        render: (record: some, index: number) => checkStatus(record.employeeCode),
      },
      {
        title: 'fullName',
        dataIndex: 'name',
      },
      {
        title: 'gender',
        dataIndex: 'gender',
      },
      {
        title: 'birthday',
        render: (record: some, index: number) => (
          <Typography variant="caption">
            {moment(record.dob).isValid() ? moment(record.dob).format(DATE_FORMAT) : null}
          </Typography>
        ),
      },
      {
        title: 'password',
        render: (record: some, index: number) => checkStatus(record.password),
      },
      {
        title: 'email',
        render: (record: some, index: number) => checkStatus(record.email),
      },
      {
        title: 'department',
        render: (record: some, index: number) => checkStatus(record.department),
      },
      {
        title: 'position',
        render: (record: some, index: number) => checkStatus(record.jobTitle),
      },
      {
        title: 'phoneNumber',
        render: (record: some, index: number) => checkStatus(record.phoneNumber),
      },
      {
        title: 'role',
        render: (record: some, index: number) => checkStatus(record.role),
      },
      {
        title: 'status',
        dataIndex: 'status',
        render: (record: some, index: number) => (
          <Typography variant="caption" style={{ color: record.status ? GREEN : RED }}>
            <FormattedMessage id={record.status ? 'global.success' : 'global.fail'} />
          </Typography>
        ),
      },
    ] as Columns[];
  }, [checkStatus]);
  return (
    <>
      <Row style={{ alignItems: 'flex-start' }}>
        <Typography variant="body2" style={{ margin: '10px 12px 0 0' }}>
          <FormattedMessage id="department.upload" />
        </Typography>
        <FileUpload onDrop={uploadFile} />
      </Row>
      <Row style={{ marginBottom: 24 }}>
        <Typography variant="body2" style={{ marginRight: 12 }}>
          <FormattedMessage id="department.uploadFileSample" />
        </Typography>
        <a href={SAMPLE_LINK} download style={{ textDecoration: 'none' }}>
          <ChipButton style={{ borderRadius: 4 }}>
            <IconFile />
            <Typography variant="body2" style={{ marginLeft: 4 }}>
              <FormattedMessage id="department.fileSample" />
            </Typography>
          </ChipButton>
        </a>
      </Row>
      <TableCustom
        dataSource={data}
        loading={loading}
        columns={columns}
        style={{ minHeight: 120 }}
        header={
          <Col>
            <Row style={{ padding: '16px 12px', justifyContent: 'space-between' }}>
              <Col>
                <Typography variant="subtitle2">
                  <FormattedMessage id="department.uploadList" />
                </Typography>
                {progress > 0 && (
                  <Typography variant="caption">
                    <FormattedMessage
                      id={
                        progress === 100
                          ? 'userManagement.uploadComplete'
                          : 'userManagement.waiting'
                      }
                    />
                  </Typography>
                )}
              </Col>
              <Row>
                {data && progress < 100 && (
                  <Button
                    variant="contained"
                    color="secondary"
                    size="medium"
                    disableElevation
                    style={{ minWidth: 140 }}
                    onClick={saveUsers}
                  >
                    <FormattedMessage id="confirm" />
                  </Button>
                )}
                {progress === 100 && (
                  <Button
                    variant="outlined"
                    color="secondary"
                    size="medium"
                    style={{ minWidth: 120 }}
                    disableElevation
                    onClick={() => dispatch(goBackAction())}
                  >
                    <FormattedMessage id="done" />
                  </Button>
                )}
              </Row>
            </Row>
            {progress > 0 && (
              <LinearProgress variant="determinate" value={progress} color="secondary" />
            )}
          </Col>
        }
      />
      <Row style={{ marginTop: 24 }}>
        {data && progress < 100 && (
          <Button
            variant="contained"
            color="secondary"
            size="large"
            disableElevation
            style={{ marginRight: 24, minWidth: 160 }}
            onClick={saveUsers}
          >
            <FormattedMessage id="confirm" />
          </Button>
        )}
        {progress < 100 ? (
          <Button
            variant="outlined"
            size="large"
            style={{ minWidth: 120 }}
            disableElevation
            onClick={() => dispatch(goBackAction())}
          >
            <FormattedMessage id="reject" />
          </Button>
        ) : (
          <Button
            variant="outlined"
            color="secondary"
            size="large"
            style={{ minWidth: 120 }}
            disableElevation
            onClick={() => dispatch(goBackAction())}
          >
            <FormattedMessage id="done" />
          </Button>
        )}
      </Row>
    </>
  );
};

export default TableUpload;
