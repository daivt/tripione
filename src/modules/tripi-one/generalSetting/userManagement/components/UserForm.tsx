import {
  Button,
  FormControlLabel,
  IconButton,
  Radio,
  RadioGroup,
  Typography,
} from '@material-ui/core';
import { useFormik } from 'formik';
import moment from 'moment';
import React from 'react';
import { FormattedMessage, useIntl } from 'react-intl';
import { shallowEqual, useDispatch, useSelector } from 'react-redux';
import { Action } from 'redux';
import { ThunkDispatch } from 'redux-thunk';
import * as yup from 'yup';
import { RED } from '../../../../../configs/colors';
import { ROUTES } from '../../../../../configs/routes';
import { some } from '../../../../../constants';
import { DATE_FORMAT } from '../../../../../models/moment';
import { validNumberRegex } from '../../../../../models/regex';
import { AppState } from '../../../../../redux/reducers';
import { ReactComponent as InformationIcon } from '../../../../../svg/ic_info.svg';
import BirthDayField from '../../../../common/components/BirthDayField';
import DateField from '../../../../common/components/DateField';
import DialogCustom from '../../../../common/components/DialogCustom';
import { Col, Row } from '../../../../common/components/elements';
import { NumberFormatCustom2, redMark } from '../../../../common/components/Form';
import FormControlAutoComplete from '../../../../common/components/FormControlAutoComplete';
import FormControlTextField from '../../../../common/components/FormControlTextField';
import Link from '../../../../common/components/Link';
import LoadingButton from '../../../../common/components/LoadingButton';
import SingleSelect from '../../../../common/components/SingleSelect';
import { roleOptions } from '../../../../common/redux/constants';
import { goBackAction } from '../../../../common/redux/reducer';
import { activeStatusOptions } from '../../constants';
import { UserManagementInfo } from '../utils';

interface Props {
  info: UserManagementInfo;
  loading: boolean;
  setInfo(info: UserManagementInfo): void;
  countries: some[];
}

const DepartmentForm: React.FC<Props> = props => {
  const dispatch = useDispatch<ThunkDispatch<AppState, null, Action<string>>>();
  const { positionOptions, departmentOptions } = useSelector(
    (state: AppState) => state.common,
    shallowEqual,
  );
  const { info, setInfo, loading, countries } = props;
  const intl = useIntl();
  const [open, setOpen] = React.useState(false);

  const getFamilyName = (str: string) => str?.trim().split(' ')[0];
  const getMiddleAndLastName = (str: string) =>
    str
      ?.trimLeft()
      .split(' ')
      .slice(1)
      .join(' ');

  const userManagementSchema = yup.object().shape({
    name: yup
      .string()
      .trim()
      .test({
        name: 'name',
        message: intl.formatMessage({ id: 'fullNameValid' }),
        test: value => {
          return value ? value.split(' ').length > 1 : true;
        },
      })
      .required(intl.formatMessage({ id: 'required' })),
    employeeCode: yup
      .string()
      .trim()
      .notRequired(),
    email: yup
      .string()
      .trim()
      .email(intl.formatMessage({ id: 'emailInvalid' }))
      .required(intl.formatMessage({ id: 'required' })),
    password: yup.string().when('id', {
      is: () => !info.id,
      then: yup
        .string()
        .trim()
        .required(intl.formatMessage({ id: 'required' }))
        .min(6, intl.formatMessage({ id: 'auth.passwordLengthValidate' }))
        .max(50, intl.formatMessage({ id: 'auth.passwordLengthValidate' })),
      otherwise: yup.string().notRequired(),
    }),
    phone: yup
      .string()
      .nullable()
      .required(intl.formatMessage({ id: 'required' })),
    department: yup
      .mixed()
      .nullable()
      .required(intl.formatMessage({ id: 'required' })),
    jobTitle: yup
      .mixed()
      .nullable()
      .required(intl.formatMessage({ id: 'required' })),
    role: yup
      .mixed()
      .nullable()
      .required(intl.formatMessage({ id: 'required' })),
    birthday: yup
      .date()
      .max(`${moment().subtract(18, 'years')}`, intl.formatMessage({ id: 'birthdayMax' }) )
      .nullable()
      .required(intl.formatMessage({ id: 'required' })),
    gender: yup
      .mixed()
      .nullable()
      .required(intl.formatMessage({ id: 'required' })),
    isActive: yup
      .boolean()
      .nullable()
      .required(intl.formatMessage({ id: 'required' })),
    passportNumber: yup
      .string()
      .nullable()
      .notRequired(),
    passportExpiredDate: yup.string().nullable().notRequired(),
    nationality: yup
      .mixed()
      .nullable()
      .notRequired(),
    country: yup
      .mixed()
      .nullable()
      .notRequired(),
  });

  const formik = useFormik({
    initialValues: info,
    onSubmit: values => {
      if (values) {
        setOpen(true);
      } else {
        setInfo(values);
      }
    },
    validationSchema: userManagementSchema,
  });

  React.useEffect(() => {
    formik.setValues(info, true);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [info]);

  return (
    <>
      <form autoComplete="off" onSubmit={formik.handleSubmit}>
        <Row style={{ alignItems: 'flex-start' }}>
          <Col style={{ marginBottom: 12, marginTop: 24, alignItems: 'flex-start' }}>
            <Row style={{ marginBottom: 12 }}>
              <Typography
                variant="body2"
                style={{ minWidth: 100, marginRight: 16, marginBottom: 20 }}
              >
                <FormattedMessage id="fullName" />
                &nbsp;{redMark}
              </Typography>
              <FormControlTextField
                id="name"
                formControlStyle={{ width: 300 }}
                value={formik.values.name?.trimLeft()}
                placeholder={intl.formatMessage({ id: 'auth.enterName' })}
                onChange={formik.handleChange}
                inputProps={{
                  maxLength: 255,
                }}
                errorMessage={
                  formik.errors.name && formik.submitCount > 0 ? formik.errors.name : undefined
                }
              />
            </Row>
            <Row style={{ marginBottom: 12 }}>
              <Typography
                variant="body2"
                style={{ minWidth: 100, marginRight: 16, marginBottom: 20 }}
              >
                <FormattedMessage id="familyName" />
                &nbsp;{redMark}
              </Typography>
              <FormControlTextField
                id="familyName"
                formControlStyle={{ width: 300 }}
                value={getFamilyName(formik.values.name)}
                placeholder={intl.formatMessage({ id: 'enterFamilyName' })}
                onChange={(e: any, value?: any) => {
                  formik.setFieldValue(
                    'name',
                    e.target.value.trim().concat(' ', getMiddleAndLastName(formik.values.name)),
                  );
                }}
                inputProps={{
                  maxLength: 255,
                }}
              />
            </Row>
            <Row style={{ marginBottom: 12 }}>
              <Typography
                variant="body2"
                style={{ minWidth: 100, marginRight: 16, marginBottom: 20 }}
              >
                <FormattedMessage id="middleAndLastName" />
                &nbsp;{redMark}
              </Typography>
              <FormControlTextField
                id="middleAndLastName"
                formControlStyle={{ width: 300 }}
                value={getMiddleAndLastName(formik.values.name)}
                placeholder={intl.formatMessage({ id: 'enterMiddleAndLastName' })}
                onChange={(e: any, value?: any) => {
                  formik.setFieldValue(
                    'name',
                    getFamilyName(formik.values.name)
                      .trim()
                      .concat(' ', e.target.value),
                  );
                }}
                inputProps={{
                  maxLength: 255,
                }}
              />
            </Row>
            <Row style={{ marginBottom: 12 }}>
              <Typography
                variant="body2"
                style={{ minWidth: 100, marginRight: 16, marginBottom: 20 }}
              >
                <FormattedMessage id="email" />
                &nbsp;{redMark}
              </Typography>
              <FormControlTextField
                id="email"
                formControlStyle={{ width: 300 }}
                value={formik.values.email}
                onChange={formik.handleChange}
                placeholder={intl.formatMessage({ id: 'enterEmail' })}
                inputProps={{
                  maxLength: 50,
                }}
                errorMessage={
                  formik.errors.email && formik.submitCount > 0 ? formik.errors.email : undefined
                }
              />
            </Row>
            {!info.id && (
              <Row style={{ marginBottom: 12 }}>
                <Typography
                  variant="body2"
                  style={{ minWidth: 100, marginRight: 16, marginBottom: 20 }}
                >
                  <FormattedMessage id="password" />
                  &nbsp;{redMark}
                </Typography>
                <FormControlTextField
                  id="password"
                  formControlStyle={{ width: 300 }}
                  value={formik.values.password}
                  onChange={formik.handleChange}
                  placeholder={intl.formatMessage({ id: 'enterPassword' })}
                  inputProps={{
                    maxLength: 50,
                  }}
                  errorMessage={
                    formik.errors.password && formik.submitCount > 0
                      ? formik.errors.password
                      : undefined
                  }
                />
              </Row>
            )}
            <Row style={{ marginBottom: 12 }}>
              <Typography
                variant="body2"
                style={{ minWidth: 100, marginRight: 16, marginBottom: 20 }}
              >
                <FormattedMessage id="phoneNumber" />
                &nbsp;{redMark}
              </Typography>
              <FormControlTextField
                formControlStyle={{ width: 300 }}
                placeholder={intl.formatMessage({ id: 'enterPhoneNumber' })}
                value={formik.values.phone}
                onChange={e =>
                  validNumberRegex.test(e.target.value) &&
                  formik.setFieldValue('phone', e.target.value)
                }
                inputProps={{
                  maxLength: 15,
                }}
                inputComponent={NumberFormatCustom2 as any}
                errorMessage={
                  formik.errors.phone && formik.submitCount > 0 ? formik.errors.phone : undefined
                }
              />
            </Row>
            <Row style={{ marginBottom: 12 }}>
              <Typography
                variant="body2"
                style={{ minWidth: 100, marginRight: 16, marginBottom: 20 }}
              >
                <FormattedMessage id="department" />
                &nbsp;{redMark}
              </Typography>
              <FormControlAutoComplete
                value={formik.values.department || null}
                getOptionSelected={(option, value) => {
                  return option.id === value.id;
                }}
                placeholder={intl.formatMessage({ id: 'department.choose' })}
                formControlStyle={{ width: 300 }}
                onChange={(e: any, value: some | null) => {
                  formik.setFieldValue('department', value);
                }}
                getOptionLabel={value => value.name}
                options={departmentOptions}
                errorMessage={
                  formik.errors.department && formik.submitCount > 0
                    ? formik.errors.department
                    : undefined
                }
              />
            </Row>
            <Row style={{ marginBottom: 12 }}>
              <Typography
                variant="body2"
                style={{ minWidth: 100, marginRight: 16, marginBottom: 20 }}
              >
                <FormattedMessage id="position" />
                &nbsp;{redMark}
              </Typography>
              <FormControlAutoComplete
                value={formik.values.jobTitle || null}
                getOptionSelected={(option, value) => {
                  return option.id === value.id;
                }}
                placeholder={intl.formatMessage({ id: 'position.choose' })}
                formControlStyle={{ width: 300 }}
                onChange={(e: any, value: some | null) => {
                  formik.setFieldValue('jobTitle', value);
                }}
                getOptionLabel={value => value.name}
                options={positionOptions}
                errorMessage={
                  formik.errors.jobTitle && formik.submitCount > 0
                    ? formik.errors.jobTitle
                    : undefined
                }
              />
            </Row>
            <Row style={{ marginBottom: 12 }}>
              <Typography
                variant="body2"
                style={{ minWidth: 100, marginRight: 16, marginBottom: 20 }}
              >
                <FormattedMessage id="role" />
                &nbsp;{redMark}
              </Typography>
              <FormControlAutoComplete
                value={formik.values.role || null}
                getOptionSelected={(option, value) => {
                  return option.value === value.value;
                }}
                placeholder={intl.formatMessage({ id: 'userManagement.enterRole' })}
                formControlStyle={{ width: 300, marginRight: 12 }}
                onChange={(e: any, value: some | null) => {
                  formik.setFieldValue('role', value);
                }}
                getOptionLabel={value => value.name}
                options={roleOptions}
                errorMessage={
                  formik.errors.role && formik.submitCount > 0 ? formik.errors.role : undefined
                }
              />
              <Link
                to={{
                  pathname: ROUTES.generalSetting.roleManagement,
                }}
                target="blank"
              >
                <IconButton style={{ marginBottom: 20, padding: 4 }}>
                  <InformationIcon />
                </IconButton>
              </Link>
            </Row>

            {info.id && (
              <Row style={{ marginBottom: 12 }}>
                <Typography
                  variant="body2"
                  style={{ minWidth: 100, marginRight: 16, marginBottom: 20 }}
                >
                  <FormattedMessage id="status" />
                </Typography>
                <SingleSelect
                  value={formik.values.isActive}
                  placeholder={intl.formatMessage({ id: 'userManagement.enterRole' })}
                  formControlStyle={{ width: 300 }}
                  onSelectOption={(value: boolean) => {
                    formik.setFieldValue('isActive', value);
                  }}
                  getOptionLabel={value => intl.formatMessage({ id: value.name })}
                  options={activeStatusOptions.slice(1)}
                  errorMessage={
                    formik.errors.isActive && formik.submitCount > 0
                      ? formik.errors.isActive
                      : undefined
                  }
                />
              </Row>
            )}

            <Row style={{ marginLeft: 116 }}>
              <LoadingButton
                loading={loading}
                variant="contained"
                color="secondary"
                style={{ marginRight: 16, minWidth: 140 }}
                size="large"
                type="submit"
                disableElevation
              >
                <FormattedMessage id="save" />
              </LoadingButton>
              <Button
                variant="outlined"
                style={{ minWidth: 140 }}
                size="large"
                onClick={() => dispatch(goBackAction())}
              >
                <FormattedMessage id="reject" />
              </Button>
            </Row>
          </Col>

          <Col
            style={{ marginBottom: 12, marginTop: 24, alignItems: 'flex-start', marginLeft: 16 }}
          >
            <Row style={{ marginBottom: 12 }}>
              <Typography
                variant="body2"
                style={{ minWidth: 180, marginRight: 16, marginBottom: 20 }}
              >
                <FormattedMessage id="birthday" />
                &nbsp;{redMark}
              </Typography>
              <BirthDayField
                inputStyle={{ width: 300 }}
                date={
                  formik.values.birthday ? moment(formik.values.birthday, DATE_FORMAT) : undefined
                }
                update={value => {
                  formik.setFieldValue('birthday', value);
                }}
                errorMessage={
                  formik.errors.birthday && formik.submitCount > 0
                    ? formik.errors.birthday
                    : undefined
                }
              />
            </Row>
            <Row style={{ marginBottom: 12 }}>
              <Typography
                variant="body2"
                style={{ minWidth: 180, marginRight: 16, marginBottom: 20 }}
              >
                <FormattedMessage id="gender" />
                &nbsp;{redMark}
              </Typography>
              <RadioGroup row style={{ paddingBottom: 16 }}>
                <Col>
                  <Row>
                    <FormControlLabel
                      value={1}
                      control={<Radio size="small" />}
                      checked={formik.values.gender === 1}
                      onChange={() => formik.setFieldValue('gender', 1)}
                      label={
                        <Typography variant="body2">
                          <FormattedMessage id="male" />
                        </Typography>
                      }
                    />
                    <FormControlLabel
                      value={0}
                      control={<Radio size="small" />}
                      checked={formik.values.gender === 0}
                      onChange={() => formik.setFieldValue('gender', 0)}
                      label={
                        <Typography variant="body2">
                          <FormattedMessage id="female" />
                        </Typography>
                      }
                    />
                  </Row>
                  <Typography variant="body2" style={{ color: RED }}>
                    {formik.errors.gender && formik.submitCount > 0
                      ? formik.errors.gender
                      : undefined}
                  </Typography>
                </Col>
              </RadioGroup>
            </Row>
            <Row style={{ marginBottom: 12 }}>
              <Typography
                variant="body2"
                style={{ minWidth: 180, marginRight: 16, marginBottom: 20 }}
              >
                <FormattedMessage id="userManagement.employeeId" />
              </Typography>
              <FormControlTextField
                id="employeeCode"
                formControlStyle={{ width: 300 }}
                placeholder={intl.formatMessage({ id: 'userManagement.enterEmployeeCode' })}
                value={formik.values.employeeCode}
                onChange={formik.handleChange}
                optional
                inputProps={{
                  maxLength: 50,
                }}
                errorMessage={
                  formik.errors.employeeCode && formik.submitCount > 0
                    ? formik.errors.employeeCode
                    : undefined
                }
              />
            </Row>
            <Row style={{ marginBottom: 12 }}>
              <Typography
                variant="body2"
                style={{ minWidth: 180, marginRight: 16, marginBottom: 20 }}
              >
                <FormattedMessage id="userManagement.personalId" />
              </Typography>
              <FormControlTextField
                id="identityNumber"
                formControlStyle={{ width: 300 }}
                placeholder={intl.formatMessage({ id: 'userManagement.enterPersonalId' })}
                value={formik.values.identityNumber}
                onChange={formik.handleChange}
                optional
                inputProps={{
                  maxLength: 50,
                }}
                errorMessage={
                  formik.errors.identityNumber && formik.submitCount > 0
                    ? formik.errors.identityNumber
                    : undefined
                }
              />
            </Row>
            <Row style={{ marginBottom: 12 }}>
              <Typography
                variant="body2"
                style={{ minWidth: 180, marginRight: 16, marginBottom: 20 }}
              >
                <FormattedMessage id="userManagement.expiredDate" />
              </Typography>
              <DateField
                style={{ width: 300 }}
                optional
                date={
                  formik.values.identityExpiredDate
                    ? moment(formik.values.identityExpiredDate, DATE_FORMAT)
                    : undefined
                }
                onChange={value => {
                  formik.setFieldValue('identityExpiredDate', value);
                }}
                isOutsideRange={(e: any) => false}
              />
            </Row>

            <Row style={{ marginBottom: 12 }}>
              <Typography
                variant="body2"
                style={{ minWidth: 180, marginRight: 16, marginBottom: 20 }}
              >
                <FormattedMessage id="userManagement.passport" />
              </Typography>
              <FormControlTextField
                id="passportNumber"
                formControlStyle={{ width: 300 }}
                placeholder={intl.formatMessage({ id: 'userManagement.enterPassport' })}
                value={formik.values.passportNumber}
                onChange={formik.handleChange}
                optional
                inputProps={{
                  maxLength: 50,
                }}
                errorMessage={
                  formik.errors.passportNumber && formik.submitCount > 0
                    ? formik.errors.passportNumber
                    : undefined
                }
              />
            </Row>

            <Row style={{ marginBottom: 12 }}>
              <Typography
                variant="body2"
                style={{ minWidth: 180, marginRight: 16, marginBottom: 20 }}
              >
                <FormattedMessage id="userManagement.dateOfExpiry" />
              </Typography>
              <DateField
                style={{ width: 300 }}
                optional
                date={
                  formik.values.passportExpiredDate
                    ? moment(formik.values.passportExpiredDate, DATE_FORMAT)
                    : undefined
                }
                onChange={value => {
                  formik.setFieldValue('passportExpiredDate', value);
                }}
                isOutsideRange={(e: any) => false}
              />
            </Row>

            <Row style={{ marginBottom: 12 }}>
              <Typography
                variant="body2"
                style={{ minWidth: 180, marginRight: 16, marginBottom: 20 }}
              >
                <FormattedMessage id="userManagement.nationality" />
              </Typography>
              <FormControlAutoComplete
                value={formik.values.nationality || null}
                getOptionSelected={(option, value) => {
                  return option.id === value.id;
                }}
                placeholder={intl.formatMessage({ id: 'userManagement.enterNationality' })}
                formControlStyle={{ width: 300 }}
                onChange={(e: any, value: some | null) => {
                  formik.setFieldValue('nationality', value);
                }}
                getOptionLabel={value => value.name}
                options={countries}
                errorMessage={
                  formik.errors.nationality && formik.submitCount > 0
                    ? formik.errors.nationality
                    : undefined
                }
              />
            </Row>

            <Row style={{ marginBottom: 12 }}>
              <Typography
                variant="body2"
                style={{ minWidth: 180, marginRight: 16, marginBottom: 20 }}
              >
                <FormattedMessage id="userManagement.national" />
              </Typography>
              <FormControlAutoComplete
                value={formik.values.country || null}
                getOptionSelected={(option, value) => {
                  return option.id === value.id;
                }}
                placeholder={intl.formatMessage({ id: 'userManagement.enterNational' })}
                formControlStyle={{ width: 300 }}
                onChange={(e: any, value: some | null) => {
                  formik.setFieldValue('country', value);
                }}
                getOptionLabel={value => value.name}
                options={countries}
                errorMessage={
                  formik.errors.country && formik.submitCount > 0
                    ? formik.errors.country
                    : undefined
                }
              />
            </Row>
          </Col>
        </Row>
      </form>
      <DialogCustom
        open={open}
        onClose={() => {
          setOpen(false);
        }}
        onAction={() => {
          setInfo(formik.values);
          setOpen(false);
        }}
        PaperProps={{ style: { minWidth: 300 } }}
        titleLabel={
          <Typography variant="subtitle1">
            <FormattedMessage id="confirm" />
          </Typography>
        }
        buttonLabel="accept"
      >
        <Col style={{ padding: '16px', minHeight: 60 }}>
          <Typography variant="body2">
            <FormattedMessage id={info.id ? 'updateConfirm' : 'createConfirm'} />
          </Typography>
        </Col>
      </DialogCustom>
    </>
  );
};

export default DepartmentForm;
