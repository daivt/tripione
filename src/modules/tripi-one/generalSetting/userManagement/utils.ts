import { some } from '../../../../constants';
import { SelectItem } from '../../../../models/object';

export interface UserManagementFilter {
  searchStr: string;
  jobTitleId?: some[];
  isActive?: boolean;
  departmentId?: some[];
}
export const defaultUserManagementFilter: UserManagementFilter = {
  searchStr: '',
};
export interface UserManagementInfo {
  id: string | null;
  employeeCode: string;
  name: string;
  email: string;
  password: string;
  jobTitle: SelectItem | null;
  isActive: boolean | null;
  department: SelectItem | null;
  role: some | null;
  phone: string;
  birthday: number | null;
  gender: number | null;
  identityNumber: string;
  identityExpiredDate: number | null;
  passportNumber: string;
  passportExpiredDate: number | null;
  nationality: some | null;
  country: SelectItem | null;

}
export const defaultUserManagementInfo: UserManagementInfo = {
  id: null,
  employeeCode: '',
  name: '',
  email: '',
  password: '',
  phone: '',
  department: null,
  jobTitle: null,
  role: null,
  isActive: true,
  birthday: null,
  gender: null,
  identityNumber: '',
  identityExpiredDate: null,
  passportNumber: '',
  passportExpiredDate: null,
  nationality: null,
  country: null,
};
