import { Button, Typography } from '@material-ui/core';
import { useSnackbar } from 'notistack';
import React, { useCallback, useState } from 'react';
import { FormattedMessage } from 'react-intl';
import { useDispatch } from 'react-redux';
import { Action } from 'redux';
import { ThunkDispatch } from 'redux-thunk';
import { API_PATHS } from '../../../../../configs/API';
import { some, SUCCESS_CODE } from '../../../../../constants';
import { AppState } from '../../../../../redux/reducers';
import { Row, snackbarSetting } from '../../../../common/components/elements';
import WarningDialog from '../../../../common/components/WarningDialog';
import { fetchThunk } from '../../../../common/redux/thunk';
import TableUpload from '../components/TableUpload';

interface Props {}

const UploadUsers: React.FC<Props> = props => {
  const dispatch = useDispatch<ThunkDispatch<AppState, null, Action<string>>>();
  const [data, setData] = useState<some[] | undefined>(undefined);
  const [error, setError] = useState<string>('');
  const [loading, setLoading] = useState(false);
  const { enqueueSnackbar, closeSnackbar } = useSnackbar();
  const [formData, setFormData] = useState<FormData | undefined>(undefined);
  const [progress, setProgress] = useState<number>(0);

  const uploadFile = React.useCallback(
    async (files: File[]) => {
      setLoading(true);
      const file = new FormData();
      file.append('file', files[0]);
      setFormData(file);
      const json = await dispatch(fetchThunk(API_PATHS.uploadUsers, 'post', file));
      if (json?.code === SUCCESS_CODE) {
        setData(json.data?.items);
        json?.message &&
          enqueueSnackbar(
            json?.message,
            snackbarSetting(key => closeSnackbar(key)),
          );
      } else {
        json?.message && setError(json.massage);
      }
      setLoading(false);
    },
    [closeSnackbar, dispatch, enqueueSnackbar],
  );

  const saveUsers = useCallback(async () => {
    if (formData) {
      setProgress(0.1);
      const json = await dispatch(fetchThunk(API_PATHS.saveUsers, 'post', formData));
      if (json?.code === SUCCESS_CODE) {
        let per = 0;
        let uploadUsers = [];
        while (per < 100) {
          const res = await dispatch(
            fetchThunk(API_PATHS.processUsers, 'post', JSON.stringify(json.data)),
          );
          if (res?.code === SUCCESS_CODE) {
            per = res.data.per;
            uploadUsers = res.data.uploadUsers;
          }
          await new Promise(resolve => setTimeout(resolve, 200));
        }
        setProgress(per);
        setData(uploadUsers);
      } else {
        json?.message &&
          enqueueSnackbar(
            json.message,
            snackbarSetting(key => closeSnackbar(key), {
              color: 'error',
            }),
          );
      }
    }
  }, [closeSnackbar, dispatch, enqueueSnackbar, formData]);

  return (
    <>
      <TableUpload
        data={data}
        loading={loading}
        uploadFile={uploadFile}
        saveUsers={saveUsers}
        progress={progress}
      />
      <WarningDialog
        open={!!error}
        footerContent={
          <Row style={{ padding: 16, justifyContent: 'center' }}>
            <Button
              variant="contained"
              color="secondary"
              style={{ marginRight: 12, minWidth: 108 }}
              onClick={() => setError('')}
              disableElevation
            >
              <FormattedMessage id="accept" />
            </Button>
          </Row>
        }
        onClose={() => {
          setError('');
        }}
        onAccept={() => {
          setError('');
        }}
        messageContent={
          <Typography variant="body2" style={{ padding: 16 }}>
            {error}
          </Typography>
        }
      />
    </>
  );
};

export default UploadUsers;
