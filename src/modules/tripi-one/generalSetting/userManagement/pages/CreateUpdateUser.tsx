import { useSnackbar } from 'notistack';
import React, { useCallback, useEffect, useMemo, useState } from 'react';
import { shallowEqual, useDispatch, useSelector } from 'react-redux';
import { Action } from 'redux';
import { ThunkDispatch } from 'redux-thunk';
import moment from 'moment';
import { some, SUCCESS_CODE } from '../../../../../constants';
import { AppState } from '../../../../../redux/reducers';
import { goBackAction } from '../../../../common/redux/reducer';
import UserForm from '../components/UserForm';
import { UserManagementInfo } from '../utils';
import { API_PATHS } from '../../../../../configs/API';
import { fetchThunk } from '../../../../common/redux/thunk';
import { snackbarSetting } from '../../../../common/components/elements';
import { DATE_FORMAT_BACK_END } from '../../../../../models/moment';
import { roleOptions } from '../../../../common/redux/constants';

interface Props {}

const CreateUpdateUser: React.FC<Props> = props => {
  const dispatch = useDispatch<ThunkDispatch<AppState, null, Action<string>>>();
  const router = useSelector((state: AppState) => state.router, shallowEqual);

  const getActionData = useMemo(() => {
    const state = router.location.state as some;
    const data = state && (state.userManagementData as UserManagementInfo);
    return {
      ...data,
      role: roleOptions.find((v: some) => v.value === data?.role) || null,
    };
  }, [router.location.state]);

  const [countries, setCountries] = useState([]);
  const [loading, setLoading] = useState(false);
  const { enqueueSnackbar, closeSnackbar } = useSnackbar();

  const getAllcountries = useCallback(async () => {
    const json = await dispatch(fetchThunk(API_PATHS.getAllCountries, 'get'));
    if (json?.data) {
      setCountries(json.data.items);
    }
  }, [dispatch]);

  const onCreateOrUpdate = useCallback(
    async (data: UserManagementInfo) => {
      setLoading(true);
      if (data.id) {
        const json = await dispatch(
          fetchThunk(
            API_PATHS.updateUsers,
            'put',
            JSON.stringify({
              ...data,
              role: data?.role?.value,
              birthday: moment(data?.birthday).format(DATE_FORMAT_BACK_END),
              identityExpiredDate: moment(data?.identityExpiredDate).isValid()
                ? moment(data?.identityExpiredDate).format(DATE_FORMAT_BACK_END)
                : null,
              passportExpiredDate: moment(data?.passportExpiredDate).isValid()
                ? moment(data?.passportExpiredDate).format(DATE_FORMAT_BACK_END)
                : null,
            }),
          ),
        );
        if (json.code === SUCCESS_CODE) {
          dispatch(goBackAction());
          enqueueSnackbar(
            json.message,
            snackbarSetting(key => closeSnackbar(key)),
          );
        } else {
          enqueueSnackbar(
            json.message,
            snackbarSetting(key => closeSnackbar(key), {
              color: 'error',
            }),
          );
        }
      } else {
        const json = await dispatch(
          fetchThunk(
            API_PATHS.updateUsers,
            'post',
            JSON.stringify({
              ...data,
              role: data?.role?.value,
              birthday: moment(data?.birthday).format(DATE_FORMAT_BACK_END),
              identityExpiredDate: moment(data?.identityExpiredDate).isValid()
                ? moment(data?.identityExpiredDate).format(DATE_FORMAT_BACK_END)
                : null,
              passportExpiredDate: moment(data?.passportExpiredDate).isValid()
                ? moment(data?.passportExpiredDate).format(DATE_FORMAT_BACK_END)
                : null,
            }),
          ),
        );
        if (json.code === SUCCESS_CODE) {
          dispatch(goBackAction());
          enqueueSnackbar(
            json.message,
            snackbarSetting(key => closeSnackbar(key)),
          );
        } else {
          enqueueSnackbar(
            json.message,
            snackbarSetting(key => closeSnackbar(key), {
              color: 'error',
            }),
          );
        }
      }
      setLoading(false);
    },
    [closeSnackbar, dispatch, enqueueSnackbar],
  );

  useEffect(() => {
    getAllcountries();
  }, [getAllcountries]);

  useEffect(() => {
    if (!getActionData) {
      dispatch(goBackAction());
    }
  }, [dispatch, getActionData]);

  if (!getActionData) {
    return null;
  }
  return (
    <UserForm
      loading={loading}
      info={getActionData}
      setInfo={onCreateOrUpdate}
      countries={countries}
    />
  );
};

export default CreateUpdateUser;
