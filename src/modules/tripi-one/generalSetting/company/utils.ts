import { some } from '../../../../constants';

export interface CompanyInfo {
  id?: number;
  name?: string;
  shortName?: string;
  address?: string;
  taxCode?: number;
  contactPersonName?: string;
  email?: string;
  companySizeRange: some | null;
  referrerPhone?: string;
  licenseAccount: some | null;
  representative?: string;
  representativePhone?: string;
}

export const defaultCompanyInfo: CompanyInfo = {
  companySizeRange: null,
  licenseAccount: null,
};
