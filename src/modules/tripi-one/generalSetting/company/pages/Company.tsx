import * as React from 'react';
import { useDispatch } from 'react-redux';
import { ThunkDispatch } from 'redux-thunk';
import { Action } from 'redux';
import { useSnackbar } from 'notistack';
import CompanyForm from '../components/CompanyForm';
import { CompanyInfo, defaultCompanyInfo } from '../utils';
import { AppState } from '../../../../../redux/reducers';
import { fetchThunk } from '../../../../common/redux/thunk';
import { API_PATHS } from '../../../../../configs/API';
import { SUCCESS_CODE } from '../../../../../constants';
import { snackbarSetting } from '../../../../common/components/elements';

interface Props {}

const Company: React.FunctionComponent<Props> = props => {
  const dispatch = useDispatch<ThunkDispatch<AppState, null, Action<string>>>();
  const [data, setData] = React.useState<CompanyInfo | undefined>(undefined);
  const { enqueueSnackbar, closeSnackbar } = useSnackbar();

  const fetchData = React.useCallback(async () => {
    const json = await dispatch(fetchThunk(API_PATHS.companyGeneral, 'get'));
    if (json?.data) {
      setData(json.data);
    }
  }, [dispatch]);

  const onUpdate = React.useCallback(
    async (info: CompanyInfo) => {
      const json = await dispatch(
        fetchThunk(API_PATHS.companyGeneral, 'put', JSON.stringify(info)),
      );
      if (json?.code === SUCCESS_CODE) {
        enqueueSnackbar(
          json.message,
          snackbarSetting(key => closeSnackbar(key)),
        );
      } else {
        enqueueSnackbar(
          json.message,
          snackbarSetting(key => closeSnackbar(key), {
            color: 'error',
          }),
        );
      }
    },
    [closeSnackbar, dispatch, enqueueSnackbar],
  );

  React.useEffect(() => {
    fetchData();
  }, [fetchData]);

  return <CompanyForm data={data || defaultCompanyInfo} onUpdateInfo={onUpdate} />;
};

export default Company;
