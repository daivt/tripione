import { Typography } from '@material-ui/core';
import { useFormik } from 'formik';
import * as React from 'react';
import { FormattedMessage, useIntl } from 'react-intl';
import { shallowEqual, useSelector } from 'react-redux';
import * as yup from 'yup';
import { GREY_600 } from '../../../../../configs/colors';
import { some } from '../../../../../constants';
import { AppState } from '../../../../../redux/reducers';
import { Row } from '../../../../common/components/elements';
import { NumberFormatCustom2, redMark } from '../../../../common/components/Form';
import FormControlAutoComplete from '../../../../common/components/FormControlAutoComplete';
import FormControlTextField from '../../../../common/components/FormControlTextField';
import LoadingButton from '../../../../common/components/LoadingButton';
import { CompanyInfo } from '../utils';

interface Props {
  data: CompanyInfo;
  onUpdateInfo(value: CompanyInfo): void;
}

const CompanyForm: React.FunctionComponent<Props> = props => {
  const { onUpdateInfo, data } = props;
  const intl = useIntl();
  const generalCompanySize = useSelector(
    (state: AppState) => state.common.generalCompanySize,
    shallowEqual,
  );

  const storeSchema = yup.object().shape({
    companySizeRange: yup
      .object()
      .nullable()
      .required(intl.formatMessage({ id: 'required' })),
    representative: yup
      .string()
      .trim()
      .required(intl.formatMessage({ id: 'required' })),
    representativePhone: yup
      .string()
      .trim()
      .required(intl.formatMessage({ id: 'required' })),
  });

  const formik = useFormik({
    initialValues: data,
    onSubmit: values => {
      onUpdateInfo(values);
    },
    validationSchema: storeSchema,
  });

  React.useEffect(() => {
    formik.setValues(data);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [data]);

  return (
    <form onSubmit={formik.handleSubmit}>
      <Typography variant="body2" style={{ margin: '4px 0', color: GREY_600 }}>
        <FormattedMessage id="company.infoInstruction" />
      </Typography>
      <Row style={{ marginBottom: 12, marginTop: 24 }}>
        <Typography variant="body2" style={{ minWidth: 130, marginRight: 20, marginBottom: 20 }}>
          <FormattedMessage id="company.companyName" />
        </Typography>
        <FormControlTextField
          id="name"
          disabled
          formControlStyle={{ width: 680 }}
          placeholder={intl.formatMessage({ id: 'company.enterCompanyName' })}
          value={formik.values.name}
          optional
        />
      </Row>
      <Row style={{ marginBottom: 12 }}>
        <Typography variant="body2" style={{ minWidth: 130, marginRight: 20, marginBottom: 20 }}>
          <FormattedMessage id="company.shortName" />
        </Typography>
        <FormControlTextField
          id="shortName"
          formControlStyle={{ width: 680 }}
          placeholder={intl.formatMessage({ id: 'company.enterShortName' })}
          value={formik.values.shortName}
          onChange={formik.handleChange}
          inputProps={{
            maxLength: 50,
          }}
        />
      </Row>
      <Row style={{ marginBottom: 12 }}>
        <Typography variant="body2" style={{ minWidth: 130, marginRight: 20, marginBottom: 20 }}>
          <FormattedMessage id="company.taxNumber" />
        </Typography>
        <FormControlTextField
          disabled
          id="taxCode"
          formControlStyle={{ width: 300, maxWidth: 600, marginRight: 12 }}
          value={formik.values.taxCode}
        />
      </Row>
      <Row style={{ marginBottom: 12 }}>
        <Typography variant="body2" style={{ minWidth: 130, marginRight: 20, marginBottom: 20 }}>
          <FormattedMessage id="company.scale" />
          &nbsp;{redMark}
        </Typography>
        <FormControlAutoComplete
          value={formik.values.companySizeRange}
          formControlStyle={{ width: 300, maxWidth: 600, margin: 0 }}
          placeholder={intl.formatMessage({ id: 'chooseCompanySize' })}
          onChange={(e: any, value: any | null) => {
            formik.setFieldValue('companySizeRange', value);
          }}
          getOptionLabel={value =>
            intl.formatMessage(
              {
                id: 'companySize.value',
              },
              {
                from: value.from,
                to: value.to,
              },
            )
          }
          getOptionSelected={(option: some, value: some) => {
            return option.from === value.from;
          }}
          options={generalCompanySize}
          errorMessage={
            formik.errors.companySizeRange && formik.submitCount > 0
              ? formik.errors.companySizeRange
              : undefined
          }
        />
      </Row>
      <Row style={{ marginBottom: 12 }}>
        <Typography variant="body2" style={{ minWidth: 130, marginRight: 20, marginBottom: 20 }}>
          <FormattedMessage id="company.address" /> &nbsp;{redMark}
        </Typography>
        <FormControlTextField
          disabled
          id="address"
          formControlStyle={{ width: 680 }}
          placeholder={intl.formatMessage({ id: 'enterAddress' })}
          value={formik.values.address}
        />
      </Row>
      <Row style={{ marginBottom: 12 }}>
        <Typography variant="body2" style={{ minWidth: 130, marginRight: 20, marginBottom: 20 }}>
          <FormattedMessage id="email" /> &nbsp;{redMark}
        </Typography>
        <FormControlTextField
          id="email"
          disabled
          formControlStyle={{ width: 680 }}
          placeholder={intl.formatMessage({ id: 'enterEmail' })}
          value={formik.values.email}
        />
      </Row>
      <Row style={{ marginBottom: 12 }}>
        <Typography variant="body2" style={{ minWidth: 130, marginRight: 20, marginBottom: 20 }}>
          <FormattedMessage id="phoneNumber" /> &nbsp;{redMark}
        </Typography>
        <FormControlTextField
          name="representativePhone"
          formControlStyle={{ width: 680 }}
          placeholder={intl.formatMessage({ id: 'enterPhoneNumber' })}
          value={formik.values.representativePhone}
          onChange={formik.handleChange}
          inputProps={{
            maxLength: 15,
          }}
          inputComponent={NumberFormatCustom2 as any}
          errorMessage={
            formik.errors.representativePhone && formik.submitCount > 0
              ? formik.errors.representativePhone
              : undefined
          }
        />
      </Row>
      <Row style={{ marginBottom: 12 }}>
        <Typography variant="body2" style={{ minWidth: 130, marginRight: 20, marginBottom: 20 }}>
          <FormattedMessage id="company.contactName" /> &nbsp;{redMark}
        </Typography>
        <FormControlTextField
          id="representative"
          formControlStyle={{ width: 680 }}
          placeholder={intl.formatMessage({ id: 'company.enterContactName' })}
          value={formik.values.representative}
          onChange={formik.handleChange}
          inputProps={{
            maxLength: 50,
          }}
          errorMessage={
            formik.errors.representative && formik.submitCount > 0
              ? formik.errors.representative
              : undefined
          }
        />
      </Row>
      <Row style={{ marginBottom: 12 }}>
        <Typography
          variant="body2"
          style={{ width: 130, minWidth: 130, marginRight: 20, marginBottom: 20 }}
        >
          <FormattedMessage id="company.introducerPhone" />
        </Typography>
        <FormControlTextField
          id="referrerPhone"
          formControlStyle={{ width: 680 }}
          placeholder={intl.formatMessage({ id: 'enterPhoneNumber' })}
          value={formik.values.referrerPhone}
          onChange={e => formik.setFieldValue('referrerPhone', e.target.value)}
          inputProps={{
            maxLength: 15,
          }}
          inputComponent={NumberFormatCustom2 as any}
        />
      </Row>
      <LoadingButton
        style={{ marginLeft: 150, minWidth: 160 }}
        type="submit"
        variant="contained"
        color="secondary"
        size="large"
        loading={!data}
        disableElevation
      >
        <FormattedMessage id="save" />
      </LoadingButton>
    </form>
  );
};

export default CompanyForm;
