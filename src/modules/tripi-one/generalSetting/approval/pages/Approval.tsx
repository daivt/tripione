import { Divider, Tab, Tabs } from '@material-ui/core';
import { useSnackbar } from 'notistack';
import queryString from 'query-string';
import * as React from 'react';
import { useIntl } from 'react-intl';
import { useDispatch } from 'react-redux';
import { useHistory, useLocation } from 'react-router';
import SwipeableViews from 'react-swipeable-views';
import { Action } from 'redux';
import { ThunkDispatch } from 'redux-thunk';
import { API_PATHS } from '../../../../../configs/API';
import { some, SUCCESS_CODE } from '../../../../../constants';
import { defaultPaginationFilter, PaginationFilter } from '../../../../../models/pagination';
import { AppState } from '../../../../../redux/reducers';
import { snackbarSetting } from '../../../../common/components/elements';
import LoadingIcon from '../../../../common/components/LoadingIcon';
import { goToReplace } from '../../../../common/redux/reducer';
import { fetchThunk } from '../../../../common/redux/thunk';
import ApprovalBox from '../components/ApprovalBox';
import ExceptionBox from '../components/ExceptionBox';
import { defaultExceptionApprovalFilter, ExceptionApprovalFilter } from '../utils';

interface Props {}

const Approval: React.FunctionComponent<Props> = props => {
  const dispatch = useDispatch<ThunkDispatch<AppState, null, Action<string>>>();
  const [approvalData, setApprovalData] = React.useState<some | undefined>(undefined);
  const [exceptionData, setExceptionData] = React.useState<some | undefined>(undefined);
  const [filter, setFilter] = React.useState<ExceptionApprovalFilter>(
    defaultExceptionApprovalFilter,
  );
  const [pagination, setPagination] = React.useState<PaginationFilter>(defaultPaginationFilter);
  const [tabValue, setTabValue] = React.useState(1);
  const [openCreate, setOpenCreate] = React.useState(false);
  const [loading, setLoading] = React.useState(true);
  const intl = useIntl();
  const location = useLocation();
  const history = useHistory();
  const { enqueueSnackbar, closeSnackbar } = useSnackbar();

  const updateQueryParamsTab = React.useCallback(() => {
    if (location.search) {
      const filterParams = queryString.parse(location.search) as any;
      const filterTmp = queryString.parse(filterParams.filters) as any;
      setFilter({
        ...filterTmp,
        departmentId: filterTmp.departmentId
          ? parseInt(`${filterTmp.departmentId}`, 10)
          : undefined,
        jobTitleId: filterTmp.jobTitleId ? parseInt(`${filterTmp.jobTitleId}`, 10) : undefined,
      });
      const paginationTmp = {
        page: filterParams.page ? parseInt(`${filterParams.page}`, 10) : 1,
        pageSize: filterParams.pageSize ? parseInt(`${filterParams.pageSize}`, 10) : 10,
      } as PaginationFilter;
      setPagination(paginationTmp);
      const tabIndex = filterParams.tabIndex ? parseInt(`${filterParams.tabIndex}`, 10) : 1;
      setTabValue(tabIndex);
    } else {
      dispatch(
        goToReplace({
          search: queryString.stringify({
            ...defaultPaginationFilter,
            tabIndex: 0,
            filters: queryString.stringify(defaultExceptionApprovalFilter),
          }),
        }),
      );
    }
  }, [dispatch, location.search]);

  const fetchData = React.useCallback(
    async (
      filterParams: ExceptionApprovalFilter,
      paginationParams: PaginationFilter,
      tabIndex: number,
    ) => {
      setLoading(true);
      if (tabIndex === 0) {
        const json = await dispatch(
          fetchThunk(
            API_PATHS.approvalFlows,
            'post',
            JSON.stringify({ ...paginationParams, filters: {} }),
          ),
        );
        if (json?.code === SUCCESS_CODE) {
          setApprovalData(json?.data);
        } else {
          enqueueSnackbar(
            json.message,
            snackbarSetting(key => closeSnackbar(key), { color: 'error' }),
          );
        }
      }
      if (tabIndex === 1) {
        const json = await dispatch(
          fetchThunk(
            API_PATHS.bypassApprovalAccounts,
            'post',
            JSON.stringify({
              ...paginationParams,
              filters: {
                searchStr: filterParams.searchStr,
                department: { id: filterParams.departmentId },
                jobTitle: { id: filterParams.jobTitleId },
              },
            }),
          ),
        );
        if (json?.code === SUCCESS_CODE) {
          setExceptionData(json?.data);
        } else {
          enqueueSnackbar(
            json.message,
            snackbarSetting(key => closeSnackbar(key), { color: 'error' }),
          );
        }
      }
      setLoading(false);
    },
    [closeSnackbar, dispatch, enqueueSnackbar],
  );

  const onCreateException = React.useCallback(
    async (values: some) => {
      setLoading(true);
      const json = await dispatch(
        fetchThunk(
          API_PATHS.updateBypassApprovalAccounts,
          'post',
          JSON.stringify({
            id: values.user.id,
          }),
        ),
      );
      if (json?.code === SUCCESS_CODE) {
        setOpenCreate(false);
        setExceptionData(one => {
          return {
            ...one,
            itemList: [json.data, ...one?.itemList],
          };
        });
        enqueueSnackbar(
          json.message,
          snackbarSetting(key => closeSnackbar(key), {}),
        );
      } else {
        enqueueSnackbar(
          json.message,
          snackbarSetting(key => closeSnackbar(key), {
            color: 'error',
          }),
        );
      }
      setLoading(false);
    },
    [closeSnackbar, dispatch, enqueueSnackbar],
  );

  const onDeleteException = React.useCallback(
    async (data: some) => {
      setLoading(true);
      const json = await dispatch(
        fetchThunk(
          API_PATHS.updateBypassApprovalAccounts,
          'delete',
          JSON.stringify({
            id: data.id,
          }),
        ),
      );
      if (json?.code === SUCCESS_CODE) {
        setOpenCreate(false);
        setExceptionData(one => {
          return {
            ...one,
            itemList: one?.itemList.filter((el: some) => el.id !== data.id),
          };
        });
        enqueueSnackbar(
          json.message,
          snackbarSetting(key => closeSnackbar(key), {}),
        );
      } else {
        enqueueSnackbar(
          json.message,
          snackbarSetting(key => closeSnackbar(key), {
            color: 'error',
          }),
        );
      }
      setLoading(false);
    },
    [closeSnackbar, dispatch, enqueueSnackbar],
  );

  const onDeleteApproval = React.useCallback(
    async (data: some) => {
      setLoading(true);
      const json = await dispatch(
        fetchThunk(
          API_PATHS.updateApprovalFlows,
          'delete',
          JSON.stringify({
            appliedDepartment: {
              id: data?.appliedDepartment?.id || null,
            },
            id: data.id,
          }),
        ),
      );
      if (json?.code === SUCCESS_CODE) {
        setApprovalData(one => {
          if (data?.appliedDepartment?.id) {
            return {
              ...one,
              itemList: one?.itemList.filter((el: some) => el.id !== data.id),
            };
          }
          return {
            ...one,
            generalFlow: null,
          };
        });
        enqueueSnackbar(
          json.message,
          snackbarSetting(key => closeSnackbar(key), {}),
        );
      } else {
        enqueueSnackbar(
          json.message,
          snackbarSetting(key => closeSnackbar(key), {
            color: 'error',
          }),
        );
      }
      setLoading(false);
    },
    [closeSnackbar, dispatch, enqueueSnackbar],
  );

  React.useEffect(() => {
    fetchData(filter, pagination, tabValue);
  }, [fetchData, filter, pagination, tabValue]);

  React.useEffect(() => {
    updateQueryParamsTab();
  }, [updateQueryParamsTab]);

  return (
    <>
      <Tabs
        value={tabValue}
        onChange={(e, val) => {
          const params = new URLSearchParams(history.location.search);
          params.set('tabIndex', val);
          params.set('page', '0');
          dispatch(
            goToReplace({
              search: params.toString(),
            }),
          );
        }}
        indicatorColor="primary"
        textColor="primary"
      >
        <Tab label={intl.formatMessage({ id: 'approval.title' })} />
        <Tab label={intl.formatMessage({ id: 'approval.pass' })} />
      </Tabs>
      <Divider />
      <SwipeableViews
        index={tabValue}
        style={{ marginTop: 16 }}
        onChangeIndex={val => setTabValue(val)}
      >
        <div>
          {approvalData ? (
            <ApprovalBox
              loading={loading}
              data={approvalData}
              pagination={pagination}
              onUpdatePagination={values => {
                const params = new URLSearchParams(history.location.search);
                params.set('pageSize', `${values.pageSize}`);
                params.set('page', `${values.page}`);
                dispatch(
                  goToReplace({
                    search: params.toString(),
                  }),
                );
              }}
              onDelete={onDeleteApproval}
            />
          ) : (
            <LoadingIcon style={{ height: 320 }} />
          )}
        </div>
        <div>
          {exceptionData ? (
            <ExceptionBox
              loading={loading}
              data={exceptionData}
              filter={filter}
              pagination={pagination}
              onUpdatePagination={values => {
                const params = new URLSearchParams(history.location.search);
                params.set('pageSize', `${values.pageSize}`);
                params.set('page', `${values.page}`);
                dispatch(
                  goToReplace({
                    search: params.toString(),
                  }),
                );
              }}
              onUpdateFilter={values => {
                const params = new URLSearchParams(history.location.search);
                params.set('filters', queryString.stringify(values));
                dispatch(
                  goToReplace({
                    search: params.toString(),
                  }),
                );
              }}
              onDeleteException={onDeleteException}
              onCreateException={onCreateException}
              openCreate={openCreate}
              setOpenCreate={setOpenCreate}
            />
          ) : (
            <LoadingIcon style={{ height: 320 }} />
          )}
        </div>
      </SwipeableViews>
    </>
  );
};

export default Approval;
