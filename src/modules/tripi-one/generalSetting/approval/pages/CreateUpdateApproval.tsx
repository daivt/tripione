import { useSnackbar } from 'notistack';
import React, { useCallback, useEffect, useMemo, useState } from 'react';
import { shallowEqual, useDispatch, useSelector } from 'react-redux';
import { Action } from 'redux';
import { ThunkDispatch } from 'redux-thunk';
import { API_PATHS } from '../../../../../configs/API';
import { some, SUCCESS_CODE } from '../../../../../constants';
import { AppState } from '../../../../../redux/reducers';
import { snackbarSetting } from '../../../../common/components/elements';
import { goBackAction } from '../../../../common/redux/reducer';
import { fetchThunk } from '../../../../common/redux/thunk';
import ApprovalForm from '../components/ApprovalForm';
import { ApprovalFlowInfo } from '../utils';

interface Props {}

const CreateUpdateApproval: React.FC<Props> = props => {
  const dispatch = useDispatch<ThunkDispatch<AppState, null, Action<string>>>();
  const router = useSelector((state: AppState) => state.router, shallowEqual);
  const [loading, setLoading] = useState(false);
  const { enqueueSnackbar, closeSnackbar } = useSnackbar();

  const getActionData = useMemo(() => {
    const state = router.location.state as some;
    const data = state && (state.approvalFlowData as ApprovalFlowInfo);
    return data;
  }, [router.location.state]);

  const onCreateOrUpdate = useCallback(
    async (data: ApprovalFlowInfo) => {
      setLoading(true);
      if (data.id) {
        const json = await dispatch(
          fetchThunk(
            API_PATHS.updateApprovalFlows,
            'put',
            JSON.stringify({
              ...data,
              flow: data.flow?.map((obj: some, index: number) => {
                return {
                  step: index + 1,
                  department: obj,
                };
              }),
            }),
          ),
        );
        if (json?.code === SUCCESS_CODE) {
          enqueueSnackbar(
            json.message,
            snackbarSetting(key => closeSnackbar(key), { color: 'success' }),
          );
          dispatch(goBackAction());
        } else {
          enqueueSnackbar(
            json.message,
            snackbarSetting(key => closeSnackbar(key), { color: 'error' }),
          );
        }
      } else {
        const json = await dispatch(
          fetchThunk(
            API_PATHS.updateApprovalFlows,
            'post',
            JSON.stringify({
              ...data,
              flow: data.flow?.map((obj: some, index: number) => {
                return {
                  step: index + 1,
                  department: obj,
                };
              }),
            }),
          ),
        );
        if (json?.code === SUCCESS_CODE) {
          enqueueSnackbar(
            json.message,
            snackbarSetting(key => closeSnackbar(key), { color: 'success' }),
          );
          dispatch(goBackAction());
        } else {
          enqueueSnackbar(
            json.message,
            snackbarSetting(key => closeSnackbar(key), { color: 'error' }),
          );
        }
      }
      setLoading(false);
    },

    [closeSnackbar, dispatch, enqueueSnackbar],
  );

  useEffect(() => {
    if (!getActionData) {
      dispatch(goBackAction());
    }
  }, [dispatch, getActionData]);

  if (!getActionData) {
    return null;
  }

  return (
    <>
      <ApprovalForm
        info={getActionData}
        loading={loading}
        onCreateOrUpdate={onCreateOrUpdate}
        isGeneral={getActionData.isGeneral}
      />
    </>
  );
};

export default CreateUpdateApproval;
