import { some } from '../../../../constants';

export interface ApprovalFlowInfo {
  id?: number;
  isGeneral?: boolean;
  appliedDepartment: null | some;
  flow: some[];
}

export const defaultApprovalFlowInfo: ApprovalFlowInfo = {
  appliedDepartment: null,
  flow: [],
};

export interface ExceptionInfo {
  id: number;
  email: string;
  name: string;
  employeeId: string;
  department?: {
    id: number;
    name: string;
  };
  position?: {
    id: number;
    name: string;
  };
}

export interface ExceptionApprovalFilter {
  searchStr: string;
  departmentId?: number;
  jobTitleId?: number;
}
export const defaultExceptionApprovalFilter: ExceptionApprovalFilter = {
  searchStr: '',
};

export interface ChangeExceptionAccount {
  user?: some;
}

export const defaultChangeExceptionAccount: ChangeExceptionAccount = {};
