import { Button } from '@material-ui/core';
import { useFormik } from 'formik';
import * as React from 'react';
import { FormattedMessage, useIntl } from 'react-intl';
import { shallowEqual, useSelector } from 'react-redux';
import { AppState } from '../../../../../redux/reducers';
import { ReactComponent as IconRefresh } from '../../../../../svg/ic_refresh.svg';
import { Row } from '../../../../common/components/elements';
import FormControlTextField from '../../../../common/components/FormControlTextField';
import LoadingButton from '../../../../common/components/LoadingButton';
import SingleSelect from '../../../../common/components/SingleSelect';
import { trimObjectValues } from '../../../../utils';
import { defaultExceptionApprovalFilter, ExceptionApprovalFilter } from '../utils';

interface Props {
  params: ExceptionApprovalFilter;
  onUpdateFilter(params: ExceptionApprovalFilter): void;
  loading?: boolean;
}

const Filter: React.FunctionComponent<Props> = props => {
  const { params, onUpdateFilter, loading } = props;
  const { departmentOptions, positionOptions } = useSelector(
    (state: AppState) => state.common,
    shallowEqual,
  );
  const intl = useIntl();
  const formik = useFormik({
    initialValues: params,
    onSubmit: values => {
      onUpdateFilter(trimObjectValues(values));
    },
  });

  React.useEffect(() => {
    formik.setValues(params);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [params]);

  return (
    <form onSubmit={formik.handleSubmit} autoComplete="off">
      <Row style={{ flexWrap: 'wrap' }}>
        <FormControlTextField
          id="searchStr"
          label={<FormattedMessage id="approval.searchStr" />}
          formControlStyle={{ width: 220 }}
          placeholder={intl.formatMessage({ id: 'approval.enterSearchStr' })}
          value={formik.values.searchStr}
          onChange={formik.handleChange}
          optional
          inputProps={{
            maxLength: 100,
          }}
        />
        <SingleSelect
          value={formik.values.departmentId}
          label={<FormattedMessage id="department" />}
          formControlStyle={{ width: 220 }}
          onSelectOption={(value: any) => {
            formik.setFieldValue('departmentId', value);
          }}
          getOptionLabel={value => value.name}
          options={[
            { id: undefined, name: intl.formatMessage({ id: 'global.all' }) },
            ...departmentOptions,
          ]}
          optional
        />
        <SingleSelect
          value={formik.values.jobTitleId}
          label={<FormattedMessage id="position" />}
          formControlStyle={{ width: 220 }}
          onSelectOption={(value: any) => {
            formik.setFieldValue('jobTitleId', value);
          }}
          getOptionLabel={value => value.name}
          options={[
            { id: undefined, name: intl.formatMessage({ id: 'global.all' }) },
            ...positionOptions,
          ]}
          optional
        />
        <Row style={{ marginTop: 4 }}>
          <LoadingButton
            type="submit"
            loading={loading}
            variant="contained"
            style={{ minWidth: 160, marginRight: 16 }}
            color="secondary"
            size="large"
            disableElevation
          >
            <FormattedMessage id="search" />
          </LoadingButton>
          <Button
            size="large"
            style={{ minWidth: 0 }}
            onClick={() => {
              formik.setValues(defaultExceptionApprovalFilter);
              onUpdateFilter(defaultExceptionApprovalFilter);
            }}
          >
            <IconRefresh />
          </Button>
        </Row>
      </Row>
    </form>
  );
};

export default Filter;
