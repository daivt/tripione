import { Divider, Typography } from '@material-ui/core';
import { useFormik } from 'formik';
import * as React from 'react';
import { FormattedMessage, useIntl } from 'react-intl';
import { useDispatch } from 'react-redux';
import { ThunkDispatch } from 'redux-thunk';
import * as yup from 'yup';
import { Action } from 'redux';
import { some } from '../../../../../constants';
import { AppState } from '../../../../../redux/reducers';
import DialogCustom from '../../../../common/components/DialogCustom';
import { Col, Row } from '../../../../common/components/elements';
import FormControlAutoComplete from '../../../../common/components/FormControlAutoComplete';
import LoadingButton from '../../../../common/components/LoadingButton';
import { ChangeExceptionAccount, defaultChangeExceptionAccount } from '../utils';
import { fetchThunk } from '../../../../common/redux/thunk';
import { API_PATHS } from '../../../../../configs/API';

interface Props {
  open: boolean;
  onClose(): void;
  onCreate(value: ChangeExceptionAccount): void;
}

const CreateExceptionDialog: React.FunctionComponent<Props> = props => {
  const { open, onClose, onCreate } = props;
  const dispatch = useDispatch<ThunkDispatch<AppState, null, Action<string>>>();

  const [loading, setLoading] = React.useState(false);
  const intl = useIntl();

  const createExceptionSchema = yup.object().shape({
    user: yup
      .mixed()
      .nullable()
      .required(intl.formatMessage({ id: 'required' })),
  });

  const formik = useFormik({
    initialValues: defaultChangeExceptionAccount,
    onSubmit: values => {
      onCreate(values);
    },
    validationSchema: createExceptionSchema,
  });

  return (
    <DialogCustom
      open={open}
      onClose={onClose}
      PaperProps={{ style: { minWidth: 400 } }}
      titleLabel={
        <Typography variant="subtitle1">
          <FormattedMessage id="addNew" />
        </Typography>
      }
      disableBackdropClick
      footerContent={<div />}
    >
      <form onSubmit={formik.handleSubmit}>
        <Col style={{ padding: '24px 24px 32px 24px' }}>
          <FormControlAutoComplete
            label={<FormattedMessage id="email" />}
            placeholder={intl.formatMessage({ id: 'approval.enterSearchStr' })}
            value={formik.values.user}
            getOptionSelected={(option, value) => {
              return option.id === value.id;
            }}
            formControlStyle={{ width: '100%', margin: 0 }}
            onChange={(e: any, value: some) => {
              formik.setFieldValue('user', value);
            }}
            getOptionLabel={value => `${value.name} - ${value.email}`}
            loadOptions={async (str: string) => {
              const json = await dispatch(
                fetchThunk(
                  API_PATHS.usersList,
                  'post',
                  JSON.stringify({
                    filters: {
                      isActive: true,
                      jobTitle: null,
                      department: null,
                      searchStr: str,
                    },
                    page: 1,
                    pageSize: 5,
                  }),
                ),
              );
              return json.data?.itemList;
            }}
            options={[]}
            errorMessage={
              formik.errors.user && formik.submitCount > 0 ? formik.errors.user : undefined
            }
            autoComplete={false}
            disableClearable
          />
        </Col>
        <Divider />
        <Row style={{ padding: 16, justifyContent: 'flex-end' }}>
          <LoadingButton
            loading={loading}
            variant="contained"
            color="secondary"
            style={{ marginRight: 12, minWidth: 110 }}
            disableElevation
            type="submit"
          >
            <FormattedMessage id="accept" />
          </LoadingButton>
        </Row>
      </form>
    </DialogCustom>
  );
};

export default CreateExceptionDialog;
