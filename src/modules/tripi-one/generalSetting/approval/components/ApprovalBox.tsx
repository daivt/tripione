import { Button, Typography } from '@material-ui/core';
import * as React from 'react';
import { FormattedMessage } from 'react-intl';
import { ROUTES } from '../../../../../configs/routes';
import { some } from '../../../../../constants';
import { PaginationFilter } from '../../../../../models/pagination';
import { ReactComponent as IconDelete } from '../../../../../svg/ic_delete.svg';
import { Col, Row } from '../../../../common/components/elements';
import Link from '../../../../common/components/Link';
import PermissionDiv from '../../../../common/components/PermissionDiv';
import TableCustom, { Columns } from '../../../../common/components/TableCustom';
import WarningDialog from '../../../../common/components/WarningDialog';
import { ApprovalFlowInfo, defaultApprovalFlowInfo } from '../utils';

interface Props {
  loading: boolean;
  data?: some;
  pagination: PaginationFilter;
  onDelete(value: ApprovalFlowInfo): void;
  onUpdatePagination(pagination: PaginationFilter): void;
}

const ApprovalBox: React.FunctionComponent<Props> = props => {
  const { onDelete, data, loading, onUpdatePagination, pagination } = props;
  const [deleteData, setDeleteData] = React.useState<ApprovalFlowInfo | undefined>(undefined);
  const columnsGeneralApproval = React.useMemo(() => {
    return [
      {
        title: 'approval.apply',
        dataIndex: 'appliedDepartment',
        style: { minWidth: 150 },
        render: (record: some, index: number) => (
          <Typography variant="caption">
            <FormattedMessage id="approval.all" />
          </Typography>
        ),
      },
      {
        title: 'approval.title',
        style: { minWidth: 200 },
        dataIndex: 'flow',
        render: (record: some, index: number) => (
          <Col>
            {record?.flow?.map((v: some, i: number) => (
              <Typography variant="caption" key={i}>
                <FormattedMessage id="step" />
                &nbsp;{v.step}&nbsp;-&nbsp;{v.department?.name}
              </Typography>
            ))}
          </Col>
        ),
      },
      {
        disableAction: true,
        width: 60,
        render: (record: ApprovalFlowInfo, index: number) => (
          <PermissionDiv permission={['tripione:corporatePortal:approvalPolicy:delete']}>
            <Button
              size="small"
              color="secondary"
              style={{ width: 24, padding: 0 }}
              onClick={() => setDeleteData(record)}
            >
              <IconDelete />
            </Button>
          </PermissionDiv>
        ),
      },
      {
        disableAction: true,
        width: 120,
        render: (record: ApprovalFlowInfo, index: number) => (
          <Link
            to={{
              pathname: ROUTES.generalSetting.approval.update,
              state: {
                approvalFlowData: {
                  ...record,
                  isGeneral: true,
                },
              },
            }}
          >
            <Button size="small" color="secondary" style={{ padding: 0 }}>
              <FormattedMessage id="edit" />
            </Button>
          </Link>
        ),
      },
    ] as Columns[];
  }, []);
  const columnsPrivateApproval = React.useMemo(() => {
    return [
      {
        title: 'approval.applyRoom',
        dataIndex: 'appliedDepartment',
        style: { minWidth: 150 },
        render: (record: some, index: number) => (
          <Typography variant="caption">{record.appliedDepartment?.name}</Typography>
        ),
      },
      {
        title: 'approval.title',
        style: { minWidth: 200 },
        dataIndex: 'flow',
        render: (record: some, index: number) => (
          <Col>
            {record.flow
              .sort((x: some, y: some) => x.step - y.step)
              .map((v: some, i: number) => (
                <Typography variant="caption" key={i}>
                  <FormattedMessage id="step" />
                  &nbsp;{v.step}&nbsp;-&nbsp;{v.department?.name}
                </Typography>
              ))}
          </Col>
        ),
      },
      {
        disableAction: true,
        width: 60,
        render: (record: ApprovalFlowInfo, index: number) => (
          <PermissionDiv permission={['tripione:corporatePortal:approvalPolicy:delete']}>
            <Button
              size="small"
              color="secondary"
              style={{ width: 24, padding: 0 }}
              onClick={() => setDeleteData(record)}
            >
              <IconDelete />
            </Button>
          </PermissionDiv>
        ),
      },
      {
        disableAction: true,
        width: 120,
        render: (record: ApprovalFlowInfo, index: number) => (
          <Link
            to={{
              pathname: ROUTES.generalSetting.approval.update,
              state: { approvalFlowData: record },
            }}
          >
            <Button size="small" color="secondary" style={{ padding: 0 }}>
              <FormattedMessage id="edit" />
            </Button>
          </Link>
        ),
      },
    ] as Columns[];
  }, []);
  return (
    <>
      <TableCustom
        dataSource={data?.generalFlow && [data?.generalFlow]}
        hiddenHeader={!data?.generalFlow || data?.generalFlow.length === 0}
        loading={loading}
        columns={columnsGeneralApproval}
        style={{ maxWidth: 850, paddingBottom: 24 }}
        header={
          <Row style={{ justifyContent: 'space-between', padding: '16px 12px' }}>
            <Typography variant="subtitle2">
              <FormattedMessage id="approval.general" />
            </Typography>
          </Row>
        }
        caption={
          <Col style={{ alignItems: 'center', padding: '16px 12px' }}>
            <Typography variant="subtitle2">
              <FormattedMessage id="approval.noDataGeneralApproval" />
            </Typography>
            <Link
              to={{
                pathname: ROUTES.generalSetting.approval.create,
                state: {
                  approvalFlowData: {
                    ...defaultApprovalFlowInfo,
                    isGeneral: true,
                  },
                },
              }}
            >
              <Button
                variant="contained"
                color="secondary"
                size="large"
                style={{ width: 200, marginTop: 16 }}
                disableElevation
              >
                <FormattedMessage id="approval.add" />
              </Button>
            </Link>
          </Col>
        }
      />
      <TableCustom
        dataSource={data?.itemList}
        hiddenHeader={data?.itemList.length === 0}
        loading={loading}
        columns={columnsPrivateApproval}
        style={{ maxWidth: 850, marginTop: 16 }}
        header={
          <Row style={{ justifyContent: 'space-between', padding: '16px 12px' }}>
            <Typography variant="subtitle2">
              <FormattedMessage id="approval.private" />
            </Typography>
            {data?.itemList?.length > 0 && (
              <Link
                to={{
                  pathname: ROUTES.generalSetting.approval.create,
                  state: { approvalFlowData: defaultApprovalFlowInfo },
                }}
              >
                <Button
                  variant="contained"
                  color="secondary"
                  style={{ minWidth: 120 }}
                  disableElevation
                >
                  <FormattedMessage id="addNew" />
                </Button>
              </Link>
            )}
          </Row>
        }
        paginationProps={{
          count: data?.totalResults || 0,
          page: pagination.page || 1,
          rowsPerPage: pagination?.pageSize || 0,
          onChangePage: (event: unknown, newPage: number) => {
            onUpdatePagination({ ...pagination, page: newPage });
          },
          onChangeRowsPerPage: (event: React.ChangeEvent<HTMLInputElement>) => {
            onUpdatePagination({
              pageSize: parseInt(event.target.value, 10),
              page: 1,
            });
          },
        }}
        caption={
          <Col style={{ alignItems: 'center', padding: '16px 12px' }}>
            <Typography variant="subtitle2">
              <FormattedMessage id="approval.noDataPrivateApproval" />
            </Typography>
            <Link
              to={{
                pathname: ROUTES.generalSetting.approval.create,
                state: { approvalFlowData: defaultApprovalFlowInfo },
              }}
            >
              <Button
                variant="contained"
                color="secondary"
                size="large"
                style={{ width: 200, marginTop: 16 }}
                disableElevation
              >
                <FormattedMessage id="approval.add" />
              </Button>
            </Link>
          </Col>
        }
      />
      <WarningDialog
        open={!!deleteData}
        onClose={() => {
          setDeleteData(undefined);
        }}
        onAccept={() => {
          deleteData && onDelete(deleteData);
          setDeleteData(undefined);
        }}
        messageContent={
          <Typography variant="body2">
            <FormattedMessage id="approval.deleteConfirm" />
          </Typography>
        }
      />
    </>
  );
};

export default ApprovalBox;
