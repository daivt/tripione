import { Button, Typography } from '@material-ui/core';
import * as React from 'react';
import { FormattedMessage } from 'react-intl';
import { some } from '../../../../../constants';
import { PaginationFilter } from '../../../../../models/pagination';
import { ReactComponent as IconDelete } from '../../../../../svg/ic_delete.svg';
import { Row } from '../../../../common/components/elements';
import TableCustom, { Columns } from '../../../../common/components/TableCustom';
import WarningDialog from '../../../../common/components/WarningDialog';
import { ExceptionApprovalFilter, ExceptionInfo } from '../utils';
import CreateExceptionDialog from './CreateExceptionDialog';
import Filter from './Filter';

interface Props {
  loading: boolean;
  data?: some;
  filter: ExceptionApprovalFilter;
  pagination: PaginationFilter;
  onDeleteException(value: ExceptionInfo): void;
  onCreateException(values: some): void;
  onUpdateFilter(filter: ExceptionApprovalFilter): void;
  onUpdatePagination(pagination: PaginationFilter): void;
  openCreate: boolean;
  setOpenCreate(value: boolean): void;
}

const ExceptionBox: React.FunctionComponent<Props> = props => {
  const {
    onDeleteException,
    data,
    loading,
    filter,
    onUpdateFilter,
    onUpdatePagination,
    pagination,
    onCreateException,
    openCreate,
    setOpenCreate,
  } = props;

  const [deleteData, setDeleteData] = React.useState<ExceptionInfo | undefined>(undefined);

  const columns = React.useMemo(() => {
    return [
      {
        title: 'fullName',
        dataIndex: 'name',
      },
      {
        title: 'email',
        dataIndex: 'email',
      },
      {
        title: 'department',
        dataIndex: 'department',
        render: (record: some, index: number) => (
          <Typography variant="caption">{record.department?.name}</Typography>
        ),
      },
      {
        title: 'position',
        dataIndex: 'position',
        render: (record: some, index: number) => (
          <Typography variant="caption">{record.jobTitle?.name}</Typography>
        ),
      },
      {
        disableAction: true,
        width: 60,
        render: (record: ExceptionInfo, index: number) => (
          <Button
            size="small"
            color="secondary"
            style={{ width: 24, padding: 0 }}
            onClick={() => setDeleteData(record)}
          >
            <IconDelete />
          </Button>
        ),
      },
    ] as Columns[];
  }, []);

  return (
    <>
      <Filter loading={loading} params={filter} onUpdateFilter={onUpdateFilter} />
      <TableCustom
        dataSource={data?.itemList}
        loading={loading}
        columns={columns}
        style={{ maxWidth: 750, marginTop: 24 }}
        header={
          <Row style={{ justifyContent: 'space-between', padding: '16px 12px' }}>
            <Typography variant="subtitle2">
              <FormattedMessage id="approval.listException" />
            </Typography>
            <Row>
              <Button
                variant="contained"
                color="secondary"
                style={{ minWidth: 120 }}
                disableElevation
                onClick={() => setOpenCreate(true)}
              >
                <FormattedMessage id="addNew" />
              </Button>
            </Row>
          </Row>
        }
        paginationProps={{
          count: data?.totalResults || 0,
          page: pagination.page || 1,
          rowsPerPage: pagination?.pageSize || 0,
          onChangePage: (event: unknown, newPage: number) => {
            onUpdatePagination({ ...pagination, page: newPage });
          },
          onChangeRowsPerPage: (event: React.ChangeEvent<HTMLInputElement>) => {
            onUpdatePagination({
              pageSize: parseInt(event.target.value, 10),
              page: 1,
            });
          },
        }}
      />
      <WarningDialog
        open={!!deleteData}
        onClose={() => {
          setDeleteData(undefined);
        }}
        onAccept={() => {
          deleteData && onDeleteException(deleteData);
          setDeleteData(undefined);
        }}
        messageContent={
          <Typography variant="body2">
            <FormattedMessage id="approval.deleteExceptionConfirm" />
          </Typography>
        }
      />
      <CreateExceptionDialog
        open={openCreate}
        onClose={() => setOpenCreate(false)}
        onCreate={onCreateException}
      />
    </>
  );
};

export default ExceptionBox;
