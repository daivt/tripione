import { Button, Typography } from '@material-ui/core';
import AddCircleIcon from '@material-ui/icons/AddCircle';
import CancelIcon from '@material-ui/icons/Cancel';
import { useFormik } from 'formik';
import React from 'react';
import { FormattedMessage, useIntl } from 'react-intl';
import { shallowEqual, useDispatch, useSelector } from 'react-redux';
import { Action } from 'redux';
import { ThunkDispatch } from 'redux-thunk';
import * as yup from 'yup';
import { BLUE } from '../../../../../configs/colors';
import { some } from '../../../../../constants';
import { AppState } from '../../../../../redux/reducers';
import DialogCustom from '../../../../common/components/DialogCustom';
import { Col, Row } from '../../../../common/components/elements';
import FormControlAutoComplete from '../../../../common/components/FormControlAutoComplete';
import LoadingButton from '../../../../common/components/LoadingButton';
import { goBackAction } from '../../../../common/redux/reducer';
import { ApprovalFlowInfo } from '../utils';

interface Props {
  info: ApprovalFlowInfo;
  loading: boolean;
  onCreateOrUpdate(info: ApprovalFlowInfo): void;
  isGeneral?: boolean;
}

const ApprovalForm: React.FC<Props> = props => {
  const { info, loading, onCreateOrUpdate, isGeneral } = props;
  const dispatch = useDispatch<ThunkDispatch<AppState, null, Action<string>>>();

  const intl = useIntl();
  const departmentOptions = useSelector(
    (state: AppState) => state.common.departmentOptions,
    shallowEqual,
  );
  const [open, setOpen] = React.useState(false);

  const mapFlow = React.useMemo(() => {
    return info.flow.map(v => v.department);
  }, [info.flow]);

  const approvalSchema = yup.object().shape({
    appliedDepartment: yup
      .mixed()
      .nullable()
      .test({
        name: 'appliedDepartment',
        message: intl.formatMessage({ id: 'required' }),
        test: value => {
          return isGeneral ? true : !!value;
        },
      }),
    flow: yup.array().of(
      yup
        .mixed()
        .nullable()
        .required(intl.formatMessage({ id: 'required' })),
    ),
  });

  const formik = useFormik({
    initialValues: {
      ...info,
      flow: info.id ? mapFlow : mapFlow.concat(null),
    },
    onSubmit: values => {
      setOpen(true);
    },
    validationSchema: approvalSchema,
  });

  React.useEffect(() => {
    formik.setValues(
      {
        ...info,
        flow: info.id ? mapFlow : mapFlow.concat(null),
      },
      true,
    );
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [info]);

  return (
    <>
      <form autoComplete="off" onSubmit={formik.handleSubmit}>
        <Typography variant="body2">
          <FormattedMessage
            id={
              isGeneral
                ? info.id
                  ? 'approval.updateTitleGeneral'
                  : 'approval.createTitleGeneral'
                : info.id
                ? 'approval.updateTitlePrivate'
                : 'approval.createTitlePrivate'
            }
          />
        </Typography>
        <Col style={{ marginBottom: 12, marginTop: 24, alignItems: 'flex-start' }}>
          {!isGeneral && (
            <Row>
              <Typography
                variant="body2"
                style={{ minWidth: 132, marginBottom: 20, marginRight: 16 }}
              >
                <FormattedMessage id="approval.appliedDepartment" />
              </Typography>
              <FormControlAutoComplete
                formControlStyle={{ width: 385, marginBottom: 12 }}
                value={formik.values.appliedDepartment}
                placeholder={intl.formatMessage({ id: 'department.choose' })}
                onChange={(e: any, value: some | null) => {
                  formik.setFieldValue('appliedDepartment', value);
                }}
                options={departmentOptions}
                getOptionLabel={v => v.name}
                getOptionSelected={(option, value) => {
                  return option.id === value.id;
                }}
                errorMessage={
                  formik.errors.appliedDepartment && formik.submitCount > 0
                    ? (formik.errors.appliedDepartment as string)
                    : undefined
                }
              />
            </Row>
          )}
          {formik.values.flow?.map((one: some | null, index: number) => {
            return (
              <Row key={index} style={{ marginBottom: 12 }}>
                <Typography
                  variant="body2"
                  style={{ minWidth: 132, marginBottom: 20, marginRight: 16 }}
                >
                  <FormattedMessage id="step" />
                  &nbsp;{index + 1}
                </Typography>
                <FormControlAutoComplete
                  key={index}
                  formControlStyle={{ width: 385 }}
                  value={one?.department || one || null}
                  placeholder={intl.formatMessage({ id: 'department.choose' })}
                  onChange={(e: any, value: some | null) => {
                    formik.setFieldValue('flow', [
                      ...formik.values.flow.slice(0, index),
                      value || null,
                      ...formik.values.flow.slice(index + 1),
                    ]);
                  }}
                  options={departmentOptions.filter(obj => {
                    return (
                      formik.values.flow?.findIndex(v => v?.id === obj?.id && v?.id !== one?.id) ===
                      -1
                    );
                  })}
                  getOptionLabel={v => v.name}
                  getOptionSelected={(option, value) => {
                    return option.id === value.id;
                  }}
                  clearOnBlur
                  errorMessage={
                    formik.errors.flow?.[index] && formik.submitCount > 0
                      ? (formik.errors.flow?.[index] as string)
                      : undefined
                  }
                />
                {index > 0 && (
                  <Button
                    color="secondary"
                    style={{ padding: '0px 8px', marginBottom: 20 }}
                    onClick={() => {
                      formik.setFieldValue('flow', [
                        ...formik.values.flow.slice(0, index),
                        ...formik.values.flow.slice(index + 1),
                      ]);
                    }}
                  >
                    <CancelIcon fontSize="small" style={{ marginRight: 8 }} />
                    <FormattedMessage id="delete" />
                  </Button>
                )}
              </Row>
            );
          })}
          <div style={{ marginLeft: 148 }}>
            <Button
              style={{ color: BLUE }}
              onClick={() => formik.setFieldValue('flow', [...formik.values.flow, null], false)}
            >
              <AddCircleIcon style={{ marginRight: 8 }} />
              <Typography variant="body2">
                <FormattedMessage id="approval.addStep" />
              </Typography>
            </Button>
          </div>
          <Row style={{ marginTop: 24, marginLeft: 148 }}>
            <LoadingButton
              loading={loading}
              variant="contained"
              color="secondary"
              style={{ marginRight: 16, minWidth: 140 }}
              size="large"
              type="submit"
              disableElevation
            >
              <FormattedMessage id="save" />
            </LoadingButton>
            <Button
              variant="outlined"
              style={{ minWidth: 140 }}
              size="large"
              onClick={() => dispatch(goBackAction())}
            >
              <FormattedMessage id="reject" />
            </Button>
          </Row>
        </Col>
      </form>
      <DialogCustom
        open={open}
        onClose={() => {
          setOpen(false);
        }}
        onAction={() => {
          onCreateOrUpdate(formik.values);
          setOpen(false);
        }}
        PaperProps={{ style: { minWidth: 380 } }}
        titleLabel={
          <Typography variant="subtitle1">
            <FormattedMessage id="confirm" />
          </Typography>
        }
        buttonLabel="accept"
      >
        <Col style={{ padding: '16px', minHeight: 60 }}>
          <Typography variant="body2">
            <FormattedMessage id="createConfirm" />
          </Typography>
        </Col>
      </DialogCustom>
    </>
  );
};

export default ApprovalForm;
