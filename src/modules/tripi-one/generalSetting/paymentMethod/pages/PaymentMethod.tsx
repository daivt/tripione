import * as React from 'react';
import { useDispatch } from 'react-redux';
import { Action } from 'redux';
import { ThunkDispatch } from 'redux-thunk';
import { some } from '../../../../../constants';
import { AppState } from '../../../../../redux/reducers';
import LoadingIcon from '../../../../common/components/LoadingIcon';
import TableBox from '../components/TableBox';
import { fakePaymentMethod } from '../constant';

interface Props {}

const PaymentMethod: React.FunctionComponent<Props> = props => {
  const dispatch = useDispatch<ThunkDispatch<AppState, null, Action<string>>>();
  const [data, setData] = React.useState<some | undefined>(undefined);
  const [loading, setLoading] = React.useState(true);

  const fetchData = React.useCallback(() => {
    setLoading(true);
    // const json = await dispatch(
    //   fetchThunk(
    //     API_PATHS.lessons,
    //     'post',
    //     JSON.stringify({ ...filterParams, status: filterParams.status?.id }),
    //   ),
    // );
    // if (json.code === SUCCESS_CODE) {
    //   setData(json.data);
    // }
    // if (json.code !== SUCCESS_CODE) {
    //   enqueueSnackbar(
    //     json.message,
    //     snackbarSetting(key => closeSnackbar(key)),
    //   );
    // }
    setLoading(false);
    setData(fakePaymentMethod.data);
  }, []);

  const onUpdate = React.useCallback((dataTmp: some[]) => {
    setLoading(true);
    console.log(dataTmp);

    // const json = await dispatch(
    //   fetchThunk(
    //     API_PATHS.lessons,
    //     'post',
    //     JSON.stringify({ ...filterParams, status: filterParams.status?.id }),
    //   ),
    // );
    // if (json.code === SUCCESS_CODE) {
    //   setData(json.data);
    // }
    // if (json.code !== SUCCESS_CODE) {
    //   enqueueSnackbar(
    //     json.message,
    //     snackbarSetting(key => closeSnackbar(key)),
    //   );
    // }
    setLoading(false);
  }, []);

  const fetchCurrentAmount = React.useCallback(() => {
    setLoading(true);
    setData(fakePaymentMethod.data);
    setLoading(false);
  }, []);

  React.useEffect(() => {
    fetchData();
    fetchCurrentAmount();
  }, [fetchCurrentAmount, fetchData]);

  if (!data) {
    return <LoadingIcon style={{ height: 320 }} />;
  }
  return <TableBox loading={loading} data={data} onUpdate={onUpdate} />;
};

export default PaymentMethod;
