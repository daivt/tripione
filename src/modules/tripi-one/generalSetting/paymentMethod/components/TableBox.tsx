import { Button, Checkbox, Tooltip, Typography } from '@material-ui/core';
import CheckBoxIcon from '@material-ui/icons/CheckBox';
import * as React from 'react';
import { FormattedMessage } from 'react-intl';
import { GREY_300, ORANGE } from '../../../../../configs/colors';
import { some } from '../../../../../constants';
import { Col, Row } from '../../../../common/components/elements';
import LoadingButton from '../../../../common/components/LoadingButton';

interface Props {
  loading: boolean;
  data: some;
  onUpdate(data: some[]): void;
}

const TableBox: React.FunctionComponent<Props> = props => {
  const { loading, data, onUpdate } = props;
  const [info, setInfo] = React.useState<some[]>(data.itemList);

  React.useEffect(() => {
    setInfo(data.itemList);
  }, [data]);

  return (
    <Col style={{ maxWidth: 650 }}>
      {info.map((object: some, index: number) => (
        <Row
          key={object.id}
          style={{
            padding: '12px 0px',

            borderBottom: `1px solid ${GREY_300}`,
          }}
        >
          <Checkbox
            checked={object.isActive}
            color="primary"
            style={{
              marginRight: 24,
              padding: 0,
              color: object.isActive ? ORANGE : undefined,
            }}
            checkedIcon={
              <CheckBoxIcon
                style={{
                  color: ORANGE,
                }}
              />
            }
            onClick={() => {
              setInfo(
                info.map((one: some, i: number) => {
                  if (i === index) {
                    return {
                      ...one,
                      isActive:
                        info.filter((v: some) => v.isActive).length > 1 ? !one.isActive : true,
                    };
                  }
                  return one;
                }),
              );
            }}
          />
          {object.logo && <img src={object.logo} alt="" />}
          <Row style={{ marginLeft: 8, overflow: 'hidden' }}>
            <Typography variant="body2" style={{ whiteSpace: 'nowrap' }}>
              {object.name}
            </Typography>
            {object?.note?.length > 0 && (
              <Tooltip title={object.note?.join(', ')}>
                <Typography
                  variant="body2"
                  style={{
                    color: ORANGE,
                    whiteSpace: 'nowrap',
                    overflow: 'hidden',
                    textOverflow: 'ellipsis',
                  }}
                >
                  &nbsp;-&nbsp;
                  <FormattedMessage id="paymentMethod.promotion" />
                  :&nbsp;
                  {object.note?.join(', ')}
                </Typography>
              </Tooltip>
            )}
          </Row>
        </Row>
      ))}

      <Row style={{ marginTop: 24 }}>
        <LoadingButton
          loading={loading}
          variant="contained"
          color="secondary"
          size="large"
          disableElevation
          style={{ marginRight: 24, minWidth: 160 }}
          onClick={() => onUpdate(info)}
        >
          <FormattedMessage id="save" />
        </LoadingButton>
        <Button
          variant="outlined"
          size="large"
          style={{ minWidth: 120 }}
          onClick={() => setInfo(data.itemList)}
        >
          <FormattedMessage id="reject" />
        </Button>
      </Row>
    </Col>
  );
};

export default TableBox;
