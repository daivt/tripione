export interface PaymentMethodInfo {
  id?: number;
  name: string;
  logo: string;
  note: string[];
  isActive: boolean;
}

export const defaultPaymentMethodInfo: PaymentMethodInfo = {
  name: '',
  logo: '',
  note: [],
  isActive: true,
};
