export const fakePaymentMethod = {
  code: 'SUCCESS',
  message: 'success',
  data: {
    totalResults: 5,
    page: 1,
    pageSize: 10,
    itemList: [
      {
        id: 7,
        name: 'Thanh toán bằng Tripi Credits',
        logo: 'https://www.iconsdb.com/icons/download/black/credit-card-2-16.ico',
        note: [
          'Giảm 10.000 đ trên tổng giá trị đơn hàng',
          'Giảm 5.000 đ trên tổng giá trị đơn hàng',
        ],
        isActive: false,
      },
      {
        id: 2,
        name: 'Thanh toán bằng VNPay QR Code',
        logo: 'https://www.iconsdb.com/icons/download/black/qr-code-16.ico',
        note: null,
        isActive: false,
      },
      {
        id: 3,
        name: 'Thanh toán bằng phương thức giữ chỗ',
        logo: null,
        note: null,
        isActive: false,
      },
      {
        id: 4,
        name: 'Thanh toán bằng Thẻ ATM/Tài khoản ngân hàng',
        logo: 'https://www.iconsdb.com/icons/download/black/atm-16.ico',
        note: null,
        isActive: false,
      },
      {
        id: 5,
        name: 'Thanh toán bằng Thẻ Visa, Master Card',
        logo: 'https://www.iconsdb.com/icons/download/blue/visa-16.ico',
        note: null,
        isActive: false,
      },
    ],
  },
};

export const fakeTransaction = {
  code: 'SUCCESS',
  message: 'success',
  data: {
    companyName: 'Công ty TNHH Lữ Hành Du Lịch Tân Kỷ Nguyên',
    amount: 12345678900,
    currencyCode: 'VND',
  },
};
