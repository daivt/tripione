import { debounce } from 'lodash';
import * as React from 'react';
import { some } from '../../../../../constants';
import TableBox from '../component/TableBox';
import { fakeRoleData } from '../constant';

interface Props {}
const RoleManagement: React.FunctionComponent<Props> = props => {
  const [data, setData] = React.useState<some | undefined>(undefined);
  const [loading, setLoading] = React.useState(true);

  const fetchData = React.useCallback(
    debounce(
      async () => {
        setLoading(true);
        // const json = await dispatch(
        //   fetchThunk(
        //     API_PATHS.lessons,
        //     'post',
        //     JSON.stringify({ ...filterParams, status: filterParams.status?.id }),
        //   ),
        // );
        // if (json.code === SUCCESS_CODE) {
        //   setData(json.data);
        // }
        // if (json.code !== SUCCESS_CODE) {
        //   enqueueSnackbar(
        //     json.message,
        //     snackbarSetting(key => closeSnackbar(key)),
        //   );
        // }
        setLoading(false);
        setData(fakeRoleData.data);
      },
      200,
      {
        trailing: true,
        leading: false,
      },
    ),
    [],
  );
  React.useEffect(() => {
    fetchData();
  }, [fetchData]);

  return <TableBox data={data} />;
};

export default RoleManagement;
