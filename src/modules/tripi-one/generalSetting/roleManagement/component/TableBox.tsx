import { Paper, Typography } from '@material-ui/core';
import DoneIcon from '@material-ui/icons/Done';
import React from 'react';
import { GREY_700 } from '../../../../../configs/colors';
import { some } from '../../../../../constants';
import { Col, Row } from '../../../../common/components/elements';

interface Props {
  data?: some;
}
export const TableBox: React.FC<Props> = props => {
  const { data } = props;
  return (
    <div>
      {data?.itemList.map((obj: some, index: number) => (
        <Paper key={index} style={{ marginBottom: 24, padding: '20px 8px' }}>
          <Row style={{ alignItems: 'flex-start' }}>
            <Typography variant="subtitle2" style={{ minWidth: 100 }}>
              {obj.name}:
            </Typography>

            <Col>
              <Typography variant="body2">{obj.description}</Typography>
              <Row style={{ flexWrap: 'wrap', marginTop: 24 }}>
                {obj.permittedActions.map((val: string, i: number) => (
                  <Row key={i} style={{ flexBasis: '33%', padding: '4px 0px' }}>
                    <DoneIcon style={{ height: 24, width: 24, color: GREY_700, marginRight: 8 }} />
                    <Typography variant="body2">{val}</Typography>
                  </Row>
                ))}
              </Row>
            </Col>
          </Row>
        </Paper>
      ))}
    </div>
  );
};

export default TableBox;
