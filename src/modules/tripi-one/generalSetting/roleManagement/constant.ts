export const fakeRoleData = {
  code: 'SUCCESS',
  message: 'success',
  data: {
    itemList: [
      {
        id: 1,
        name: 'Chủ quản',
        description:
          'Là người quản trị doanh nghiệp, có quyền truy cập và thực hiện toàn bộ các tính năng trên hệ thống Web Portal và mobile App theo danh sách tính năng sau:',
        permittedActions: [
          'Thiết lập chuyến đi',
          'Thiết lập yêu cầu phát sinh',
          'Phê duyệt chuyến đi',
          'Phê duyệt yêu cầu phát sinh',
          'Xem chi tiết chuyến đi',
          'Quản lý số dư tài khoản',
          'Quản lý hóa đơn',
          'Thiết lập thông tin công ty',
          'Thiết lập phòng ban',
          'Thiết lập chức vụ',
          'Thiết lập người dùng',
          'Thiết lập dịch vụ',
          'Thiết lập chính sách',
          'Phê duyệt chuyến đi',
          'Thiết lập Phương thức thanh toán',
          'Quản lý số dư tài khoản',
        ],
      },
      {
        id: 2,
        name: 'Quản trị viên',
        description:
          'Là người quản trị hệ thống, thực hiện phê duyệt để xuất vé máy bay và phòng khách sạn cho các yêu cầu chuyến đi, yêu cầu phát sinh của các thành viên, thực hiện thiết lập chuyến đi, yêu cầu phát sinh của tài khoản cá nhân, và thiết lập các thông tin chung của hệ thống theo danh sách tính năng sau trên Web Portal và mobile App:',
        permittedActions: [
          'Thiết lập chuyến đi',
          'Thiết lập yêu cầu phát sinh',
          'Phê duyệt chuyến đi',
          'Phê duyệt yêu cầu phát sinh',
          'Xem chi tiết chuyến đi',
          'Quản lý số dư tài khoản',
          'Quản lý hóa đơn',
          'Thiết lập thông tin công ty',
          'Thiết lập phòng ban',
          'Thiết lập chức vụ',
          'Thiết lập người dùng',
          'Thiết lập dịch vụ',
          'Thiết lập chính sách',
          'Phê duyệt chuyến đi',
          'Thiết lập Phương thức thanh toán',
        ],
      },
      {
        id: 3,
        name: 'Thành viên',
        description:
          'Là người tham gia hệ thống, thực hiện thiết lập, xem chi tiết chuyến đi, yêu cầu phát sinh của tài khoản cá nhân, phê duyệt yêu cầu chuyến đi và các phát sinh của các thành viên khác theo phân cấp nếu có trong thiết lập trên Web Portal và mobile App theo danh sách các tính năng sau:',
        permittedActions: ['Thiết lập chuyến đi', 'Thiết lập yêu cầu phát sinh', 'Phê duyệt chuyến đi', 'Xem chi tiết chuyến đi'],
      },
    ],
  },
};
