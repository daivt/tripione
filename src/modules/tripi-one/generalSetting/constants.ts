import { SelectItem } from '../../../models/object';

export const activeStatusOptions: SelectItem[] = [
  { id: undefined, name: 'global.all' },
  { id: true, name: 'global.active' },
  { id: false, name: 'global.inactive' },
];
