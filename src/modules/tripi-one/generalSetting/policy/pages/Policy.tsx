import { Divider, Tab, Tabs } from '@material-ui/core';
import * as React from 'react';
import { useIntl } from 'react-intl';
import { useDispatch } from 'react-redux';
import { RouteComponentProps, useLocation } from 'react-router';
import SwipeableViews from 'react-swipeable-views';
import { Action } from 'redux';
import { ThunkDispatch } from 'redux-thunk';
import { ROUTES } from '../../../../../configs/routes';
import { AppState } from '../../../../../redux/reducers';
import { goToReplace } from '../../../../common/redux/reducer';
import GroupTab from './GroupTab';
import PersonTab from './PersonTab';

interface Props extends RouteComponentProps<{ tabIndex: string }> {}

const Policy: React.FunctionComponent<Props> = props => {
  const { match } = props;
  const { tabIndex } = match.params;
  const tabValue = Number(tabIndex) || 0;
  const dispatch = useDispatch<ThunkDispatch<AppState, null, Action<string>>>();
  const location = useLocation();
  const intl = useIntl();
  return (
    <>
      <Tabs
        value={tabValue}
        onChange={(e, val) => {
          const params = new URLSearchParams(location.search);
          params.set('page', '0');
          dispatch(
            goToReplace({
              pathname: ROUTES.generalSetting.policy.result.gen(val),
              search: params.toString(),
            }),
          );
        }}
        indicatorColor="primary"
        textColor="primary"
      >
        <Tab label={intl.formatMessage({ id: 'policy.group' })} />
        <Tab label={intl.formatMessage({ id: 'policy.personal' })} />
      </Tabs>
      <Divider />
      <SwipeableViews
        index={tabValue}
        style={{ marginTop: 16 }}
        onChangeIndex={val =>
          dispatch(
            goToReplace({
              pathname: ROUTES.generalSetting.policy.result.gen(val),
            }),
          )
        }
      >
        <GroupTab />
        <PersonTab />
      </SwipeableViews>
    </>
  );
};

export default Policy;
