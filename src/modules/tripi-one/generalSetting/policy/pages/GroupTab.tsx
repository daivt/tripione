import { debounce } from 'lodash';
import { useSnackbar } from 'notistack';
import queryString from 'query-string';
import * as React from 'react';
import { useDispatch } from 'react-redux';
import { useHistory, useLocation } from 'react-router';
import { Action } from 'redux';
import { ThunkDispatch } from 'redux-thunk';
import { API_PATHS } from '../../../../../configs/API';
import { some, SUCCESS_CODE } from '../../../../../constants';
import { defaultPaginationFilter, PaginationFilter } from '../../../../../models/pagination';
import { AppState } from '../../../../../redux/reducers';
import { snackbarSetting } from '../../../../common/components/elements';
import LoadingIcon from '../../../../common/components/LoadingIcon';
import { goToReplace } from '../../../../common/redux/reducer';
import { fetchThunk } from '../../../../common/redux/thunk';
import GroupPolicy from '../components/GroupPolicy';
import { defaultPolicyGroupFilter, GroupPolicyInfo, PolicyGroupFilter } from '../utils';

interface Props {}

const GroupTab: React.FunctionComponent<Props> = props => {
  const dispatch = useDispatch<ThunkDispatch<AppState, null, Action<string>>>();
  const [groupData, setGroupData] = React.useState<some | undefined>(undefined);
  const [groupFilter, setGroupFilter] = React.useState<PolicyGroupFilter>(defaultPolicyGroupFilter);
  const [pagination, setPagination] = React.useState<PaginationFilter>(defaultPaginationFilter);
  const [loading, setLoading] = React.useState(true);
  const { enqueueSnackbar, closeSnackbar } = useSnackbar();
  const location = useLocation();
  const history = useHistory();

  const updateQueryParamsTab = React.useCallback(() => {
    if (location.search) {
      const filterParams = queryString.parse(location.search) as any;
      const groupFilterTmp = queryString.parse(filterParams.groupFilter) as any;
      const filter = {
        ...groupFilterTmp,
        departmentId: groupFilterTmp.departmentId
          ? parseInt(`${groupFilterTmp.departmentId}`, 10)
          : undefined,
        positionId: groupFilterTmp.positionId
          ? parseInt(`${groupFilterTmp.positionId}`, 10)
          : undefined,
      };
      setGroupFilter(filter);
      const paginationTmp = {
        page: filterParams.page ? parseInt(`${filterParams.page}`, 10) : 1,
        pageSize: filterParams.pageSize ? parseInt(`${filterParams.pageSize}`, 10) : 10,
      } as PaginationFilter;
      setPagination(paginationTmp);
    } else {
      dispatch(
        goToReplace({
          search: queryString.stringify({
            ...defaultPaginationFilter,
            groupFilter: queryString.stringify(defaultPolicyGroupFilter),
          }),
        }),
      );
    }
  }, [dispatch, location.search]);

  const fetchDataGroup = React.useCallback(
    debounce(
      async (groupFilterParams: PolicyGroupFilter, paginationParams: PaginationFilter) => {
        setLoading(true);
        const json = await dispatch(
          fetchThunk(
            API_PATHS.policiesGroup,
            'post',
            JSON.stringify({
              filters: {
                department: {
                  id: groupFilterParams.departmentId,
                },
                jobTitle: {
                  id: groupFilterParams.positionId,
                },
              },
              page: paginationParams.page,
              pageSize: paginationParams.pageSize,
            }),
          ),
        );
        if (json.code === SUCCESS_CODE) {
          setGroupData(json.data);
        }
        if (json.code !== SUCCESS_CODE) {
          enqueueSnackbar(
            json.message,
            snackbarSetting(key => closeSnackbar(key)),
          );
        }
        setLoading(false);
      },
      500,
      {
        trailing: true,
        leading: false,
      },
    ),
    [],
  );

  const onDeleteGroup = React.useCallback(
    async (value: GroupPolicyInfo) => {
      const json = await dispatch(
        fetchThunk(
          API_PATHS.updateGroupPolicies,
          'delete',
          JSON.stringify({
            ids: [value?.id],
          }),
        ),
      );

      if (json.code === SUCCESS_CODE) {
        setGroupData(one => ({
          ...one,
          itemList: one?.itemList.filter((item: any) => item.id !== value.id),
        }));
        enqueueSnackbar(
          json.message,
          snackbarSetting(key => closeSnackbar(key)),
        );
      } else {
        enqueueSnackbar(
          json.message,
          snackbarSetting(key => closeSnackbar(key), {
            color: 'error',
          }),
        );
      }
    },
    [closeSnackbar, dispatch, enqueueSnackbar],
  );

  React.useEffect(() => {
    fetchDataGroup(groupFilter, pagination);
  }, [fetchDataGroup, groupFilter, pagination]);

  React.useEffect(() => {
    updateQueryParamsTab();
  }, [updateQueryParamsTab]);

  return (
    <>
      <div>
        {groupData ? (
          <GroupPolicy
            loading={loading}
            data={groupData}
            pagination={pagination}
            onDeleteGroup={onDeleteGroup}
            onUpdatePagination={values => {
              const params = new URLSearchParams(history.location.search);
              params.set('pageSize', `${values.pageSize}`);
              params.set('page', `${values.page}`);
              dispatch(
                goToReplace({
                  search: params.toString(),
                }),
              );
            }}
            filter={groupFilter}
            onUpdateFilter={values => {
              const params = new URLSearchParams(history.location.search);
              params.set('groupFilter', queryString.stringify(values));
              dispatch(
                goToReplace({
                  search: params.toString(),
                }),
              );
            }}
          />
        ) : (
          <LoadingIcon style={{ height: 320 }} />
        )}
      </div>
    </>
  );
};

export default GroupTab;
