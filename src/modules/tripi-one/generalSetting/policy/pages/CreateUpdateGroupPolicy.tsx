import { useSnackbar } from 'notistack';
import React, { useCallback, useState } from 'react';
import { useIntl } from 'react-intl';
import { shallowEqual, useDispatch, useSelector } from 'react-redux';
import { Action } from 'redux';
import { ThunkDispatch } from 'redux-thunk';
import { API_PATHS } from '../../../../../configs/API';
import { some, SUCCESS_CODE } from '../../../../../constants';
import { AppState } from '../../../../../redux/reducers';
import { snackbarSetting } from '../../../../common/components/elements';
import { goBackAction } from '../../../../common/redux/reducer';
import { fetchThunk } from '../../../../common/redux/thunk';
import CreateUpdateGroupPolicyForm from '../components/CreateUpdateGroupPolicyForm';
import { GroupPolicyInfo } from '../utils';

interface Props {}

const CreateUpdateGroupPolicy: React.FC<Props> = props => {
  const dispatch = useDispatch<ThunkDispatch<AppState, null, Action<string>>>();
  const router = useSelector((state: AppState) => state.router, shallowEqual);
  const [loading, setLoading] = useState(false);
  const { enqueueSnackbar, closeSnackbar } = useSnackbar();
  const intl = useIntl();

  const getActionData = React.useMemo(() => {
    const state = router.location.state as some;
    const data = state && (state.groupPolicyData as GroupPolicyInfo);
    return data;
  }, [router.location.state]);

  const onCreateUpdate = useCallback(
    async (data: GroupPolicyInfo) => {
      setLoading(true);
      if (data.id) {
        const json = await dispatch(
          fetchThunk(API_PATHS.updateGroupPolicies, 'put', JSON.stringify(data)),
        );
        if (json?.code === SUCCESS_CODE) {
          enqueueSnackbar(
            json.message,
            snackbarSetting(key => closeSnackbar(key), { color: 'success' }),
          );
          dispatch(goBackAction());
        } else {
          enqueueSnackbar(
            json.message,
            snackbarSetting(key => closeSnackbar(key), { color: 'error' }),
          );
        }
      } else {
        const json = await dispatch(
          fetchThunk(
            API_PATHS.updateGroupPolicies,
            'post',
            JSON.stringify({
              ...data,
              departments: data.departments.map((v: some) => v.id),
              jobTitles: data.jobTitles.map((v: some) => v.id),
            }),
          ),
        );
        if (json?.code === SUCCESS_CODE) {
          enqueueSnackbar(
            json.message,
            snackbarSetting(key => closeSnackbar(key), { color: 'success' }),
          );
          dispatch(goBackAction());
        } else {
          enqueueSnackbar(
            json.message,
            snackbarSetting(key => closeSnackbar(key), { color: 'error' }),
          );
        }
      }
      setLoading(false);
    },

    [closeSnackbar, dispatch, enqueueSnackbar],
  );

  React.useEffect(() => {
    if (getActionData === undefined) {
      dispatch(goBackAction());
    }
  }, [dispatch, getActionData]);
  if (!getActionData) {
    return null;
  }

  return (
    <>
      <CreateUpdateGroupPolicyForm
        info={getActionData}
        loading={loading}
        onCreateUpdate={onCreateUpdate}
      />
    </>
  );
};

export default CreateUpdateGroupPolicy;
