import { debounce } from 'lodash';
import { useSnackbar } from 'notistack';
import queryString from 'query-string';
import * as React from 'react';
import { useDispatch } from 'react-redux';
import { useLocation } from 'react-router';
import { Action } from 'redux';
import { ThunkDispatch } from 'redux-thunk';
import { API_PATHS } from '../../../../../configs/API';
import { some, SUCCESS_CODE } from '../../../../../constants';
import { defaultPaginationFilter, PaginationFilter } from '../../../../../models/pagination';
import { AppState } from '../../../../../redux/reducers';
import { snackbarSetting } from '../../../../common/components/elements';
import LoadingIcon from '../../../../common/components/LoadingIcon';
import { goToReplace } from '../../../../common/redux/reducer';
import { fetchThunk } from '../../../../common/redux/thunk';
import PersonPolicy from '../components/PersonPolicy';
import { defaultPolicyPersonFilter, PersonPolicyInfo, PolicyPersonFilter } from '../utils';

interface Props {}

const PersonTab: React.FunctionComponent<Props> = props => {
  const dispatch = useDispatch<ThunkDispatch<AppState, null, Action<string>>>();
  const [personData, setPersonData] = React.useState<some | undefined>(undefined);
  const [personFilter, setPersonFilter] = React.useState<PolicyPersonFilter>(
    defaultPolicyPersonFilter,
  );
  const [pagination, setPagination] = React.useState<PaginationFilter>(defaultPaginationFilter);
  const [loading, setLoading] = React.useState(true);
  const { enqueueSnackbar, closeSnackbar } = useSnackbar();
  const location = useLocation();
  // const history = useHistory();

  const updateQueryParamsTab = React.useCallback(() => {
    if (location.search) {
      const filterParams = queryString.parse(location.search) as any;
      const personFilterTmp = queryString.parse(filterParams.personFilter) as any;
      setPersonFilter({
        name: personFilterTmp.name || '',
        employeeId: personFilterTmp.employeeId ? personFilterTmp.employeeId : '',
        departmentId: personFilterTmp.departmentId
          ? parseInt(`${personFilterTmp.departmentId}`, 10)
          : undefined,
        positionId: personFilterTmp.positionId
          ? parseInt(`${personFilterTmp.positionId}`, 10)
          : undefined,
      });
      const paginationTmp = {
        page: filterParams.page ? parseInt(`${filterParams.page}`, 10) : 1,
        pageSize: filterParams.pageSize ? parseInt(`${filterParams.pageSize}`, 10) : 10,
      } as PaginationFilter;
      setPagination(paginationTmp);
    } else {
      dispatch(
        goToReplace({
          search: queryString.stringify({
            ...defaultPaginationFilter,
            personFilter: queryString.stringify(defaultPolicyPersonFilter),
          }),
        }),
      );
    }
  }, [dispatch, location.search]);

  const fetchDataPerson = React.useCallback(
    debounce(
      async (personFilterParams: PolicyPersonFilter, paginationParams: PaginationFilter) => {
        setLoading(true);
        const json = await dispatch(
          fetchThunk(
            API_PATHS.policiesIndividual,
            'post',
            JSON.stringify({
              filters: {
                departmentId: personFilterParams.departmentId,
                employeeCode: personFilterParams.employeeId,
                jobTitleId: personFilterParams.positionId,
                name: personFilterParams.name,
              },
              page: paginationParams.page,
              pageSize: paginationParams.pageSize,
            }),
          ),
        );
        if (json.code === SUCCESS_CODE) {
          setPersonData(json.data);
        } else {
          enqueueSnackbar(
            json.message,
            snackbarSetting(key => closeSnackbar(key)),
          );
        }
        setLoading(false);
      },
      500,
      {
        trailing: true,
        leading: false,
      },
    ),
    [],
  );

  const onDeletePerson = React.useCallback(
    async (value: PersonPolicyInfo) => {
      const json = await dispatch(
        fetchThunk(
          API_PATHS.updateIndividualPolicies,
          'delete',
          JSON.stringify({
            ids: [value?.id],
          }),
        ),
      );

      if (json.code === SUCCESS_CODE) {
        setPersonData(one => ({
          ...one,
          itemList: one?.itemList.filter((item: any) => item.id !== value.id),
        }));
        enqueueSnackbar(
          json.message,
          snackbarSetting(key => closeSnackbar(key)),
        );
      } else {
        enqueueSnackbar(
          json.message,
          snackbarSetting(key => closeSnackbar(key), {
            color: 'error',
          }),
        );
      }
    },
    [closeSnackbar, dispatch, enqueueSnackbar],
  );

  React.useEffect(() => {
    fetchDataPerson(personFilter, pagination);
  }, [personFilter, pagination, fetchDataPerson]);

  React.useEffect(() => {
    updateQueryParamsTab();
  }, [updateQueryParamsTab]);

  return (
    <>
      {personData ? (
        <PersonPolicy
          loading={loading}
          data={personData}
          pagination={pagination}
          onDeletePerson={onDeletePerson}
          onUpdatePagination={values => {
            const params = new URLSearchParams(location.search);
            params.set('pageSize', values.pageSize.toString());
            params.set('page', values.page.toString());
            dispatch(
              goToReplace({
                search: params.toString(),
              }),
            );
          }}
          filter={personFilter}
          onUpdateFilter={values => {
            const params = new URLSearchParams(location.search);
            params.set('personFilter', queryString.stringify(values));
            dispatch(
              goToReplace({
                search: params.toString(),
              }),
            );
          }}
        />
      ) : (
        <LoadingIcon style={{ height: 320 }} />
      )}
    </>
  );
};

export default PersonTab;
