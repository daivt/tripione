import { Button, Typography } from '@material-ui/core';
import * as React from 'react';
import { FormattedMessage, FormattedNumber } from 'react-intl';
import { ROUTES } from '../../../../../configs/routes';
import { some } from '../../../../../constants';
import { PaginationFilter } from '../../../../../models/pagination';
import { ReactComponent as IconDelete } from '../../../../../svg/ic_delete.svg';
import { ReactComponent as IconEdit } from '../../../../../svg/ic_edit.svg';
import { Row } from '../../../../common/components/elements';
import Link from '../../../../common/components/Link';
import PermissionDiv from '../../../../common/components/PermissionDiv';
import TableCustom, { Columns } from '../../../../common/components/TableCustom';
import WarningDialog from '../../../../common/components/WarningDialog';
import { defaultGroupPolicyInfo, GroupPolicyInfo, PolicyGroupFilter } from '../utils';
import GroupFilter from './GroupFilter';

interface Props {
  loading: boolean;
  data?: some;
  filter: PolicyGroupFilter;
  pagination: PaginationFilter;
  onUpdateFilter(filter: PolicyGroupFilter): void;
  onUpdatePagination(pagination: PaginationFilter): void;
  onDeleteGroup(value: GroupPolicyInfo): void;
}

const GroupPolicy: React.FunctionComponent<Props> = props => {
  const {
    data,
    loading,
    filter,
    onUpdateFilter,
    onUpdatePagination,
    pagination,
    onDeleteGroup,
  } = props;

  const [deleteData, setDeleteData] = React.useState<GroupPolicyInfo | undefined>(undefined);

  const columns = React.useMemo(() => {
    return [
      {
        title: 'department',
        dataIndex: 'department',
        render: (record: some, index: number) => (
          <Typography variant="caption">{record.department?.name}</Typography>
        ),
      },
      {
        title: 'position',
        dataIndex: 'position',
        render: (record: some, index: number) => (
          <Typography variant="caption">{record.jobTitle?.name}</Typography>
        ),
      },
      {
        title: 'policy.budgetHotel',
        dataIndex: 'hotelPolicy',
        render: (record: some, index: number) => (
          <Typography variant="caption">
            <FormattedNumber value={record.hotelPolicy?.budget} />
            &nbsp;
            <FormattedMessage id="currency" />
          </Typography>
        ),
      },
      {
        title: 'policy.budgetFlight',
        dataIndex: 'flightPolicy',
        render: (record: some, index: number) => (
          <Typography variant="caption">
            <FormattedNumber value={record.flightPolicy?.budget} />
            &nbsp;
            <FormattedMessage id="currency" />
          </Typography>
        ),
      },
      {
        disableAction: true,
        width: 150,
        fixed: 'right',

        render: (record: some, index: number) => (
          <Row style={{ justifyContent: 'center' }}>
            <Link
              to={{
                pathname: ROUTES.generalSetting.policy.updateGroup,
                state: {
                  groupPolicyData: record,
                },
              }}
            >
              <Button
                size="small"
                color="secondary"
                style={{ width: 24, padding: 0, marginRight: 20 }}
              >
                <IconEdit />
              </Button>
            </Link>
            <PermissionDiv permission={['tripione:corporatePortal:group:budgetPolicy:delete']}>
              <Button
                size="small"
                color="secondary"
                style={{ width: 24, padding: 0 }}
                onClick={() => setDeleteData(record as GroupPolicyInfo)}
              >
                <IconDelete />
              </Button>
            </PermissionDiv>
          </Row>
        ),
      },
    ] as Columns[];
  }, []);

  return (
    <>
      <GroupFilter loading={loading} params={filter} onUpdateFilter={onUpdateFilter} />
      <TableCustom
        dataSource={data?.itemList}
        loading={loading}
        columns={columns}
        style={{ maxWidth: 1000, marginTop: 24 }}
        header={
          <Row style={{ justifyContent: 'space-between', padding: '16px 12px' }}>
            <Typography variant="subtitle2">
              <FormattedMessage id="department.listAccount" />
            </Typography>
            <Row>
              <Link
                to={{
                  pathname: ROUTES.generalSetting.policy.createGroup,
                  state: { groupPolicyData: defaultGroupPolicyInfo },
                }}
              >
                <Button
                  variant="contained"
                  color="secondary"
                  style={{ minWidth: 120 }}
                  disableElevation
                >
                  <FormattedMessage id="addNew" />
                </Button>
              </Link>
            </Row>
          </Row>
        }
        paginationProps={{
          count: data?.totalResults || 0,
          page: pagination.page || 1,
          rowsPerPage: pagination?.pageSize || 0,
          onChangePage: (event: unknown, newPage: number) => {
            onUpdatePagination({ ...pagination, page: newPage });
          },
          onChangeRowsPerPage: (event: React.ChangeEvent<HTMLInputElement>) => {
            onUpdatePagination({
              pageSize: parseInt(event.target.value, 10),
              page: 1,
            });
          },
        }}
      />

      <WarningDialog
        open={!!deleteData}
        onClose={() => {
          setDeleteData(undefined);
        }}
        onAccept={() => {
          deleteData && onDeleteGroup(deleteData);
          setDeleteData(undefined);
        }}
        messageContent={
          <Typography variant="body2">
            <FormattedMessage id="position.deleteConfirm" />
          </Typography>
        }
      />
    </>
  );
};

export default GroupPolicy;
