import { Button, Typography } from '@material-ui/core';
import * as React from 'react';
import { FormattedMessage, FormattedNumber } from 'react-intl';
import { PURPLE_200 } from '../../../../../configs/colors';
import { ROUTES } from '../../../../../configs/routes';
import { some } from '../../../../../constants';
import { PaginationFilter } from '../../../../../models/pagination';
import { ReactComponent as IconDelete } from '../../../../../svg/ic_delete.svg';
import { ReactComponent as IconEdit } from '../../../../../svg/ic_edit.svg';
import { ReactComponent as IconInfo } from '../../../../../svg/ic_info.svg';
import { Row } from '../../../../common/components/elements';
import Link from '../../../../common/components/Link';
import PermissionDiv from '../../../../common/components/PermissionDiv';
import TableCustom, { Columns } from '../../../../common/components/TableCustom';
import WarningDialog from '../../../../common/components/WarningDialog';
import { defaultPersonPolicyInfo, PersonPolicyInfo, PolicyPersonFilter } from '../utils';
import DetailDialog from './DetailDialog';
import PersonFilter from './PersonFilter';

interface Props {
  loading: boolean;
  data?: some;
  filter: PolicyPersonFilter;
  pagination: PaginationFilter;
  onUpdateFilter(filter: PolicyPersonFilter): void;
  onUpdatePagination(pagination: PaginationFilter): void;
  onDeletePerson(value: PersonPolicyInfo): void;
}

const PersonPolicy: React.FunctionComponent<Props> = props => {
  const {
    data,
    loading,
    filter,
    onUpdateFilter,
    onUpdatePagination,
    pagination,
    onDeletePerson,
  } = props;
  const [deleteData, setDeleteData] = React.useState<PersonPolicyInfo | undefined>(undefined);
  const [detail, setDetail] = React.useState<some | undefined>(undefined);

  const columns = React.useMemo(() => {
    return [
      {
        title: 'policy.employeeId',
        dataIndex: 'employeeId',
        render: (record: some, index: number) => (
          <Typography variant="caption">{record.user.employeeCode}</Typography>
        ),
      },
      {
        title: 'policy.accountName',
        dataIndex: 'email',
        render: (record: some, index: number) => (
          <Typography variant="caption">{record.user.name}</Typography>
        ),
      },
      {
        title: 'department',
        dataIndex: 'department',
        render: (record: some, index: number) => (
          <Typography variant="caption">{record.user?.department?.name}</Typography>
        ),
      },
      {
        title: 'position',
        dataIndex: 'jobTitle',
        render: (record: some, index: number) => (
          <Typography variant="caption">{record.user?.jobTitle?.name}</Typography>
        ),
      },
      {
        title: 'policy.budgetHotel',
        dataIndex: 'hotelPolicy',
        render: (record: some, index: number) => (
          <Typography variant="caption">
            <FormattedNumber value={record.flightPolicy?.budget} />
            &nbsp;
            {record.flightPolicy?.currencyCode}
          </Typography>
        ),
      },
      {
        title: 'policy.budgetFlight',
        dataIndex: 'flightPolicy',
        render: (record: some, index: number) => (
          <Typography variant="caption">
            <FormattedNumber value={record.hotelPolicy?.budget} />
            &nbsp;
            {record.currencyCode}
          </Typography>
        ),
      },
      {
        disableAction: true,
        width: 150,
        fixed: 'right',
        render: (record: some, index: number) => {
          return (
            <Row style={{ justifyContent: 'center' }}>
              <Button
                size="small"
                color="secondary"
                style={{ width: 24, padding: 0, marginRight: 20 }}
                onClick={() => {
                  setDetail(record);
                }}
              >
                <IconInfo className="svgFill" style={{ stroke: PURPLE_200 }} />
              </Button>
              <Link
                to={{
                  pathname: ROUTES.generalSetting.policy.updatePerson,
                  state: {
                    personPolicyData: { ...record },
                  },
                }}
              >
                <Button
                  size="small"
                  color="secondary"
                  style={{ width: 24, padding: 0, marginRight: 20 }}
                >
                  <IconEdit />
                </Button>
              </Link>
              <PermissionDiv permission={['tripione:corporatePortal:group:budgetPolicy:delete']}>
                <Button
                  size="small"
                  color="secondary"
                  style={{ width: 24, padding: 0 }}
                  onClick={() => setDeleteData(record as PersonPolicyInfo)}
                >
                  <IconDelete />
                </Button>
              </PermissionDiv>
            </Row>
          );
        },
      },
    ] as Columns[];
  }, []);

  return (
    <>
      <PersonFilter loading={loading} params={filter} onUpdateFilter={onUpdateFilter} />

      <TableCustom
        dataSource={data?.itemList}
        loading={loading}
        columns={columns}
        style={{ maxWidth: 1278, marginTop: 24 }}
        header={
          <Row style={{ justifyContent: 'space-between', padding: '16px 12px' }}>
            <Typography variant="subtitle2">
              <FormattedMessage id="policy.listAccount" />
            </Typography>
            <Row>
              <Link
                to={{
                  pathname: ROUTES.generalSetting.policy.createPerson,
                  state: { personPolicyData: defaultPersonPolicyInfo },
                }}
              >
                <Button
                  variant="contained"
                  color="secondary"
                  style={{ minWidth: 120 }}
                  disableElevation
                >
                  <FormattedMessage id="addNew" />
                </Button>
              </Link>
            </Row>
          </Row>
        }
        paginationProps={{
          count: data?.totalResults || 0,
          page: pagination.page || 1,
          rowsPerPage: pagination?.pageSize || 0,
          onChangePage: (event: unknown, newPage: number) => {
            onUpdatePagination({ ...pagination, page: newPage });
          },
          onChangeRowsPerPage: (event: React.ChangeEvent<HTMLInputElement>) => {
            onUpdatePagination({
              pageSize: parseInt(event.target.value, 10),
              page: 1,
            });
          },
        }}
        onRowClick={(record: some) => setDetail(record)}
      />

      {detail && <DetailDialog open onClose={() => setDetail(undefined)} data={detail} />}

      <WarningDialog
        open={!!deleteData}
        onClose={() => {
          setDeleteData(undefined);
        }}
        onAccept={() => {
          deleteData && onDeletePerson(deleteData);
          setDeleteData(undefined);
        }}
        messageContent={
          <Typography variant="body2">
            <FormattedMessage id="policy.deleteConfirm" />
          </Typography>
        }
      />
    </>
  );
};

export default PersonPolicy;
