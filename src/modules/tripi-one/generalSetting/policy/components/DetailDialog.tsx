import { Box, Button, DialogActions, Typography } from '@material-ui/core';
import StarRoundedIcon from '@material-ui/icons/StarRounded';
import { Rating } from '@material-ui/lab';
import moment from 'moment';
import * as React from 'react';
import { FormattedMessage, FormattedNumber } from 'react-intl';
import { ORANGE } from '../../../../../configs/colors';
import { some } from '../../../../../constants';
import { DATE_TIME_FORMAT } from '../../../../../models/moment';
import DialogCustom from '../../../../common/components/DialogCustom';
import { Col, Row } from '../../../../common/components/elements';
import EmailLink from '../../../../common/components/EmailLink';

interface Props {
  open: boolean;
  onClose: () => void;
  data: some;
}
const DetailDialog: React.FC<Props> = props => {
  const { open, onClose, data } = props;

  const renderInfo = React.useCallback((info: some) => {
    return (info && info.map((obj: some) => obj.name).join(', ')) || <FormattedMessage id="tbd" />;
  }, []);

  return (
    <DialogCustom
      open={open}
      onClose={onClose}
      PaperProps={{ style: { width: 550 } }}
      titleLabel={
        <Typography variant="subtitle1">
          <FormattedMessage id="policy.personalDetail" />
        </Typography>
      }
      buttonLabel="close"
      footerContent={
        <DialogActions style={{ padding: 16, justifyContent: 'flex-end' }}>
          <Button
            type="submit"
            variant="contained"
            color="secondary"
            style={{ padding: '0px 32px' }}
            disableElevation
          >
            <Typography variant="body2">
              <FormattedMessage id="edit" />
            </Typography>
          </Button>
          <Button
            variant="outlined"
            style={{ padding: '0px 24px' }}
            disableElevation
            onClick={onClose}
          >
            <Typography variant="body2">
              <FormattedMessage id="reject" />
            </Typography>
          </Button>
        </DialogActions>
      }
    >
      <Col style={{ padding: 16 }}>
        <Row style={{ alignItems: 'start', marginBottom: 8 }}>
          <Typography variant="body2" style={{ flex: '0 0 146px' }}>
            <FormattedMessage id="employeeId" />:
          </Typography>
          <Typography variant="body2">{data.user?.employeeCode}</Typography>
        </Row>
        <Row style={{ alignItems: 'start', marginBottom: 8 }}>
          <Typography variant="body2" style={{ flex: '0 0 146px' }}>
            <FormattedMessage id="userName" />:
          </Typography>
          <EmailLink value={data.user?.email} />
        </Row>
        <Row style={{ alignItems: 'start', marginBottom: 8 }}>
          <Typography variant="body2" style={{ flex: '0 0 146px' }}>
            <FormattedMessage id="department" />:
          </Typography>
          <Typography variant="body2">{data.user?.department?.name}</Typography>
        </Row>
        <Row style={{ alignItems: 'start', marginBottom: 8 }}>
          <Typography variant="body2" style={{ flex: '0 0 146px' }}>
            <FormattedMessage id="position" />:
          </Typography>
          <Typography variant="body2">{data.user?.jobTitle?.name}</Typography>
        </Row>
        <Row style={{ alignItems: 'start', marginBottom: 8 }}>
          <Typography variant="body2" style={{ flex: '0 0 146px' }}>
            <FormattedMessage id="policy.budgetFlightTitle" />:
          </Typography>
          <Typography variant="body2" className="price">
            <FormattedNumber value={data.flightPolicy.budget} />
            &nbsp;
            <FormattedMessage id="fullCurrency" />
            <FormattedMessage id="flightTag" />
          </Typography>
        </Row>
        <Row style={{ alignItems: 'start', marginBottom: 8 }}>
          <Typography variant="body2" style={{ flex: '0 0 146px' }}>
            <FormattedMessage id="policy.budgetHotelTitle" />:
          </Typography>
          <Typography variant="body2" className="price">
            <FormattedNumber value={data.hotelPolicy.budget} />
            &nbsp;
            <FormattedMessage id="fullCurrency" />
            <FormattedMessage id="hotelTag" />
          </Typography>
        </Row>
        <Row style={{ alignItems: 'start', marginBottom: 8 }}>
          <Typography variant="body2" style={{ flex: '0 0 146px' }}>
            <FormattedMessage id="ticketClasses" />:
          </Typography>
          <Typography variant="body2">{renderInfo(data.flightPolicy.ticketClasses)}</Typography>
        </Row>
        <Row style={{ alignItems: 'start' }}>
          <Typography variant="body2" style={{ flex: '0 0 134px' }}>
            <FormattedMessage id="maxStar" />:
          </Typography>
          <Box component="fieldset" borderColor="transparent" style={{ padding: 0, margin: 0 }}>
            <Rating
              name="maxStars"
              value={data.hotelPolicy.maxStars}
              readOnly
              size="small"
              icon={<StarRoundedIcon fontSize="small" />}
            />
          </Box>
        </Row>
        <Row style={{ alignItems: 'start', marginBottom: 8 }}>
          <Typography variant="body2" style={{ flex: '0 0 146px' }}>
            <FormattedMessage id="policy.needApproval" />:
          </Typography>
          <Typography variant="body2">
            {data.needApproval ? <FormattedMessage id="yes" /> : <FormattedMessage id="no" />}
          </Typography>
        </Row>
        <Row style={{ alignItems: 'start', marginBottom: 8 }}>
          <Typography variant="body2" style={{ flex: '0 0 146px' }}>
            <FormattedMessage id="policy.createdBy" />:
          </Typography>
          <EmailLink value={data.createdBy} />
        </Row>
        <Row style={{ alignItems: 'start', marginBottom: 8 }}>
          <Typography variant="body2" style={{ flex: '0 0 146px' }}>
            <FormattedMessage id="policy.createdDate" />:
          </Typography>
          <Typography variant="body2">
            {moment(data.createdDate).isValid()
              ? moment(data.createdDate).format(DATE_TIME_FORMAT)
              : null}
          </Typography>
        </Row>
        <Row style={{ alignItems: 'start', marginBottom: 8 }}>
          <Typography variant="body2" style={{ flex: '0 0 146px' }}>
            <FormattedMessage id="policy.lastModifiedBy" />:
          </Typography>
          <EmailLink value={data.updatedBy} />
        </Row>
        <Row style={{ alignItems: 'start', marginBottom: 8 }}>
          <Typography variant="body2" style={{ flex: '0 0 146px' }}>
            <FormattedMessage id="policy.lastModifiedDate" />:
          </Typography>
          <Typography variant="body2">
            {moment(data.updatedDate).isValid()
              ? moment(data.updatedDate).format(DATE_TIME_FORMAT)
              : null}
          </Typography>
        </Row>
      </Col>
    </DialogCustom>
  );
};

export default DetailDialog;
