import { Button, Chip, Paper, Radio, Typography } from '@material-ui/core';
import StarRounded from '@material-ui/icons/StarRounded';
import { Rating } from '@material-ui/lab';
import { useFormik } from 'formik';
import React from 'react';
import { FormattedMessage, useIntl } from 'react-intl';
import { shallowEqual, useDispatch, useSelector } from 'react-redux';
import { Action } from 'redux';
import { ThunkDispatch } from 'redux-thunk';
import * as yup from 'yup';
import { API_PATHS } from '../../../../../configs/API';
import { some } from '../../../../../constants';
import { AppState } from '../../../../../redux/reducers';
import { PAGE_SIZE_20 } from '../../../../booking/constants';
import DialogCustom from '../../../../common/components/DialogCustom';
import { Col, RenderTag, Row } from '../../../../common/components/elements';
import { NumberFormatCustom } from '../../../../common/components/Form';
import FormControlAutoComplete from '../../../../common/components/FormControlAutoComplete';
import FormControlTextField from '../../../../common/components/FormControlTextField';
import InputAdornmentSolid from '../../../../common/components/InputAdornmentSolid';
import LoadingButton from '../../../../common/components/LoadingButton';
import { goBackAction } from '../../../../common/redux/reducer';
import { fetchThunk } from '../../../../common/redux/thunk';
import { PersonPolicyInfo } from '../utils';

interface Props {
  info: PersonPolicyInfo;
  loading: boolean;
  onCreateUpdate(info: PersonPolicyInfo): void;
}

const CreateUpdatePersonPolicyForm: React.FC<Props> = props => {
  const { info, onCreateUpdate, loading } = props;
  const dispatch = useDispatch<ThunkDispatch<AppState, null, Action<string>>>();
  const { generalFlight } = useSelector((state: AppState) => state.common, shallowEqual);
  const intl = useIntl();
  const [open, setOpen] = React.useState(false);

  const personPolicySchema = yup.object().shape({
    users: yup.array().when('id', {
      is: true,
      then: yup
        .array()
        .nullable()
        .notRequired(),
      otherwise: yup
        .array()
        .min(1, intl.formatMessage({ id: 'required' }))
        .of(yup.object().required(intl.formatMessage({ id: `required` }))),
    }),
    budgetPerMonth: yup
      .number()
      .nullable()
      .required(intl.formatMessage({ id: 'required' })),
    flightPolicy: yup.object().shape({
      budget: yup
        .number()
        .nullable()
        .required(intl.formatMessage({ id: 'required' })),
      ticketClasses: yup.array().when('allowAllTicketClasses', {
        is: true,
        then: yup
          .array()
          .nullable()
          .notRequired(),
        otherwise: yup
          .array()
          .nullable()
          .min(1, intl.formatMessage({ id: 'required' })),
      }),
      airlines: yup.array().when('allowAllAirlines', {
        is: true,
        then: yup
          .array()
          .nullable()
          .notRequired(),
        otherwise: yup
          .array()
          .nullable()
          .min(1, intl.formatMessage({ id: 'required' })),
      }),
    }),
    hotelPolicy: yup.object().shape({
      budget: yup
        .number()
        .nullable()
        .required(intl.formatMessage({ id: 'required' })),
      maxStars: yup
        .number()
        .nullable()
        .max(5)
        .min(1)
        .required(intl.formatMessage({ id: 'required' })),
    }),
  });

  const formik = useFormik({
    initialValues: info,
    onSubmit: values => {
      if (values.id) {
        setOpen(true);
      } else {
        onCreateUpdate(values);
      }
    },
    validationSchema: personPolicySchema,
  });

  const emailErrorMessage = React.useMemo(() => {
    if (formik.errors?.users && formik.submitCount > 0) {
      if (typeof formik.errors?.users === 'string') {
        return formik.errors?.users;
      }

      const errors = '';
      return intl.formatMessage(
        {
          id: 'policy.listEmailInvalid',
        },
        { email: errors },
      );
    }
    return undefined;
  }, [formik.errors, formik.submitCount, intl]);

  React.useEffect(() => {
    formik.setValues(info, true);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [info]);

  return (
    <>
      <form autoComplete="off" onSubmit={formik.handleSubmit}>
        <div style={{ marginBottom: 12, marginTop: 24, alignItems: 'flex-start' }}>
          {formik.values.id ? (
            <Row style={{ marginTop: 12, marginBottom: 24 }}>
              <Typography variant="body2" style={{ marginRight: 16, minWidth: 100 }}>
                <FormattedMessage id="policy.accountName" />
              </Typography>
              <Chip label={info.user?.email} />
            </Row>
          ) : (
            <Row style={{ alignItems: 'flex-start', marginBottom: 12 }}>
              <Typography variant="body2" style={{ marginRight: 16, marginTop: 10, minWidth: 100 }}>
                <FormattedMessage id="policy.accountName" />
              </Typography>
              <div>
                <FormControlAutoComplete<some, true, true, undefined>
                  id="users"
                  multiple
                  placeholder={
                    formik.values.users && formik.values.users.length > 0
                      ? ''
                      : intl.formatMessage({ id: 'approval.enterSearchStr' })
                  }
                  value={formik.values.users}
                  formControlStyle={{ minWidth: 350 }}
                  filterOptions={(options, state) => options}
                  onChange={(e: any, value: some[]) => {
                    formik.setFieldValue('users', value);
                  }}
                  getOptionLabel={value => value.name}
                  getOptionSelected={(option, value) => {
                    return option.id === value.id;
                  }}
                  loadOptions={async (searchStr: string) => {
                    const json = await dispatch(
                      fetchThunk(
                        API_PATHS.getListUser,
                        'post',
                        JSON.stringify({
                          filters: {
                            searchStr,
                          },
                          page: 1,
                          pageSize: PAGE_SIZE_20,
                        }),
                      ),
                    );
                    return json.data?.itemList.map((v: some) => {
                      return {
                        id: v.id,
                        name: `${v?.name} ${`${v.email ? `(${v.email})` : ''}`}` || '',
                      };
                    });
                  }}
                  options={[]}
                  errorMessage={emailErrorMessage}
                  disableCloseOnSelect
                  renderTags={(value: some[], e: any) => RenderTag(value, e, 'name')}
                />
              </div>
              <Typography variant="body2" color="textSecondary" style={{ marginTop: 10 }}>
                <FormattedMessage id="policy.emailNote" />
              </Typography>
            </Row>
          )}

          <Row style={{ alignItems: 'flex-start', marginBottom: 12 }}>
            <Typography variant="body2" style={{ marginRight: 16, marginTop: 10, minWidth: 100 }}>
              <FormattedMessage id="policy.budgetPerMonth" />
            </Typography>
            <div>
              <FormControlTextField
                id="budgetPerMonth"
                formControlStyle={{ width: '100%', marginRight: 0 }}
                placeholder={intl.formatMessage({ id: 'addMoney' })}
                value={formik.values.budgetPerMonth || ''}
                onChange={e => {
                  formik.setFieldValue('budgetPerMonth', e.target.value);
                }}
                inputProps={{
                  maxLength: 100,
                }}
                inputComponent={NumberFormatCustom as any}
                endAdornment={
                  <InputAdornmentSolid>
                    <FormattedMessage id="policy.budgetFlightTagPerMonth" />
                  </InputAdornmentSolid>
                }
                errorMessage={
                  formik.errors.flightPolicy?.budget && formik.submitCount > 0
                    ? formik.errors.flightPolicy?.budget
                    : undefined
                }
              />
            </div>
          </Row>

          <Paper style={{ padding: '16px 12px' }}>
            <Row style={{ marginBottom: 12 }}>
              <Typography variant="body2" style={{ marginRight: 16, marginBottom: 20 }}>
                <FormattedMessage id="policy.budgetFlightTitle" />
              </Typography>
              <FormControlTextField
                id="flightPolicy"
                formControlStyle={{ width: 350 }}
                placeholder={intl.formatMessage({ id: 'addMoney' })}
                value={formik.values.flightPolicy?.budget || ''}
                onChange={e =>
                  formik.setFieldValue(
                    'flightPolicy',
                    {
                      ...formik.values.flightPolicy,
                      budget: Number(e.target.value),
                    },
                    false,
                  )
                }
                inputComponent={NumberFormatCustom as any}
                endAdornment={
                  <InputAdornmentSolid>
                    <FormattedMessage id="policy.budgetFlightTag" />
                  </InputAdornmentSolid>
                }
                errorMessage={
                  formik.errors.flightPolicy?.budget && formik.submitCount > 0
                    ? formik.errors.flightPolicy?.budget
                    : undefined
                }
              />
            </Row>
            <Row>
              <Row style={{ marginBottom: 20, marginRight: 20 }}>
                <Typography variant="subtitle2" style={{ minWidth: 100 }}>
                  <FormattedMessage id="policy.ticketClasses" />
                </Typography>
                <Radio
                  style={{ marginRight: 8, padding: 4 }}
                  checked={formik.values.flightPolicy?.allowAllTicketClasses}
                  onClick={e =>
                    formik.setFieldValue('flightPolicy', {
                      ...formik.values.flightPolicy,
                      allowAllTicketClasses: true,
                      ticketClasses: null,
                    })
                  }
                />
                <Typography variant="body2">
                  <FormattedMessage id="all" />
                </Typography>
                <Radio
                  style={{ marginRight: 8 }}
                  checked={!formik.values.flightPolicy?.allowAllTicketClasses}
                  onClick={e => {
                    formik.setFieldValue(
                      'flightPolicy',
                      {
                        ...formik.values.flightPolicy,
                        allowAllTicketClasses: false,
                        ticketClasses: [],
                      },
                      false,
                    );
                  }}
                />
                <Typography variant="body2">
                  <FormattedMessage id="optional" />
                </Typography>
              </Row>
              {!formik.values.flightPolicy?.allowAllTicketClasses && (
                <FormControlAutoComplete<some, true, undefined, undefined>
                  multiple
                  value={formik.values.flightPolicy.ticketClasses}
                  placeholder={intl.formatMessage({ id: 'policy.chooseTicketClasses' })}
                  formControlStyle={{ width: 350 }}
                  options={generalFlight.ticketclass.map((v: some) => ({
                    id: v.cid,
                    name: v.v_name,
                  }))}
                  getOptionLabel={v => v.name}
                  onChange={(e: any, value: some[]) => {
                    formik.setFieldValue('flightPolicy', {
                      ...formik.values.flightPolicy,
                      ticketClasses: value,
                    });
                  }}
                  getOptionSelected={(option, value) => {
                    return option.id === value.id;
                  }}
                  errorMessage={
                    formik.errors?.flightPolicy?.ticketClasses && formik.submitCount > 0
                      ? (formik.errors?.flightPolicy?.ticketClasses as string)
                      : undefined
                  }
                  renderTags={(value: some[], e: any) => RenderTag(value, e, 'name')}
                  disableCloseOnSelect
                />
              )}
            </Row>
            <Row>
              <Row style={{ marginBottom: 20, marginRight: 20 }}>
                <Typography variant="subtitle2" style={{ minWidth: 100 }}>
                  <FormattedMessage id="policy.airlines" />
                </Typography>
                <Radio
                  style={{ marginRight: 8, padding: 4 }}
                  checked={formik.values.flightPolicy?.allowAllAirlines}
                  onClick={e =>
                    formik.setFieldValue('flightPolicy', {
                      ...formik.values.flightPolicy,
                      allowAllAirlines: true,
                      airlines: null,
                    })
                  }
                />
                <Typography variant="body2">
                  <FormattedMessage id="all" />
                </Typography>
                <Radio
                  style={{ marginRight: 8 }}
                  checked={!formik.values.flightPolicy?.allowAllAirlines}
                  onClick={e =>
                    formik.setFieldValue(
                      'flightPolicy',
                      {
                        ...formik.values.flightPolicy,
                        allowAllAirlines: false,
                        airlines: [],
                      },
                      false,
                    )
                  }
                />
                <Typography variant="body2">
                  <FormattedMessage id="optional" />
                </Typography>
              </Row>
              {!formik.values.flightPolicy?.allowAllAirlines && (
                <FormControlAutoComplete<some, true, undefined, undefined>
                  multiple
                  value={formik.values.flightPolicy?.airlines}
                  placeholder={intl.formatMessage({ id: 'policy.chooseAirlines' })}
                  formControlStyle={{ width: 350 }}
                  options={generalFlight.airlines.map((v: some) => ({
                    id: v.aid,
                    name: v.name,
                  }))}
                  getOptionLabel={v => v.name}
                  onChange={(e: any, value: some[]) => {
                    formik.setFieldValue('flightPolicy', {
                      ...formik.values.flightPolicy,
                      airlines: value,
                    });
                  }}
                  getOptionSelected={(option, value) => {
                    return option.id === value.id;
                  }}
                  renderTags={RenderTag}
                  disableCloseOnSelect
                  errorMessage={
                    formik.errors?.flightPolicy?.airlines && formik.submitCount > 0
                      ? (formik.errors?.flightPolicy?.airlines as string)
                      : undefined
                  }
                />
              )}
            </Row>
          </Paper>

          <Paper style={{ padding: '16px 12px', marginTop: 24 }}>
            <Row style={{ marginBottom: 12 }}>
              <Typography
                variant="body2"
                style={{ marginRight: 16, marginBottom: 20, minWidth: 120 }}
              >
                <FormattedMessage id="policy.budgetHotelTitle" />
              </Typography>
              <FormControlTextField
                id="hotelPolicy"
                formControlStyle={{ width: 350 }}
                placeholder={intl.formatMessage({ id: 'addMoney' })}
                value={formik.values.hotelPolicy?.budget || ''}
                onChange={e =>
                  formik.setFieldValue(
                    'hotelPolicy',
                    {
                      ...formik.values.hotelPolicy,
                      budget: Number(e.target.value),
                    },
                    false,
                  )
                }
                inputComponent={NumberFormatCustom as any}
                endAdornment={
                  <InputAdornmentSolid>
                    <FormattedMessage id="policy.budgetHotelTag" />
                  </InputAdornmentSolid>
                }
                errorMessage={
                  formik.errors.hotelPolicy?.budget && formik.touched.hotelPolicy?.budget
                    ? formik.errors.hotelPolicy?.budget
                    : undefined
                }
              />
            </Row>
            <Row style={{ marginBottom: 12 }}>
              <Typography variant="body2" style={{ marginRight: 16, minWidth: 120 }}>
                <FormattedMessage id="policy.rateStar" />
              </Typography>
              <Rating
                name="rating"
                icon={<StarRounded />}
                value={formik.values.hotelPolicy?.maxStars || 0}
                onChange={(event, value) => {
                  formik.setFieldValue(
                    'hotelPolicy',
                    {
                      ...formik.values.hotelPolicy,
                      maxStars: value || 1,
                    },
                    false,
                  );
                }}
              />
            </Row>
          </Paper>
          <Row style={{ marginTop: 24 }}>
            <LoadingButton
              loading={loading}
              variant="contained"
              color="secondary"
              style={{ marginRight: 16, minWidth: 140 }}
              size="large"
              type="submit"
              disableElevation
            >
              <FormattedMessage id="save" />
            </LoadingButton>
            <Button
              variant="outlined"
              style={{ minWidth: 140 }}
              size="large"
              onClick={() => dispatch(goBackAction())}
            >
              <FormattedMessage id="reject" />
            </Button>
          </Row>
        </div>
      </form>
      <DialogCustom
        open={open}
        onClose={() => {
          setOpen(false);
        }}
        onAction={() => {
          onCreateUpdate(formik.values);
          setOpen(false);
        }}
        PaperProps={{ style: { minWidth: 380 } }}
        titleLabel={
          <Typography variant="subtitle1">
            <FormattedMessage id="confirm" />
          </Typography>
        }
        buttonLabel="accept"
      >
        <Col style={{ padding: '16px', minHeight: 60 }}>
          <Typography variant="body2">
            <FormattedMessage id="updateConfirm" />
          </Typography>
        </Col>
      </DialogCustom>
    </>
  );
};

export default CreateUpdatePersonPolicyForm;
