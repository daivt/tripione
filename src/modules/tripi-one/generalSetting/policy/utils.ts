import { some } from '../../../../constants';
import { SelectItem } from '../../../../models/object';

export interface ApprovalFlowInfo {
  id?: number;
  appliedDepartment: null | some;
  flow: SelectItem[];
}

export const defaultApprovalFlowInfo: ApprovalFlowInfo = {
  appliedDepartment: null,
  flow: [],
};

export interface GroupPolicyInfo {
  id?: number;
  budgetPerMonth: number | null;
  departments: some[];
  department?: some;
  jobTitles: some[] ;
  jobTitle?: some ;
  flightPolicy: {
    budget: number | null;
    currencyCode: string;
    allowAllAirlines: boolean;
    allowAllTicketClasses: boolean;
    ticketClasses:
      {
          id: number;
          name: string;
          isActive: boolean;
          code: string;
        }[]
      ;
    airlines:
      {
          id: number;
          aid: number;
          name: string;
          isActive: boolean;
        }[]
      ;
  };
  hotelPolicy: {
    budget: number | null;
    currencyCode: string;
    maxStars: number;
  };
}

export const defaultGroupPolicyInfo: GroupPolicyInfo = {
  departments: [],
  budgetPerMonth: null,
  jobTitles: [],
  flightPolicy: {
    allowAllAirlines: false,
    allowAllTicketClasses: false,
    budget: null,
    currencyCode: '',
    ticketClasses: [],
    airlines: [],
  },
  hotelPolicy: {
    budget: null,
    currencyCode: '',
    maxStars: 0,
  },
};

export interface PersonPolicyInfo {
  id?: number;
  users?: some[];
  user?: some;
  budgetPerMonth: number | null;
  flightPolicy: {
    allowAllAirlines: boolean;
    allowAllTicketClasses: boolean;
    budget: number | null;
    currencyCode: string;
    ticketClasses:
    {
      id: number;
      name: string;
      isActive: boolean;
      code: string;
    }[] ;
    airlines:
    {
      id: number;
      aid: number;
      name: string;
      isActive: boolean;
    }[];
  };
  hotelPolicy: {
    budget: number | null;
    currencyCode: string;
    maxStars: number;
  };
}

export const defaultPersonPolicyInfo: PersonPolicyInfo = {
  budgetPerMonth: null,
  flightPolicy: {
    allowAllAirlines: false,
    allowAllTicketClasses: false,
    budget: null,
    currencyCode: '',
    ticketClasses: [],
    airlines: [],
  },
  hotelPolicy: {
    budget: null,
    currencyCode: '',
    maxStars: 1,
  },
};
export interface PolicyGroupFilter {
  departmentId?: number;
  positionId?: number;
}
export const defaultPolicyGroupFilter: PolicyGroupFilter = {};

export interface PolicyPersonFilter {
  name: string;
  employeeId: string;
  departmentId?: number;
  positionId?: number;
}

export const defaultPolicyPersonFilter: PolicyPersonFilter = {
  name: '',
  employeeId: '',
};
