import { Typography, Divider, Button } from '@material-ui/core';
import * as React from 'react';
import { FormattedMessage, useIntl } from 'react-intl';
import * as yup from 'yup';
import { useFormik } from 'formik';
import DialogCustom from '../../../common/components/DialogCustom';
import { Col, Row } from '../../../common/components/elements';
import FormControlTextField from '../../../common/components/FormControlTextField';
import { IAccountPassword, defaultAccountPassword } from '../utils';

interface Props {
  onUpdatePassword(value: IAccountPassword): void;
  open: boolean;
  onClose(): void;
}

const ChangePassword = (props: Props) => {
  const { onUpdatePassword, open, onClose } = props;
  const intl = useIntl();

  const passwordSchema = yup.object().shape({
    oldPassword: yup
      .string()
      .trim()
      .required(intl.formatMessage({ id: 'required' })),
    newPassword: yup
      .string()
      .trim()
      .required(intl.formatMessage({ id: 'required' })),
    confirmNewPassword: yup
      .string()
      .trim()
      .required(intl.formatMessage({ id: 'required' })),
  });

  const formik = useFormik({
    initialValues: defaultAccountPassword,
    onSubmit: values => {
      onUpdatePassword(values);
    },
    validationSchema: passwordSchema,
  });

  return (
    <div>
      <DialogCustom
        open={open}
        onClose={onClose}
        PaperProps={{ style: { minWidth: 400 } }}
        titleLabel={
          <Typography variant="subtitle1">
            <FormattedMessage id="changePassword" />
          </Typography>
        }
        buttonLabel="close"
        footerContent={<div />}
      >
        <form onSubmit={formik.handleSubmit}>
          <Col style={{ alignItems: 'center' }}>
            <FormControlTextField
              id="oldPassword"
              formControlStyle={{ width: 250, marginTop: 12 }}
              label={<FormattedMessage id="password" />}
              placeholder={intl.formatMessage({ id: 'enterPassword' })}
              value={formik.values.oldPassword}
              onChange={formik.handleChange}
              inputProps={{
                maxLength: 50,
                autoComplete: 'new-password',
              }}
              type="password"
              errorMessage={
                formik.errors.oldPassword && formik.touched.oldPassword
                  ? formik.errors.oldPassword
                  : undefined
              }
            />
            <FormControlTextField
              id="newPassword"
              formControlStyle={{ width: 250, marginTop: 12 }}
              label={<FormattedMessage id="newPassword" />}
              placeholder={intl.formatMessage({ id: 'enterPassword' })}
              value={formik.values.newPassword}
              onChange={formik.handleChange}
              inputProps={{
                maxLength: 50,
                autoComplete: 'new-password',
              }}
              type="password"
              errorMessage={
                formik.errors.newPassword && formik.touched.newPassword
                  ? formik.errors.newPassword
                  : undefined
              }
            />
            <FormControlTextField
              id="confirmNewPassword"
              formControlStyle={{ width: 250, marginTop: 12 }}
              label={<FormattedMessage id="confirmNewPassword" />}
              placeholder={intl.formatMessage({ id: 'enterPassword' })}
              value={formik.values.confirmNewPassword}
              onChange={formik.handleChange}
              inputProps={{
                maxLength: 50,
                autoComplete: 'new-password',
              }}
              type="password"
              errorMessage={
                formik.errors.confirmNewPassword && formik.touched.confirmNewPassword
                  ? formik.errors.confirmNewPassword
                  : undefined
              }
            />
          </Col>
          <Divider />
          <Row style={{ padding: 16, justifyContent: 'flex-end' }}>
            <Button
              type="submit"
              variant="contained"
              color="secondary"
              style={{ marginRight: 12, minWidth: 110 }}
              disableElevation
            >
              <FormattedMessage id="accept" />
            </Button>
            <Button variant="outlined" style={{ minWidth: 92 }} onClick={() => onClose()}>
              <FormattedMessage id="reject" />
            </Button>
          </Row>
        </form>
      </DialogCustom>
    </div>
  );
};

export default ChangePassword;
