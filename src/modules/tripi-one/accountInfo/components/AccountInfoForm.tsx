import { Button, Typography } from '@material-ui/core';
import { useFormik } from 'formik';
import * as React from 'react';
import { FormattedMessage, useIntl } from 'react-intl';
import { useDispatch } from 'react-redux';
import { Action } from 'redux';
import { ThunkDispatch } from 'redux-thunk';
import * as yup from 'yup';
import { some } from '../../../../constants';
import { AppState } from '../../../../redux/reducers';
import { Row } from '../../../common/components/elements';
import { redMark } from '../../../common/components/Form';
import FormControlTextField from '../../../common/components/FormControlTextField';
import { goBackAction } from '../../../common/redux/reducer';
import { IAccountInfo } from '../utils';
import ChangePassword from './ChangePassword';

interface Props {
  data: IAccountInfo;
  onUpdateInfo(value: some): void;
}

const AccountInfoForm = (props: Props) => {
  const { data, onUpdateInfo } = props;
  const [changePasswordOpen, setChangePasswordOpen] = React.useState(false);

  const dispatch = useDispatch<ThunkDispatch<AppState, null, Action<string>>>();
  const intl = useIntl();

  const accountInfoSchema = yup.object().shape({
    employeeId: yup
      .string()
      .trim()
      .required(intl.formatMessage({ id: 'required' })),
    name: yup
      .string()
      .trim()
      .required(intl.formatMessage({ id: 'required' })),
    phone: yup.string().trim(),
  });

  const formik = useFormik({
    initialValues: data,
    onSubmit: values => {
      onUpdateInfo(values);
    },
    validationSchema: accountInfoSchema,
  });

  React.useEffect(() => {
    formik.setValues(data);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [data]);
  return (
    <form onSubmit={formik.handleSubmit} style={{ minWidth: 620 }}>
      <Row style={{ margin: '14px 0 12px' }}>
        <Typography variant="body2" style={{ width: 108, marginRight: 20, marginBottom: 20 }}>
          <FormattedMessage id="employeeId" /> &nbsp;{redMark}
        </Typography>
        <FormControlTextField
          id="employeeId"
          formControlStyle={{ width: 388 }}
          placeholder={intl.formatMessage({ id: 'insertEmployeeId' })}
          value={formik.values.employeeId}
          onChange={formik.handleChange}
          inputProps={{
            maxLength: 50,
          }}
          errorMessage={
            formik.errors.employeeId && formik.touched.employeeId
              ? formik.errors.employeeId
              : undefined
          }
        />
      </Row>
      <Row style={{ margin: '14px 0 12px' }}>
        <Typography variant="body2" style={{ width: 108, marginRight: 20, marginBottom: 20 }}>
          <FormattedMessage id="fullName" /> &nbsp;{redMark}
        </Typography>
        <FormControlTextField
          id="name"
          formControlStyle={{ width: 388 }}
          placeholder={intl.formatMessage({ id: 'fullName' })}
          value={formik.values.name}
          onChange={formik.handleChange}
          inputProps={{
            maxLength: 50,
          }}
          errorMessage={formik.errors.name && formik.touched.name ? formik.errors.name : undefined}
        />
      </Row>
      <Row style={{ margin: '14px 0 12px' }}>
        <Typography variant="body2" style={{ width: 108, marginRight: 20, marginBottom: 20 }}>
          <FormattedMessage id="phoneNumber" />
          :&nbsp;
        </Typography>
        <FormControlTextField
          id="phone"
          formControlStyle={{ width: 388 }}
          placeholder={intl.formatMessage({ id: 'enterPhoneNumber' })}
          value={formik.values.phone}
          onChange={formik.handleChange}
          optional
          inputProps={{
            maxLength: 12,
          }}
        />
      </Row>
      <Row style={{ marginBottom: 12 }}>
        <Typography variant="body2" style={{ width: 108, marginRight: 20, marginBottom: 20 }}>
          <FormattedMessage id="email" />
          :&nbsp;
        </Typography>
        <Typography variant="body2" style={{ width: 108, marginBottom: 20, flex: 1 }}>
          {formik.values.email}
        </Typography>
      </Row>
      <Row style={{ marginBottom: 12 }}>
        <Typography variant="body2" style={{ width: 108, marginRight: 20, marginBottom: 20 }}>
          <FormattedMessage id="department" />
          :&nbsp;
        </Typography>
        <Typography variant="body2" style={{ width: 108, marginBottom: 20, flex: 1 }}>
          {formik.values.department.name}
        </Typography>
      </Row>
      <Row style={{ marginBottom: 12 }}>
        <Typography variant="body2" style={{ width: 108, marginRight: 20, marginBottom: 20 }}>
          <FormattedMessage id="position" />
          :&nbsp;
        </Typography>
        <Typography variant="body2" style={{ width: 108, marginBottom: 20, flex: 1 }}>
          {formik.values.position.name}
        </Typography>
      </Row>
      <Row style={{ marginLeft: 124 }}>
        <Button
          type="submit"
          variant="contained"
          style={{ minWidth: 144, marginRight: 20 }}
          color="secondary"
          size="large"
          disableElevation
          onClick={() => console.log('Saving')}
        >
          <FormattedMessage id="save" />
        </Button>
        <Button
          variant="contained"
          style={{ minWidth: 144, marginRight: 20 }}
          color="secondary"
          size="large"
          disableElevation
          onClick={() => setChangePasswordOpen(true)}
        >
          <FormattedMessage id="changePassword" />
        </Button>
        <Button
          type="submit"
          variant="outlined"
          style={{ minWidth: 144 }}
          size="large"
          disableElevation
          onClick={() => dispatch(goBackAction())}
        >
          <FormattedMessage id="reject" />
        </Button>
      </Row>
      <ChangePassword
        open={changePasswordOpen}
        onClose={() => setChangePasswordOpen(false)}
        onUpdatePassword={() => console.log('Update and change password')}
      />
    </form>
  );
};

export default AccountInfoForm;
