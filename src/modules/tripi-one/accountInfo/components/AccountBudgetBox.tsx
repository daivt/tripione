import { Box, Divider, Paper, Typography } from '@material-ui/core';
import StarRoundedIcon from '@material-ui/icons/StarRounded';
import { Rating } from '@material-ui/lab';
import * as React from 'react';
import { FormattedMessage, FormattedNumber } from 'react-intl';
import { ORANGE } from '../../../../configs/colors';
import { some } from '../../../../constants';
import { Row } from '../../../common/components/elements';

interface Props {
  data: some;
}

const AccountBudgetBox = (props: Props) => {
  const { data } = props;

  const renderInfo = React.useCallback((info: some) => {
    return (
      info
        .filter((one: some) => one.isActive)
        .map((obj: some) => obj.name)
        .join(', ') || <FormattedMessage id="tbd" />
    );
  }, []);

  return (
    <Paper style={{ width: 406, margin: '16px 0', borderRadius: 0 }}>
      <Typography variant="subtitle2" style={{ padding: '16px 8px' }}>
        <FormattedMessage id="quota" />
      </Typography>
      <Divider />
      <div style={{ padding: '16px 24px' }}>
        <Row style={{ marginBottom: 8 }}>
          <Typography variant="body2" style={{ width: 144, marginRight: 8 }}>
            <FormattedMessage id="flightBudget" />
            :&nbsp;
          </Typography>
          <Typography variant="body2" style={{ color: ORANGE }}>
            <FormattedNumber value={data.flightPolicy.budget} />
            &nbsp;
            {data.flightPolicy.currencyCode}
            <FormattedMessage id="flightTag" />
          </Typography>
        </Row>
        <Row style={{ marginBottom: 8 }}>
          <Typography variant="body2" style={{ width: 144, marginRight: 8 }}>
            <FormattedMessage id="ticketClasses" />
            :&nbsp;
          </Typography>
          <Typography variant="body2">{renderInfo(data.flightPolicy.ticketClasses)}</Typography>
        </Row>
        <Row style={{ marginBottom: 8 }}>
          <Typography variant="body2" style={{ width: 144, marginRight: 8 }}>
            <FormattedMessage id="airlines" />
            :&nbsp;
          </Typography>
          <Typography variant="body2">{renderInfo(data.flightPolicy.airlines)}</Typography>
        </Row>
        <Row style={{ marginBottom: 8 }}>
          <Typography variant="body2" style={{ width: 144, marginRight: 8 }}>
            <FormattedMessage id="hotelBudget" />
            :&nbsp;
          </Typography>
          <Typography variant="body2" style={{ color: ORANGE }}>
            <FormattedNumber value={data.hotelPolicy.budget} />
            &nbsp;
            {data.hotelPolicy.currencyCode}
            <FormattedMessage id="hotelTag" />
          </Typography>
        </Row>
        <Row style={{ marginBottom: 8 }}>
          <Typography variant="body2" style={{ width: 144, marginRight: 8 }}>
            <FormattedMessage id="maxStar" />
            :&nbsp;
          </Typography>
          <Box component="fieldset" borderColor="transparent" style={{ padding: 0, margin: 0 }}>
            <Rating
              name="maxStars"
              value={data.hotelPolicy.maxStars}
              readOnly
              size="small"
              icon={<StarRoundedIcon fontSize="small" />}
            />
          </Box>
        </Row>
      </div>
    </Paper>
  );
};

export default AccountBudgetBox;
