import { debounce } from 'lodash';
import * as React from 'react';
import { some } from '../../../../constants';
import { Row } from '../../../common/components/elements';
import LoadingIcon from '../../../common/components/LoadingIcon';
import AccountBudgetBox from '../components/AccountBudgetBox';
import AccountInfoForm from '../components/AccountInfoForm';
import { fakeAccountBudgetData, fakeAccountInfoData } from '../constants';
import { IAccountInfo } from '../utils';

interface Props {}

const AccountInfo = (props: Props) => {
  const [accountInfo, setAccountInfo] = React.useState<some | undefined>();
  const [accountBudget, setAccountBudget] = React.useState<some | undefined>();
  const [loading, setLoading] = React.useState(true);

  const onUpdateInfo = React.useCallback((info: IAccountInfo) => {
    console.log(info);
  }, []);

  const fetchAccountInfo = React.useCallback(
    debounce(
      async () => {
        setLoading(true);
        setAccountInfo(fakeAccountInfoData.data);
        setLoading(false);
      },
      200,
      {
        trailing: true,
        leading: false,
      },
    ),
    [],
  );

  const fetchAccountBudget = React.useCallback(
    debounce(
      async () => {
        setLoading(true);
        setAccountBudget(fakeAccountBudgetData.data.itemList[0]);
        setLoading(false);
      },
      200,
      {
        trailing: true,
        leading: false,
      },
    ),
    [],
  );

  React.useEffect(() => {
    fetchAccountInfo();
    fetchAccountBudget();
  }, [fetchAccountBudget, fetchAccountInfo]);

  if (!accountInfo || !accountBudget) {
    return <LoadingIcon style={{ height: 320 }} />;
  }

  return (
    <Row
      style={{
        flexWrap: 'wrap',
        alignItems: 'flex-start',
        justifyContent: 'flex-end',
        flexDirection: 'row-reverse',
      }}
    >
      <AccountBudgetBox data={accountBudget} />
      <AccountInfoForm data={accountInfo as IAccountInfo} onUpdateInfo={onUpdateInfo} />
    </Row>
  );
};

export default AccountInfo;
