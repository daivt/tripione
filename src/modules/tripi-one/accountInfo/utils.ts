import { SelectItem } from "../../../models/object";
import { some } from "../../../constants";

export interface IAccountBudget {
  flightPolicy: {
    budget: number;
    ticketClasses: some[];
    airlines: some[];
  };
  hotelPolicy: {
    budget: number;
    maxStars: number;
  }
}

export interface IAccountInfo {
  employeeId: string ;
  name: string;
  phone: string;
  email: string;
  department: SelectItem;
  position: SelectItem;
}
export interface IAccountPassword {
  oldPassword: string;
  newPassword: string;
  confirmNewPassword: string;
}

export const defaultAccountPassword: IAccountPassword = {
  oldPassword: '',
  newPassword: '',
  confirmNewPassword: '',
}
