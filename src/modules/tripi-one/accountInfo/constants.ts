export const fakeAccountInfoData = {
  code: "SUCCESS",
  message: "success",
  data: {
    id: 123456789,
    email: "quannm.it@tripi.vn",
    phone: "0123456789",
    name: "Ngô Minh Quân",
    profilePhoto: "https://i.pinimg.com/236x/d0/f4/fc/d0f4fc818a35285642ba057436fc8720.jpg",
    employeeId: "IT00984",
    createdTime: 1590437391831,
    updatedTime: null,
    emailValidated: false,
    role: {
      id: 1,
      name: "Thành viên"
    },
    lastLoginDate: null,
    department: {
      id: 1,
      name: "Ban Giám Đốc"
    },
    position: {
      id: 1,
      name: "Giám Đốc Điều Hành"
    },
    isActive: true,
    isCompanyFirstAccount: false
  }
}

export const fakeAccountBudgetData = {
  code: "SUCCESS",
  message: "success",
  data: {
    totalResults: 2,
    page: 1,
    pageSize: 10,
    itemList: [
      {
        id: 1,
        employeeId: "IT00984",
        email: "quannm.it@tripi.vn",
        department: {
          id: 2,
          name: "Phòng IT"
        },
        position: {
          id: 2,
          name: "Lập trình viên"
        },
        flightPolicy: {
          budget: 8000000,
          currencyCode: "VND",
          ticketClasses: [
            {
              id: 1,
              name: "Phổ thông",
              isActive: true
            },
            {
              id: 2,
              name: "Phổ thông đặc biệt",
              isActive: true
            },
            {
              id: 3,
              name: "Thương gia",
              isActive: false
            },
            {
              id: 4,
              name: "Hạng nhất",
              isActive: false
            }
          ],
          airlines: [
            {
              id: 1,
              name: "Vietnam Airlines",
              isActive: false
            },
            {
              id: 2,
              name: "Vietjet Air",
              isActive: true
            },
            {
              id: 3,
              name: "Jetstar Pacific Airline",
              isActive: false
            },
            {
              id: 4,
              name: "Bamboo Airways",
              isActive: true
            }
          ]
        },
        hotelPolicy: {
          budget: 12000000,
          currencyCode: "VND",
          maxStar: 3
        }
      },
      {
        id: 2,
        employeeId: "IT001",
        email: "giang.tran@tripi.vn",
        department: {
          id: 3,
          name: "Ban giám đốc"
        },
        position: {
          id: 3,
          name: "CEO"
        },
        flightPolicy: {
          budget: 50000000,
          currencyCode: "VND",
          ticketClasses: [
            {
              id: 1,
              name: "Phổ thông",
              isActive: true
            },
            {
              id: 2,
              name: "Phổ thông đặc biệt",
              isActive: true
            },
            {
              id: 3,
              name: "Thương gia",
              isActive: true
            },
            {
              id: 4,
              name: "Hạng nhất",
              isActive: true
            }
          ],
          airlines: [
            {
              id: 1,
              name: "Vietnam Airlines",
              isActive: false
            },
            {
              id: 2,
              name: "Vietjet Air",
              isActive: true
            },
            {
              id: 3,
              name: "Jetstar Pacific Airline",
              isActive: false
            },
            {
              id: 4,
              name: "Bamboo Airways",
              isActive: true
            }
          ]
        },
        hotelPolicy: {
          budget: 100000000,
          currencyCode: "VND",
          maxStar: 5
        }
      }
    ]
  }
}
