import { debounce } from '@material-ui/core';
import queryString from 'query-string';
import React, { useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useLocation } from 'react-router';
import { Action } from 'redux';
import { ThunkDispatch } from 'redux-thunk';
import { useSnackbar } from 'notistack';
import { some, SUCCESS_CODE } from '../../../../constants';
import { AppState } from '../../../../redux/reducers';
import TableBox from '../components/TableBox';
import { defaultInvoicesFilter, InvoicesFilter, RequestForm } from '../utils';
import { PaginationFilter, defaultPaginationFilter } from '../../../../models/pagination';
import { goToReplace } from '../../../common/redux/reducer';
import { fetchThunk } from '../../../common/redux/thunk';
import { API_PATHS } from '../../../../configs/API';
import { snackbarSetting } from '../../../common/components/elements';

interface Props {}
const InvoicesManager: React.FC<Props> = props => {
  const dispatch = useDispatch<ThunkDispatch<AppState, null, Action<string>>>();
  const [filter, setFilter] = useState<InvoicesFilter>(defaultInvoicesFilter);
  const location = useLocation();
  const [pagination, setPagination] = React.useState<PaginationFilter>(defaultPaginationFilter);
  const [data, setData] = useState<some | undefined>();
  const [loading, setLoading] = useState(true);
  const { enqueueSnackbar, closeSnackbar } = useSnackbar();
  const [invoiceCancelInfo, setInvoiceCancelInfo] = React.useState<number[]>([]);
  const [invoiceRequestInfo, setInvoiceRequestInfo] = React.useState<RequestForm | undefined>(undefined);

  const updateQueryParams = React.useCallback(() => {
    if (location.search) {
      const filterParams = queryString.parse(location.search) as any;
      const filterTmp = queryString.parse(filterParams.filters) as any;
      const createdBy = filterTmp.createdBy && queryString.parse(filterTmp.createdBy);
      setFilter({
        ...filterTmp,
        created: filterTmp.created && Number(filterTmp.created),
        createdBy: createdBy && { id: Number(createdBy.id), name: createdBy.name },
        department: filterTmp.department && Number(filterTmp.department),
        jobTitle: filterTmp.jobTitle && Number(filterTmp.jobTitle),
        status: filterTmp.status && Number(filterTmp.status),
      });
      const paginationTmp = {
        page: filterParams.page ? parseInt(`${filterParams.page}`, 10) : 1,
        pageSize: filterParams.pageSize ? parseInt(`${filterParams.pageSize}`, 10) : 10,
      } as PaginationFilter;
      setPagination(paginationTmp);
    } else {
      dispatch(
        goToReplace({
          search: queryString.stringify({
            ...defaultInvoicesFilter,
            filters: queryString.stringify({}),
          }),
        }),
      );
    }
  }, [dispatch, location.search]);

  const fetchData = React.useCallback(
    async (filterParams: InvoicesFilter, paginationParams: PaginationFilter) => {
      setLoading(true);
      const json = await dispatch(
        fetchThunk(
          API_PATHS.invoicesList,
          'post',
          JSON.stringify({
            filters: { ...filterParams, createdBy: filterParams?.createdBy?.id },
            ...paginationParams,
          }),
        ),
      );
      if (json.code === SUCCESS_CODE) {
        setData(json.data);
      } else {
        enqueueSnackbar(
          json.message,
          snackbarSetting(key => closeSnackbar(key), { color: 'error' }),
        );
      }
      setLoading(false);
    },
    [closeSnackbar, dispatch, enqueueSnackbar],
  );

  const onInvoiceCancel = React.useCallback(
    async (tripIds: number[]) => {
      setLoading(true);
      const json = await dispatch(
        fetchThunk(API_PATHS.invoicesRequestsCancel, 'put', JSON.stringify({tripIds})),
      );
      if (json.code === SUCCESS_CODE) {
        setInvoiceCancelInfo([]);
        setData((one: some) => {
          const newData = one.itemList.filter((item: some) => tripIds.includes(item.trip.id)).map((item: some) => ({ ...item, status: 4 }))
          return {
            ...one,
            itemList: one.itemList.map((obj:some) => newData.find((o: some) => o.id === obj.id) || obj)
          }
        })
        enqueueSnackbar(
          json.message,
          snackbarSetting(key => closeSnackbar(key)),
        );
      } else {
        enqueueSnackbar(
          json.message,
          snackbarSetting(key => closeSnackbar(key), { color: 'error' }),
        );
      }
      setLoading(false);
    },
    [closeSnackbar, dispatch, enqueueSnackbar],
  );

  const onInvoiceRequest = React.useCallback(
    async (values: some) => {
      setLoading(true);
      const json = await dispatch(
        fetchThunk(API_PATHS.invoicesRequests, 'put', JSON.stringify(values)),
      );
      if (json.code === SUCCESS_CODE) {
        setInvoiceRequestInfo(undefined);
        setData((one: some) => {
          const newData = one.itemList.filter((item: some) => values.tripIds.includes(item.trip.id)).map((item: some) => ({ ...item, status: 3 }))
          return {
            ...one,
            itemList: one.itemList.map((obj:some) => newData.find((o: some) => o.id === obj.id) || obj)
          }
        })
        enqueueSnackbar(
          json.message,
          snackbarSetting(key => closeSnackbar(key)),
        );
      } else {
        enqueueSnackbar(
          json.message,
          snackbarSetting(key => closeSnackbar(key), { color: 'error' }),
        );
      }
      setLoading(false);
    },
    [closeSnackbar, dispatch, enqueueSnackbar],
  );

  React.useEffect(() => {
    updateQueryParams();
  }, [location.search, updateQueryParams]);

  React.useEffect(() => {
    fetchData(filter, pagination);
  }, [fetchData, filter, pagination]);

  return (
    <TableBox
      loading={loading}
      data={data}
      filter={filter}
      invoiceRequestInfo={invoiceRequestInfo}
      setInvoiceRequestInfo={setInvoiceRequestInfo}
      onInvoiceRequest={onInvoiceRequest}
      invoiceCancelInfo={invoiceCancelInfo}
      setInvoiceCancelInfo={setInvoiceCancelInfo}
      onInvoiceCancel={(tripIds: number[]) => onInvoiceCancel(tripIds)}
      onUpdateFilter={values => {
        dispatch(
          goToReplace({
            search: queryString.stringify({
              ...pagination,
              page: 1,
              filters: queryString.stringify({
                ...values,
                createdBy: values.createdBy && queryString.stringify(values.createdBy),
              }),
            }),
          }),
        );
      }}
      pagination={pagination}
      onUpdatePagination={values => {
        dispatch(
          goToReplace({
            search: queryString.stringify({
              ...values,
              filters: queryString.stringify({
                ...filter,
                createdBy: filter.createdBy && queryString.stringify(filter.createdBy),
              }),
            }),
          }),
        );
      }}
    />
  );
};

export default InvoicesManager;
