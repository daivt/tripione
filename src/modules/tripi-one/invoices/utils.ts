import { some } from "../../../constants";
import { GREEN, ORANGE, PINK, PURPLE, RED } from '../../../configs/colors';
import { SelectItem } from '../../../models/object';

export interface InvoicesFilter {
  created?: number;
  tripName?: string;
  createdBy?: SelectItem | undefined;
  department: SelectItem | undefined;
  jobTitle: SelectItem | undefined;
  status: SelectItem | undefined;
}
export const defaultInvoicesFilter: InvoicesFilter = {
  createdBy: undefined,
  department: undefined,
  jobTitle: undefined,
  status: undefined,
};
export interface InvoiceCompanyInfo {
  requestID: string;
}
export interface RequestForm {
  email: string;
  mobile: string;
  tripIds: some | undefined;
}
export const defaultRequestForm: RequestForm = {
  email: '',
  mobile: '',
  tripIds: undefined,
};

export function getStatusColorInvoice(status: number) {
  if (status === 0) {
    return PINK;
  }
  if (status === 1) {
    return ORANGE;
  }
  if (status === 2) {
    return PURPLE;
  }
  if (status === 3) {
    return GREEN;
  }
  if (status === 4) {
    return RED;
  }
  return 'black';
}
