import { Button, Checkbox, DialogActions, Divider, IconButton, Typography } from '@material-ui/core';
import moment from 'moment';
import React from 'react';
import { FormattedMessage } from 'react-intl';
import { shallowEqual, useSelector } from 'react-redux';
import { GREY_700, PURPLE } from '../../../../configs/colors';
import { some } from '../../../../constants';
import { DATE_FORMAT } from '../../../../models/moment';
import { PaginationFilter } from '../../../../models/pagination';
import { AppState } from '../../../../redux/reducers';
import { ReactComponent as InformationIcon } from '../../../../svg/ic_info.svg';
import DialogCustom from '../../../common/components/DialogCustom';
import { Row } from '../../../common/components/elements';
import TableCustom, { Columns } from '../../../common/components/TableCustom';
import { getStatusColorInvoice, InvoicesFilter, RequestForm } from '../utils';
import DetailDialog from './DetailDialog';
import Filter from './Filter';
import RequestDialog from './RequestDialog';

interface Props {
  loading: boolean;
  filter: InvoicesFilter;
  onUpdateFilter(filter: InvoicesFilter): void;
  data?: some;
  pagination: PaginationFilter;
  onUpdatePagination(pagination: PaginationFilter): void;
  invoiceCancelInfo: number[];
  setInvoiceCancelInfo(data: number[]): void;
  onInvoiceCancel(data: number[]): void;
  invoiceRequestInfo: some | undefined;
  setInvoiceRequestInfo(values: some | undefined): void;
  onInvoiceRequest(values: some | undefined): void;
}

const TableBox: React.FC<Props> = props => {
  const {
    loading,
    filter,
    onUpdateFilter,
    data,
    pagination,
    onUpdatePagination,
    invoiceRequestInfo,
    setInvoiceRequestInfo,
    onInvoiceRequest,
    invoiceCancelInfo,
    setInvoiceCancelInfo,
    onInvoiceCancel,
  } = props;
  const { invoiceStatus } = useSelector((state: AppState) => state.common, shallowEqual);
  const [detail, setDetail] = React.useState<some | undefined>(undefined);
  const [tripIds, setTripIds] = React.useState<some[]>([]);

  const showCheckboxButton = React.useCallback(
    (record: some) => {
      if (record.status === 0 || record.status === 1) {
        return (<Checkbox
          color="secondary"
          disabled={tripIds.length > 0 && tripIds.findIndex((item: some) => item.status === record?.status) === -1}
          onChange={event => {
            if (tripIds.findIndex((item: some) => item.id === record?.trip.id) !== -1) {
              setTripIds((one: some[]) => {
                return one.filter((item: some) => item.id !== record?.trip.id)
              })
            } else {
              setTripIds((one: some[]) => {
                return [ ...one, {id: record?.trip.id, status: record?.status}]
              })
            }
          }}
        />)
      }
      return null
    },
    [tripIds]
  )

  const columns = React.useMemo(() => {
    return [
      {
        title: 'invoices.exportDate',
        render: (record: some, index: number) => (
          <Typography variant="caption">
            {moment(record.exportDate).isValid()
              ? moment(record.exportDate).format(DATE_FORMAT)
              : null}
          </Typography>
        ),
      },
      {
        title: 'invoices.tripName',
        render: (record: some, index: number) => (
          <Typography variant="caption">{record.trip?.name}</Typography>
        ),
      },
      {
        title: 'invoices.createdBy',
        render: (record: some, index: number) => (
          <Typography variant="caption">{record.trip?.createdBy}</Typography>
        ),
      },
      {
        title: 'position',
        render: (record: some, index: number) => (
          <Typography variant="caption">{record.trip?.jobTitle?.name}</Typography>
        ),
      },
      {
        title: 'department',
        render: (record: some, index: number) => (
          <Typography variant="caption">{record.trip?.department?.name}</Typography>
        ),
      },
      {
        title: 'invoices.status',
        render: (record: some, index: number) => (
          <Typography
            variant="caption"
            style={{ color: `${getStatusColorInvoice(record.status)}` }}
          >
            <FormattedMessage id={invoiceStatus.find((v: some) => v.id === record.status)?.name} />
          </Typography>
        ),
      },
      {
        title: 'invoices.requestDate',
        dataIndex: 'created',
        render: (record: some, index: number) => (
          <Typography variant="caption">
            {moment(record.created).isValid() ? moment(record.created).format(DATE_FORMAT) : null}
          </Typography>
        ),
      },
      {
        disableAction: true,
        width: 80,
        render: (record: some, index: number) => (
          <IconButton
            onClick={() => {
              setDetail(record);
            }}
            style={{ padding: 4 }}
          >
            <InformationIcon className="svgFillAll" style={{ stroke: PURPLE }}/>
          </IconButton>
        ),
      },
      {
        disableAction: true,
        width: 120,
        render: (record: some, index: number) => showCheckboxButton(record),
      },
    ] as Columns[];
  }, [invoiceStatus, showCheckboxButton]);

  React.useEffect(() => {
    if (!invoiceRequestInfo || !invoiceCancelInfo) setTripIds([])
  }, [invoiceCancelInfo, invoiceRequestInfo]);

  return (
    <>
      <Filter loading={loading} filter={filter} onUpdateFilter={onUpdateFilter} />
      <TableCustom
        dataSource={data?.itemList}
        loading={loading}
        columns={columns}
        style={{ marginTop: 24 }}
        noColumnIndex
        header={
          <Row style={{ padding: '16px 12px', justifyContent: 'space-between' }}>
            <Typography variant="subtitle2">
              <FormattedMessage id="invoices.invoicesList" />
            </Typography>
            <div>
              <Button
                variant="contained"
                color="secondary"
                disableElevation
                disabled={tripIds.length === 0 || tripIds.findIndex((item: some) => item.status === 1) !== -1}
                style={{ minWidth: 140, marginRight: 12}}
                onClick={e => {
                  setInvoiceRequestInfo(data?.itemList[0]);
                }}
              >
                <Typography variant="caption">
                  <FormattedMessage id="invoices.sendRequest" />
                </Typography>
              </Button>
              <Button
                variant="outlined"
                color="secondary"
                disableElevation
                disabled={tripIds.length === 0 || tripIds.findIndex((item: some) => item.status === 0) !== -1}
                style={{ minWidth: 120}}
                onClick={e => {
                  setInvoiceCancelInfo(tripIds.map((item: some) => item.id));
                }}
              >
                <Typography variant="caption">
                  <FormattedMessage id="invoices.cancelRequest" />
                </Typography>
              </Button>
            </div>
          </Row>
        }
        paginationProps={{
          count: data?.totalResults || 0,
          page: pagination.page || 1,
          rowsPerPage: pagination?.pageSize || 0,
          onChangePage: (event: unknown, newPage: number) => {
            onUpdatePagination({ ...pagination, page: newPage });
          },
          onChangeRowsPerPage: (event: React.ChangeEvent<HTMLInputElement>) => {
            onUpdatePagination({
              pageSize: parseInt(event.target.value, 10),
              page: 1,
            });
          },
        }}
        onRowClick={(record: some) => setDetail(record)}
      />
      <DetailDialog open={!!detail} onClose={() => setDetail(undefined)} detailData={detail} />
      <DialogCustom
        open={invoiceCancelInfo.length > 0}
        onClose={() => {
          setInvoiceCancelInfo([]);
        }}
        PaperProps={{ style: { minWidth: 400 } }}
        titleLabel={
          <Typography variant="subtitle1">
            <FormattedMessage id="invoices.cancelRequest" />
          </Typography>
        }
        footerContent={<div />}
      >
        <>
          <Typography variant="body2" style={{ color: GREY_700, margin: '16px 16px 56px' }}>
            <FormattedMessage id="invoices.cancelRequestMessenger" />
          </Typography>
          <Divider />
          <DialogActions style={{ padding: 16, justifyContent: 'flex-end' }}>
            <Button
              variant="contained"
              color="secondary"
              style={{ padding: '0px 32px' }}
              disableElevation
              onClick={() => {
                onInvoiceCancel(invoiceCancelInfo);
              }}
            >
              <Typography variant="body2">
                <FormattedMessage id="confirm" />
              </Typography>
            </Button>
          </DialogActions>
        </>
      </DialogCustom>
      <RequestDialog
        data={invoiceRequestInfo}
        onClose={() => setInvoiceRequestInfo(undefined)}
        onInvoiceRequest={(values: RequestForm) =>
          onInvoiceRequest({ ...values, tripIds: tripIds.map((item: some) => item.id) })
        }
      />
    </>
  );
};

export default TableBox;
