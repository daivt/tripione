import { Button, DialogActions, Divider, Typography } from '@material-ui/core';
import moment from 'moment';
import React from 'react';
import { FormattedMessage, FormattedNumber } from 'react-intl';
import { shallowEqual, useSelector } from 'react-redux';
import { BLUE, GREY_700, PURPLE, PURPLE_50, WHITE } from '../../../../configs/colors';
import { some } from '../../../../constants';
import { DATE_FORMAT } from '../../../../models/moment';
import { ReactComponent as BillIcon } from '../../../../svg/Invoice_Bill.svg';
import { ReactComponent as DownloadIcon } from '../../../../svg/Invoice_download.svg';
import DialogCustom from '../../../common/components/DialogCustom';
import { Col, Line, Row } from '../../../common/components/elements';
import { getStatusColorInvoice } from '../utils';
import { AppState } from '../../../../redux/reducers';

interface Props {
  open: boolean;
  onClose: () => void;
  detailData?: some;
}
const DetailDialog: React.FC<Props> = props => {
  const { open, onClose, detailData } = props;
  const { invoiceStatus } = useSelector((state: AppState) => state.common, shallowEqual);
  return (
    <DialogCustom
      open={open}
      onClose={onClose}
      PaperProps={{ style: { width: 770 } }}
      titleLabel={
        <Typography variant="subtitle1">
          <FormattedMessage id="invoices.billDetail" />
        </Typography>
      }
      buttonLabel="close"
      footerContent={
        detailData?.status.id === 1 ? (
          <DialogActions style={{ padding: 16, justifyContent: 'flex-end' }}>
            <Button
              type="submit"
              variant="contained"
              color="secondary"
              style={{ padding: '0px 32px' }}
              disableElevation
            >
              <Typography variant="body2">
                <FormattedMessage id="invoices.sendRequest" />
              </Typography>
            </Button>
            <Button
              variant="contained"
              style={{ padding: '0px 24px', backgroundColor: WHITE, border: '1px solid #9E9E9E' }}
              disableElevation
              onClick={onClose}
            >
              <Typography variant="body2" style={{ color: GREY_700 }}>
                <FormattedMessage id="reject" />
              </Typography>
            </Button>
          </DialogActions>
        ) : null
      }
    >
      <Col>
        <Row style={{ margin: '24px 32px', alignItems: 'start' }}>
          <Col style={{ marginRight: 64 }}>
            <Typography variant="subtitle2" color="primary">
              <FormattedMessage id="invoices.billInformation" />
            </Typography>
            <Line>
              <Typography variant="body2" style={{ minWidth: 90, marginRight: 55 }}>
                <FormattedMessage id="invoices.companyName" />:
              </Typography>
              <Typography variant="body2">{detailData?.invoiceCompanyInfo.companyName}</Typography>
            </Line>
            <Line>
              <Typography variant="body2" style={{ minWidth: 90, marginRight: 55 }}>
                <FormattedMessage id="invoices.taxId" />:
              </Typography>
              <Typography variant="body2">{detailData?.invoiceCompanyInfo.taxCode}</Typography>
            </Line>
            <Line>
              <Typography variant="body2" style={{ minWidth: 90, marginRight: 55 }}>
                <FormattedMessage id="email" />:
              </Typography>
              <Typography variant="body2">{detailData?.email}</Typography>
            </Line>
            <Line>
              <Typography variant="body2" style={{ minWidth: 90, marginRight: 55 }}>
                <FormattedMessage id="phoneNumber" />:
              </Typography>
                <Typography variant="body2">{detailData?.phone}</Typography>
            </Line>
            <Line>
              <Typography variant="body2" style={{ minWidth: 90, marginRight: 55 }}>
                <FormattedMessage id="address" />:
              </Typography>
              <Typography variant="body2">{detailData?.address}</Typography>
            </Line>
            {detailData?.requestDate && (
              <Line>
                <Typography variant="body2" style={{ minWidth: 90, marginRight: 55 }}>
                  <FormattedMessage id="invoices.requestDate" />:
                </Typography>
                <Typography variant="body2">
                  {moment(detailData?.requestDate).format(DATE_FORMAT)}
                </Typography>
              </Line>
            )}
            <Line>
              <Typography variant="body2" style={{ minWidth: 90, marginRight: 55 }}>
                <FormattedMessage id="invoices.status" />:
              </Typography>
              <Typography
                variant="body2"
                style={{
                  color: `${getStatusColorInvoice(detailData?.status)}`,
                }}
              >
                <FormattedMessage
                  id={invoiceStatus.find((v: some) => v.id === detailData?.status)?.name}
                />
              </Typography>
            </Line>
            {detailData?.exportDate && (
              <Line>
                <Typography variant="body2" style={{ minWidth: 90, marginRight: 55 }}>
                  <FormattedMessage id="invoices.exportDate" />:
                </Typography>
                <Typography variant="body2">
                  {moment(detailData?.exportDate).format(DATE_FORMAT)}
                </Typography>
              </Line>
            )}
          </Col>
          <Col>
            <Typography variant="subtitle2" color="primary">
              <FormattedMessage id="invoices.orderInformation" />
            </Typography>
            <Line>
              <Typography variant="body2" style={{ minWidth: 90, marginRight: 16 }}>
                <FormattedMessage id="invoices.tripName" />:
              </Typography>
              <Typography variant="body2">{detailData?.trip.name}</Typography>
            </Line>
            <Line>
              <Typography variant="body2" style={{ minWidth: 90, marginRight: 16 }}>
                <FormattedMessage id="invoices.createdBy" />:
              </Typography>
              <Typography variant="body2">{detailData?.trip.createdBy}</Typography>
            </Line>
            <Line>
              <Typography variant="body2" style={{ minWidth: 90, marginRight: 16 }}>
                <FormattedMessage id="position" />:
              </Typography>
              <Typography variant="body2">{detailData?.trip.jobTitle.name}</Typography>
            </Line>
            <Line>
              <Typography variant="body2" style={{ minWidth: 90, marginRight: 16 }}>
                <FormattedMessage id="department" />:
              </Typography>
              <Typography variant="body2">{detailData?.trip.department.name}</Typography>
            </Line>
            <Line>
              <Typography variant="body2" style={{ minWidth: 90, marginRight: 16 }}>
                <FormattedMessage id="invoices.amount" />:
              </Typography>
              <Typography variant="body2">
                <FormattedNumber value={detailData?.trip.amount} />
                &nbsp;
                <FormattedMessage id="currency" />
              </Typography>
            </Line>
          </Col>
        </Row>
        {detailData?.invoiceFiles?.length > 0 && (
          <Col style={{ margin: '0px 32px 27px' }}>
            <Divider />
            <Typography variant="subtitle2" color="primary" style={{ marginTop: 16 }}>
              <FormattedMessage id="invoices.listBill" />
            </Typography>
            <Line>
              {detailData?.invoiceFiles.map((v: some, index: number) => (
                <Line key={index} style={{ marginRight: 43 }}>
                  <BillIcon />
                  <Typography variant="body2" style={{ color: BLUE }}>
                    <FormattedMessage id="invoices.bill" />
                    &nbsp;{index + 1}
                  </Typography>
                </Line>
              ))}
              <Button style={{ backgroundColor: PURPLE_50 }}>
                <DownloadIcon />
                <Typography variant="body2" style={{ color: PURPLE, marginLeft: 8 }}>
                  <FormattedMessage id="invoices.downloadBill" />
                </Typography>
              </Button>
            </Line>
          </Col>
        )}
      </Col>
    </DialogCustom>
  );
};

export default DetailDialog;
