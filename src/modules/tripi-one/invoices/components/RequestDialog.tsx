import { Button, DialogActions, Divider, Typography } from '@material-ui/core';
import { useFormik } from 'formik';
import React from 'react';
import { FormattedMessage, useIntl } from 'react-intl';
import { shallowEqual, useSelector } from 'react-redux';
import * as yup from 'yup';
import { some } from '../../../../constants';
import { AppState } from '../../../../redux/reducers';
import DialogCustom from '../../../common/components/DialogCustom';
import { Col, Row } from '../../../common/components/elements';
import { NumberFormatCustom2, redMark } from '../../../common/components/Form';
import FormControlTextField from '../../../common/components/FormControlTextField';
import { defaultRequestForm, RequestForm } from '../utils';

interface Props {
  onClose: () => void;
  onInvoiceRequest(values: RequestForm): void;
  data?: some;
}

const RequestDialog: React.FC<Props> = props => {
  const { onClose, data, onInvoiceRequest } = props;
  const userData = useSelector((state: AppState) => state.account.userData, shallowEqual);
  const intl = useIntl();
  const requestSchema = yup.object().shape({
    email: yup
      .string()
      .trim()
      .nullable()
      .required(intl.formatMessage({ id: 'required' }))
      .email(intl.formatMessage({ id: 'emailInvalid' })),
    mobile: yup
      .string()
      .trim()
      .nullable()
      .required(intl.formatMessage({ id: 'required' })),
  });
  const formik = useFormik({
    initialValues: defaultRequestForm,
    onSubmit: values => {
      onInvoiceRequest(values);
    },
    validationSchema: requestSchema,
  });

  React.useEffect(() => {
    formik.setValues({ ...defaultRequestForm, email: userData?.email, mobile: userData?.phone });
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [userData]);

  return (
    <DialogCustom
      open={!!data}
      onClose={onClose}
      PaperProps={{ style: { width: 500 } }}
      titleLabel={
        <Typography variant="subtitle1">
          <FormattedMessage id="invoices.billDetail" />
        </Typography>
      }
      footerContent={<div />}
    >
      <form onSubmit={formik.handleSubmit}>
        <Col style={{ margin: '24px 24px' }}>
          <Row style={{ alignItems: 'start', marginTop: 8 }}>
            <Typography variant="body2" style={{ minWidth: 90, marginRight: 25 }}>
              <FormattedMessage id="invoices.companyName" />:
            </Typography>
            <Typography variant="body2">{data?.invoiceCompanyInfo?.companyName}</Typography>
          </Row>
          <Row style={{ alignItems: 'start', marginTop: 8 }}>
            <Typography variant="body2" style={{ minWidth: 90, marginRight: 25 }}>
              <FormattedMessage id="invoices.taxId" />:
            </Typography>
            <Typography variant="body2">{data?.invoiceCompanyInfo?.taxCode}</Typography>
          </Row>
          <Row style={{ alignItems: 'start', marginTop: 8 }}>
            <Typography variant="body2" style={{ minWidth: 90, marginRight: 25 }}>
              <FormattedMessage id="address" />:
            </Typography>
            <Typography variant="body2">{data?.invoiceCompanyInfo?.address}</Typography>
          </Row>
          <Row style={{ marginTop: 16 }}>
            <Typography variant="body2" style={{ minWidth: 90, marginRight: 25, marginBottom: 20 }}>
              <FormattedMessage id="email" />
              {redMark}
            </Typography>
            <FormControlTextField
              id="email"
              placeholder={intl.formatMessage({ id: 'invoices.enterEmail' })}
              inputProps={{
                maxLength: '100',
              }}
              formControlStyle={{ margin: 0 }}
              value={formik.values.email}
              onChange={formik.handleChange}
              errorMessage={
                formik.errors.email && formik.submitCount > 0 ? formik.errors.email : undefined
              }
            />
          </Row>
          <Row>
            <Typography variant="body2" style={{ minWidth: 90, marginRight: 25, marginBottom: 20 }}>
              <FormattedMessage id="phoneNumber" />
              {redMark}
            </Typography>
            <FormControlTextField
              name="mobile"
              placeholder={intl.formatMessage({ id: 'enterPhoneNumber' })}
              inputProps={{
                maxLength: '100',
              }}
              formControlStyle={{ margin: 0 }}
              inputComponent={NumberFormatCustom2 as any}
              value={formik.values.mobile}
              onChange={formik.handleChange}
              errorMessage={
                formik.errors.mobile && formik.submitCount > 0
                  ? formik.errors.mobile
                  : undefined
              }
            />
          </Row>
        </Col>
        <Divider />
        <DialogActions style={{ padding: 16, justifyContent: 'flex-end' }}>
          <Button
            type="submit"
            variant="contained"
            color="secondary"
            style={{ padding: '0px 32px' }}
            disableElevation
          >
            <Typography variant="body2">
              <FormattedMessage id="invoices.sendRequest" />
            </Typography>
          </Button>
        </DialogActions>
      </form>
    </DialogCustom>
  );
};

export default RequestDialog;
