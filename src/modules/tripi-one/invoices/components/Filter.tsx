import { Button } from '@material-ui/core';
import { useFormik } from 'formik';
import moment from 'moment';
import React from 'react';
import { FormattedMessage, useIntl } from 'react-intl';
import { useSelector, shallowEqual, useDispatch } from 'react-redux';
import { ThunkDispatch } from 'redux-thunk';
import { Action } from 'redux';
import { API_PATHS } from '../../../../configs/API';
import { some } from '../../../../constants';
import { AppState } from '../../../../redux/reducers';
import { ReactComponent as RefreshIcon } from '../../../../svg/ic_refresh.svg';
import { PAGE_SIZE_20 } from '../../../booking/constants';
import DateField from '../../../common/components/DateField';
import { Row } from '../../../common/components/elements';
import FormControlAutoComplete from '../../../common/components/FormControlAutoComplete';
import FormControlTextField from '../../../common/components/FormControlTextField';
import LoadingButton from '../../../common/components/LoadingButton';
import SingleSelect from '../../../common/components/SingleSelect';
import { trimObjectValues } from '../../../utils';
import { fetchThunk } from '../../../common/redux/thunk';
import { defaultInvoicesFilter, InvoicesFilter } from '../utils';

interface Props {
  filter: InvoicesFilter;
  loading: boolean;
  onUpdateFilter(filter: InvoicesFilter): void;
}
const Filter: React.FC<Props> = props => {
  const dispatch = useDispatch<ThunkDispatch<AppState, null, Action<string>>>();
  const { invoiceStatus, departmentOptions, positionOptions } = useSelector(
    (state: AppState) => state.common,
    shallowEqual,
  );
  const { filter, loading, onUpdateFilter } = props;
  const intl = useIntl();
  const formik = useFormik({
    initialValues: filter,
    onSubmit: values => {
      onUpdateFilter(trimObjectValues(values));
    },
  });

  React.useEffect(() => {
    formik.setValues(filter);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [filter]);

  return (
    <form onSubmit={formik.handleSubmit}>
      <Row style={{ flexWrap: 'wrap' }}>
        <DateField
          label={<FormattedMessage id="invoices.requestDate" />}
          style={{ width: 250 }}
          optional
          date={formik.values.created ? moment(formik.values.created) : undefined}
          onChange={date => {
            formik.setFieldValue('created', date?.valueOf());
          }}
        />
        <FormControlAutoComplete
          label={<FormattedMessage id="tripManagement.creatorName" />}
          placeholder={intl.formatMessage({ id: 'tripManagement.insertCreatorName' })}
          value={formik.values.createdBy || undefined}
          formControlStyle={{ width: 250 }}
          onChange={(e: any, value: some | null) => {
            formik.setFieldValue('createdBy', value, true);
          }}
          loadOptions={async (str: string) => {
            const json = await dispatch(
              fetchThunk(
                API_PATHS.getListUser,
                'post',
                JSON.stringify({
                  filters: { str },
                  page: 1,
                  pageSize: PAGE_SIZE_20,
                }),
              ),
            );
            return json.data?.itemList.map((v: some) => {
              return {
                id: v.id,
                name: v.name,
              };
            });
          }}
          getOptionSelected={(option: some, value: some) => {
            return option.id === value.id;
          }}
          getOptionLabel={(v: some) => v?.name || ''}
          filterOptions={(options, state) => options}
          options={[]}
          optional
        />
        <FormControlTextField
          id="tripName"
          formControlStyle={{ width: 250 }}
          label={<FormattedMessage id="tripManagement.tripName" />}
          placeholder={intl.formatMessage({ id: 'tripManagement.insertTripName' })}
          value={formik.values.tripName}
          onChange={formik.handleChange}
          inputProps={{
            maxLength: 50,
          }}
        />
        <SingleSelect
          value={formik.values?.department || undefined}
          label={<FormattedMessage id="department" />}
          formControlStyle={{ width: 250 }}
          onSelectOption={(value: any) => {
            formik.setFieldValue('department', value);
          }}
          getOptionLabel={value => value.name}
          options={[
            { id: undefined, name: intl.formatMessage({ id: 'all' }) },
            ...departmentOptions,
          ]}
          optional
        />
        <SingleSelect
          value={formik.values?.jobTitle}
          label={<FormattedMessage id="position" />}
          formControlStyle={{ width: 250 }}
          onSelectOption={(value: any) => {
            formik.setFieldValue('jobTitle', value);
          }}
          getOptionLabel={value => value.name}
          options={[{ id: undefined, name: intl.formatMessage({ id: 'all' }) }, ...positionOptions]}
          optional
        />
        <SingleSelect
          value={formik.values?.status}
          label={<FormattedMessage id="status" />}
          formControlStyle={{ width: 250 }}
          onSelectOption={(value: any) => {
            formik.setFieldValue('status', value);
          }}
          getOptionLabel={value => intl.formatMessage({ id: value.name })}
          options={invoiceStatus}
          optional
        />
        <Row>
          <LoadingButton
            loading={loading}
            type="submit"
            variant="contained"
            size="large"
            style={{ minWidth: '140px', marginRight: '20px' }}
            color="secondary"
            disableElevation
            onClick={() => console.log(formik.values)}
          >
            <FormattedMessage id="search" />
          </LoadingButton>
          <Button
            onClick={() => {
              formik.setValues(defaultInvoicesFilter);
              onUpdateFilter(defaultInvoicesFilter);
            }}
          >
            <RefreshIcon />
          </Button>
        </Row>
      </Row>
    </form>
  );
};

export default Filter;
