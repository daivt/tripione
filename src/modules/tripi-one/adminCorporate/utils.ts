import { some } from '../../../constants';

export interface IAdminCorporateFilter {
  name?: string;
  taxCode?: string;
  email?: string;
  isActive?: boolean;
  fromDate: number | undefined;
  toDate: number | undefined;
}

export const defaultAdminCorporateFilter: IAdminCorporateFilter = {
  fromDate: undefined,
  toDate: undefined,
}
export interface IChangeAccount {
  user: some | null;
  note?: string
}

export const defaultChangeAccount: IChangeAccount = {
  user: null
}

export interface CorporateCompanyInfo {
  id?: number;
  name?: string;
  shortName?: string;
  address?: string;
  taxCode?: number;
  contactPersonName?: string;
  email?: string;
  companySizeRange: some | null;
  referrerPhone?: string;
  licenseAccount: some | null;
  representative?: string;
  representativePhone?: string;
  creditAmount?: number;
  isActive?: boolean;
  contractEndDate: number | undefined;
  contractStartDate: number | undefined;
  currencyCode?: string;
}

export const defaultCompanyInfo: CorporateCompanyInfo = {
  companySizeRange: null,
  licenseAccount: null,
  contractStartDate: undefined,
  contractEndDate: undefined,
};
