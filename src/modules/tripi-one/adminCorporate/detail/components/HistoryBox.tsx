import { Button, Typography } from '@material-ui/core';
import moment from 'moment';
import * as React from 'react';
import { FormattedMessage } from 'react-intl';
import { some } from '../../../../../constants';
import { DATE_TIME_FORMAT } from '../../../../../models/moment';
import { PaginationFilter } from '../../../../../models/pagination';
import DialogCustom from '../../../../common/components/DialogCustom';
import { Row } from '../../../../common/components/elements';
import TableCustom, { Columns } from '../../../../common/components/TableCustom';

interface Props {
  data?: some;
  loading: boolean;
  pagination: PaginationFilter;
  onUpdatePagination(pagination: PaginationFilter): void;
}

const HistoryBox = (props: Props) => {
  const { loading, data, pagination, onUpdatePagination } = props;
  const [detail, setDetail] = React.useState<string>('');

  const columns = React.useMemo(() => {
    return [
      {
        title: 'admin.corporate.description',
        dataIndex: 'content',
        variant: 'caption',
      },
      {
        title: 'admin.corporate.oldValue',
        dataIndex: 'oldValue',
        variant: 'caption',
      },
      {
        title: 'admin.corporate.newValue',
        dataIndex: 'newValue',
        variant: 'caption',
      },
      {
        title: 'admin.corporate.user',
        dataIndex: 'createdBy',
        variant: 'caption',
      },
      {
        title: 'admin.corporate.createdTime',
        dataIndex: 'createdTime',
        variant: 'caption',
        render: (record: some, index: number) => (
          <Typography variant="caption">
            {moment(record.createdDate).isValid()
              ? moment(record.createdDate).format(DATE_TIME_FORMAT)
              : null}
          </Typography>
        ),
      },
      {
        title: 'note',
        render: (record: some, index: number) =>
          record.detail && (
            <Button
              variant="text"
              color="secondary"
              size="medium"
              onClick={() => setDetail(record.detail)}
              disableElevation
            >
              <Typography variant="caption">
                <FormattedMessage id="detail" />
              </Typography>
            </Button>
          ),
      },
    ] as Columns[];
  }, []);
  return (
    <>
      <TableCustom
        dataSource={data?.itemList}
        loading={loading}
        columns={columns}
        style={{ minWidth: '688px', marginBottom: 16 }}
        header={
          <Row style={{ padding: '16px 12px' }}>
            <Typography variant="subtitle2" style={{ fontWeight: 500, marginRight: '24px' }}>
              <FormattedMessage id="admin.corporate.history" />
            </Typography>
          </Row>
        }
        paginationProps={{
          count: data?.totalResults || 0,
          page: pagination.page || 1,
          rowsPerPage: pagination?.pageSize || 0,
          onChangePage: (event: unknown, newPage: number) => {
            onUpdatePagination({ ...pagination, page: newPage });
          },
          onChangeRowsPerPage: (event: React.ChangeEvent<HTMLInputElement>) => {
            onUpdatePagination({
              pageSize: parseInt(event.target.value, 10),
              page: 1,
            });
          },
        }}
      />
      <DialogCustom
        titleLabel={
          <Typography variant="subtitle2">
            <FormattedMessage id="admin.approval.detail" />
          </Typography>
        }
        PaperProps={{ style: { width: 380 } }}
        open={!!detail}
        onClose={() => setDetail('')}
      >
        <Typography variant="caption" style={{ padding: 16 }}>
          {detail}
        </Typography>
      </DialogCustom>
    </>
  );
};

export default HistoryBox;
