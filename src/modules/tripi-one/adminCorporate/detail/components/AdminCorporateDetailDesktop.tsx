import { Button, Typography } from '@material-ui/core';
import * as React from 'react';
import { FormattedMessage, useIntl } from 'react-intl';
import { some } from '../../../../../constants';
import { PaginationFilter } from '../../../../../models/pagination';
import ConfirmDialog from '../../../../common/components/ConfirmDialog';
import { Col, Row } from '../../../../common/components/elements';
import FormControlTextField from '../../../../common/components/FormControlTextField';
import { CorporateCompanyInfo, IChangeAccount } from '../../utils';
import ChangeAccountModal from './ChangeAccountModal';
import CompanyInfoBox from './CompanyInfoBox';
import HistoryBox from './HistoryBox';

interface Props {
  id: string;
  data: CorporateCompanyInfo;
  loading: boolean;
  updateInfo(value: CorporateCompanyInfo): void;
  onChangeAccount(value: IChangeAccount): void;
  lockAccount(id: string): void;
  unlockAccount(id: string): void;
  historyData?: some;
  historyLoading: boolean;
  historyPagination: PaginationFilter;
  onUpdatePagination(pagination: PaginationFilter): void;
  modalOpen: boolean;
  setModalOpen(value: boolean): void;
  changeAccountOpen: boolean;
  setChangeAccountOpen(value: boolean): void;
  note: string;
  setNote(note: string): void;
}

const AdminCorporateDetailDesktop = (props: Props) => {
  const {
    id,
    data,
    loading,
    updateInfo,
    onUpdatePagination,
    onChangeAccount,
    lockAccount,
    unlockAccount,
    historyData,
    historyLoading,
    historyPagination,
    modalOpen,
    setModalOpen,
    changeAccountOpen,
    setChangeAccountOpen,
    note,
    setNote,
  } = props;

  const intl = useIntl();

  return (
    <Row style={{ flexWrap: 'wrap', alignItems: 'flex-start' }}>
      <CompanyInfoBox
        loading={loading}
        data={data}
        updateInfo={values => {
          updateInfo(values);
        }}
      />
      <Col style={{ flex: 1 }}>
        <HistoryBox
          data={historyData}
          loading={historyLoading}
          pagination={historyPagination}
          onUpdatePagination={onUpdatePagination}
        />
        <Row>
          <Button
            type="submit"
            variant="contained"
            style={{ minWidth: 144, marginRight: 20 }}
            color="secondary"
            size="large"
            disableElevation
            onClick={() => setChangeAccountOpen(true)}
          >
            <FormattedMessage id="admin.corporate.changeAccount" />
          </Button>
          <Button
            disabled={!data.isActive}
            type="submit"
            variant="contained"
            style={{ minWidth: 144, marginRight: 20 }}
            color="secondary"
            size="large"
            disableElevation
            onClick={() => setModalOpen(true)}
          >
            <FormattedMessage id="admin.corporate.lockAccount" />
          </Button>
          <Button
            disabled={data.isActive}
            type="submit"
            variant="contained"
            style={{ minWidth: 144, marginRight: 20 }}
            color="secondary"
            size="large"
            disableElevation
            onClick={() => setModalOpen(true)}
          >
            <FormattedMessage id="admin.corporate.unlockAccount" />
          </Button>
        </Row>
      </Col>
      <ChangeAccountModal
        companyId={data.id}
        open={changeAccountOpen}
        onClose={() => setChangeAccountOpen(false)}
        onChangeAccount={values => onChangeAccount(values)}
      />
      <ConfirmDialog
        titleLabel={
          <Typography variant="subtitle2" style={{ padding: '12px 16px' }}>
            <FormattedMessage id="admin.corporate.changeAccount" />
          </Typography>
        }
        open={modalOpen}
        onClose={() => {
          setNote('');
          setModalOpen(false);
        }}
        onAccept={() => {
          data.isActive ? lockAccount(id) : unlockAccount(id);
          setModalOpen(false);
          setNote('');
        }}
        onReject={() => {
          setNote('');
          setModalOpen(false);
        }}
      >
        <Typography variant="body2" style={{ padding: 16, minHeight: 62 }}>
          <FormattedMessage
            id={
              data.isActive
                ? 'admin.corporate.lockAccountConfirm'
                : 'admin.corporate.unlockAccountConfirm'
            }
          />
        </Typography>
        <FormControlTextField
          id="note"
          formControlStyle={{ width: 467, margin: 'auto', padding: '0 16px' }}
          label={<FormattedMessage id="note" />}
          placeholder={intl.formatMessage({ id: 'enterNote' })}
          value={note}
          onChange={e => setNote(e.target.value)}
          inputProps={{
            maxLength: 50,
          }}
          type="text"
          optional
        />
      </ConfirmDialog>
    </Row>
  );
};

export default AdminCorporateDetailDesktop;
