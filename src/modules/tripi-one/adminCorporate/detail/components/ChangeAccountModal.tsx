import { Button, Divider, Typography } from '@material-ui/core';
import { useFormik } from 'formik';
import * as React from 'react';
import { FormattedMessage, useIntl } from 'react-intl';
import { useDispatch } from 'react-redux';
import { Action } from 'redux';
import { ThunkDispatch } from 'redux-thunk';
import * as yup from 'yup';
import { API_PATHS } from '../../../../../configs/API';
import { AppState } from '../../../../../redux/reducers';
import DialogCustom from '../../../../common/components/DialogCustom';
import { Col, Row } from '../../../../common/components/elements';
import FormControlAutoComplete from '../../../../common/components/FormControlAutoComplete';
import FormControlTextField from '../../../../common/components/FormControlTextField';
import { fetchThunk } from '../../../../common/redux/thunk';
import { defaultChangeAccount, IChangeAccount } from '../../utils';

interface Props {
  onChangeAccount(value: IChangeAccount): void;
  open: boolean;
  onClose(): void;
  companyId?: number;
}

const ChangeAccountModal = (props: Props) => {
  const { onChangeAccount, open, onClose, companyId } = props;
  const dispatch = useDispatch<ThunkDispatch<AppState, null, Action<string>>>();
  const intl = useIntl();

  const changeAccountSchema = yup.object().shape({
    user: yup
      .mixed()
      .nullable()
      .required(intl.formatMessage({ id: 'required' })),
    note: yup.string().trim(),
  });

  const formik = useFormik({
    initialValues: defaultChangeAccount,
    onSubmit: values => {
      onChangeAccount(values);
    },
    validationSchema: changeAccountSchema,
  });

  return (
    <div>
      <DialogCustom
        open={open}
        onClose={onClose}
        PaperProps={{ style: { minWidth: 500 } }}
        titleLabel={
          <Typography variant="subtitle1">
            <FormattedMessage id="admin.corporate.changeAccount" />
          </Typography>
        }
        buttonLabel="close"
        footerContent={<div />}
      >
        <form onSubmit={formik.handleSubmit}>
          <Col style={{ alignItems: 'center', paddingTop: 12 }}>
            <FormControlAutoComplete
              label={<FormattedMessage id="email" />}
              formControlStyle={{ minWidth: 467, marginRight: 0 }}
              value={formik.values.user}
              getOptionSelected={(option, value) => {
                return option.id === value.id;
              }}
              placeholder={intl.formatMessage({ id: 'enterEmail' })}
              onChange={(e: any, value?: any) => {
                formik.setFieldValue('user', value);
              }}
              loadOptions={async (text: string) => {
                const json = await dispatch(
                  fetchThunk(
                    API_PATHS.getCorporateUsers,
                    'post',
                    JSON.stringify({
                      filters: {
                        companyId,
                        searchStr: text,
                      },
                      page: 1,
                      pageSize: 5,
                    }),
                  ),
                );
                return json.data?.itemList;
              }}
              options={[]}
              getOptionLabel={v => v.email}
              errorMessage={
                formik.errors.user && formik.submitCount > 0 ? formik.errors.user : undefined
              }
              autoComplete={false}
              clearOnBlur
            />
            <FormControlTextField
              id="note"
              formControlStyle={{ width: 467, margin: 'auto' }}
              label={<FormattedMessage id="note" />}
              placeholder={intl.formatMessage({ id: 'enterNote' })}
              value={formik.values.note}
              onChange={formik.handleChange}
              inputProps={{
                maxLength: 50,
              }}
              type="text"
              optional
            />
          </Col>
          <Divider />
          <Row style={{ padding: 16, justifyContent: 'flex-end' }}>
            <Button
              type="submit"
              variant="contained"
              color="secondary"
              style={{ marginRight: 12, minWidth: 110 }}
              disableElevation
            >
              <FormattedMessage id="accept" />
            </Button>
            <Button variant="outlined" style={{ minWidth: 92 }} onClick={() => onClose()}>
              <FormattedMessage id="reject" />
            </Button>
          </Row>
        </form>
      </DialogCustom>
    </div>
  );
};

export default ChangeAccountModal;
