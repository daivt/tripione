import { Divider, Paper, Typography } from '@material-ui/core';
import { useFormik } from 'formik';
import moment, { Moment } from 'moment';
import * as React from 'react';
import { FormattedMessage, FormattedNumber, useIntl } from 'react-intl';
import { shallowEqual, useSelector } from 'react-redux';
import * as yup from 'yup';
import { GREEN, GREY } from '../../../../../configs/colors';
import { some } from '../../../../../constants';
import { validNumberRegex } from '../../../../../models/regex';
import { AppState } from '../../../../../redux/reducers';
import DateRangeFormControl from '../../../../common/components/DateRangeFormControl';
import { Col, Row } from '../../../../common/components/elements';
import EmailLink from '../../../../common/components/EmailLink';
import { NumberFormatCustom2 } from '../../../../common/components/Form';
import FormControlAutoComplete from '../../../../common/components/FormControlAutoComplete';
import FormControlTextField from '../../../../common/components/FormControlTextField';
import LoadingButton from '../../../../common/components/LoadingButton';
import LoadingIcon from '../../../../common/components/LoadingIcon';
import { CorporateCompanyInfo } from '../../utils';

interface Props {
  data: CorporateCompanyInfo;
  updateInfo(value: CorporateCompanyInfo): void;
  loading: boolean;
}

const CompanyInfoBox = (props: Props) => {
  const { data, updateInfo, loading } = props;
  const intl = useIntl();
  const generalCompanySize = useSelector(
    (state: AppState) => state.common.generalCompanySize,
    shallowEqual,
  );

  const companyInfoSchema = yup.object().shape({
    companySizeRange: yup
      .object()
      .nullable()
      .required(intl.formatMessage({ id: 'required' })),
    representative: yup
      .string()
      .trim()
      .required(intl.formatMessage({ id: 'required' })),
    referrerPhone: yup
      .string()
      .trim()
      .required(intl.formatMessage({ id: 'required' })),
  });

  const formik = useFormik({
    initialValues: data,
    onSubmit: values => {
      updateInfo(values);
    },
    validationSchema: companyInfoSchema,
  });

  React.useEffect(() => {
    formik.setValues(data);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [data]);

  return loading ? (
    <Paper
      style={{
        minWidth: 406,
        minHeight: 600,
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        margin: '0 30px 24px 0',
      }}
    >
      <LoadingIcon />
    </Paper>
  ) : (
    <form onSubmit={formik.handleSubmit} autoComplete="off">
      <Paper style={{ minWidth: 406, margin: '0 30px 24px 0', borderRadius: 0 }}>
        <Typography variant="subtitle2" style={{ padding: '16px 8px' }}>
          <FormattedMessage id="admin.corporate.companyInfo" />
        </Typography>
        <Divider />
        <Col style={{ padding: '12px 16px' }}>
          <Row style={{ alignItems: 'flex-start', marginBottom: 8 }}>
            <Typography variant="body2" style={{ flex: 1, paddingRight: 8, marginTop: 12 }}>
              <FormattedMessage id="companyName" />:
            </Typography>
            <FormControlTextField
              id="name"
              formControlStyle={{ width: 232, marginRight: 0 }}
              placeholder={intl.formatMessage({ id: 'company.enterCompanyName' })}
              value={formik.values.name}
              onChange={formik.handleChange}
              inputProps={{
                maxLength: 50,
              }}
              errorMessage={
                formik.errors.name && formik.submitCount > 0 ? formik.errors.name : undefined
              }
            />
          </Row>
          <Row style={{ alignItems: 'flex-start', marginBottom: 8 }}>
            <Typography variant="body2" style={{ flex: 1, paddingRight: 8, marginTop: 12 }}>
              <FormattedMessage id="taxCode" />:
            </Typography>
            <FormControlTextField
              id="taxCode"
              formControlStyle={{ width: 232, marginRight: 0 }}
              placeholder={intl.formatMessage({ id: 'enterTaxCode' })}
              value={formik.values.taxCode}
              onChange={formik.handleChange}
              inputProps={{
                maxLength: 50,
              }}
              errorMessage={
                formik.errors.taxCode && formik.submitCount > 0 ? formik.errors.taxCode : undefined
              }
            />
          </Row>
          <Row style={{ alignItems: 'flex-start', marginBottom: 8 }}>
            <Typography variant="body2" style={{ flex: 1, paddingRight: 8, marginTop: 12 }}>
              <FormattedMessage id="address" />:
            </Typography>
            <FormControlTextField
              id="address"
              formControlStyle={{ width: 232, marginRight: 0 }}
              placeholder={intl.formatMessage({ id: 'enterAddress' })}
              value={formik.values.address}
              onChange={formik.handleChange}
              inputProps={{
                maxLength: 50,
              }}
              errorMessage={
                formik.errors.address && formik.submitCount > 0 ? formik.errors.address : undefined
              }
            />
          </Row>
          <Row style={{ alignItems: 'flex-start', marginBottom: 8 }}>
            <Typography variant="body2" style={{ flex: 1, paddingRight: 8, marginTop: 12 }}>
              <FormattedMessage id="companySize" />:
            </Typography>
            <FormControlAutoComplete
              value={formik.values.companySizeRange}
              formControlStyle={{ width: 232, marginRight: 0 }}
              placeholder={intl.formatMessage({ id: 'chooseCompanySize' })}
              onChange={(e: some, value: some | null) => {
                formik.setFieldValue('companySizeRange', value);
              }}
              getOptionLabel={value =>
                intl.formatMessage(
                  {
                    id: 'companySize.value',
                  },
                  {
                    from: value.from,
                    to: value.to,
                  },
                )
              }
              getOptionSelected={(option: some, value: some) => {
                return option.from === value.from;
              }}
              options={generalCompanySize}
              errorMessage={
                formik.errors.companySizeRange && formik.submitCount > 0
                  ? formik.errors.companySizeRange
                  : undefined
              }
            />
          </Row>
          <Divider />
          <Typography variant="subtitle2" style={{ padding: '16px 0' }}>
            <FormattedMessage id="admin.corporate.contractTime" />
          </Typography>
          <Row>
            <Typography variant="body2" style={{ flex: 1, paddingRight: 8, marginBottom: 20 }}>
              <FormattedMessage id="admin.corporate.contractTime" />:
            </Typography>
            <DateRangeFormControl
              style={{ width: 233, marginRight: 0 }}
              optional
              startDate={
                formik.values.contractStartDate
                  ? moment(formik.values.contractStartDate)
                  : undefined
              }
              endDate={
                formik.values.contractEndDate ? moment(formik.values.contractEndDate) : undefined
              }
              onChange={(start?: Moment, end?: Moment) => {
                formik.setFieldValue('contractStartDate', start ? start.valueOf() : undefined);
                start && formik.setFieldValue('contractEndDate', end ? end.valueOf() : undefined);
              }}
              isOutsideRange={(e: any) => false}
              numberOfMonths={1}
            />
          </Row>
          <Divider />
          <Row style={{ alignItems: 'flex-start', marginBottom: 8, marginTop: 16 }}>
            <Typography variant="body2" style={{ flex: 1, paddingRight: 8, marginTop: 12 }}>
              <FormattedMessage id="company.contactName" />:
            </Typography>
            <FormControlTextField
              id="representative"
              formControlStyle={{ width: 232, marginRight: 0 }}
              placeholder={intl.formatMessage({ id: 'company.enterContactName' })}
              value={formik.values.representative}
              onChange={formik.handleChange}
              inputProps={{
                maxLength: 50,
              }}
              errorMessage={
                formik.errors.representative && formik.submitCount > 0
                  ? formik.errors.representative
                  : undefined
              }
            />
          </Row>
          <Row style={{ alignItems: 'flex-start', marginBottom: 8 }}>
            <Typography variant="body2" style={{ flex: 1, paddingRight: 8, marginTop: 12 }}>
              <FormattedMessage id="phoneNumber" />:
            </Typography>
            <FormControlTextField
              id="referrerPhone"
              formControlStyle={{ width: 232, marginRight: 0 }}
              placeholder={intl.formatMessage({ id: 'enterPhoneNumber' })}
              value={formik.values.referrerPhone}
              onChange={e =>
                validNumberRegex.test(e.target.value) &&
                formik.setFieldValue('referrerPhone', e.target.value)
              }
              inputProps={{
                maxLength: 50,
              }}
              inputComponent={NumberFormatCustom2 as any}
              errorMessage={
                formik.errors.referrerPhone && formik.submitCount > 0
                  ? formik.errors.referrerPhone
                  : undefined
              }
            />
          </Row>
          <Row style={{ marginBottom: 16, flexWrap: 'wrap' }}>
            <Typography variant="body2" style={{ flex: 1, paddingRight: 8 }}>
              <FormattedMessage id="email" />:
            </Typography>
            {data.licenseAccount?.email && <EmailLink value={data.licenseAccount?.email} />}
          </Row>

          <Row style={{ alignItems: 'flex-start', marginBottom: 16 }}>
            <Typography variant="body2" style={{ flex: 1, paddingRight: 8 }}>
              <FormattedMessage id="admin.corporate.creditAmount" />:
            </Typography>
            <Typography variant="body2">
              <FormattedNumber value={data.creditAmount || 0} />
              &nbsp;
              {data.currencyCode}
            </Typography>
          </Row>
          <Row style={{ alignItems: 'flex-start', marginBottom: 8 }}>
            <Typography variant="body2" style={{ flex: 1, paddingRight: 8 }}>
              <FormattedMessage id="admin.corporate.accountStatus" />:
            </Typography>
            <Typography
              variant="body2"
              style={{
                display: 'block',
                textAlign: 'center',
                color: data.isActive ? GREEN : GREY,
              }}
            >
              <FormattedMessage id={data.isActive ? 'global.active' : 'global.inactive'} />
            </Typography>
          </Row>
          <Row style={{ justifyContent: 'flex-end', padding: '8px 0' }}>
            <LoadingButton
              loading={loading}
              type="submit"
              variant="contained"
              style={{ width: 144, marginRight: 0 }}
              color="secondary"
              size="large"
              disableElevation
            >
              <FormattedMessage id="save" />
            </LoadingButton>
          </Row>
        </Col>
      </Paper>
    </form>
  );
};

export default CompanyInfoBox;
