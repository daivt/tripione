import { useSnackbar } from 'notistack';
import queryString from 'query-string';
import * as React from 'react';
import { useDispatch } from 'react-redux';
import { RouteComponentProps, useLocation } from 'react-router';
import { Action } from 'redux';
import { ThunkDispatch } from 'redux-thunk';
import { API_PATHS } from '../../../../../configs/API';
import { some, SUCCESS_CODE } from '../../../../../constants';
import { defaultPaginationFilter, PaginationFilter } from '../../../../../models/pagination';
import { AppState } from '../../../../../redux/reducers';
import { snackbarSetting } from '../../../../common/components/elements';
import RedirectDiv from '../../../../common/components/RedirectDiv';
import { goToReplace } from '../../../../common/redux/reducer';
import { fetchThunk } from '../../../../common/redux/thunk';
import { CorporateCompanyInfo, defaultCompanyInfo, IChangeAccount } from '../../utils';
import AdminCorporateDetailDesktop from '../components/AdminCorporateDetailDesktop';

interface Props extends RouteComponentProps<{ id: string }> {}

const AdminCorporateDetail = (props: Props) => {
  const dispatch = useDispatch<ThunkDispatch<AppState, null, Action<string>>>();
  const { match } = props;
  const { id } = match.params;
  const [historyData, setHistoryData] = React.useState<some | undefined>();
  const [loading, setLoading] = React.useState(false);
  const [historyLoading, setHistoryLoading] = React.useState(false);
  const [pagination, setPagination] = React.useState<PaginationFilter>(defaultPaginationFilter);

  const [data, setData] = React.useState<CorporateCompanyInfo>(defaultCompanyInfo);
  const [note, setNote] = React.useState<string>('');
  const [changeAccountOpen, setChangeAccountOpen] = React.useState(false);
  const [modalOpen, setModalOpen] = React.useState(false);

  const { enqueueSnackbar, closeSnackbar } = useSnackbar();
  const location = useLocation();

  const updateQueryParams = React.useCallback(() => {
    if (location.search) {
      const filterParams = queryString.parse(location.search) as any;
      const paginationTmp = {
        page: filterParams.page ? parseInt(`${filterParams.page}`, 10) : 1,
        pageSize: filterParams.pageSize ? parseInt(`${filterParams.pageSize}`, 10) : 10,
      } as PaginationFilter;
      setPagination(paginationTmp);
    } else {
      dispatch(
        goToReplace({
          search: queryString.stringify({
            ...defaultPaginationFilter,
          }),
        }),
      );
    }
  }, [dispatch, location.search]);

  const fetchDetail = React.useCallback(
    async (companyId: string) => {
      if (!Number.isInteger(Number(id))) {
        return;
      }
      setLoading(true);
      const json = await dispatch(fetchThunk(API_PATHS.corporateDetail(Number(companyId)), 'get'));
      if (json?.code === SUCCESS_CODE) {
        setData(json.data);
      } else {
        enqueueSnackbar(
          json.message,
          snackbarSetting(key => closeSnackbar(key), {
            color: 'error',
          }),
        );
      }
      setLoading(false);
    },
    [closeSnackbar, dispatch, enqueueSnackbar, id],
  );

  const fetchHistoryData = React.useCallback(
    async (paginationParams: PaginationFilter) => {
      setHistoryLoading(true);
      const json = await dispatch(
        fetchThunk(
          API_PATHS.getApprovalHistory,
          'post',
          JSON.stringify({
            filters: {
              companyId: id,
              events: [3, 4, 5, 6],
            },
            ...paginationParams,
          }),
        ),
      );
      if (json?.code === SUCCESS_CODE) {
        setHistoryData(json.data);
      } else {
        enqueueSnackbar(
          json.message,
          snackbarSetting(key => closeSnackbar(key), {
            color: 'error',
          }),
        );
      }
      setHistoryLoading(false);
    },
    [closeSnackbar, dispatch, enqueueSnackbar, id],
  );

  const lockAccount = React.useCallback(
    async (companyId: string) => {
      const json = await dispatch(
        fetchThunk(
          API_PATHS.deactivateCorporate,
          'put',
          JSON.stringify({ id: Number(companyId), note }),
        ),
      );
      if (json?.code === SUCCESS_CODE) {
        setData(one => {
          return {
            ...one,
            isActive: !one.isActive,
          };
        });
        enqueueSnackbar(
          json.message,
          snackbarSetting(key => closeSnackbar(key)),
        );
      } else {
        enqueueSnackbar(
          json.message,
          snackbarSetting(key => closeSnackbar(key), {
            color: 'error',
          }),
        );
      }
    },
    [closeSnackbar, dispatch, enqueueSnackbar, note],
  );

  const unlockAccount = React.useCallback(
    async (companyId: string) => {
      const json = await dispatch(
        fetchThunk(
          API_PATHS.activateCorporate,
          'put',
          JSON.stringify({ id: Number(companyId), note }),
        ),
      );
      if (json?.code === SUCCESS_CODE) {
        setData(one => {
          return {
            ...one,
            isActive: !one.isActive,
          };
        });
        enqueueSnackbar(
          json.message,
          snackbarSetting(key => closeSnackbar(key)),
        );
      } else {
        enqueueSnackbar(
          json.message,
          snackbarSetting(key => closeSnackbar(key), {
            color: 'error',
          }),
        );
      }
    },
    [closeSnackbar, dispatch, enqueueSnackbar, note],
  );

  const updateInfo = React.useCallback(
    async (values: CorporateCompanyInfo) => {
      setLoading(true);
      const json = await dispatch(
        fetchThunk(API_PATHS.updateCorporate, 'put', JSON.stringify(values)),
      );
      if (json?.code === SUCCESS_CODE) {
        enqueueSnackbar(
          json.message,
          snackbarSetting(key => closeSnackbar(key), {}),
        );
      } else {
        enqueueSnackbar(
          json.message,
          snackbarSetting(key => closeSnackbar(key), {
            color: 'error',
          }),
        );
      }
      setLoading(false);
    },
    [closeSnackbar, dispatch, enqueueSnackbar],
  );

  const updateCorporateAccount = React.useCallback(
    async (values: IChangeAccount) => {
      setLoading(true);
      const json = await dispatch(
        fetchThunk(
          API_PATHS.updateLicenseCorporateAccount,
          'put',
          JSON.stringify({
            companyId: Number(id),
            userId: values?.user?.id,
            note: values?.note,
          }),
        ),
      );
      if (json?.code === SUCCESS_CODE) {
        fetchDetail(id);
        enqueueSnackbar(
          json.message,
          snackbarSetting(key => closeSnackbar(key), {}),
        );
        setChangeAccountOpen(false);
      } else {
        enqueueSnackbar(
          json.message,
          snackbarSetting(key => closeSnackbar(key), {
            color: 'error',
          }),
        );
      }
      setLoading(false);
    },
    [closeSnackbar, dispatch, enqueueSnackbar, fetchDetail, id],
  );

  React.useEffect(() => {
    updateQueryParams();
  }, [updateQueryParams]);

  React.useEffect(() => {
    fetchDetail(id);
  }, [fetchDetail, id]);

  React.useEffect(() => {
    fetchHistoryData(pagination);
  }, [fetchHistoryData, pagination, data]);

  if (!Number.isInteger(Number(id))) {
    return <RedirectDiv />;
  }

  return (
    <AdminCorporateDetailDesktop
      id={id}
      data={data}
      loading={loading}
      updateInfo={updateInfo}
      onUpdatePagination={value => setPagination(value)}
      onChangeAccount={updateCorporateAccount}
      lockAccount={lockAccount}
      unlockAccount={unlockAccount}
      historyData={historyData}
      historyLoading={historyLoading}
      historyPagination={pagination}
      modalOpen={modalOpen}
      setModalOpen={setModalOpen}
      changeAccountOpen={changeAccountOpen}
      setChangeAccountOpen={setChangeAccountOpen}
      note={note}
      setNote={setNote}
    />
  );
};

export default AdminCorporateDetail;
