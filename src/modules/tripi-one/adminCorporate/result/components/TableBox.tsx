import { Button, Typography } from '@material-ui/core';
import moment from 'moment';
import * as React from 'react';
import { FormattedMessage, FormattedNumber } from 'react-intl';
import { useDispatch } from 'react-redux';
import { ThunkDispatch } from 'redux-thunk';
import { Action } from 'redux';
import { GREEN } from '../../../../../configs/colors';
import { ROUTES } from '../../../../../configs/routes';
import { some } from '../../../../../constants';
import { DATE_FORMAT } from '../../../../../models/moment';
import { PaginationFilter } from '../../../../../models/pagination';
import { statusBaseOptions } from '../../../../../models/status';
import { AppState } from '../../../../../redux/reducers';
import { ReactComponent as IconEdit } from '../../../../../svg/ic_edit.svg';
import { Row } from '../../../../common/components/elements';
import TableCustom, { Columns } from '../../../../common/components/TableCustom';
import { goToAction } from '../../../../common/redux/reducer';
import { IAdminCorporateFilter } from '../../utils';
import Filter from './Filter';

interface Props {
  loading: boolean;
  data?: some;
  filter: IAdminCorporateFilter;
  pagination: PaginationFilter;
  onUpdateFilter(filter: IAdminCorporateFilter): void;
  onUpdatePagination(pagination: PaginationFilter): void;
}

const TableBox: React.FunctionComponent<Props> = props => {
  const dispatch = useDispatch<ThunkDispatch<AppState, null, Action<string>>>();
  const { loading, filter, onUpdateFilter, pagination, onUpdatePagination, data } = props;

  const columns = React.useMemo(() => {
    return [
      {
        title: 'companyName',
        style: { minWidth: 120 },
        dataIndex: 'name',
        variant: 'caption',
      },
      {
        title: 'taxCode',
        dataIndex: 'taxCode',
        variant: 'caption',
      },
      {
        title: 'admin.corporate.licensedEmail',
        width: 150,
        render: (record: some, index: number) => (
          <Typography variant="caption">{record.licenseAccount?.email}</Typography>
        ),
      },
      {
        title: 'admin.corporate.contractExpirationDate',
        dataIndex: 'contractExpirationDate',
        render: (record: some, index: number) => (
          <Typography variant="caption">
            {record.contractEndDate && moment(record.contractEndDate).isValid()
              ? moment(record.contractEndDate).format(DATE_FORMAT)
              : null}
          </Typography>
        ),
      },
      {
        title: 'admin.corporate.creditAmount',
        width: 150,
        dataIndex: 'creditAmount',
        render: (record: some, index: number) => (
          <Typography variant="caption">
            <FormattedNumber value={record.creditAmount} />
            &nbsp;
            {record.currencyCode}
          </Typography>
        ),
      },
      {
        title: 'admin.corporate.numberOfUsers',
        styleHeader: { textAlign: 'center' },
        width: 120,
        dataIndex: 'numberOfUsers',
        render: (record: some, index: number) => (
          <Typography variant="caption" style={{ display: 'block', textAlign: 'center' }}>
            {record.numberOfUsers}
          </Typography>
        ),
      },
      {
        title: 'admin.corporate.accountStatus',
        styleHeader: { textAlign: 'center' },
        width: 120,
        dataIndex: 'isActive',
        variant: 'caption',
        render: (record: some, index: number) => (
          <Typography
            variant="caption"
            style={{ display: 'block', textAlign: 'center', color: record.isActive && GREEN }}
          >
            <FormattedMessage
              id={statusBaseOptions.find(one => one.id === record?.isActive)?.name}
            />
          </Typography>
        ),
      },
      {
        title: '',
        dataIndex: '',
        variant: 'caption',
        render: (record: some, index: number) => (
          <Button
            variant="text"
            color="secondary"
            disableElevation
            style={{ padding: '7px 22px' }}
            onClick={() => {
              dispatch(
                goToAction({
                  pathname: ROUTES.adminCorporate.detail.gen(record.id),
                }),
              );
            }}
          >
            <Typography variant="caption">
              <IconEdit />
            </Typography>
          </Button>
        ),
      },
    ] as Columns[];
  }, [dispatch]);

  return (
    <>
      <Filter loading={loading} filter={filter} onUpdateFilter={onUpdateFilter} />
      <TableCustom
        dataSource={data?.itemList}
        loading={loading}
        columns={columns}
        style={{ marginTop: 24 }}
        header={
          <Row style={{ padding: '16px 12px' }}>
            <Typography variant="subtitle2" style={{ fontWeight: 500, marginRight: '24px' }}>
              <FormattedMessage id="admin.corporate.list" />
            </Typography>
          </Row>
        }
        paginationProps={{
          count: data?.totalResults || 0,
          page: pagination.page || 1,
          rowsPerPage: pagination?.pageSize || 0,
          onChangePage: (event: unknown, newPage: number) => {
            onUpdatePagination({ ...pagination, page: newPage });
          },
          onChangeRowsPerPage: (event: React.ChangeEvent<HTMLInputElement>) => {
            onUpdatePagination({
              pageSize: parseInt(event.target.value, 10),
              page: 1,
            });
          },
        }}
      />
    </>
  );
};

export default TableBox;
