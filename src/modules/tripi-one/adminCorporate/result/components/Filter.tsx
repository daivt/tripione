import { Button } from '@material-ui/core';
import { useFormik } from 'formik';
import React from 'react';
import { FormattedMessage, useIntl } from 'react-intl';
import moment, { Moment } from 'moment';
import { statusBaseOptions } from '../../../../../models/status';
import { ReactComponent as IconRefresh } from '../../../../../svg/ic_refresh.svg';
import { Row } from '../../../../common/components/elements';
import FormControlTextField from '../../../../common/components/FormControlTextField';
import LoadingButton from '../../../../common/components/LoadingButton';
import SingleSelect from '../../../../common/components/SingleSelect';
import { IAdminCorporateFilter, defaultAdminCorporateFilter } from '../../utils';
import DateRangeFormControl from '../../../../common/components/DateRangeFormControl';
import { trimObjectValues } from '../../../../utils';

interface Props {
  filter: IAdminCorporateFilter;
  onUpdateFilter(filter: IAdminCorporateFilter): void;
  loading?: boolean;
}

const Filter: React.FunctionComponent<Props> = (props: Props) => {
  const { filter, onUpdateFilter, loading } = props;
  const intl = useIntl();

  const formik = useFormik({
    initialValues: filter,
    onSubmit: values => onUpdateFilter(trimObjectValues(values)),
  });

  React.useEffect(() => {
    formik.setValues(filter);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [filter]);
  return (
    <form onSubmit={formik.handleSubmit} autoComplete="off">
      <Row style={{ flexWrap: 'wrap' }}>
        <FormControlTextField
          id="name"
          label={<FormattedMessage id="companyName" />}
          placeholder={intl.formatMessage({ id: 'enterCompanyName' })}
          value={formik.values.name}
          formControlStyle={{ width: 220 }}
          onChange={formik.handleChange}
          optional
          inputProps={{
            maxLength: '100',
          }}
        />
        <FormControlTextField
          id="taxCode"
          label={<FormattedMessage id="taxCode" />}
          placeholder={intl.formatMessage({ id: 'taxCode' })}
          value={formik.values.taxCode}
          formControlStyle={{ width: 220 }}
          onChange={formik.handleChange}
          optional
          inputProps={{
            maxLength: '100',
          }}
        />
        <FormControlTextField
          id="email"
          label={<FormattedMessage id="email" />}
          placeholder={intl.formatMessage({ id: 'enterEmail' })}
          value={formik.values.email}
          formControlStyle={{ width: 220 }}
          onChange={formik.handleChange}
          optional
          inputProps={{
            maxLength: '100',
          }}
        />
        <SingleSelect
          value={formik.values.isActive}
          label={<FormattedMessage id="admin.corporate.accountStatus" />}
          formControlStyle={{ width: 220 }}
          onSelectOption={(value: any) => {
            formik.setFieldValue('isActive', value);
          }}
          getOptionLabel={value => intl.formatMessage({ id: value.name })}
          options={[{ id: undefined, name: 'all' }, ...statusBaseOptions]}
          optional
        />
        <DateRangeFormControl
          style={{ width: 220 }}
          label={intl.formatMessage({ id: 'admin.corporate.contractExpirationDateRange' })}
          optional
          startDate={formik.values.fromDate ? moment(formik.values.fromDate) : undefined}
          endDate={formik.values.toDate ? moment(formik.values.toDate) : undefined}
          onChange={(start?: Moment, end?: Moment) => {
            formik.setFieldValue('fromDate', start ? start.valueOf() : undefined);
            start && formik.setFieldValue('toDate', end ? end.valueOf() : undefined);
          }}
          numberOfMonths={1}
          isOutsideRange={(e: any) => false}
        />
        <Row style={{ marginTop: 4 }}>
          <LoadingButton
            type="submit"
            loading={loading}
            variant="contained"
            style={{ minWidth: 160, marginRight: 16 }}
            color="secondary"
            size="large"
            disableElevation
          >
            <FormattedMessage id="search" />
          </LoadingButton>
          <Button
            size="large"
            style={{ minWidth: 0 }}
            onClick={() => {
              formik.setValues(defaultAdminCorporateFilter);
              onUpdateFilter(defaultAdminCorporateFilter);
            }}
          >
            <IconRefresh />
          </Button>
        </Row>
      </Row>
    </form>
  );
};

export default Filter;
