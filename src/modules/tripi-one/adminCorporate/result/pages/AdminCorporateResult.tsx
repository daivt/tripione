import { debounce } from 'lodash';
import queryString from 'query-string';
import React, { useCallback } from 'react';
import { useDispatch } from 'react-redux';
import { useLocation } from 'react-router';
import { Action } from 'redux';
import { ThunkDispatch } from 'redux-thunk';
import { API_PATHS } from '../../../../../configs/API';
import { some, SUCCESS_CODE } from '../../../../../constants';
import { defaultPaginationFilter, PaginationFilter } from '../../../../../models/pagination';
import { AppState } from '../../../../../redux/reducers';
import { goToReplace } from '../../../../common/redux/reducer';
import { fetchThunk } from '../../../../common/redux/thunk';
import { defaultAdminCorporateFilter, IAdminCorporateFilter } from '../../utils';
import TableBox from '../components/TableBox';

interface Props {}

const AdminCorporate: React.FunctionComponent<Props> = props => {
  const dispatch = useDispatch<ThunkDispatch<AppState, null, Action<string>>>();
  const [data, setData] = React.useState<some | undefined>();
  const [loading, setLoading] = React.useState(true);

  const [filter, setFilter] = React.useState<IAdminCorporateFilter>(defaultAdminCorporateFilter);
  const [pagination, setPagination] = React.useState<PaginationFilter>(defaultPaginationFilter);

  const location = useLocation();
  const updateQueryParams = React.useCallback(() => {
    if (location.search) {
      const filterParams = queryString.parse(location.search) as any;
      const filterTmp = queryString.parse(filterParams.filters) as any;
      setFilter({
        ...filterTmp,
        contractStartDate: Number(filterTmp.fromDate),
        contractEndDate: Number(filterTmp.toDate),
        isActive: filterTmp.isActive ? filterTmp.isActive === 'true' : undefined,
      });
      const paginationTmp = {
        page: filterParams.page ? parseInt(`${filterParams.page}`, 10) : 1,
        pageSize: filterParams.pageSize ? parseInt(`${filterParams.pageSize}`, 10) : 10,
      } as PaginationFilter;
      setPagination(paginationTmp);
    } else {
      dispatch(
        goToReplace({
          search: queryString.stringify({
            ...defaultPaginationFilter,
            filters: queryString.stringify({}),
          }),
        }),
      );
    }
  }, [dispatch, location.search]);

  const fetchData = useCallback(
    debounce(
      async (filterParams: IAdminCorporateFilter, paginationParams: PaginationFilter) => {
        setLoading(true);
        const json = await dispatch(
          fetchThunk(
            API_PATHS.corporateList,
            'post',
            JSON.stringify({
              ...paginationParams,
              filters: filterParams,
            }),
          ),
        );
        if (json?.code === SUCCESS_CODE) {
          setData(json.data);
        }
        setLoading(false);
      },
      200,
      {
        trailing: true,
        leading: false,
      },
    ),
    [],
  );

  React.useEffect(() => {
    updateQueryParams();
  }, [updateQueryParams]);

  React.useEffect(() => {
    fetchData(filter, pagination);
  }, [fetchData, filter, pagination]);

  return (
    <>
      <TableBox
        data={data}
        loading={loading}
        filter={filter}
        onUpdateFilter={values => {
          dispatch(
            goToReplace({
              search: queryString.stringify({
                ...pagination,
                page: 1,
                filters: queryString.stringify(values),
              }),
            }),
          );
        }}
        pagination={pagination}
        onUpdatePagination={values => {
          dispatch(
            goToReplace({
              search: queryString.stringify({ ...values, filters: queryString.stringify(filter) }),
            }),
          );
        }}
      />
    </>
  );
};

export default AdminCorporate;
