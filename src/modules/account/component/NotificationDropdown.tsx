import { Button, Collapse, IconButton, Paper, Typography } from '@material-ui/core';
import React from 'react';
import InfiniteScroll from 'react-infinite-scroller';
import { FormattedMessage } from 'react-intl';
import PerfectScrollbar from 'react-perfect-scrollbar';
import { shallowEqual, useDispatch, useSelector } from 'react-redux';
import { useHistory } from 'react-router';
import { ThunkDispatch } from 'redux-thunk';
import { Action } from 'typesafe-actions';
import { BLUE, GREY_100, GREY_300, GREY_400, SECONDARY } from '../../../configs/colors';
import { some } from '../../../constants';
import { HEADER_HEIGHT } from '../../../layout/constants';
import { AppState } from '../../../redux/reducers';
import { ReactComponent as IconBellBlack } from '../../../svg/ic_bell_black.svg';
import { Row } from '../../common/components/elements';
import LoadingIcon from '../../common/components/LoadingIcon';
import { fetchNotification, markAllRead, markRead, resetUnread } from '../redux/accountReducer';
import NotificationCard from './NotificationCard';
import NotificationDialog from './NotificationDialog';


interface Props {}

const NotificationDropdown: React.FunctionComponent<Props> = props => {
  const dispatch = useDispatch<ThunkDispatch<AppState, null, Action<string>>>();
  const history = useHistory();

  const {notification, unreadCount} = useSelector(
    (state: AppState) => state.account,
    shallowEqual,
    );

  const [isFocus, setFocus] = React.useState(false);
  const [detail, setDetail] = React.useState<some | undefined>(undefined);
  const [loading, setLoading] = React.useState(false);

  const onBlur = React.useCallback(
    (e: React.FocusEvent<HTMLDivElement>) => {
      if (e.relatedTarget instanceof Element) {
        if (e.currentTarget.contains(e.relatedTarget as Element)) {
          return;
        }
      }
      !detail && setFocus(false);
    },
    [detail],
  );

  return (
    <div
      tabIndex={-1}
      onBlur={onBlur}
      style={{
        outline: 'none',
      }}
    >
      <IconButton
        style={{
          width: 40,
          height: 40,
          padding: 0,
          display: 'flex',
          justifyContent: 'center',
          alignItems: 'center',
          marginRight: '10px',
          background: GREY_100,
          border: `1px solid ${GREY_400}`,
        }}
        onClick={() => {
          (unreadCount > 0 && isFocus) && dispatch(resetUnread())
          setFocus(!isFocus);
        }}
      >
        <IconBellBlack />
        {unreadCount > 0 && (
          <span
            style={{
              position: 'absolute',
              top: -2,
              right: -4,
              background: SECONDARY,
              fontSize: '10px',
              color: 'white',
              width: '18px',
              height: '18px',
              display: 'flex',
              justifyContent: 'center',
              alignItems: 'center',
              borderRadius: '50%',
            }}
          >
            {unreadCount > 50 ? '50+' : unreadCount}
          </span>
        )}
      </IconButton>
      {notification.items && (
        <Collapse
          in={isFocus}
          unmountOnExit
          style={{
            position: 'absolute',
            width: '424px',
            zIndex: 110,
            top: HEADER_HEIGHT,
            right: 0,
          }}
        >
          <Paper
            style={{
              display: 'flex',
              flexDirection: 'column',
              height: 'calc(100vh - 250px)',
            }}
            variant="outlined"
          >
            <Row
              style={{
                display: 'flex',
                alignItems: 'center',
                padding: '16px 18px',
                justifyContent: 'space-between',
                borderBottom: `1px solid ${GREY_300}`,
              }}
            >
              <Typography variant="h6">
                <FormattedMessage id="notification" />
              </Typography>
              <Button variant="text" style={{ padding: '4px' }} onClick={() => dispatch(markAllRead())}>
                <Typography variant="body2" style={{ color: BLUE }}>
                  <FormattedMessage id="markReadAll" values={{ num: unreadCount }} />
                </Typography>
              </Button>
            </Row>
            <PerfectScrollbar options={{ wheelPropagation: false }} style={{ flex: 1 }}>
              {notification.items?.length > 0 ? (
                <InfiniteScroll
                  pageStart={0}
                  initialLoad={false}
                  loadMore={page => dispatch(fetchNotification(page))}
                  hasMore={notification.items?.length < notification.totalCount && !loading}
                  loader={
                    <LoadingIcon
                      key="loader"
                      style={{
                        display: 'flex',
                        alignItems: 'center',
                        justifyContent: 'center',
                      }}
                    />
                  }
                  useWindow={false}
                >
                  <div>
                    {notification.items?.map((v: some, index: number) => (
                      <NotificationCard
                        key={v.id}
                        info={v}
                        onUnread={() => dispatch(markRead(v.id))}
                        onClick={() => {
                          // setDetail(fakeNotificationDialog);
                        }}
                      />
                    ))}
                  </div>
                </InfiniteScroll>
              ) : (
                <div
                  style={{
                    display: 'flex',
                    justifyContent: 'center',
                    alignItems: 'center',
                    height: '100%',
                  }}
                />
              )}
            </PerfectScrollbar>
          </Paper>
        </Collapse>
      )}
      <NotificationDialog data={detail} onClose={() => setDetail(undefined)} />
    </div>
  );
};

export default NotificationDropdown;
