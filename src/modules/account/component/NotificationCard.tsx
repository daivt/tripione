import { Typography } from '@material-ui/core';
import moment from 'moment';
import * as React from 'react';
import { FormattedMessage } from 'react-intl';
import styled from 'styled-components';
import { GREY_100, GREY_300 } from '../../../configs/colors';
import { some } from '../../../constants';
import { ReactComponent as IconEmail } from '../../../svg/email.svg';
import { ReactComponent as IconEmailOpen } from '../../../svg/email_open.svg';

const RawLink = styled.a`
  text-decoration: none;
  color: unset;
`;

const Line = styled.div`
  display: flex;
  align-items: center;
  cursor: 'pointer';
  &:hover {
    background: ${GREY_100};
  }
  cursor: pointer;
`;

interface Props {
  info: some;
  onClick: () => void;
  onUnread: (id: number) => void;
}

const NotificationCard: React.FunctionComponent<Props> = props => {
  const { info, onUnread } = props;
  const [data, setData] = React.useState(info);

  const lastExecuteTime = moment(data.lastExecuteTime, 'HH:mm DD/MM/YYYY');
  const isToday = lastExecuteTime.isSame(moment(), 'day');
  const isYesterday = lastExecuteTime.isSame(moment().subtract(1, 'days'), 'day');

  const dateTime = React.useMemo(() => {
    if (isToday) {
      return <FormattedMessage id="today" />;
    }
    if (isYesterday) {
      return <FormattedMessage id="yesterday" />;
    }
    return data.lastExecuteTime;
  }, [data.lastExecuteTime, isToday, isYesterday]);

  const readStatus = React.useMemo(() => {
    return data.opened;
  }, [data.opened]);

  const content = React.useCallback(() => {
    return (
      <Line
        onClick={() => {
          if (!readStatus) {
            onUnread(data.id);
            setData(one => {
              return { ...one, opened: true };
            });
          }
        }}
      >
        <div
          style={{
            width: '48px',
            paddingTop: '9px',
            height: '100%',
            alignSelf: 'start',
            display: 'flex',
            justifyContent: 'center',
          }}
        >
          {readStatus ? <IconEmailOpen /> : <IconEmail />}
        </div>
        <div
          style={{
            flex: 1,
            borderBottom: `1px solid ${GREY_300}`,
            padding: '9px 16px 12px 0px',
            minHeight: '72px',
            overflow: 'hidden',
          }}
        >
          <Line
            style={{
              justifyContent: 'space-between',
            }}
          >
            <Typography
              variant="subtitle2"
              color={readStatus ? 'textSecondary' : 'textPrimary'}
              style={{
                flex: 1,
                textOverflow: 'ellipsis',
                whiteSpace: 'nowrap',
                maxWidth: '650px',
                overflow: 'hidden',
              }}
            >
              {data.content?.content}
            </Typography>
            <Typography variant="caption" color={readStatus ? 'textSecondary' : 'textPrimary'}>
              {dateTime}
            </Typography>
          </Line>
          <Typography
            variant="caption"
            color={readStatus ? 'textSecondary' : 'textPrimary'}
            style={{
              textOverflow: 'ellipsis',
              overflow: 'hidden',
            }}
          >
            {data.content?.createdBy}
            <br />
            {data.createdTime}
          </Typography>
        </div>
      </Line>
    );
  }, [data.content, data.createdTime, data.id, dateTime, onUnread, readStatus]);

  React.useEffect(() => {
    setData(info);
  }, [info]);

  return <>{data.content?.url ? <RawLink href={data.content?.url}>{content()}</RawLink> : content()}</>;
};

export default NotificationCard;
