import * as React from 'react';
import { useDispatch } from 'react-redux';
import HelpOutlineIcon from '@material-ui/icons/HelpOutline';
import { IconButton } from '@material-ui/core';
import LanguageSelect from '../../intl/components/LanguageSelect';
import UserInfoDropdown from './UserInfoDropdown';
import NotificationDropdown from './NotificationDropdown';
import { Row } from '../../common/components/elements';
import { openGuide } from '../../auth/redux/authReducer';

interface Props {}

const Badge: React.FunctionComponent<Props> = props => {
  const dispatch = useDispatch();
  return (
    <div
      style={{
        alignItems: 'center',
        display: 'flex',
        justifyContent: 'flex-end',
        flexShrink: 0,
        flexGrow: 1,
        position: 'relative',
      }}
    >
      <Row data-tour="step-6">
        <IconButton style={{ marginRight: 16 }} onClick={() => dispatch(openGuide(true))}>
          <HelpOutlineIcon />
        </IconButton>
        <LanguageSelect />
        <NotificationDropdown />
        <UserInfoDropdown />
      </Row>
    </div>
  );
};

export default Badge;
