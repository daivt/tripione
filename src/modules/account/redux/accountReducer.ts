import { Action } from 'redux';
import { ThunkAction } from 'redux-thunk';
import { ActionType, createAction, getType } from 'typesafe-actions';
import { API_PATHS } from "../../../configs/API";
import { some, SUCCESS_CODE } from "../../../constants";
import { fetchThunk } from '../../common/redux/thunk';
import { AppState } from "../../../redux/reducers";


export interface AccountState {
  readonly userData?: some;
  unreadCount: number;
  notification: some;
}

export const setUserData = createAction('account/setUserData', (data?: some) => ({ data }))();
export const setNotification = createAction('account/setNotification', (data: some) => ({ data }))();
export const setUnreadCount = createAction('account/setUnread', (data: number) => ({ data }))();

export const DEFAULT_ACCOUNT_STATE = {
  unreadCount: -1,
  userData: {},
  notification: {}
};

export function fetchNotification(page: number): ThunkAction<Promise<void>, AppState, null, Action<string>> {
  return async (dispatch, getState) => {
    const state = getState();
    const { notification } = state.account;
    const json = await dispatch(fetchThunk(API_PATHS.getNotificationList(page), 'get'));
    if (json?.code === SUCCESS_CODE) {
      dispatch(setNotification({
        ...json.data,
        items: notification.items ? notification.items.concat(json.data.items) : json.data.items
      }));
    }
  };
}

export function resetUnread(): ThunkAction<Promise<void>, AppState, null, Action<string>> {
  return async (dispatch, getState) => {
    const json = await dispatch(fetchThunk(API_PATHS.resetUnread, 'post'));
    if (json?.code === SUCCESS_CODE) {
      dispatch(setUnreadCount(0));
    }
  };
}

export function fetchUnread(): ThunkAction<Promise<void>, AppState, null, Action<string>> {
  return async (dispatch, getState) => {
    const json = await dispatch(fetchThunk(API_PATHS.unreadCount, 'get'));
    if (json?.code === SUCCESS_CODE) {
      dispatch(setUnreadCount(json.data));
    }
  };
}

export function markRead(id: number): ThunkAction<Promise<void>, AppState, null, Action<string>> {
  return async (dispatch, getState) => {
    const state = getState();
    const {account} = state;
    const json = await dispatch(fetchThunk(API_PATHS.markRead, 'post', JSON.stringify({id})));
    if (json?.code === SUCCESS_CODE) {
      dispatch(setNotification({
        ...account.notification,
        items: account.notification.items && account.notification.items.map((item: some) => {
          if (item.id === id) {
            return { ...item, opened: true }
          }
          return item
        })
      }));
    }
  };
}

export function markAllRead(): ThunkAction<Promise<void>, AppState, null, Action<string>> {
  return async (dispatch, getState) => {
    const state = getState();
    const { account } = state;
    const json = await dispatch(fetchThunk(API_PATHS.markAllRead, 'post'));
    if (json?.code === SUCCESS_CODE) {
      dispatch(setNotification({
        ...account.notification,
        items: account.notification.items && account.notification.items.map((item: some) => {
          return { ...item, opened: true }
        })
      }));
    }
  };
}

const actions = {
  setUserData,
  setNotification,
  setUnreadCount
};

type ActionT = ActionType<typeof actions>;

export default function reducer(
  state: AccountState = DEFAULT_ACCOUNT_STATE,
  action: ActionT,
): AccountState {
  switch (action.type) {
    case getType(setUserData):
      return { ...state, userData: action.payload.data };
    case getType(setNotification):
      return { ...state, notification: action.payload.data };
    case getType(setUnreadCount):
      return { ...state, unreadCount: action.payload.data };
    default:
      return state;
  }
}
