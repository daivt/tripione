import 'moment/locale/vi';
import React from 'react';
import { connect, useDispatch } from 'react-redux';
import { Route, Switch } from 'react-router';
import { Action } from 'redux';
import { ThunkDispatch } from 'redux-thunk';
import 'slick-carousel/slick/slick-theme.css';
import 'slick-carousel/slick/slick.css';
import firebase from './configs/firebase';
import { BOOKING_URL, ROUTES } from './configs/routes';
import { validateAccessToken } from './modules/auth/redux/authThunks';
import Terms from './modules/booking/common/Terms';
import AuthProblemDialog from './modules/common/components/AuthProblemDialog';
import LoadingIcon from './modules/common/components/LoadingIcon';
import NetworkProblemDialog from './modules/common/components/NetworkProblemDialog';
import ProtectedRoute from './modules/common/components/ProtectedRoute';
import RedirectRoute from './modules/common/components/RedirectRoute';
import { AppState } from './redux/reducers';
import './scss/slickOverride.scss';
import './scss/svg.scss';
import './scss/tripiOne.scss';

const Login = React.lazy(() => import('./modules/auth/login/pages/Login'));
const Register = React.lazy(() => import('./modules/auth/register/pages/Register'));
const AccountVerify = React.lazy(() => import('./modules/auth/accountVerify/pages/AccountVerify'));

const FirstLogin = React.lazy(() => import('./modules/auth/firstLogin/pages/FirstLogin'));
const ForgotPassword = React.lazy(() =>
  import('./modules/auth/forgotPassword/pages/ForgotPassword'),
);

const ChangePassword = React.lazy(() =>
  import('./modules/auth/changePassword/pages/ChangePassword'),
);
const ResetPassword = React.lazy(() => import('./modules/auth/resetPassword/pages/ResetPassword'));

const DefaultLayoutBooking = React.lazy(() =>
  import('./layout/bookingLayout/DefaultLayoutBooking'),
);

const DefaultLayout = React.lazy(() => import('./layout/defaultLayout/DefaultLayout'));

function mapStateToProps(state: AppState) {
  return {
    auth: state.auth,
  };
}

interface Props extends ReturnType<typeof mapStateToProps> {}

const App: React.FC<Props> = props => {
  const { auth } = props;
  const dispatch = useDispatch<ThunkDispatch<AppState, null, Action<string>>>();
  const messaging = firebase.messaging();
  React.useEffect(() => {
    dispatch(validateAccessToken());
  }, [dispatch]);

  React.useEffect(() => {
    messaging
      .requestPermission()
      .then(async function() {
        const token = await messaging.getToken();
        console.log(token);
      })
      .catch(function(err) {
        console.log('Unable to get permission to notify.', err);
      });
  }, [dispatch, messaging]);

  React.useEffect(() => {
    messaging.onMessage(nextOrObserver => console.log(nextOrObserver));
  }, [messaging]);

  return (
    <>
      <NetworkProblemDialog />
      <AuthProblemDialog />{' '}
      <React.Suspense fallback={<LoadingIcon style={{ marginTop: 240 }} />}>
        <Switch>
          <Route exact path={ROUTES.terms} component={Terms} />
          <RedirectRoute auth={auth.auth} path={ROUTES.forgotPass} component={ForgotPassword} />
          <RedirectRoute auth={auth.auth} path={ROUTES.register} component={Register} />
          <RedirectRoute auth={auth.auth} path={ROUTES.verify} component={AccountVerify} />
          <RedirectRoute auth={auth.auth} path={ROUTES.firstLogin} component={FirstLogin} />
          <RedirectRoute auth={auth.auth} path={ROUTES.changePassword} component={ChangePassword} />
          <RedirectRoute auth={auth.auth} path={ROUTES.resetPassword} component={ResetPassword} />
          <RedirectRoute auth={auth.auth} path={ROUTES.login} component={Login} />
          <ProtectedRoute
            auth={auth.auth}
            strict
            path={BOOKING_URL}
            component={DefaultLayoutBooking}
          />
          <ProtectedRoute auth={auth.auth} path="/" component={DefaultLayout} />
        </Switch>
      </React.Suspense>
    </>
  );
};

export default connect(mapStateToProps)(App);
