export const validNumberRegex = /^[0-9]*$/;
export const validPhoneNumberRegex = /^[0-9+]*$/;
export const specialCharacterRegex = /[\]\\[!@#$%^&*(),.?":{}|<>+_;=~`'\\/-]/;
export const rawAlphabetRegex = /^[a-zA-Z ]*$/;
