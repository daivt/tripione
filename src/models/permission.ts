export type ServiceType =
  | 'generalSetting'
  | 'invoices'
  | 'transactions'
  | 'admin'
  | 'report'
  | 'tripManagement';
export type BookingServices = 'flight' | 'hotel' | 'pay';

export type PermissionType =
  | 'view'
  | 'add'
  | 'edit'
  | 'delete'
  | 'viewDetail'
  | 'updateStore'
  | 'active';

// export interface Role {
//   module: ServiceType;
//   role: PermissionType[];
// }
export interface RoutesTabType {
  name: ServiceType | string; // key of module
  title?: string; // string id of title breadcrumb sidebar helmet
  path?: string; // path link
  directPath?: string; // path link
  module?: string; // path link
  component?: any;
  isModule?: boolean; // is highest page
  hidden?: boolean;
  hiddenMenu?: RoutesTabType[];
  subMenu?: RoutesTabType[];
  backStep?: number;
  exact?: boolean;
  listRole?: string[];
  noStickyHeader?: boolean;
  disableInBreadcrumb?: boolean;
  disableBreadcrumb?: boolean;
}
