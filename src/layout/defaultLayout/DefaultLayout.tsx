import { Container, useMediaQuery } from '@material-ui/core';
import * as React from 'react';
import { connect } from 'react-redux';
import { Redirect, Route, Switch, useLocation } from 'react-router';
import { Action } from 'redux';
import { ThunkDispatch } from 'redux-thunk';
import { GREY_100 } from '../../configs/colors';
import { ROUTES, ROUTES_TAB } from '../../configs/routes';
import { MUI_THEME } from '../../configs/setupTheme';
import { Col, PageWrapper } from '../../modules/common/components/elements';
import LoadingIcon from '../../modules/common/components/LoadingIcon';
import { AppState } from '../../redux/reducers';
import { flatRoutes, getListRoutesActivate } from '../utils';
import DefaultAside from './DefaultAside';
import DefaultBreadcrumbs from './DefaultBreadcrumbs';
import DefaultFooter from './DefaultFooter';
import DefaultHeader from './DefaultHeader';
import { fetchGeneralData } from '../../modules/common/redux/reducer';
import Guide from '../../guide/Guide';
import QuickSetup from '../../modules/tripi-one/quickSetup/pages/QuickSetup';
import { fetchNotification, fetchUnread } from '../../modules/account/redux/accountReducer';

const mapStateToProps = (state: AppState) => {
  return {
    userData: state.account.userData,
  };
};

interface Props extends ReturnType<typeof mapStateToProps> {
  dispatch: ThunkDispatch<AppState, null, Action<string>>;
}

const DefaultLayout: React.FunctionComponent<Props> = props => {
  const { dispatch, userData } = props;
  const location = useLocation();
  const [openAside, setOpenAside] = React.useState(true);
  const matches = useMediaQuery(MUI_THEME.breakpoints.up('md'));

  React.useEffect(() => {
    setOpenAside(matches);
  }, [matches]);

  React.useEffect(() => {
    dispatch(fetchGeneralData());
    dispatch(fetchUnread());
    dispatch(fetchNotification(0));
    // return () => {
    //   document.getElementById('zendesk')?.remove();
    //   const elements = document.getElementsByClassName('zopim');
    //   while (elements?.length > 0) {
    //     elements?.[0].parentNode?.removeChild(elements[0]);
    //   }
    // };
  }, [dispatch]);

  const listRoutes = React.useMemo(() => {
    return getListRoutesActivate(userData?.roleInfo?.permissions, flatRoutes(ROUTES_TAB));
  }, [userData]);

  return (
    <>
      {/* <Helmet
        key={`${moment()}`}
        link={[{ rel: 'shortcut icon', href: '/favicon.ico' }]}
        script={
          userData
            ? [
                {
                  id: 'zendesk',
                  type: 'text/javascript',
                  innerHTML: `window.$zopim || (function (d, s) {
            var z = $zopim = function (c) { z._.push(c) }, $ = z.s =
              d.createElement(s), e = d.getElementsByTagName(s)[0]; z.set = function (o) {
                z.set.
                _.push(o)
              }; z._ = []; z.set._ = []; $.async = !0; $.setAttribute("charset", "utf-8");
            $.src = "https://v2.zopim.com/?65HOiqr6wy3uprSP7044dd24nTOhZSyN"; z.t = +new Date; $.
              type = "text/javascript"; e.parentNode.insertBefore($, e)
          })(document, "script");`,
                },
              ]
            : undefined
        }
      /> */}
      <Guide />

      <PageWrapper style={{ background: GREY_100, flexDirection: 'row' }}>
        <DefaultAside
          open={openAside}
          onClose={() => {
            setOpenAside(!openAside);
          }}
        />
        <Col
          style={{
            flex: 1,
            minHeight: '100vh',
            overflow: 'hidden',
          }}
        >
          <DefaultHeader openAside={openAside} />
          <DefaultBreadcrumbs />
          <Container
            style={{
              paddingTop: 16,
              maxWidth: 'none',
              flex: 1,
              padding: '16px 24px 24px 32px',
              display: 'flex',
              flexDirection: 'column',
            }}
          >
            <React.Suspense fallback={<LoadingIcon style={{ marginTop: 240 }} />}>
              <Switch location={location}>
                {listRoutes.map(
                  (route, index) =>
                    route.component && (
                      <Route
                        key={index}
                        exact={route.exact}
                        path={route.path}
                        component={route.component}
                      />
                    ),
                )}
                <Redirect to={ROUTES.notFound404} />
              </Switch>
            </React.Suspense>
          </Container>
          <QuickSetup />
          <DefaultFooter />
        </Col>
      </PageWrapper>
    </>
  );
};

export default connect(mapStateToProps)(DefaultLayout);
