import { AppBar, Container } from '@material-ui/core';
import * as React from 'react';
import 'react-perfect-scrollbar/dist/css/styles.css';
import { connect } from 'react-redux';
import { ThunkDispatch } from 'redux-thunk';
import { Action } from 'typesafe-actions';
import { GREY_300 } from '../../configs/colors';
import Badge from '../../modules/account/component/Badge';
import { Row } from '../../modules/common/components/elements';
import Link from '../../modules/common/components/Link';
import { AppState } from '../../redux/reducers';
import { ReactComponent as LogoOne } from '../../svg/logo_one.svg';
import { HEADER_HEIGHT } from '../constants';

const mapStateToProps = (state: AppState) => {
  return { router: state.router };
};
interface Props extends ReturnType<typeof mapStateToProps> {
  dispatch: ThunkDispatch<AppState, null, Action<string>>;
  noSticky?: boolean;
  isBookingModule?: boolean;
  openAside?: boolean;
}

const DefaultHeader: React.FunctionComponent<Props> = props => {
  const { noSticky, isBookingModule, openAside } = props;

  return (
    <AppBar
      position={noSticky ? 'relative' : 'sticky'}
      style={{
        height: HEADER_HEIGHT,
        backgroundColor: 'white',
        boxShadow: 'none',
        borderRadius: 0,
        borderBottom: `1px solid ${GREY_300}`,
      }}
    >
      <Container
        style={
          isBookingModule
            ? { display: 'flex', height: '100%' }
            : { display: 'flex', height: '100%', margin: 'unset', maxWidth: 'unset' }
        }
      >
        {!openAside && (
          <Row
            style={{
              transition: 'width 0.3s',
              justifyContent: 'center',
            }}
          >
            <Link to="/">
              <LogoOne />
            </Link>
          </Row>
        )}
        <Row style={{ flex: 1, justifyContent: 'flex-end' }}>
          <Badge />
        </Row>
      </Container>
    </AppBar>
  );
};

export default connect(mapStateToProps)(DefaultHeader);
