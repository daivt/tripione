import { Typography } from '@material-ui/core';
import React from 'react';
import { FormattedHTMLMessage, FormattedMessage } from 'react-intl';
import { Row } from '../../modules/common/components/elements';
import { ReactComponent as IconFacebook } from '../../svg/facebook.svg';
import { ReactComponent as IconGoogle } from '../../svg/google_plus.svg';

interface Props {}

const DefaultFooter: React.FC<Props> = () => {
  return (
    <Row
      style={{
        background: 'white',
        padding: '18px 30px',
      }}
    >
      <Row
        style={{
          flex: 1,
        }}
      >
        <Typography variant="body2" color="textSecondary">
          <FormattedMessage id="footer.license" />
        </Typography>
        <IconFacebook style={{ margin: '0px 8px 0 16px' }} />
        <IconGoogle style={{ margin: '0px 8px' }} />
      </Row>
      <Typography variant="body2" color="textSecondary">
        <FormattedHTMLMessage id="footer.alpha" />
      </Typography>
    </Row>
  );
};

export default DefaultFooter;
