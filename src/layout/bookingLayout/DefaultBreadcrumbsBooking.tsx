import { Breadcrumbs, Typography } from '@material-ui/core';
import NavigateNextIcon from '@material-ui/icons/NavigateNext';
import { go } from 'connected-react-router';
import React from 'react';
import { FormattedMessage, useIntl } from 'react-intl';
import { useDispatch } from 'react-redux';
import { useHistory } from 'react-router';
import { Action } from 'redux';
import { ThunkDispatch } from 'redux-thunk';
import Helmet from 'react-helmet';
import { PRIMARY } from '../../configs/colors';
import { ROUTES_BOOKING } from '../../configs/routes';
import { some } from '../../constants';
import { Row } from '../../modules/common/components/elements';
import Link from '../../modules/common/components/Link';
import { isHasPermission } from '../../modules/common/redux/reducer';
import { AppState } from '../../redux/reducers';
import { getAllRoutesContain, getCurrentRoute, comparePathName } from '../utils';

interface Props {}

const DefaultBreadcrumbsBooking: React.FC<Props> = props => {
  const intl = useIntl();
  const dispatch = useDispatch<ThunkDispatch<AppState, null, Action<string>>>();
  const history = useHistory();
  const { location } = history;
  const { pathname, search } = location;

  const state = location.state as { [key: string]: any };

  const isActive = React.useMemo(() => {
    return dispatch(isHasPermission(pathname));
  }, [dispatch, pathname]);

  const getList = React.useMemo(() => {
    return getAllRoutesContain(ROUTES_BOOKING, pathname);
  }, [pathname]);

  const getCurrentIndex = React.useMemo(() => {
    return getList.findIndex(v => v.path && comparePathName(pathname, v.path));
  }, [getList, pathname]);

  const getCurrent = React.useMemo(() => {
    return getCurrentRoute(pathname, ROUTES_BOOKING);
  }, [pathname]);

  const isBackAble = React.useCallback(
    (value: some): any => {
      let check = false;
      state &&
        Object.entries(state).forEach(v => {
          if (comparePathName(v[0], value.path)) {
            check = state && state[`${v[0]}`];
          }
        });
      return check;
    },
    [state],
  );

 
  const getTitle = React.useMemo(() => {
    return getCurrent?.title || getCurrent?.name;
  }, [getCurrent]);

  if (!isActive || getCurrent?.disableBreadcrumb) {
    return null;
  }

  return (
    <>
      <Helmet>
        <title>{getTitle && intl.formatMessage({ id: getTitle })}</title>
      </Helmet>
      <Breadcrumbs
        separator={<NavigateNextIcon fontSize="small" />}
        style={{ margin: '24px 0px 16px' }}
      >
        {getList.map((v: some, index: number) => (
          <Row key={index}>
            {index < getCurrentIndex ? (
              <>
                {v.path || !v.isModule ? (
                  <>
                    {isBackAble(v) ? (
                      <>
                        <Typography
                          variant="caption"
                          style={{ color: PRIMARY, cursor: 'pointer' }}
                          onClick={() => dispatch(go(-v.backStep))}
                        >
                          {index + 1}.
                          {(v.title || v.name) && <FormattedMessage id={v.title || v.name} />}
                        </Typography>
                      </>
                    ) : (
                      <>
                        <Link
                          to={{
                            pathname: v.path,
                            state: { ...state, [`${v.path}`]: true },
                          }}
                        >
                          <Typography variant="caption" style={{ color: PRIMARY }}>
                            {index + 1}.
                            {(v.title || v.name) && <FormattedMessage id={v.title || v.name} />}
                          </Typography>
                        </Link>
                      </>
                    )}
                  </>
                ) : (
                  <>
                    <Typography variant="caption" style={{ color: PRIMARY }}>
                      {index + 1}.
                      {(v.title || v.name) && <FormattedMessage id={v.title || v.name} />}
                    </Typography>
                  </>
                )}
              </>
            ) : (
              <>
                <Typography
                  variant="caption"
                  color={index === getCurrentIndex ? 'textPrimary' : 'textSecondary'}
                >
                  {index + 1}. {(v.title || v.name) && <FormattedMessage id={v.title || v.name} />}
                </Typography>
              </>
            )}
          </Row>
        ))}
      </Breadcrumbs>
    </>
  );
};

export default DefaultBreadcrumbsBooking;
