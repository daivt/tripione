import { Container, IconButton, useMediaQuery, useTheme } from '@material-ui/core';
import CloseIcon from '@material-ui/icons/CloseOutlined';
import * as React from 'react';
import { FormattedMessage } from 'react-intl';
import { shallowEqual, useDispatch, useSelector } from 'react-redux';
import { Redirect, Route, Switch, useLocation } from 'react-router';
import { Action } from 'redux';
import { ThunkDispatch } from 'redux-thunk';
import { GREY_100, GREY_700, PURPLE_100 } from '../../configs/colors';
import { ROUTES, ROUTES_BOOKING } from '../../configs/routes';
import { MUI_THEME } from '../../configs/setupTheme';
import { some } from '../../constants';
import { PageWrapper } from '../../modules/common/components/elements';
import Link from '../../modules/common/components/Link';
import LoadingIcon from '../../modules/common/components/LoadingIcon';
import { fetchGeneralData } from '../../modules/common/redux/reducer';
import { AppState } from '../../redux/reducers';
import { HEADER_HEIGHT } from '../constants';
import DefaultFooter from '../defaultLayout/DefaultFooter';
import DefaultHeader from '../defaultLayout/DefaultHeader';
import { flatRoutes, getCurrentRoute, getListRoutesActivate } from '../utils';
import DefaultBreadcrumbsBooking from './DefaultBreadcrumbsBooking';

interface Props {}

const DefaultLayoutBooking: React.FunctionComponent<Props> = props => {
  const location = useLocation();
  const userData = useSelector((state: AppState) => state.account.userData, shallowEqual);
  const dispatch = useDispatch<ThunkDispatch<AppState, null, Action<string>>>();
  const [openSideBar, setOpenSideBar] = React.useState(true);
  const matches = useMediaQuery(MUI_THEME.breakpoints.up('md'));
  const state = location.state as some;
  const theme = useTheme();
  const isTablet = useMediaQuery(theme.breakpoints.down('md'));

  React.useEffect(() => {
    setOpenSideBar(matches);
  }, [matches]);

  React.useEffect(() => {
    dispatch(fetchGeneralData());
  }, [dispatch]);

  const listRoutes = React.useMemo(() => {
    return getListRoutesActivate(userData?.roleInfo?.permissions, flatRoutes(ROUTES_BOOKING));
  }, [userData]);

  const getRoute = React.useMemo(() => {
    return getCurrentRoute(location.pathname, ROUTES_BOOKING);
  }, [location.pathname]);

  return (
    <>
      <PageWrapper style={{ background: GREY_100, minHeight: '100vh' }}>
        <DefaultHeader noSticky={getRoute?.noStickyHeader} isBookingModule />
        {state && state.tripiId && (
          <Link
            to={{
              pathname: ROUTES.tripManagement.detail.gen(state.tripiId),
              search: `?tabIndex=${state.module === 'hotel' ? 1 : 0}`,
            }}
            style={{
              position: 'fixed',
              top: isTablet ? 'unset' : HEADER_HEIGHT + 8,
              bottom: isTablet ? 80 : 'unset',
              right: 8,
              zIndex: 1100,
              cursor: 'pointer',
            }}
            tooltip={<FormattedMessage id="tripManagement.back" />}
          >
            <IconButton
              style={isTablet ? { background: PURPLE_100, color: GREY_700 } : { color: GREY_700 }}
            >
              <CloseIcon color="inherit" />
            </IconButton>
          </Link>
        )}
        <Container
          style={{
            maxWidth: 'none',
            flex: 1,
            padding: 0,
          }}
        >
          <Container>
            <DefaultBreadcrumbsBooking />
          </Container>
          <React.Suspense fallback={<LoadingIcon style={{ marginTop: 240 }} />}>
            <Switch location={location}>
              {listRoutes.map(
                (route, index) =>
                  route.component && (
                    <Route
                      key={index}
                      exact={route.exact}
                      path={route.path}
                      component={route.component}
                    />
                  ),
              )}
              <Redirect to={ROUTES.booking.notFound404} />
            </Switch>
          </React.Suspense>
        </Container>
        <DefaultFooter />
      </PageWrapper>
    </>
  );
};

export default DefaultLayoutBooking;
