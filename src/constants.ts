export type some = { [key: string]: any };

export const EVN =
  process.env.NODE_ENV === 'development' ? 'dev' : process.env.REACT_APP_ENV === 'stage' ? 'stage' : 'alpha';
// export const DEV = true;

export const SUCCESS_CODE = 200;

export const TOKEN = 'token';

export const PAGE_SIZE = 10;

export const ROW_PER_PAGE = [10, 20, 30, 50];

export const BOOKING_ID = 'bookingId';

export const TABLET_WIDTH_NUM = 1024;
export const MIN_TABLET_WIDTH_NUM = 750;
export const MOBILE_WIDTH_NUM = 480;
export const DESKTOP_WIDTH_NUM = 1260;

export const KEY_GOOGLE_MAP = 'AIzaSyB5KK2_ztp6aS5cgH6UbNyQjkf4gmhhJk4';
